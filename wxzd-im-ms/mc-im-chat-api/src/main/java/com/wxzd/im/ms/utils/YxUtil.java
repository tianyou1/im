package com.wxzd.im.ms.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.github.pagehelper.PageInfo;
import com.wxzd.im.ms.parameter.Constants;
import com.wxzd.im.ms.parameter.ResponseData;

import java.util.*;

/*
工具类
*/
public class YxUtil {

    public static void main(String args[]) {

    }

    public static Long[] splitLong(String ids) {
        if (ids == null || ids.length() == 0) {
            return new Long[0];
        }
        if (ids.indexOf(",") == -1) {
            Long id[] = new Long[1];
            id[0] = Long.parseLong(ids);
            return id;
        } else {
            ArrayList<Long> list = new ArrayList<Long>();
            String[] idString = ids.split(",");
            for (String id : idString) {
                if (id != null && id.trim().length() > 0) {
                    list.add(Long.parseLong(id));
                }
            }
            return list.toArray(new Long[list.size()]);
        }
    }

    public static String[] split(String ids) {
        if (ids == null || ids.length() == 0) {
            return new String[0];
        }
        if (ids.indexOf(",") == -1) {
            String id[] = new String[1];
            id[0] = ids;
            return id;
        } else {
            ArrayList<String> list = new ArrayList<String>();
            String[] idString = ids.split(",");
            for (String id : idString) {
                if (id != null && id.trim().length() > 0) {
                    list.add(id);
                }
            }
            return list.toArray(new String[list.size()]);
        }
    }

    public static Set<Long> splitSet(String ids) {
        if (ids == null || ids.length() == 0) {
            return new HashSet<>();
        }
        if (ids.indexOf(",") == -1) {
            Set<Long> set = new HashSet<>();
            set.add(Long.parseLong(ids));
            return set;
        } else {
            Set<Long> longSet = new HashSet<>();
            String[] idString = ids.split(",");
            for (String id : idString) {
                if (id != null && id.trim().length() > 0) {
                    longSet.add(Long.parseLong(id));
                }
            }
            return longSet;
        }
    }

    public static String setToString(Set<Long> longs) {
        if (longs == null || longs.size() == 0) {
            return "";
        }
        StringBuilder str = new StringBuilder();
        for (Long l : longs) {
            if (str.toString().equals("")) {
                str.append(l);
            } else {
                str.append(",");
                str.append(l);
            }
        }
        return str.toString();
    }

    public static String listToString(List<Long> longs) {
        if (longs == null || longs.size() == 0) {
            return "";
        }
        StringBuilder str = new StringBuilder();
        for (Long l : longs) {
            if (str.toString().equals("")) {
                str.append(l);
            } else {
                str.append(",");
                str.append(l);
            }
        }
        return str.toString();
    }


    public static ResponseData createSuccessData(Object obj) {
        return getResponse(Constants.SUCCESS_CODE, obj);
    }

    /**
     * 过滤array数据
     *
     * @param obj
     * @return
     */
    public static ResponseData createSuccessDataNoFilter(Object obj) {
        return getResponseNoFilter(Constants.SUCCESS_CODE, obj);
    }

    //过滤器 将JSON.toJSONString() 中的null值 过滤为""; JSON.toJSONString(patrol,filter)
    private static ValueFilter filter = (obj, s, v) -> {
        if (v == null)
            return "";
        return v;
    };

    public static ResponseData createSimpleSuccess(int code, Object succTipContent) {
        HashMap<String, Object> info = new HashMap<String, Object>();
        info.put("info", succTipContent);
        return getResponseNoFilter(code, info);
    }

    public static ResponseData createSimpleSuccess(Object succTipContent) {
        HashMap<String, Object> info = new HashMap<String, Object>();
        info.put("info", succTipContent);
        return getResponseNoFilter(Constants.SUCCESS_CODE, info);
    }

    public static ResponseData createFail(String failContent) {
        HashMap<String, Object> info = new HashMap<String, Object>();
        info.put("info", failContent);
        return getResponseNoFilter(Constants.FAIL_CODE, info);
    }


    public static ResponseData getResponse(int code, Object content) {
        ResponseData data = new ResponseData();
        content = JSONObject.parseObject(JSON.toJSONString(content, filter));
        data.setCode(code);
        data.setData(content);
        return data;
    }

    public static ResponseData getResponseNoFilter(int code, Object content) {
        ResponseData data = new ResponseData();
        JSONObject json = new JSONObject();
        json.put("jsonValue", content);
        content = JSONObject.parseObject(JSON.toJSONString(json, filter));
        data.setCode(code);
        data.setData(((JSONObject) content).get("jsonValue"));
        return data;
    }

    /**
     * 获取返回
     *
     * @param list list
     * @return ResponseResult
     */
    public static ResponseData getPageResponse(List<?> list) {
        // 获取分页查询后的数据
        PageInfo<?> pageInfo = new PageInfo<>(list);
        //总数量
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if (0 == total) {
            return createFail("未查询到数据");
        } else {
            return createSuccessData(pageInfo);
        }
    }
}
