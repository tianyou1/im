package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImUserSession;
import com.wxzd.im.ms.entity.ImUserSessionKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ImUserSessionMapper {
    int deleteByPrimaryKey(ImUserSessionKey key);

    int insert(ImUserSession record);

    int insertSelective(ImUserSession record);

    ImUserSession selectByPrimaryKey(ImUserSessionKey key);

    int updateByPrimaryKeySelective(ImUserSession record);

    int updateByPrimaryKey(ImUserSession record);

    int createSession(ImUserSession record);

    int deleteByCondition(ImUserSession imUserSession);

    int batchDelete(List<ImUserSession> list);

    /**
     * 修改聊天会话最后的内容
     *
     * @param lastContent
     * @param userId
     * @param destId
     * @param destType
     * @return
     */
    int updateSessionLastContent(@Param("lastContent") String lastContent, @Param("userId") Long userId, @Param("destId") Long destId, @Param("destType") Integer destType);

    /**
     * 读取会话
     *
     * @param userId
     * @param destId
     * @param destType
     * @return
     */
    int readSessionCount(@Param("userId") Long userId, @Param("destId") Long destId, @Param("destType") Integer destType);

    /**
     * 删除会话
     *
     * @param userId
     * @param destId
     * @param destType
     * @return
     */
    int deleteSession(@Param("userId") Long userId, @Param("destId") Long destId, @Param("destType") Integer destType);

    int batchCreateSession(@Param("list") List<ImUserSession> list);
}