package com.wxzd.im.ms.api.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.parameter.LockKey;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.parameter.StaticConstant;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wxzd.im.ms.parameter.IMConstants.*;
import static com.wxzd.im.ms.parameter.ReturnConstant.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;


@Controller
@Api(value = "好友接口")
@RequestMapping(value = "/userFriend", method = {RequestMethod.POST})
public class UserFriendController {

    @Autowired
    private MessageFactory messageFactory;
    @Autowired
    private ImUserService userService;
    @Autowired
    private ImGroupMemberService groupMemberService;
    @Autowired
    private ImGroupService groupService;
    @Autowired
    private ImTempIdentityService tempIdentityService;
    @Autowired
    private ImFriendService friendService;
    @Autowired
    private ImUserRequestAuditService userRequestAuditService;
    @Autowired
    private ReadyService readyService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CacheService cacheService;
    @Autowired
    private ChatServerFeign chatServerFeign;

    @ApiOperation("截屏操作通知")
    @RequestMapping("/screenShotNotify")
    @ResponseBody
    public ResponseData screenShotNotify(@RequestHeader String token,
                                         @ApiParam("好友id") @RequestParam long friendId,
                                         @ApiParam("自己名称") @RequestParam String username) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer device = CommonUtils.getDevTypeByToken(token);
        //判断双方是否为好友
        boolean b = friendService.isFriendCheck(userId, friendId);
        String name;
        if (StringUtils.isNotBlank(username)) {
            name = username;
        } else {
            ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
            name = user.getNickName();
        }
        if (b) {
            // 组装消息
            JSONObject json = new JSONObject();
            json.put("name", name);
            json.put("userId", userId);
            json.put("textTip", "name截取了屏幕");
            ImMessage message = messageFactory.screenShotNotice(userId, friendId, device, json.toJSONString());
            //同步好友那边的消息
            message.setSystemType(p2PSingleSystemType);
            message.setGeoId(friendId);
            chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(message), friendId);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } else {
            return YxUtil.createFail(operationErrorReturn);
        }

    }

    @ApiOperation("设置好友阅后即焚")
    @RequestMapping("/setFriendReadDel")
    @ResponseBody
    public ResponseData setSecondAuth(@RequestHeader String token,
                                      @ApiParam("好友id") @RequestParam long friendId,
                                      @ApiParam("是否启动阅后即焚：0否，1是") @RequestParam int isEphemeralChat,
                                      @ApiParam("消息保留时间（N秒）") @RequestParam int ephemeralChatTime,
                                      @ApiParam("自己名称") @RequestParam String username) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer device = CommonUtils.getDevTypeByToken(token);
        if (isEphemeralChat == setOn && ephemeralChatTime < defaultChatTime) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //修改好友表和对方的好友表（两边同步阅后即焚时间和是否开启）

        ImFriend friend = cacheService.getFriend(friendId, userId);

        boolean b = friendService.updateCheck(userId, friendId, isEphemeralChat, ephemeralChatTime, friend);
        if (!b) {
            return YxUtil.createFail(notFriendBothReturn);
        }


        //通知
        int msgType;
        String textTip;
        ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
        JSONObject json = new JSONObject();
        json.put("is_ephemeral_chat", isEphemeralChat);
        json.put("ephemeral_chat_time", ephemeralChatTime);
        if (isEphemeralChat == setOn) {
            //时间转换
            String time = CommonUtils.getEphemeralChatTime(ephemeralChatTime);
            msgType = MSG_TYPE_FRIEND_SET_EPHEMERALCHAT;
            textTip = "name设置了" + time + "的阅后即焚";
        } else {
            msgType = MSG_TYPE_FRIEND_OFF_EPHEMERALCHAT;
            textTip = "name取消了阅后即焚";
        }
        json.put("textTip", textTip);
        json.put("name", user.getNickName());
        json.put("userId", userId);
        ImMessage msg = messageFactory.friendSetNotice(userId, friendId, device, msgType, json.toJSONString());
        //通知自己
        commonService.noticeMyself(msg, device, userId);

        //如果好友拉黑了自己，不做双向消息处理
        if (friend.getIsBlack().equals(isBlack)) {
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        }
        //通知好友
        commonService.noticeFriendAndSaveBoth(msg, 0, friendId);
        return YxUtil.createSimpleSuccess(operationSuccessReturn);
    }


    @ApiOperation("通过用户ID、用户账号、临时身份ID添加")
    @RequestMapping("/findFriendInfo")
    @ResponseBody
    public ResponseData findFriendInfo(@RequestHeader String token,
                                       @ApiParam("好友信息：用户ID、用户账号、临时身份ID") @RequestParam String friendInfo) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找账号
//        ImUserWithBLOBs friend = userService.selectByAccount(friendInfo);

        ImUserWithBLOBs friend = null;
        JSONObject json = new JSONObject();

        //查找id
        try {
            friend = userService.selectByPrimaryKey(Long.parseLong(friendInfo));
        } catch (NumberFormatException e) {
            return YxUtil.createFail(paramErrorReturn);
        }
        if (friend == null) {
            //查找临时id
            ImTempIdentity tempIdentity = tempIdentityService.selectByTempId(Long.parseLong(friendInfo));
            if (tempIdentity == null) {
                return YxUtil.createFail(tempIdNoFoundReturn);
            } else {

                friend = userService.selectByPrimaryKey(tempIdentity.getIdentityId());
                if (friend == null) {
                    return YxUtil.createFail(destAccountInvalidReturn);
                } else {
                    if (userId == friend.getId()) {
                        return YxUtil.createFail(findMyselfIdReturn);
                    }
                    json.put("identityType", 3);
                }
            }
        } else {
            if (userId == friend.getId()) {
                return YxUtil.createFail(findMyselfIdReturn);
            }
            json.put("identityType", 1);
        }


        //自己的
        ImFriend userFriend = cacheService.getFriend(userId, friend.getId());
        //好友的
        ImFriend friendUser = cacheService.getFriend(friend.getId(), userId);
        if (friendUser != null) {
            if (friendUser.getIsFriend().equals(isFriendStranger)) {
                return YxUtil.createFail(notFriendReturn);
            }
        }
        if (checkFriendBoth(userFriend, friendUser)) {
            json.put("id", friend.getId());
            json.put("remark", userFriend.getRemark());
        } else {
            json.put("id", "");
            json.put("remark", "");
        }
        json.put("city", friend.getCity());
        json.put("province", friend.getProvince());
        json.put("nickName", friend.getNickName());
        json.put("sex", friend.getSex());
        json.put("headUrl", friend.getHeadUrl());
        json.put("sign", friend.getSign());
        return YxUtil.createSuccessData(json);

    }

    //判断是否是已经添加的好友
    private boolean checkFriendBoth(ImFriend userFriend, ImFriend friendUser) {
        return userFriend != null && userFriend.getIsFriend().equals(isFriendBoth) && friendUser != null && friendUser.getIsFriend().equals(isFriendBoth);
    }


    @ApiOperation("根据好友id添加（好友临时id或者id）")
    @RequestMapping("/requestFriendByUserId")
    @ResponseBody
    public ResponseData requestFriendByUserId(@RequestHeader String token,
                                              @ApiParam("好友id或者好友临时id") @RequestParam long friendId,
                                              @ApiParam("私密联系人，0否，1是") @RequestParam(defaultValue = "0") int isSecretFriend,
                                              @ApiParam("好友备注") @RequestParam(required = false) String remark,
                                              @ApiParam("申请理由") @RequestParam(required = false) String requestContent) {
        if (StringUtils.isNotBlank(requestContent)) {
            if (!CheckUtil.checkFriendReq(requestContent)) {
                return YxUtil.createFail(maxFriendReqNumReturn);
            }
        }

        if (StringUtils.isNotBlank(remark)) {
            if (!CheckUtil.checkRemark(remark)) {
                return YxUtil.createFail(maxRemarkNumReturn);
            }
        }

        long userId = CommonUtils.getUserIdByToken(token);
        String lockKey = LockKey.reqFriendByUserIdKey + friendId + "_" + userId + "_" + isSecretFriend;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {
            ImUserWithBLOBs friend = userService.selectByPrimaryKey(friendId);
            if (friend == null) {
                //查找临时id或者临时二维码
                ImTempIdentity tempIdentity = tempIdentityService.selectByTempId(friendId);
                if (tempIdentity == null) {
                    return YxUtil.createFail(tempIdentityNoFoundReturn);
                } else {
                    if (!CheckUtil.checkQRCodeExpiryTime(tempIdentity)) {
                        return YxUtil.createFail(tempIdentityEndTimeReturn);
                    }

                    if (!CheckUtil.checkQRCodeCurrentTimes(tempIdentity)) {
                        return YxUtil.createFail(tempIdentityEndUseReturn);
                    }
                    friend = userService.selectByPrimaryKey(tempIdentity.getIdentityId());
                    if (friend == null) {
                        return YxUtil.createFail(destAccountInvalidReturn);
                    }

                    if (userId == friend.getId()) {
                        return YxUtil.createFail(addMyselfReturn);
                    }
                    tempIdentity.setCurrentTimes(tempIdentity.getCurrentTimes() + 1);
                    tempIdentityService.updateByPrimaryKeySelective(tempIdentity);
                }
            } else {
                String myself = checkMyself(userId, friend);
                if (StringUtils.isNotEmpty(myself)) {
                    return YxUtil.createFail(myself);
                }
            }
            return requestFriendResult(userId, friend.getId(), idType, isSecretFriend, remark, requestContent);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(addErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }

    }

    /**
     * 验证是否查询自己
     *
     * @param userId 用户id
     * @param friend 好友
     * @return String
     */
    private String checkMyself(long userId, ImUserWithBLOBs friend) {
        if (userId == friend.getId()) {
            return addMyselfReturn;
        }

        if (friend.getSearchUserId().equals(searchUserIdClose)) {
            return addByIdCloseReturn;
        }
        return null;
    }

    @ApiOperation("根据好友账号添加")
    @RequestMapping("/requestFriendByAccount")
    @ResponseBody
    public ResponseData requestFriendByAccount(@RequestHeader String token,
                                               @ApiParam("好友id") @RequestParam long friendId,
                                               @ApiParam("好友昵称") @RequestParam String nickname,
                                               @ApiParam("私密联系人，0否，1是") @RequestParam(defaultValue = "0") int isSecretFriend,
                                               @ApiParam("好友备注") @RequestParam(required = false) String remark,
                                               @ApiParam("申请理由") @RequestParam(required = false) String requestContent) {
        if (StringUtils.isNotBlank(requestContent)) {
            if (!CheckUtil.checkFriendReq(requestContent)) {
                return YxUtil.createFail(maxFriendReqNumReturn);
            }
        }

        if (StringUtils.isNotBlank(remark)) {
            if (!CheckUtil.checkRemark(remark)) {
                return YxUtil.createFail(maxRemarkNumReturn);
            }
        }
        long userId = CommonUtils.getUserIdByToken(token);
        String lockKey = LockKey.reqFriendByAccountKey + friendId + "_" + userId + "_" + isSecretFriend;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {
            ImUserWithBLOBs imUser = userService.selectByPrimaryKey(friendId);
            if (imUser.getSearchAccount().equals(searchAccountClose)) {
                return YxUtil.createFail(addByAccountCloseReturn);
            }

            if (userId == imUser.getId()) {
                return YxUtil.createFail(addMyselfReturn);
            }

            if (!nickname.equals(imUser.getNickName())) {
                return YxUtil.createFail(accountLoseEfficacy);
            }
            return requestFriendResult(userId, imUser.getId(), businessCardType, isSecretFriend, remark, requestContent);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(addErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }

    }

    /**
     * 添加好友公共方法
     *
     * @param userId
     * @param friendId
     * @param identityType
     * @param isSecretFriend
     * @param remark
     * @param requestContent
     * @return
     */
    private ResponseData requestFriendResult(Long userId, Long friendId, Integer identityType, Integer isSecretFriend, String remark, String requestContent) {
        int code = friendService.requestFriendById(userId, friendId, identityType, isSecretFriend, remark, requestContent);
        if (code == 1) {
            return YxUtil.createSimpleSuccess(addSuccessReturn);
        } else if (code == 2) {
            return YxUtil.createFail(reqAlreadySendReturn);
        } else if (code == 3) {
            return YxUtil.createFail(friendEachReturn);
        } else if (code == 4) {
            return YxUtil.createSimpleSuccess(addApplySuccessReturn);
        } else {
            return YxUtil.createFail(addErrorReturn);
        }
    }

    @ApiOperation("根据群成员id添加")
    @RequestMapping("/requestFriendByGroupMemberId")
    @ResponseBody
    public ResponseData requestFriendByGroupMemberId(@RequestHeader String token,
                                                     @ApiParam("群成员id") @RequestParam long groupMemberId,
                                                     @ApiParam("私密联系人，0否，1是") @RequestParam(defaultValue = "0") int isSecretFriend,
                                                     @ApiParam("好友备注") @RequestParam(required = false) String remark,
                                                     @ApiParam("申请理由") @RequestParam(required = false) String requestContent) {
        if (StringUtils.isNotBlank(requestContent)) {
            if (!CheckUtil.checkFriendReq(requestContent)) {
                return YxUtil.createFail(maxFriendReqNumReturn);
            }
        }

        if (StringUtils.isNotBlank(remark)) {
            if (!CheckUtil.checkRemark(remark)) {
                return YxUtil.createFail(maxRemarkNumReturn);
            }
        }
        long userId = CommonUtils.getUserIdByToken(token);

        String lockKey = LockKey.reqFriendByGroupMemberIdKey + groupMemberId + "_" + userId + "_" + isSecretFriend;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {
            ImGroupMember imGroupMember = groupMemberService.selectByPrimaryKey(groupMemberId);
            ImUser friend = userService.selectByPrimaryKey(imGroupMember.getUserId());
            if (friend.getSearchGroupId().equals(searchGroupIdClose)) {
                return YxUtil.createFail(addByGroupCloseReturn);
            }
            ImGroup imGroup = groupService.selectByPrimaryKey(imGroupMember.getGroupId());
            ImGroupMember imUserGroupMemberInfo = groupMemberService.selectByPrimaryKey(userId);
            if (isFriendEachOtherOpen.equals(imGroup.getIsFriendEachOther())
                    && CheckUtil.checkPlainMember(imUserGroupMemberInfo)) {
                return YxUtil.createFail(groupAddByFriendCloseReturn);
            }

            if (userId == friend.getId()) {
                return YxUtil.createFail(addMyselfReturn);
            }
            return requestFriendResult(userId, imGroupMember.getUserId(), temporaryIdType, isSecretFriend, remark, requestContent);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(addErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }


    }

    @ApiOperation("处理好友申请")
    @RequestMapping("/dealFriendRequest")
    @ResponseBody
    public ResponseData dealFriendRequest(@RequestHeader String token,
                                          @ApiParam("好友id") @RequestParam long friendId,
                                          @ApiParam("确认状态：1：同意，2：拒绝") @RequestParam int confirmState,
                                          @ApiParam("好友备注") @RequestParam(required = false) String remark,
                                          @ApiParam("私密联系人，0否，1是") @RequestParam(defaultValue = "0") int isSecretFriend) {
        if (!CheckUtil.checkRemark(remark)) {
            return YxUtil.createFail(maxRemarkNumReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);

        String lockKey = LockKey.dealFriendReqKey + friendId + "_" + userId + "_" + confirmState + "_" + isSecretFriend;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {

            ImFriend userFriend = cacheService.getFriend(userId, friendId);
            ImFriend friend = cacheService.getFriend(friendId, userId);
            //如果是好友，同意直接修改请求
            if (isFriend(userFriend, friend)) {
                //修改请求列表
                userRequestAuditService.updateByUserIdAndFriendId(agreeStatus, userId, System.currentTimeMillis(), friendId);
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            }

            int count = friendService.countFriends(userId);
            if (count >= friendCount) {
                return YxUtil.createFail(maxFriendNumReturn);
            }
            int fCount = friendService.countFriends(friendId);
            if (fCount >= friendCount) {
                return YxUtil.createFail(maxOppositeFriendNumReturn);
            }

            ImUserRequestAudit requestAudit = userRequestAuditService.selectByCondition(
                    new ImUserRequestAudit()
                            .setDestId(userId)
                            .setFromId(friendId).setDestType(singleType)
            );

            //转成天
            long day = (System.currentTimeMillis() - requestAudit.getRequestTime()) / (1000 * 60 * 60 * 24);
            if (day > 7) {
                return YxUtil.createFail(msgTimeOutReturn);
            }
            return friendService.dealFriendRequest(userId, device, friendId, confirmState, remark, isSecretFriend, userFriend, friend);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }

    }

    //判断双方是否为好友
    private boolean isFriend(ImFriend userFriend, ImFriend friend) {
        return userFriend != null && isFriendBoth.equals(userFriend.getIsFriend()) && friend != null && isFriendBoth.equals(friend.getIsFriend());
    }


    @ApiOperation("好友备注（设置接口）")
    @RequestMapping("/updateRemark")
    @ResponseBody
    public ResponseData updateRemark(@RequestHeader String token,
                                     @ApiParam("好友id") @RequestParam(defaultValue = "0") long friendId,
                                     @ApiParam("新备注") @RequestParam(defaultValue = "") String remark) {
        if (!CheckUtil.checkRemark(remark)) {
            return YxUtil.createFail(maxRemarkNumReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        if (friendId > 0 && userId > 0) {
            ImFriend imFriend = cacheService.getFriend(userId, friendId);
            imFriend.setRemark(remark);
            int count = friendService.updateByPrimaryKeySelective(imFriend);
            if (count > 0) {
                //更新好友缓存
                cacheService.updateFriendCache(imFriend);
                // 通知我的其它设备
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("remark", remark);
                sendMsg(userId, friendId, device, MSG_TYPE_FRIEND_UPDATE_REMARK, JSON.toJSONString(jsonObject), userId);
                return getSetRes(count);
            }
        }
        return YxUtil.createFail(operationErrorReturn);
    }

    @ApiOperation("获取黑名单列表")
    @RequestMapping("/getBlackList")
    @ResponseBody
    public ResponseData getBlackList(@RequestHeader String token,
                                     @ApiParam("用户昵称或好友备注") @RequestParam(required = false) String username) {
        long userId = CommonUtils.getUserIdByToken(token);

        if (userId > 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("userId", userId);
            if (StringUtils.isNotEmpty(username)) {
                map.put("name", username);
            }
            return YxUtil.createSuccessDataNoFilter(friendService.getBlackList(map));
        }
        return YxUtil.createFail(getErrorReturn);
    }


    @ApiOperation("删除好友")
    @RequestMapping("/delFriend")
    @ResponseBody
    public ResponseData delFriend(@RequestHeader String token,
                                  @ApiParam("好友id") @RequestParam long friendId) {
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        if (userId > 0) {
            userService.delUser(userId, friendId, device);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        }
        return YxUtil.createFail(operationErrorReturn);
    }

    @ApiOperation("好友列表查询接口")
    @RequestMapping("/queryFriendList")
    @ResponseBody
    public ResponseData queryFriendList(@RequestHeader String token) {
        HashMap<String, Object> data = new HashMap<>();
        long userId = CommonUtils.getUserIdByToken(token);
        //查询是否开启私密模式
        String isSecret = CommonUtils.getSecretByToken(token);
        //如果token是私密 则加载私密好友列表
        data.put("friendsInfo", readyService.getFriendInfo(userId, Integer.valueOf(isSecret)));
        return YxUtil.createSuccessData(data);
    }

    @ApiOperation("好友明细信息获取接口")
    @RequestMapping("/friendDetail")
    @ResponseBody
    public ResponseData friendDetail(@RequestHeader String token,
                                     @ApiParam("好友id") @RequestParam(defaultValue = "0") long friendId) {
        JSONObject jsonObject = new JSONObject();
        long userId = CommonUtils.getUserIdByToken(token);
        if (userId > 0 && friendId > 0) {
            //获取详细信息
            List<Map<String, Object>> friendDetail = userService.friendDetail(userId, friendId);
            if (friendDetail != null && friendDetail.size() > 0) {
                jsonObject.put("friendDetail", friendDetail);
                return YxUtil.createSuccessData(jsonObject);
            } else {
                return YxUtil.createFail(getErrorReturn);
            }
        }
        return YxUtil.createFail(getErrorReturn);
    }


    @ApiOperation("接收消息提示设置")
    @RequestMapping("/showSessionMessageSet")
    @ResponseBody
    public ResponseData showSessionMessageSet(@RequestHeader String token,
                                              @ApiParam("会话消息显示，0否，1是") @RequestParam Integer showSessionMessage,
                                              @ApiParam("好友id") @RequestParam long friendId) {
        if (CheckUtil.checkSetVau(showSessionMessage)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        friend.setIsShowSessionMessage(showSessionMessage);
        int res = friendService.updateByPrimaryKeySelective(friend);
        if (res > 0) {
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }

    @ApiOperation("加入黑名单设置")
    @RequestMapping("/blackSetOn")
    @ResponseBody
    public ResponseData blackSetOn(@RequestHeader String token,
                                   @ApiParam("好友id") @RequestParam long friendId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        //更新数据库
        friend.setIsBlack(setOn);
        int count = friendService.updateByPrimaryKeySelective(friend);
        if (count > 0) {
            //删除会话
            commonService.deleteFriendSession(userId, friendId, singleType);
            //删除黑名单缓存
            cacheService.setBlack(friend);
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }

    }

    @ApiOperation("移出黑名单设置")
    @RequestMapping("/blackSetOff")
    @ResponseBody
    public ResponseData blackSetOff(@RequestHeader String token,
                                    @ApiParam("好友id") @RequestParam long friendId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(operationErrorReturn);
        }
        int count;
        //获取好友类型
        int type = friend.getIsFriend();
        //如果是陌生人
        if (type == isFriendStranger) {

            count = commonService.deleteFriend(friendId, userId);


        } else {
            friend.setIsBlack(setOff);
            count = friendService.updateByPrimaryKeySelective(friend);
        }
        if (count > 0) {
            //删除黑名单
            cacheService.setBlack(friend);
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }


    @ApiOperation("接收消息提示设置")
    @RequestMapping("/receiveTipSet")
    @ResponseBody
    public ResponseData receiveTipSet(@RequestHeader String token,
                                      @ApiParam(" 1接收提示0不接收提示") @RequestParam Integer receiveTip,
                                      @ApiParam("好友id") @RequestParam long friendId) {
        if (CheckUtil.checkSetVau(receiveTip)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        friend.setReceiveTip(receiveTip);
        int res = friendService.updateByPrimaryKeySelective(friend);
        if (res > 0) {
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }


    @ApiOperation("好友聊天记录加密开启接口")
    @RequestMapping("/openChatHistory")
    @ResponseBody
    public ResponseData openChatHistory(@RequestHeader String token,
                                        @ApiParam("好友id") @RequestParam long friendId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
        if (StringUtils.isEmpty(user.getSecretModePwd())) {
            return YxUtil.createFail(noSecretModePwdReturn);
        }
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        friend.setIsSecretHistory(setOn);
        int res = friendService.updateByPrimaryKeySelective(friend);
        if (res > 0) {
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }


    /**
     * 好友聊天记录加密取消接口
     *
     * @param token     token
     * @param friendId  好友id
     * @param secretPwd 隐私密码
     * @return
     */
    @ApiOperation("好友聊天记录加密取消接口")
    @RequestMapping("/closeChatHistory")
    @ResponseBody
    public ResponseData closeChatHistory(@RequestHeader String token,
                                         @ApiParam("好友id") @RequestParam long friendId,
                                         @ApiParam("隐私密码") @RequestParam String secretPwd) {
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        //解密私密模式密码
        secretPwd = RSACipherUtil.decrypt(secretPwd);
        if (CheckUtil.checkSecretPwd(imUser, secretPwd)) {
            return YxUtil.createFail(secretPwdErrorReturn);
        }
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        friend.setIsSecretHistory(setOff);
        int count = friendService.updateByPrimaryKeySelective(friend);
        if (count > 0) {
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            JSONObject json = new JSONObject();
            json.put("is_secret_history", setOff);
            sendMsg(userId, friendId, device, MSG_TYPE_FRIEND_OFF_SECRETHISTORY, json.toJSONString(), userId);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }

    @ApiOperation("开启私密联系人接口")
    @RequestMapping("/openSecretFriend")
    @ResponseBody
    public ResponseData openSecretFriend(@RequestHeader String token,
                                         @ApiParam("好友id") @RequestParam long friendId) {
        if (!CheckUtil.checkSecretModel(token)) {
            return YxUtil.createFail(notInSecretModelReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        friend.setIsSecretFriend(setOn);
        int res = friendService.updateByPrimaryKeySelective(friend);
        if (res > 0) {
            //更新私密好友缓存
            cacheService.secretFriendChange(friend);
            //更新好友缓存
            cacheService.updateFriendCache(friend);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }

    /**
     * 取消私密联系人接口
     *
     * @param token    token
     * @param friendId 好友id
     * @return
     */
    @ApiOperation("取消私密联系人接口")
    @RequestMapping("/closeSecretFriend")
    @ResponseBody
    public ResponseData closeSecretFriend(@RequestHeader String token,
                                          @ApiParam("好友id") @RequestParam long friendId) {
        if (!CheckUtil.checkSecretModel(token)) {
            return YxUtil.createFail(notInSecretModelReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        ImFriend imFriend = cacheService.getFriend(userId, friendId);
        if (imFriend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        imFriend.setIsSecretFriend(setOff);
        int count = friendService.updateByPrimaryKeySelective(imFriend);
        if (count > 0) {
            //更新私密好友缓存
            cacheService.secretFriendChange(imFriend);
            //更新好友缓存
            cacheService.updateFriendCache(imFriend);
            JSONObject json = new JSONObject();
            json.put("is_secret_friend", setOff);
            //通知自己的其他设备
            sendMsg(userId, friendId, device, MSG_TYPE_FRIEND_OFF_SECRETFRIEND, json.toJSONString(), userId);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }


    /**
     * 设置返回
     *
     * @param num num
     * @return ResponseData
     */
    private static ResponseData getSetRes(int num) {
        if (num > 0) {
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }

    }

    /**
     * 获取请求列表(分页)
     *
     * @param name 好友昵称
     * @return ResponseData
     */
    @ApiOperation("获取请求列表(分页)")
    @RequestMapping("/friendRequestPage")
    @ResponseBody
    public ResponseData friendRequestPage(@RequestHeader String token,
                                          @ApiParam("好友昵称或者备注") @RequestParam(defaultValue = "") String name,
                                          @ApiParam("当前页") @RequestParam(defaultValue = "1") int page,
                                          @ApiParam("每页大小") @RequestParam(defaultValue = "20") int size) {
        long userId = CommonUtils.getUserIdByToken(token);
        PageHelper.startPage(page, size);

        Page listPage = new Page();
        listPage.setPageNo(page);
        listPage.setPageSize(size);
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("name", name);
        List<Map<String, Object>> list = groupService.friendReqList(map);
        int totalCount = groupService.countFriendReqList(map);
        listPage.setTotalCount(totalCount);
        listPage.setList(list);
        for (Map<String, Object> object : list) {
            long requestTime = (long) object.get("requestTime");
            //转成天
            long day = (System.currentTimeMillis() - requestTime) / (1000 * 60 * 60 * 24);
            if (day > 7 && !object.get("auditStatus").equals(agreeStatus)) {
                object.put("requestStatus", '3');
            }
        }

        return YxUtil.createSuccessData(listPage);
    }

    /**
     * 好友信息设置接口
     *
     * @param friendId        好友（目的)id
     * @param receiveTip      1接收提示0不接收提示
     * @param isBlack         0不是黑名单1黑名单
     * @param isSecretFriend  1是私密联系人0不是
     * @param isScreenshotTip 1开启截屏提醒0关闭
     * @return ResponseData
     */
    @ApiOperation("好友信息设置接口")
    @RequestMapping("/friendInfoSet")
    @ResponseBody
    public ResponseData friendInfoSet(@RequestHeader String token,
                                      @ApiParam("好友id") @RequestParam(defaultValue = "0") long friendId,
                                      @ApiParam(" 1接收提示0不接收提示") @RequestParam(defaultValue = "") String receiveTip,
                                      @ApiParam("0不是黑名单1黑名单") @RequestParam(defaultValue = "") String isBlack,
                                      @ApiParam("1是私密联系人") @RequestParam(defaultValue = "") String isSecretFriend,
                                      @ApiParam("1开启截屏提醒0关闭") @RequestParam(defaultValue = "") String isScreenshotTip,
                                      @ApiParam("聊天记录是否加密，1是") @RequestParam(defaultValue = "") String isSecretHistory,
                                      @ApiParam("会话消息显示，0否，1是") @RequestParam(defaultValue = "") String isShowSessionMessage) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查询是否开启私密模式
        int device = CommonUtils.getDevTypeByToken(token);
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }
        try {
            int msgType = 0;
            JSONObject json = new JSONObject();
            int count = 0;
            //消息免打扰 推设备
            if (StringUtils.isNotBlank(receiveTip)) {
                if (!CheckUtil.checkSetVau(receiveTip)) {
                    return YxUtil.createFail(paramErrorReturn);
                }
                if (receiveTip.equals(setOn.toString())) {
                    msgType = MSG_TYPE_FRIEND_OFF_RECEIVETIP;
                } else {
                    msgType = MSG_TYPE_FRIEND_SET_RECEIVETIP;
                }
                json.put("receiveTip", receiveTip);
                friend.setReceiveTip(Integer.valueOf(receiveTip));
                count = friendService.updateByPrimaryKeySelective(friend);
            }
            //黑名单 推设备
            if (StringUtils.isNotBlank(isBlack)) {
                if (!CheckUtil.checkSetVau(isBlack)) {
                    return YxUtil.createFail(paramErrorReturn);
                }
                if (isBlack.equals(setOn.toString())) {
                    msgType = MSG_TYPE_FRIEND_SET_BLACK;
                    friend.setIsBlack(setOn);
                    count = friendService.updateByPrimaryKeySelective(friend);
                    commonService.deleteFriendSession(userId, friendId, singleType);
                } else {
                    msgType = MSG_TYPE_FRIEND_OFF_BLACK;
                    //获取好友类型
                    int type = friend.getIsFriend();
                    //如果是陌生人
                    if (type == isFriendStranger) {
                        count = commonService.deleteFriend(friendId, userId);
                    } else {
                        friend.setIsBlack(setOff);
                        count = friendService.updateByPrimaryKeySelective(friend);
                    }
                }
                json.put("isBlack", isBlack);
                cacheService.setBlack(friend);
            }
            //是否为私密联系人 推设备
            if (StringUtils.isNotBlank(isSecretFriend)) {
                if (!isSecretFriend.equals(setOn.toString())) {
                    return YxUtil.createFail(paramErrorReturn);
                }
                msgType = MSG_TYPE_FRIEND_SET_SECRETFRIEND;
                json.put("is_secret_friend", isSecretFriend);
                friend.setIsSecretFriend(setOn);
                count = friendService.updateByPrimaryKeySelective(friend);
                //更新私密好友缓存
                cacheService.secretFriendChange(friend);
            }
            //是否提醒屏幕截图 推
            if (StringUtils.isNotBlank(isScreenshotTip)) {
                if (!CheckUtil.checkSetVau(isScreenshotTip)) {
                    return YxUtil.createFail(paramErrorReturn);
                }
                ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
                String textTip;
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("is_screenshot_tip", isScreenshotTip);
                if (isScreenshotTip.equals(setOn.toString())) {
                    msgType = MSG_TYPE_FRIEND_SET_SCREENSHOTTIP;
                    textTip = "name设置了截屏提醒";
                } else {
                    msgType = MSG_TYPE_FRIEND_OFF_SCREENSHOTTIP;
                    textTip = "name取消了截屏提醒";
                }
                friend.setIsScreenshotTip(Integer.valueOf(isScreenshotTip));
                count = friendService.updateByPrimaryKeySelective(friend);
                jsonObject.put("textTip", textTip);
                jsonObject.put("name", user.getNickName());
                jsonObject.put("userId", userId);
                ImMessage msg = messageFactory.friendSetNotice(userId, friendId, device, msgType, jsonObject.toJSONString());
                commonService.noticeMyself(msg, device, userId);

                ImFriend frienduser = cacheService.getFriend(friendId, userId);

                //如果好友拉黑了自己，不做双向消息处理
                if (frienduser.getIsBlack().equals(StaticConstant.isBlack)) {
                    return YxUtil.createSimpleSuccess(operationSuccessReturn);
                }
                commonService.noticeFriendAndSaveBoth(msg, 0, friendId);
            }
            //是否聊天记录加密 推设备
            if (StringUtils.isNotBlank(isSecretHistory)) {
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                if (StringUtils.isEmpty(user.getSecretModePwd())) {
                    return YxUtil.createFail(noSecretModePwdReturn);
                }
                if (!isSecretHistory.equals(setOn.toString())) {
                    return YxUtil.createFail(paramErrorReturn);
                }
                friend.setIsSecretHistory(setOn);
                count = friendService.updateByPrimaryKeySelective(friend);
                msgType = MSG_TYPE_FRIEND_SET_SECRETHISTORY;
                json.put("is_secret_history", isSecretHistory);
            }
            //是否会话消息显示 推设备
            if (StringUtils.isNotBlank(isShowSessionMessage)) {
                if (!CheckUtil.checkSetVau(isShowSessionMessage)) {
                    return YxUtil.createFail(paramErrorReturn);
                }
                if (isShowSessionMessage.equals(setOn.toString())) {
                    msgType = MSG_TYPE_FRIEND_SET_SHOWMESSAGE;
                } else {
                    msgType = MSG_TYPE_FRIEND_OFF_SHOWMESSAGE;
                }
                friend.setIsShowSessionMessage(Integer.valueOf(isShowSessionMessage));
                count = friendService.updateByPrimaryKeySelective(friend);
                json.put("is_show_session_message", isShowSessionMessage);
            }
            if (count > 0) {
                //更新好友缓存
                cacheService.updateFriendCache(friend);
                //通知自己的其他设备
                if (StringUtils.isBlank(isScreenshotTip)) {
                    sendMsg(userId, friendId, device, msgType, json.toJSONString(), userId);
                }
                return YxUtil.createSimpleSuccess(setSuccessReturn);
            } else {
                return YxUtil.createFail(setErrorReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    /**
     * 通知
     *
     * @param userId
     * @param friendId
     * @param device
     * @param msgType
     * @param content
     * @param destId
     */
    private void sendMsg(long userId, long friendId, int device, int msgType, String content, long destId) {
        ImMessage msg = messageFactory.friendSetNotice(userId, friendId, device, msgType, content);
        commonService.noticeMyself(msg, device, destId);
    }

    /**
     * 获取推送名称
     *
     * @param userId
     * @param friendId
     * @return
     */
    private String getReturnName(long userId, long friendId) {
        ImFriend friend = cacheService.getFriend(userId, friendId);
        if (friend != null && StringUtils.isNotBlank(friend.getRemark())) {
            return friend.getRemark();
        } else {
            ImUser user = userService.selectByPrimaryKey(friendId);
            return user.getNickName();
        }
    }


}
