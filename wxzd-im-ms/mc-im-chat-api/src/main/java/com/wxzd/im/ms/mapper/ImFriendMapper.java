package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImFriend;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface ImFriendMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImFriend record);

    int insertSelective(ImFriend record);

    ImFriend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImFriend record);

    int updateByPrimaryKey(ImFriend record);

    ImFriend selectByFriendIdAndUserId(ImFriend friend);

    List<ImFriend> selectFriends(Long userId);

    int updateByUserIdAndFriendId(@Param("isEphemeralChat") Integer isEphemeralChat, @Param("ephemeralChatTime") Integer ephemeralChatTime, @Param("userId") Long userId, @Param("friendId") Long friendId);

    List<Map<String, Object>> selectSecretList(@Param("userId") Long userId, @Param("name") String name);

    void deleteByUserIdAndFriendId(@Param("userId") Long userId, @Param("friendId") Long friendId);

    HashMap<String, Object> selectSingleFriend(@Param("userId") Long userId, @Param("friendId") Long friendId);

    int deleteByCondition(ImFriend record);

    int updateByUserIdAndFriendIdAgree(@Param("remark") String remark, @Param("isSecretFriend") Integer isSecretFriend, @Param("userId") Long userId, @Param("friendId") Long friendId);

    List<Map<String, Object>> getFriendInfo(Map<String, Object> map);

    List<Map<String, Object>> getSessionList(Map<String, Object> map);

    List<Map<String, Object>> getBlackList(Map<String, Object> map);

    ImFriend selectIsFriendByUserIdAndFriendId(@Param("userId") Long userId, @Param("friendId") Long friendId);

    Set<Long> selectFriendsSet(long userId);

    int countFriends(long userId);
}