package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.WebDictionaryDetail;
import com.wxzd.im.ms.mapper.WebDictionaryDetailMapper;
import com.wxzd.im.ms.service.WebDictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebDictionaryDetailServiceImpl implements WebDictionaryDetailService {
    @Autowired
    private WebDictionaryDetailMapper webDictionaryDetailMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return webDictionaryDetailMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(WebDictionaryDetail record) {
        return webDictionaryDetailMapper.insert(record);
    }

    @Override
    public int insertSelective(WebDictionaryDetail record) {
        return webDictionaryDetailMapper.insertSelective(record);
    }

    @Override
    public WebDictionaryDetail selectByPrimaryKey(Integer id) {
        return webDictionaryDetailMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(WebDictionaryDetail record) {
        return webDictionaryDetailMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(WebDictionaryDetail record) {
        return webDictionaryDetailMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<WebDictionaryDetail> selectByDictionaryId(Integer dictionaryId) {
        return webDictionaryDetailMapper.selectByDictionaryId(dictionaryId);
    }
}
