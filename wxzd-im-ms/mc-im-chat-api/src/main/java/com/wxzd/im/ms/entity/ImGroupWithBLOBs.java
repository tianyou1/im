package com.wxzd.im.ms.entity;

public class ImGroupWithBLOBs extends ImGroup {
    private String headUrl;

    private String bulletinContent;

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl == null ? null : headUrl.trim();
    }

    public String getBulletinContent() {
        return bulletinContent;
    }

    public void setBulletinContent(String bulletinContent) {
        this.bulletinContent = bulletinContent == null ? null : bulletinContent.trim();
    }
}