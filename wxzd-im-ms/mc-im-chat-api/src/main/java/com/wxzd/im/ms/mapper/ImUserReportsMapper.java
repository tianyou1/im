package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImUserReports;
import com.wxzd.im.ms.entity.ImUserReportsWithBLOBs;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImUserReportsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserReportsWithBLOBs record);

    int insertSelective(ImUserReportsWithBLOBs record);

    ImUserReportsWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserReportsWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImUserReportsWithBLOBs record);

    int updateByPrimaryKey(ImUserReports record);
}