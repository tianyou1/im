package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImUser;
import com.wxzd.im.ms.entity.ImUserWithBLOBs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ImUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserWithBLOBs record);

    int insertSelective(ImUserWithBLOBs record);

    ImUserWithBLOBs selectByPrimaryKey(Long id);

    ImUserWithBLOBs selectByAccount(String account);

    int countByAccount(String account);

    List<Map<String, Object>> selectAllUser();

    int updateByPrimaryKeySelective(ImUserWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImUserWithBLOBs record);

    int updateByPrimaryKey(ImUser record);

    List<Map<String, Object>> queryGroupManagers(Map<String, Object> map);

    List<Map<String, Object>> queryGroupMembers(Map<String, Object> map);

    List<Map<String, Object>> friendDetail(@Param("userId") long userId, @Param("friendId") long friendId);

    Map<String, Object> getImFriendInfo(@Param("userId") long userId, @Param("friendId") long friendId);
}