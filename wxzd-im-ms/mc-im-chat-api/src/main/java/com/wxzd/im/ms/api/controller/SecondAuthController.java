package com.wxzd.im.ms.api.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.zxing.WriterException;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.ImMessage;
import com.wxzd.im.ms.entity.ImUserSecondaryVerification;
import com.wxzd.im.ms.entity.ImUserWithBLOBs;
import com.wxzd.im.ms.enums.TokenCheckEnum;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.parameter.Constants;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

import static com.wxzd.im.ms.parameter.ReturnConstant.secCheckErrorReturn;
import static com.wxzd.im.ms.parameter.ReturnConstant.secCheckPwdErrorReturn;
import static com.wxzd.im.ms.parameter.StaticConstant.*;

@RestController
@Api(value = "二次验证接口")
@RequestMapping(value = "/second", method = {RequestMethod.POST})
public class SecondAuthController {

    @Autowired
    private ImUserSecondaryVerificationService secondService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ImUserService userService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private MessageFactory msgFactory;
    @Autowired
    private ChatServerFeign chatServerFeign;

    private final static Table<String, String, Integer> userRateMap = HashBasedTable.create();

    /**
     * 获取秘钥
     *
     * @return
     */
    @ApiOperation("获取秘钥")
    @RequestMapping("/findGoogleSecret")
    @ResponseBody
    public ResponseData findGoogleSecret(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        try {
            ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
            String secretKey = GoogleAuthenticator.generateSecretKey();
            JSONObject json = new JSONObject();
            json.put("qcode", QrCodeCreateUtil.createQrCode("otpauth://totp/XChat%20" + imUser.getAccount() + "?secret=" + secretKey));
            json.put("secretKey", secretKey);
            return YxUtil.createSimpleSuccess(json);
        } catch (IOException | WriterException e) {
            return YxUtil.createSimpleSuccess("生成失败");
        }
    }


    /**
     * 获取手机验证
     */
    @ApiOperation("获取手机验证")
    @RequestMapping("/sendMobileCheckInfo")
    @ResponseBody
    public ResponseData sendMobileCheckInfo(HttpServletRequest request, @RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);
        //手机号
        return mobileResult(request, secondaryVerification);
    }


    /**
     * 获取邮箱验证
     */
    @ApiOperation("获取邮箱验证")
    @RequestMapping("/sendEmailCheckInfo")
    @ResponseBody
    public ResponseData sendEmailCheckInfo(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);
        //邮箱
        return emailResult(secondaryVerification);
    }


    /**
     * 获取忘记密码需要的手机验证
     */
    @ApiOperation("忘记密码获取手机验证")
    @RequestMapping("/sendForgetMobileCheckInfo")
    @ResponseBody
    public ResponseData sendForgetMobileCheckInfo(HttpServletRequest request, @ApiParam("账号") @RequestParam String account, @ApiParam("手机号") @RequestParam String mobile) {
        if (StringUtils.isEmpty(account)) {
            return YxUtil.createFail("账号为空");
        }
        ImUserWithBLOBs imUser = userService.selectByAccount(account);
        if (imUser == null) {
            return YxUtil.createFail("账号不存在");
        }
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(imUser.getId());
        if (!mobile.equals(secondaryVerification.getMobile().substring(secondaryVerification.getMobile().length() - 4))) {
            return YxUtil.createFail("手机号错误");
        }
        //手机号
        return mobileResult(request, secondaryVerification);
    }

    /**
     * 忘记密码获取手机验证
     */
    @ApiOperation("忘记密码获取手机验证")
    @RequestMapping("/sendForgetEmailCheckInfo")
    @ResponseBody
    public ResponseData sendForgetEmailCheckInfo(@ApiParam("账号") @RequestParam String account) {
        if (StringUtils.isEmpty(account)) {
            return YxUtil.createFail("账号为空");
        }
        ImUserWithBLOBs imUser = userService.selectByAccount(account);
        if (imUser == null) {
            return YxUtil.createFail("账号不存在");
        }
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(imUser.getId());
        //邮箱
        return emailResult(secondaryVerification);
    }

    private ResponseData mobileResult(HttpServletRequest request, ImUserSecondaryVerification secondaryVerification) {
        String key = LoginService.getClientIP(request) + "_/second/getValidateNum";
        String dd = DateUtil.today();
        StaticLog.info("请求的日期=" + dd);
        StaticLog.info("请求的ip=" + key);
        Integer preTime = userRateMap.get(key, dd);
        if (preTime != null && preTime > 100) {
            StaticLog.info(key + "_" + secondaryVerification.getMobile() + ",发短信太频繁！！！");
            return YxUtil.createFail("欠费");
        }
        if (preTime != null) {
            userRateMap.put(key, dd, preTime + 1);
        } else {
            userRateMap.put(key, dd, 1);
        }

        String code = Utils.randomInt(6);
        RedisUtil.set((Constants.SMS_KEY + secondaryVerification.getMobile()).getBytes(), code.getBytes());
        RedisUtil.expired((Constants.SMS_KEY + secondaryVerification.getMobile()).getBytes(), 600);
        smsService.sendSmsRY(secondaryVerification.getMobile(), code);
        return YxUtil.createSuccessDataNoFilter("发送成功");
    }

    private ResponseData emailResult(ImUserSecondaryVerification secondaryVerification) {
        String code = Utils.randomInt(6);
        RedisUtil.set((Constants.SMS_EMAIL + secondaryVerification.getEmail()).getBytes(), code.getBytes());
        RedisUtil.expired((Constants.SMS_EMAIL + secondaryVerification.getEmail()).getBytes(), 600);
        emailService.sendEmail(secondaryVerification.getEmail(), code);
        return YxUtil.createSuccessDataNoFilter("发送成功");
    }


    /**
     * 二次验证接口
     *
     * @param account
     * @param checkInfo
     * @param devType
     * @return
     */
    @ApiOperation("二次验证接口")
    @RequestMapping("/secondCheck")
    @ResponseBody
    public ResponseData secondCheck(@ApiParam("账号") @RequestParam String account,
                                    @ApiParam("验证信息：手机验证码/邮箱验证码/谷歌秘钥验证/二次密码验证") @RequestParam String checkInfo,
                                    @ApiParam("设备类型：登录设备类型，1安卓2苹果3pc") @RequestParam int devType) {
        if (StringUtils.isEmpty(checkInfo)) {
            return YxUtil.createFail("请输入验证信息");
        }

        HashMap<String, Object> data = new HashMap<>();
        if (StringUtils.isEmpty(account)) {
            return YxUtil.createFail("账号为空");
        }
        ImUserWithBLOBs imUser = userService.selectByAccount(account);
        if (imUser == null) {
            return YxUtil.createFail("账号不存在");
        }
        long userId = imUser.getId();
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);
        if (secondaryVerification == null) {
            return YxUtil.createFail("二次验证信息不存在，请重新登录");
        }

        //手机号验证
        if (typePhone.equals(secondaryVerification.getVerifyType())) {
            boolean b = smsService.equalValidate(secondaryVerification.getMobile(), checkInfo);
            return secondResult(userId, devType, b, data, secCheckErrorReturn);
        }

        //邮箱验证
        if (typeEmail.equals(secondaryVerification.getVerifyType())) {
            boolean b = emailService.equalValidate(secondaryVerification.getEmail(), checkInfo);
            return secondResult(userId, devType, b, data, secCheckErrorReturn);
        }
        //谷歌验证
        if (typeGoogle.equals(secondaryVerification.getVerifyType())) {
            boolean b = GoogleAuthenticator.authcode(checkInfo, secondaryVerification.getGoogleKey());
            return secondResult(userId, devType, b, data, secCheckErrorReturn);
        }
        //二级密码验证
        if (typeSecond.equals(secondaryVerification.getVerifyType())) {
            boolean b = secondaryVerification.getSecondaryPwd().equals(Utils.encrypt(checkInfo));
            return secondResult(userId, devType, b, data, secCheckPwdErrorReturn);
        }
        return YxUtil.createSimpleSuccess(data);
    }

    //二次验证返回
    private ResponseData secondResult(long userId, int device, boolean b, HashMap<String, Object> data, String errorMsg) {
        if (b) {
            String token = TokenUtil.getUserToken(userId, device);
            String tokenTemp = TokenUtil.getUserTokenTemp(userId, device);
            if (StringUtils.isNotBlank(tokenTemp)) {
                //发送通知上线
                ImMessage msg = msgFactory.userOnlineNotice(userId, userId, device);
                chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
                if (token != null) {
                    //如果当前设备在线 发送下线通知
                    ImMessage imMessage = msgFactory.userOfflineNotice(userId, userId, device);
                    chatServerFeign.notifyNew(JSONObject.toJSONString(imMessage), userId);
                }
                token = TokenUtil.generateUserToken(userId, device, TokenEnum.tokenValid.getCode());
                //更新登录的设备
                commonService.setOnline(userId, device);
                data.put("token", token);
                TokenUtil.deleteUserTokenTemp(userId, device);
                return YxUtil.createSimpleSuccess(data);
            }

            if (StringUtils.isNotEmpty(token)) {
                String isSecret = CommonUtils.getSecretByToken(token);
                //如果是真实token或者私密的token，按原来的返回
                if (String.valueOf(TokenEnum.tokenValid.getCode()).equals(isSecret) || String.valueOf(TokenEnum.tokenValidSecret.getCode()).equals(isSecret)) {
                    data.put("token", token);
                    TokenUtil.generateUserTokenCheck(token, TokenCheckEnum.tempToken.getCode());
                    return YxUtil.createSimpleSuccess(data);
                } else {
                    token = TokenUtil.generateUserToken(userId, device, TokenEnum.tokenValid.getCode());
                    data.put("token", token);
                    return YxUtil.createSimpleSuccess(data);
                }
            } else {
                token = TokenUtil.generateUserToken(userId, device, TokenEnum.tokenValid.getCode());
                data.put("token", token);
                TokenUtil.generateUserTokenCheck(token, TokenCheckEnum.tempToken.getCode());
                return YxUtil.createSimpleSuccess(data);
            }

        } else {
            return YxUtil.createFail(errorMsg);
        }
    }

    public static void main(String[] args) {
        System.out.println(RSACipherUtil.encrypt("a1234567"));
    }

    /**
     * 查找二级信息根据账号
     *
     * @param account
     * @return
     */
    @ApiOperation("查找二级信息根据账号")
    @RequestMapping("/findSecondCheckInfo")
    @ResponseBody
    public ResponseData findSecondCheckInfo(@ApiParam("用户账号") @RequestParam String account) {
        if (StringUtils.isEmpty(account)) {
            return YxUtil.createFail("账号为空");
        }
        HashMap<String, Object> data = new HashMap<>();
        ImUserWithBLOBs imUser = userService.selectByAccount(account);
        if (imUser == null) {
            return YxUtil.createFail("账号不存在");
        }
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(imUser.getId());

        if (secondaryVerification == null) {
            return YxUtil.createFail("未设置二次验证");
        }
        //手机号
        if (typePhone.equals(secondaryVerification.getVerifyType())) {
            data.put("secondInfo", TuoMUtils.mobileTuo(secondaryVerification.getMobile()));
            data.put("verifyType", secondaryVerification.getVerifyType());
            return YxUtil.createSimpleSuccess(data);
        }

        //邮箱
        if (typeEmail.equals(secondaryVerification.getVerifyType())) {
            data.put("secondInfo", TuoMUtils.email(secondaryVerification.getEmail()));
            data.put("verifyType", secondaryVerification.getVerifyType());
            return YxUtil.createSimpleSuccess(data);
        }
        //谷歌
        if (typeGoogle.equals(secondaryVerification.getVerifyType())) {
            data.put("secondInfo", TuoMUtils.bankCard(secondaryVerification.getGoogleKey()));
            data.put("verifyType", secondaryVerification.getVerifyType());
            return YxUtil.createSimpleSuccess(data);
        }
        //二级密码
        if (typeSecond.equals(secondaryVerification.getVerifyType())) {
            data.put("secondInfo", "");
            data.put("verifyType", secondaryVerification.getVerifyType());
            return YxUtil.createSimpleSuccess(data);
        }
        return YxUtil.createSimpleSuccess(data);
    }

    @ApiOperation("获取验证码接口")
    @RequestMapping("/getValidateNum")
    @ResponseBody
    public ResponseData getValidateNum(HttpServletRequest request,
                                       @ApiParam("手机号") @RequestParam String mobile) {
        String key = LoginService.getClientIP(request) + "_/second/getValidateNum";
        String dd = DateUtil.today();
        StaticLog.info("请求的日期=" + dd);
        StaticLog.info("请求的ip=" + key);
        Integer preTime = userRateMap.get(key, dd);
        if (preTime != null && preTime > 100) {
            StaticLog.info(key + "_" + mobile + ",发短信太频繁！！！");
            return YxUtil.createFail("欠费");
        }
        if (preTime != null) {
            userRateMap.put(key, dd, preTime + 1);
        } else {
            userRateMap.put(key, dd, 1);
        }

        String code = Utils.randomInt(6);
        RedisUtil.set((Constants.SMS_KEY + mobile).getBytes(), code.getBytes());
        RedisUtil.expired((Constants.SMS_KEY + mobile).getBytes(), 600);
        smsService.sendSmsRY(mobile, code);
        return YxUtil.createSuccessDataNoFilter("发送成功");
    }

    @ApiOperation("获取邮箱验证码接口")
    @RequestMapping("/getEmailValidateNum")
    @ResponseBody
    public ResponseData getEmailValidateNum(@ApiParam("邮箱地址") @RequestParam String emailAddress) {

        String code = Utils.randomInt(6);
        RedisUtil.set((Constants.SMS_EMAIL + emailAddress).getBytes(), code.getBytes());
        RedisUtil.expired((Constants.SMS_EMAIL + emailAddress).getBytes(), 600);
        emailService.sendEmail(emailAddress, code);
        return YxUtil.createSuccessDataNoFilter("发送成功");
    }


}
