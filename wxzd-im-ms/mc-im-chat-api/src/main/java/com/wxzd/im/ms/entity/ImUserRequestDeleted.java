package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ImUserRequestDeleted {
    private Long id;

    private Long requestId;

    private Long userId;

    private Long createTime;
}