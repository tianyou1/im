package com.wxzd.im.ms.enums;

/**
 * @author cx
 * @ClassName: TokenEnum
 * @Description: token枚举类
 * @date 2020.08.13
 */
public enum TokenEnum {
    tokenAndroidDevice(1, "安卓设备"),
    tokenIOSDevice(2, "苹果设备"),
    tokenPcDevice(3, "PC设备"),

    tokenTemporary(0, "临时token"),
    tokenValid(1, "有效token"),
    tokenValidSecret(2, "私密token"),
    cancelToken(3, "账号注销token"),
    msgHistoryToken(4, "聊天记录密码token");


    private int code;
    private String name;

    private TokenEnum(int code, String name) {
        this.code = code;
        this.name = name();
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }


}
