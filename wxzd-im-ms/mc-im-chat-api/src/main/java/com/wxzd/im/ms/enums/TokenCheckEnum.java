package com.wxzd.im.ms.enums;

public enum TokenCheckEnum {
    tempToken("0", "临时操作的验证token");


    private String code;
    private String name;

    private TokenCheckEnum(String code, String name) {
        this.code = code;
        this.name = name();
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }


}
