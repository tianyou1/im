package com.wxzd.im.ms.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * 二维码生成
 *
 * @version 1.0
 */
public class QrCodeCreateUtil {

    /**
     * 生成包含字符串信息的二维码图片
     *
     * @param content 二维码携带信息
     * @throws WriterException
     * @throws IOException
     */
    public static String createQrCode(String content) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Map hints = new HashMap();
        hints.put(EncodeHintType.MARGIN, 0);  //设置白边
        BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, 600, 600, hints);


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", outputStream);

        Base64.Encoder encoder = Base64.getEncoder();

        String text = encoder.encodeToString(outputStream.toByteArray());

        return text;
    }

    /**
     * 生成包含字符串信息的二维码图片
     *
     * @param content 二维码携带信息
     * @throws WriterException
     * @throws IOException
     */
    public static String createQrCodeSetSize(String content, int width, int height) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, width, height);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", outputStream);

        Base64.Encoder encoder = Base64.getEncoder();

        String text = encoder.encodeToString(outputStream.toByteArray());

        return text;
    }

    /**
     * 读二维码并输出携带的信息
     */
//    public static void readQrCode(InputStream inputStream) throws IOException{
//              //从输入流中获取字符串信息
//               BufferedImage image = ImageIO.read(inputStream);
//            //将图像转换为二进制位图源
//           LuminanceSource source = new BufferedImageLuminanceSource(image);
//          BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//             QRCodeReader reader = new QRCodeReader();
//            Result result = null ;
//            try {
//                  result = reader.decode(bitmap);
//                } catch (ReaderException e) {
//                     e.printStackTrace();
//                   }
//               System.out.println(result.getText());
//           }


}
