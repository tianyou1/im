package com.wxzd.im.ms.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    public static String format_sample = "yyyy-MM-dd HH:mm:ss";
    public static SimpleDateFormat format_standard = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public static SimpleDateFormat format_simple = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public DateUtil() {
    }

    public static String getTimeString(long time) {
        return time == 0L ? "" : format_standard.format(new Date(time));
    }

    public static String getTimeString(long time, String format) {
        if (time == 0L) {
            return "";
        } else {
            SimpleDateFormat aformat_standard = new SimpleDateFormat(format, Locale.getDefault());
            return aformat_standard.format(new Date(time));
        }
    }

    public static String getEnglishTimeString(long time, String format) {
        if (time == 0L) {
            return "";
        } else {
            SimpleDateFormat aformat_standard = new SimpleDateFormat(format, Locale.ENGLISH);
            return aformat_standard.format(new Date(time));
        }
    }

    public static String getTimeString(Date time) {
        return time == null ? "" : format_standard.format(time);
    }

    public static Date getDate(String time, String format) {
        if (time != null && time.length() >= 8) {
            try {
                SimpleDateFormat sd = new SimpleDateFormat(format, Locale.getDefault());
                return sd.parse(time);
            } catch (ParseException var3) {
                var3.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public static String getTimeString(Date time, String format) {
        SimpleDateFormat format_simple = new SimpleDateFormat(format, Locale.getDefault());
        return format_simple.format(time);
    }

    public static String getTimeString(Date time, String format, String format1, boolean maybeMissedMonthDay) {
        if (time == null) {
            return "";
        } else if (!maybeMissedMonthDay) {
            SimpleDateFormat format_simple = new SimpleDateFormat(format, Locale.getDefault());
            return format_simple.format(time);
        } else {
            String res = getTimeString(time);
            if (res == null) {
                return null;
            } else if (res.endsWith(" 23:23:59")) {
                return res.split("-")[0];
            } else {
                SimpleDateFormat format_simple;
                if (res.endsWith(" 00:23:59")) {
                    format_simple = new SimpleDateFormat(format1, Locale.getDefault());
                    return format_simple.format(time);
                } else {
                    format_simple = new SimpleDateFormat(format, Locale.getDefault());
                    return format_simple.format(time);
                }
            }
        }
    }

    public static boolean[] hasMonthDay(Date time) {
        if (time == null) {
            return new boolean[]{false, false};
        } else {
            try {
                SimpleDateFormat sd = new SimpleDateFormat(format_sample, Locale.getDefault());
                String date = sd.format(time);
                if (date.endsWith("23:23:59")) {
                    return new boolean[]{false, false};
                } else {
                    return date.endsWith("00:23:59") ? new boolean[]{true, false} : new boolean[]{false, false};
                }
            } catch (Exception var3) {
                var3.printStackTrace();
                return new boolean[]{false, false};
            }
        }
    }

    public static Date getDate(String year, String month, String day) {
        month = monthdayFormat(month);
        day = monthdayFormat(day);
        if (year != null && year.length() != 0) {
            String time = null;
            boolean hasMonth = false;
            boolean hasDay = false;
            if (month != null && month.length() != 0) {
                hasMonth = true;
            }

            if (month != null && month.length() != 0 && day != null && day.length() != 0) {
                hasDay = true;
            }

            if (hasMonth && hasDay) {
                time = year + "-" + month + "-" + day + " 00:00:00";
            } else if (hasMonth && !hasDay) {
                time = year + "-" + month + "-01 00:23:59";
            } else if (!hasMonth && !hasDay || !hasMonth && hasDay) {
                time = year + "-01-01 23:23:59";
            }

            try {
                SimpleDateFormat sd = new SimpleDateFormat(format_sample, Locale.getDefault());
                return sd.parse(time);
            } catch (ParseException var7) {
                var7.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    private static String monthdayFormat(String monthorday) {
        if (monthorday != null && monthorday.length() == 1) {
            monthorday = "0" + monthorday;
        }

        return monthorday;
    }

    public static int compareTime(Date date, String time) {
        String a = getTimeString(date, "HH");
        String b = getTimeString(date, "MM");
        String str = a + ":" + b;
        System.out.println(str);
        return str.compareTo(time);
    }

    public static int getYmd(Date date, String cast) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cast.equals("year") ? cal.get(1) : cal.get(3);
    }

    public static String getYesDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(5, -1);
        String yesterday = (new SimpleDateFormat("yyyy-MM-dd ")).format(cal.getTime());
        return yesterday;
    }

    public static int getMonthDay() {
        Calendar cal = Calendar.getInstance();
        int day = cal.getActualMaximum(5);
        return day;
    }

    public static void main(String[] arg) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(1);
        int month = cal.get(2) + 1;
        int dayOfMonth = dayOfMonth();
        Calendar c = Calendar.getInstance();
        String m = getYmd(new Date(), "year") + "";
        String y = getYmd(new Date(), "month") + "";
        String s111 = String.format("%tm", new Date());
        System.out.println(String.format("%tY", new Date()));
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        for (int i = 1; i < dayOfMonth; ++i) {
            c.add(5, -1);
            System.out.println(df.format(c.getTime()));
        }

        NumberFormat formatter = new DecimalFormat("0.00");
        long a = 13L;
        Double s = Double.parseDouble(a + "");
        int b = 3;
        Double s1 = Double.parseDouble(b + "");
        boolean c1 = true;
        System.out.println(formatter.format(s1 / s));
        Integer kqTag = null;
        if (kqTag == null) {
            System.out.println("3333333333333333333333");
        }

        if (kqTag == null) {
            kqTag = 999;
        }

        switch (kqTag) {
            case 1:
                System.out.println("11111111111111111111111");
                break;
            default:
                System.out.println("2222222222222222");
        }

        Date date = new Date(1431080917741L);
        System.out.println(date);
        String month1 = String.format("%tm", new Date());
        System.out.println("mot-----------" + month1);
    }

    private static Date getFirstDay() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(2, 0);
        c.set(5, 1);
        String first = df.format(c.getTime());
        System.out.println("===============first:" + first);
        return c.getTime();
    }

    public static int dayOfMonth() {
        Calendar c = Calendar.getInstance();
        return c.get(5);
    }
}
