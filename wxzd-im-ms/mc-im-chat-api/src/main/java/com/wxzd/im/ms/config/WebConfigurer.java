package com.wxzd.im.ms.config;

import com.wxzd.im.ms.intercepter.AllInterceptor;
import com.wxzd.im.ms.intercepter.TokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 支持跨越
        registry.addMapping("/**")
                .allowedMethods("*")
                .allowedOrigins("*")
                .allowedHeaders("*");
        WebMvcConfigurer.super.addCorsMappings(registry);
    }

    // 这个方法用来注册拦截器，我们自己写好的拦截器需要通过这里添加注册才能生效
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns("/**") 表示拦截所有的请求，
        // excludePathPatterns("/login", "/register") 表示除了登陆与注册之外，因为登陆注册不需要登陆也可以访问

        //注册token拦截器
        InterceptorRegistration registration = registry.addInterceptor(new TokenInterceptor());
        registration.addPathPatterns("/**");
//        registration.excludePathPatterns("/images/**", "/css/**","/js/**","/upload/**",
//                "/report/**","/login/getLoginToken","/login/getLoginCode","/login/doLogin",
//                "/login/register","/login/updatePwd","/second/**","/group/getAnonymityName",
//                "/exception");


        registration.excludePathPatterns("/images/**", "/css/**", "/js/**", "/upload/**",
                "/report/**", "/login/getLoginToken", "/login/getLoginCode", "/chat/doLogin",
                "/chat/register", "/chat/updatePwd", "/second/**", "/group/getAnonymityName",
                "/exception");

        //去掉swagger-ui资源的拦截
        registration.excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");
        //注册all拦截器
        InterceptorRegistration registration2 = registry.addInterceptor(new AllInterceptor());
        registration2.addPathPatterns("/**");
        registration2.excludePathPatterns("/images/**", "/css/**", "/js/**", "/upload/**",
                "/report/**");
    }


}