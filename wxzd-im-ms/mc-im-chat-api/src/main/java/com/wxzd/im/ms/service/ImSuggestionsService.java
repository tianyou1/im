package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImSuggestions;

public interface ImSuggestionsService {
    int deleteByPrimaryKey(Long id);

    int insert(ImSuggestions record);

    int insertSelective(ImSuggestions record);

    ImSuggestions selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImSuggestions record);

    int updateByPrimaryKeyWithBLOBs(ImSuggestions record);

    int updateByPrimaryKey(ImSuggestions record);
}