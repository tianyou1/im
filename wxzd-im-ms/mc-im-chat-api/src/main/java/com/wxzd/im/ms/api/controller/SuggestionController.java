package com.wxzd.im.ms.api.controller;

import com.wxzd.im.ms.entity.ImSuggestions;
import com.wxzd.im.ms.entity.ImUserWithBLOBs;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.ImSuggestionsService;
import com.wxzd.im.ms.service.ImUserService;
import com.wxzd.im.ms.utils.CheckUtil;
import com.wxzd.im.ms.utils.CommonUtils;
import com.wxzd.im.ms.utils.YxUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.wxzd.im.ms.parameter.ReturnConstant.maxSuggestionNumReturn;
import static com.wxzd.im.ms.parameter.ReturnConstant.operationSuccessReturn;
import static com.wxzd.im.ms.parameter.StaticConstant.unHandle;
import static com.wxzd.im.ms.utils.SnowflakeIdWorkerUtil.snowflake;

@RestController
@Api(value = "大部分意见反馈HTTP接口")
@RequestMapping(value = "/suggestion", method = RequestMethod.POST)
public class SuggestionController {


    @Autowired
    private ImUserService userService;

    @Autowired
    private ImSuggestionsService suggestionService;

    /**
     * 意见反馈接口
     *
     * @param token
     * @return
     */
    @ApiOperation("意见反馈接口")
    @RequestMapping("/suggestion")
    @ResponseBody
    public ResponseData suggestion(@RequestHeader String token,
                                   @ApiParam("联系手机（可选）") @RequestParam(required = false) String mobile,
                                   @ApiParam("联系邮箱（可选）") @RequestParam(required = false) String email,
                                   @ApiParam("反馈内容") @RequestParam String content) {
        if (!CheckUtil.checkSuggestion(content)) {
            return YxUtil.createFail(maxSuggestionNumReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        ImSuggestions imSuggestions = new ImSuggestions();
        imSuggestions.setCreateTime(System.currentTimeMillis());
        imSuggestions.setUserMobile(mobile);
        imSuggestions.setUserEmail(email);
        imSuggestions.setFeedbackContent(content);
        imSuggestions.setIsProcessed(unHandle);
        imSuggestions.setUserName(imUser.getNickName());
        imSuggestions.setUserId(userId);
        imSuggestions.setId(snowflake.nextId());
        suggestionService.insertSelective(imSuggestions);
        return YxUtil.createSimpleSuccess(operationSuccessReturn);
    }
}
