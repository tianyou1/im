package com.wxzd.im.ms.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;

/**
 * 雪花算法工具类
 */
public class SnowflakeIdWorkerUtil {

    public static final Snowflake snowflake = IdUtil.createSnowflake(RandomUtil.randomLong(0, 31), RandomUtil.randomLong(0, 31));

    public static final Snowflake snowflaketime = new Snowflake(RandomUtil.randomLong(0, 31), RandomUtil.randomLong(0, 31), true);

    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
        for (int i = 0; i < 10000; i++) {

            new Thread(() -> {
                System.out.println(snowflake.nextId());
            }).start();
        }


    }

}
