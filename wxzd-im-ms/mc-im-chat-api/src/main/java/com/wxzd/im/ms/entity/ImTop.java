package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ImTop {
    private Long id;

    private Long userId;

    private Integer destType;

    private Long destId;

}