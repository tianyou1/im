package com.wxzd.im.ms.api.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取一些通用的配置
 */
@ConfigurationProperties(prefix = "file-upload-config")
@Component
public class FileUploadConfigProperties {
    private String fileSystemServer;

    private Integer headFileSize;

    private Integer reportFileSize;

    private Integer chatFileSize;

    public String getFileSystemServer() {
        return fileSystemServer;
    }

    public void setFileSystemServer(String fileSystemServer) {
        this.fileSystemServer = fileSystemServer;
    }

    public Integer getHeadFileSize() {
        return headFileSize;
    }

    public void setHeadFileSize(Integer headFileSize) {
        this.headFileSize = headFileSize;
    }

    public Integer getReportFileSize() {
        return reportFileSize;
    }

    public void setReportFileSize(Integer reportFileSize) {
        this.reportFileSize = reportFileSize;
    }

    public Integer getChatFileSize() {
        return chatFileSize;
    }

    public void setChatFileSize(Integer chatFileSize) {
        this.chatFileSize = chatFileSize;
    }
}
