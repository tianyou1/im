package com.wxzd.im.ms.schedule;

import com.wxzd.im.ms.entity.ImUserWithBLOBs;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.parameter.Constants;
import com.wxzd.im.ms.service.ImMessageHistoryService;
import com.wxzd.im.ms.service.ImUserService;
import com.wxzd.im.ms.utils.DateUtil;
import com.wxzd.im.ms.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.wxzd.im.ms.parameter.StaticConstant.*;

/**
 * 阅后即焚消息保存3个月删除/活动账号销毁判断
 */
@Component
public class UserReadDeleteHistoryService {

    @Autowired
    private ImUserService userService;
    @Autowired
    private ImMessageHistoryService messageHistoryService;


    //每月1号凌晨1点执行
//  @Scheduled(fixedRate = 60000)
    @Scheduled(cron = "0 0 1 1 * ?")
    public void useAutoCancel() {
        List<Map<String, Object>> userList = userService.selectAllUser();
        userList.forEach(e -> {
            if (vaildUser == Integer.parseInt(e.get("status").toString())) {
                if (autoCancelYes == Integer.parseInt(e.get("is_auto_cancel").toString())) {
                    List<Map<String, Object>> record = messageHistoryService.selectByBelongUserId((Long) e.get("id"));

                    long lastloginDate = 0;
                    long currentDate = System.currentTimeMillis();

                    if (record.isEmpty()) {
                        lastloginDate = Long.valueOf(DateUtil.getDate(e.get("last_login_time").toString(),"yyyy-MM-dd HH:mm:ss").getTime());
                    } else {
                        lastloginDate = Long.valueOf(record.get(record.size() - 1).get("sendTime").toString());

                        //删除阅后即焚保存3个月的数据
                        for (Map<String, Object> stringObjectMap : record) {
                            if (stringObjectMap.get("is_ephemeral_chat").equals(ephemeralChat)) {
                                long expireReadDeleteMonth = Long.valueOf(stringObjectMap.get("readTime").toString()) / 1000 + Long.valueOf(stringObjectMap.get("ephemeral_chat_time").toString());
                                long mothReadDeleteSize = 60 * 60 * 24 * 30L;
                                long readDeleteMonth = (currentDate / 1000 - expireReadDeleteMonth) / mothReadDeleteSize;
                                //删除阅后即焚数据
                                if (readDeleteMonth > 3) {
                                    messageHistoryService.deleteByPrimaryKey((Long) stringObjectMap.get("id"));
                                }
                            }
                        }
                    }

                    long expireTime = currentDate - lastloginDate;
                    long mothSize = 1000 * 60 * 60 * 24 * 30L;
                    long expireMonth = expireTime / mothSize;
                    //如果当前时间减去设置的销毁时间大于0
                    if (expireMonth - Integer.parseInt(e.get("auto_cancel_months").toString()) > 0) {
                        //账号改为失效
                        ImUserWithBLOBs user = userService.selectByPrimaryKey((Long) e.get("id"));
                        user.setStatus(loseUser);
                        int count = userService.updateByPrimaryKeySelective(user);
                        if (count > 0) {
                            //更新用户缓存
                            userService.updateUserCache(user);
                            //清除token（真实或者私密的）
                            RedisUtil.hdel(Constants.USER_TOKEN, e.get("id") + "_" + TokenEnum.tokenValid.getCode());
                            RedisUtil.hdel(Constants.USER_TOKEN, e.get("id") + "_" + TokenEnum.tokenValidSecret.getCode());
                        }

                    }

                }


            }


        });

    }

}
