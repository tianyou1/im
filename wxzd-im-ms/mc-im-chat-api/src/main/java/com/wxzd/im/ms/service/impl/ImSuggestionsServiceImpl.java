package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImSuggestions;
import com.wxzd.im.ms.mapper.ImSuggestionsMapper;
import com.wxzd.im.ms.service.ImSuggestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImSuggestionsServiceImpl implements ImSuggestionsService {
    @Autowired
    private ImSuggestionsMapper imSuggestionsMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imSuggestionsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImSuggestions record) {
        return this.imSuggestionsMapper.insert(record);
    }

    @Override
    public int insertSelective(ImSuggestions record) {
        return this.imSuggestionsMapper.insertSelective(record);
    }

    @Override
    public ImSuggestions selectByPrimaryKey(Long id) {
        return this.imSuggestionsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImSuggestions record) {
        return this.imSuggestionsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImSuggestions record) {
        return this.imSuggestionsMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImSuggestions record) {
        return this.imSuggestionsMapper.updateByPrimaryKey(record);
    }
}
