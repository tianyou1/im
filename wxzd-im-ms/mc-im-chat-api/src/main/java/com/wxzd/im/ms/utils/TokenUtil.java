package com.wxzd.im.ms.utils;

import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.parameter.Constants;
import org.apache.commons.lang.time.DateUtils;

public class TokenUtil {


    /**
     * token生成
     *
     * @param userId
     * @param device
     * @param type
     * @return
     */
    public static String generateUserToken(Object userId, Object device, Object type) {
        String token = Utils.encrypt("" + System.currentTimeMillis()) + "|" + Utils.encrypt(String.valueOf(userId)) + "|" + Utils.encrypt(String.valueOf(device)) + "|" + Utils.encrypt(type.toString());
        RedisUtil.hset(Constants.USER_TOKEN, userId + "_" + device, token);
        return token;
    }

    /**
     * token Temp生成
     *
     * @param userId
     * @param device
     * @param type
     * @return
     */
    public static String generateUserTokenTemp(Object userId, Object device, Object type) {
        String token = Utils.encrypt("" + System.currentTimeMillis()) + "|" + Utils.encrypt(String.valueOf(userId)) + "|" + Utils.encrypt(String.valueOf(device)) + "|" + Utils.encrypt(type.toString());

        RedisUtil.set((Constants.USER_TOKEN_TEMP + userId + "_" + device).getBytes(), token.getBytes());
        RedisUtil.expired((Constants.USER_TOKEN_TEMP + userId + "_" + device).getBytes(), 300);
        return token;
    }

    /**
     * historyWand：聊天验证生成
     *
     * @param time
     * @param paramArr
     * @return
     */
    public static String generateHistoryWand(int time, String[] paramArr) {
        return Utils.encrypt(Utils.encrypt((System.currentTimeMillis() + time * 60 * 1000) + "") + "|" + paramArr[1] + "|" + paramArr[2] + "|" + Utils.encrypt(TokenEnum.msgHistoryToken.getCode() + ""));
    }

    /**
     * cancelWand：账号销毁生成
     *
     * @param userId
     * @param devType
     * @return
     */
    public static String generateCancelWand(long userId, int devType) {
        return Utils.encrypt(Utils.encrypt((System.currentTimeMillis() + 5 * DateUtils.MILLIS_PER_MINUTE * DateUtils.MILLIS_PER_SECOND) + "") + "|" + Utils.encrypt(userId + "") + "|" + Utils.encrypt(devType + "") + "|" + Utils.encrypt(TokenEnum.cancelToken.getCode() + ""));
    }

    /**
     * 获取token
     *
     * @param userId
     * @param device
     * @return
     */
    public static String getUserToken(Object userId, Object device) {
        return RedisUtil.hget(Constants.USER_TOKEN, userId + "_" + device);
    }

    /**
     * 获取token Temp
     *
     * @param userId
     * @param device
     * @return
     */
    public static String getUserTokenTemp(Object userId, Object device) {
        byte[] stringbytes = RedisUtil.get((Constants.USER_TOKEN_TEMP + userId + "_" + device).getBytes());
        if (stringbytes == null) {
            return "";
        } else {
            return new String(stringbytes);
        }
    }

    /**
     * 删除token Temp
     *
     * @param userId
     * @param device
     * @return
     */
    public static void deleteUserTokenTemp(Object userId, Object device) {
        RedisUtil.delete((Constants.USER_TOKEN_TEMP + userId + "_" + device).getBytes());
    }

    /**
     * 生成验证token
     *
     * @param token
     * @return
     */
    public static void generateUserTokenCheck(String token, String code) {
        RedisUtil.set((Constants.USER_TOKEN_CHECK + token).getBytes(), code.getBytes());
    }

    /**
     * 获取redis验证的值
     *
     * @param token
     * @return
     */
    public static String getUserTokenCheck(String token) {
        return getUserCheck(Constants.USER_TOKEN_CHECK, token);
    }


    /**
     * 设置code
     *
     * @param code
     * @param value
     */
    public static void setUserCode(String code, String value) {
        RedisUtil.set((Constants.USER_CODE + code).getBytes(), value.getBytes());
    }

    /**
     * 设置二维码过期
     *
     * @param code
     * @param value
     */
    public static void generateUserCodeCheck(String code, String value, int seconds) {
        RedisUtil.set((Constants.USER_CODE + code).getBytes(), value.getBytes());
        RedisUtil.expired((Constants.USER_CODE + code).getBytes(), seconds);
    }

    /**
     * 获取登录的code对应的值 根据对应的值生成新的token
     *
     * @param code
     */
    public static String getUserCodeCheck(String code) {
        return getUserCheck(Constants.USER_CODE, code);
    }

    private static String getUserCheck(String value, String type) {
        byte[] stringbytes = RedisUtil.get((value + type).getBytes());
        if (stringbytes == null) {
            return "";
        } else {
            return new String(stringbytes);
        }
    }

}
