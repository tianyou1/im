package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImUserRequestAudit;
import com.wxzd.im.ms.mapper.ImUserRequestAuditMapper;
import com.wxzd.im.ms.service.ImUserRequestAuditService;
import com.wxzd.im.ms.utils.YxUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ImUserRequestAuditServiceImpl implements ImUserRequestAuditService {

    @Autowired
    private ImUserRequestAuditMapper userRequestAuditMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.userRequestAuditMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.insert(record);
    }

    @Override
    public int insertSelective(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.insertSelective(record);
    }

    @Override
    public ImUserRequestAudit selectByPrimaryKey(Long id) {
        return this.userRequestAuditMapper.selectByPrimaryKey(id);
    }

    @Override
//    @Caching( evict = {
//            @CacheEvict(cacheNames = inviteReqListCache,key ="#record.userId",condition = "#record.destType==2  "),
//            @CacheEvict(cacheNames = confirmReqListCache,key ="#record.userId"),
//            @CacheEvict(cacheNames = countConfirmReqListCache,key ="#record.userId"),
//            @CacheEvict(cacheNames = countInviteReqListCache,key ="#record.userId"),
//
//            @CacheEvict(cacheNames = friendReqListCache,key ="#record.userId"),
//            @CacheEvict(cacheNames = countFriendReqListCache,key ="#record.userId"),
//    })
    public int updateByPrimaryKeySelective(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.updateByPrimaryKey(record);
    }

    /**
     * 根据条件查找
     *
     * @param record
     * @return
     */
    @Override
    public ImUserRequestAudit selectByCondition(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.selectByCondition(record);
    }

    /**
     * 根据条件查找
     *
     * @param record
     * @return
     */
    @Override
    public List<ImUserRequestAudit> selectByConditions(ImUserRequestAudit record) {
        return this.userRequestAuditMapper.selectByConditions(record);
    }

    /**
     * 判断是否发送过邀请
     *
     * @param map
     * @return
     */
    @Override
    public ImUserRequestAudit isSentMsg(Map<String, Object> map) {
        return this.userRequestAuditMapper.isSentMsg(map);
    }

    @Override
    public int updateByUserIdAndFriendId(Integer auditStatus, Long userId, Long time, Long friendId) {
        return userRequestAuditMapper.updateByUserIdAndFriendId(auditStatus, userId, time, friendId);
    }

    @Override
    public List<Map<String, Object>> unReadFriendReqCount(Long destId) {
        return this.userRequestAuditMapper.unReadFriendReqCount(destId);
    }

    @Override
    public List<Map<String, Object>> unReadConfirmReqCount(Long userId) {
        return this.userRequestAuditMapper.unReadConfirmReqCount(userId);
    }

    @Override
    public List<Map<String, Object>> unReadInviteReqCount(Long userId) {
        return this.userRequestAuditMapper.unReadInviteReqCount(userId);
    }

    @Override
    public int readRequest(String ids, long userId) {
        int res = 0;
        List<Long> idList = Arrays.asList(YxUtil.splitLong(ids));
        List<ImUserRequestAudit> userRequestAudits = userRequestAuditMapper.selectByIds(idList);
        for (ImUserRequestAudit userRequestAudit : userRequestAudits) {
            String val = "";
            String str = userRequestAudit.getIsRead();
            boolean result;
            //如果是第一次
            if (StringUtils.isBlank(str)) {
                val += userId;
            } else {
                String[] strings = str.split(",");
                result = Arrays.asList(strings).contains(String.valueOf(userId));
                if (!result) {
                    val = str + "," + userId;
                } else {
                    val = str + "";
                }
            }
            userRequestAudit.setIsRead(val);
        }
        Long[] requestIds = YxUtil.splitLong(ids);
        for (int i = 0; i < requestIds.length; i++) {
            ImUserRequestAudit o = new ImUserRequestAudit();
            o.setIsRead(userRequestAudits.get(i).getIsRead());
            o.setId(requestIds[i]);
            userRequestAuditMapper.updateByPrimaryKeySelective(o);
        }
        res++;
        return res;
    }

    @Override
//    @Caching( evict = {
//            @CacheEvict(cacheNames = friendReqListCache,key ="#userId"),
//
//    })
    public int deleteByUserIdAndFriendId(long userId, long destId) {
        return this.userRequestAuditMapper.deleteByUserIdAndFriendId(userId, destId);
    }

    @Override
    public int batchInsertSelective(@Param("list") List<ImUserRequestAudit> list) {
        return this.userRequestAuditMapper.batchInsertSelective(list);
    }
}
