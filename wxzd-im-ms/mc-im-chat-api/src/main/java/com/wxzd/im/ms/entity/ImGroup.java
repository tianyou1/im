package com.wxzd.im.ms.entity;

public class ImGroup {
    private Long id;

    private String name;

    private String descriptions;

    private String detail;

    private Long createrId;

    private Long createTime;

    private Long bulletinEditorId;

    private Long bulletinEditTime;

    private Integer isTop;

    private Long orgId;

    private Integer isBanned;

    private Integer isViewFriend;

    private Integer groupStatus;

    private Integer isInviteConfirm;

    private Integer isScreenshotTip;

    private Integer isEphemeralChat;

    private Integer ephemeralChatTime;

    private Integer isAnonymous;

    private Integer isFriendEachOther;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions == null ? null : descriptions.trim();
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getBulletinEditorId() {
        return bulletinEditorId;
    }

    public void setBulletinEditorId(Long bulletinEditorId) {
        this.bulletinEditorId = bulletinEditorId;
    }

    public Long getBulletinEditTime() {
        return bulletinEditTime;
    }

    public void setBulletinEditTime(Long bulletinEditTime) {
        this.bulletinEditTime = bulletinEditTime;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Integer getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Integer isBanned) {
        this.isBanned = isBanned;
    }

    public Integer getIsViewFriend() {
        return isViewFriend;
    }

    public void setIsViewFriend(Integer isViewFriend) {
        this.isViewFriend = isViewFriend;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }

    public Integer getIsInviteConfirm() {
        return isInviteConfirm;
    }

    public void setIsInviteConfirm(Integer isInviteConfirm) {
        this.isInviteConfirm = isInviteConfirm;
    }

    public Integer getIsScreenshotTip() {
        return isScreenshotTip;
    }

    public void setIsScreenshotTip(Integer isScreenshotTip) {
        this.isScreenshotTip = isScreenshotTip;
    }

    public Integer getIsEphemeralChat() {
        return isEphemeralChat;
    }

    public void setIsEphemeralChat(Integer isEphemeralChat) {
        this.isEphemeralChat = isEphemeralChat;
    }

    public Integer getEphemeralChatTime() {
        return ephemeralChatTime;
    }

    public void setEphemeralChatTime(Integer ephemeralChatTime) {
        this.ephemeralChatTime = ephemeralChatTime;
    }

    public Integer getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(Integer isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public Integer getIsFriendEachOther() {
        return isFriendEachOther;
    }

    public void setIsFriendEachOther(Integer isFriendEachOther) {
        this.isFriendEachOther = isFriendEachOther;
    }
}