package com.wxzd.im.ms.service;


import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.entity.ImMessage;
import com.wxzd.im.ms.entity.ImUserWithBLOBs;
import com.wxzd.im.ms.parameter.IMConstants;
import com.wxzd.im.ms.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.wxzd.im.ms.parameter.StaticConstant.p2PSingleSystemType;
import static com.wxzd.im.ms.parameter.StaticConstant.systemTypeSend;

@Service
public class MessageFactory {
    @Autowired
    private ImUserService userService;

    /**
     * 用户设置变更
     *
     * @param fromId  用户id
     * @param destId  目标id
     * @param devType 设备类型
     * @return ImMessage
     */
    public ImMessage userSetUpdateNotice(long fromId, long destId, int devType) {
        ImMessage msg = generateUserNotice(fromId, destId, devType, IMConstants.MSG_TYPE_USER_SET_UPDATE, "");
        msg.setSystemType(systemTypeSend);
        return msg;
    }

    /**
     * 修改了资料通知好友
     *
     * @param fromId 修改资料的人
     * @param destId 好友
     * @return ImMessage
     */
    public ImMessage userModifyProfileNotice(long fromId, long destId, ImUserWithBLOBs user) {
        JSONObject json = new JSONObject();
        json.put("headUrl", user.getHeadUrl());
        json.put("nickName", user.getNickName());
        json.put("city", user.getCity());
        json.put("province", user.getProvince());
        json.put("name", user.getName());
        json.put("sign", user.getSign());
        json.put("sex", user.getSex());
        json.put("district", user.getDistrict());
        ImMessage msg = generateUserNotice(fromId, destId, 0, IMConstants.MSG_TYPE_MODIFY_PROFILE, json.toJSONString());
        msg.setFromName(user.getNickName());
        msg.setImageIconUrl(user.getHeadUrl());
        msg.setSystemType(p2PSingleSystemType);
        return msg;
    }


    /**
     * 截图通知
     *
     * @param fromId  用户id
     * @param destId  目标id
     * @param devType 设备类型
     * @param content
     * @return ImMessage
     */
    public ImMessage screenShotNotice(long fromId, long destId, int devType, String content) {
        return generateUserNotice(fromId, destId, devType, IMConstants.MSG_TYPE_SCREEN_SHOT, content);
    }

    /**
     * 修改了资料通知好友
     *
     * @param fromId 修改资料的人
     * @param destId 好友
     * @return ImMessage
     */
    public ImMessage textMessage(long fromId, long destId, String text) {
        ImUserWithBLOBs user = userService.selectByPrimaryKey(fromId);
        ImMessage msg = generateUserNotice(fromId, destId, 0, IMConstants.MSG_TYPE_TEXT, "");
        msg.setFromName(user.getNickName());
        msg.setImageIconUrl(user.getHeadUrl());
        msg.setContent(text);
        msg.setFromType(IMConstants.MSG_FROM_P2P);
        return msg;
    }

    /**
     * 有人请求加好友
     *
     * @param fromId 请求人
     * @param destId 被请求人
     * @return ImMessage
     */
    public ImMessage friendReqNotice(long fromId, long destId) {
        ImMessage msg = generateUserInfo(fromId, destId, 0);
        msg.setMessageType(IMConstants.MSG_TYPE_FRIEND_REQ);
        return msg;
    }

    /**
     * 请求好友通过
     *
     * @param fromId 同意的人
     * @param destId 被同意的人
     * @return ImMessage
     */
    public ImMessage acceptFriendNotice(long fromId, long destId) {
        Map<String, Object> user = userService.getImFriendInfo(fromId, destId);
        ImMessage msg = new ImMessage();
        msg.setFromName((String) user.get("nickName"));
        msg.setImageIconUrl((String) user.get("headUrl"));
        JSONObject json = new JSONObject();
        json.put("name", user.get("nickName"));
        json.put("headUrl", user.get("headUrl"));
        json.put("sex", user.get("sex"));
        json.put("remark", user.get("remark"));
        msg.setContent(json.toJSONString());
        msg.setDestId(destId);
        msg.setFromId(fromId);
        msg.setFromType(IMConstants.MSG_FROM_SYS);
        msg.setMessageType(IMConstants.MSG_TYPE_ACCEPT_FRIEND);
        msg.setMsgId(Utils.getUUID());
        msg.setSendTime(System.currentTimeMillis());
        msg.setSystemType(p2PSingleSystemType);
        return msg;
    }


    //生成用户信息消息推送
    private ImMessage generateUserNotice(long fromId, long destId, int devType, int messageType, String content) {
        ImMessage msg = new ImMessage();
        msg.setDestId(destId);
        msg.setFromId(fromId);
        msg.setDevType(devType);
        msg.setFromType(IMConstants.MSG_FROM_SYS);
        msg.setMessageType(messageType);
        msg.setMsgId(Utils.getUUID());
        msg.setContent(content);
        msg.setSendTime(System.currentTimeMillis());
        return msg;
    }

    //生成好友信息
    private ImMessage generateUserInfo(long fromId, long destId, int devType) {
        JSONObject json = new JSONObject();
        return generateCommonInfo(fromId, destId, devType, json);
    }

    //生成好友和群邀请公共信息
    private ImMessage generateCommonInfo(long fromId, long destId, int devType, JSONObject json) {
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(fromId);
        json.put("name", imUser.getNickName());
        json.put("headUrl", imUser.getHeadUrl());
        ImMessage msg = generateUserNotice(fromId, destId, devType, 0, json.toJSONString());
        msg.setFromName(imUser.getNickName());
        msg.setImageIconUrl(imUser.getHeadUrl());
        return msg;
    }

    /**
     * 好友解除好友关系
     *
     * @param fromId 发送人id
     * @param destId 目标id
     * @return ImMessage
     */
    public ImMessage delFriendNotice(long fromId, long destId, int devType) {
        return generateUserNotice(fromId, destId, devType, IMConstants.MSG_TYPE_DEL_FRIEND, "");
    }

    /**
     * 好友上线通知
     *
     * @param fromId  发送人id
     * @param destId  目标id
     * @param devType 设备类型
     * @return ImMessage
     */
    public ImMessage userOnlineNotice(long fromId, long destId, int devType) {
        return generateUserNotice(fromId, destId, devType, IMConstants.MSG_TYPE_ONLINE, "");
    }


    /**
     * 好友离线通知
     *
     * @param fromId  发送人id
     * @param destId  目标id
     * @param devType 设备类型
     * @return ImMessage
     */
    public ImMessage userOfflineNotice(long fromId, long destId, int devType) {
        return generateUserNotice(fromId, destId, devType, IMConstants.MSG_TYPE_OFFLINE, "");
    }

    /**
     * 修改密码推送
     *
     * @param fromId  发送人id
     * @param destId  目标id
     * @param devType 设备类型
     * @return ImMessage
     */
    public ImMessage userUpdatePwdNotice(long fromId, long destId, int devType) {
        return generateUserNotice(fromId, destId, devType, IMConstants.MSG_TYPE_UPDATE_PWD, "");
    }

    /**
     * 群设置通知
     *
     * @param fromId  发送人
     * @param destId  接收人
     * @param devType 设备类型
     * @return ImMessage
     */
    public ImMessage groupSetNotice(long fromId, long destId, int devType, int msgType, String content) {
        return generateUserNotice(fromId, destId, devType, msgType, content);
    }

    /**
     * 好友设置通知
     *
     * @param fromId  发送人
     * @param destId  接收人
     * @param devType 设备类型
     * @return ImMessage
     */
    public ImMessage friendSetNotice(long fromId, long destId, int devType, int msgType, String content) {
        return groupSetNotice(fromId, destId, devType, msgType, content);
    }

}
