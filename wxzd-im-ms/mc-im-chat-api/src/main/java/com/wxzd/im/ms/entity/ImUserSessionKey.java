package com.wxzd.im.ms.entity;

public class ImUserSessionKey {
    private Long userId;

    private Integer destType;

    private Long destId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getDestType() {
        return destType;
    }

    public void setDestType(Integer destType) {
        this.destType = destType;
    }

    public Long getDestId() {
        return destId;
    }

    public void setDestId(Long destId) {
        this.destId = destId;
    }
}