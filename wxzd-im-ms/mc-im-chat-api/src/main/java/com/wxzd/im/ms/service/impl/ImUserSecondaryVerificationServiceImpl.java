package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImUserSecondaryVerification;
import com.wxzd.im.ms.mapper.ImUserSecondaryVerificationMapper;
import com.wxzd.im.ms.service.ImUserSecondaryVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import static com.wxzd.im.ms.parameter.CacheConstants.secByUserIdCache;

@Service
public class ImUserSecondaryVerificationServiceImpl implements ImUserSecondaryVerificationService {

    @Autowired
    private ImUserSecondaryVerificationMapper secondaryVerificationMapper;

    @Override
    @Cacheable(cacheNames = secByUserIdCache, key = "#userId", unless = "#result==null")
    public ImUserSecondaryVerification selectByUserId(Long userId) {
        return secondaryVerificationMapper.selectByUserId(userId);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return secondaryVerificationMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImUserSecondaryVerification record) {
        return secondaryVerificationMapper.insert(record);
    }

    @Override
    public int insertSelective(ImUserSecondaryVerification record) {
        return secondaryVerificationMapper.insertSelective(record);
    }

    @Override
    public ImUserSecondaryVerification selectByPrimaryKey(Long id) {
        return secondaryVerificationMapper.selectByPrimaryKey(id);
    }

    @Override
    @CachePut(value = secByUserIdCache, key = "#record.userId")
    public ImUserSecondaryVerification updateByPrimaryKeySelective(ImUserSecondaryVerification record) {
        secondaryVerificationMapper.updateByPrimaryKeySelective(record);
        return record;
    }

    @Override
    public int updateByPrimaryKey(ImUserSecondaryVerification record) {
        return secondaryVerificationMapper.updateByPrimaryKey(record);
    }
}
