package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImTempIdentity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImTempIdentityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImTempIdentity record);

    int insertSelective(ImTempIdentity record);

    ImTempIdentity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImTempIdentity record);

    int updateByPrimaryKey(ImTempIdentity record);

    ImTempIdentity selectTempIdentityByUserId(@Param("userId") Long userId, @Param("identityType") Integer identityType, @Param("tempType") Integer tempType);

    int deleteTempIdentityByUserId(@Param("userId") Long userId, @Param("identityType") Integer identityType, @Param("tempType") Integer tempType);

    ImTempIdentity selectByTempId(Long tempId);

    ImTempIdentity selectByIdentityId(Long identityId);

    int deleteByIdentityId(Long identityId);
}