package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ImMessageHistory {
    private Long id;

    private Long belongUserId;

    private Integer devType;

    private Integer geoId;

    private String msgId;

    private Long fromId;

    private Integer fromType;

    private String imageIconUrl;

    private Long destId;

    private String fromName;

    private Integer messageType;

    private Long sendTime;

    private Long receiveTime;

    private Long readTime;

    private Integer version;

    private Integer status;

    private Integer isEphemeralChat;

    private Integer ephemeralChatTime;

    private String content;

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}