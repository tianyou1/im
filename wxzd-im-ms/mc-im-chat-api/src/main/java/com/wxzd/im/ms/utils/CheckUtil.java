package com.wxzd.im.ms.utils;

import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.service.CacheService;
import com.wxzd.im.ms.service.CommonService;
import com.wxzd.im.ms.service.ImGroupService;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Date;

import static com.wxzd.im.ms.parameter.StaticConstant.*;

public class CheckUtil {

    /**
     * 判断字符串中是否仅包含字母数字和汉字
     *  各种字符的unicode编码的范围：
     * 汉字：[0x4e00,0x9fa5]（或十进制[19968,40869]）
     * 数字：[0x30,0x39]（或十进制[48, 57]）
     *小写字母：[0x61,0x7a]（或十进制[97, 122]）
     * 大写字母：[0x41,0x5a]（或十进制[65, 90]）

     */

    /**
     * 账号检验 大写英文字母、小写英文字母、数字、常用符号组成的长度4-32个字符的非纯数字字符串
     *
     * @param str
     * @return
     */
    public static boolean checkAccount(String str) {
        if (str.length() < accountMinL || str.length() > accountMaxL) {
            return false;
        }

        //特殊字符匹配
        String specialRegex = "^.*[^\\w].*$";
        if(str.matches(specialRegex)){
            return false;
        }

        //中文
        String chineseRegex="^.*[\\u4e00-\\u9fa5].*$";
        if(str.matches(chineseRegex)){
            return false;
        }
        String regex = "^.*[^\\d].*$";
        return str.matches(regex);
    }

    /**
     * 密码检验 大写英文字母、小写英文字母、数字、常用符号组成的长度为8-16个字符的字符串。且其中至少包含1个英文字母和1个数字
     *
     * @param str
     * @return
     */
    public static boolean checkPwd(String str) {
        str = str.replace(" ", "");
        String regex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{" + pwdMinL + "," + pwdMaxL + "}$";
        return str.matches(regex);
    }

    /**
     * 是否是私密模式
     *
     * @param token
     * @return
     */
    public static boolean checkSecretModel(String token) {
        return Integer.valueOf(Utils.decrypt(token.split("\\|")[3])).equals(isSecretModeYes);
    }

    /**
     * 设置的值是否不合法
     *
     * @param setVau
     * @return
     */
    public static boolean checkSetVau(Integer setVau) {
        return setVau == null || !(setVau.equals(setOn) || setVau.equals(setOff));
    }

    /**
     * 设置的值是否合法
     *
     * @param setVau
     * @return
     */
    public static boolean checkSetVau(String setVau) {
        return setVau.equals(setOn.toString()) || setVau.equals(setOff.toString());
    }

    /**
     * 检验隐私密码
     *
     * @param user
     * @param secretPwd
     * @return
     */
    public static boolean checkSecretPwd(ImUser user, String secretPwd) {
        if (StringUtils.isEmpty(secretPwd)) {
            return true;
        }
        boolean res = user != null && StringUtils.isNotBlank(user.getSecretModePwd());
        if (res) {
            res = !Utils.encrypt(secretPwd).equals(user.getSecretModePwd());
        }
        return res;
    }

    /**
     * 校验二次密码 验证信息为长度为16的字符串
     *
     * @param str
     * @return
     */
    public static boolean checkSecondaryPwd(String str) {
        return StringUtils.isNotBlank(str) && str.length() <= secondaryPwdMaxL;
    }

    /**
     * 校验好友申请理由 “发送添加申请说明”最多40个字。
     *
     * @return
     */
    public static boolean checkFriendReq(String str) {
        return (str.length() <= friendReqMaxL);
    }

    /**
     * 校验备注长度 备注名称最多16个字
     *
     * @param str
     * @return
     */
    public static boolean checkRemark(String str) {
        return (str.length() <= remarkMaxL);
    }

    /**
     * 校验昵称长度 昵称最多16个字
     *
     * @param str
     * @return
     */
    public static boolean checkNickName(String str) {
        if (StringUtils.isBlank(str)) {
            return true;
        }
        return str.length() <= nickNameMaxLength;
    }

    /**
     * 校验个性签名长度 个性签名最多16个字
     *
     * @param str
     * @return
     */
    public static boolean checkSign(String str) {
        if (StringUtils.isBlank(str)) {
            return true;
        }
        return str.length() <= signMaxLength;
    }

    /**
     * 校验意见反馈长度 意见反馈最多200个字
     *
     * @param str
     * @return
     */
    public static boolean checkSuggestion(String str) {
        return StringUtils.isNotBlank(str) && str.length() <= suggestionMaxL;
    }

    /**
     * 校验群名称长度 群名称最多16个字 " " 不能作为群名称
     *
     * @param str
     * @return
     */
    public static boolean checkGroupName(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        return (str.length() <= groupNameLength);
    }

    /**
     * 校验群公告长度 群公告最多80个字 " "
     *
     * @param str
     * @return
     */
    public static boolean checkGroupBulletin(String str) {
        return (str.length() <= groupBulletinLength);
    }

    /**
     * 校验群管理员人数 最多12个
     *
     * @param
     * @return
     */
    public static boolean checkManagerNum(int managerCount) {
        return managerCount <= managerMaxNum;
    }

    /**
     * 校验邀请人数 最多10个
     *
     * @param
     * @return
     */
    public static boolean checkInviteNum(int inviteCount) {
        return inviteCount <= maxInviteNum;
    }

    /**
     * 校验群聊最多人数
     *
     * @param
     * @return
     */
    public static boolean checkGroupNum(int groupNum) {
        return groupNum <= maxGroupNum;
    }

    /**
     * 检验字符串中是否含有id
     *
     * @param string
     * @param id
     * @return
     */
    public static boolean checkHaveId(String string, long id) {
        String[] strings = YxUtil.split(string);
        return Arrays.asList(strings).contains(String.valueOf(id));
    }

    /**
     * 校验二维码是否过期
     *
     * @param tempIdentity
     * @return
     */
    public static boolean checkQRCodeExpiryTime(ImTempIdentity tempIdentity) {
        return tempIdentity.getExpiryTime().getTime() >= new Date().getTime();
    }

    /**
     * 校验二维码是否超过使用
     *
     * @param tempIdentity
     * @return
     */
    public static boolean checkQRCodeCurrentTimes(ImTempIdentity tempIdentity) {
        return tempIdentity.getEffectiveTimes() > tempIdentity.getCurrentTimes();
    }

    /**
     * 是否是普通群员
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkPlainMember(ImGroupMember groupMember) {
        return groupMember != null && groupMember.getRole().equals(groupRoleMember);
    }

    /**
     * 是否是群主
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkRoleOwner(ImGroupMember groupMember) {
        return groupMember != null && groupMember.getRole().equals(groupRoleOwner);
    }

    /**
     * 是否是管理员
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkRoleManager(ImGroupMember groupMember) {
        return groupMember != null && groupMember.getRole().equals(groupRoleManager);
    }

    /**
     * 是否是管理员或群主
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkRoleOwnerAndManager(ImGroupMember groupMember) {
        return groupMember != null && (groupMember.getRole().equals(groupRoleOwner) || groupMember.getRole().equals(groupRoleManager));
    }

    /**
     * 是否可以操作的对象
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkCanSet(ImGroupMember groupMember, long userId) {
        return groupMember != null && groupMember.getUserId() != userId;
    }

    /**
     * 是否可以操作的对象
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkCanSet(ImGroupMember member, long userId, int role) {
        return member != null && member.getUserId() != userId && member.getRole() > role;
    }

    /**
     * 是否可以查询群成员信息
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkGroupCanViewFriend(ImGroupService groupService, long groupId) {
        ImGroup group = groupService.selectByPrimaryKey(groupId);
        return group != null && group.getIsViewFriend().equals(setOn);
    }

    /**
     * 是否可以需要群主确认
     *
     * @param
     * @param
     * @param
     * @return
     */
    public static boolean checkGroupNeedConfirm(ImGroupService groupService, long groupId) {
        ImGroupWithBLOBs group = groupService.selectByPrimaryKey(groupId);
        return group != null && group.getIsInviteConfirm().equals(setOn);
    }

    /**
     * 是否个人可以处理通知
     *
     * @return
     */
    public static boolean checkCanHandleReqAudit(ImUserRequestAudit imUserRequestAudit, long userId) {
        return imUserRequestAudit != null && imUserRequestAudit.getAuditStatus().equals(waitStatus) && imUserRequestAudit.getUserId() == userId;
    }

    /**
     * 是否群主管理员可以处理通知
     *
     * @return
     */
    public static boolean checkCanHandleReqAudit(ImUserRequestAudit imUserRequestAudit) {
        return imUserRequestAudit != null && imUserRequestAudit.getAuditStatus().equals(waitOwnerStatus);
    }

    /**
     * 验证好友请求是否可以发送
     *
     * @return
     */
    public static boolean checkFriendReqAudit(ImUserRequestAudit imUserRequestAudit, ImUserRequestDeleted imUserRequestDeleted) {
        //请求存在，并且未读，并且有效，并且待审核，并且申请时间小于等于7天，并且未删除
        return imUserRequestAudit != null && StringUtils.isEmpty(imUserRequestAudit.getIsRead())&&requestValidStatus.equals(imUserRequestAudit.getRequestStatus())&&waitStatus.equals(imUserRequestAudit.getAuditStatus())&&(System.currentTimeMillis() - imUserRequestAudit.getRequestTime()) / (1000 * 60 * 60 * 24)<=7 && imUserRequestDeleted == null;
    }

    /**
     * 群是否解散
     *
     * @return
     */
    public static boolean checkGroupDissolve(ImGroup group) {
        return group != null && group.getGroupStatus().equals(groupDissolve);
    }

    /**
     * 群是否冻结
     *
     * @return
     */
    public static boolean checkGroupFreeze(ImGroup group) {
        return group != null && group.getGroupStatus().equals(groupFreeze);
    }

    /**
     * 个人添加方式设置类别
     *
     * @return
     */
    public static boolean checkSearchType(int searchType) {
        return searchType != searchTypeUserId && searchType != searchTypeGroupId && searchType != searchTypeAccount;
    }

    /**
     * 用户消息通知提醒设置类别
     *
     * @return
     */
    public static boolean checkNotifyType(int notifyType) {
        return notifyType != voiceAndVideoNotify && notifyType != notifyShow && notifyType != voiceNotify && notifyType != vibrationNotify;
    }

    /**
     * 聊天记录加密设置类别
     *
     * @return
     */
    public static boolean checkSecretType(int secretType) {
        return secretType != voiceAndVideoNotify && secretType != notifyShow;
    }

    /**
     * 二维码时间限制
     *
     * @return
     */
    public static boolean checkQRCodeTime(String time) {
        return System.currentTimeMillis()>DateUtil.getDate(time,"yyyy-MM-dd HH:mm").getTime();
    }

    /**
     * 二维码次数限制
     *
     * @return
     */
    public static boolean checkQRCodeTimes(int times) {
        return times<QRCodeMinL||times>QRCodeMaxL;
    }


}
