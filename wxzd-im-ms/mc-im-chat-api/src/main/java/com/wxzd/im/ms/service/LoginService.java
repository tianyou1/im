package com.wxzd.im.ms.service;

import cn.hutool.log.StaticLog;
import com.github.jarod.qqwry.IPZone;
import com.github.jarod.qqwry.QQWry;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.parameter.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class LoginService {

    @Autowired
    private ChatServerFeign chatServerFeign;

    public static String getClientIP(HttpServletRequest request) {
        String fromSource = "X-Real-IP";
        String ip = request.getHeader("X-Real-IP");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
            fromSource = "X-Forwarded-For";
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
            fromSource = "Proxy-Client-IP";
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
            fromSource = "WL-Proxy-Client-IP";
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            fromSource = "request.getRemoteAddr";
        }
        StaticLog.info("App Client IP: " + ip + ", fromSource: " + fromSource);
        return ip;
    }

    /**
     * @param request
     * @return
     */
    public String getImServiceUrl(HttpServletRequest request, int device) {
        try {

            //如果重启登录服务器时，指向到华北2备份服务器
//			if(config.getIsbackup()){
//				return config.getLocal();
//			}

			/*
			String clientIp = getClientIP(request);
			QQWry qqwry = new QQWry();
			IPZone ipzone = qqwry.findIP(clientIp);
			String detailAddr = ipzone.getMainInfo() + "," + ipzone.getSubInfo();
			
						
			
			// 华南1
			if (detailAddr.indexOf("广东") != -1 || detailAddr.indexOf("江苏") != -1 || detailAddr.indexOf("广西") != -1
					|| detailAddr.indexOf("云南") != -1 || detailAddr.indexOf("安徽") != -1
					|| detailAddr.indexOf("海南") != -1 || detailAddr.indexOf("江西") != -1
					|| detailAddr.indexOf("澳门") != -1 || detailAddr.indexOf("贵州") != -1) {
				return "112.74.42.167";
			}
					
			
			
			// 华东1
			if ( detailAddr.indexOf("上海") != -1 || detailAddr.indexOf("") != -1
					|| detailAddr.indexOf("浙江") != -1 || detailAddr.indexOf("福建") != -1
					|| detailAddr.indexOf("重庆") != -1 || detailAddr.indexOf("湖北") != -1
					|| detailAddr.indexOf("四川") != -1 || detailAddr.indexOf("湖南") != -1) {
				return "116.62.13.57";
			}
			
			// 华北1
			if (detailAddr.indexOf("山东") != -1 ||detailAddr.indexOf("北京") != -1 || detailAddr.indexOf("河南") != -1 || detailAddr.indexOf("河北") != -1
					|| detailAddr.indexOf("陕西") != -1 || detailAddr.indexOf("天津") != -1
					|| detailAddr.indexOf("山西") != -1 || detailAddr.indexOf("辽宁") != -1
					|| detailAddr.indexOf("黑龙江") != -1 || detailAddr.indexOf("内蒙古") != -1
					|| detailAddr.indexOf("甘肃") != -1 || detailAddr.indexOf("青海") != -1
					|| detailAddr.indexOf("新疆") != -1 || detailAddr.indexOf("西藏") != -1
					|| detailAddr.indexOf("吉林") != -1 || detailAddr.indexOf("宁夏") != -1) {
				return "59.110.113.177";
			}
*/
			/*
			// 香港
			if (detailAddr.indexOf("香港") != -1) {
				return "47.91.158.95";
			}
			// 日本/台湾
			if (detailAddr.indexOf("日本") != -1 || detailAddr.indexOf("台湾") != -1) {
				return "47.91.16.244";
			}
			// 新加坡/韩国
			if (detailAddr.indexOf("新加坡") != -1 || detailAddr.indexOf("韩国") != -1) {
				return "47.88.242.54";
			}
			// 美国
			if (detailAddr.indexOf("美国") != -1) {
				return "47.88.55.103";
			}
			*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 默认华北1服务器,本来应该112.74.42.167，测试改：139.129.250.144
        return "119.29.151.20";

    }

    public String getSocketUrl() {
        ResponseData re = chatServerFeign.getSocketUrl();
        if (re == null) {
            return "";
        } else {
            Map<String, Object> map = (Map<String, Object>) re.getData();
            return map.get("host") + ":" + map.get("socketPort");
        }
    }

    public static void main(String args[]) {
        try {
            QQWry qqwry = new QQWry();
            IPZone ipzone = qqwry.findIP("112.251.182.223");

            String detailAddr = ipzone.getMainInfo() + "," + ipzone.getSubInfo();
            StaticLog.info(ipzone.getIp());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
