package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImMessage;
import com.wxzd.im.ms.mapper.ImMessageMapper;
import com.wxzd.im.ms.service.ImMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImMessageServiceImpl implements ImMessageService {
    @Autowired
    private ImMessageMapper imMessageMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imMessageMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImMessage record) {
        return this.imMessageMapper.insert(record);
    }

    @Override
    public int insertSelective(ImMessage record) {
        return this.imMessageMapper.insertSelective(record);
    }

    @Override
    public ImMessage selectByPrimaryKey(Long id) {
        return this.imMessageMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImMessage record) {
        return this.imMessageMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImMessage record) {
        return this.imMessageMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImMessage record) {
        return this.imMessageMapper.updateByPrimaryKey(record);
    }

}
