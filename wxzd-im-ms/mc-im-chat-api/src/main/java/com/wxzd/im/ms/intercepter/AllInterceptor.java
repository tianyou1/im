package com.wxzd.im.ms.intercepter;

import cn.hutool.core.date.DateTime;
import cn.hutool.log.StaticLog;
import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

public class AllInterceptor extends HandlerInterceptorAdapter {

    Logger logger = Logger.getLogger(AllInterceptor.class);

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        response.setHeader("Access-Control-Allow-Origin", "*");

        response.setHeader("Access-Control-Allow-Headers", "*");

        response.setHeader("Access-Control-Allow-Methods", "*");

        request.setAttribute("contextPath", request.getContextPath());
        loggerRequest(request, response, handler);
        return true;

    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

    }

    private void loggerRequest(HttpServletRequest request, HttpServletResponse response, Object handler) {
        try {
            String reqURL = request.getRequestURL().toString();
            String ip = request.getHeader("X-Real-IP");

            if (handler instanceof HandlerMethod) {
                StringBuffer params = new StringBuffer();
                Enumeration<String> key = request.getParameterNames();
                if (key != null) {
                    while (key.hasMoreElements()) {
                        String paramName = (String) key.nextElement();
                        String paramValue = request.getParameter(paramName);
                        params.append(paramName + "=" + paramValue + "&");
                    }
                }

                String parmeter = params.toString();

                StringBuilder sb = new StringBuilder(1000);
                sb.append("-----------------------").append(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"))
                        .append("-------------------------------------\n");
                HandlerMethod h = (HandlerMethod) handler;
                // Controller 的包名
                sb.append("Controller: ").append(h.getBean().getClass().getName()).append("\n");
                // 方法名称
                sb.append("Method    : ").append(h.getMethod().getName()).append("\n");
                // 请求方式 post\put\get 等等
                sb.append("RequestMethod    : ").append(request.getMethod()).append("\n");
                // 所有的请求参数
                sb.append("Params    : ").append(parmeter).append("\n");
                // 部分请求链接
                sb.append("URI       : ").append(request.getRequestURI()).append("\n");
                // 完整的请求链接
                sb.append("AllURI    : ").append(reqURL).append("\n");
                // 请求方的 ip地址
                sb.append("request IP: ").append(ip).append("\n");
                StaticLog.info(sb.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
