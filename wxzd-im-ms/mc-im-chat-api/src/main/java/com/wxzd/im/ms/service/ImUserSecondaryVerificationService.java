package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImUserSecondaryVerification;

public interface ImUserSecondaryVerificationService {

    /**
     * 根据用户id查找
     *
     * @param userId
     * @return
     */
    ImUserSecondaryVerification selectByUserId(Long userId);

    int deleteByPrimaryKey(Long id);

    int insert(ImUserSecondaryVerification record);

    int insertSelective(ImUserSecondaryVerification record);

    ImUserSecondaryVerification selectByPrimaryKey(Long id);

    ImUserSecondaryVerification updateByPrimaryKeySelective(ImUserSecondaryVerification record);

    int updateByPrimaryKey(ImUserSecondaryVerification record);

}
