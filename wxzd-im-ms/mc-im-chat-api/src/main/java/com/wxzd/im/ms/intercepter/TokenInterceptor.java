package com.wxzd.im.ms.intercepter;

import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.entity.result.ResultCodeEnum;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.utils.TokenUtil;
import com.wxzd.im.ms.utils.Utils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenInterceptor extends HandlerInterceptorAdapter {

    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        response.setHeader("Access-Control-Allow-Origin", "*");

        response.setHeader("Access-Control-Allow-Headers", "*");

        response.setHeader("Access-Control-Allow-Methods", "*");


        String token = request.getHeader("token");
        if (token == null) {
            token = request.getParameter("token");
        }
        if (token == null || token.indexOf("|") <= 0) {
            JSONObject json = generateTokenData("token不正确,token有误");
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write(json.toJSONString());
            return false;
        }

        String preToken = token.split("\\|")[0];
        String secToken = token.split("\\|")[1];//用户id
        String thirdToken = token.split("\\|")[2];//设备类型
        String fourToken = token.split("\\|")[3];//是否需要验证二级密码

        try {
            if (Long.parseLong(Utils.decrypt(preToken.trim())) > System.currentTimeMillis()) {
                JSONObject json = generateTokenData("token不正确,过时token");
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().write(json.toJSONString());
                return false;
            }

            String userId = Utils.decrypt(secToken.trim());
            String device = Utils.decrypt(thirdToken.trim());
            String isVerification = Utils.decrypt(fourToken.trim());

            //只要去判断是否有效就可以了
            if (Integer.valueOf(isVerification) == TokenEnum.tokenValid.getCode() || Integer.valueOf(isVerification) == TokenEnum.tokenValidSecret.getCode()) {
                String redisToken = TokenUtil.getUserToken(userId, device);
                if (redisToken == null) {
                    JSONObject json = generateTokenData("token不正确，token非法");
                    response.setContentType("text/html;charset=utf-8");
                    response.getWriter().write(json.toJSONString());
                    return false;
                }
                if (redisToken.equals(token)) {
                    return true;
                } else {
                    JSONObject json = generateTokenData("token不正确");
                    response.setContentType("text/html;charset=utf-8");
                    response.getWriter().write(json.toJSONString());
                    return false;
                }
            } else {
                JSONObject json = generateTokenData("token不正确,无效token");
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().write(json.toJSONString());
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject json = generateTokenData("token不正确,发生了意料之外的错误");
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write(json.toJSONString());
            return false;
        }
    }

    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {

    }


    private JSONObject generateTokenData(String content) {
        JSONObject json = new JSONObject();
        json.put("code", ResultCodeEnum.SERVER_ERROR.getCode().toString());
        JSONObject json1 = new JSONObject();
        json1.put("info", content);
        json.put("data", json1);
        return json;
    }


}
