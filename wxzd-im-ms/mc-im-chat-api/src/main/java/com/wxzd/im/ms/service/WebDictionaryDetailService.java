package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.WebDictionaryDetail;

import java.util.List;

public interface WebDictionaryDetailService {
    int deleteByPrimaryKey(Integer id);

    int insert(WebDictionaryDetail record);

    int insertSelective(WebDictionaryDetail record);

    WebDictionaryDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WebDictionaryDetail record);

    int updateByPrimaryKey(WebDictionaryDetail record);

    /**
     * 查找根据字典id
     *
     * @param dictionaryId
     * @return
     */
    List<WebDictionaryDetail> selectByDictionaryId(Integer dictionaryId);
}
