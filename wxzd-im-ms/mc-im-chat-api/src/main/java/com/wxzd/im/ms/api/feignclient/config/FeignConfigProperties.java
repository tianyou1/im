package com.wxzd.im.ms.api.feignclient.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取一些通用的配置
 */
@ConfigurationProperties(prefix = "feign")
@Component
public class FeignConfigProperties {

    private String tempDir;

    public String getTempDir() {
        return tempDir;
    }

    public void setTempDir(String tempDir) {
        this.tempDir = tempDir;
    }
}
