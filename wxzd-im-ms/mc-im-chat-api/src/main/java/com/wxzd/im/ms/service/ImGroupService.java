package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.parameter.ResponseData;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ImGroupService {
    int deleteByPrimaryKey(Long id);

    int insert(ImGroupWithBLOBs record);

    int insertSelective(ImGroupWithBLOBs record);

    ImGroupWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImGroupWithBLOBs record);

    ImGroupWithBLOBs updateGroupCache(ImGroupWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImGroupWithBLOBs record);

    int updateByPrimaryKey(ImGroup record);

    /**
     * 创建群
     *
     * @param
     * @param
     * @param userId
     * @param
     * @return
     */
    ResponseData createGroup(List<ImGroupMember> inMembers, List<ImUserSession> inSessions, List<ImUserRequestAudit> outRequestAudits, long groupId, long userId, Set<Long> memberSet, Set<Long> outSet, int device);

    /**
     * 根据是否隐私模式加载群组列表
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> getGroupsInfo(Map<String, Object> map);

    /**
     * @param member      原来群主
     * @param groupMember 转让对象
     * @param devType
     */
    void transferGroup(ImGroupMember member, ImGroupMember groupMember, int devType);

    /**
     * 管理员批量设置
     *
     * @param groupId
     * @param managers
     * @param notManagers
     * @param userId
     * @param device
     */
    void managerListSet(Long groupId, Set<Long> managers, Set<Long> notManagers, long userId, int device);

    /**
     * 退出群聊
     *
     * @param device
     * @param
     * @param member
     */
    void updateQuitGroup(int device, ImGroupMember member);

    /**
     * 邀请加群
     *
     * @param
     * @param groupId
     * @param userId
     * @param
     * @param
     */
    //void inviteJoinGroup(List<ImGroupMember> inMembers, List<ImUserSession> inSessions, List<ImUserRequestAudit> outRequestAudits, long groupId, long userId,Set<Long> memberSet,Set<Long> outSet,int flag);

    void inviteJoinGroup(Set<Long> memberIds, long groupId, long userId, int device, String requestContent);

    /**
     * 用户进群审核接口（被邀请人）
     *
     * @param userId
     * @param devType
     * @param isAccept
     * @param imUserRequestAudit
     * @param
     */
    void acceptInviteGroupReq(long userId, int devType, int isAccept, ImUserRequestAudit imUserRequestAudit);

    /**
     * 用户进群审核接口（群主管理员）
     *
     * @param userId
     * @param devType
     * @param isAccept
     * @param imUserRequestAudit
     * @param
     */
    void acceptInviteReq(long userId, int devType, int isAccept, ImUserRequestAudit imUserRequestAudit);

    /**
     * 解散群
     *
     * @param groupId
     * @param userId
     * @param devType
     * @return
     */
    int dismissGroup(long groupId, long userId, int devType);

    /**
     * 修改群内备注
     *
     * @param member
     * @param markName
     * @param
     * @param device
     */
    void updateGroupMemberMark(ImGroupMember member, String markName, int device);

    /**
     * 移除群成员
     *
     * @param member
     * @param destMember
     */
    void removeFromGroup(ImGroupMember member, ImGroupMember destMember);

    /**
     * 批量移除群成员
     *
     * @param member
     * @param memberIds
     */
    void removeIdsFromGroup(ImGroupMember member, Set<Long> memberIds);

    /**
     * 根据条件查找群
     *
     * @param record
     * @return
     */
    List<ImGroup> selectByConditions(ImGroup record);

    /**
     * 好友请求列表
     *
     * @param
     * @return
     */
    List<Map<String, Object>> friendReqList(Map<String, Object> map);

    /**
     * 好友请求列表(总数)
     *
     * @param
     * @return
     */
    int countFriendReqList(Map<String, Object> map);

    /**
     * 群组邀请列表
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> inviteReqList(long userId);

    /**
     * 群组邀请数量
     *
     * @param userId
     * @return
     */
    int countInviteReqList(long userId);

    /**
     * 进群确认列表
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> confirmReqList(long userId);

    /**
     * 进群确认数量
     *
     * @param userId
     * @return
     */
    int countConfirmReqList(long userId);

    /**
     * 群好友查询
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> groupFriendQuery(Map<String, Object> map);


}