package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.WebDictionaryDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WebDictionaryDetailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WebDictionaryDetail record);

    int insertSelective(WebDictionaryDetail record);

    WebDictionaryDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WebDictionaryDetail record);

    int updateByPrimaryKey(WebDictionaryDetail record);

    /**
     * 查找根据字典id
     *
     * @param dictionaryId
     * @return
     */
    List<WebDictionaryDetail> selectByDictionaryId(@Param("dictionaryId") Integer dictionaryId);
}