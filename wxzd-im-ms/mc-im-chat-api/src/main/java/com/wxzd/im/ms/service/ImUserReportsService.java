package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImUserReports;
import com.wxzd.im.ms.entity.ImUserReportsWithBLOBs;

public interface ImUserReportsService {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserReportsWithBLOBs record);

    int insertSelective(ImUserReportsWithBLOBs record);

    ImUserReportsWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserReportsWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImUserReportsWithBLOBs record);

    int updateByPrimaryKey(ImUserReports record);
}
