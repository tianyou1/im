package com.wxzd.im.ms.service;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.log.StaticLog;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.mashape.unirest.http.Unirest;
import com.wxzd.im.ms.parameter.Constants;
import com.wxzd.im.ms.utils.RedisUtil;
import com.wxzd.im.ms.utils.Utils;
import com.yunpian.sdk.YunpianClient;
import com.yunpian.sdk.model.Result;
import com.yunpian.sdk.model.SmsSingleSend;
import com.yunpian.sdk.model.Template;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SmsService {

    public boolean sendSms(String mobile, String validateNum) {
        try {
            StaticLog.info("验证码：" + validateNum);
            // 设置超时时间-可自行调整
            System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
            System.setProperty("sun.net.client.defaultReadTimeout", "10000");
            // 初始化ascClient需要的几个参数
            final String product = "Dysmsapi";// 短信API产品名称（短信产品名固定，无需修改）
            final String domain = "dysmsapi.aliyuncs.com";// 短信API产品域名（接口地址固定，无需修改）
            // 替换成你的AK
            final String accessKeyId = "LTAIJfTUPRB7X5IC";// 你的accessKeyId,参考本文档步骤2
            final String accessKeySecret = "efE9kLBB6FIaHVtMz4zwHyAJPHMKlV";// 你的accessKeySecret，参考本文档步骤2
            // 初始化ascClient,暂时不支持多region（请勿修改）
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            // 组装请求对象
            SendSmsRequest request = new SendSmsRequest();
            // 使用post提交
            request.setMethod(MethodType.POST);
            // 必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
            request.setPhoneNumbers(mobile);
            // 必填:短信签名-可在短信控制台中找到
            request.setSignName("私聊社区");
            // 必填:短信模板-可在短信控制台中找到
            request.setTemplateCode("SMS_163853199");
            // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            // 友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
            request.setTemplateParam("{\"name\":\"Tom\", \"code\":\"" + validateNum + "\"}");
            // 可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
            // request.setSmsUpExtendCode("90997");
            // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            request.setOutId("yourOutId");
            // 请求失败这里会抛ClientException异常
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
                // 请求成功
                StaticLog.info(sendSmsResponse.getMessage());
                return true;
            } else {
                StaticLog.info(sendSmsResponse.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean sendSmsRY(String mobile, String validateNum) {
        HashMap<String, Object> result = null;
        CCPRestSmsSDK restAPI = new CCPRestSmsSDK();
        restAPI.init("app.cloopen.com", "8883");
        restAPI.setAccount("8a48b5514f2b46d0014f2b7880e10082", "8d855d95e0d940cc9cac1b39fe2ef6d6");
        restAPI.setAppId("8aaf07085f5c54cf015f958eea4b1578");
        result = restAPI.sendTemplateSMS(mobile, "216716", new String[]{validateNum, "10"});

        StaticLog.info("SDKTestGetSubAccounts result=" + result);
        if ("000000".equals(result.get("statusCode"))) {
            /*
             * @SuppressWarnings("unchecked") HashMap<String, Object> data =
             * (HashMap<String, Object>) result.get("data"); Set<String> keySet
             * = data.keySet(); for (String key : keySet) { Object object =
             * data.get(key); StaticLog.info(key + " = " + object); }
             */
            return true;
        } else {
            StaticLog.info("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
        }
        return false;
    }

    public boolean sendSmsOld(String mobile, String validateNum) {
        RedisUtil.expired(Constants.SMS_KEY.getBytes(), 60 * 30);
        RedisUtil.hset(Constants.SMS_KEY, mobile, validateNum);

        YunpianClient clnt = new YunpianClient("0f814eee88ca833f4270d64188ce05bf").init();

        String text = "【移动云信】您的验证码是#code#。如非本人操作，请忽略本短信";
        Map<String, String> tplParam = clnt.newParam(1);

        if (mobile.startsWith("+1")) {
            tplParam.put("tpl_id", "1689668");// 其它
            Result<List<Template>> templates = clnt.tpl().get(tplParam);
            text = templates.getData().get(0).getTpl_content().replace("#code#", validateNum);
        } else if (mobile.startsWith("+81")) {
            tplParam.put("tpl_id", "1689674");// 日本
            Result<List<Template>> templates = clnt.tpl().get(tplParam);
            text = templates.getData().get(0).getTpl_content().replace("#code#", validateNum);
        } else if (mobile.startsWith("+82")) {
            tplParam.put("tpl_id", "1689670");// 韩国
            Result<List<Template>> templates = clnt.tpl().get(tplParam);
            text = templates.getData().get(0).getTpl_content().replace("#code#", validateNum);
        } else if (mobile.startsWith("+86")) {
            tplParam.put("tpl_id", "1687344");// 中国
            Result<List<Template>> templates = clnt.tpl().get(tplParam);
            text = templates.getData().get(0).getTpl_content().replace("#code#", validateNum);
        } else {
            tplParam.put("tpl_id", "1687344");// 默认中国
            Result<List<Template>> templates = clnt.tpl().get(tplParam);
            text = templates.getData().get(0).getTpl_content().replace("#code#", validateNum);
        }

        Map<String, String> param = clnt.newParam(5);
        param.put("mobile", mobile);
        param.put("text", text);

        Result<SmsSingleSend> r = clnt.sms().single_send(param);
        StaticLog.info("验证码：" + validateNum);
        clnt.close();
        if (r.getCode() == 0) {
            return true;
        }

        return false;
    }


    /**
     * https://api.smsbao.com
     * 短信宝接口
     *
     * @param mobile
     * @param validateNum
     * @return
     */
    public boolean sendSmsDXB(String mobile, String validateNum) {
        boolean res = false;
        try {
            String signStr = "【蚂蚁刷呗】";
            String content = URLUtil.encode(signStr + "你的验证码为：" + validateNum);
            String userName = "ag8098";
            String pwd = Utils.toMd5("qwe123");
            String body = Unirest.get("https://api.smsbao.com/sms?u=" + userName + "&p=" + pwd + "&m=" + mobile + "&c=" + content).asString().getBody();
            StaticLog.info("发送结果：" + body);
            res = true;

        } catch (Exception e) {
            StaticLog.info("短信宝发送失败，手机号：{}，验证码为：{}，异常信息：{}", mobile, validateNum, e.toString());
        }

        return res;
    }

    /**
     * http://www.isms360.com/channel.aspx?id=25
     * <p>
     * http://m.isms360.com:8085/mt/MT3.ashx?src=username&pwd=123456&
     * ServiceID=SEND&dest=86137012345678&sender=106577777&
     * msg=6D4B8BD5002B002BFF01&codec=8
     * 天天短信接口
     *
     * @param mobile
     * @param validateNum
     * @return
     */
    public boolean sendSmsTT(String mobile, String validateNum) {
        boolean res = false;
        try {
            String signStr = "【全网聊】";
            String unicodeStr = signStr + "尊敬的用户您好，您的短信验证码为:" + validateNum;
            String content = HexUtil.encodeHexStr(unicodeStr, CharsetUtil.charset("UTF-16BE")).toUpperCase();
            StaticLog.info("十六进制：" + content);
            String userName = "quanwangliao";
            String pwd = "qq727722";
            String body = Unirest.get("http://m.isms360.com:8085/mt/MT3.ashx?src=" + userName + "&pwd=" + pwd + "&ServiceID=SEND&dest=" + mobile + "&msg=" + content + "&codec=8").asString().getBody();
            StaticLog.info("发送结果：" + body);
            res = true;

        } catch (Exception e) {
            StaticLog.info("天天短信发送失败，手机号：{}，验证码为：{}，异常信息：{}", mobile, validateNum, e.toString());
        }

        return res;
    }

    public boolean equalValidate(String mobile, String validateNum) {
        byte[] bytes = RedisUtil.get((Constants.SMS_KEY + mobile).getBytes());
        if (bytes == null) {
            return false;
        }
        String num = new String(bytes);
        return validateNum.equals(num);

    }

    public static void main(String args[]) throws Exception {
        //SmsService service=new SmsService();
        //service.sendSmsRY("13558756061", "ry");
        //service.sendSmsOld("13558756061", "云信");
        //service.sendSms("13558756061", "bd");
        //HttpResponse<InputStream> response = Unirest.get("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erW1nyiavocyuia8W6ny6XEJo2aBeHd7J3Ybw9fPKghck8ALicZLOm20NK7d55z5X9RBxSwTsVA8FQww/132").asBinary();
        //FileUtil.writeFromStream(response.getBody(), "d:/1.jpg");

        try {
			/*SmsService sms=new SmsService();			
			sms.sendSmsTT("17628097063", "12352");*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
