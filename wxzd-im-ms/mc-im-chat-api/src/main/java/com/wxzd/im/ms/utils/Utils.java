package com.wxzd.im.ms.utils;

import com.wxzd.im.ms.utils.entity.ProtobufSerializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nustaq.serialization.FSTConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Utils {
    static final FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();

    static final Log log = LogFactory.getLog(Utils.class);

    public static boolean isOpen = true;

    private static final int len = 16;

    private static final String key = "01213910847463829232312312";

    public static ApplicationContext getAppcxt() {
        ClassPathXmlApplicationContext classPathXmlApplicationContext;
        String[] url = {"config/*.xml"};
        classPathXmlApplicationContext = new ClassPathXmlApplicationContext(url);
        return (ApplicationContext) classPathXmlApplicationContext;
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    public static byte[] getMsgId() {
        return getUUID().getBytes();
    }

    public static List<byte[]> getNotifactions(RedisTemplate<String, byte[]> template, String key, byte[] val) {
        return null;
    }

    public static void setRedisList(RedisTemplate<String, byte[]> template, String key, byte[] val) {
        template.setValueSerializer((RedisSerializer) new ProtobufSerializer());
        template.opsForList().rightPush(key, val);
    }

    public static List<byte[]> getRedisList(RedisTemplate<String, byte[]> template, String key) {
        template.setValueSerializer((RedisSerializer) new ProtobufSerializer());
        List<byte[]> list = template.opsForList().range(key, 0L, -1L);
        return list;
    }

    public static String randomInt(int length) {
        StringBuilder sb = new StringBuilder();
        Random rand = new Random();
        for (int i = 0; i < length; i++)
            sb.append(rand.nextInt(10));
        return sb.toString();
    }

    public static String random(int length) {
        StringBuilder sb = new StringBuilder();
        Random rand = new Random();
        Random randdata = new Random();
        int data = 0;
        for (int i = 0; i < length; i++) {
            int index = rand.nextInt(3);
            switch (index) {
                case 0:
                    data = randdata.nextInt(10);
                    sb.append(data);
                    break;
                case 1:
                    data = randdata.nextInt(26) + 65;
                    sb.append((char) data);
                    break;
                case 2:
                    data = randdata.nextInt(26) + 97;
                    sb.append((char) data);
                    break;
            }
        }
        return sb.toString();
    }

    public static String toMd5(String str) {
        String reStr = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(str.getBytes());
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : bytes) {
                int bt = b & 0xFF;
                if (bt < 16)
                    stringBuffer.append(0);
                stringBuffer.append(Integer.toHexString(bt));
            }
            reStr = stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return reStr;
    }

    public static byte[] mergeByte(byte[] one, byte[]... bytes) {
        byte[] combined = null;
        int len = one.length;
        for (byte[] two : bytes) {
            if (two != null)
                len += two.length;
        }
        combined = new byte[len];
        System.arraycopy(one, 0, combined, 0, one.length);
        int pos = one.length;
        for (byte[] two : bytes) {
            if (two != null && two.length > 0) {
                System.arraycopy(two, 0, combined, pos, two.length);
                pos += two.length;
            }
        }
        return combined;
    }

    public static byte[] getBytes(byte[] b, int start, int length) {
        byte[] bytes = new byte[length];
        System.arraycopy(b, start, bytes, 0, length);
        return bytes;
    }

    public static byte[] intToBytes(int my_int) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeInt(my_int);
            out.close();
            byte[] int_bytes = bos.toByteArray();
            bos.close();
            return int_bytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int bytesToInt(byte[] int_bytes) {
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(int_bytes);
            ObjectInputStream ois = new ObjectInputStream(bis);
            int my_int = ois.readInt();
            ois.close();
            return my_int;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static boolean writeFile(byte[] input, String path) {
        try {
            File file2 = new File(path);
            if (file2.exists())
                file2.delete();
            file2.createNewFile();
            FileOutputStream fos = new FileOutputStream(file2);
            fos.write(input);
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static byte[] readFile(String path) {
        try {
            File file = new File(path);
            FileInputStream fis = new FileInputStream(file);
            byte[] fileByte = new byte[fis.available()];
            fis.read(fileByte);
            fis.close();
            return fileByte;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] serializeObjectToBytes(Object obj) {
        try {
            byte[] chatByte = conf.asByteArray(obj);
            if (chatByte == null || chatByte.length == 0)
                return null;
            return chatByte;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object deserializeObjectFromBytes(byte[] chatByte) {
        try {
            return conf.asObject(chatByte);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void serializeObjectToFile(Object obj, String file) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(obj);
            byte[] chatByte = out.toByteArray();
            if (chatByte == null || chatByte.length == 0)
                return;
            writeFile(chatByte, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object deserializeObjectFromFile(String file) {
        try {
            byte[] chatByte = readFile(file);
            if (chatByte == null || chatByte.length == 0)
                return null;
            ByteArrayInputStream in = new ByteArrayInputStream(chatByte);
            ObjectInputStream is = new ObjectInputStream(in);
            return is.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getByteByStream(InputStream is, int totalLen) {
        byte[] receive = new byte[totalLen];
        try {
            int offset = 0;
            int numRead = 0;
            while (offset < receive.length && (numRead = is.read(receive, offset, receive.length - offset)) >= 0)
                offset += numRead;
            if (offset < receive.length) {
                System.out.println("Could not completely read request body");
                return null;
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return receive;
    }

    public static final String encrypt(String password) {
        if (password == null)
            return "";
        if (password.length() == 0)
            return "";
        BigInteger bi_passwd = new BigInteger(password.getBytes());
        BigInteger bi_r0 = new BigInteger("01213910847463829232312312");
        BigInteger bi_r1 = bi_r0.xor(bi_passwd);
        return bi_r1.toString(16);
    }

    public static final String decrypt(String encrypted) {
        if (encrypted == null)
            return "";
        if (encrypted.length() == 0)
            return "";
        BigInteger bi_confuse = new BigInteger("01213910847463829232312312");
        try {
            BigInteger bi_r1 = new BigInteger(encrypted, 16);
            BigInteger bi_r0 = bi_r1.xor(bi_confuse);
            return new String(bi_r0.toByteArray());
        } catch (Exception e) {
            return "";
        }
    }
}