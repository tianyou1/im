package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImGroupMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ImGroupMemberService {
    int deleteByPrimaryKey(Long id);

    int insert(ImGroupMember record);

    int insertSelective(ImGroupMember record);

    ImGroupMember selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImGroupMember record);

    ImGroupMember updateGroupMemberCache(ImGroupMember record);

    void removeSecretGroupCache(ImGroupMember record);

    void removeGroupCache(Long userId);

    ImGroupMember updateGroupMemberRoleCache(ImGroupMember record);

    int updateByPrimaryKey(ImGroupMember record);

    ImGroupMember selectByCondition(ImGroupMember record);

    List<ImGroupMember> selectByConditions(ImGroupMember record);

    int countMembers(Map<String, Object> map);

    int deleteByCondition(ImGroupMember imGroupMember);

    List<Long> selectOwnerAndManagers(long groupId);

    Set<Long> selectUserIdSet(long groupId);

    List<Long> selectAllMemberUserId(@Param("groupId") Long groupId, @Param("role") Integer role);

    Set<Long> selectAllMemberId(@Param("groupId") Long groupId, @Param("role") Integer role);

    int batchInsertSelective(List<ImGroupMember> members);
}