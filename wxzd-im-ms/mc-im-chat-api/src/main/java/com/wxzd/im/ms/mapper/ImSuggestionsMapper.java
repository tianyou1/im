package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImSuggestions;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImSuggestionsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImSuggestions record);

    int insertSelective(ImSuggestions record);

    ImSuggestions selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImSuggestions record);

    int updateByPrimaryKeyWithBLOBs(ImSuggestions record);

    int updateByPrimaryKey(ImSuggestions record);
}