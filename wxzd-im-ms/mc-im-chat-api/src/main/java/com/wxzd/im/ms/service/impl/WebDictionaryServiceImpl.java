package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.WebDictionary;
import com.wxzd.im.ms.mapper.WebDictionaryMapper;
import com.wxzd.im.ms.service.WebDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WebDictionaryServiceImpl implements WebDictionaryService {
    @Autowired
    private WebDictionaryMapper webDictionaryMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return webDictionaryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(WebDictionary record) {
        return webDictionaryMapper.insert(record);
    }

    @Override
    public int insertSelective(WebDictionary record) {
        return webDictionaryMapper.insertSelective(record);
    }

    @Override
    public WebDictionary selectByPrimaryKey(Integer id) {
        return webDictionaryMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(WebDictionary record) {
        return webDictionaryMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(WebDictionary record) {
        return webDictionaryMapper.updateByPrimaryKey(record);
    }

    @Override
    public WebDictionary selectByType(String type) {
        return webDictionaryMapper.selectByType(type);
    }
}
