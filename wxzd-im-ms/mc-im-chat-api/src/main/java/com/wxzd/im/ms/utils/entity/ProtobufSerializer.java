package com.wxzd.im.ms.utils.entity;

import org.springframework.data.redis.serializer.RedisSerializer;

public class ProtobufSerializer implements RedisSerializer<byte[]> {
  public byte[] deserialize(byte[] bytes) {
    return bytes;
  }
  
  public byte[] serialize(byte[] object) {
    return object;
  }
}