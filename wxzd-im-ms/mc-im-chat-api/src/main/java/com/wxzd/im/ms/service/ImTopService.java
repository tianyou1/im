package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImTop;

import java.util.List;
import java.util.Map;

public interface ImTopService {
    int deleteByPrimaryKey(Long id);

    int insert(ImTop record);

    int insertSelective(ImTop record);

    ImTop selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImTop record);

    int updateByPrimaryKey(ImTop record);

    int deleteByCondition(ImTop record);

    /**
     * 加载置顶列表
     *
     * @param
     * @return
     */
    List<Map<String, Object>> getTopList(Long userId);

    /**
     * 置顶
     *
     * @param userId
     * @param destId
     * @param destType
     */
    void isTop(Long userId, Long destId, Integer destType);

    /**
     * 取消置顶
     *
     * @param userId
     * @param destId
     * @param destType
     */
    void cancelTop(Long userId, Long destId, Integer destType);
}