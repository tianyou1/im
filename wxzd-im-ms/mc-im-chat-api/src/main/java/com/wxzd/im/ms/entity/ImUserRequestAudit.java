package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ImUserRequestAudit {
    private Long id;

    private Long fromId;

    private Long userId;

    private Long destId;

    private Integer destType;

    private Long requestTime;

    private Integer requestStatus;

    private Long auditorId;

    private Long auditTime;

    private String auditContent;

    private Integer auditStatus;

    private String isRead;

    private String requestContent;




}