package com.wxzd.im.ms.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.github.pagehelper.PageHelper;
import com.google.zxing.WriterException;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.parameter.LockKey;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.wxzd.im.ms.parameter.IMConstants.*;
import static com.wxzd.im.ms.parameter.ReturnConstant.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;
import static com.wxzd.im.ms.utils.BanedTimeSet.*;

@RestController
@Api(value = "大部分群组HTTP接口")
@RequestMapping(value = "/group", method = RequestMethod.POST)
@SuppressWarnings({"unchecked", "rawtypes"})
public class GroupController {

    @Autowired
    private ImGroupService groupService;

    @Autowired
    private ImGroupMemberService groupMemberService;

    @Autowired
    private ImUserService userService;

    @Autowired
    private ImTempIdentityService tempIdentityService;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private ImUserRequestAuditService userRequestAuditService;

    @Autowired
    private MessageFactory msgFactory;

    @Autowired
    private ReadyService readyService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ChatServerFeign chatServerFeign;

    private ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    @ApiOperation("创建群组，返回群相关信息")
    @RequestMapping("/createGroup")
    @ResponseBody
    public ResponseData createGroup(@RequestHeader String token,
                                    @ApiParam("群名称") @RequestParam String name,
                                    @ApiParam("创建时加入邀请加入的成员id，用逗号分隔开") @RequestParam String ids) {
        //判断名称
        if (StringUtils.isBlank(name)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //过滤重复的数据
        Set<Long> memberSet = YxUtil.splitSet(ids);
        long userId = CommonUtils.getUserIdByToken(token);
        //获取双向好友set
        Set<Long> friendSet = cacheService.getFriendsSet(userId);
        //去除不是好友的
        memberSet.retainAll(friendSet);
        //判断邀请人的数量
        if (!CheckUtil.checkGroupNum(memberSet.size() + 1)) {
            return YxUtil.createFail(groupNumReturn);
        }
        //初始化群
        long groupId = commonService.createGroup(name, userId);
        //添加好友进群
        //先筛选
        //无法直接进群的outSet 进群的就是memberIds
        Set<Long> outSet = new HashSet<>();
        //分开直接进群和无法进群的
        commonService.getInOutIdsByUser(memberSet, outSet);
        //组织 批量操作的数据 批量添加群成员 批量添加会话
        //可以直接进群的
        List<ImGroupMember> inMembers = new ArrayList<>();
        //将群主添加到批量操作
        inMembers.add(commonService.addGroupMember2(userId, groupId, userId, groupRoleOwner));
        List<ImUserSession> inSessions = new ArrayList<>();
        //将群主会话添加到批量操作
        inSessions.add(commonService.createSession2(userId, groupId));
        for (Long uId : memberSet) {
            commonService.addGroupMemberList2(inMembers, userId, groupId, uId, groupRoleMember);
            commonService.addSessionList(inSessions, uId, groupId);
        }
        //无法直接进群的
        List<ImUserRequestAudit> outRequestAudits = new ArrayList<>();
        for (Long uId : outSet) {
            commonService.addUserRequestList(outRequestAudits, userId, uId, groupId, uId, groupType, "", requestValidStatus, waitStatus);
        }
        try {
            int device = CommonUtils.getDevTypeByToken(token);
            return groupService.createGroup(inMembers, inSessions, outRequestAudits, groupId, userId, memberSet, outSet, device);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群信息查询接口")
    @RequestMapping("/queryGroupInfo")
    @ResponseBody
    public ResponseData queryGroupInfo(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId) {
        //获取用户id
        Long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (groupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        //实际操作
        try {
            //查找群主信息
            ImGroupMember imGroupMember = cacheService.getOwner(groupId);
            //获取群主的的用户信息
            ImUserWithBLOBs imUser = userService.selectByPrimaryKey(imGroupMember.getUserId());
            //查找群
            ImGroupWithBLOBs group = groupService.selectByPrimaryKey(groupId);
            Map<String, Object> map = JSON.parseObject(JSON.toJSONString(group, filter), Map.class);
            map.put("ownerName", imUser.getNickName());
            map.put("role", groupMember.getRole());
            map.put("isSecretGroup", groupMember.getIsSecretGroup());
            map.put("receiveTip", groupMember.getReceiveTip());
            map.put("isShowSessionMessage", groupMember.getIsShowSessionMessage());
            map.put("isCollect", groupMember.getIsCollect());
            map.put("isSecretHistory", groupMember.getIsSecretHistory());
            map.put("isAnonymous", groupMember.getIsAnonymous());
            map.put("anonymousName", groupMember.getAnonymousName());
            map.put("anonymousHeaderUrl", groupMember.getAnonymousHeaderUrl());
            map.put("memberId", groupMember.getId());//2020 12 3
            map.put("is_anonymous", group.getIsAnonymous());
            return YxUtil.createSuccessData(map);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }

    }

    //过滤器 将JSON.toJSONString() 中的null值 过滤为""; JSON.toJSONString(patrol,filter)
    private static ValueFilter filter = (obj, s, v) -> {
        if (v == null)
            return "";
        return v;
    };

    @ApiOperation("通过群ID（群临时ID）查询群信息接口")
    @RequestMapping("/queryGroupInfoByTempId")
    @ResponseBody
    public ResponseData queryGroupInfoByTempId(@RequestHeader String token,
                                               @ApiParam("群临时id") @RequestParam(required = false) Long tempId,
                                               @ApiParam("群id") @RequestParam(required = false) Long id) {
        if ((tempId == null && id == null) || (tempId != null && id != null)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //实际操作
        try {
            long groupId;
            Map<String, Object> map = new HashMap<>();
            if (tempId != null) {
                ImTempIdentity imTempIdentity = tempIdentityService.selectByTempId(tempId);
                if (imTempIdentity == null || imTempIdentity.getIdentityId() < 0) {
                    return YxUtil.createFail(QRCodeManualExpiredReturn);
                }
                if (!CheckUtil.checkQRCodeExpiryTime(imTempIdentity)) {
                    return YxUtil.createFail(QRCodeEndTimeReturn);
                }
                if (!CheckUtil.checkQRCodeCurrentTimes(imTempIdentity)) {
                    return YxUtil.createFail(QRCodeEndUseReturn);
                }
                groupId = imTempIdentity.getIdentityId();
                map.put("tempId", tempId);
                map.put("groupId", "");
            } else {
                groupId = id;
                map.put("tempId", "");
                map.put("groupId", id);
            }
            //查找群
            ImGroupWithBLOBs group = groupService.selectByPrimaryKey(groupId);
            map.put("name", group.getName());
            map.put("headUrl", group.getHeadUrl());
            //查找群内人数
            map.put("memberCount", cacheService.countMembers(groupId));
            return YxUtil.createSuccessData(map);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);

        }
    }

    @ApiOperation("群设置接口 群名称设置（后台接口需要验证用户是否为群主或管理员）")
    @RequestMapping("/groupNameSet")
    @ResponseBody
    public ResponseData groupNameSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long destId,
                                     @ApiParam("群名称") @RequestParam String name) {
        if (!CheckUtil.checkGroupName(name)) {
            return YxUtil.createFail(groupNameReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, destId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            //群
            ImGroupWithBLOBs group = groupService.selectByPrimaryKey(destId);
            group.setName(name);
            int res = groupService.updateByPrimaryKeySelective(group);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(group);
                //推送一条系统消息到群里 表示群主修改了群名称
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                //获取设备
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, destId, device, MSG_TYPE_UPDATE_GROUP_NAME, fName + "更改了群名称");
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群设置接口 群公告设置（后台接口需要验证用户是否为群主或管理员）")
    @RequestMapping("/groupDescriptionSet")
    @ResponseBody
    public ResponseData groupDescriptionSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long destId,
                                            @ApiParam("群公告") @RequestParam(defaultValue = "") String descriptions) {
        if (!CheckUtil.checkGroupBulletin(descriptions)) {
            return YxUtil.createFail(groupBulletinNumReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);

        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, destId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            //群主
            ImGroupWithBLOBs record = groupService.selectByPrimaryKey(destId);
            record.setBulletinContent(descriptions);
            record.setBulletinEditorId(userId);
            record.setBulletinEditTime(System.currentTimeMillis());
            int res = groupService.updateByPrimaryKeySelective(record);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(record);
                //推送一条系统消息到群里 表示群主修改了群公告
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, destId, device, MSG_TYPE_UPDATE_GROUP_BULLETIN, fName + "发布了群公告");
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群设置接口 群头像设置（后台接口需要验证用户是否为群主或管理员）")
    @RequestMapping("/groupHeadUrlSet")
    @ResponseBody
    public ResponseData groupHeadUrlSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long destId,
                                        @ApiParam("群头像") @RequestParam String headUrl) {
        if (StringUtils.isBlank(headUrl)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);

        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, destId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs group = groupService.selectByPrimaryKey(destId);
            group.setHeadUrl(headUrl);
            int res = groupService.updateByPrimaryKeySelective(group);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(group);
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                //推送一条系统消息到群里 表示群主修改了群头像
                //组装content
                JSONObject json = new JSONObject();
                json.put("headUrl", headUrl);
                json.put("info", fName + "修改了群头像");
                json.put("userId", userId);
                json.put("memberId", groupMember.getId());
                json.put("name", fName);
                json.put("textTip", "name修改了群头像");
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, destId, device, MSG_TYPE_UPDATE_GROUP_HEAD, json.toJSONString());
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    @ApiOperation("群设置接口 阅后即焚设置（后台接口需要验证用户是否为群主或管理员） 弃用")
    @RequestMapping("/groupEphemeralChatSet")
    @ResponseBody
    public ResponseData groupEphemeralChatSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long destId,
                                              @ApiParam("是否启动阅后即焚，0禁用，1启用") @RequestParam int ephemeralChat, @ApiParam("消息保留时间（N秒）") @RequestParam int ephemeralChatTime) {
        long userId = CommonUtils.getUserIdByToken(token);
        //判断设置时间是否为负数
        if (ephemeralChatTime < 0) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, destId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            //设置阅后即焚状态
            ImGroupWithBLOBs record = groupService.selectByPrimaryKey(destId);
            record.setIsEphemeralChat(ephemeralChat);
            record.setEphemeralChatTime(ephemeralChatTime);
            int res = groupService.updateByPrimaryKeySelective(record);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(record);
                //通知
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                //发送通知
                String contentStr;
                int msgType;
                if (ephemeralChat > 0) {
                    //时间转换
                    String time = CommonUtils.getEphemeralChatTime(ephemeralChatTime);
                    contentStr = "设置了" + time + "秒的阅后即焚";
                    msgType = MSG_TYPE_SET_GROUP_EPHEMERALCHAT;
                } else {
                    contentStr = "取消了阅后即焚";
                    msgType = MSG_TYPE_CANCEL_GROUP_EPHEMERALCHAT;
                }
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, destId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群设置接口 群主转让设置（群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/transferGroup")
    @ResponseBody
    public ResponseData transferGroup(
            @RequestHeader String token,
            @ApiParam("群id") @RequestParam(defaultValue = "0") long groupId,
            @ApiParam("新群主的群成员id") @RequestParam(defaultValue = "0") long receiverUserId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(member)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //查找转让对象的对应的成员
        ImGroupMember member1 = groupMemberService.selectByPrimaryKey(receiverUserId);
        if (!CheckUtil.checkCanSet(member1, userId)) {
            return YxUtil.createFail(operationErrorReturn);
        }
        //判断管理员人数
        int managerCount = cacheService.countManagers(groupId);
        if (CheckUtil.checkRoleManager(member1)) {
            managerCount = managerCount - 1;
        }
        if (!CheckUtil.checkManagerNum(managerCount)) {
            return YxUtil.createFail(transferManagerNumReturn);
        }
        //实际操作
        try {
            int device = CommonUtils.getDevTypeByToken(token);
            groupService.transferGroup(member, member1, device);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);

        }
    }

    @ApiOperation("群设置接口（后台接口需要验证用户是否为群主或管理员）  群二维码设置接口")
    @RequestMapping("/groupQRCodeSet")
    @ResponseBody
    public ResponseData groupQRCodeSet(@RequestHeader String token,
                                       @ApiParam("群id") @RequestParam long groupId,
                                       @ApiParam("二维码有效日期（yyyy-mm-dd hh24:mi）") @RequestParam String expiryTime,
                                       @ApiParam("二维码最大使用次数（N次）") @RequestParam int effectiveTime) {
        if (CheckUtil.checkQRCodeTimes(effectiveTime)) {
            return YxUtil.createFail(QRCodeCurrentTimesReturn);
        }
        if (CheckUtil.checkQRCodeTime(expiryTime)) {
            return YxUtil.createFail(QRCodeCurrentTimeReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //实际操作
        try {
            //生成二维码
            //保存或修改临时身份表
            ImTempIdentity imTempIdentity = tempIdentityService.selectByIdentityId(groupId);
            if (imTempIdentity == null) {
                imTempIdentity = new ImTempIdentity();
                imTempIdentity.setIdentityType(identityTypeGroup);
                imTempIdentity.setIdentityId(groupId);
                imTempIdentity.setTempType(tempTypeQcode);
            } else {
                //删除tempId缓存
                cacheService.deleteByTempId(imTempIdentity.getTempId());
            }
            imTempIdentity.setCurrentTimes(0);
            imTempIdentity.setExpiryTime(DateUtil.getDate(expiryTime, "yyyy-MM-dd HH:mm"));//失效时间
            imTempIdentity.setEffectiveTimes(effectiveTime);
            String lockKey = LockKey.groupQRCodeKey + imTempIdentity.getIdentityId() + "_" + identityTypeGroup + "_" + imTempIdentity.getTempType();
            imTempIdentity.setTempId(sequenceService.getTempId(lockKey));
            if (imTempIdentity.getId() == null) {
                tempIdentityService.insertSelective(imTempIdentity);
            } else {
                tempIdentityService.updateByPrimaryKeySelective(imTempIdentity);
            }
            //通知到群里
            ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
            String fName = user.getNickName();
            int device = CommonUtils.getDevTypeByToken(token);
            cachedThreadPool.execute(() -> {
                commonService.batchNoticeAllMembers(userId, groupId, device, MSG_TYPE_SET_GROUP_QRCODE, fName + "设置了二维码");
            });
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);

        }
    }

    @ApiOperation("进群邀请确认设置接口（后台接口需要验证用户是否为群主或管理员）")
    @RequestMapping("/inviteConfirmSet")
    @ResponseBody
    public ResponseData groupOtherSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                      @ApiParam("邀请进群确认（0：不需要确认，1需要确认）") @RequestParam Integer inviteConfirm) {
        if (CheckUtil.checkSetVau(inviteConfirm)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
            imGroup.setIsInviteConfirm(inviteConfirm);
            int res = groupService.updateByPrimaryKeySelective(imGroup);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(imGroup);
                //通知
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                String contentStr;
                int msgType;
                if (inviteConfirm.equals(setOn)) {
                    contentStr = "设置了群邀请进群确认";
                    msgType = MSG_TYPE_SET_INVITECONFIRM;
                } else {
                    contentStr = "取消了群邀请进群确认";
                    msgType = MSG_TYPE_CANCEL_INVITECONFIRM;
                }
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("全体禁言设置接口（后台接口需要验证用户是否为群主或管理员）")
    @RequestMapping("/isBannedSet")
    @ResponseBody
    public ResponseData isBannedSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                    @ApiParam("0未禁言1全体禁言") @RequestParam Integer isBanned) {
        long userId = CommonUtils.getUserIdByToken(token);

        if (CheckUtil.checkSetVau(isBanned)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
            imGroup.setIsBanned(isBanned);
            int res = groupService.updateByPrimaryKeySelective(imGroup);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(imGroup);
                //通知
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                String contentStr;
                int msgType;
                if (isBanned.equals(setOn)) {
                    contentStr = "设置了全体禁言";
                    msgType = MSG_TYPE_SET_ALL_BANNED;
                } else {
                    contentStr = "取消了全体禁言";
                    msgType = MSG_TYPE_CANCEL_ALL_BANNED;
                }
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    @ApiOperation("允许群成员相互查看设置接口（群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/isViewFriendSet")
    @ResponseBody
    public ResponseData isViewFriendSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                        @ApiParam("允许群成员相互查看 1可以查看群成员0不能查看群成员") @RequestParam Integer isViewFriend) {
        long userId = CommonUtils.getUserIdByToken(token);
        if (CheckUtil.checkSetVau(isViewFriend)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
            imGroup.setIsViewFriend(isViewFriend);
            int res = groupService.updateByPrimaryKeySelective(imGroup);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(imGroup);
                //通知
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                String contentStr;
                int msgType;
                if (isViewFriend.equals(setOn)) {
                    contentStr = "允许群成员相互查看";
                    msgType = MSG_TYPE_ALLOW_VIEW_MEMBERS;
                } else {
                    contentStr = "禁止群成员相互查看";
                    msgType = MSG_TYPE_NOT_ALLOW_VIEW_MEMBERS;
                }
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    @ApiOperation("截屏通知设置接口（群主，后台接口需要验证用户是否为群主或管理员）")
    @RequestMapping("/screenshotTipSet")
    @ResponseBody
    public ResponseData screenshotTipSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                         @ApiParam("截屏通知，0不通知，1通知）") @RequestParam Integer screenshotTip) {
        long userId = CommonUtils.getUserIdByToken(token);
        if (CheckUtil.checkSetVau(screenshotTip)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
            imGroup.setIsScreenshotTip(screenshotTip);
            int res = groupService.updateByPrimaryKeySelective(imGroup);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(imGroup);
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                String contentStr;
                int msgType;
                if (screenshotTip.equals(setOn)) {
                    contentStr = "设置了截屏提醒";
                    msgType = MSG_TYPE_SET_SCREENSHOTTIP;
                } else {
                    contentStr = "取消了截屏提醒";
                    msgType = MSG_TYPE_CANCEL_SCREENSHOTTIP;
                }
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    //通知
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("匿名聊天设置接口（群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/isAnonymousSet")
    @ResponseBody
    public ResponseData isAnonymousSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                       @ApiParam("是否可以进行匿名聊天 0不允许1允许）") @RequestParam Integer isAnonymous) {
        if (CheckUtil.checkSetVau(isAnonymous)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
            imGroup.setIsAnonymous(isAnonymous);
            int res = groupService.updateByPrimaryKeySelective(imGroup);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(imGroup);
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                String contentStr;
                int msgType;
                if (isAnonymous.equals(setOn)) {
                    contentStr = "允许了匿名聊天";
                    msgType = MSG_TYPE_SET_ANONYMOUS;
                } else {
                    contentStr = "取消了匿名聊天";
                    msgType = MSG_TYPE_CANCEL_ANONYMOUS;
                }

                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    //通知
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群成员互加设置接口（群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/isFriendEachOtherSet")
    @ResponseBody
    public ResponseData isFriendEachOtherSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                             @ApiParam("禁止群成员互加，0禁用，1启用）") @RequestParam Integer isFriendEachOther) {
        long userId = CommonUtils.getUserIdByToken(token);
        if (CheckUtil.checkSetVau(isFriendEachOther)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
            imGroup.setIsFriendEachOther(isFriendEachOther);
            int res = groupService.updateByPrimaryKeySelective(imGroup);
            if (res > 0) {
                //更新群缓存
                cacheService.updateGroupCache(imGroup);
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                String fName = user.getNickName();
                String contentStr;
                int msgType;
                if (isFriendEachOther.equals(setOn)) {
                    contentStr = "禁止了群成员互加";
                    msgType = MSG_TYPE_NOT_ALLOW_ADD_MEMBERS;
                } else {
                    contentStr = "允许了群成员互加";
                    msgType = MSG_TYPE_ALLOW_ADD_MEMBERS;
                }
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    //通知
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群管理员接口 群管理员查询接口 （群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/queryGroupManagerList")
    @ResponseBody
    public ResponseData queryGroupManagerList(@RequestHeader String token,
                                              @ApiParam("群id") @RequestParam Long groupId,
                                              @ApiParam("群管理员昵称") @RequestParam(defaultValue = "") String name) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //实际操作
        try {
            List<Map<String, Object>> list = new ArrayList<>();
            Map<String, Object> map = new HashMap();
            map.put("groupId", groupId);
            if (StringUtils.isNotBlank(name)) {
                name = "%" + name + "%";
                map.put("markName", name);
                list = userService.queryGroupManagers(map);
            } else {
                list = userService.queryGroupManagers(map);
            }
            return YxUtil.createSuccessDataNoFilter(list);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群管理员接口 群管理员设置取消接口 （群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/groupManagerSet")
    @ResponseBody
    public ResponseData groupManagerSet(@RequestHeader String token,
                                        @ApiParam("群id") @RequestParam Long groupId,
                                        @ApiParam("群管理员id") @RequestParam Long destId,
                                        @ApiParam("isManager=2设为管理员，isManager=3取消管理") @RequestParam Integer isManager) {
        long userId = CommonUtils.getUserIdByToken(token);
        if (isManager == null || !(isManager.equals(groupManagerSet) || isManager.equals(groupManagerCancel))) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        int countMembers = cacheService.countManagers(groupId);
        if (isManager.equals(groupManagerSet) && countMembers > managerMaxNum) {
            return YxUtil.createFail(managersNumReturn);
        }
        ImGroupMember member = groupMemberService.selectByPrimaryKey(destId);
        if (!CheckUtil.checkCanSet(member, userId)) {
            return YxUtil.createFail(operationErrorReturn);
        }
        //实际操作
        try {
            member.setRole(isManager);
            int res = groupMemberService.updateByPrimaryKeySelective(member);
            if (res > 0) {
                //更新群成员缓存
                cacheService.updateGroupMemberCache(member);
                //更新群管理员缓存
                cacheService.updateGroupMemberRoleCache(member);
                int device = CommonUtils.getDevTypeByToken(token);
                //组织content
                JSONObject jsonObject = new JSONObject();
                Long[] longs = new Long[]{destId};
                Long[] longs1 = new Long[0];
                if (isManager.equals(groupManagerSet)) {
                    jsonObject.put("managers", longs);
                    jsonObject.put("notManagers", longs1);
                } else {
                    jsonObject.put("managers", longs1);
                    jsonObject.put("notManagers", longs);
                }
                cachedThreadPool.execute(() -> {
                    //通知
                    commonService.batchNoticeAllMembers(userId, groupId, device, MSG_TYPE_GROUP_MANAGER_SET, jsonObject.toJSONString());
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);

        }
    }

    @ApiOperation("群管理员接口 群管理员批量设置取消接口 （群主，后台接口需要验证用户是否为群主）")
    @RequestMapping("/groupManagerListSet")
    @ResponseBody
    public ResponseData groupManagerListSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                            @ApiParam("群管理员ids") @RequestParam(required = false) String manager,
                                            @ApiParam("非群管理员ids") @RequestParam(required = false) String notManager) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //过滤重复的数据
        Set<Long> managers = YxUtil.splitSet(manager);
        //过滤重复的数据
        Set<Long> notManagers = YxUtil.splitSet(notManager);
        if (managers.size() > managerMaxNum || notManagers.size() > managerMaxNum) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //先过滤不是群成员的
        Set<Long> setDBManagers = cacheService.selectAllManagersId(groupId);
        if (setDBManagers.size() < notManagers.size()) {
            return YxUtil.createFail(operationErrorReturn);
        }
        Set<Long> setDBMembers = cacheService.selectAllMemberId(groupId);
        //先取交集 传入的是群成员的
        managers.retainAll(setDBMembers);
        //取差集 传入的已经是群管理员的
        managers.removeAll(setDBManagers);
        //取交集 传入的是管理员的
        notManagers.retainAll(setDBManagers);
        //检查人数是否超过
        if ((setDBManagers.size() - notManagers.size() + managers.size()) > managerMaxNum) {
            return YxUtil.createFail(managersNumReturn);
        }
        //实际操作
        if (managers.size() == 0 && notManagers.size() == 0) {
            return YxUtil.createFail(paramErrorReturn);
        }
        try {
            int device = CommonUtils.getDevTypeByToken(token);
            groupService.managerListSet(groupId, managers, notManagers, userId, device);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群二维码接口 群二维码查询接口")
    @RequestMapping("/queryGroupQRCode")
    @ResponseBody
    public ResponseData queryGroupQRCode(@RequestHeader String token,
                                         @ApiParam("群id") @RequestParam long groupId) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (groupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        try {
            ImTempIdentity imTempIdentity = tempIdentityService.selectByIdentityId(groupId);
            if (imTempIdentity == null || imTempIdentity.getIdentityId() < 0) {
                return YxUtil.createFail(QRCodeManualExpiredReturn);
            }
            if (!CheckUtil.checkQRCodeExpiryTime(imTempIdentity)) {
                return YxUtil.createFail(QRCodeEndTimeReturn);
            }
            if (!CheckUtil.checkQRCodeCurrentTimes(imTempIdentity)) {
                return YxUtil.createFail(QRCodeEndUseReturn);
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", QRCodeGType);
            jsonObject.put("data", imTempIdentity.getTempId());
            String QRCode = QrCodeCreateUtil.createQrCode(JSON.toJSONString(jsonObject));
            Map<String, Object> data = new HashMap<>();
            data.put("QRCode", QRCode);
            data.put("expiryTime", DateUtil.getTimeString(imTempIdentity.getExpiryTime(), "yyyy-MM-dd HH:mm"));
            data.put("effectiveTimes", imTempIdentity.getEffectiveTimes());
            data.put("currentTimes", imTempIdentity.getCurrentTimes());
            data.put("leftTimes", imTempIdentity.getEffectiveTimes() - imTempIdentity.getCurrentTimes());
            return YxUtil.createSuccessData(data);
        } catch (IOException | WriterException e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation(" 群成员接口 群成员列表查询接口")
    @RequestMapping("/groupMemberList")
    @ResponseBody
    public ResponseData groupMemberList(@RequestHeader String token,
                                        @ApiParam("群id") @RequestParam Long groupId,
                                        @ApiParam("群名片") @RequestParam(defaultValue = "") String name) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (groupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        if (!CheckUtil.checkGroupCanViewFriend(groupService, groupId)) {
            return YxUtil.createFail(canNotSeeGroupMemberInfoReturn);
        }
        //实际操作
        try {
            List<Map<String, Object>> list = new ArrayList<>();
            Map<String, Object> map = new HashMap();
            map.put("groupId", groupId);
            if (StringUtils.isNotBlank(name)) {
                name = "%" + name + "%";
                map.put("markName", name);
                list = userService.queryGroupMembers(map);
            } else {
                list = userService.queryGroupMembers(map);
            }
            return YxUtil.createSuccessDataNoFilter(list);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation(" 群成员接口 群成员明细查询接口")
    @RequestMapping("/groupMemberDetail")
    @ResponseBody
    public ResponseData groupMemberDetail(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId, @ApiParam("群成员id") @RequestParam Long destId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        ImGroupMember imGroupMember = groupMemberService.selectByPrimaryKey(destId);
        if (imGroupMember == null) {
            return YxUtil.createFail(destNotInGroupReturn);
        }
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(imGroupMember.getUserId());
        if (imUser == null) {
            return YxUtil.createFail(destAccountInvalidReturn);
        }
        return getMemberDetail(userId, imGroupMember, imUser, token, member);
    }

    @ApiOperation("根据用户ID查询群成员明细查询接口")
    @RequestMapping("/groupMemberDetailByUserId")
    @ResponseBody
    public ResponseData groupMemberDetailByUserId(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId, @ApiParam("用户id") @RequestParam Long destId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        ImGroupMember imGroupMember = cacheService.getMember(destId, groupId);
        if (imGroupMember == null) {
            return YxUtil.createFail(destNotInGroupReturn);
        }
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(imGroupMember.getUserId());
        if (imUser == null) {
            return YxUtil.createFail(destAccountInvalidReturn);
        }
        return getMemberDetail(userId, imGroupMember, imUser, token, member);

    }

    private ResponseData getMemberDetail(long userId, ImGroupMember imGroupMember, ImUserWithBLOBs imUser, String token, ImGroupMember member) {
        try {
            ImFriend imFriend = cacheService.getFriend(userId, imGroupMember.getUserId());
            HashMap<String, Object> data = new HashMap<>();
            data.put("id", imGroupMember.getId());
            data.put("sex", imUser.getSex());
            data.put("nickName", imUser.getNickName());
            data.put("headUrl", imUser.getHeadUrl());
            data.put("markName", CommonUtils.ifNullValue(imGroupMember.getMarkName()));
            data.put("sign", CommonUtils.ifNullValue(imUser.getSign()));
            data.put("search_group_id", imUser.getSearchGroupId());

            //私密模式下面才返回私密好友ID
            if (imFriend != null && imFriend.getIsFriend().equals(isFriendBoth)) {
                if (imFriend.getIsSecretFriend().equals(setOn)
                        && !CheckUtil.checkSecretModel(token)) {
                    //非私密模式下不返回私密好友ID
                    data.put("friendId", "");
                    //非私密模式下不能通过群聊添加私密好友
                    data.put("search_group_id", setOff);
                } else {//普通好友和私密模式下，返回好友ID
                    data.put("friendId", imFriend.getFriendId());
                }
            } else {//不是好友
                data.put("friendId", "");
            }

            if (imGroupMember.getUserId() == userId) {
                data.put("isCurrentUser", isCurrentUser);
            } else {
                data.put("isCurrentUser", notCurrentUser);
            }
            //黑名单
            if (imFriend != null) {
                data.put("isBlack", imFriend.getIsBlack());
            } else {
                data.put("isBlack", unBlack);
            }
            //禁言 需要判断当前用户是不是群管理员
            if (member.getRole() < groupRoleMember) {
                if (imGroupMember.getIsBanned() > System.currentTimeMillis()) {
                    data.put("isBanned", banned);
                } else {
                    data.put("isBanned", notBanned);
                }
            } else {
                data.put("isBanned", notShowBanned);
            }
            ImGroup imGroup = groupService.selectByPrimaryKey(imGroupMember.getGroupId());
            data.put("isFriendEachOther", imGroup.getIsFriendEachOther());
            return YxUtil.createSuccessData(data);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation(" 群成员接口 群成员权限设置接口（群主、管理员，需要验证成员身份）")
    @RequestMapping("/memberAuthoritySet")
    @ResponseBody
    public ResponseData memberAuthoritySet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                           @ApiParam("群成员id") @RequestParam Long destId,
                                           @ApiParam("-1永久禁言，0取消禁言，禁言时间分钟数") @RequestParam(required = false) Integer isBanned,
                                           @ApiParam("是否黑名单0否1是") @RequestParam(defaultValue = "") String isBlack) {
        if (StringUtils.isBlank(isBlack) && isBanned < foreverBanned - 1) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //查找被设置的对应的成员
        ImGroupMember member = groupMemberService.selectByPrimaryKey(destId);
        if (!CheckUtil.checkCanSet(member, userId, groupMember.getRole())) {
            return YxUtil.createFail(operationErrorReturn);
        }
        try {
            //禁言 黑名单设置
            String contentStr;
            Long bannedTime;
            int msgType;
            int count;
            if (StringUtils.isBlank(isBlack)) {
                //永久禁言
                if (isBanned.equals(foreverBanned)) {
                    bannedTime = foreverBannedTime;
                    contentStr = "设置了永久禁言";
                    msgType = MSG_TYPE_SET_MEMBER_FOREVER_BANNED;
                } else if (isBanned.equals(unBanned)) {
                    bannedTime = unBannedTime;
                    contentStr = "取消了禁言";
                    msgType = MSG_TYPE_SET_MEMBER_MINUTE_BANNED;
                } else {
                    bannedTime = BanedTimeSet.setBanned(isBanned);
                    contentStr = "设置了" + isBanned + "分钟的禁言";
                    msgType = MSG_TYPE_CANCEL_MEMBER_BANNED;
                }
                member.setIsBanned(bannedTime);
                count = groupMemberService.updateByPrimaryKeySelective(member);
            } else {
                if (isBlack.equals(setOff.toString())) {
                    contentStr = "取消了黑名单";
                    msgType = MSG_TYPE_CANCEL_MEMBER_BLACK;
                } else if (isBlack.equals(setOn.toString())) {
                    contentStr = "设置了黑名单";
                    msgType = MSG_TYPE_SET_MEMBER_BLACK;
                } else {
                    return YxUtil.createFail(paramErrorReturn);
                }
                member.setIsBlack(Integer.valueOf(isBlack));
                count = groupMemberService.updateByPrimaryKeySelective(member);
            }
            if (count > 0) {
                //更新群成员缓存
                cacheService.updateGroupMemberCache(member);
                ImUserWithBLOBs formUser = cacheService.selectByPrimaryKeyUser(userId);
                String fromName = formUser.getNickName();
                ImUserWithBLOBs destUser = cacheService.selectByPrimaryKeyUser(member.getUserId());
                String destName = destUser.getNickName();
                int device = CommonUtils.getDevTypeByToken(token);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fromName + "对" + destName + contentStr);
                });
            }
            return getSetRes(count);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    @ApiOperation("群成员接口 群成员权限设置接口 黑名单设置（群主、管理员，需要验证成员身份）")
    @RequestMapping("/memberAuthorityBlackSet")
    @ResponseBody
    public ResponseData memberAuthorityBlackSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                                @ApiParam("群成员id") @RequestParam Long destId,
                                                @ApiParam("是否黑名单0否1是") @RequestParam Integer turn) {
        if (CheckUtil.checkSetVau(turn)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);

        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //查找被设置的对应的成员
        ImGroupMember member = groupMemberService.selectByPrimaryKey(destId);
        if (!CheckUtil.checkCanSet(member, userId, groupMember.getRole())) {
            return YxUtil.createFail(operationErrorReturn);
        }
        //实际操作
        try {
            member.setIsBlack(turn);
            int res = groupMemberService.updateByPrimaryKeySelective(member);
            if (res > 0) {
                //更新群成员缓存
                cacheService.updateGroupMemberCache(member);
                ImUserWithBLOBs formUser = cacheService.selectByPrimaryKeyUser(userId);
                String fromName = formUser.getNickName();
                ImUserWithBLOBs destUser = cacheService.selectByPrimaryKeyUser(member.getUserId());
                String destName = destUser.getNickName();
                int device = CommonUtils.getDevTypeByToken(token);
                String contentStr;
                int msgType;
                if (turn.equals(setOff)) {
                    contentStr = "拉出了黑名单";
                    msgType = MSG_TYPE_CANCEL_MEMBER_BLACK;
                } else if (turn.equals(setOn)) {
                    contentStr = "拉入了黑名单";
                    msgType = MSG_TYPE_SET_MEMBER_BLACK;
                } else {
                    return YxUtil.createFail(paramErrorReturn);
                }
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembers(userId, groupId, device, msgType, fromName + "将" + destName + contentStr);
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群成员接口 群成员权限设置接口 禁言设置（群主、管理员，需要验证成员身份）")
    @RequestMapping("/memberAuthorityBannedSet")
    @ResponseBody
    public ResponseData memberAuthorityBannedSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                                 @ApiParam("群成员id") @RequestParam Long destId,
                                                 @ApiParam("-1永久禁言，0取消禁言，禁言时间分钟数") @RequestParam Integer isBanned) {
        if (isBanned < foreverBanned - 1) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //查找被设置的对应的成员
        ImGroupMember member = groupMemberService.selectByPrimaryKey(destId);
        if (!CheckUtil.checkCanSet(member, userId, groupMember.getRole())) {
            return YxUtil.createFail(operationErrorReturn);
        }
        //实际操作
        try {
            String contentStr;
            int msgType;
            Long bannedTime;
            //永久禁言
            if (isBanned.equals(foreverBanned)) {
                bannedTime = foreverBannedTime;
                contentStr = "设置了永久禁言";
                msgType = MSG_TYPE_SET_MEMBER_FOREVER_BANNED;
            } else if (isBanned.equals(unBanned)) {
                bannedTime = unBannedTime;
                contentStr = "取消了禁言";
                msgType = MSG_TYPE_SET_MEMBER_MINUTE_BANNED;
            } else {
                bannedTime = BanedTimeSet.setBanned(isBanned);
                String time = CommonUtils.getBannedTime(isBanned);
                contentStr = "设置了" + time + "的禁言";
                msgType = MSG_TYPE_CANCEL_MEMBER_BANNED;
            }
            member.setIsBanned(bannedTime);
            int res = groupMemberService.updateByPrimaryKeySelective(member);
            if (res > 0) {
                //更新群成员缓存
                cacheService.updateGroupMemberCache(member);
                ImUserWithBLOBs formUser = cacheService.selectByPrimaryKeyUser(userId);
                ImUserWithBLOBs destUser = cacheService.selectByPrimaryKeyUser(member.getUserId());
                //消息组装
                JSONObject json = new JSONObject();
                json.put("groupId", groupId);
                json.put("fromName", formUser.getNickName());
                json.put("fromUserId", userId);
                json.put("fromMemberId", groupMember.getId());
                json.put("destName", destUser.getNickName());
                json.put("destUserId", member.getUserId());
                json.put("destMemberId", member.getId());
                json.put("bannedTime", bannedTime);
                json.put("textTip", "fromName对destName" + contentStr);
                cachedThreadPool.execute(() -> {
                    commonService.batchNoticeAllMembersSave(userId, groupId, noDevice, msgType, json.toJSONString());
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                return YxUtil.createFail(operationErrorReturn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    @ApiOperation(" 群成员接口 群成员退出接口")
    @RequestMapping("/quitGroup")
    @ResponseBody
    public ResponseData quitGroup(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户信息对应的角色
        ImGroupMember imGroupMember = cacheService.getMember(userId, groupId);
        if (imGroupMember == null || CheckUtil.checkRoleOwner(imGroupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            int device = CommonUtils.getDevTypeByToken(token);
            groupService.updateQuitGroup(device, imGroupMember);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    /**
     * 20201124
     *
     * @param token
     * @param groupId
     * @param inviteIds
     * @param requestContent
     * @return
     */
//    @ApiOperation("群成员邀请接口 他人邀请")
//    @RequestMapping("/inviteJoinGroup")
//    @ResponseBody
//    public ResponseData inviteJoinGroup(@RequestHeader String token,
//                                        @ApiParam("群id") @RequestParam Long groupId,
//                                        @ApiParam("被邀请人ids") @RequestParam String inviteIds,
//                                        @ApiParam("请求群主同意信息") @RequestParam(required = false,defaultValue = "") String requestContent) {
//        long userId = CommonUtils.getUserIdByToken(token);
//        //查找当前用户信息对应的角色
//        ImGroupMember imGroupMember = commonService.getMember(userId, groupId);
//        if (imGroupMember == null) {
//            return YxUtil.createFail(notInGroupReturn);
//        }
//        //过滤重复的id
//        Set<Long> memberSet=YxUtil.splitSet(inviteIds);
//        //每次最多邀请人数
//        if (!CheckUtil.checkInviteNum(memberSet.size())) {
//            return YxUtil.createFail(maxInviteNumReturn);
//        }
//        //判断是否会超过上限
//        //群里总人数
//        int allCount = commonService.countMembers(groupId);
//        if (!CheckUtil.checkGroupNum(allCount + memberSet.size())) {
//            return YxUtil.createFail(groupNumReturn);
//        }
//        //获取双向好友set
//        Set<Long> friendSet=commonService.getFriendsSet(userId);
//        //去除不是好友的
//        memberSet.retainAll(friendSet);
//        //获取群成员
//        Set<Long> groupMemberSet = commonService.getGroupMemberSet(groupId);
//        //去除已经在群里的
//        memberSet.removeAll(groupMemberSet);
//        if ( 0 == memberSet.size() ) {
//            return YxUtil.createFail(paramErrorReturn);
//        }
//        //判断是否非法操作
//        for (long friendId : memberSet) {
//            //判断是否发送过邀请了
//            ImUserRequestAudit imUserRequestAudit = commonService.getUserRequest(friendId, userId, groupId);
//            if (imUserRequestAudit != null) {
//                //判断是不是已经请求是不是删除
//                ImUserRequestDeleted imUserRequestDeleted = commonService.getUserRequestDel(friendId, imUserRequestAudit.getId());
//                if (imUserRequestDeleted == null) {
//                    memberSet.remove(friendId);
//                }
//            }
//        }
//        if ( 0==memberSet.size() ) {
//            return YxUtil.createFail(alreadyInviteReturn);
//        }
//        //这里的的memberSet 是真正要邀请的
//        Set<Long> outSet=new HashSet<>();
//        ImGroupWithBLOBs group=groupService.selectByPrimaryKey(imGroupMember.getGroupId());
//        //群主
//        ImGroupMember owner=commonService.getOwner(imGroupMember.getGroupId());
//        //判断群是否开启了进群确认
//        //开 邀请人是管理员或群主或者关
//        int flag;
//        List<ImGroupMember> inMembers = new ArrayList<>();
//        List<ImUserSession> inSessions = new ArrayList<>();
//        //如果是flag=1 outRequestAudits则是无法进群的邀请
//        //如果是flag=0 outRequestAudits则是发送给群主审核的邀请
//        List<ImUserRequestAudit> outRequestAudits = new ArrayList<>();
//        if ((group.getIsInviteConfirm().equals(setOn) && (imGroupMember.getRole().equals(groupRoleOwner) || imGroupMember.getRole().equals(groupRoleManager))) || group.getIsInviteConfirm().equals(setOff)) {
//            flag=setOn;
//            //分开直接进群和无法进群的
//            commonService.getInOutIdsByUser(memberSet,outSet);
//            //组织 批量操作的数据 批量添加群成员 批量添加会话
//            //可以直接进群的
//            for(Long uId:memberSet){
//                commonService.addGroupMemberList(inMembers,userId,groupId,uId,groupRoleMember);
//                commonService.addSessionList(inSessions,uId,groupId);
//            }
//            //无法直接进群的
//            for(Long uId:outSet){
//                commonService.addUserRequestList(outRequestAudits,userId,uId,groupId,uId,groupType, requestContent, requestValidStatus, waitStatus);
//            }
//        } else{
//            //添加申请列表
//            flag=setOff;
//            for(long uId:memberSet){
//                commonService.addUserRequestList(outRequestAudits,userId,uId,groupId,owner.getUserId(),groupType, "", requestValidStatus, waitOwnerStatus);
//            }
//        }
//
//        try {
//            int device = CommonUtils.getDevTypeByToken(token);
//            groupService.inviteJoinGroup( inMembers, inSessions,  outRequestAudits,  groupId,  userId, memberSet, outSet, flag);
//            return YxUtil.createSimpleSuccess(operationSuccessReturn);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return YxUtil.createFail(operationErrorReturn);
//        }
//    }
    @ApiOperation("群成员邀请接口 他人邀请")
    @RequestMapping("/inviteJoinGroup")
    @ResponseBody
    public ResponseData inviteJoinGroup(@RequestHeader String token,
                                        @ApiParam("群id") @RequestParam Long groupId,
                                        @ApiParam("被邀请人ids") @RequestParam String inviteIds,
                                        @ApiParam("请求群主同意信息") @RequestParam String requestContent) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户信息对应的角色
        ImGroupMember imGroupMember = cacheService.getMember(userId, groupId);
        if (imGroupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        //过滤重复的数据
        Set<Long> memberIds = YxUtil.splitSet(inviteIds);
        //每次邀请最大人数
        if (!CheckUtil.checkInviteNum(memberIds.size())) {
            return YxUtil.createFail(maxInviteNumReturn);
        }
        //获取对应的好友
        Set<Long> friendIdSet = cacheService.getFriendsSet(userId);
        //取交集
        memberIds.retainAll(friendIdSet);
        int device = CommonUtils.getDevTypeByToken(token);
        //锁
        String lockKey = LockKey.inviteJoinGroupKey + groupId + "_" + userId;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        //获取群成员
        Set<Long> groupMemberSet = cacheService.getGroupMemberSet(groupId);
        memberIds.removeAll(groupMemberSet);
        //如果过滤了没有数据
        if (0 == memberIds.size()) {
            return YxUtil.createFail(operationErrorReturn);
        }
        //判断总人数
        int allCount = cacheService.countMembers(groupId);
        if (!CheckUtil.checkGroupNum(allCount + memberIds.size())) {
            return YxUtil.createFail(groupNumReturn);
        }
        //判断是否非法操作
        for (long friendId : memberIds) {
            //判断是否发送过邀请了
            ImUserRequestAudit imUserRequestAudit = cacheService.getUserRequest(friendId, userId, groupId);
            if (imUserRequestAudit != null) {
                //判断是不是已经请求是不是删除
                ImUserRequestDeleted imUserRequestDeleted = commonService.getUserRequestDel(friendId, imUserRequestAudit.getId());
                if (imUserRequestDeleted == null) {
                    memberIds.remove(friendId);
                }
            }
        }
        //如果过滤了没有数据
        if (0 == memberIds.size()) {
            return YxUtil.createFail(alreadyInviteReturn);
        }
        try {
            groupService.inviteJoinGroup(memberIds, groupId, userId, device, requestContent);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }
    }

    @ApiOperation("群二维码接口 通过二维码主动扫码进群")
    @RequestMapping("/joinGroupByQRCode")
    @ResponseBody
    public ResponseData joinGroupByQRCode(@RequestHeader String token,
                                          @ApiParam("临时群id") @RequestParam long tempId,
                                          @ApiParam("请求群主同意信息") @RequestParam(required = false) String requestContent) {
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        String lockKey = LockKey.joinGroupByQrcodeKey + tempId + "_" + userId;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {
            ImTempIdentity imTempIdentity = tempIdentityService.selectByTempId(tempId);
            if (imTempIdentity == null || imTempIdentity.getIdentityId() < 0) {
                return YxUtil.createFail(QRCodeManualExpiredReturn);
            }
            if (!CheckUtil.checkQRCodeCurrentTimes(imTempIdentity)) {
                return YxUtil.createFail(QRCodeEndUseReturn);
            }
            if (!CheckUtil.checkQRCodeExpiryTime(imTempIdentity)) {
                return YxUtil.createFail(QRCodeEndTimeReturn);
            }
            long groupId = imTempIdentity.getIdentityId();
            //判断群状态是否正常
            ImGroupWithBLOBs group = groupService.selectByPrimaryKey(groupId);
            if (!group.getGroupStatus().equals(groupNormal)) {
                return YxUtil.createFail(groupCanNotInReturn);
            }
            int allCount = cacheService.countMembers(groupId);
            if (!CheckUtil.checkGroupNum(allCount + 1)) {
                return YxUtil.createFail(groupNumReturn);
            }
            //获取对应的群成员信息
            ImGroupMember member = cacheService.getMember(userId, groupId);
            if (member != null) {
                return YxUtil.createFail(inGroupReturn);
            }
            //判断群主有没有设置群审核
            //查找群主信息
            ImGroupMember imGroupMember = cacheService.getOwner(groupId);
            if (CheckUtil.checkGroupNeedConfirm(groupService, groupId)) {
                //需要群主确认
                //判断是否发送过加群申请了
                ImUserRequestAudit imUserRequestAudit = cacheService.getUserRequest(userId, userId, device);
                if (imUserRequestAudit != null) {
                    return YxUtil.createFail(alreadyApplyReturn);
                }
                //给群主发送一个加群申请
                //添加到申请列表
                commonService.addUserRequest(userId, userId, groupId, imGroupMember.getUserId(), QRCodeType, requestContent, requestValidStatus, waitOwnerStatus);
                //通知管理员
                List<Long> list = cacheService.getManagerAndOwner(groupId);
                String userIds = YxUtil.listToString(list);
                cachedThreadPool.execute(() -> {
                    //给群主和管理员发送邀请好友加入群的系统消息
                    commonService.batchNoticeMembers(userIds, userId, groupId, noDevice, MSG_TYPE_INVITE_GROUP_CONFIRM, "");
                });
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            } else {
                //不需要群主确认
                ImGroupMember inMember = commonService.addGroupMember(userId, groupId, userId, groupRoleMember);
                //为加入群的成员创建会话
                commonService.createSession(userId, groupId);
                //用户信息
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                //组装消息
                JSONObject json = new JSONObject();
                json.put("groupId", groupId);
                json.put("groupName", group.getName());
                json.put("groupHeadUrl", group.getHeadUrl());
                //组装消息
                json.put("textTip", "name通过扫描二维码加入了群聊");
                json.put("name", user.getNickName());
                json.put("memberId", inMember.getId());
                json.put("userId", userId);
                //8的消息
                JSONObject jsonObject = new JSONObject();
                jsonObject.putAll(json);
                JSONArray array = new JSONArray();
                JSONObject userJson = new JSONObject();
                userJson.put("name", user.getNickName());
                userJson.put("headUrl", user.getHeadUrl());
                userJson.put("memberId", inMember.getId());
                array.add(userJson);
                jsonObject.put("memberArray", array);
                cachedThreadPool.execute(() -> {
                    //给进群的推一个14 name通过扫描二维码加入了群聊
                    commonService.noticeSingleAndSave(userId, groupId, noDevice, MSG_TYPE_ACCEPT_GROUP, json.toJSONString());
                    //给群里的人推一个8 name通过扫描二维码加入了群聊
                    commonService.batchNoticeAllMembersSave(userId, groupId, noDevice, MSG_TYPE_JOIN_GROUP, jsonObject.toJSONString());
                    //通知被邀请已经进群的ids加入房间
                    chatServerFeign.joinRoomOne(String.valueOf(userId), groupId);

                });
                //修改申请状态
                imTempIdentity.setCurrentTimes(imTempIdentity.getCurrentTimes() + 1);
                tempIdentityService.updateByPrimaryKeySelective(imTempIdentity);
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }

    }

    @ApiOperation("群成员邀请接口 用户进群审核接口（被邀请人）")
    @RequestMapping("/acceptInviteGroupReq")
    @ResponseBody
    public ResponseData acceptInviteGroupReq(@RequestHeader String token,
                                             @ApiParam("是否接受群邀请") @RequestParam Integer isAccept,
                                             @ApiParam("是否当前通知的id") @RequestParam long msgId) {
        if (CheckUtil.checkSetVau(isAccept)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);

        String lockKey = LockKey.acceptInviteGroupKey + msgId + "_" + userId;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        //查找申请列表
        ImUserRequestAudit imUserRequestAudit = userRequestAuditService.selectByPrimaryKey(msgId);
        int allCount = cacheService.countMembers(imUserRequestAudit.getDestId());
        if (!CheckUtil.checkGroupNum(allCount + 1)) {
            return YxUtil.createFail(groupNumReturn);
        }
        //判断群状态是否正常
        ImGroupWithBLOBs group = groupService.selectByPrimaryKey(imUserRequestAudit.getDestId());
        if (!group.getGroupStatus().equals(groupNormal)) {
            return YxUtil.createFail(groupCanNotInReturn);
        }
        //判断是否非法操作
        if (!CheckUtil.checkCanHandleReqAudit(imUserRequestAudit, userId)) {
            return YxUtil.createFail(alreadyHandleReturn);
        }
        //是否已经是群成员
        ImGroupMember member = cacheService.getMember(imUserRequestAudit.getUserId(), imUserRequestAudit.getDestId());
        if (member != null) {
            return YxUtil.createFail(inGroupReturn);
        }
        try {
            groupService.acceptInviteGroupReq(userId, device, isAccept, imUserRequestAudit);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }
    }

    @ApiOperation("群成员邀请接口 用户进群审核接口（群主,管理员）")
    @RequestMapping("/acceptInviteReq")
    @ResponseBody
    public ResponseData acceptInviteReq(@RequestHeader String token,
                                        @ApiParam("是否接受群邀请") @RequestParam Integer isAccept,
                                        @ApiParam("是否当前通知的id") @RequestParam long msgId) {
        if (CheckUtil.checkSetVau(isAccept)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);

        String lockKey = LockKey.acceptInviteReqKey + msgId + "_" + userId + isAccept;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        //查找申请列表
        ImUserRequestAudit imUserRequestAudit = userRequestAuditService.selectByPrimaryKey(msgId);
        long groupId = imUserRequestAudit.getDestId();
        //判断群状态
        ImGroupWithBLOBs group = groupService.selectByPrimaryKey(groupId);
        if (CheckUtil.checkGroupDissolve(group)) {
            return YxUtil.createFail(groupDissolveReturn);
        } else if (CheckUtil.checkGroupFreeze(group)) {
            return YxUtil.createFail(groupFreezeReturn);
        }
        int allCount = cacheService.countMembers(imUserRequestAudit.getDestId());
        if (!CheckUtil.checkGroupNum(allCount + 1)) {
            return YxUtil.createFail(groupNumReturn);
        }
        //判断是否非法操作
        if (!CheckUtil.checkCanHandleReqAudit(imUserRequestAudit)) {
            return YxUtil.createFail(alreadyHandleReturn);
        }
        //对方是否已经是群成员
        ImGroupMember member = cacheService.getMember(imUserRequestAudit.getUserId(), groupId);
        if (member != null) {
            return YxUtil.createFail(destInGroupReturn);
        }
        //查找当前用户信息对应的角色
        ImGroupMember imGroupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(imGroupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        try {
            groupService.acceptInviteReq(userId, device, isAccept, imUserRequestAudit);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }
    }

    @ApiOperation("群聊解散接口（群主，需要验证群主身份）")
    @RequestMapping("/dismissGroup")
    @ResponseBody
    public ResponseData dismissGroup(@RequestHeader String token,
                                     @ApiParam("群id") @RequestParam(defaultValue = "0") long groupId) {
        long userId = CommonUtils.getUserIdByToken(token);
        //获取对应的群成员
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwner(member)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        int device = CommonUtils.getDevTypeByToken(token);
        try {
            groupService.dismissGroup(groupId, userId, device);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }

    }

    @ApiOperation(" 群成员信息修改接口 群备注（个人备注）修改设置接口")
    @RequestMapping("/updateGroupMemberMark")
    @ResponseBody
    public ResponseData updateGroupMemberMark(@RequestHeader String token,
                                              @ApiParam("所在群的id") @RequestParam(defaultValue = "0") long groupId,
                                              @ApiParam("要设置的备注名") @RequestParam(defaultValue = "") String markName) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        int device = CommonUtils.getDevTypeByToken(token);
        try {
            groupService.updateGroupMemberMark(member, markName, device);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    @RequestMapping("/removeFromGroup")
    @ResponseBody
    @ApiOperation("群成员移除接口(需验证群主、群管理员)")
    public ResponseData removeFromGroup(@RequestHeader String token,
                                        @ApiParam("群id") @RequestParam(defaultValue = "0") long groupId,
                                        @ApiParam("群成员id") @RequestParam(defaultValue = "0") long memberId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(member)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //被操作的对象
        ImGroupMember imGroupMember = groupMemberService.selectByPrimaryKey(memberId);
        if (!CheckUtil.checkCanSet(imGroupMember, userId, member.getRole())) {
            return YxUtil.createFail(operationErrorReturn);
        }
        try {
            groupService.removeFromGroup(member, imGroupMember);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @RequestMapping("/removeIdsFromGroup")
    @ResponseBody
    @ApiOperation("群成员批量移除接口(需验证群主、群管理员)")
    public ResponseData removeIdsFromGroup(@RequestHeader String token,
                                           @ApiParam("群id") @RequestParam(defaultValue = "0") long groupId,
                                           @ApiParam("群成员id") @RequestParam(defaultValue = "0") String delIds) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(member)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        //过滤重复的
        Set<Long> memberIds = YxUtil.splitSet(delIds);


        if (member.getRole().equals(groupRoleOwner)) {
            //获取所有成员的member id
            Set<Long> membersSet = cacheService.selectAllMemberId(groupId);
            //移出自己
            membersSet.remove(member.getId());
            //取交集
            memberIds.retainAll(membersSet);
        } else {
            //获取普通成员的member id
            Set<Long> memberSet = cacheService.selectMemberId(groupId);
            //取交集
            memberIds.retainAll(memberSet);
        }
        if (0 == memberIds.size()) {
            //return YxUtil.createFail(paramErrorReturn);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);//2020 12 08 移除群成员，没有入参的时候，先当作成功处理。
        }
        try {
            groupService.removeIdsFromGroup(member, memberIds);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("群组邀请列表接口")
    @RequestMapping("/inviteReqList")
    @ResponseBody
    public ResponseData inviteReqList(@RequestHeader String token,
                                      @ApiParam("好友昵称或者备注") @RequestParam(required = false) String name,
                                      @ApiParam("当前页") @RequestParam(defaultValue = "1") int page,
                                      @ApiParam("每页大小") @RequestParam(defaultValue = "10") int size) {
        long userId = CommonUtils.getUserIdByToken(token);
        PageHelper.startPage(page, size);
        //组装数据
        Page listPage = new Page();
        listPage.setPageNo(page);
        listPage.setPageSize(size);
        List<Map<String, Object>> list = groupService.inviteReqList(userId);
        int totalCount = groupService.countInviteReqList(userId);
        listPage.setTotalCount(totalCount);
        listPage.setList(list);
        return YxUtil.createSuccessData(listPage);
    }

    @ApiOperation("进群确认列表接口")
    @RequestMapping("/confirmReqList")
    @ResponseBody
    public ResponseData confirmReqList(@RequestHeader String token,
                                       @ApiParam("好友昵称或者备注") @RequestParam(required = false) String name,
                                       @ApiParam("当前页") @RequestParam(defaultValue = "1") int page,
                                       @ApiParam("每页大小") @RequestParam(defaultValue = "10") int size) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查询list分页
        PageHelper.startPage(page, size);
        //组装数据
        Page listPage = new Page();
        listPage.setPageNo(page);
        listPage.setPageSize(size);
        List<Map<String, Object>> list = groupService.confirmReqList(userId);
        int totalCount = groupService.countConfirmReqList(userId);
        listPage.setTotalCount(totalCount);
        listPage.setList(list);
        return YxUtil.createSuccessData(listPage);
    }

    @ApiOperation("聊天记录加密设置开启接口")
    @RequestMapping("/memberHistorySetOn")
    @ResponseBody
    public ResponseData memberHistorySetOn(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs user=cacheService.selectByPrimaryKeyUser(userId);
        if(StringUtils.isEmpty(user.getSecretModePwd())){
            return YxUtil.createFail(noSecretModePwdReturn);
        }
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        member.setIsSecretHistory(setOn);
        int count = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (count > 0) {
            memberSet(member, device, userId, groupId, MSG_TYPE_GROUP_MEMBER_CHATHISTORY_SET);
        }
        return getSetRes(count);
    }

    private void memberSet(ImGroupMember member, int device, long userId, long groupId, int type) {
        //更新群成员缓存
        cacheService.updateGroupMemberCache(member);
        //推送自己的其他设备
        ImMessage msg = msgFactory.groupSetNotice(userId, groupId, device, type, "");
        commonService.noticeMyself(msg, device, userId);

    }

    @ApiOperation("聊天记录加密设置关闭接口")
    @RequestMapping("/memberHistorySetOff")
    @ResponseBody
    public ResponseData memberHistorySetOff(@RequestHeader String token,
                                            @ApiParam("群id") @RequestParam Long groupId,
                                            @ApiParam("私密模式密码") @RequestParam String secretPwd) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs imUser = cacheService.selectByPrimaryKeyUser(userId);
        //解密私密模式密码
        secretPwd = RSACipherUtil.decrypt(secretPwd);
        if (CheckUtil.checkSecretPwd(imUser, secretPwd)) {
            return YxUtil.createFail(secretPwdErrorReturn);
        }
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        member.setIsSecretHistory(setOff);
        int count = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (count > 0) {
            memberSet(member, device, userId, groupId, MSG_TYPE_GROUP_MEMBER_CHATHISTORY_OFF);
        }
        return getSetRes(count);
    }


    @ApiOperation("群设置接口 收藏群聊设置")
    @RequestMapping("/collectSet")
    @ResponseBody
    public ResponseData collectSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                   @ApiParam("收藏群聊设置") @RequestParam Integer turn) {
        if (CheckUtil.checkSetVau(turn)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        member.setIsCollect(turn);
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (res > 0) {
            if (setOn.equals(turn)) {
                memberSet(member, device, userId, groupId, MSG_TYPE_COLLECT_GROUP_SET);
            } else {
                memberSet(member, device, userId, groupId, MSG_TYPE_COLLECT_GROUP_OFF);
            }
        }
        return getSetRes(res);
    }

    @ApiOperation("群设置接口 消息显示设置")
    @RequestMapping("/showSessionMessageSet")
    @ResponseBody
    public ResponseData showSessionMessageSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                              @ApiParam("消息显示设置") @RequestParam Integer turn) {
        if (CheckUtil.checkSetVau(turn)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        member.setIsShowSessionMessage(turn);
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (res > 0) {
            if (setOn.equals(turn)) {
                memberSet(member, device, userId, groupId, MSG_TYPE_SHOW_SESSION_MESSAGE_SET);
            } else {
                memberSet(member, device, userId, groupId, MSG_TYPE_SHOW_SESSION_MESSAGE_OFF);
            }
        }
        return getSetRes(res);
    }

    @ApiOperation("群设置接口 私密群聊设置")
    @RequestMapping("/secretGroupSet")
    @ResponseBody
    public ResponseData secretGroupSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                       @ApiParam("私密群聊设置") @RequestParam Integer turn) {
        if (CheckUtil.checkSetVau(turn)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        member.setIsSecretGroup(turn);
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (res > 0) {
            if (setOn.equals(turn)) {
                memberSet(member, device, userId, groupId, MSG_TYPE_SECRET_GROUP_SET);
            } else {
                memberSet(member, device, userId, groupId, MSG_TYPE_SECRET_GROUP_OFF);
            }
            cacheService.removeSecretGroupCache(member);
        }
        return getSetRes(res);
    }

    @ApiOperation("群消息提醒设置接口")
    @RequestMapping("/groupUserSet")
    @ResponseBody
    public ResponseData groupUserSet(@RequestHeader String token,
                                     @ApiParam("群id") @RequestParam(defaultValue = "0") long groupId,
                                     @ApiParam("设置类型 1 群消息提醒设置 2是否为私密群聊3.添加消息显示4.收藏群聊设置") @RequestParam() Integer type,
                                     @ApiParam("开关 开1关0") @RequestParam() Integer turn) {
        if (CheckUtil.checkSetVau(turn)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        switch (type) {
            case 1:
                member.setReceiveTip(turn);
                break;
            case 2:
                member.setIsSecretGroup(turn);
                break;
            case 3:
                member.setIsShowSessionMessage(turn);
                break;
            case 4:
                member.setIsCollect(turn);
                break;
            default:
                break;
        }
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (res > 0) {
            if (setOn.equals(turn)) {
                switch (type) {
                    case 1:
                        memberSet(member, device, userId, groupId, MSG_TYPE_RECEIVETIP_SET);
                        break;
                    case 2:
                        memberSet(member, device, userId, groupId, MSG_TYPE_SECRET_GROUP_SET);
                        break;
                    case 3:
                        memberSet(member, device, userId, groupId, MSG_TYPE_SHOW_SESSION_MESSAGE_SET);
                        break;
                    case 4:
                        memberSet(member, device, userId, groupId, MSG_TYPE_COLLECT_GROUP_SET);
                        break;
                    default:
                        break;
                }
            } else {
                switch (type) {
                    case 1:
                        memberSet(member, device, userId, groupId, MSG_TYPE_RECEIVETIP_OFF);
                        break;
                    case 2:
                        memberSet(member, device, userId, groupId, MSG_TYPE_SECRET_GROUP_OFF);
                        break;
                    case 3:
                        memberSet(member, device, userId, groupId, MSG_TYPE_SHOW_SESSION_MESSAGE_OFF);
                        break;
                    case 4:
                        memberSet(member, device, userId, groupId, MSG_TYPE_COLLECT_GROUP_OFF);
                        break;
                    default:
                        break;
                }
            }
        }
        return getSetRes(res);
    }

    @ApiOperation("群设置接口 群消息提醒设置")
    @RequestMapping("/receiveTipSet")
    @ResponseBody
    public ResponseData receiveTipSet(@RequestHeader String token, @ApiParam("群id") @RequestParam Long groupId,
                                      @ApiParam("群消息提醒设置") @RequestParam Integer turn) {
        if (CheckUtil.checkSetVau(turn)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        member.setReceiveTip(turn);
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        int device = CommonUtils.getDevTypeByToken(token);
        if (res > 0) {
            if (setOn.equals(turn)) {
                memberSet(member, device, userId, groupId, MSG_TYPE_RECEIVETIP_SET);
            } else {
                memberSet(member, device, userId, groupId, MSG_TYPE_RECEIVETIP_OFF);
            }
        }
        return getSetRes(res);
    }

    @ApiOperation("用户群聊列表查询接口")
    @RequestMapping("/queryGroupList")
    @ResponseBody
    public ResponseData queryGroupList(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查询是否开启私密模式
        String isSecret = CommonUtils.getSecretByToken(token);
        List<Map<String, Object>> groupsInfo = readyService.getGroupsInfo(userId, Integer.valueOf(isSecret));
        return YxUtil.createSuccessDataNoFilter(groupsInfo);
    }

    @ApiOperation("群好友查询")
    @RequestMapping("/groupFriendQuery")
    @ResponseBody
    public ResponseData groupFriendQuery(@RequestHeader String token,
                                         @ApiParam("群id") @RequestParam(defaultValue = "0") long groupId) {
        long userId = CommonUtils.getUserIdByToken(token);
        List<Map<String, Object>> list = cacheService.groupFriendQuery(userId, groupId);
        return YxUtil.createSuccessDataNoFilter(list);
    }

    @ApiOperation("群成员拉黑接口")
    @RequestMapping("/blackSet")
    @ResponseBody
    public ResponseData blackSet(@RequestHeader String token,
                                 @ApiParam("群成员id") @RequestParam(defaultValue = "0") long groupMemberId,
                                 @ApiParam("是否黑名单0否1是") @RequestParam(defaultValue = "") Integer isBlack) {
        if (isBlack == null || !(isBlack.equals(setOn) || isBlack.equals(setOff))) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember imGroupMember = groupMemberService.selectByPrimaryKey(groupMemberId);
        long memberId = imGroupMember.getUserId();
        try {
            int count;
            if (isBlack.equals(setOff)) {
                count = commonService.deleteFriend(memberId, userId);
            } else {
                //黑名单
                count = userService.memberBlackSet(userId, memberId);
            }
            return getSetRes(count);
        } catch (Exception e) {
            e.printStackTrace();
            return YxUtil.createFail(operationErrorReturn);
        }
    }


    /**
     * 设置返回
     *
     * @param num int
     * @return int
     */
    private static ResponseData getSetRes(int num) {
        if (num > 0) {
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } else {
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("匿名账号设置")
    @RequestMapping("/setAnonymityAccount")
    @ResponseBody
    public ResponseData setAnonymityAccount(@RequestHeader String token,
                                            @ApiParam("群id") @RequestParam long groupId,
                                            @ApiParam("匿名名称") @RequestParam String anonymousName,
                                            @ApiParam("是否匿名聊天 0不匿名1匿名") @RequestParam int isAnonymous,
                                            @ApiParam("匿名头像") @RequestParam(defaultValue = "") String anonymousHeaderUrl) {
        long userId = CommonUtils.getUserIdByToken(token);


        ImGroupMember imGroupMember = cacheService.getMember(userId, groupId);
        if (imGroupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }

        ImGroupWithBLOBs group = cacheService.selectByPrimaryKeyGroup(groupId);
        if (isAnonymousNo.equals(group.getIsAnonymous())) {
            return YxUtil.createFail(anonymousCloseReturn);
        }
        //添加默认头像
        if (StringUtils.isEmpty(anonymousHeaderUrl)) {
            imGroupMember.setAnonymousHeaderUrl(anonymousHeadUrl);
        } else {
            imGroupMember.setAnonymousHeaderUrl(anonymousHeaderUrl);
        }
        //设置名称
        if (StringUtils.isEmpty(imGroupMember.getAnonymousName())) {
            //如果没有传名称
            if (StringUtils.isEmpty(anonymousName)) {
                return YxUtil.createFail(anonymousNameUnSetReturn);
            } else {
                //如果有直接修改
                imGroupMember.setAnonymousName(anonymousName);
            }
        } else {
            //判端有没有传
            if (StringUtils.isNotEmpty(anonymousName)) {
                //判端是否和数据库的一样
                if (anonymousName.equals(imGroupMember.getAnonymousName())) {
                    imGroupMember.setAnonymousName(imGroupMember.getAnonymousName());
                } else {
                    //不一样就做修改
                    imGroupMember.setAnonymousName(anonymousName);
                }
            } else {
                //没有传就取数据库的
                imGroupMember.setAnonymousName(imGroupMember.getAnonymousName());
            }
        }
        imGroupMember.setIsAnonymous(isAnonymous);
        int res = groupMemberService.updateByPrimaryKeySelective(imGroupMember);
        if (res > 0) {
            //更新群成员缓存
            cacheService.updateGroupMemberCache(imGroupMember);
            int devType = CommonUtils.getDevTypeByToken(token);
            //同步其他设备
            JSONObject json = new JSONObject();
            json.put("isAnonymous", isAnonymous);
            json.put("anonymousName", anonymousName);
            json.put("anonymousHeaderUrl", anonymousHeaderUrl);
            json.put("groupId", groupId);
            json.put("memberId", imGroupMember.getId());
            ImMessage message = msgFactory.groupSetNotice(userId, groupId, devType, MSG_TYPE_SET_MEMBER_ISANONYMOUS, json.toJSONString());
            //推送
            cachedThreadPool.execute(() -> {
                //通知自己其他设备
                chatServerFeign.notifyNew(JSONObject.toJSONString(message), userId);
            });
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } else {
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    @ApiOperation("获取匿名名称")
    @RequestMapping("/getAnonymityName")
    @ResponseBody
    public ResponseData getAnonymityName() {
        JSONObject json = new JSONObject();
        json.put("anonymousName", RandomName.randomName(true, 3));
        json.put("anonymousHeaderUrl", anonymousHeadUrl);
        return YxUtil.createSuccessData(json);
    }

    @ApiOperation("个人群聊状态查询接口")
    @RequestMapping("/getBannedStatus")
    @ResponseBody
    public ResponseData getBannedStatus(@RequestHeader String token, @ApiParam("群id") @RequestParam long groupId) {
        HashMap<String, Object> data = new HashMap<>();
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember member = cacheService.getMember(userId, groupId);
        if (member == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        if (member.getIsBanned() < System.currentTimeMillis()) {
            data.put("bannedStatus", notBanned);
        } else {
            data.put("bannedStatus", banned);
        }
        return YxUtil.createSuccessData(data);
    }

    @ApiOperation("群二维码删除接口")
    @RequestMapping("/deleteQRCode")
    @ResponseBody
    public ResponseData deleteQRCode(@RequestHeader String token, @ApiParam("群id") @RequestParam long groupId) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (!CheckUtil.checkRoleOwnerAndManager(groupMember)) {
            return YxUtil.createFail(withoutPermissionReturn);
        }
        return getSetRes(tempIdentityService.deleteByIdentityId(groupId));
    }

    @ApiOperation("群公告明细查询接口")
    @RequestMapping("/bulletinDetail")
    @ResponseBody
    public ResponseData bulletinDetail(@RequestHeader String token, @ApiParam("群id") @RequestParam long groupId) {
        HashMap<String, Object> data = new HashMap<>();
        long userId = Long.parseLong(Utils.decrypt(token.split("\\|")[1]));
        //查找当前用户的对应的成员
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (groupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        ImGroupWithBLOBs imGroup = groupService.selectByPrimaryKey(groupId);
        data.put("content", CommonUtils.ifNullValue(imGroup.getBulletinContent()));

        String time = "";
        if (imGroup.getBulletinEditTime() != null) {
            time = DateUtil.getTimeString(imGroup.getBulletinEditTime(), "yyyy-MM-dd HH:mm:ss");
        }
        data.put("time", time);
        String name = "";
        String headUrl = "";
        data.put("headUrl", headUrl);
        if (imGroup.getBulletinEditorId() != null) {
            ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(imGroup.getBulletinEditorId());
            data.put("headUrl", user.getHeadUrl());
            ImFriend friend = cacheService.getFriend(userId, imGroup.getBulletinEditorId());
            if (friend != null && !StringUtils.isEmpty(friend.getRemark())) {
                name=friend.getRemark();
            } else {
                name=user.getNickName();
            }
        }
        data.put("name", name);
        return YxUtil.createSuccessData(data);
    }

    @ApiOperation("群组截屏操作通知(通知群主和管理员)")
    @RequestMapping("/screenGroupShotNotify")
    @ResponseBody
    public ResponseData screenGroupShotNotify(@RequestHeader String token,
                                              @ApiParam("群组id") @RequestParam long groupId) {
        ImGroupWithBLOBs imGroup = cacheService.selectByPrimaryKeyGroup(groupId);
        if (imGroup == null) {
            return YxUtil.createFail(noGroupReturn);
        }
        if (imGroup.getIsScreenshotTip().equals(isScreenshotTipClose)) {
            return YxUtil.createFail(screenshotTipCloseReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        if (CheckUtil.checkRoleOwner(groupMember)) {
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        }
        ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(groupMember.getUserId());
        JSONObject json = new JSONObject();
        json.put("userId", user.getId());
        json.put("name", user.getNickName());
        json.put("memberId", groupMember.getId());
        json.put("textTip", "name截取了屏幕");
        int device = CommonUtils.getDevTypeByToken(token);
        if (CheckUtil.checkRoleManager(groupMember)) {
            //通知群主
            ImGroupMember imGroupMember = cacheService.getOwner(groupId);
            // 组装消息
            ImMessage message = msgFactory.groupSetNotice(userId, groupId, device, MSG_TYPE_SCREEN_SHOT_GROUP, json.toJSONString());
            //通知
            message.setDevType(device);
            message.setSystemType(singleSystemType);
            message.setGeoId(imGroupMember.getUserId());
            chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(message), imGroupMember.getUserId());
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        }
        //通知群管理员和群主
        List<Long> list = cacheService.getManagerAndOwner(groupId);
        String userIds = YxUtil.listToString(list);
        cachedThreadPool.execute(() -> {
            commonService.batchNoticeMembersSave(userIds, userId, groupId, device, MSG_TYPE_SCREEN_SHOT_GROUP, json.toJSONString());
        });
        return YxUtil.createSimpleSuccess(operationSuccessReturn);
    }
}
