package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImUser;
import com.wxzd.im.ms.entity.ImUserWithBLOBs;
import com.wxzd.im.ms.parameter.ResponseData;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface ImUserService {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserWithBLOBs record);

    int insertSelective(ImUserWithBLOBs record);

    ImUserWithBLOBs selectByPrimaryKey(Long id);

    ImUserWithBLOBs selectByAccount(String account);

    int countByAccount(String account);

    List<Map<String, Object>> selectAllUser();

    int updateByPrimaryKeySelective(ImUserWithBLOBs record);

    ImUserWithBLOBs updateUserCache(ImUserWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImUserWithBLOBs record);

    int updateByPrimaryKey(ImUser record);

    ImUserWithBLOBs updateProfile(Long userId, String nickName, String city, String province, String name, String sign, String sex, String district, int devType);

    List<Map<String, Object>> queryGroupManagers(Map<String, Object> map);

    List<Map<String, Object>> queryGroupMembers(Map<String, Object> map);

    /**
     * 群成员拉黑
     *
     * @param userId
     * @param destId
     * @return
     */
    int memberBlackSet(long userId, long destId);

    ResponseData doLoginMy(String account, String pwd, Integer device);

    void handleIP(HttpServletRequest request, ImUserWithBLOBs user);

    ResponseData updatePwd(String token, String password);

    void delUser(long userId, long destId, int devType);

    List<Map<String, Object>> friendDetail(long userId, long friendId);

    ImUserWithBLOBs getMyInfo(long userId);

    Map<String, Object> getImFriendInfo(long userId, long friendId);
}