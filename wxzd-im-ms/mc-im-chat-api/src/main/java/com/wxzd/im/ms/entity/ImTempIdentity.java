package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Accessors(chain = true)
@Data
public class ImTempIdentity {
    private Long id;

    private Integer identityType;

    private Long identityId;

    private Long tempId;

    private Integer tempType;

    private Date expiryTime;

    private Integer effectiveTimes;

    private Integer currentTimes;

    public ImTempIdentity() {
    }

    public ImTempIdentity(Integer identityType, Long identityId, Long tempId, Integer tempType, Date expiryTime, Integer effectiveTimes, Integer currentTimes) {
        this.identityType = identityType;
        this.identityId = identityId;
        this.tempId = tempId;
        this.tempType = tempType;
        this.expiryTime = expiryTime;
        this.effectiveTimes = effectiveTimes;
        this.currentTimes = currentTimes;
    }

}