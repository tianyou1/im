package com.wxzd.im.ms.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Page implements Serializable {
    private static final long serialVersionUID = 4435583612697951915L;
    private int pageNo = 1;
    private int pageSize = 20;
    private long totalCount;
    private String orderBy;
    private Page.OrderType orderType;
    private List list = new ArrayList();

    public Page() {
    }

    public int getPageNo() {
        return this.pageNo < 1 ? 1 : this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public Page.OrderType getOrderType() {
        return this.orderType;
    }

    public void setOrderType(Page.OrderType orderType) {
        this.orderType = orderType;
    }

    public long getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalPage() {
        return this.totalCount % (long) this.pageSize == 0L ? this.totalCount / (long) this.pageSize : this.totalCount / (long) this.pageSize + 1L;
    }

    public List getList() {
        return this.list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public static enum OrderType {
        desc,
        asc;

        private OrderType() {
        }
    }
}
