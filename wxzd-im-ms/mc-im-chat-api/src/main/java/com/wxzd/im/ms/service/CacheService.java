package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.wxzd.im.ms.parameter.CacheConstants.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;

@Service
public class CacheService {
    @Autowired
    private ImGroupService groupService;

    @Autowired
    private ImGroupMemberService groupMemberService;

    @Autowired
    private ImFriendService friendService;

    @Autowired
    private ImUserRequestAuditService requestAuditService;

    @Autowired
    private ImUserService userService;

    @Autowired
    private ImTempIdentityService tempIdentityService;

    /**
     * 获取所有成员
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = memberListCache, key = "#groupId", unless = "#result == null or #result.size()==0")
    public List<Long> getAllMembers(long groupId) {
        return groupMemberService.selectAllMemberUserId(groupId, null);
    }


    /**
     * 获取所有成员 Id
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = allMemberSetCache, key = "#groupId", unless = "#result == null or #result.size()==0")
    public Set<Long> selectAllMemberId(long groupId) {
        return groupMemberService.selectAllMemberId(groupId, null);
    }

    /**
     * 获取管理员 Id
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = managerSetCache, key = "#groupId", unless = "#result == null or #result.size()==0")
    public Set<Long> selectAllManagersId(long groupId) {
        return groupMemberService.selectAllMemberId(groupId, groupRoleManager);
    }

    /**
     * 获取普通群成员 Id
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = memberSetCache, key = "#groupId", unless = "#result == null or #result.size()==0")
    public Set<Long> selectMemberId(long groupId) {
        return groupMemberService.selectAllMemberId(groupId, groupRoleMember);
    }

    /**
     * 获取已审核的 私密的群成员列表
     */
    //@Cacheable(cacheNames = managerListCache, key = "#userId",unless = "#result == null")
    public List<ImGroupMember> getAcceptSecretMember(Long userId, Integer isCheck, Integer isSecret) {
        ImGroupMember record = new ImGroupMember();
        record.setUserId(userId);
        record.setIsAccept(isCheck);
        record.setIsSecretGroup(isSecret);
        return groupMemberService.selectByConditions(record);
    }

    /**
     * 获取群主和管理员
     */
    @Cacheable(cacheNames = managerAndOwnerListCache, key = "#groupId", unless = "#result == null or #result.size()==0")
    public List<Long> getManagerAndOwner(Long groupId) {
        return groupMemberService.selectOwnerAndManagers(groupId);
    }


    /**
     * 判断是否发送过邀请
     *
     * @param userId
     * @param fromId
     * @param destId
     * @return
     */
    //@Cacheable(cacheNames = userRequestCache,key ="#p0")
    public ImUserRequestAudit getUserRequest(long userId, long fromId, long destId) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("fromId", fromId);
        map.put("destId", destId);
        return requestAuditService.isSentMsg(map);
    }

    /**
     * 获取群内总人数
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = countMemberCache, key = "#groupId", unless = "#result == null")
    public int countMembers(Long groupId) {
        Map<String, Object> map = new HashMap<>();
        map.put("groupId", groupId);
        return groupMemberService.countMembers(map);
    }

    /**
     * 获取群内管理员总人数
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = countManagerCache, key = "#groupId", unless = "#result == null")
    public int countManagers(Long groupId) {
        Map<String, Object> map = new HashMap<>();
        map.put("role", groupRoleManager);
        map.put("groupId", groupId);
        return groupMemberService.countMembers(map);
    }

    /**
     * 群好友查询
     *
     * @param userId
     * @param groupId
     * @return
     */
    //@Cacheable(cacheNames = groupFriendQueryCache, key = "#userId+':'+#groupId",unless = "#result == null")
    public List<Map<String, Object>> groupFriendQuery(long userId, long groupId) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("groupId", groupId);
        return groupService.groupFriendQuery(map);
    }


    /**
     * 获取对应的好友userId Set列表
     *
     * @param userId
     * @return
     */
    @Cacheable(cacheNames = friendSetCache, key = "#userId", unless = "#result == null or #result.size()==0")
    public Set<Long> getFriendsSet(long userId) {
        return friendService.selectFriendsSet(userId);
    }

    /**
     * 获取对应的群成员userId Set列表
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = groupMemberSetCache, key = "#groupId", unless = "#result == null or #result.size()==0")
    public Set<Long> getGroupMemberSet(long groupId) {
        return groupMemberService.selectUserIdSet(groupId);
    }

    /**
     * 获取对应的群成员信息
     *
     * @param userId
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = memberCache, key = "#groupId+':'+#userId", unless = "#result == null")
    public ImGroupMember getMember(Long userId, Long groupId) {
        ImGroupMember record = new ImGroupMember();
        record.setUserId(userId);
        record.setGroupId(groupId);
        return groupMemberService.selectByCondition(record);
    }

    /**
     * 获取对应的群的群主信息
     *
     * @return
     */
    @Cacheable(cacheNames = ownerCache, key = "#groupId", unless = "#result == null")
    public ImGroupMember getOwner(Long groupId) {
        ImGroupMember record = new ImGroupMember();
        record.setGroupId(groupId);
        record.setRole(groupRoleOwner);
        return groupMemberService.selectByCondition(record);
    }

    /**
     * 获取好友
     *
     * @param userId
     * @param friendId
     * @return
     */
    public ImFriend getFriend(long userId, long friendId) {
        ImFriend friend = new ImFriend();
        friend.setFriendId(friendId);
        friend.setUserId(userId);
        return friendService.selectByFriendIdAndUserId(friend);
    }

    /************************************删除缓存操作***************************************************/

    /**
     * 删除群成员
     *
     * @param record
     */
    @Caching(evict = {
            //删除对应的群成员信息
            @CacheEvict(value = memberCache, key = "#record.groupId+':'+#record.userId"),
            @CacheEvict(value = groupsMemberCache, key = "#record.id"),
            //群成员数
            @CacheEvict(value = countMemberCache, key = "#record.groupId"),
            //群管理员数
            @CacheEvict(value = countManagerCache, key = "#record.groupId", condition = "#record.role==2"),
            //群成员list
            @CacheEvict(value = memberListCache, key = "#record.groupId"),
            //全部群成员set id
            @CacheEvict(value = allMemberSetCache, key = "#record.groupId"),
            //群成员set id
            @CacheEvict(value = memberSetCache, key = "#record.groupId", condition = "#record.role==3"),
            //群管理员set id
            @CacheEvict(value = managerSetCache, key = "#record.groupId", condition = "#record.role==2"),
            //群管理员和群主list
            @CacheEvict(value = managerAndOwnerListCache, key = "#record.groupId", condition = "#record.role==2 or #record.role==1"),
            //群成员Set
            @CacheEvict(value = groupMemberSetCache, key = "#record.groupId"),

            //群成员列表
            //@CacheEvict(value = memberListMapCache, key = "#record.groupId"),
            //如果是管理员
            //@CacheEvict(value = managerListMapCache, key = "#record.groupId", condition = "#record.role==2"),


            //chat server
            @CacheEvict(value = memberCacheMap, key = "#record.userId+':'+#record.groupId"),
            @CacheEvict(value = secretMemberCacheMap, key = "#record.userId+':'+#record.groupId"),
            @CacheEvict(value = noSecretMemberListCacheMap, key = "#record.userId"),
            @CacheEvict(value = allMemberListCacheMap, key = "#record.userId"),
            @CacheEvict(value = memberListCacheMap, key = "#record.groupId"),

    })
    public void removeMember(ImGroupMember record) {
    }

    @CacheEvict(value = friendBlackListCache, key = "#friend.userId")
    public void setBlack(ImFriend friend) {
    }

    //@CacheEvict(value = friendInfoListCache,key ="#friend.userId")
    public void setFriendSecret(ImFriend friend) {
    }

    /**
     * 根据id查找用户信息
     *
     * @param id
     * @return
     */
    public ImUserWithBLOBs selectByPrimaryKeyUser(Long id) {
        return userService.selectByPrimaryKey(id);
    }

    /**
     * 根据account查找用户信息
     *
     * @param account
     * @return
     */
    public ImUserWithBLOBs selectByAccount(String account) {
        return userService.selectByAccount(account);
    }

    /**
     * 更新用户缓存
     *
     * @param user
     * @return
     */
    public ImUserWithBLOBs updateUserCache(ImUserWithBLOBs user) {
        return userService.updateUserCache(user);
    }

    /**
     * 根据id查找用户信息
     *
     * @param id
     * @return
     */
    public ImGroupWithBLOBs selectByPrimaryKeyGroup(Long id) {
        return groupService.selectByPrimaryKey(id);
    }

    /**
     * 更新群缓存
     *
     * @param group
     * @return
     */
    public ImGroupWithBLOBs updateGroupCache(ImGroupWithBLOBs group) {
        return groupService.updateGroupCache(group);
    }


    /**
     * 更新群成员缓存
     *
     * @param groupMember
     * @return
     */
    public ImGroupMember updateGroupMemberCache(ImGroupMember groupMember) {
        return groupMemberService.updateGroupMemberCache(groupMember);
    }

    /**
     * 删除私密群组缓存
     *
     * @param groupMember
     * @return
     */
    public void removeSecretGroupCache(ImGroupMember groupMember) {
        groupMemberService.removeSecretGroupCache(groupMember);
    }


    /**
     * 删除群缓存
     */
    public void removeGroupCache(Long userId) {
        groupMemberService.removeGroupCache(userId);
    }


    /**
     * 更新群管理员缓存
     *
     * @param groupMember
     * @return
     */
    public ImGroupMember updateGroupMemberRoleCache(ImGroupMember groupMember) {
        return groupMemberService.updateGroupMemberRoleCache(groupMember);
    }

    /**
     * 更新好友设置缓存
     *
     * @param friend
     * @return
     */
    public ImFriend updateFriendCache(ImFriend friend) {
        return friendService.updateFriendCache(friend);
    }


    /**
     * 好友数量变更时需要更新的缓存
     *
     * @param friend
     */
    public void friendNumChange(ImFriend friend) {
        friendService.friendNumChange(friend);
    }

    /**
     * 好友加入私密或移除私密
     *
     * @param friend
     */
    public void secretFriendChange(ImFriend friend) {
        friendService.secretFriendChange(friend);
    }

    /**
     * 删除tempId缓存
     *
     * @param tempId
     */
    public void deleteByTempId(Long tempId) {
        tempIdentityService.deleteByTempId(tempId);
    }
}
