package com.wxzd.im.ms.service;

public interface SequenceService {

    /**
     * 获取序列
     *
     * @param lockKey
     * @return
     */
    long getTempId(String lockKey);
}
