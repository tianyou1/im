package com.wxzd.im.ms.parameter;

public class LockKey {

    //创建群
    public static final String createGroupKey = "create_group_key";
    //生成群临时id
    public static final String groupQRCodeKey = "group_qrcode_key";
    //生成用户临时身份id
    public static final String userTempKey = "user_temp_key";
    //邀请加入群
    public static final String inviteJoinGroupKey = "invite_join_group_key";
    //通过二维码扫描进群
    public static final String joinGroupByQrcodeKey = "join_group_by_qrcode_key";
    //群成员邀请 用户进群审核接口
    public static final String acceptInviteGroupKey = "accept_invite_group_key";
    //邀请请求
    public static final String acceptInviteReqKey = "accept_invite_req_key";
    //根据用户id或临时id添加
    public static final String reqFriendByUserIdKey = "req_friend_by_user_id_key";
    //根据账号添加
    public static final String reqFriendByAccountKey = "req_friend_by_account_key";
    //根据群成员id添加
    public static final String reqFriendByGroupMemberIdKey = "req_friend_by_member_id_key";
    //处理好友申请
    public static final String dealFriendReqKey = "deal_friend_req_key";

}
