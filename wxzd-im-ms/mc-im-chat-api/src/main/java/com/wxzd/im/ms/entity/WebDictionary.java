package com.wxzd.im.ms.entity;

import java.util.Date;

public class WebDictionary {
    private Integer id;

    private String type;

    private String typeName;

    private Integer seqNo;

    private Integer status;

    private String remark;

    private Integer accessLockCode;

    private Integer updateBy;

    private String updateByAddress;

    private Date updateTime;

    private Integer createBy;

    private String createByAddress;

    private Date createTime;

    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getAccessLockCode() {
        return accessLockCode;
    }

    public void setAccessLockCode(Integer accessLockCode) {
        this.accessLockCode = accessLockCode;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateByAddress() {
        return updateByAddress;
    }

    public void setUpdateByAddress(String updateByAddress) {
        this.updateByAddress = updateByAddress == null ? null : updateByAddress.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public String getCreateByAddress() {
        return createByAddress;
    }

    public void setCreateByAddress(String createByAddress) {
        this.createByAddress = createByAddress == null ? null : createByAddress.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}