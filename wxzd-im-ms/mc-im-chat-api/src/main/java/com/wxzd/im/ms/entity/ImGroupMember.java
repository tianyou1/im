package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ImGroupMember {
    private Long id;

    private Long groupId;

    private Long userId;

    private String markName;

    private Integer role;

    private Long creatorId;

    private Long createTime;

    private Integer receiveTip;

    private Integer isAccept;

    private Long isBanned;

    private Integer isBlack;

    private Integer isAnonymous;

    private String anonymousHeaderUrl;

    private String anonymousName;

    private Integer isSecretGroup;

    private Integer isShowSessionMessage;

    private Integer isCollect;

    private Integer isSecretHistory;



}