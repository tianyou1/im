package com.wxzd.im.ms.entity;

import java.util.Date;

public class ImUser {
    private Long id;

    private String name;

    private String openId;

    private String qqOpenId;

    private String account;

    private String idNo;

    private String nickName;

    private String realName;

    private String pwd;

    private String mobile;

    private String mail;

    private String sex;

    private Long birthday;

    private String sign;

    private String province;

    private Integer isOnline;

    private Integer needAuth;

    private String period;

    private Integer searchMobile;

    private Integer newNotification;

    private String city;

    private String district;

    private Long createTime;

    private Integer status;

    private String detail;

    private Integer isAuth;

    private Long recommandUserId;

    private String longitude;

    private String latitude;

    private String location;

    private String alipayName;

    private String alipayAccount;

    private String firstRedpacket;

    private String secretModePwd;

    private String chatLogPwd;

    private Integer isShowInputStatus;

    private Integer isAutoCancel;

    private Integer autoCancelMonths;

    private Integer isShowUserid;

    private Integer searchAccount;

    private Integer searchUserId;

    private Integer searchGroupId;

    private Integer isGroupedNeedAuth;

    private Date lastLoginTime;

    private Long isBanned;

    private Integer isShowSessionMessage;

    private Integer isSecretHistory;

    private Integer isShowGroupSessionMessage;

    private Integer isSecretGroupHistory;

    private Integer isNewMessageNotification;

    private Integer isNewGroupMessageNotification;

    private Integer isVideoCallNotification;

    private Integer isNotificationShowDetail;

    private Integer isVoiceReminder;

    private Integer isVibrationReminder;

    private Integer isMarkSecretFriends;

    private Integer isMarkSecretMode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    public String getQqOpenId() {
        return qqOpenId;
    }

    public void setQqOpenId(String qqOpenId) {
        this.qqOpenId = qqOpenId == null ? null : qqOpenId.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo == null ? null : idNo.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public Integer getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Integer isOnline) {
        this.isOnline = isOnline;
    }

    public Integer getNeedAuth() {
        return needAuth;
    }

    public void setNeedAuth(Integer needAuth) {
        this.needAuth = needAuth;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period == null ? null : period.trim();
    }

    public Integer getSearchMobile() {
        return searchMobile;
    }

    public void setSearchMobile(Integer searchMobile) {
        this.searchMobile = searchMobile;
    }

    public Integer getNewNotification() {
        return newNotification;
    }

    public void setNewNotification(Integer newNotification) {
        this.newNotification = newNotification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    public Integer getIsAuth() {
        return isAuth;
    }

    public void setIsAuth(Integer isAuth) {
        this.isAuth = isAuth;
    }

    public Long getRecommandUserId() {
        return recommandUserId;
    }

    public void setRecommandUserId(Long recommandUserId) {
        this.recommandUserId = recommandUserId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    public String getAlipayName() {
        return alipayName;
    }

    public void setAlipayName(String alipayName) {
        this.alipayName = alipayName == null ? null : alipayName.trim();
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount == null ? null : alipayAccount.trim();
    }

    public String getFirstRedpacket() {
        return firstRedpacket;
    }

    public void setFirstRedpacket(String firstRedpacket) {
        this.firstRedpacket = firstRedpacket == null ? null : firstRedpacket.trim();
    }

    public String getSecretModePwd() {
        return secretModePwd;
    }

    public void setSecretModePwd(String secretModePwd) {
        this.secretModePwd = secretModePwd == null ? null : secretModePwd.trim();
    }

    public String getChatLogPwd() {
        return chatLogPwd;
    }

    public void setChatLogPwd(String chatLogPwd) {
        this.chatLogPwd = chatLogPwd == null ? null : chatLogPwd.trim();
    }

    public Integer getIsShowInputStatus() {
        return isShowInputStatus;
    }

    public void setIsShowInputStatus(Integer isShowInputStatus) {
        this.isShowInputStatus = isShowInputStatus;
    }

    public Integer getIsAutoCancel() {
        return isAutoCancel;
    }

    public void setIsAutoCancel(Integer isAutoCancel) {
        this.isAutoCancel = isAutoCancel;
    }

    public Integer getAutoCancelMonths() {
        return autoCancelMonths;
    }

    public void setAutoCancelMonths(Integer autoCancelMonths) {
        this.autoCancelMonths = autoCancelMonths;
    }

    public Integer getIsShowUserid() {
        return isShowUserid;
    }

    public void setIsShowUserid(Integer isShowUserid) {
        this.isShowUserid = isShowUserid;
    }

    public Integer getSearchAccount() {
        return searchAccount;
    }

    public void setSearchAccount(Integer searchAccount) {
        this.searchAccount = searchAccount;
    }

    public Integer getSearchUserId() {
        return searchUserId;
    }

    public void setSearchUserId(Integer searchUserId) {
        this.searchUserId = searchUserId;
    }

    public Integer getSearchGroupId() {
        return searchGroupId;
    }

    public void setSearchGroupId(Integer searchGroupId) {
        this.searchGroupId = searchGroupId;
    }

    public Integer getIsGroupedNeedAuth() {
        return isGroupedNeedAuth;
    }

    public void setIsGroupedNeedAuth(Integer isGroupedNeedAuth) {
        this.isGroupedNeedAuth = isGroupedNeedAuth;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Long isBanned) {
        this.isBanned = isBanned;
    }

    public Integer getIsShowSessionMessage() {
        return isShowSessionMessage;
    }

    public void setIsShowSessionMessage(Integer isShowSessionMessage) {
        this.isShowSessionMessage = isShowSessionMessage;
    }

    public Integer getIsSecretHistory() {
        return isSecretHistory;
    }

    public void setIsSecretHistory(Integer isSecretHistory) {
        this.isSecretHistory = isSecretHistory;
    }

    public Integer getIsShowGroupSessionMessage() {
        return isShowGroupSessionMessage;
    }

    public void setIsShowGroupSessionMessage(Integer isShowGroupSessionMessage) {
        this.isShowGroupSessionMessage = isShowGroupSessionMessage;
    }

    public Integer getIsSecretGroupHistory() {
        return isSecretGroupHistory;
    }

    public void setIsSecretGroupHistory(Integer isSecretGroupHistory) {
        this.isSecretGroupHistory = isSecretGroupHistory;
    }

    public Integer getIsNewMessageNotification() {
        return isNewMessageNotification;
    }

    public void setIsNewMessageNotification(Integer isNewMessageNotification) {
        this.isNewMessageNotification = isNewMessageNotification;
    }

    public Integer getIsNewGroupMessageNotification() {
        return isNewGroupMessageNotification;
    }

    public void setIsNewGroupMessageNotification(Integer isNewGroupMessageNotification) {
        this.isNewGroupMessageNotification = isNewGroupMessageNotification;
    }

    public Integer getIsVideoCallNotification() {
        return isVideoCallNotification;
    }

    public void setIsVideoCallNotification(Integer isVideoCallNotification) {
        this.isVideoCallNotification = isVideoCallNotification;
    }

    public Integer getIsNotificationShowDetail() {
        return isNotificationShowDetail;
    }

    public void setIsNotificationShowDetail(Integer isNotificationShowDetail) {
        this.isNotificationShowDetail = isNotificationShowDetail;
    }

    public Integer getIsVoiceReminder() {
        return isVoiceReminder;
    }

    public void setIsVoiceReminder(Integer isVoiceReminder) {
        this.isVoiceReminder = isVoiceReminder;
    }

    public Integer getIsVibrationReminder() {
        return isVibrationReminder;
    }

    public void setIsVibrationReminder(Integer isVibrationReminder) {
        this.isVibrationReminder = isVibrationReminder;
    }

    public Integer getIsMarkSecretFriends() {
        return isMarkSecretFriends;
    }

    public void setIsMarkSecretFriends(Integer isMarkSecretFriends) {
        this.isMarkSecretFriends = isMarkSecretFriends;
    }

    public Integer getIsMarkSecretMode() {
        return isMarkSecretMode;
    }

    public void setIsMarkSecretMode(Integer isMarkSecretMode) {
        this.isMarkSecretMode = isMarkSecretMode;
    }
}