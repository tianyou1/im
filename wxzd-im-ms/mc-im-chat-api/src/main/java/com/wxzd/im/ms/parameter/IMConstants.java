package com.wxzd.im.ms.parameter;

import java.util.Arrays;
import java.util.List;

public class IMConstants {

    public final static List<Integer> SPEC_MSG_TYPE = Arrays.asList(2, 3, 4, 16, 17, 18, 28, 29, 30, 32, 33, 34, 35);

    public final static int DEV_PC = 3;
    public final static int DEV_IOS = 2;
    public final static int DEV_ANDROID = 1;

    // 聊天
    public final static String IM_EVENT_CHAT = "chat";

    // 心跳
    public final static String IM_EVENT_HEART = "heart";

    // 人对人
    public final static int MSG_FROM_P2P = 1;
    // 群聊
    public final static int MSG_FROM_GROUP = 2;
    // 系统消息
    public final static int MSG_FROM_SYS = 3;
    // 第三方
    public final static int MSG_FROM_THIRD = 4;

    //////////////////////////////////////////////////

    // READY
    public final static int MSG_TYPE_READY = 1;
    // 文本
    public final static int MSG_TYPE_TEXT = 2;

    public final static int MSG_TYPE_IMG = 3;
    // 文件
    public final static int MSG_TYPE_FILE = 4;

    // 上线
    public final static int MSG_TYPE_ONLINE = 5;

    // 离线
    public final static int MSG_TYPE_OFFLINE = 6;

    // 删除好友
    public final static int MSG_TYPE_DEL_FRIEND = 7;

    //xx邀请xx加入了群聊
    public final static int MSG_TYPE_JOIN_GROUP = 8;

    // 退出群聊
    public final static int MSG_TYPE_QUIT_GROUP = 9;


    public final static int MSG_TYPE_INVITE_GROUP = 10;

    //好友申请
    public final static int MSG_TYPE_FRIEND_REQ = 11;

    //通过好友请求
    public final static int MSG_TYPE_ACCEPT_FRIEND = 12;

    public final static int MSG_TYPE_GROUP_REQ = 13;

    //xx邀请你加入了群聊
    public final static int MSG_TYPE_ACCEPT_GROUP = 14;

    public final static int MSG_TYPE_OTHER_LOGIN = 15;

    public final static int MSG_TYPE_VOICE = 16;

    public final static int MSG_TYPE_RED_PACKET = 17;

    // 转账通知
    public final static int MSG_TYPE_TRANSFER = 18;

    // 有人接收红包时，，提示发红包人
    public final static int MSG_TYPE_RECEIVE_RED_NOTICE = 19;

    // 确认转账收钱通知
    public final static int MSG_TYPE_TRANSFER_REC = 20;

    // 退回转账通知
    public final static int MSG_TYPE_TRANSFER_BACK = 21;

    // 退回红包通知
    public final static int MSG_TYPE_REDPACKET_BACK = 22;

    // 自动添加好友成功
    public final static int MSG_TYPE_AUTO_FRIEND = 23;

    // 红包被光
    public final static int MSG_TYPE_RED_FINISHED = 24;

    // 好友资料有修改
    public final static int MSG_TYPE_MODIFY_PROFILE = 26;

    // 群成员更新了备注名
    public final static int MSG_TYPE_MODIFY_GROUP_COMMENT = 27;

    /**
     * 28 发送名片 29 发送位置(content:{url:http:xxxx,addr:详细地址 ,lat:经度, lut:纬度}) 30
     * 发送小视频 ( content{img:http, time:2, video:http}) 31 心跳 32 消息撤回 33 消息已读 34
     * 表情云 35 at类型的消息,目前@的消息只能是文本类型，content={atIds="11,22,33",content="文本或者表情"}
     * 36 有好友发布了朋友圈动态 37 好友评论了你的动态，或者回复了你的评论
     */

    // 发送名片
    public final static int MSG_TYPE_SEND_CARD = 28;

    // 发送位置
    public final static int MSG_TYPE_SEND_LOCATION = 29;
    // 发送小视频
    public final static int MSG_TYPE_SEND_VIDEO = 30;

    // 心跳
    public final static int MSG_TYPE_HEARTBEAT = 31;
    // 消息撤回
    public final static int MSG_TYPE_REBACK = 32;
    // 消息已读
    public final static int MSG_TYPE_READED = 33;
    // 表情云
    public final static int MSG_TYPE_EMOJI_YUN = 34;
    // at类型的消息
    public final static int MSG_TYPE_AT = 35;
    // 有好友发布了朋友圈动态
    public final static int MSG_TYPE_NEW_FEED = 36;
    // 好友评论了你的动态，或者回复了你的评论
    public final static int MSG_TYPE_REFER_FEED = 37;
    // 好友赞了你的动态，或者回复了你的评论
    public final static int MSG_TYPE_REFER_PRAISE = 38;

    // 到账通知
    public final static int MSG_TYPE_REC_MONEY = 39;

    // AA收款消息
    public final static int MSG_TYPE_AA_RECEIVE = 40;

    // 请求语音通话
    public final static int MSG_TYPE_VOICE_CHAT = 42;
    // 请求视频通话
    public final static int MSG_TYPE_VIDEO_CHAT = 44;

    // 有新闻公告信息
    public final static int MSG_TYPE_NEW_NOTICE = 51;

    // 在黑名单
    public final static int MSG_TYPE_IS_BLACK = 53;

    // 不是好友
    public final static int MSG_TYPE_NOT_FRIEND = 54;

    // 群解散
    public final static int MSG_TYPE_GROUP_DISMISS = 55;

    // 骰子
    public final static int MSG_TYPE_SEZHI = 56;

    //置顶
    public final static int MSG_IS_TOP = 57;

    //取消置顶
    public final static int MSG_CANCEL_TOP = 58;

    // 群转让
    public final static int MSG_TYPE_GROUP_TRANSFER = 59;

    //截图
    public final static int MSG_TYPE_SCREEN_SHOT = 60;

    //阅后即焚设置
    public final static int MSG_TYPE_READ_DEL_SET = 61;

    //阅后即焚设置（自己）
    public final static int MSG_TYPE_UPDATE_READ_DEL = 62;

    //通知 有人邀请他加入xx群 用于更新邀请列表数量
    public final static int MSG_TYPE_INVITE_GROUP_REQ = 64;

    //群设置变更
    public final static int MSG_TYPE_GROUP_SET_CHANGE = 66;

    //阅后即焚设置
    public final static int MSG_TYPE_READ_DEL_SET_GROUP = 67;

    //邀请别人加入群成功通知
    public final static int MSG_TYPE_INVITE_GROUP_REQ_ACCPET = 70;

    //邀请别人加入群 需群主同意 用于更新进群确认列表数量
    public final static int MSG_TYPE_INVITE_GROUP_CONFIRM = 71;

    //拒绝好友的邀请加群申请
    public final static int MSG_TYPE_ACCEPT_GROUP_REQ_NO = 72;

    //同意好友的邀请加群申请
    public final static int MSG_TYPE_ACCEPT_GROUP_REQ_YES = 73;

    //用户设置变更
    public final static int MSG_TYPE_USER_SET_UPDATE = 74;

    //账号注销成功
    public final static int MSG_TYPE_AUTH_CANCEL = 75;

    //同意群员的邀请好友加群
    public final static int MSG_TYPE_AGREE_INVITE_REQ = 76;

    //拒绝群员的邀请好友加群
    public final static int MSG_TYPE_DISAGREE_INVITE_REQ = 77;

    //踢出群聊 xx被xx移出了群聊
    public final static int MSG_TYPE_REMOVE_GROUP = 79;

    //撤回通知
    public final static int MSG_TYPE_REBACK_NOTIFY = 80;

    //修改备注
    public final static int MSG_TYPE_UPDATE_REMARK = 81;

    //用户系统禁言
    public final static int MSG_TYPE_IS_BANNED = 82;

    /**
     * 群截屏提醒
     */
    public final static int MSG_TYPE_SCREEN_SHOT_GROUP = 83;
    /**
     * 输入状态显示
     */
    public final static int IS_SHOW_INPUT = 84;

    /**
     * 批量转发
     */
    public final static int BATCH_TRANSMIT = 85;

    /************好友**************/

    /**
     * 好友设置黑名单
     */
    public final static int MSG_TYPE_FRIEND_SET_BLACK = 101;

    /**
     * 好友移除黑名单
     */
    public final static int MSG_TYPE_FRIEND_OFF_BLACK = 102;

    /**
     * 好友设置消息免打扰
     */
    public final static int MSG_TYPE_FRIEND_SET_RECEIVETIP = 103;

    /**
     * 好友移除消息免打扰
     */
    public final static int MSG_TYPE_FRIEND_OFF_RECEIVETIP = 104;

    /**
     * 好友设置截屏提醒
     */
    public final static int MSG_TYPE_FRIEND_SET_SCREENSHOTTIP = 105;

    /**
     * 好友取消截屏提醒
     */
    public final static int MSG_TYPE_FRIEND_OFF_SCREENSHOTTIP = 106;

    /**
     * 会话消息显示
     */
    public final static int MSG_TYPE_FRIEND_SET_SHOWMESSAGE = 107;

    /**
     * 会话消息不显示
     */
    public final static int MSG_TYPE_FRIEND_OFF_SHOWMESSAGE = 108;

    /**
     * 是私密联系人
     */
    public final static int MSG_TYPE_FRIEND_SET_SECRETFRIEND = 109;

    /**
     * 不是私密联系人
     */
    public final static int MSG_TYPE_FRIEND_OFF_SECRETFRIEND = 110;

    /**
     * 好友阅后即焚设置
     */
    public final static int MSG_TYPE_FRIEND_SET_EPHEMERALCHAT = 111;

    /**
     * 好友阅后即焚取消
     */
    public final static int MSG_TYPE_FRIEND_OFF_EPHEMERALCHAT = 112;

    /**
     * 好友聊天记录加密设置
     */
    public final static int MSG_TYPE_FRIEND_SET_SECRETHISTORY = 113;

    /**
     * 好友聊天记录取消加密
     */
    public final static int MSG_TYPE_FRIEND_OFF_SECRETHISTORY = 114;

    /**
     * 好友聊天记录单向删除
     */
    public final static int MSG_TYPE_FRIEND_CHATHISTORY_DELETE = 115;

    /**
     * 好友聊天记录双向删除
     */
    public final static int MSG_TYPE_FRIEND_CHATHISTORY_BOTH_DELETE = 116;

    /**
     * 修改好友备注
     */
    public final static int MSG_TYPE_FRIEND_UPDATE_REMARK = 117;

    /**
     * 名片
     */
    public final static int MSG_TYPE_PROFILE = 120;

    /************群组**************/

    /**
     * 修改群头像
     */
    public final static int MSG_TYPE_UPDATE_GROUP_HEAD = 201;

    /**
     * 修改群名称
     */
    public final static int MSG_TYPE_UPDATE_GROUP_NAME = 202;

    /**
     * 修改群公告
     */
    public final static int MSG_TYPE_UPDATE_GROUP_BULLETIN = 203;

    /**
     * 设置了二维码
     */
    public final static int MSG_TYPE_SET_GROUP_QRCODE = 204;

    /**
     * 设置全体禁言
     */
    public final static int MSG_TYPE_SET_ALL_BANNED = 205;

    /**
     * 取消全体禁言
     */
    public final static int MSG_TYPE_CANCEL_ALL_BANNED = 206;

    /**
     * 设置匿名聊天
     */
    public final static int MSG_TYPE_SET_ANONYMOUS = 207;

    /**
     * 取消匿名聊天
     */
    public final static int MSG_TYPE_CANCEL_ANONYMOUS = 208;

    /**
     * 设置邀请确认
     */
    public final static int MSG_TYPE_SET_INVITECONFIRM = 209;

    /**
     * 取消邀请确认
     */
    public final static int MSG_TYPE_CANCEL_INVITECONFIRM = 210;

    /**
     * 设置截屏通知
     */
    public final static int MSG_TYPE_SET_SCREENSHOTTIP = 211;

    /**
     * 取消截屏通知
     */
    public final static int MSG_TYPE_CANCEL_SCREENSHOTTIP = 212;

    /**
     * 设置阅后即焚
     */
    public final static int MSG_TYPE_SET_GROUP_EPHEMERALCHAT = 213;

    /**
     * 取消阅后即焚
     */
    public final static int MSG_TYPE_CANCEL_GROUP_EPHEMERALCHAT = 214;

    /**
     * 允许群成员相互查看
     */
    public final static int MSG_TYPE_ALLOW_VIEW_MEMBERS = 215;

    /**
     * 禁止群成员相互查看
     */
    public final static int MSG_TYPE_NOT_ALLOW_VIEW_MEMBERS = 216;

    /**
     * 禁止群成员互加
     */
    public final static int MSG_TYPE_NOT_ALLOW_ADD_MEMBERS = 217;

    /**
     * 允许群成员互加
     */
    public final static int MSG_TYPE_ALLOW_ADD_MEMBERS = 218;

    /**
     * 设置群成员 永久禁言
     */
    public final static int MSG_TYPE_SET_MEMBER_FOREVER_BANNED = 219;

    /**
     * 设置群成员 分钟禁言
     */
    public final static int MSG_TYPE_SET_MEMBER_MINUTE_BANNED = 220;

    /**
     * 设置群成员 取消禁言
     */
    public final static int MSG_TYPE_CANCEL_MEMBER_BANNED = 221;

    /**
     * 设置群成员 取消黑名单
     */
    public final static int MSG_TYPE_CANCEL_MEMBER_BLACK = 222;

    /**
     * 设置群成员 设置黑名单
     */
    public final static int MSG_TYPE_SET_MEMBER_BLACK = 223;

    /**
     * 匿名消息推送，同步其他设备
     */
    public final static int MSG_TYPE_SET_MEMBER_ISANONYMOUS = 224;

    /**
     * 扫码加群
     */
    public final static int MSG_TYPE_JOIN_GROUP_BYQR = 225;

    /**
     * 群管理员设置和取消
     */
    public final static int MSG_TYPE_GROUP_MANAGER_SET = 226;

    /**
     * 群聊 聊天记录加密开启
     */
    public final static int MSG_TYPE_GROUP_MEMBER_CHATHISTORY_SET = 227;

    /**
     * 群聊 聊天记录加密关闭
     */
    public final static int MSG_TYPE_GROUP_MEMBER_CHATHISTORY_OFF = 228;


    /**
     * 退出群聊 含显示信息
     */
    public final static int MSG_TYPE_QUIT_GROUP_MSG = 229;

    /**
     * 移出群聊 含显示信息
     */
    public final static int MSG_TYPE_REMOVE_GROUP_MSG = 230;

    /**
     * 群聊 收藏群聊
     */
    public final static int MSG_TYPE_COLLECT_GROUP_SET = 231;

    /**
     * 群聊 取消收藏
     */
    public final static int MSG_TYPE_COLLECT_GROUP_OFF = 232;

    /**
     * 群聊 消息显示
     */
    public final static int MSG_TYPE_SHOW_SESSION_MESSAGE_SET = 233;

    /**
     * 群聊 消息显示 取消
     */
    public final static int MSG_TYPE_SHOW_SESSION_MESSAGE_OFF = 234;

    /**
     * 群聊 私密群聊
     */
    public final static int MSG_TYPE_SECRET_GROUP_SET = 235;

    /**
     * 群聊 私密群聊 取消
     */
    public final static int MSG_TYPE_SECRET_GROUP_OFF = 236;

    /**
     * 群聊 消息提醒
     */
    public final static int MSG_TYPE_RECEIVETIP_SET = 237;

    /**
     * 群聊 消息提醒 取消
     */
    public final static int MSG_TYPE_RECEIVETIP_OFF = 238;

    /**
     * 群聊聊天记录删除
     */
    public final static int MSG_TYPE_GROUP_DELETE_HISTORY = 239;

    /**
     * 修改密码
     */
    public final static int MSG_TYPE_UPDATE_PWD = 301;

    // 服务器拒绝消息转
    public final static int MSG_TYPE_REFUSE_MSG = 4444;


}
