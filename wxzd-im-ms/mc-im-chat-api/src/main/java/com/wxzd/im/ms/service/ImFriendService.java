package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImFriend;
import com.wxzd.im.ms.parameter.ResponseData;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ImFriendService {
    int deleteByPrimaryKey(Long id);

    int insert(ImFriend record);

    int insertSelective(ImFriend record);

    void friendNumChange(ImFriend friend);

    void secretFriendChange(ImFriend friend);

    ImFriend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImFriend record);

    ImFriend updateFriendCache(ImFriend record);

    int updateByPrimaryKey(ImFriend record);

    ImFriend selectByFriendIdAndUserId(ImFriend friend);

    /**
     * 查找好友根据用户id
     *
     * @param userId
     * @return
     */
    List<ImFriend> selectFriends(Long userId);


    /**
     * 获取私密联系人
     *
     * @param userId
     * @param name
     * @return
     */
    List<Map<String, Object>> selectSecretList(Long userId, String name);


    /**
     * 添加好友
     *
     * @param userId
     * @param destId
     * @param sourceType
     * @param isSecretFriend
     * @param remark
     * @param requestContent
     * @return
     */
    int requestFriendById(long userId, long destId, int sourceType, int isSecretFriend, String remark, String requestContent);

    /**
     * 根据条件删除好友
     *
     * @param record
     * @return
     */
    int deleteByCondition(ImFriend record);

    /**
     * 根据是否私密加载好友列表
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> getFriendInfo(Map<String, Object> map);


    /**
     * 根据是否私密加载会话列表
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> getSessionList(Map<String, Object> map);

    /**
     * 同意时修改好友信息
     *
     * @param remark
     * @param isSecretFriend
     * @param userId
     * @param friendId
     * @return
     */
    int updateByUserIdAndFriendIdAgree(String remark, Integer isSecretFriend, Long userId, Long friendId);

    /**
     * 获取黑名单列表
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> getBlackList(Map<String, Object> map);

    /**
     * 处理好友申请
     *
     * @param userId
     * @param device
     * @param friendId
     * @param confirmState
     * @param remark
     * @param isSecretFriend
     * @param userFriend
     * @param friend
     * @return
     */
    ResponseData dealFriendRequest(long userId, int device, long friendId, int confirmState, String remark, int isSecretFriend,ImFriend userFriend,ImFriend friend);


    /**
     * 修改阅后即焚判断
     *
     * @param userId
     * @param friendId
     * @param isEphemeralChat
     * @param ephemeralChatTime
     * @return
     */
    boolean updateCheck(long userId, long friendId, int isEphemeralChat, int ephemeralChatTime,ImFriend friend);


    /**
     * 查找两边好友都为好友的判断
     *
     * @param userId
     * @param friendId
     * @return
     */
    boolean isFriendCheck(long userId, long friendId);

    /**
     * 获取好友idset列表
     *
     * @param userId
     * @return
     */
    Set<Long> selectFriendsSet(long userId);

    /**
     * 统计好友数量
     *
     * @param userId
     * @return
     */
    int countFriends(long userId);

    void deleteDetailCacheUserId(long userId);

    void deleteDetailCacheFriendId(long friendId);
}