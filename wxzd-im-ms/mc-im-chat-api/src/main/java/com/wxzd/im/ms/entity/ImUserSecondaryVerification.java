package com.wxzd.im.ms.entity;

public class ImUserSecondaryVerification {
    private Long id;

    private Long userId;

    private Integer isSecondaryVerification;

    private Integer verifyType;

    private String mobile;

    private String email;

    private String googleKey;

    private String secondaryPwd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getIsSecondaryVerification() {
        return isSecondaryVerification;
    }

    public void setIsSecondaryVerification(Integer isSecondaryVerification) {
        this.isSecondaryVerification = isSecondaryVerification;
    }

    public Integer getVerifyType() {
        return verifyType;
    }

    public void setVerifyType(Integer verifyType) {
        this.verifyType = verifyType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getGoogleKey() {
        return googleKey;
    }

    public void setGoogleKey(String googleKey) {
        this.googleKey = googleKey == null ? null : googleKey.trim();
    }

    public String getSecondaryPwd() {
        return secondaryPwd;
    }

    public void setSecondaryPwd(String secondaryPwd) {
        this.secondaryPwd = secondaryPwd == null ? null : secondaryPwd.trim();
    }
}