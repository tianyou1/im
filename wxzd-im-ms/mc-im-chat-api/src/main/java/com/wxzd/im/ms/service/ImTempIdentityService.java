package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImTempIdentity;

public interface ImTempIdentityService {

    /**
     * 根据用户id和临时身份类型查找
     *
     * @param userId
     * @param tempType
     * @return
     */
    ImTempIdentity selectTempIdentityByUserId(Long userId, Integer identityType, Integer tempType);


    /**
     * 用户临时id身份删除
     * @param userId
     * @param identityType
     * @param tempType
     * @return
     */
    int deleteTempIdentityByUserId(Long userId, Integer identityType, Integer tempType);


    /**
     * 添加
     *
     * @param imTempIdentity
     * @return
     */
    int insert(ImTempIdentity imTempIdentity);

    /**
     * 修改
     *
     * @param imTempIdentity
     * @return
     */
    ImTempIdentity updateByPrimaryKeySelective(ImTempIdentity imTempIdentity);

    int insertSelective(ImTempIdentity record);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Long id);

    ImTempIdentity selectByTempId(Long tempId);

    ImTempIdentity selectByIdentityId(Long identityId);

    /**
     * 通过identityId删除
     *
     * @param identityId
     * @return
     */
    int deleteByIdentityId(Long identityId);

    void deleteByTempId(Long tempId);
}
