package com.wxzd.im.ms.utils;

import java.math.BigInteger;

/**
 * 系统加密方式
 */
public class EncryptionUtils {
    public static final String encrypt(String password) {
        if (password == null) {
            return "";
        } else if (password.length() == 0) {
            return "";
        } else {
            BigInteger bi_passwd = new BigInteger(password.getBytes());
            BigInteger bi_r0 = new BigInteger("01213910847463829232332121");
            BigInteger bi_r1 = bi_r0.xor(bi_passwd);
            return bi_r1.toString(16);
        }
    }

    public static final String decrypt(String encrypted) {
        if (encrypted == null) {
            return "";
        } else if (encrypted.length() == 0) {
            return "";
        } else {
            BigInteger bi_confuse = new BigInteger("01213910847463829232332121");

            try {
                BigInteger bi_r1 = new BigInteger(encrypted, 16);
                BigInteger bi_r0 = bi_r1.xor(bi_confuse);
                return new String(bi_r0.toByteArray());
            } catch (Exception var4) {
                return "";
            }
        }
    }
}
