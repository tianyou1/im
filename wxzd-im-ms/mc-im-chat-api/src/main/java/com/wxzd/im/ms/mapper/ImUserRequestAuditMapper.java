package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImUserRequestAudit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface ImUserRequestAuditMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserRequestAudit record);

    int insertSelective(ImUserRequestAudit record);

    ImUserRequestAudit selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserRequestAudit record);

    int updateByPrimaryKeyWithBLOBs(ImUserRequestAudit record);

    int updateByPrimaryKey(ImUserRequestAudit record);

    int deleteByUserIdAndFriendId(@Param("userId") Long userId, @Param("friendId") Long friendId);

    ImUserRequestAudit selectByCondition(ImUserRequestAudit record);

    List<ImUserRequestAudit> selectByConditions(ImUserRequestAudit record);

    ImUserRequestAudit isSentMsg(Map<String, Object> map);

    List<Map<String, Object>> unReadFriendReqCount(Long destId);

    List<Map<String, Object>> unReadConfirmReqCount(Long userId);

    List<Map<String, Object>> unReadInviteReqCount(Long userId);


    int updateByUserIdAndFriendId(@Param("auditStatus") Integer auditStatus, @Param("userId") Long userId, @Param("time") Long time, @Param("friendId") Long friendId);

    /**
     * 根据idList查找
     *
     * @param idList
     * @return
     */
    List<ImUserRequestAudit> selectByIds(@Param("idList") List<Long> idList);

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int batchUpdateRequestAudit(@Param("list") List<ImUserRequestAudit> list);

    int batchInsertSelective(@Param("list") List<ImUserRequestAudit> list);
}