package com.wxzd.im.ms.api.feignclient.service;

import com.wxzd.im.ms.parameter.ResultBean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
    /**
     * 文件上传
     *
     * @param file
     * @param referenceId
     * @param folderId
     * @return
     */
    @RequestMapping(value = "/file/upload", method = {RequestMethod.POST}, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResultBean upload(@RequestPart(value = "file") MultipartFile file,
                      @RequestParam(value = "referenceId", required = false) String referenceId,
                      @RequestParam(value = "folderId", required = false) String folderId);

    /**
     * 获取前缀
     */
    @RequestMapping(value = "/file/getVisitPrefix", method = {RequestMethod.POST})
    ResultBean getVisitPrefix();
}
