package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImUserSecondaryVerification;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImUserSecondaryVerificationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserSecondaryVerification record);

    int insertSelective(ImUserSecondaryVerification record);

    ImUserSecondaryVerification selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserSecondaryVerification record);

    int updateByPrimaryKey(ImUserSecondaryVerification record);

    ImUserSecondaryVerification selectByUserId(Long userId);
}