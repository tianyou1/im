package com.wxzd.im.ms.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ImFriend {
    private Long id;

    private Long userId;

    private Long friendId;

    private String remark;

    private Long createrId;

    private Long createTime;

    private Integer isBlack;

    private Integer isFriend;

    private Integer receiveTip;

    private Integer isSecretFriend;

    private Integer isSecretHistory;

    private Integer isScreenshotTip;

    private Integer isEphemeralChat;

    private Integer ephemeralChatTime;

    private Integer sourceType;

    private Integer isShowSessionMessage;


}