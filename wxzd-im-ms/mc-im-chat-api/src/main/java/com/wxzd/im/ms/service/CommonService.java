package com.wxzd.im.ms.service;

import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.parameter.IMConstants;
import com.wxzd.im.ms.utils.RedisUtil;
import com.wxzd.im.ms.utils.YxUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static com.wxzd.im.ms.parameter.IMConstants.MSG_TYPE_ACCEPT_GROUP;
import static com.wxzd.im.ms.parameter.IMConstants.MSG_TYPE_INVITE_GROUP_REQ;
import static com.wxzd.im.ms.parameter.StaticConstant.*;
import static com.wxzd.im.ms.utils.BanedTimeSet.unBannedTime;

@Service
public class CommonService {
    @Autowired
    private ImFriendService friendService;

    @Autowired
    private ImGroupService groupService;

    @Autowired
    private ImGroupMemberService groupMemberService;

    @Autowired
    private ImUserSessionService userSessionService;

    @Autowired
    private ImUserRequestAuditService requestAuditService;

    @Autowired
    private ImUserRequestDeletedService requestDeletedService;

    @Autowired
    private ImTopService topService;

    @Autowired
    private ImUserService userService;

    @Autowired
    private MessageFactory msgFactory;

    @Autowired
    private ChatServerFeign chatServerFeign;

    /**
     * 创建群返回群id
     *
     * @param name
     * @param userId
     * @return
     */
    public long createGroup(String name, long userId) {
        ImGroupWithBLOBs group = new ImGroupWithBLOBs();
        group.setName(name);
        group.setCreaterId(userId);
        group.setCreateTime(System.currentTimeMillis());
        //群头像
        group.setHeadUrl(groupHeadUrl);
        groupService.insertSelective(group);
        return group.getId();
    }

    /**
     * 添加普通群成员
     *
     * @param creatorId
     * @param groupId
     * @param memberUserId
     * @return
     */
    public ImGroupMember addGroupMember2(long creatorId, long groupId, long memberUserId, int role) {
        ImGroupMember member = new ImGroupMember();
        member.setCreateTime(System.currentTimeMillis());
        member.setCreatorId(creatorId);
        member.setRole(role);
        member.setGroupId(groupId);
        member.setUserId(memberUserId);
        member.setIsBanned(unBannedTime);
        return member;
    }

    /**
     * 添加普通群成员
     *
     * @param creatorId
     * @param groupId
     * @param memberUserId
     * @return
     */
    public ImGroupMember addGroupMember(long creatorId, long groupId, long memberUserId, int role) {
        ImGroupMember member = new ImGroupMember();
        member.setCreateTime(System.currentTimeMillis());
        member.setCreatorId(creatorId);
        member.setRole(role);
        member.setGroupId(groupId);
        member.setUserId(memberUserId);
        member.setIsBanned(unBannedTime);
        groupMemberService.insertSelective(member);
        return member;
    }


    /**
     * 添加普通群成员并且加入list
     *
     * @param creatorId
     * @param groupId
     * @param memberUserId
     * @return
     */
    public void addGroupMemberList2(List<ImGroupMember> members, long creatorId, long groupId, long memberUserId, int role) {
        ImGroupMember member = new ImGroupMember();
        member.setCreateTime(System.currentTimeMillis());
        member.setCreatorId(creatorId);
        member.setRole(role);
        member.setGroupId(groupId);
        member.setUserId(memberUserId);
        member.setIsBanned(unBannedTime);
        members.add(member);
    }

    /**
     * 添加普通群成员并且加入list
     *
     * @param creatorId
     * @param groupId
     * @param memberUserId
     * @return
     */
    public void addGroupMemberList(List<ImGroupMember> members, long creatorId, long groupId, long memberUserId, int role) {
        ImGroupMember member = new ImGroupMember();
        member.setCreateTime(System.currentTimeMillis());
        member.setCreatorId(creatorId);
        member.setRole(role);
        member.setGroupId(groupId);
        member.setUserId(memberUserId);
        member.setIsBanned(unBannedTime);
        groupMemberService.insertSelective(member);
        members.add(member);
    }


    /**
     * 为进群的人添加会话
     *
     * @param userId
     * @param destId
     * @return
     */
    public ImUserSession createSession2(long userId, long destId) {
        ImUserSession imUserSession = new ImUserSession();
        imUserSession.setMsgType(IMConstants.MSG_TYPE_TEXT);
        imUserSession.setFromName("");
        imUserSession.setUserId(userId);
        imUserSession.setDestType(groupType);
        imUserSession.setDestId(destId);
        imUserSession.setUnreadCount(Long.parseLong("0"));
        imUserSession.setCreateTime(System.currentTimeMillis());
        imUserSession.setLastContent(sessionLastContent);
        imUserSession.setLastTime(System.currentTimeMillis());
        imUserSession.setFromId(userId);
        return imUserSession;
    }

    /**
     * 为进群的人添加会话
     *
     * @param userId
     * @param destId
     * @return
     */
    public ImUserSession createSession(long userId, long destId) {
        ImUserSession imUserSession = new ImUserSession();
        imUserSession.setMsgType(IMConstants.MSG_TYPE_TEXT);
        imUserSession.setFromName("");
        imUserSession.setUserId(userId);
        imUserSession.setDestType(groupType);
        imUserSession.setDestId(destId);
        imUserSession.setUnreadCount(Long.parseLong("0"));
        imUserSession.setCreateTime(System.currentTimeMillis());
        imUserSession.setLastContent(sessionLastContent);
        imUserSession.setLastTime(System.currentTimeMillis());
        imUserSession.setFromId(userId);
        userSessionService.createSession(imUserSession);
        return imUserSession;
    }

    /**
     * 为进群的人添加会话
     *
     * @param userId
     * @param destId
     * @return
     */
    public void addSessionList(List<ImUserSession> sessions, long userId, long destId) {
        sessions.add(createSession2(userId, destId));
    }

    /**
     * 根据用户的设置过滤是否可以拉群
     *
     * @param memberSet
     * @param outSet
     */
    public void getInOutIdsByUser(Set<Long> memberSet, Set<Long> outSet) {
        String s = YxUtil.setToString(memberSet);
        Long[] list = YxUtil.splitLong(s);
        for (Long uId : list) {
            //获取用户信息
            ImUserWithBLOBs user = userService.selectByPrimaryKey(uId);
            //如果无法直接进群
            if (setOn.equals(user.getIsGroupedNeedAuth())) {
                memberSet.remove(uId);
                outSet.add(uId);
            }
        }
    }


    /**
     * 给不在群的人删除会话
     *
     * @param userId
     * @param destId
     * @return
     */
    public int deleteSession(long userId, long destId) {
        return deleteFriendSession(userId, destId, groupType);
    }

    /**
     * 添加申请记录
     *
     * @param fromId      邀请人id
     * @param userId      被邀请人id
     * @param destId
     * @param auditorId
     * @param destType
     * @param content
     * @param reqStatus
     * @param auditStatus
     * @return
     */
    public ImUserRequestAudit addUserRequest2(long fromId, long userId, long destId, long auditorId, int destType, String content, int reqStatus, int auditStatus) {
        ImUserRequestAudit imUserRequestAudit = new ImUserRequestAudit();
        imUserRequestAudit.setFromId(fromId);
        imUserRequestAudit.setUserId(userId);
        imUserRequestAudit.setDestId(destId);
        imUserRequestAudit.setAuditorId(auditorId);
        imUserRequestAudit.setDestType(destType);
        imUserRequestAudit.setRequestTime(System.currentTimeMillis());
        imUserRequestAudit.setRequestContent(content);//拉人进群 用户设置需要审核 请求内容
        imUserRequestAudit.setRequestStatus(reqStatus);
        imUserRequestAudit.setAuditStatus(auditStatus);//待用户审核
        return imUserRequestAudit;
    }

    /**
     * 添加申请记录
     *
     * @param fromId      邀请人id
     * @param userId      被邀请人id
     * @param destId
     * @param auditorId
     * @param destType
     * @param content
     * @param reqStatus
     * @param auditStatus
     * @return
     */
    public ImUserRequestAudit addUserRequest(long fromId, long userId, long destId, long auditorId, int destType, String content, int reqStatus, int auditStatus) {
        ImUserRequestAudit imUserRequestAudit = new ImUserRequestAudit();
        imUserRequestAudit.setFromId(fromId);
        imUserRequestAudit.setUserId(userId);
        imUserRequestAudit.setDestId(destId);
        imUserRequestAudit.setAuditorId(auditorId);
        imUserRequestAudit.setDestType(destType);
        imUserRequestAudit.setRequestTime(System.currentTimeMillis());
        imUserRequestAudit.setRequestContent(content);//拉人进群 用户设置需要审核 请求内容
        imUserRequestAudit.setRequestStatus(reqStatus);
        imUserRequestAudit.setAuditStatus(auditStatus);//待用户审核
        requestAuditService.insertSelective(imUserRequestAudit);
        return imUserRequestAudit;
    }

    /**
     * 添加申请记录
     *
     * @param fromId      邀请人id
     * @param userId      被邀请人id
     * @param destId
     * @param auditorId
     * @param destType
     * @param content
     * @param reqStatus
     * @param auditStatus
     * @return
     */
    public void addUserRequestList(List<ImUserRequestAudit> requestAudits, long fromId, long userId, long destId, long auditorId, int destType, String content, int reqStatus, int auditStatus) {
        requestAudits.add(addUserRequest2(fromId, userId, destId, auditorId, destType, content, reqStatus, auditStatus));
    }

    /**
     * 删除会话置顶
     *
     * @param userId
     * @param destId
     * @return
     */
    public int deleteTop(long userId, long destId, int destType) {
        ImTop imTop = new ImTop();
        imTop.setUserId(userId);
        imTop.setDestId(destId);
        imTop.setDestType(destType);
        return topService.deleteByCondition(imTop);
    }

    /***
     * 通知群主的其他设备
     * @param
     * @param fromId
     * @param groupId
     * @param memberId
     * @param groupName
     * @param groupHeadUrl
     * @param
     */
    public void noticeOwner(long fromId, long groupId, long memberId, String groupName, String groupHeadUrl, int device) {
        //消息组装
        ImMessage message = msgFactory.groupSetNotice(fromId, groupId, device, MSG_TYPE_ACCEPT_GROUP, "");
        JSONObject json = new JSONObject();
        json.put("groupName", groupName);
        json.put("groupHeadUrl", groupHeadUrl);
        json.put("groupId", groupId);
        json.put("name", "您");
        json.put("userId", fromId);
        json.put("memberId", memberId);
        json.put("textTip", "name创建了群聊");
        message.setContent(json.toJSONString());
        message.setSystemType(singleSystemType);
        //通知并保存聊天记录
        chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(message), fromId);
    }

    /***
     * 通知进群的
     * @param memberSet
     * @param fromId
     * @param groupId
     * @param memberId
     * @param groupName
     * @param groupHeadUrl
     * @param inviteName
     */
    public void noticeInIds(Set<Long> memberSet, long fromId, long groupId, long memberId, String groupName, String groupHeadUrl, String inviteName) {
        //set转string 逗号分隔
        String inIds = YxUtil.setToString(memberSet);
        //通知被邀请已经进群的ids加入房间
        chatServerFeign.joinRoom(inIds, groupId);
        //消息组装
        ImMessage message = msgFactory.groupSetNotice(fromId, groupId, 0, MSG_TYPE_ACCEPT_GROUP, "");
        JSONObject json = new JSONObject();
        json.put("groupName", groupName);
        json.put("groupHeadUrl", groupHeadUrl);
        json.put("groupId", groupId);
        json.put("name", inviteName);
        json.put("userId", fromId);
        json.put("memberId", memberId);
        json.put("textTip", "name邀请你加入了群聊");
        message.setContent(json.toJSONString());
        message.setSystemType(singleSystemType);
        //通知并保存聊天记录
        chatServerFeign.notifyImServerUsersSaveSingle(JSONObject.toJSONString(message), inIds);
    }

    /**
     * 通知无法直接拉进群的
     *
     * @param outSet
     * @param userId
     * @param groupId
     */
    public void noticeNotInIds(Set<Long> outSet, long userId, long groupId) {
        ImMessage msg = msgFactory.groupSetNotice(userId, groupId, 0, MSG_TYPE_INVITE_GROUP_REQ, "");
        String out = YxUtil.setToString(outSet);
        chatServerFeign.notifyImServerUsers(JSONObject.toJSONString(msg), out);
    }

    /**
     * 通知我的其他设备
     *
     * @param msg
     * @param device
     * @param userId
     */
    public void noticeMyself(ImMessage msg, int device, long userId) {
        //更新自己
        msg.setDevType(device);
        StaticLog.info(JSONObject.toJSONString(msg));
        chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
    }


    /**
     * 通知好友并且保存两边数据
     *
     * @param msg
     * @param device
     * @param userId
     */
    public void noticeFriendAndSaveBoth(ImMessage msg, int device, long userId) {
        //更新自己
        msg.setDevType(device);
        msg.setSystemType(p2PSingleSystemType);
        StaticLog.info(JSONObject.toJSONString(msg));
        chatServerFeign.notifyImServerSaveBoth(JSONObject.toJSONString(msg), userId);
    }

    /**
     * 判断是否发送过邀请
     *
     * @param userId
     * @param destId
     * @return
     */
    public List<ImUserRequestAudit> getUserRequests(long userId, long destId) {
        ImUserRequestAudit record = new ImUserRequestAudit();
        record.setUserId(userId);
        record.setDestId(destId);
        record.setDestType(groupType);
        return requestAuditService.selectByConditions(record);
    }


    /**
     * 获取被删除的记录
     *
     * @param userId
     * @param requestId
     * @return
     */
    public ImUserRequestDeleted getUserRequestDel(long userId, long requestId) {
        ImUserRequestDeleted record = new ImUserRequestDeleted();
        record.setUserId(userId);
        record.setRequestId(requestId);
        return requestDeletedService.selectByCondition(record);
    }


    /**
     * 删除好友
     *
     * @param friend
     * @param userId
     * @return
     */
    public int deleteFriend(long friend, long userId) {
        ImFriend record = new ImFriend();
        record.setFriendId(friend);
        record.setUserId(userId);
        return friendService.deleteByCondition(record);
    }

    /**
     * 给不在群的人删除会话
     *
     * @param userId
     * @param destId
     * @return
     */
    public int deleteFriendSession(long userId, long destId, int destType) {
        ImUserSession imUserSession = new ImUserSession();
        imUserSession.setUserId(userId);
        imUserSession.setDestId(destId);
        imUserSession.setDestType(destType);
        return userSessionService.deleteByCondition(imUserSession);
    }

    /**
     * 更新在线的redis 缓存
     *
     * @param userId
     * @param device
     */
    public void setOnline(long userId, int device) {
        //更新在线设备
        String onlineDevice = RedisUtil.hget("onlineDevice", String.valueOf(userId));
        String str;
        if (StringUtils.isEmpty(onlineDevice)) {
            str = String.valueOf(device);
        } else {
            onlineDevice = onlineDevice + "," + String.valueOf(device);
            Set<Long> strings = YxUtil.splitSet(onlineDevice);
            str = YxUtil.setToString(strings);
        }
        RedisUtil.hset("onlineDevice", String.valueOf(userId), str);
    }


    /**
     * 批量通知
     */
    public void batchNoticeMembers(String userIds, long fromId, long destId, int device, int msgType, String content) {
        //组装消息
        ImMessage msg = msgFactory.groupSetNotice(fromId, destId, device, msgType, content);
        //推送ids
        StaticLog.info("joinmsg" + JSONObject.toJSONString(msg));
        chatServerFeign.notifyImServerUsers(JSONObject.toJSONString(msg), userIds);
    }

    /**
     * 批量通知 全部成员
     */
    public void batchNoticeAllMembers(long fromId, long destId, int device, int msgType, String content) {
        //组装消息
        ImMessage msg = msgFactory.groupSetNotice(fromId, destId, device, msgType, content);
        //推送ids
        chatServerFeign.notifyGroupAll(JSONObject.toJSONString(msg));
    }

    /**
     * 批量通知
     */
    public void batchNoticeMembersSave(String userIds, long fromId, long destId, int device, int msgType, String content) {
        //组装消息
        ImMessage msg = msgFactory.groupSetNotice(fromId, destId, device, msgType, content);
        msg.setSystemType(singleSystemType);
        //推送ids
        chatServerFeign.notifyImServerUsersSaveSingle(JSONObject.toJSONString(msg), userIds);
    }

    /**
     * 批量通知 全部成员
     */
    public void batchNoticeAllMembersSave(long fromId, long destId, int device, int msgType, String content) {
        //组装消息
        ImMessage msg = msgFactory.groupSetNotice(fromId, destId, device, msgType, content);
        msg.setSystemType(groupSystemType);
        //推送ids
        chatServerFeign.notifyGroupAllAndSaveHistory(JSONObject.toJSONString(msg));
    }

    /**
     * 通知 单个 保存
     */
    public void noticeSingleAndSave(long fromId, long destId, int device, int msgType, String content) {
        //组装消息
        ImMessage msg = msgFactory.groupSetNotice(fromId, destId, device, msgType, content);
        msg.setSystemType(p2PSingleSystemType);
        //fromId和msg接收人id相同时 不用setgeoid
        chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(msg), fromId);
    }

    /**
     * 批量通知并且保存数据(邀请进群)
     *
     * @param inMemberList 被拉进来的人
     * @param fromId
     * @param destId
     * @param device
     * @param msgType
     * @param content
     */
    public void batchNoticeInviteMembers(List<ImGroupMember> inMemberList, long fromId, long destId, long memberId, int device, int msgType, String content) {
        ImMessage message = msgFactory.groupSetNotice(fromId, destId, device, msgType, "");
        StringBuilder memberNames = new StringBuilder();
        ImUserWithBLOBs fromUser = userService.selectByPrimaryKey(fromId);
        String inviteUserString;
        JSONObject json = new JSONObject();

        //邀请的人
        if (!inMemberList.isEmpty()) {
            for (ImGroupMember member1 : inMemberList) {
                if (!member1.getUserId().equals(fromId)) {
                    ImUserWithBLOBs inviteUser = userService.selectByPrimaryKey(member1.getUserId());
                    memberNames.append(inviteUser.getNickName()).append(",");
                }
            }
            ImGroupWithBLOBs group = groupService.selectByPrimaryKey(destId);
            json.put("groupName", group.getName());
            json.put("groupHeadUrl", group.getHeadUrl());
            json.put("groupId", destId);
            json.put("name", fromUser.getNickName());
            json.put("userId", fromUser.getId());
            json.put("memberId", memberId);
            if (StringUtils.isNotEmpty(String.valueOf(memberNames))) {
                inviteUserString = "邀请" + memberNames.substring(0, memberNames.lastIndexOf(",")) + content;
                json.put("textTip", "name" + inviteUserString);
            } else {
                json.put("textTip", "");
            }
        }
        JSONArray array = new JSONArray();
        for (ImGroupMember member : inMemberList) {
            message.setDevType(device);
            //只推送群成员
            ImUserWithBLOBs imUser = userService.selectByPrimaryKey(member.getUserId());
            JSONObject userJson = new JSONObject();
            userJson.put("name", imUser.getNickName());
            userJson.put("headUrl", imUser.getHeadUrl());
            userJson.put("memberId", member.getId());
            array.add(userJson);
        }
        //推送给群成员
        if (array.size() > 0) {
            json.put("memberArray", array);
            message.setContent(json.toJSONString());
            message.setSystemType(groupSystemType);
            //通知群内的人
            chatServerFeign.notifyGroupAllAndSaveHistory(JSONObject.toJSONString(message));
        }

    }
}
