package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImMessageHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ImMessageHistoryMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImMessageHistory record);

    int insertSelective(ImMessageHistory record);

    ImMessageHistory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImMessageHistory record);

    int updateByPrimaryKeyWithBLOBs(ImMessageHistory record);

    int updateByPrimaryKey(ImMessageHistory record);

    int batchInsert(List<ImMessageHistory> list);

    /**
     * 根据用户ID查找
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> selectByBelongUserId(@Param("userId") Long userId);

    /**
     * 删除单项消息好友
     *
     * @param belongUserId
     * @param destId
     * @return
     */
    int deleteByOneWayFriend(@Param("belongUserId") Long belongUserId, @Param("destId") Long destId);

    /**
     * 删除单项消息群组
     *
     * @param belongUserId
     * @param destId
     * @return
     */
    int deleteByOneWayGroup(@Param("belongUserId") Long belongUserId, @Param("destId") Long destId);

    /**
     * 删除双向好友消息
     *
     * @param fromId
     * @param destId
     * @return
     */
    int deleteByBothWayFriend(@Param("fromId") Long fromId, @Param("destId") Long destId);

    /**
     * 删除群组消息
     *
     * @param groupId
     * @return
     */
    int deleteByBothWayGroup(@Param("groupId") Long groupId);

    /**
     * 根据msgId删除单条记录
     *
     * @param msgId
     * @return
     */
    int deleteByMsgId(@Param("msgId") String msgId);

    /**
     * 根据用户id和msgId单条删除
     *
     * @param msgId
     * @param userId
     * @return
     */
    int deleteByMsgIdAndUserId(@Param("msgId") String msgId, @Param("userId") Long userId);
}