package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImUserRequestDeleted;
import com.wxzd.im.ms.mapper.ImUserRequestDeletedMapper;
import com.wxzd.im.ms.service.ImUserRequestDeletedService;
import com.wxzd.im.ms.utils.YxUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ImUserRequestDeletedServiceImpl implements ImUserRequestDeletedService {
    @Autowired
    private ImUserRequestDeletedMapper imUserRequestDeletedMapper;


    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imUserRequestDeletedMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImUserRequestDeleted record) {
        return this.imUserRequestDeletedMapper.insert(record);
    }

    @Override
    public int insertSelective(ImUserRequestDeleted record) {
        return this.imUserRequestDeletedMapper.insertSelective(record);
    }

    @Override
    public ImUserRequestDeleted selectByPrimaryKey(Long id) {
        return this.imUserRequestDeletedMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImUserRequestDeleted record) {
        return this.imUserRequestDeletedMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ImUserRequestDeleted record) {
        return this.imUserRequestDeletedMapper.updateByPrimaryKey(record);
    }

    @Override
    public ImUserRequestDeleted selectByCondition(ImUserRequestDeleted record) {
        return this.imUserRequestDeletedMapper.selectByCondition(record);
    }

    @Override
    public int deleteRequest(String ids, long userId) {
        int res = 0;
        Long[] requestIds = YxUtil.splitLong(ids);
        List<ImUserRequestDeleted> id1 = new ArrayList<>();
        //防止重复删除
        //先删除
        imUserRequestDeletedMapper.batchDeleteRequest(Arrays.asList(requestIds));
        //后新增
        for (int i = 0; i < requestIds.length; i++) {
            ImUserRequestDeleted o = new ImUserRequestDeleted();
            o.setRequestId(requestIds[i]);
            o.setUserId(userId);
            o.setCreateTime(System.currentTimeMillis());
            id1.add(o);
        }
        imUserRequestDeletedMapper.batchInsertRequestDeleted(id1);
        res++;
        return res;
    }
}
