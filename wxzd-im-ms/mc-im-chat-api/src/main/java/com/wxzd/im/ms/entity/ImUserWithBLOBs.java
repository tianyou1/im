package com.wxzd.im.ms.entity;

public class ImUserWithBLOBs extends ImUser {
    private String headUrl;

    private String feedBackImage;

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl == null ? null : headUrl.trim();
    }

    public String getFeedBackImage() {
        return feedBackImage;
    }

    public void setFeedBackImage(String feedBackImage) {
        this.feedBackImage = feedBackImage == null ? null : feedBackImage.trim();
    }
}