package com.wxzd.im.ms.parameter;

import static com.wxzd.im.ms.parameter.StaticConstant.*;

public class ReturnConstant {

    /**********************系统******************************/


    //临时身份ID有效时间到期
    public static final String tempIdentityEndTimeReturn = "临时身份已过期";

    //临时身份ID使用次数已达有效次数
    public static final String tempIdentityEndUseReturn = "临时身份次数已用完";

    //未找到临时身份ID
    public static final String tempIdentityNoFoundReturn = "未找到临时身份";

    //临时身份ID有效时间到期
    public static final String tempIdEndTimeReturn = "临时身份ID已过期";

    //临时身份ID使用次数已达有效次数
    public static final String tempIdEndUseReturn = "临时身份ID次数已用完";

    //未找到临时身份ID
    public static final String tempIdNoFoundReturn = "临时身份ID已失效";

    //二维码有效时间有误
    public static final String QRCodeCurrentTimeReturn = "二维码有效时间有误";

    //二维码有效次数有误
    public static final String QRCodeCurrentTimesReturn = "二维码有效次数有误";

    //二维码有效时间到期
    public static final String QRCodeEndTimeReturn = "二维码已过期";

    //二维码可用次数为0
    public static final String QRCodeEndUseReturn = "二维码次数已用完";

    //二维码手动失效
    public static final String QRCodeManualExpiredReturn = "二维码已失效";

    //生成临时身份 总次数不能设置小于或等于0
    public static final String tempIdEffectiveTimeZeroReturn = "总次数不能设置小于或等于0";

    //其他应用二维码
    public static final String otherAppQRCodeReturn = "当前不是有效二维码";

    //网络异常
    public static final String errorReturn = "当前网络异常，请检查后再试";

    //系统报错返回
    public static final String sysErrorReturn = "当前网络繁忙，请稍后再试";

    //验证码错误，请核对验证码后再次输入。(手机 邮箱 谷歌)
    public static final String secCheckErrorReturn ="验证码错误，请核对验证码后再次输入。";

    //密保口令错误（二级密码）
    public static final String secCheckPwdErrorReturn ="密保口令错误";

    //参数不正确
    public static final String paramErrorReturn = "参数不正确";

    //操作失败
    public static final String operationErrorReturn = "操作失败";

    //获取失败
    public static final String getErrorReturn = "获取失败";

    //添加失败
    public static final String addErrorReturn = "添加失败";

    //注销失败
    public static final String destroyErrorReturn = "注销失败";

    //设置失败
    public static final String setErrorReturn = "设置失败";

    //操作成功
    public static final String operationSuccessReturn = "操作成功";

    //获取成功
    public static final String getSuccessReturn = "获取成功";

    //添加成功
    public static final String addSuccessReturn = "添加成功";

    //申请已发送
    public static final String addApplySuccessReturn = "申请已发送";

    //注销成功
    public static final String destroySuccessReturn = "注销成功";

    //设置成功
    public static final String setSuccessReturn = "设置成功";

    //通知已处理
    public static final String alreadyHandleReturn = "通知已处理";

    //请求已过期
    public static final String msgTimeOutReturn = "请求已过期";

    //反馈内容
    public static final String maxSuggestionNumReturn = "反馈内容最多不超过" + suggestionMaxL + "个字";

    //当前不在私密模式中
    public static final String notInSecretModelReturn = "当前不在私密模式中";


    /**********************个人******************************/

    //隐私密码错误
    public static final String secretPwdErrorReturn = "隐私密码错误";

    //密码错误
    public static final String pwdErrorReturn = "密码错误";

    //账号格式有误
    public static final String accountFormatReturn = "账号可以是大小写字母、数字和常用符号，长度4-32，不能为纯数字";

    //账号包含空格
    public static final String accountBlankFormatReturn = "请检查账号是否包含空格";

    //密码格式有误
    public static final String passwordFormatReturn = "密码大写英文字母、小写英文字母、数字、常用符号组成的长度为8-16个字符的字符串。且其中至少包含1个英文字母和1个数字";

    //二次验证密码最多不超过16个字
    public static final String secondaryPwdReturn = "验证信息最多不超过" + secondaryPwdMaxL + "个字";

    //昵称最多不超过16个字
    public static final String nickNameReturn = "昵称最多不超过" + nickNameMaxLength + "个字";

    //个性签名最多不超过40个字
    public static final String signReturn = "个性签名最多不超过" + signMaxLength + "个字";

    //请设置销毁时间
    public static final String destroyTimeReturn = "请设置销毁时间";

    //二次验证注销 验证信息错误
    public static final String checkInfoErrorReturn = "验证信息错误";

    //验证时间过期，请重新注销
    public static final String checkInfoTimeOutReturn = "验证时间过期，请重新注销";

    //验证时间过期，请重新注销
    public static final String checkInfoFailReturn = "验证失败，请重新获取";

    //临时操作限二次验证后5分钟，您已超时，请重新验证
    public static final String tempOperationTimeOutReturn = "验证时间过期，请重新注销";

    //注册成功
    public static final String registerSuccess = "注册成功";

    //账户已存在
    public static final String accountExist = "账户已存在";

    //未设置私密模式密码，无法使用此功能
    public static final String noSecretModePwdReturn = "未设置私密模式密码，无法使用此功能";


    /**********************好友******************************/

    //被添加者账号失效
    public static final String destAccountInvalidReturn = "对方的账号已失效";

    //已是好友
    public static final String alreadyFriendReturn = "您已是对方的好友";

    //对方不是你的好友
    public static final String notFriendReturn = "对方不是您的好友";

    //你不是对方的好友
    public static final String notFriendBothReturn = "您不是对方的好友";

    //好友上限（限定人数3000人）
    public static final String maxFriendNumReturn = "当前好友人数已满";

    //好友上限（限定人数3000人对方）
    public static final String maxOppositeFriendNumReturn = "好友当前好友人数已满";

    //好友申请说明
    public static final String maxFriendReqNumReturn = "申请说明最多不超过" + friendReqMaxL + "个字";

    //好友备注
    public static final String maxRemarkNumReturn = "好友备注最多不超过" + remarkMaxL + "个字";

    //添加自己
    public static final String addMyselfReturn = "不能添加自己";

    //名片
    public static final String accountLoseEfficacy = "名片已失效";

    //好友未开启群聊添加
    public static final String addByGroupCloseReturn = "用户未开启群聊添加";

    //好友未开启名片添加
    public static final String addByAccountCloseReturn = "用户未开启名片添加";

    //好友未开启ID添加
    public static final String addByIdCloseReturn = "用户未开启ID添加";

    //禁止群成员互加
    public static final String groupAddByFriendCloseReturn = "禁止群成员互加";

    //你们已经成为好友
    public static final String friendEachReturn = "你们已经成为好友";

    //申请已发送，等待对方处理
    public static final String reqAlreadySendReturn = "申请已发送，等待对方处理";

    //不能查自己id
    public static final String findMyselfIdReturn = "不能查自己id";

    //不能查自己账号
    public static final String findMyselfAccountReturn = "不能查自己账号";

    //请先验证聊天记录密码
    public static final String checkHistoryPwdReturn = "请先验证聊天记录密码";
    /**********************群组******************************/

    //已在本群
    public static final String inGroupReturn = "您已在群聊中";

    //不在本群
    public static final String notInGroupReturn = "您不在群聊中";

    //对方已在本群
    public static final String destInGroupReturn = "对方已在群聊中";

    //对方不在本群
    public static final String destNotInGroupReturn = "对方不在群聊中";

    //群满人
    public static final String groupNumReturn = "当前群聊人数已满（群人员上限" + maxGroupNum + "人）";

    //群解散
    public static final String groupDismissReturn = "该群聊已解散";

    //群不存在
    public static final String noGroupReturn = "该群聊不存在";

    //群名称字数超过
    public static final String groupNameReturn = "群名称最多不超过" + groupNameLength + "个字";

    //当前用户角色权限不足
    public static final String withoutPermissionReturn = "当前用户角色权限不足";

    //群公告字数超过
    public static final String groupBulletinNumReturn = "群公告不能超过" + groupBulletinLength + "个字";

    //管理员超过上限
    public static final String managersNumReturn = "群管理员人数已达上限";

    //转让时管理员超过上限
    public static final String transferManagerNumReturn = managersNumReturn + "，请先取消一个管理员再进行转让操作";

    //当前群无法查询群成员信息
    public static final String canNotSeeGroupMemberInfoReturn = "当前群无法查询群成员信息";

    //单次邀请人数上限
    public static final String maxInviteNumReturn = "单次邀请人数不能超过" + maxInviteNum + "个";

    //已邀请加群
    public static final String alreadyInviteReturn = "您已邀请过对方，请等待对方答复";

    //已申请加群
    public static final String alreadyApplyReturn = "您已申请加群，请等待审核";

    //群内未开启匿名聊天
    public static final String anonymousCloseReturn = "群内未开启匿名聊天";

    //未设置匿名名称
    public static final String anonymousNameUnSetReturn = "请先设置匿名名称";

    //群内截屏通知未开启
    public static final String screenshotTipCloseReturn = "群内截屏通知未开启";

    //群已解散
    public static final String groupDissolveReturn = "群已解散";

    //群已冻结
    public static final String groupFreezeReturn = "群已冻结";

    //该群暂无法加入
    public static final String groupCanNotInReturn = "该群暂无法加入";

    /**********************文件******************************/

    //文件超过系统要求的大小
    public static final String sizeNoAllowedReturn = "文件过大";

    //文件格式不正确
    public static final String formatErrorReturn = "文件格式不正确";
}
