package com.wxzd.im.ms.utils;

public class CommonUtils {
    /**
     * 获取用户id
     *
     * @param token
     * @return
     */
    public static Long getUserIdByToken(String token) {
        return Long.parseLong(Utils.decrypt(token.split("\\|")[1]));
    }

    /**
     * 获取是否私密
     *
     * @param token
     * @return
     */
    public static String getSecretByToken(String token) {
        return Utils.decrypt(token.split("\\|")[3]);
    }

    /**
     * 获取设备
     *
     * @param token
     * @return
     */
    public static Integer getDevTypeByToken(String token) {
        return Integer.parseInt(Utils.decrypt(token.split("\\|")[2]));
    }

    /**
     * 如果是null返回""
     *
     * @param value
     * @return
     */
    public static String ifNullValue(String value) {
        return value == null ? "" : value;
    }

    /**
     * 阅后即焚的时间
     *
     * @param time
     * @return
     */
    public static String getEphemeralChatTime(int time) {
        String timeStr;
        if (time < 60) {
            timeStr = time + "秒";
        } else if (time < 3600) {
            timeStr = time / 60 + "分钟";
        } else if (time < 86400) {
            timeStr = time / 60 / 60 + "小时";
        } else if (time < 604800) {
            timeStr = time / 60 / 60 / 24 + "天";
        } else if (time == 604800) {
            timeStr = time / 60 / 60 / 24 / 7 + "星期";
        } else {
            timeStr = time / 60 / 60 / 24 + "天";
        }
        return timeStr;
    }

    /**
     * 禁言的时间
     *
     * @param time
     * @return
     */
    public static String getBannedTime(int time) {
        String timeStr;
        if (time < 60) {
            timeStr = time + "分钟";
        } else if (time < 1440) {
            timeStr = time / 60 + "小时";
        } else if (time < 43200) {
            timeStr = time / 60 / 24 + "天";
        } else {
            timeStr = time + "分钟";
        }
        return timeStr;
    }
}
