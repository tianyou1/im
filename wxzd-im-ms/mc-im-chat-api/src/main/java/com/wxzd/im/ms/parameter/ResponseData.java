package com.wxzd.im.ms.parameter;

import java.io.Serializable;

public class ResponseData implements Serializable {

    private static final long serialVersionUID = -8877305990016647577L;
    private int code;
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


}
