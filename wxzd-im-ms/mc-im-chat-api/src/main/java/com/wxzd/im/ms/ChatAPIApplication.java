package com.wxzd.im.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient(autoRegister = true)
@EnableSwagger2
@EnableFeignClients
@EnableTransactionManagement
@EnableScheduling
@EnableCaching  //开启缓存
public class ChatAPIApplication {

    public static void main(String[] args) {
        System.out.println("接口服务准备启动！");
        SpringApplication.run(ChatAPIApplication.class, args);
        System.out.println("接口服务启动成功！");
    }

}