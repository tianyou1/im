package com.wxzd.im.ms.api.feignclient.config;

import feign.codec.Encoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class FeignConfig {
    @Bean
    public Encoder encoder() {
        return new MyMultipartEncoder();
    }


}
