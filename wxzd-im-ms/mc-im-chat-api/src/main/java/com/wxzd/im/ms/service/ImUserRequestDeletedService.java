package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImUserRequestDeleted;

public interface ImUserRequestDeletedService {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserRequestDeleted record);

    int insertSelective(ImUserRequestDeleted record);

    ImUserRequestDeleted selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserRequestDeleted record);

    int updateByPrimaryKey(ImUserRequestDeleted record);

    ImUserRequestDeleted selectByCondition(ImUserRequestDeleted record);

    /**
     * 删除申请接口
     *
     * @param ids
     * @param userId
     * @return
     */
    int deleteRequest(String ids, long userId);
}