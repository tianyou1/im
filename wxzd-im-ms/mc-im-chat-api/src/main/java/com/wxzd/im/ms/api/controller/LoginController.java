package com.wxzd.im.ms.api.controller;

import cn.hutool.core.date.DateField;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.zxing.WriterException;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import static com.wxzd.im.ms.parameter.ReturnConstant.*;
import static com.wxzd.im.ms.parameter.StaticConstant.QRCodePCType;
import static com.wxzd.im.ms.parameter.StaticConstant.setOn;
import static com.wxzd.im.ms.utils.TokenUtil.generateUserToken;

@RestController
@Api(value = "登录注册模块部分方法")
@RequestMapping(value = "/login", method = RequestMethod.POST)
@SuppressWarnings({"unchecked", "rawtypes"})
public class LoginController {
    /**
     * QREndTime 二维码过期时间 codeEndTime code过期时间 unScan 未扫描 scanning 已扫描 fail 过期 success成功
     */
    private final static int QREndTime = 300;

    private final static int codeEndTime = 15;

    private final static String unScan = "0";

    private final static String scanning = "1";

    private final static String fail = "2";

    private final static String success = "4";

    /**
     * 二维码code获取接口（待登录端）
     *
     * @param device 登录设备类型
     * @return ResponseData
     */
    @ApiOperation("二维码code获取接口（待登录端）")
    @RequestMapping("/getLoginCode")
    @ResponseBody
    public ResponseData getLoginCode(@ApiParam("登录设备类型，1为安卓2为苹果3为pc") @RequestParam(defaultValue = "0") int device) {
        HashMap<String, Object> data = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", QRCodePCType);
            jsonObject.put("data", IdUtil.simpleUUID() + "|" + device);
            String QRCode = QrCodeCreateUtil.createQrCode(JSON.toJSONString(jsonObject));
            data.put("QRCode", QRCode);
            data.put("code", jsonObject.getString("data"));
            data.put("expiryTime", DateUtil.getTimeString(cn.hutool.core.date.DateUtil.offset(new Date(), DateField.SECOND, QREndTime), "yyyy-MM-dd HH:mm:ss"));
            //
            TokenUtil.generateUserCodeCheck(jsonObject.getString("data"), jsonObject.getString("data"), QREndTime);
            return YxUtil.createSuccessData(data);
        } catch (IOException | WriterException e) {
            return YxUtil.createFail(operationErrorReturn);
        }
    }

    /**
     * 二维码code获取token接口（待登录端）
     *
     * @param code 登录的code
     */
    @ApiOperation("二维码code获取token接口（待登录端）")
    @RequestMapping("/getLoginToken")
    @ResponseBody
    public ResponseData getLoginToken(@ApiParam("登录的code") @RequestParam() String code) {
        HashMap<String, Object> data = new HashMap<>();
        String value = TokenUtil.getUserCodeCheck(code);
        data.put("token", "");
        data.put("syncMsg", "");
        if (value.equals("")) {
            data.put("status", fail);
        } else if (value.equals(code)) {
            data.put("status", unScan);
        } else if (value.equals(scanning)) {
            data.put("status", scanning);
        } else {
            JSONObject jsonObject = JSONObject.parseObject(value);
            String token = jsonObject.getString("token");
            String syncMsg = jsonObject.getString("syncMsg");
            data.put("token", token);
            data.put("syncMsg", syncMsg);
            data.put("status", success);
        }
        return YxUtil.createSuccessData(data);
    }


    /**
     * 二维码已扫通知接口（手机扫描端）
     *
     * @param token token
     * @param code  登录的code
     * @return ResponseData
     */
    @ApiOperation("二维码已扫通知接口（手机扫描端）")
    @RequestMapping("/codeScan")
    @ResponseBody
    public ResponseData codeScan(@RequestHeader String token,
                                 @ApiParam("登录的code") @RequestParam() String code) {
        TokenUtil.setUserCode(code, scanning);
        return YxUtil.createSimpleSuccess(operationSuccessReturn);
    }

    /**
     * 扫码生成token接口（手机扫描端）
     *
     * @param token   token
     * @param code    登录的code
     * @param syncMsg 是否同步消息0不同步1同步
     * @return ResponseData
     */
    @ApiOperation("扫码生成token接口（手机扫描端）")
    @RequestMapping("/codeLogin")
    @ResponseBody
    public ResponseData codeLogin(@RequestHeader String token,
                                  @ApiParam("登录的code") @RequestParam() String code,
                                  @ApiParam("是否同步消息0不同步1同步") @RequestParam() int syncMsg) {
        long userId = CommonUtils.getUserIdByToken(token);
        String[] strings = code.split("\\|");
        if (strings.length == 0) {
            return YxUtil.createFail(paramErrorReturn);
        }
        int device = Integer.valueOf(strings[1]);
        int sync = 0;
        if (syncMsg == setOn) {
            sync = 1;
        }
        String newToken = generateUserToken(userId, device, TokenEnum.tokenValid.getCode());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("syncMsg", sync);
        jsonObject.put("token", newToken);
        TokenUtil.generateUserCodeCheck(code, JSON.toJSONString(jsonObject), codeEndTime);
        return YxUtil.createSimpleSuccess(operationSuccessReturn);
    }
}
