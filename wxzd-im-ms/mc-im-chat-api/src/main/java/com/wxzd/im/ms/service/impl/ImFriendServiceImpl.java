package com.wxzd.im.ms.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.mapper.ImFriendMapper;
import com.wxzd.im.ms.parameter.IMConstants;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.CheckUtil;
import com.wxzd.im.ms.utils.YxUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.wxzd.im.ms.parameter.CacheConstants.*;
import static com.wxzd.im.ms.parameter.ReturnConstant.operationErrorReturn;
import static com.wxzd.im.ms.parameter.ReturnConstant.operationSuccessReturn;
import static com.wxzd.im.ms.parameter.StaticConstant.*;

@Service
public class ImFriendServiceImpl implements ImFriendService {
    @Autowired
    private ImFriendMapper imFriendMapper;
    @Autowired
    private MessageFactory msgFactory;
    @Autowired
    private ImUserRequestAuditService userRequestAuditService;
    @Autowired
    private ImUserRequestDeletedService userRequestDeletedService;
    @Autowired
    private ImUserSessionService userSessionService;
    @Autowired
    private ImUserService userService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CacheService cacheService;

    @Autowired
    private ChatServerFeign chatServerFeign;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imFriendMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImFriend record) {
        return this.imFriendMapper.insert(record);
    }

    @Override
    public int insertSelective(ImFriend record) {
        return this.imFriendMapper.insertSelective(record);
    }

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = friendSetCache, key = "#record.userId"),
                    @CacheEvict(value = countFriendCache, key = "#record.userId"),

            })
    public void friendNumChange(ImFriend record) {

    }

    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = userSecretFriendCacheMap, key = "#record.userId+':'+#record.friendId"),
            })
    public void secretFriendChange(ImFriend record) {

    }

    @Override
    //@Cacheable(cacheNames = friendCache,key ="#id",unless = "#result == null")
    public ImFriend selectByPrimaryKey(Long id) {
        return this.imFriendMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImFriend record) {
        return this.imFriendMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    @Caching(
            put = {
                    @CachePut(value = friendByFriendCache, key = "#record.userId+':'+#record.friendId"),
            },
            evict = {
                    //修改ImFriend 与ImUser无关
                    @CacheEvict(value = friendDetailCache, key = "#record.userId+':*'"),
                    @CacheEvict(value = userFriendCacheMap, key = "#record.userId+':'+#record.friendId"),
            })
    public ImFriend updateFriendCache(ImFriend record) {
        return record;
    }

    @Override
    public int updateByPrimaryKey(ImFriend record) {
        return this.imFriendMapper.updateByPrimaryKey(record);
    }

    @Override
    @Cacheable(cacheNames = friendByFriendCache, key = "#friend.userId+':'+#friend.friendId", unless = "#result == null")
    public ImFriend selectByFriendIdAndUserId(ImFriend friend) {
        return this.imFriendMapper.selectByFriendIdAndUserId(friend);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = friendByFriendCache, key = "#record.userId+':'+#record.friendId"),
            @CacheEvict(value = friendSetCache, key = "#record.userId"),
            @CacheEvict(value = countFriendCache, key = "#record.userId"),

            @CacheEvict(value = userFriendCacheMap, key = "#record.userId+':'+#record.friendId"),
    })
    public int deleteByCondition(ImFriend record) {
        return this.imFriendMapper.deleteByCondition(record);
    }

    @Override
    public List<ImFriend> selectFriends(Long userId) {
        return this.imFriendMapper.selectFriends(userId);
    }

    @Override
    public int updateByUserIdAndFriendIdAgree(String remark, Integer isSecretFriend, Long userId, Long friendId) {
        return imFriendMapper.updateByUserIdAndFriendIdAgree(remark, isSecretFriend, userId, friendId);
    }

    @Override
    public List<Map<String, Object>> selectSecretList(Long userId, String name) {
        return imFriendMapper.selectSecretList(userId, name);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public int requestFriendById(long userId, long destId, int sourceType, int isSecretFriend, String remark, String requestContent) {
        // 查询目标用户
        ImUserWithBLOBs destUser = userService.selectByPrimaryKey(destId);
        if (destUser == null) {
            return -1;
        }
        ImFriend singleFriend = cacheService.getFriend(destId, userId);
        if (singleFriend != null && singleFriend.getIsFriend().equals(isFriendBoth) && singleFriend.getId() != null) {
            ImFriend userFriend = cacheService.getFriend(userId, destId);
            if (userFriend != null && userFriend.getIsFriend().equals(isFriendBoth)) {
                return 3;
            }
            // 如果是单向好友；则直接成为好友,发送我们已经成为好友的信息
            ImFriend friend = new ImFriend();
            friend.setCreaterId(userId);
            friend.setCreateTime(System.currentTimeMillis());
            friend.setFriendId(destId);
            friend.setIsFriend(isFriendBoth);
            friend.setUserId(userId);
            friend.setSourceType(sourceType);
            friend.setIsSecretFriend(isSecretFriend);
            friend.setRemark(remark);


            //删除好友 和好友缓存
            commonService.deleteFriend(destId, userId);

            insertSelective(friend);
            //更新私密好友缓存
            cacheService.secretFriendChange(friend);
            //更新 缓存
            cacheService.friendNumChange(friend);


            //表示对方已经同意成为好友
            ImMessage msg = msgFactory.acceptFriendNotice(destId, userId);
            chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);

            //发送自己这边成为好友的推送
            String helloWord = "我们已经成为好友，现在开始聊天吧";
            ImMessage msgToSelf = msgFactory.textMessage(destId, userId, helloWord);
            chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(msgToSelf), userId);

            updateSingleInitSession(msgToSelf);
            return 1;
        }
        if (destUser.getNeedAuth() == 0) {
            ImFriend userFriend = cacheService.getFriend(userId, destId);
            //如果已经为好友，则不允许重复添加
            if (singleFriend != null && userFriend != null && singleFriend.getIsFriend().equals(isFriendBoth) && userFriend.getIsFriend().equals(isFriendBoth)) {
                return 3;
            } else {
                // 对方不需要认证时，直接加双方为好友
                // 自动为好友
                return addFriendByInfo(userId, destId, true, sourceType, isSecretFriend, remark, requestContent);
            }

        } else {
            // 已发送等待对方同意
            return addFriendByInfo(userId, destId, false, sourceType, isSecretFriend, remark, requestContent);
        }
    }

    private int addFriendByInfo(long userId, long destId, boolean isDouble, int sourceType, int isSecretFriend, String remark, String requestContent) {

        if (isDouble) {

            //添加两边数据
            saveImFriendData(null, isFriendBoth, destId, userId, sourceType, remark, isSecretFriend);

            //表示对方已经同意成为好友
            ImMessage userMsg = msgFactory.acceptFriendNotice(destId, userId);
            chatServerFeign.notifyNew(JSONObject.toJSONString(userMsg), userId);

            //表示对方已经同意成为好友
            ImMessage friendMsg = msgFactory.acceptFriendNotice(userId, destId);
            chatServerFeign.notifyNew(JSONObject.toJSONString(friendMsg), destId);

            // 给双方发送一条消息，我们已经成为好友，现在开始聊天吧。
            String helloWord = "我们已经成为好友，现在开始聊天吧";

            ImMessage msgToFrend = msgFactory.textMessage(destId, userId, helloWord);
            ImMessage msgToSelf = msgFactory.textMessage(userId, destId, helloWord);
            chatServerFeign.notifyImServerAgreeFriend(JSONObject.toJSONString(msgToFrend), JSONObject.toJSONString(msgToSelf));
            //更新会话
            updateInitSession(msgToFrend);
            return 1;

        } else {
            ImUserRequestAudit imUserRequestAudit = userRequestAuditService.selectByCondition(
                    new ImUserRequestAudit()
                            .setFromId(userId)
                            .setUserId(userId)
                            .setDestId(destId).setDestType(singleType)
            );
            ImUserRequestDeleted imUserRequestDeleted = null;
            if (imUserRequestAudit != null) {
                imUserRequestDeleted = userRequestDeletedService.selectByCondition(
                        new ImUserRequestDeleted()
                                .setRequestId(imUserRequestAudit.getId())
                );
            }
            if (CheckUtil.checkFriendReqAudit(imUserRequestAudit, imUserRequestDeleted)) {
                return 2;
            }

            //添加好友请求数据
            ImFriend userFriend = cacheService.getFriend(userId, destId);
            if (userFriend != null && userFriend.getIsFriend().equals(isFriendBoth)) {
                saveImFriendData(userFriend, isFriendSignal, destId, userId, sourceType, remark, isSecretFriend);
            } else {
                saveImFriendData(null, isFriendSignal, destId, userId, sourceType, remark, isSecretFriend);
            }
            ImMessage msg = msgFactory.friendReqNotice(userId, destId);
            chatServerFeign.notifyNew(JSONObject.toJSONString(msg), destId);

            userRequestAuditService.deleteByUserIdAndFriendId(userId, destId);
            //添加到申请列表
            userRequestAuditService.insertSelective(new ImUserRequestAudit()
                    .setUserId(userId)
                    .setFromId(userId)
                    .setDestId(destId)
                    .setDestType(singleType)
                    .setRequestTime(System.currentTimeMillis())
                    .setRequestContent(requestContent)
                    .setRequestStatus(requestValidStatus)
                    .setAuditStatus(waitStatus));
            return 4;
        }
    }

    private void saveImFriendData(ImFriend userFriend, int isFriend, long userId, long destId, int sourceType, String remark, int isSecretFriend) {
        ImFriend friend = new ImFriend()
                .setCreaterId(userId)
                .setCreateTime(System.currentTimeMillis())
                .setFriendId(destId)
                .setIsBlack(unBlack)
                .setIsFriend(isFriend)
                .setReceiveTip(tipMessage)
                .setUserId(userId)
                .setIsSecretHistory(noHistorySecret)
                .setIsScreenshotTip(noScreenshotTip)
                .setIsSecretFriend(noSecret)
                .setIsEphemeralChat(noEphemeralChat)
                .setEphemeralChatTime(defaultChatTime);
        if (sourceType == businessCardType) {
            friend.setSourceType(otherBusinessCardType);
        }
        if (sourceType == idType) {
            friend.setSourceType(otherIdType);
        }
        if (sourceType == temporaryIdType) {
            friend.setSourceType(otherTemporaryIdType);
        }
        //删除好友 并删除好友缓存
        commonService.deleteFriend(destId, userId);
        insertSelective(friend);
        //更新私密好友缓存
        cacheService.secretFriendChange(friend);
        //更新 缓存
        cacheService.friendNumChange(friend);

        ImFriend friend2 = new ImFriend();
        BeanUtils.copyProperties(friend, friend2);
        friend2.setId(null)
                .setUserId(destId)
                .setFriendId(userId)
                .setRemark(remark)
                .setIsSecretFriend(isSecretFriend)
                .setSourceType(sourceType);
        if (userFriend == null) {
            //更新私密好友缓存
            cacheService.secretFriendChange(friend);
            //删除好友 并删除好友缓存
            commonService.deleteFriend(userId, destId);
            insertSelective(friend2);
            //更新 缓存
            cacheService.friendNumChange(friend);
        }
    }


    private void updateInitSession(ImMessage data) {
        String text = "我们已经成为好友，现在开始聊天吧";
        ImUserSession userSession = new ImUserSession();
        userSession.setMsgType(data.getMessageType());
        userSession.setFromName(data.getFromName());
        userSession.setUserId(data.getFromId());
        userSession.setDestType(data.getFromType());
        userSession.setDestId(data.getDestId());
        userSession.setUnreadCount(1L);
        userSession.setCreateTime(System.currentTimeMillis());
        userSession.setLastContent(text);
        userSession.setLastTime(System.currentTimeMillis());
        userSession.setFromId(data.getFromId());
        userSession.setSessionType(1);
        userSessionService.insert(userSession);

        ImUserSession friendSession = new ImUserSession();
        BeanUtils.copyProperties(userSession, friendSession);
        friendSession.setUserId(data.getDestId());
        friendSession.setDestId(data.getFromId());
        friendSession.setFromId(data.getDestId());
        userSessionService.insert(friendSession);

    }

    private void updateSingleInitSession(ImMessage data) {
        String text = "我们已经成为好友，现在开始聊天吧";
        ImUserSession userSession = new ImUserSession();
        userSession.setMsgType(data.getMessageType());
        userSession.setFromName(data.getFromName());
        userSession.setUserId(data.getDestId());
        userSession.setDestType(data.getFromType());
        userSession.setDestId(data.getFromId());
        userSession.setUnreadCount(1L);
        userSession.setCreateTime(System.currentTimeMillis());
        userSession.setLastContent(text);
        userSession.setLastTime(System.currentTimeMillis());
        userSession.setFromId(data.getDestId());
        userSession.setSessionType(1);
        userSessionService.insert(userSession);
    }

    @Override
    //@Cacheable(cacheNames = friendInfoListCache,key ="#map.get('userId')",unless = "#result == null")
    public List<Map<String, Object>> getFriendInfo(Map<String, Object> map) {
        return this.imFriendMapper.getFriendInfo(map);
    }

    @Override
    public List<Map<String, Object>> getSessionList(Map<String, Object> map) {
        return this.imFriendMapper.getSessionList(map);
    }

    @Override
    //@Cacheable(cacheNames = friendBlackListCache, key = "#map.get('userId')", unless = "#result == null or #result.size()==0")
    public List<Map<String, Object>> getBlackList(Map<String, Object> map) {
        return this.imFriendMapper.getBlackList(map);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData dealFriendRequest(long userId, int device, long friendId, int confirmState, String remark, int isSecretFriend,ImFriend userFriend,ImFriend friend) {
        //同意申请
        if (agreeState == confirmState) {
            ImMessage msg = msgFactory.acceptFriendNotice(userId, friendId);
            chatServerFeign.notifyNew(JSONObject.toJSONString(msg), friendId);
            //更新申请
            int i = userRequestAuditService.updateByUserIdAndFriendId(agreeStatus, userId, System.currentTimeMillis(), friendId);
            if (i > 0) {
                if (device > 0) {
                    // 同步消息到其它设备
                    msg = msgFactory.acceptFriendNotice(friendId, userId);
                    chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
                }
                userFriend.setRemark(remark);
                userFriend.setIsSecretFriend(isSecretFriend);
                userFriend.setIsFriend(isFriendBoth);
                int count1=updateByPrimaryKeySelective(userFriend);
                friend.setIsFriend(isFriendBoth);
                int count2=updateByPrimaryKeySelective(friend);
                if(count1>0&&count2>0){
                    cacheService.updateFriendCache(userFriend);
                    cacheService.updateFriendCache(friend);

                    // 给双方发送一条消息，我们已经成为好友，现在开始聊天吧。
                    ImMessage msgToFrend = msgFactory.textMessage(userId, friendId, "我们已经成为好友，现在开始聊天吧");
                    ImMessage msgToSelf = msgFactory.textMessage(friendId, userId, "我们已经成为好友，现在开始聊天吧");
                    chatServerFeign.notifyImServerAgreeFriend(JSONObject.toJSONString(msgToFrend), JSONObject.toJSONString(msgToSelf));
                    //为成员创建会话
                    ImUserSession userSession = new ImUserSession();
                    userSession.setMsgType(IMConstants.MSG_TYPE_TEXT);
                    userSession.setFromName("");
                    userSession.setUserId(userId);
                    userSession.setDestType(1);
                    userSession.setDestId(friendId);
                    userSession.setUnreadCount(0L);
                    userSession.setCreateTime(System.currentTimeMillis());
                    userSession.setLastContent("我们已经成为好友，现在开始聊天吧");
                    userSession.setLastTime(System.currentTimeMillis());
                    userSession.setFromId(userId);
                    userSession.setSessionType(1);
                    userSessionService.createSession(userSession);
                    ImUserSession userSession1 = new ImUserSession();
                    BeanUtils.copyProperties(userSession, userSession1);
                    //为成员创建会话
                    userSession1.setUserId(friendId);
                    userSession1.setDestId(userId);
                    userSession1.setFromId(friendId);
                    userSessionService.createSession(userSession1);
                    return YxUtil.createSimpleSuccess(operationSuccessReturn);
                }else {
                    return YxUtil.createFail(operationErrorReturn);
                }
            } else {
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            }
        } else {
            //拒绝申请,更新申请
            userRequestAuditService.updateByUserIdAndFriendId(3, userId, Clock.systemUTC().millis(), friendId);
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        }
    }


    /**
     * 设置阅后即焚判断
     *
     * @param userId
     * @param friendId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean updateCheck(long userId, long friendId, int isEphemeralChat, int ephemeralChatTime,ImFriend friend) {
        ImFriend user = cacheService.getFriend(userId, friendId);
        if (user == null || friend == null || !user.getIsFriend().equals(isFriendBoth) || !friend.getIsFriend().equals(isFriendBoth)) {
            return false;
        }
        user.setIsEphemeralChat(isEphemeralChat);
        user.setEphemeralChatTime(ephemeralChatTime);
        int update1 = this.updateByPrimaryKeySelective(user);

        friend.setIsEphemeralChat(isEphemeralChat);
        friend.setEphemeralChatTime(ephemeralChatTime);
        int update2 = this.updateByPrimaryKeySelective(friend);
        if (update1 <= 0 || update2 <= 0) {
            //如果只有一方设置成功，则重置两边的数据，恢复默认数据
            user.setIsEphemeralChat(setOff);
            user.setEphemeralChatTime(defaultChatTime);
            this.updateByPrimaryKeySelective(user);

            friend.setIsEphemeralChat(setOff);
            friend.setEphemeralChatTime(defaultChatTime);
            this.updateByPrimaryKeySelective(friend);
            cacheService.updateFriendCache(user);
            cacheService.updateFriendCache(friend);
            return false;
        } else {
            cacheService.updateFriendCache(user);
            cacheService.updateFriendCache(friend);
            return true;
        }
    }


    /**
     * 判断双方是否为好友
     *
     * @param userId
     * @param friendId
     * @return
     */
    public boolean isFriendCheck(long userId, long friendId) {
        ImFriend user = cacheService.getFriend(userId, friendId);
        ImFriend friend = cacheService.getFriend(friendId, userId);
        if (user == null || friend == null || !isFriendBoth.equals(user.getIsFriend()) || !isFriendBoth.equals(friend.getIsFriend())) {
            return false;
        }
        if(friend.getIsBlack().equals(isBlack)){
            return false;
        }
        return friend.getIsScreenshotTip().equals(screenshotTip);
    }

    @Override
    public Set<Long> selectFriendsSet(long userId) {
        return this.imFriendMapper.selectFriendsSet(userId);
    }

    @Override
    @Cacheable(cacheNames = countFriendCache, key = "#userId", unless = "#result == null")
    public int countFriends(long userId) {
        return imFriendMapper.countFriends(userId);
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = friendDetailCache, key = "#userId+':*'"),
    })
    public void deleteDetailCacheUserId(long userId) {
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = friendDetailCache, key = "'*:'+#friendId"),
    })
    public void deleteDetailCacheFriendId(long friendId) {
    }
}
