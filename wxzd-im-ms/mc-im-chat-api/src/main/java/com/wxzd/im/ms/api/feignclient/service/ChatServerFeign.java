package com.wxzd.im.ms.api.feignclient.service;


import com.wxzd.im.ms.parameter.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "chat-server-client", url = "${feign.chatServerUrl}")
public interface ChatServerFeign {

    @RequestMapping(value = "/chat/joinRoom",method = RequestMethod.POST)
    ResponseData joinRoom(@RequestParam("userIds") String userIds, @RequestParam("roomId") long roomId);

    @RequestMapping(value = "/chat/joinRoomOne",method = RequestMethod.POST)
    ResponseData joinRoomOne(@RequestParam("userId") String userId, @RequestParam("roomId") long roomId);

    @RequestMapping(value = "/chat/leaveRoom",method = RequestMethod.POST)
    ResponseData leaveRoom(@RequestParam("userId") long userId, @RequestParam("roomId") long roomId);

    @RequestMapping(value = "/chat/leaveRooms",method = RequestMethod.POST)
    ResponseData leaveRooms(@RequestParam("userId") long userId, @RequestParam("roomIds") String roomIds);


    @RequestMapping(value = "/chat/leaveRoomUserIds",method = RequestMethod.POST)
    ResponseData leaveRoomUserIds(@RequestParam("userIds") String userIds, @RequestParam("roomId") long roomId);

    /**
     * 发送消息（好友）
     *
     * @param data
     * @param userId
     * @return
     */
    @RequestMapping(value = "/chat/notifyNew",method = RequestMethod.POST)
    ResponseData notifyNew(@RequestParam("data") String data, @RequestParam("userId") long userId);

    /**
     * 发送消息并保存两边数据（好友）
     *
     * @param data
     * @param userId
     * @return
     */
    @RequestMapping(value = "/chat/notifyImServerSaveBoth",method = RequestMethod.POST)
    ResponseData notifyImServerSaveBoth(@RequestParam("data") String data, @RequestParam("userId") long userId);

    /**
     * 发送消息并保存单边数据（好友）
     *
     * @param data
     * @param userId
     * @return
     */
    @RequestMapping(value = "/chat/notifyImServerSaveSingle",method = RequestMethod.POST)
    ResponseData notifyImServerSaveSingle(@RequestParam("data") String data, @RequestParam("userId") long userId);

    /**
     * 通知好友同意
     *
     * @param msgToFriend
     * @param msgToSelf
     * @return
     */
    @RequestMapping(value = "/chat/notifyImServerAgreeFriend",method = RequestMethod.POST)
    ResponseData notifyImServerAgreeFriend(@RequestParam("msgToFriend") String msgToFriend, @RequestParam("msgToSelf") String msgToSelf);

    /**
     * 推送群消息(全部人)
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/chat/notifyGroupAll",method = RequestMethod.POST)
    ResponseData notifyGroupAll(@RequestParam("data") String data);


    /**
     * 推送群消息(全部人)
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/chat/notifyGroupAllAndSaveHistory",method = RequestMethod.POST)
    ResponseData notifyGroupAllAndSaveHistory(@RequestParam("data") String data);

    /**
     * 推送群消息(部分人并且保存)
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/chat/notifyImServerUsersSaveSingle",method = RequestMethod.POST)
    ResponseData notifyImServerUsersSaveSingle(@RequestParam("data") String data, @RequestParam("userIds") String userIds);

    /**
     * 推送群消息(部分人)
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/chat/notifyImServerUsers",method = RequestMethod.POST)
    ResponseData notifyImServerUsers(@RequestParam("data") String data, @RequestParam("userIds") String userIds);

    /**
     * 获取socket最小负载
     *
     * @return
     */
    @RequestMapping(value = "/chat/getSocketUrl",method = RequestMethod.POST)
    ResponseData getSocketUrl();

}
