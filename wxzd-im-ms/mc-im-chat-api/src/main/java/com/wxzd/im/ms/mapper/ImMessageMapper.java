package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImMessage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImMessageMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImMessage record);

    int insertSelective(ImMessage record);

    ImMessage selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImMessage record);

    int updateByPrimaryKeyWithBLOBs(ImMessage record);

    int updateByPrimaryKey(ImMessage record);
}