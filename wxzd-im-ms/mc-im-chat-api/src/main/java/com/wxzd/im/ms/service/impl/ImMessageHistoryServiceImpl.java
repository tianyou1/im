package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImMessageHistory;
import com.wxzd.im.ms.entity.ImMessageHistoryMong;
import com.wxzd.im.ms.mapper.ImMessageHistoryMapper;
import com.wxzd.im.ms.parameter.IMConstants;
import com.wxzd.im.ms.service.ImMessageHistoryService;
import com.wxzd.im.ms.utils.MongoPage;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.wxzd.im.ms.parameter.StaticConstant.*;

@Service
public class ImMessageHistoryServiceImpl implements ImMessageHistoryService {

    @Autowired
    private ImMessageHistoryMapper imMessageHistoryMapper;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imMessageHistoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImMessageHistory record) {
        return this.imMessageHistoryMapper.insert(record);
    }

    @Override
    public int insertSelective(ImMessageHistory record) {
        return this.imMessageHistoryMapper.insertSelective(record);
    }

    @Override
    public ImMessageHistory selectByPrimaryKey(Long id) {
        return this.imMessageHistoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImMessageHistory record) {
        return this.imMessageHistoryMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImMessageHistory record) {
        return this.imMessageHistoryMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImMessageHistory record) {
        return this.imMessageHistoryMapper.updateByPrimaryKey(record);
    }

    @Override
    public int batchInsert(List<ImMessageHistory> list) {
        return this.imMessageHistoryMapper.batchInsert(list);
    }

    @Override
    public List<Map<String, Object>> selectByBelongUserId(Long userId) {
        return imMessageHistoryMapper.selectByBelongUserId(userId);
    }

    @Override
    public int deleteByOneWayFriend(Long beLongUserId, Long destId) {
        return imMessageHistoryMapper.deleteByOneWayFriend(beLongUserId, destId);
    }

    @Override
    public int deleteByOneWayGroup(Long beLongUserId, Long destId) {
        return imMessageHistoryMapper.deleteByOneWayGroup(beLongUserId, destId);
    }

    @Override
    public int deleteByBothWayFriend(Long fromId, Long destId) {
        return imMessageHistoryMapper.deleteByBothWayFriend(fromId, destId);
    }

    @Override
    public int deleteByBothWayGroup(Long groupId) {
        return imMessageHistoryMapper.deleteByBothWayGroup(groupId);
    }

    @Override
    public int deleteByMsgId(String msgId) {
        return imMessageHistoryMapper.deleteByMsgId(msgId);
    }

    @Override
    public int deleteByMsgIdAndUserId(String msgId, Long userId) {
        return imMessageHistoryMapper.deleteByMsgIdAndUserId(msgId, userId);
    }

    public MongoPage getSignalHistory(long destId, int destType, int size, int pageNo, long userId, String txt, int messageType) {
        //size=15;
        //更新会话
        //if(id==0)
        MongoPage page = new MongoPage();
        page.setPageNo(pageNo);
        page.setPageSize(size);
        List<Integer> statusList = Arrays.asList(readStatus, unReadStatus);
        Query query = Query.query(Criteria.where("belongUserId").is(userId).and("fromType").is(destType).and("status").in(statusList).andOperator(new Criteria().orOperator(Criteria.where("fromId").is(destId), Criteria.where("destId").is(destId))));

        //组装查询数据
        findMongoDbHistory(txt, query, messageType, page, pageNo, size);


        return page;
    }

    public MongoPage getGroupHistory(long destId, int destType, int size, int pageNo, long userId, String txt, int messageType) {
        //size=15;
        //更新会话
        //if(id==0)
        MongoPage page = new MongoPage();
        page.setPageNo(pageNo);
        page.setPageSize(size);
        List<Integer> statusList = Arrays.asList(readStatus, unReadStatus);
        Query query = Query.query(Criteria.where("belongUserId").is(userId).and("fromType").is(destType).and("status").in(statusList).and("destId").is(destId));

        //组装查询数据
        findMongoDbHistory(txt, query, messageType, page, pageNo, size);

        return page;
    }

    private void findMongoDbHistory(String txt, Query query, int messageType, MongoPage page, int pageNo, int size) {
        //添加文字模糊查找
        if (StringUtils.isNotEmpty(txt)) {
            //是文本消息并且内容是相关的
            query.addCriteria(Criteria.where("content").regex(txt).and("messageType").is(IMConstants.MSG_TYPE_TEXT));
        }

        //添加类型搜索（如图片，文件）
        if (messageType != 0) {
            query.addCriteria(Criteria.where("messageType").is(messageType));
        }

        // 查询总数
        query.with(Sort.by(Sort.Direction.DESC, "sendTime"));

        List<ImMessageHistoryMong> datas = mongoTemplate.find(query, ImMessageHistoryMong.class);
        List<ImMessageHistoryMong> list = new ArrayList<>();
        //过滤数据
        filterData(datas, list);

        page.setTotalCount(datas.size());
        if (pageNo * size > datas.size()) {
            if ((pageNo - 1) * size > datas.size()) {
                page.setList(new ArrayList<>());
            } else {
                page.setList(datas.subList(((pageNo - 1) * size), datas.size()));
            }
        } else {
            page.setList(datas.subList(((pageNo - 1) * size), pageNo * size));
        }
    }

    private void filterData(List<ImMessageHistoryMong> datas, List<ImMessageHistoryMong> list) {
        //过滤条件
        Map<Long, ImMessageHistoryMong> map = new HashMap<>();
        ImMessageHistoryMong mapData =new ImMessageHistoryMong();
        for (ImMessageHistoryMong data : datas) {
            if (data.getReadTime() != null) {
                if (data.getIsEphemeralChat().equals(ephemeralChat) && (data.getReadTime() / 1000 + data.getEphemeralChatTime()) <= System.currentTimeMillis() / 1000 && data.getStatus().equals(readStatus)) {
                    list.add(data);
                }
            }

            //组装存入map的数据
            mapData.setSendTime(data.getSendTime());
            mapData.setImageIconUrl(data.getImageIconUrl());
            mapData.setFromId(data.getFromId());
            if (map.get(data.getFromId()) != null) {
                if (map.get(data.getFromId()).getSendTime() < data.getSendTime()) {
                    updateImageUrl(map, mapData, datas);
                }
            } else {
                updateImageUrl(map, mapData, datas);
            }
        }
        //移除元素
        datas.removeAll(list);
    }

    /**
     * 更新用户头像
     *
     * @param map
     * @param data
     * @param datas
     */
    private void updateImageUrl(Map<Long, ImMessageHistoryMong> map, ImMessageHistoryMong data, List<ImMessageHistoryMong> datas) {
        //判断需要保存最新头像是不是匿名的，如果是就跳过
        if (StringUtils.isNotBlank(data.getImageIconUrl()) && !anonymousHeadUrl.equals(data.getImageIconUrl())) {
            map.put(data.getFromId(), data);
            for (ImMessageHistoryMong data1 : datas) {
                //判断需要修改的头像是不是匿名的，如果是就跳过
                if (StringUtils.isNotBlank(data1.getImageIconUrl()) && !anonymousHeadUrl.equals(data1.getImageIconUrl())) {
                    if (data.getFromId().equals(data1.getFromId()) && data.getSendTime() > data1.getSendTime()) {
                        data1.setImageIconUrl(map.get(data.getFromId()).getImageIconUrl());
                    }
                }
            }
        }
    }


    /**
     * 根据msgId查找之前的数据
     *
     * @param destId
     * @param destType
     * @param size
     * @param msgId
     * @param userId
     * @return
     */
    public Map<String, Object> getSignalHistoryByMsgId(long destId, int destType, int size, String msgId, long userId) {
        List<Integer> statusList = Arrays.asList(readStatus, unReadStatus);
        Query query = Query.query(Criteria.where("belongUserId").is(userId).and("fromType").is(destType).and("status").in(statusList).andOperator(new Criteria().orOperator(Criteria.where("fromId").is(destId), Criteria.where("destId").is(destId)))).with(Sort.by(Sort.Direction.DESC, "sendTime"));

        return findHistoryByMsgId(query, msgId, size);
    }


    /**
     * 根据msgId查找之前的数据
     *
     * @param destId
     * @param destType
     * @param size
     * @param msgId
     * @param userId
     * @return
     */
    public Map<String, Object> getGroupHistoryByMsgId(long destId, int destType, int size, String msgId, long userId) {
        List<Integer> statusList = Arrays.asList(readStatus, unReadStatus);
        Query query = Query.query(Criteria.where("belongUserId").is(userId).and("fromType").is(destType).and("status").in(statusList).and("destId").is(destId)).with(Sort.by(Sort.Direction.DESC, "sendTime"));

        return findHistoryByMsgId(query, msgId, size);
    }


    private Map<String, Object> findHistoryByMsgId(Query query, String msgId, int size) {
        Map<String, Object> map = new HashMap<>();
        ImMessageHistoryMong imMessageHistoryMong = null;
        if (StringUtils.isNotEmpty(msgId)) {
            Query query2 = Query.query(Criteria.where("msgId").is(msgId));
            imMessageHistoryMong = mongoTemplate.findOne(query2, ImMessageHistoryMong.class);
        }

        List<ImMessageHistoryMong> messageHistoryMongs = mongoTemplate.find(query, ImMessageHistoryMong.class);
        List<ImMessageHistoryMong> list = new ArrayList<>();

        //过滤条件
        filterData(messageHistoryMongs, list);

        int msgLine = 0;

        if (imMessageHistoryMong != null&&!messageHistoryMongs.isEmpty()) {
            //查找所在下标
            for (int i = 0; i < messageHistoryMongs.size(); i++) {
                if (messageHistoryMongs.get(i).getMsgId().equals(imMessageHistoryMong.getMsgId())) {
                    msgLine = i;
                }
            }
        }

        //正面拉取
        if (size >= 0) {
            //-1是为了去掉搜索的msg的行，防止重复
            int indexStart = messageHistoryMongs.size() - msgLine - 1 - 1;
            if (indexStart < 0) {
                indexStart = 0;
            }

            //多取一条，过滤重复
            int firstSize = 0;
            if(!messageHistoryMongs.isEmpty()){
                firstSize=msgLine+1;
            }

            //多取一条，过滤重复
            int lastSize = msgLine + size + 1;
            if (lastSize > messageHistoryMongs.size()) {
                lastSize = messageHistoryMongs.size();
            }
            if (imMessageHistoryMong==null) {
                if (size > messageHistoryMongs.size()) {
                    size = messageHistoryMongs.size();
                }
                map.put("list", messageHistoryMongs.subList(0, size));
                indexStart = messageHistoryMongs.size() - 1;
                if (indexStart < 0) {
                    indexStart = 0;
                }
                map.put("startRow", indexStart);
                int indexEnd = messageHistoryMongs.size() - size;
                if (indexEnd < 0) {
                    indexEnd = 0;
                }
                map.put("endRow", indexEnd);
            } else {
                map.put("list", messageHistoryMongs.subList(firstSize, lastSize));
                map.put("startRow", indexStart);
                int indexEnd = messageHistoryMongs.size() - msgLine - size - 1;
                if (indexEnd < 0) {
                    indexEnd = 0;
                }
                map.put("endRow", indexEnd);
            }
        } else {
            //反向拉取
            //-1是为了去掉搜索的msg的行，防止重复
            int indexStart = messageHistoryMongs.size() - msgLine - 1 + 1;
            if (indexStart < 0) {
                indexStart = 0;
            }
            //多取一条，过滤重复
            int firstSize = msgLine + size;
            if (firstSize < 0) {
                firstSize = 0;
            }
            map.put("list", messageHistoryMongs.subList(firstSize, msgLine));
            map.put("startRow", indexStart);
            int indexEnd = messageHistoryMongs.size() - msgLine - size - 1;
            if (indexEnd >= messageHistoryMongs.size() - 1) {
                indexEnd = messageHistoryMongs.size() - 1;
            }
            map.put("endRow", indexEnd);
        }


        return map;
    }
}
