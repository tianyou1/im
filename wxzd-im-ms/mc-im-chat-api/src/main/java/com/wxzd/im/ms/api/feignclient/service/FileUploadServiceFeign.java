package com.wxzd.im.ms.api.feignclient.service;


import com.wxzd.im.ms.api.feignclient.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "file-system-client", url = "${feign.fileSystemUrl}", configuration = FeignConfig.class)
public interface FileUploadServiceFeign extends FileUploadService {

}
