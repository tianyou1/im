package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImMessageHistory;
import com.wxzd.im.ms.utils.MongoPage;

import java.util.List;
import java.util.Map;

public interface ImMessageHistoryService {
    int deleteByPrimaryKey(Long id);

    int insert(ImMessageHistory record);

    int insertSelective(ImMessageHistory record);

    ImMessageHistory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImMessageHistory record);

    int updateByPrimaryKeyWithBLOBs(ImMessageHistory record);

    int updateByPrimaryKey(ImMessageHistory record);

    int batchInsert(List<ImMessageHistory> list);

    /**
     * 查看消息记录根据用户ID
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> selectByBelongUserId(Long userId);

    /**
     * 获取单聊消息
     *
     * @param destId
     * @param destType
     * @param size
     * @param pageNo
     * @param userId
     * @param txt
     * @param messageType
     * @return
     */
    MongoPage getSignalHistory(long destId, int destType, int size, int pageNo, long userId, String txt, int messageType);

    /**
     * 获取群聊消息
     *
     * @param destId
     * @param destType
     * @param size
     * @param pageNo
     * @param userId
     * @param txt
     * @param messageType
     * @return
     */
    MongoPage getGroupHistory(long destId, int destType, int size, int pageNo, long userId, String txt, int messageType);

    /**
     * 删除根据用户id，目标id和类型好友
     *
     * @param beLongUserId
     * @param destId
     * @return
     */
    int deleteByOneWayFriend(Long beLongUserId, Long destId);

    /**
     * 删除根据用户id，目标id和类型群组
     *
     * @param beLongUserId
     * @param destId
     * @return
     */
    int deleteByOneWayGroup(Long beLongUserId, Long destId);

    /**
     * 删除双向好友消息
     *
     * @param fromId
     * @param destId
     * @return
     */
    int deleteByBothWayFriend(Long fromId, Long destId);

    /**
     * 删除全部群组消息
     *
     * @param groupId
     * @return
     */
    int deleteByBothWayGroup(Long groupId);

    /**
     * 根据msgId删除
     *
     * @param msgId
     * @return
     */
    int deleteByMsgId(String msgId);

    /**
     * 单项单条删除
     *
     * @param msgId
     * @return
     */
    int deleteByMsgIdAndUserId(String msgId, Long userId);


    /**
     * 单聊拉取
     *
     * @param destId
     * @param destType
     * @param size
     * @param msgId
     * @param userId
     * @return
     */
    Map<String, Object> getSignalHistoryByMsgId(long destId, int destType, int size, String msgId, long userId);


    /**
     * 群聊拉取
     *
     * @param destId
     * @param destType
     * @param size
     * @param msgId
     * @param userId
     * @return
     */
    Map<String, Object> getGroupHistoryByMsgId(long destId, int destType, int size, String msgId, long userId);

}