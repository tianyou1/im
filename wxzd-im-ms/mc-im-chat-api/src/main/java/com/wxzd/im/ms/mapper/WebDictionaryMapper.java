package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.WebDictionary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WebDictionaryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WebDictionary record);

    int insertSelective(WebDictionary record);

    WebDictionary selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WebDictionary record);

    int updateByPrimaryKey(WebDictionary record);

    /**
     * 根据类型查找
     *
     * @param type
     * @return
     */
    WebDictionary selectByType(@Param("type") String type);
}