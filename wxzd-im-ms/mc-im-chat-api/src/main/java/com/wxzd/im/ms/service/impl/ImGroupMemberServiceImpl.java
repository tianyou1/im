package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImGroupMember;
import com.wxzd.im.ms.mapper.ImGroupMemberMapper;
import com.wxzd.im.ms.service.ImGroupMemberService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.wxzd.im.ms.parameter.CacheConstants.*;

@Service
public class ImGroupMemberServiceImpl implements ImGroupMemberService {

    @Autowired
    private ImGroupMemberMapper imGroupMemberMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imGroupMemberMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImGroupMember record) {
        return this.imGroupMemberMapper.insert(record);
    }

    @Override
    @Caching(evict = {
            //群成员数
            @CacheEvict(value = countMemberCache, key = "#record.groupId"),
            //群成员list
            @CacheEvict(value = memberListCache, key = "#record.groupId"),
            //全部群成员set
            @CacheEvict(value = allMemberSetCache, key = "#record.groupId"),
            //群管理员和群主list
            @CacheEvict(value = managerAndOwnerListCache, key = "#record.groupId", condition = "#record.role==2 or #record.role==1"),
            //群成员Set
            @CacheEvict(value = groupMemberSetCache, key = "#record.groupId"),
            //群成员set id
            @CacheEvict(value = memberSetCache, key = "#record.groupId", condition = "#record.role==3"),


            //群成员列表
            //@CacheEvict(value = memberListMapCache, key = "#record.groupId"),
            //群管理员列表
            //@CacheEvict(value = managerListMapCache, key = "#record.groupId", condition = "#record.role==2"),
            //chat server
            @CacheEvict(value = allMemberListCacheMap, key = "#record.userId"),
            @CacheEvict(value = noSecretMemberListCacheMap, key = "#record.userId"),
            @CacheEvict(value = memberListCacheMap, key = "#record.groupId"),

    })
    public int insertSelective(ImGroupMember record) {
        return this.imGroupMemberMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKeySelective(ImGroupMember record) {
        return this.imGroupMemberMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    @Caching(
            put = {
                    @CachePut(value = memberCache, key = "#record.groupId+':'+#record.userId"),
                    @CachePut(value = ownerCache, key = "#record.groupId", condition = "#record.role==1"),
                    @CachePut(value = groupsMemberCache, key = "#record.id"),
            },
            evict = {
                    @CacheEvict(value = memberCacheMap, key = "#record.userId+':'+#record.groupId"),
            })
    public ImGroupMember updateGroupMemberCache(ImGroupMember record) {
        return record;
    }


    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = secretMemberCacheMap, key = "#record.userId+':'+#record.groupId"),
                    @CacheEvict(value = noSecretMemberListCacheMap, key = "#record.userId"),
            })
    public void removeSecretGroupCache(ImGroupMember record) {

    }


    @Override
    @Caching(
            evict = {
                    //chat server
                    @CacheEvict(value = allMemberListCacheMap, key = "#userId"),
            })
    public void removeGroupCache(Long userId) {

    }


    @Override
    @Caching(
            evict = {
                    //管理员数
                    @CacheEvict(value = countManagerCache, key = "#record.groupId"),
                    //群管理员set id
                    @CacheEvict(value = managerSetCache, key = "#record.groupId"),
                    //群成员set id
                    @CacheEvict(value = memberSetCache, key = "#record.groupId"),
                    //群管理员和群主list
                    @CacheEvict(value = managerAndOwnerListCache, key = "#record.groupId"),
            })
    public ImGroupMember updateGroupMemberRoleCache(ImGroupMember record) {
        return record;
    }

    @Override
    @Cacheable(cacheNames = groupsMemberCache, key = "#id", unless = "#result == null")
    public ImGroupMember selectByPrimaryKey(Long id) {
        return this.imGroupMemberMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(ImGroupMember record) {
        return this.imGroupMemberMapper.updateByPrimaryKey(record);
    }

    @Override
    public ImGroupMember selectByCondition(ImGroupMember record) {
        return this.imGroupMemberMapper.selectByCondition(record);
    }

    @Override
    public List<ImGroupMember> selectByConditions(ImGroupMember record) {
        return this.imGroupMemberMapper.selectByConditions(record);
    }

    @Override
    public int countMembers(Map<String, Object> map) {
        return this.imGroupMemberMapper.countMembers(map);
    }

    @Override
    public int deleteByCondition(ImGroupMember imGroupMember) {
        return this.imGroupMemberMapper.deleteByCondition(imGroupMember);
    }

    @Override
    public List<Long> selectOwnerAndManagers(long groupId) {
        return this.imGroupMemberMapper.selectOwnerAndManagers(groupId);
    }

    @Override
    public Set<Long> selectUserIdSet(long groupId) {
        return this.imGroupMemberMapper.selectUserIdSet(groupId);
    }

    @Override
    public List<Long> selectAllMemberUserId(@Param("groupId") Long groupId, @Param("role") Integer role) {
        return this.imGroupMemberMapper.selectAllMemberUserId(groupId, role);
    }

    @Override
    public Set<Long> selectAllMemberId(@Param("groupId") Long groupId, @Param("role") Integer role) {
        return this.imGroupMemberMapper.selectAllMemberId(groupId, role);
    }

    @Override
    @Caching(evict = {
            //群成员数
            @CacheEvict(value = countMemberCache, key = "#members.get(0).groupId"),
            //群成员list
            @CacheEvict(value = memberListCache, key = "#members.get(0).groupId"),
            //全部群成员set
            @CacheEvict(value = allMemberSetCache, key = "#members.get(0).groupId"),
            //群管理员和群主list
            @CacheEvict(value = managerAndOwnerListCache, key = "#members.get(0).groupId"),
            //群成员Set
            @CacheEvict(value = groupMemberSetCache, key = "#members.get(0).groupId"),
            //群成员set id
            @CacheEvict(value = memberSetCache, key = "#members.get(0).groupId"),


            //群成员列表
            //@CacheEvict(value = memberListMapCache, key = "#record.groupId"),
            //群管理员列表
            //@CacheEvict(value = managerListMapCache, key = "#record.groupId", condition = "#record.role==2"),
            @CacheEvict(value = memberListCacheMap, key = "#members.get(0).groupId")
    })
    public int batchInsertSelective(List<ImGroupMember> members) {
        return this.imGroupMemberMapper.batchInsertSelective(members);
    }
}
