package com.wxzd.im.ms.api.controller;

import com.wxzd.im.ms.api.config.FileUploadConfigProperties;
import com.wxzd.im.ms.api.feignclient.service.FileUploadServiceFeign;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.parameter.ResultBean;
import com.wxzd.im.ms.utils.YxUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wxzd.im.ms.parameter.ReturnConstant.formatErrorReturn;
import static com.wxzd.im.ms.parameter.ReturnConstant.sizeNoAllowedReturn;
import static com.wxzd.im.ms.parameter.StaticConstant.*;

@RestController
@Api(value = "上传接口模块")
@RequestMapping(value = "/upload", method = RequestMethod.POST)
public class UploadController {


    @Autowired
    private FileUploadServiceFeign uploadServiceFeign;

    @Autowired
    private FileUploadConfigProperties fileConfig;

    @ApiOperation("上传头像")
    @RequestMapping(value = "/uploadImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseData uploadImage(@RequestPart(value = "fileUpload") MultipartFile[] fileUpload) {
        int size = fileConfig.getHeadFileSize();
        for (MultipartFile aFile : fileUpload) {
            if (aFile.getSize() > size) {
                return YxUtil.createFail(sizeNoAllowedReturn);
            }
            String contentType=aFile.getContentType();
            if (!(contentType.contains("image")||contentType.equals("application/octet-stream"))) {
                return YxUtil.createFail(formatErrorReturn);
            }
        }
        return YxUtil.createSuccessDataNoFilter(fileSystemUpload(fileUpload, headImageFolderId));
    }

    @ApiOperation("上传聊天文件")
    @RequestMapping(value = "/uploadChat", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseData uploadChat(@RequestPart(value = "fileUpload") MultipartFile[] fileUpload) {
        int size = fileConfig.getChatFileSize();
        for (MultipartFile aFile : fileUpload) {
            if (aFile.getSize() > size) {
                return YxUtil.createFail(sizeNoAllowedReturn);
            }

        }
        return YxUtil.createSuccessDataNoFilter(fileSystemUpload(fileUpload, chatFolderId));
    }

    @ApiOperation("上传举报附件")
    @RequestMapping(value = "/uploadReportImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseData uploadReportImage(@RequestPart(value = "fileUpload") MultipartFile[] fileUpload) {
        int size = fileConfig.getReportFileSize();
        for (MultipartFile aFile : fileUpload) {
            if (aFile.getSize() > size) {
                return YxUtil.createFail(sizeNoAllowedReturn);
            }

        }
        return YxUtil.createSuccessDataNoFilter(fileSystemUpload(fileUpload, reportFolderId));
    }

    private List fileSystemUpload(MultipartFile[] fileUpload, String folderId) {
        List fileList = new ArrayList();
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile aFile : fileUpload) {
                //保存到文件服务器
                ResultBean res = uploadServiceFeign.upload(aFile, "", folderId);
                Map<String, Object> map = (Map<String, Object>) res.getObj();
                fileList.add(map.get("visitUrl"));
            }
        }

        return fileList;
    }

    /**
     * 获取文件系统的访问前缀
     *
     * @return ResponseData
     */
    @ApiOperation("获取文件系统的访问前缀")
    @RequestMapping(value = "/getVisitPrefix", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseData getVisitPrefix() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        ResultBean rb = uploadServiceFeign.getVisitPrefix();
        data.put("visitPrefix", rb.getObj());
        return YxUtil.createSuccessDataNoFilter(data);
    }


}
