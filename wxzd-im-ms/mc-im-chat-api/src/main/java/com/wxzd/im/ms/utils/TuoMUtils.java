package com.wxzd.im.ms.utils;

import org.apache.commons.lang.StringUtils;

public class TuoMUtils {

    /**
     * 姓名脱敏
     *
     * @param userName
     * @param index
     * @return
     */
    public static String hideName(String userName, int index) {
        if (StringUtils.isBlank(userName)) {
            return "";
        }
        String name = StringUtils.left(userName, index);
        return StringUtils.rightPad(name, StringUtils.length(userName), "*");
    }

    /**
     * 身份证脱敏
     *
     * @param cardNo
     * @param index
     * @param end
     * @return
     */
    public static String hideCerCardNum(String cardNo, int index, int end) {
        if (StringUtils.isBlank(cardNo)) {
            return "";
        }
        return StringUtils.left(cardNo, index).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(cardNo, end), StringUtils.length(cardNo), "*"), "***"));
    }

    /**
     * 电话脱敏
     *
     * @param phoneNum
     * @param end
     * @return
     */
    public static String hidePhone(String phoneNum, int end) {
        if (StringUtils.isBlank(phoneNum)) {
            return "";
        }
        return StringUtils.leftPad(StringUtils.right(phoneNum, end), StringUtils.length(phoneNum), "*");
    }

    /**
     * 邮箱脱敏
     *
     * @param email
     * @return
     */
    public static String email(String email) {
        if (StringUtils.isBlank(email)) {
            return "";
        }
        int index = StringUtils.indexOf(email, "@");
        if (index <= 1) {
            return email;
        } else {
            return StringUtils.rightPad(StringUtils.left(email, 1), index, "*").concat(StringUtils.mid(email, index, StringUtils.length(email)));
        }

    }

    /**
     * 银行卡号脱敏
     *
     * @param cardNum
     * @return
     */
    public static String bankCard(String cardNum) {
        if (StringUtils.isBlank(cardNum)) {
            return "";
        }
        return StringUtils.left(cardNum, 4).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(cardNum, 4), StringUtils.length(cardNum), "*"), "******"));
    }

    /**
     * 手机脱敏
     *
     * @param mobile
     * @return
     */
    public static String mobileTuo(String mobile) {
        if (StringUtils.isBlank(mobile)) {
            return "";
        }
        return StringUtils.left(mobile, 3).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(mobile, 2), StringUtils.length(mobile), "*"), "***"));
    }

    public static void main(String[] args) {
        System.out.println(TuoMUtils.mobileTuo("15858846222"));
//        System.out.println(TuoMUtils.email("90749825@qq.com"));
//        System.out.println(TuoMUtils.bankCard("IWXA7BE7HPY5VTXG"));
    }
}
