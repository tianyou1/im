package com.wxzd.im.ms.service;

import com.wxzd.im.ms.parameter.Constants;
import com.wxzd.im.ms.utils.RedisUtil;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    //邮箱验证码
    public void sendEmail(String emailaddress, String code) {
        // 不要使用SimpleEmail,会出现乱码问题
        HtmlEmail email = new HtmlEmail();
        try {
            // 这里是SMTP发送服务器的名字：，普通qq号只能是smtp.qq.com ；
            email.setHostName("smtp.gmail.com");
            //设置需要鉴权端口
            email.setSmtpPort(465);
            //开启 SSL 加密
            email.setSSLOnConnect(true);
            // 字符编码集的设置
            email.setCharset("utf-8");
            // 收件人的邮箱
            email.addTo(emailaddress);
            // 发送人的邮箱
            email.setFrom("by219it@gmail.com", "myEmail");
            // 如果需要认证信息的话，设置认证：用户名-密码。分别为发件人在邮件服务器上的注册名称和得到的授权码
            email.setAuthentication("by219it@gmail.com", "a123456.#");
            email.setSubject("验证码");
            // 要发送的信息，由于使用了HtmlEmail，可以在邮件内容中使用HTML标签
            email.setMsg("您的验证码是：" + code + "，请于10分钟内正确输入");
            // 发送
            email.send();
        } catch (EmailException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("邮件发送失败!");
        }
    }

    public boolean equalValidate(String emailAddress, String validateNum) {
        byte[] bytes = RedisUtil.get((Constants.SMS_EMAIL + emailAddress).getBytes());
        if (bytes == null) {
            return false;
        }
        String num = new String(bytes);
        return validateNum.equals(num);

    }


}
