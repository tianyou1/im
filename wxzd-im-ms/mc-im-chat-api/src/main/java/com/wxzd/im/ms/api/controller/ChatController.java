package com.wxzd.im.ms.api.controller;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.api.feignclient.service.FileUploadServiceFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.parameter.IMConstants;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.parameter.ResultBean;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.wxzd.im.ms.parameter.IMConstants.*;
import static com.wxzd.im.ms.parameter.ReturnConstant.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;


@RestController
@Api(value = "大部分聊天HTTP接口")
@RequestMapping(value = "/chat", method = RequestMethod.POST)
public class ChatController {
    @Autowired
    private ImUserService userService;
    @Autowired
    private ImGroupMemberService groupMemberService;
    @Autowired
    private ImGroupService groupService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private ImMessageHistoryService messageHistoryService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ImUserSecondaryVerificationService secondService;
    @Autowired
    private ImUserReportsService userReportService;
    @Autowired
    private WebDictionaryService webDictionaryService;
    @Autowired
    private WebDictionaryDetailService webDictionaryDetailService;
    @Autowired
    private ImUserRequestAuditService requestAuditService;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private ImUserSessionService userSessionService;
    @Autowired
    private ImTopService topService;
    @Autowired
    private ReadyService readyService;
    @Autowired
    private ImUserRequestDeletedService requestDeletedService;
    @Autowired
    private MessageFactory messageFactory;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CacheService cacheService;
    @Autowired
    private ChatServerFeign chatServerFeign;
    @Autowired
    private FileUploadServiceFeign uploadServiceFeign;

    @ApiOperation("聊天记录查询token获取接口")
    @RequestMapping("/getMsgHistoryToken")
    @ResponseBody
    public ResponseData getMsgHistoryToken(
            @RequestHeader String token,
            @ApiParam("密码保存时间（N分钟）") int time,
            @ApiParam("隐私密码") @RequestParam String secretModePwd) {
        String[] paramArr = token.split("\\|");
        long userId = Long.parseLong(Utils.decrypt(paramArr[1]));
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        //解密隐私密码
        secretModePwd = RSACipherUtil.decrypt(secretModePwd);
        if (CheckUtil.checkSecretPwd(imUser, secretModePwd)) {
            return YxUtil.createFail(secretPwdErrorReturn);
        }
        JSONObject json = new JSONObject();
        //生成临时token，指定time分钟有效
        json.put("historyWand", TokenUtil.generateHistoryWand(time, paramArr));
        return YxUtil.createSuccessData(json);
    }


    @ApiOperation("单聊历史记录")
    @RequestMapping("/getMsgHistory")
    @ResponseBody
    public ResponseData getMsgHistory(
            @ApiParam("接收消息id") @RequestParam long destId,
            @ApiParam("每页数量") @RequestParam(defaultValue = "20") int size,
            @ApiParam("页数") @RequestParam(defaultValue = "1") int pageNo,
            @ApiParam("消息来源id") @RequestHeader String token,
            @ApiParam("内容") @RequestParam(required = false) String txt,
            @ApiParam("消息类型：如视频，图片") @RequestParam(defaultValue = "0") int messageType,
            @ApiParam("聊天记录密码临时token") @RequestParam(required = false) String historyWand) {
        Long userId = CommonUtils.getUserIdByToken(token);
        //判断聊天记录密码临时token
        ResponseData responseData = checkFriendWand(userId, destId, historyWand);
        if (responseData != null) {
            return responseData;
        }
        //单聊记录
        MongoPage page = messageHistoryService.getSignalHistory(destId, singleType, size, pageNo, userId, txt, messageType);
        return YxUtil.createSuccessDataNoFilter(page);

    }

    /**
     * 验证好友是否设置聊天记录加密
     *
     * @param userId
     * @param destId
     * @param historyWand
     * @return
     */
    private ResponseData checkFriendWand(Long userId, Long destId, String historyWand) {
        ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
        ImFriend friend = cacheService.getFriend(userId, destId);
        if (friend == null) {
            return YxUtil.createFail(notFriendReturn);
        }

        //判断全局
        if (user.getIsSecretHistory() != null && secretFriendSetOpen.equals(user.getIsSecretHistory()) && StringUtils.isEmpty(historyWand) ||
                //判断好友
                friend.getIsSecretHistory().equals(historySecret) && StringUtils.isEmpty(historyWand)) {
            return YxUtil.createFail(checkHistoryPwdReturn);
        }

        if (user.getIsSecretHistory() != null && secretFriendSetOpen.equals(user.getIsSecretHistory()) || friend.getIsSecretHistory().equals(historySecret)) {
            return checkHistoryWord(historyWand);
        }
        return null;
    }


    /**
     * 验证好友是否设置聊天记录加密
     *
     * @param userId
     * @param destId
     * @param historyWand
     * @return
     */
    private ResponseData checkGroupWand(Long userId, Long destId, String historyWand) {
        ImUser user = userService.selectByPrimaryKey(userId);
        ImGroupMember groupMember = cacheService.getMember(userId, destId);
        if (groupMember == null) {
            return YxUtil.createFail(notInGroupReturn);
        }
        //判断全局
        if (user.getIsSecretGroupHistory() != null && secretgroupSetOpen.equals(user.getIsSecretGroupHistory()) && StringUtils.isEmpty(historyWand) ||
                //判断设置群
                groupMember.getIsSecretHistory() != null && groupMember.getIsSecretHistory().equals(setOn) && StringUtils.isEmpty(historyWand)) {
            return YxUtil.createFail(checkHistoryPwdReturn);
        }

        if (user.getIsSecretGroupHistory() != null && secretgroupSetOpen.equals(user.getIsSecretGroupHistory()) || groupMember.getIsSecretHistory() != null && groupMember.getIsSecretHistory().equals(setOn)) {
            return checkHistoryWord(historyWand);
        }
        return null;
    }

    /**
     * 验证聊天密码
     *
     * @param historyWand 标识
     * @return int
     */
    private ResponseData checkHistoryWord(String historyWand) {
        String[] historyWandArray = Utils.decrypt(historyWand).split("\\|");
        //时间（token生成时间）
        long time = Long.parseLong(Utils.decrypt(historyWandArray[0]));

        if (!Utils.decrypt(historyWandArray[3]).equals(TokenEnum.msgHistoryToken.getCode() + "")) {
            return YxUtil.createFail("验证信息错误");
        }

        if (time < System.currentTimeMillis()) {
            return YxUtil.createFail("验证时间过期，请重新输入");
        }

        return null;
    }


    @ApiOperation("群聊历史记录")
    @RequestMapping("/getGroupMsgHistory")
    @ResponseBody
    public ResponseData getGroupMsgHistory(
            @ApiParam("接收消息id") @RequestParam long destId,
            @ApiParam("每页数量") @RequestParam(defaultValue = "20") int size,
            @ApiParam("页数") @RequestParam(defaultValue = "1") int pageNo,
            @ApiParam("消息来源id") @RequestHeader String token,
            @ApiParam("内容") @RequestParam(required = false) String txt,
            @ApiParam("消息类型：如视频，图片") @RequestParam(defaultValue = "0") int messageType,
            @ApiParam("聊天记录密码临时token") @RequestParam(required = false) String historyWand) {
        //判断聊天记录密码临时token
        Long userId = CommonUtils.getUserIdByToken(token);
        //判断聊天记录密码临时token
        ResponseData responseData = checkGroupWand(userId, destId, historyWand);
        if (responseData != null) {
            return responseData;
        }
        //群聊记录
        MongoPage page = messageHistoryService.getGroupHistory(destId, groupType, size, pageNo, userId, txt, messageType);
        return YxUtil.createSuccessDataNoFilter(page);

    }


    @ApiOperation("聊天记录historyWand有效性判断接口")
    @RequestMapping("/checkChatHistoryWand")
    @ResponseBody
    public ResponseData checkChatHistoryWand(
            @RequestHeader String token,
            @ApiParam("聊天记录密码临时token") @RequestParam String historyWand) {
        //判断聊天记录密码临时token
        long userId = CommonUtils.getUserIdByToken(token);
        JSONObject jsonParam = new JSONObject();
        if (StringUtils.isBlank(historyWand)) {
            jsonParam.put("isValid", 0);
            jsonParam.put("info", "用户信息错误");
            return YxUtil.createSuccessData(jsonParam);
        }
        String[] historyWandArray = Utils.decrypt(historyWand).split("\\|");
        long userHistoryWordId = Long.parseLong(Utils.decrypt(historyWandArray[1]));
        //判断用户ID是否相同
        if (userId != userHistoryWordId) {
            jsonParam.put("isValid", 0);
            jsonParam.put("info", "用户信息错误");
            return YxUtil.createSuccessData(jsonParam);
        }
        //时间（token生成时间）
        long time = Long.parseLong(Utils.decrypt(historyWandArray[0]));

        if (!Utils.decrypt(historyWandArray[3]).equals(TokenEnum.msgHistoryToken.getCode() + "")) {
            jsonParam.put("isValid", 0);
            jsonParam.put("info", "验证信息错误");
            return YxUtil.createSuccessData(jsonParam);
        }
        if (time < System.currentTimeMillis()) {
            jsonParam.put("isValid", 0);
            jsonParam.put("info", "验证时间过期，请重新输入");
            return YxUtil.createSuccessData(jsonParam);
        }
        jsonParam.put("isValid", 1);
        jsonParam.put("info", "验证成功");
        return YxUtil.createSuccessData(jsonParam);
    }


    @ApiOperation("好友聊天聊天记录删除接口（单条）")
    @RequestMapping("/deleteFriendMsgHistory")
    @ResponseBody
    public ResponseData deleteFriendMsgHistory(
            @ApiParam("好友id") @RequestParam(required = false) long friendId,
            @ApiParam("消息id") @RequestParam String msgId,
            @ApiParam("（1：单向服务器删除， 2：双向同步及服务器删除）") @RequestParam int deleteType,
            @RequestHeader String token) {
        return deleteSignalHistory(msgId, token, IMConstants.MSG_FROM_P2P, deleteType);
    }

    @ApiOperation("好友聊天聊天记录删除接口（批量）")
    @RequestMapping("/batchDeleteFriendMsgHistory")
    @ResponseBody
    public ResponseData batchDeleteFriendMsgHistory(
            @ApiParam("接收消息id") @RequestParam long friendId,
            @ApiParam("（1：单向服务器删除， 2：双向同步及服务器删除）") @RequestParam int deleteType,
            @RequestHeader String token) {
        Long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        JSONObject json = new JSONObject();
        json.put("textTip", "");
        if (deleteType == signalDeleteType) {
            messageHistoryService.deleteByBothWayFriend(userId, friendId);
            Update update = new Update();
            mongoTemplate.updateMulti(Query.query(new Criteria().orOperator(Criteria.where("belongUserId").is(userId).and("destId").is(friendId).and("fromType").is(1), Criteria.where("belongUserId").is(userId).and("fromId").is(friendId).and("fromType").is(1))),
                    update.set("status", deleteStatus), ImMessageHistoryMong.class);

            //通知
            ImMessage msg = messageFactory.friendSetNotice(userId, friendId, device, MSG_TYPE_FRIEND_CHATHISTORY_DELETE, json.toJSONString());
            //通知自己
            msg.setSystemType(p2PSingleSystemType);
            chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(msg), userId);
            return YxUtil.createSimpleSuccess("删除成功");
        }
        if (deleteType == bothDeleteType) {
            messageHistoryService.deleteByBothWayFriend(userId, friendId);
            Update update = new Update();
            mongoTemplate.updateMulti(Query.query(new Criteria().orOperator(Criteria.where("fromId").is(userId).and("destId").is(friendId).and("fromType").is(1), Criteria.where("destId").is(userId).and("fromId").is(friendId).and("fromType").is(1))),
                    update.set("status", deleteStatus), ImMessageHistoryMong.class);
            //通知
            ImMessage msg = messageFactory.friendSetNotice(userId, friendId, device, MSG_TYPE_FRIEND_CHATHISTORY_BOTH_DELETE, json.toJSONString());
            //通知自己
            commonService.noticeMyself(msg, device, userId);

            ImFriend frienduser = cacheService.getFriend(friendId, userId);

            //如果好友拉黑了自己，不做双向消息处理
            if(frienduser.getIsBlack().equals(isBlack)){
                return YxUtil.createSimpleSuccess(operationSuccessReturn);
            }
            //通知好友
            commonService.noticeFriendAndSaveBoth(msg, 0, friendId);
            return YxUtil.createSimpleSuccess("删除成功");
        }
        return YxUtil.createSimpleSuccess("删除成功");
    }


    //删除历史记录（单向全部，或者双向全部）
    private void updateSessionByAllFriendOrGroup(long userId, long destId, int deleteType) {
        ImUserSession session = new ImUserSession();
        session.setUserId(userId);
        session.setDestType(IMConstants.MSG_FROM_GROUP);
        session.setDestId(destId);
        session.setLastContent("");
        if (deleteType == signalDeleteType) {
            userSessionService.updateByPrimaryKeySelective(session);
        }
        if (deleteType == bothDeleteType) {
            //删除两边会话
            session.setUserId(null);
            session.setMsgType(IMConstants.MSG_TYPE_TEXT);
            userSessionService.updateByPrimaryKeySelective(session);
        }
    }

    @ApiOperation("群聊天聊天记录删除接口（单条）")
    @RequestMapping("/deleteGroupMsgHistory")
    @ResponseBody
    public ResponseData deleteGroupMsgHistory(
            @ApiParam("群id") @RequestParam(required = false) long groupId,
            @ApiParam("消息id") @RequestParam String msgId,
            @ApiParam("1：单向本地服务器删除，2：双向同步及服务器删除（群管理员）") @RequestParam int deleteType,
            @RequestHeader String token) {
        if (deleteType == bothDeleteType) {
            Long userId = CommonUtils.getUserIdByToken(token);
            ImGroupMember imGroupMember = cacheService.getMember(userId, groupId);
            if (imGroupMember == null) {
                return YxUtil.createFail("群组不存在");
            }
            if (!imGroupMember.getRole().equals(groupRoleOwner)) {
                return YxUtil.createFail("您不是群主，无法删除别人的数据");
            }
        }
        return deleteSignalHistory(msgId, token, IMConstants.MSG_FROM_GROUP, deleteType);
    }

    @ApiOperation("群聊天聊天记录删除接口（批量）")
    @RequestMapping("/batchDeleteGroupMsgHistory")
    @ResponseBody
    public ResponseData batchDeleteGroupMsgHistory(
            @ApiParam("群id") @RequestParam long groupId,
            @ApiParam("1：单向本地服务器删除，2：双向同步及服务器删除（群管理员）") @RequestParam int deleteType,
            @RequestHeader String token) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer device = CommonUtils.getDevTypeByToken(token);
        JSONObject json = new JSONObject();
        json.put("textTip", "");
        //判断临时记录token
        if (deleteType == signalDeleteType) {
            updateSessionByAllFriendOrGroup(userId, groupId, deleteType);
            messageHistoryService.deleteByOneWayGroup(userId, groupId);
            Update update = new Update();
            mongoTemplate.updateMulti(Query.query(Criteria.where("belongUserId").is(userId).and("destId").is(groupId).and("fromType").is(2)),
                    update.set("status", deleteStatus), ImMessageHistoryMong.class);


            //通知
            ImMessage msg = messageFactory.groupSetNotice(userId, groupId, device, MSG_TYPE_GROUP_DELETE_HISTORY, json.toJSONString());
            msg.setSystemType(singleSystemType);
            chatServerFeign.notifyImServerSaveSingle(JSONObject.toJSONString(msg),userId);
            return YxUtil.createSimpleSuccess("删除成功");
        }
        if (deleteType == bothDeleteType) {
            updateSessionByAllFriendOrGroup(userId, groupId, deleteType);
            ImGroupMember imGroupMember = cacheService.getMember(userId, groupId);
            if (imGroupMember == null) {
                return YxUtil.createFail("群组不存在");
            }
            if (!imGroupMember.getRole().equals(groupRoleOwner)) {
                return YxUtil.createFail("您不是群主，无法删除别人的数据");
            }
            messageHistoryService.deleteByBothWayGroup(groupId);
            Update update = new Update();
            mongoTemplate.updateMulti(Query.query(Criteria.where("destId").is(groupId)),
                    update.set("status", deleteStatus), ImMessageHistoryMong.class);

            //通知
            ImMessage msg = messageFactory.groupSetNotice(userId, groupId, device, MSG_TYPE_GROUP_DELETE_HISTORY, json.toJSONString());
            msg.setSystemType(groupSystemType);
            chatServerFeign.notifyGroupAllAndSaveHistory(JSONObject.toJSONString(msg));
            return YxUtil.createSimpleSuccess("删除成功");
        }
        return YxUtil.createSimpleSuccess("删除成功");
    }

    /**
     * 单条历史记录删除公用方法
     *
     * @param msgId      前端生成的消息id
     * @param token      标识
     * @param destType   目标类型
     * @param deleteType 删除类型
     * @return ResponseData
     */
    private ResponseData deleteSignalHistory(String msgId, String token, int destType, int deleteType) {
        Long userId = CommonUtils.getUserIdByToken(token);

        String[] msgIdArray = msgId.split(",");
        for (String msgid : msgIdArray) {
            if (deleteType == signalDeleteType) {
                //删除消息前更新会话
                deleteSingleMessageHistoryUpdateSession(userId, msgid, destType, deleteType);
                messageHistoryService.deleteByMsgIdAndUserId(msgid, userId);
                //删除mongo中ImMessageHistory表对应的msgId和belongUserId
                Update update = new Update();
                mongoTemplate.updateMulti(Query.query(Criteria.where("msgId").is(msgid).and("belongUserId").is(userId)),
                        update.set("status", deleteStatus), ImMessageHistoryMong.class);
            }
            if (deleteType == bothDeleteType) {
                if (destType == IMConstants.MSG_FROM_P2P) {
                    Query query = Query.query(Criteria.where("msgId").is(msgid));
                    List<ImMessageHistoryMong> messageHistoryMongs = mongoTemplate.find(query, ImMessageHistoryMong.class);
                    if (!messageHistoryMongs.isEmpty()) {
                        //删除消息前更新会话
                        deleteSingleMessageHistoryUpdateSession(messageHistoryMongs.get(0).getBelongUserId(), msgid, IMConstants.MSG_FROM_P2P, deleteType);
                        if (messageHistoryMongs.size() > 1) {
                            //删除消息前更新会话
                            deleteSingleMessageHistoryUpdateSession(messageHistoryMongs.get(1).getBelongUserId(), msgid, IMConstants.MSG_FROM_P2P, deleteType);
                        }
                    }
                }

                if (destType == IMConstants.MSG_FROM_GROUP) {
                    deleteSingleMessageHistoryUpdateSession(userId, msgid, IMConstants.MSG_FROM_GROUP, deleteType);
                }

                messageHistoryService.deleteByMsgId(msgid);
                Update update = new Update();
                mongoTemplate.updateMulti(Query.query(Criteria.where("msgId").is(msgid)),
                        update.set("status", deleteStatus), ImMessageHistoryMong.class);
            }
        }
        return YxUtil.createSimpleSuccess("删除成功");
    }

    //删除消息更新会话
    private void deleteSingleMessageHistoryUpdateSession(long userId, String msgId, int destType, int deleteType) {
        List<Integer> statusList = Arrays.asList(readStatus, unReadStatus);
        Query query = Query.query(Criteria.where("belongUserId").is(userId).and("fromType").is(destType).and("status").in(statusList)).with(Sort.by(Sort.Direction.DESC, "sendTime")).limit(2);
        List<ImMessageHistoryMong> messageHistoryMongs = mongoTemplate.find(query, ImMessageHistoryMong.class);
        ImMessageHistoryMong messageHistoryMong1 = null;
        ImMessageHistoryMong messageHistoryMong2 = null;
        if (!messageHistoryMongs.isEmpty()) {
            messageHistoryMong1 = messageHistoryMongs.get(0);
            if (messageHistoryMongs.size() > 1) {
                messageHistoryMong2 = messageHistoryMongs.get(1);
            }
        }
        if (messageHistoryMong1 != null) {
            if (msgId.equals(messageHistoryMong1.getMsgId())) {
                ImMessageHistoryMong msg = mongoTemplate.findOne(Query.query(Criteria.where("belongUserId").is(userId).and("msgId").is(msgId)), ImMessageHistoryMong.class);
                ImUserSession session = new ImUserSession();
                session.setUserId(userId);
                if (msg != null) {
                    if (messageHistoryMong2 == null) {
                        session.setLastContent("");
                        session.setMsgType(IMConstants.MSG_TYPE_TEXT);
                    } else {
                        session.setLastContent(ChatUserSession.getSessionContent(messageHistoryMong2.getContent(), messageHistoryMong2.getMessageType()));
                        session.setMsgType(messageHistoryMong2.getMessageType());
                    }
                    if (IMConstants.MSG_FROM_GROUP == destType && bothDeleteType == deleteType) {
                        session.setUserId(null);
                    }
                    if (userId == msg.getDestId() && IMConstants.MSG_FROM_P2P == destType) {
                        session.setDestId(msg.getFromId());
                    } else {
                        session.setDestId(msg.getDestId());
                    }
                    session.setDestType(msg.getFromType());
                    userSessionService.updateByPrimaryKeySelective(session);
                }
            }
        }
    }


    @ApiOperation("好友聊天记录查询接口（拉取用）")
    @RequestMapping("/getFriendMsgHistoryMsg")
    @ResponseBody
    public ResponseData getFriendMsgHistoryMsg(
            @ApiParam("接收消息id") @RequestParam long destId,
            @ApiParam("每页数量") @RequestParam(defaultValue = "20") int size,
            @ApiParam("消息ID") @RequestParam(required = false) String msgId,
            @ApiParam("消息来源id") @RequestHeader String token,
            @ApiParam("聊天记录密码临时token") @RequestParam(required = false) String historyWand) {
        Long userId = CommonUtils.getUserIdByToken(token);
        //判断聊天记录密码临时token
        ResponseData responseData = checkFriendWand(userId, destId, historyWand);
        if (responseData != null) {
            return responseData;
        }
        //单聊记录
        Map<String, Object> messageMap = messageHistoryService.getSignalHistoryByMsgId(destId, singleType, size, msgId, userId);
        return YxUtil.createSuccessDataNoFilter(messageMap);

    }

    @ApiOperation("群组聊天记录查询接口（拉取用）")
    @RequestMapping("/getGroupMsgHistoryMsg")
    @ResponseBody
    public ResponseData getGroupMsgHistoryMsg(
            @ApiParam("接收消息id") @RequestParam long destId,
            @ApiParam("每页数量") @RequestParam(defaultValue = "20") int size,
            @ApiParam("消息ID") @RequestParam(required = false) String msgId,
            @ApiParam("消息来源id") @RequestHeader String token,
            @ApiParam("聊天记录密码临时token") @RequestParam(required = false) String historyWand) {
        Long userId = CommonUtils.getUserIdByToken(token);
        //判断聊天记录密码临时token
        ResponseData responseData = checkGroupWand(userId, destId, historyWand);
        if (responseData != null) {
            return responseData;
        }

        //群聊记录
        Map<String, Object> messageMap = messageHistoryService.getGroupHistoryByMsgId(destId, groupType, size, msgId, userId);
        return YxUtil.createSuccessDataNoFilter(messageMap);
    }


    @ApiOperation("读取会话")
    @RequestMapping("/readSession")
    @ResponseBody
    public ResponseData readSession(@RequestHeader String token,
                                    @ApiParam("目标id：群id或者好友id") @RequestParam long destId,
                                    @ApiParam("会话类型：1：好友，2群聊") @RequestParam int destType) {

        long userId = CommonUtils.getUserIdByToken(token);
        userSessionService.readSessionCount(userId, destId, destType);
        return YxUtil.createSimpleSuccess("会话已读");
    }

    @ApiOperation("删除会话")
    @RequestMapping("/deleteSession")
    @ResponseBody
    public ResponseData deleteSession(
            @ApiParam("目标id：群id或者好友id") @RequestParam long destId,
            @ApiParam("会话类型：1：好友，2群聊") @RequestParam int destType,
            @RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        userSessionService.deleteSession(userId, destId, destType);
        messageHistoryService.deleteByBothWayFriend(userId, destId);
        return YxUtil.createSimpleSuccess("删除完成");
    }

    @ApiOperation("设置置顶")
    @RequestMapping("/setTop")
    @ResponseBody
    public ResponseData setTop(@ApiParam("目标id：群或者好友id") long destId,
                               @RequestHeader String token,
                               @ApiParam("目标类型：1：好友，2：群聊") int destType) {

        long userId = CommonUtils.getUserIdByToken(token);

        if (destType > 0 && userId > 0 && destId > 0) {
            topService.isTop(userId, destId, destType);
        }

        return YxUtil.createSimpleSuccess(setSuccessReturn);
    }


    @ApiOperation("获取置顶列表")
    @RequestMapping("/findTopList")
    @ResponseBody
    public ResponseData findTopList(@RequestHeader String token) {

        long userId = CommonUtils.getUserIdByToken(token);

        JSONObject json = new JSONObject();

        json.put("topList", topService.getTopList(userId));

        return YxUtil.createSuccessData(json);
    }


    @ApiOperation("删除置顶")
    @RequestMapping("/cancleTop")
    @ResponseBody
    public ResponseData cancleTop(@ApiParam("目标id：群或者好友id") long destId,
                                  @RequestHeader String token,
                                  @ApiParam("目标类型：1：好友，2：群聊") int destType) {

        long userId = CommonUtils.getUserIdByToken(token);
        if (destType > 0 && userId > 0 && destId > 0) {
            topService.cancelTop(userId, destId, destType);
        }
        return YxUtil.createSimpleSuccess("删除成功");
    }


    @ApiOperation("通过用户id获取会话")
    @RequestMapping("/getSessionByUserId")
    @ResponseBody
    public ResponseData getSessionByUserId(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        //查询是否开启私密模式
        String isSecret = CommonUtils.getSecretByToken(token);
        List sessionList = readyService.getSessionList(userId, Integer.valueOf(isSecret));
        return YxUtil.createSuccessDataNoFilter(sessionList);
    }


    @ApiOperation("设置二级认证")
    @RequestMapping("/setSecondAuth")
    @ResponseBody
    public ResponseData setSecondAuth(@RequestHeader String token,
                                      @ApiParam("二次验证类型，1：手机，2：邮箱，3：谷歌，4：二级密码") @RequestParam String verifyType,
                                      @ApiParam("是否启动二次验证，0：否，1：是") @RequestParam String isSecond,
                                      @ApiParam("手机号/邮箱/秘钥账号/二次密码") @RequestParam String checkAccount,
                                      @ApiParam("验证：手机号验证码/邮箱验证码/秘钥验证码/二次验证密码") @RequestParam String checkInfo) {
        if (StringUtils.isEmpty(isSecond)) {
            return YxUtil.createFail("是否开启状态为空");
        }

        if (StringUtils.isEmpty(verifyType)) {
            return YxUtil.createFail("验证类型为空");
        }

        if (StringUtils.isEmpty(checkAccount)) {
            return YxUtil.createFail("验证账号为空");
        }
        if (StringUtils.isEmpty(checkInfo)) {
            return YxUtil.createFail("验证信息为空");
        }
        Long userId = CommonUtils.getUserIdByToken(token);

        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);

        if (secondaryVerification == null) {

            //手机号验证
            if (typePhone.equals(Integer.valueOf(verifyType))) {
                if (StringUtils.isEmpty(checkInfo)) {
                    return YxUtil.createFail("验证信息为空");
                }
                boolean b = smsService.equalValidate(checkAccount, checkInfo);
                //二次验证返回
                if (b) {
                    secondaryVerification = new ImUserSecondaryVerification();
                    secondaryVerification.setUserId(userId);
                    secondaryVerification.setMobile(checkAccount);
                    secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
                    secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
                    secondService.insert(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，验证信息错误");
                }
            }

            //邮箱验证
            if (typeEmail.equals(Integer.valueOf(verifyType))) {
                if (StringUtils.isEmpty(checkInfo)) {
                    return YxUtil.createFail("验证信息为空");
                }
                boolean b = emailService.equalValidate(checkAccount, checkInfo);
                if (b) {
                    secondaryVerification = new ImUserSecondaryVerification();
                    secondaryVerification.setUserId(userId);
                    secondaryVerification.setEmail(checkAccount);
                    secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
                    secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
                    secondService.insert(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，验证信息错误");
                }

            }

            if (typeGoogle.equals(Integer.valueOf(verifyType))) {
                boolean b = GoogleAuthenticator.authcode(checkInfo, checkAccount);
                if (b) {
                    secondaryVerification = new ImUserSecondaryVerification();
                    secondaryVerification.setUserId(userId);
                    secondaryVerification.setGoogleKey(checkAccount);
                    secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
                    secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
                    secondService.insert(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，验证信息错误");
                }

            }


            //二级密码验证
            if (typeSecond.equals(Integer.valueOf(verifyType))) {
                if (!CheckUtil.checkSecondaryPwd(checkInfo)) {
                    return YxUtil.createFail(secondaryPwdReturn);
                }
                boolean b = checkAccount.equals(checkInfo);
                if (b) {
                    secondaryVerification = new ImUserSecondaryVerification();
                    createTypeSecondData(secondaryVerification, userId, checkAccount, isSecond, verifyType);
                    secondService.insert(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，验证信息错误");
                }
            }
            return YxUtil.createSimpleSuccess(setSuccessReturn);

        } else {
            //如果开启，判断是否验证时间超过了
            if (isSecondaryYes.equals(secondaryVerification.getIsSecondaryVerification())) {
                String checkToken = TokenUtil.getUserTokenCheck(token);
                if (StringUtils.isEmpty(checkToken)) {
                    return YxUtil.createFail("临时操作限二次验证后5分钟，您已超时，请重新验证");
                }

            }

            //手机号验证
            if (typePhone.equals(Integer.valueOf(verifyType))) {

                if (StringUtils.isEmpty(checkInfo)) {
                    return YxUtil.createFail("验证信息为空");
                }
                boolean b = smsService.equalValidate(checkAccount, checkInfo);
                //二次验证返回
                if (b) {
                    secondaryVerification.setUserId(userId);
                    secondaryVerification.setMobile(checkAccount);
                    secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
                    secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
                    secondService.updateByPrimaryKeySelective(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，请重新获取");
                }
            }

            //邮箱验证
            if (typeEmail.equals(Integer.valueOf(verifyType))) {
                if (StringUtils.isEmpty(checkInfo)) {
                    return YxUtil.createFail("验证信息为空");
                }
                boolean b = emailService.equalValidate(checkAccount, checkInfo);
                if (b) {
                    secondaryVerification.setUserId(userId);
                    secondaryVerification.setEmail(checkAccount);
                    secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
                    secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
                    secondService.updateByPrimaryKeySelective(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，请重新获取");
                }
            }

            //谷歌秘钥
            if (typeGoogle.equals(Integer.valueOf(verifyType))) {
                boolean b = GoogleAuthenticator.authcode(checkInfo, checkAccount);
                if (b) {
                    secondaryVerification.setUserId(userId);
                    secondaryVerification.setGoogleKey(checkAccount);
                    secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
                    secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
                    secondService.updateByPrimaryKeySelective(secondaryVerification);
                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，请重新获取");
                }

            }

            //二级密码验证
            if (typeSecond.equals(Integer.valueOf(verifyType))) {
                if (StringUtils.isEmpty(checkInfo)) {
                    return YxUtil.createFail("验证信息为空");
                }
                boolean b = checkAccount.equals(checkInfo);
                if (b) {
                    createTypeSecondData(secondaryVerification, userId, checkAccount, isSecond, verifyType);
                    secondService.updateByPrimaryKeySelective(secondaryVerification);

                    return YxUtil.createSimpleSuccess(setSuccessReturn);
                } else {
                    return YxUtil.createFail("验证失败，请重新获取");
                }
            }
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        }
    }

    /**
     * 创建二次验证密码的数据
     *
     * @param secondaryVerification 二次验证信息
     * @param userId                用户id
     * @param checkAccount          密码
     * @param isSecond              是否开启
     * @param verifyType            验证类型
     */
    private void createTypeSecondData(ImUserSecondaryVerification secondaryVerification, long userId, String checkAccount, String isSecond, String verifyType) {
        secondaryVerification.setUserId(userId);
        secondaryVerification.setSecondaryPwd(Utils.encrypt(checkAccount));
        secondaryVerification.setIsSecondaryVerification(Integer.parseInt(isSecond));
        secondaryVerification.setVerifyType(Integer.valueOf(verifyType));
    }

    @ApiOperation("举报反馈接口")
    @RequestMapping("/userReport")
    @ResponseBody
    public ResponseData userReport(@RequestHeader String token,
                                   @ApiParam("举报类型(字典-IM_REPORT_TYPE，敏感词，恐吓勒索...)") @RequestParam int reportType,
                                   @ApiParam("举报来源类型(1：单聊，2：群聊，3：公众号，4：朋友圈)") @RequestParam int fromType,
                                   @ApiParam("被举报对象类型(1：单聊，2：群，3：公众号，4：群成员，)") @RequestParam int reportedType,
                                   @ApiParam("被举报对象ID") @RequestParam long reportedId,
                                   @ApiParam("举报说明") @RequestParam String reportedReasion,
                                   @ApiParam("举报附件路径（多个用,号隔开）") @RequestParam String reportFilePaths) {
        long userId = CommonUtils.getUserIdByToken(token);
        if (reportedReasion.length() > report_descriptions_length) {
            return YxUtil.createFail("举报说明过长");
        }
        if (reportedType == signalReportedType) {
            ImUser imUser = userService.selectByPrimaryKey(reportedId);
            String i = checkReport(imUser);
            if (StringUtils.isNotEmpty(i)) {
                YxUtil.createFail(i);
            }
        }

        if (reportedType == groupReportedType) {
            ImGroup imGroup = groupService.selectByPrimaryKey(reportedId);

            if (imGroup.getGroupStatus().equals(groupDissolve)) {
                return YxUtil.createFail("群已解散");
            }
            if (imGroup.getGroupStatus().equals(groupFreeze)) {
                return YxUtil.createFail("群已冻结");
            }
        }

        if (reportedType == groupMemberReportedType) {
            ImGroupMember imGroupMember = groupMemberService.selectByPrimaryKey(reportedId);
            ImUserWithBLOBs imUser = userService.selectByPrimaryKey(imGroupMember.getUserId());
            String i = checkReport(imUser);
            if (StringUtils.isNotEmpty(i)) {
                YxUtil.createFail(i);
            }
        }

        ImUserReportsWithBLOBs imUserReports = new ImUserReportsWithBLOBs();
        imUserReports.setId(SnowflakeIdWorkerUtil.snowflake.nextId());
        imUserReports.setReportType(reportType);
        imUserReports.setFromUserId(userId);
        imUserReports.setFromType(fromType);
        imUserReports.setReportedDate(new Date());
        imUserReports.setReportedType(reportedType);
        imUserReports.setReportedId(reportedId);
        imUserReports.setReportedReasion(reportedReasion);
        imUserReports.setReportFilePaths(reportFilePaths);
        imUserReports.setReportStatus(0);
        imUserReports.setReportHandleMark("");
        imUserReports.setReportHandleDate(null);
        userReportService.insert(imUserReports);
        return YxUtil.createSimpleSuccess("举报成功");
    }

    /**
     * 验证举报账号和群用户账号
     *
     * @param imUser 用户
     * @return String
     */
    private String checkReport(ImUser imUser) {
        if (imUser.getStatus().equals(loseUser)) {
            return "举报的账号已销毁";
        }

        if (imUser.getStatus().equals(freezeUser)) {
            return "举报的账号已冻结";
        }
        return null;
    }

    @ApiOperation("获取举报类型")
    @RequestMapping("/findReportType")
    @ResponseBody
    public ResponseData findReportType(@RequestHeader String token,
                                       @RequestParam @ApiParam("字典类型：IM_EPHEMERAL_CHAT_TIME：阅后即焚时长，IM_REPORT_TYPE：举报类型，IM_AUTO_CALCEL_TIME：账号自动注销时长") String dictType) {
        WebDictionary webDictionary = webDictionaryService.selectByType(dictType);
        if (webDictionary == null) {
            return YxUtil.createFail("字典数据不存在");
        }
        List<WebDictionaryDetail> webDictionaryDetail = webDictionaryDetailService.selectByDictionaryId(webDictionary.getId());
        JSONArray array = new JSONArray();
        for (WebDictionaryDetail dictionaryDetail : webDictionaryDetail) {
            JSONObject json = new JSONObject();
            json.put("name", dictionaryDetail.getName());
            json.put("value", dictionaryDetail.getValue());
            array.add(json);
        }
        //按照value升序
        array.sort(Comparator.comparing(obj -> ((JSONObject) obj).getIntValue("value")));
        return YxUtil.createSuccessDataNoFilter(array);
    }

    /**
     * 申请信息状态已读设置接口
     *
     * @param ids 消息通知
     * @return ResponseData
     */
    @ApiOperation("申请信息状态已读设置接口")
    @RequestMapping("/readRequest")
    @ResponseBody
    public ResponseData readRequest(@RequestHeader String token,
                                    @ApiParam("申请ids") @RequestParam(defaultValue = "0") String ids) {
        long userId = CommonUtils.getUserIdByToken(token);
        return getSetRes(requestAuditService.readRequest(ids, userId));
    }

    /**
     * 设置返回
     *
     * @param num int
     * @return ResponseData
     */
    private static ResponseData getSetRes(int num) {
        if (num > 0) {
            return YxUtil.createSimpleSuccess(operationSuccessReturn);
        } else {
            return YxUtil.createFail(operationErrorReturn);
        }

    }

    @ApiOperation("好友申请删除接口")
    @RequestMapping("/deleteRequest")
    @ResponseBody
    public ResponseData deleteRequest(@ApiParam("申请ids") @RequestParam(defaultValue = "0") String ids,
                                      @RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        return getSetRes(requestDeletedService.deleteRequest(ids, userId));
    }


    /*************Login**************************/
    @ApiOperation("普通注册接口")
    @RequestMapping("/register")
    @ResponseBody
    public ResponseData register(@ApiParam("注册的账号") @RequestParam(defaultValue = "") String account,
                                 @ApiParam("用户登录密码") @RequestParam(defaultValue = "") String pwd) {
        //检查是否包含空格
        if(account.contains(" ")){
            return YxUtil.createFail(accountBlankFormatReturn);
        }
        if (!CheckUtil.checkAccount(account)) {
            return YxUtil.createFail(accountFormatReturn);
        }
        //解密密码
        pwd = RSACipherUtil.decrypt(pwd);
        if (!CheckUtil.checkPwd(pwd)) {
            return YxUtil.createFail(passwordFormatReturn);
        }
        int count = userService.countByAccount(account);
        if (count > 0) {
            return YxUtil.createFail(accountExist);
        } else {
            ImUserWithBLOBs user = new ImUserWithBLOBs();
            user.setNickName(RandomName.randomName(true, 3));
            user.setPwd(Utils.encrypt(pwd));
            user.setHeadUrl(userHeadUrl);
            user.setAccount(account);
            user.setCreateTime(System.currentTimeMillis());
            user.setIsBanned(BanedTimeSet.unBannedTime);
            userService.insertSelective(user);
            return YxUtil.createSimpleSuccess(registerSuccess);
        }
    }


    @ApiOperation("登录接口")
    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData doLogin(HttpServletRequest request,
                                @ApiParam("默认为空，暂未设置") @RequestParam(defaultValue = "") String account,
                                @ApiParam("登录密码") @RequestParam(defaultValue = "") String pwd,
                                @ApiParam("登录设备类型，1为安卓2为苹果3为pc") @RequestParam Integer device) {
        return userService.doLoginMy(account, pwd, device);
    }

    @ApiOperation("登录退出接口")
    @RequestMapping(value = "/logout")
    @ResponseBody
    public ResponseData logout(@RequestHeader String token) {

        long userId = CommonUtils.getUserIdByToken(token);
        String device = CommonUtils.getDevTypeByToken(token).toString();
        //设置离线
        ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
        user.setIsOnline(offline);
        int count = userService.updateByPrimaryKeySelective(user);
        if (count > 0) {
            //更新用户缓存
            cacheService.updateUserCache(user);
            //删除用户token
            RedisUtil.hdel("userToken", userId + "_" + device);
            //删除 在线设备
            String onlineDevice = RedisUtil.hget("onlineDevice", user.getId().toString());
            if (!StringUtils.isEmpty(onlineDevice)) {
                Set<Long> set = YxUtil.splitSet(onlineDevice);
                set.remove(Long.parseLong(device));
                if (set.size() == 0) {
                    RedisUtil.hdel("onlineDevice", String.valueOf(userId));
                } else {
                    RedisUtil.hset("onlineDevice", user.getId().toString(), YxUtil.setToString(set));
                }
            }
            return YxUtil.createSuccessDataNoFilter(destroySuccessReturn);
        } else {
            return YxUtil.createFail(destroyErrorReturn);
        }
    }

    private static ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    @ApiOperation("初始化接口")
    @RequestMapping(value = "/ready", method = RequestMethod.POST)
    @ResponseBody
    public ResponseData ready(HttpServletRequest request, @RequestHeader String token,
                              @ApiParam("是否同步消息0不同步1同步") @RequestParam(required = false, defaultValue = "1") int syncMsg) {

        HashMap<String, Object> data = new HashMap<>();
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs my = userService.selectByPrimaryKey(userId);
        // 处理地址
        cachedThreadPool.execute(() -> userService.handleIP(request, my));
        //查询是否开启私密模式
        String isSecret = CommonUtils.getSecretByToken(token);
        //加载好友列表
        data.put("friendsInfo", readyService.getFriendInfo(userId, Integer.valueOf(isSecret)));
        //加载群列表
        data.put("groupsInfo", readyService.getGroupsInfo(userId, Integer.valueOf(isSecret)));
        //加载会话
        if (syncMsg == setOn) {
            data.put("sessionList", readyService.getSessionList(userId, Integer.valueOf(isSecret)));
        } else {
            data.put("sessionList", new ArrayList<>());
        }
        //返回置顶列表
        data.put("topList", readyService.getTopList(userId));
        //返回大厅url
        data.put("hallUrl", "");
        //返回个人信息
        ImUserWithBLOBs myInfo = userService.getMyInfo(userId);
        data.put("myInfo", myInfo);
        //新增通知消息未读统计
        //好友申请未读统计
        data.put("friendReq", readyService.unReadReqCount(friendReqType, userId));
        //进群确认未读统计
        data.put("confirmReq", readyService.unReadReqCount(confirmReqType, userId));
        //邀请入群未读统计
        data.put("inviteReq", readyService.unReadReqCount(inviteReqType, userId));
        //新增返回文件系统前缀
        try {
            ResultBean result = uploadServiceFeign.getVisitPrefix();
            data.put("visitPrefix", result.getObj());
        } catch (Exception e) {
            data.put("visitPrefix", "");
            e.printStackTrace();
        }
        return YxUtil.createSuccessData(data);
    }


    /*************User**************************/
    @ApiOperation("修改密码接口")
    @RequestMapping("/updatePwd")
    @ResponseBody
    public ResponseData updatePwd(@RequestHeader String token,
                                  @ApiParam("新密码") @RequestParam(defaultValue = "") String pwd) {
        long userId = CommonUtils.getUserIdByToken(token);
        //解密密码
        pwd = RSACipherUtil.decrypt(pwd);
        if (!CheckUtil.checkPwd(pwd)) {
            return YxUtil.createFail(passwordFormatReturn);
        }
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);
        //如果开启，判断是否验证时间超过了
        if (secondaryVerification != null && isSecondaryYes.equals(secondaryVerification.getIsSecondaryVerification())) {
            //有设置二级验证
            String checkToken = TokenUtil.getUserTokenCheck(token);
            if (StringUtils.isEmpty(checkToken)) {
                return YxUtil.createFail("临时操作限二次验证后5分钟，您已超时，请重新验证");
            } else {
                if (StringUtils.isBlank(pwd)) {
                    return YxUtil.createFail("密码不能为空");
                }
                return userService.updatePwd(token, pwd);
            }
        } else {
            //没有设置二级验证
            if (StringUtils.isBlank(pwd)) {
                return YxUtil.createFail("密码不能为空");
            }
            return userService.updatePwd(token, pwd);
        }
    }

    public static void main(String[] args) {
        System.out.println(IdUtil.simpleUUID());
    }
}
