package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImTop;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ImTopMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImTop record);

    int insertSelective(ImTop record);

    ImTop selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImTop record);

    int updateByPrimaryKey(ImTop record);

    int deleteByCondition(ImTop record);

    List<Map<String, Object>> getTopList(Long userId);
}