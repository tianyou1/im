package com.wxzd.im.ms.enums;

/**
 * 序列id枚举
 */
public enum SequenceEnum {
    tempIdentityId(990000000, "临时身份id");


    private long code;
    private String name;

    private SequenceEnum(int code, String name) {
        this.code = code;
        this.name = name();
    }

    public long getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }


}
