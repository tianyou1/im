package com.wxzd.im.ms.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.mapper.ImGroupMapper;
import com.wxzd.im.ms.parameter.LockKey;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.DistributeLockUtil;
import com.wxzd.im.ms.utils.YxUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.wxzd.im.ms.parameter.CacheConstants.*;
import static com.wxzd.im.ms.parameter.IMConstants.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;


@Service
@Transactional
public class ImGroupServiceImpl implements ImGroupService {
    @Autowired
    private ImGroupMapper imGroupMapper;

    @Autowired
    private ImGroupMemberService groupMemberService;

    @Autowired
    private ImUserService userService;

    @Autowired
    private ImUserRequestAuditService userRequestAuditService;

    @Autowired
    private ImUserSessionService userSessionService;

    @Autowired
    private MessageFactory msgFactory;

    @Autowired
    private ImTempIdentityService tempIdentityService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ChatServerFeign chatServerFeign;

    private ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imGroupMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImGroupWithBLOBs record) {
        return this.imGroupMapper.insert(record);
    }

    @Override
    public int insertSelective(ImGroupWithBLOBs record) {
        return this.imGroupMapper.insertSelective(record);
    }

    @Override
    @Cacheable(cacheNames = groupCache, key = "#id", unless = "#result == null")
    public ImGroupWithBLOBs selectByPrimaryKey(Long id) {
        return this.imGroupMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImGroupWithBLOBs record) {
        return this.imGroupMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    @Caching(
            put = {
                    @CachePut(value = groupCache, key = "#record.id"),
            },
            evict = {
                    @CacheEvict(value = groupCacheMap, key = "#record.id"),
            })
    public ImGroupWithBLOBs updateGroupCache(ImGroupWithBLOBs record) {
        return record;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImGroupWithBLOBs record) {
        return this.imGroupMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImGroup record) {
        return this.imGroupMapper.updateByPrimaryKey(record);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseData createGroup(List<ImGroupMember> inMembers, List<ImUserSession> inSessions, List<ImUserRequestAudit> outRequestAudits, long groupId, long userId, Set<Long> memberSet, Set<Long> outSet, int device) {
        //锁
        String lockKey = LockKey.createGroupKey + groupId + "_" + userId;
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {
            //批量插入
            //插入进群的 成员和会话
            if (inMembers.size() > 0) {
                groupMemberService.batchInsertSelective(inMembers);
                //删除chat server的缓存
                for (ImGroupMember member : inMembers) {
                    cacheService.removeGroupCache(member.getUserId());
                    cacheService.removeSecretGroupCache(member);
                }
                userSessionService.batchCreateSession(inSessions);
            }
            //插入无法进群的 申请列表
            if (outRequestAudits.size() > 0) {
                userRequestAuditService.batchInsertSelective(outRequestAudits);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            //释放锁
            DistributeLockUtil.unlock(lockKey);
        }
        //通知 开始
        ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
        //邀请人名字
        String fName = user.getNickName();
        //群
        ImGroupWithBLOBs group = cacheService.selectByPrimaryKeyGroup(groupId);
        //邀请人的群成员信息
        ImGroupMember requestMember = cacheService.getMember(userId, groupId);
        //通知
        cachedThreadPool.execute(() -> {
            //把群主加入房间
            chatServerFeign.joinRoomOne(String.valueOf(userId), groupId);
            //通知 自己的其他设备
            commonService.noticeOwner(userId, groupId, requestMember.getId(), group.getName(), group.getHeadUrl(), device);

            /********************************通知进群的*************************************/
            if (memberSet.size() > 0) {
                commonService.noticeInIds(memberSet, userId, groupId, requestMember.getId(), group.getName(), group.getHeadUrl(), fName);
            }
            /***********************通知无法进群的**********************************/
            if (outSet.size() > 0) {
                commonService.noticeNotInIds(outSet, userId, groupId);
            }
        });
        //返回
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", groupId);
        return YxUtil.createSuccessData(jsonObject);
    }


    @Override
    public List<Map<String, Object>> getGroupsInfo(Map<String, Object> map) {
        return this.imGroupMapper.getGroupsInfo(map);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void transferGroup(ImGroupMember member, ImGroupMember groupMember, int device) {
        //更新成员角色
        //当前用户转为管理员
        member.setRole(groupRoleManager);
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        //转发对象转为群主
        groupMember.setRole(groupRoleOwner);
        int count = groupMemberService.updateByPrimaryKeySelective(groupMember);
        if (res > 0 && count > 0) {
            //更新群成员缓存
            cacheService.updateGroupMemberCache(member);
            //更新群管理员缓存
            cacheService.updateGroupMemberRoleCache(member);
            //更新群成员缓存
            cacheService.updateGroupMemberCache(groupMember);
            //更新群管理员缓存
            cacheService.updateGroupMemberRoleCache(groupMember);
            //通知
            long groupId = member.getGroupId();
            long userId = member.getUserId();
            long receiverUserId = groupMember.getUserId();
            cachedThreadPool.execute(() -> {
                commonService.batchNoticeAllMembers(userId, groupId, device, MSG_TYPE_GROUP_TRANSFER, receiverUserId + "");
            });
        }
    }

    @Override
    @Transactional
    public void managerListSet(Long groupId, Set<Long> managers, Set<Long> notManagers, long userId, int device) {
        //批量操作
        List<ImGroupMember> managerList = new ArrayList<>();
        List<ImGroupMember> notManagerList = new ArrayList<>();
        //管理员
        addMember(managerList, managers, groupRoleManager, groupId);
        //群成员
        addMember(notManagerList, notManagers, groupRoleMember, groupId);
        //设置管理员的
        int count = 0;
        int res = 0;
        if (managerList.size() > 0) {
            count = this.imGroupMapper.batchUpdate(managerList);
        }
        //取消管理员的
        if (notManagerList.size() > 0) {
            res = this.imGroupMapper.batchUpdate(notManagerList);
        }
        if ((managerList.size() > 0 && count > 0) || (notManagerList.size() > 0 && res > 0)) {
            updateMember(managerList);
            updateMember(notManagerList);
            //线程池
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("managers", managers);
            jsonObject.put("notManagers", notManagers);
            cachedThreadPool.execute(() -> {
                commonService.batchNoticeAllMembers(userId, groupId, device, MSG_TYPE_GROUP_MANAGER_SET, jsonObject.toJSONString());
            });
        } else {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }

    /**
     * 添加修改成员信息
     *
     * @param list
     * @param
     * @param
     * @return
     */
    private void updateMember(List<ImGroupMember> list) {
        for (ImGroupMember imGroupMember : list) {
            //更新群成员信息缓存
            cacheService.updateGroupMemberCache(imGroupMember);
            //更新群管理员缓存
            cacheService.updateGroupMemberRoleCache(imGroupMember);
        }
    }

    /**
     * 添加批量成员
     *
     * @param list
     * @param longs
     * @param role
     * @return
     */
    private List<ImGroupMember> addMember(List<ImGroupMember> list, Set<Long> longs, int role, Long groupId) {
        for (long id : longs) {
            ImGroupMember imGroupMember = groupMemberService.selectByPrimaryKey(id);
            imGroupMember.setGroupId(groupId);
            imGroupMember.setRole(role);
            list.add(imGroupMember);
        }
        return list;
    }

    /**
     * 邀请入群
     *
     * @param
     * @param groupId
     * @param userId
     * @param
     * @param
     */
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void inviteJoinGroup(List<ImGroupMember> inMembers, List<ImUserSession> inSessions, List<ImUserRequestAudit> outRequestAudits, long groupId, long userId,Set<Long> memberSet,Set<Long> outSet,int flag){
//        //锁
//        String lockKey = LockKey.inviteJoinGroupKey + groupId + "_" + userId;
//        DistributeLockUtil.tryLock(lockKey, 1, 10);
//        try{
//            if(setOn.equals(flag)){
//                //第一种
//                //批量插入
//                //插入进群的 成员和会话
//                groupMemberService.batchInsertSelective(inMembers);
//                userSessionService.batchCreateSession(inSessions);
//
//
//                //插入无法进群的 申请列表
//                userRequestAuditService.batchInsertSelective(outRequestAudits);
//
//            }else if(setOff.equals(flag)){
//                //第二种
//                //发送给群主的 申请列表
//                userRequestAuditService.batchInsertSelective(outRequestAudits);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//            throw e;
//        }finally {
//            DistributeLockUtil.unlock(lockKey);
//        }
//        //失效其他请求
//        for(long uId:memberSet){
//            this.failReq(uId, groupId);
//        }
//
//
//
//
//        //通知 开始
//        if(setOn.equals(flag)){
//            ImUserWithBLOBs user=userService.selectByPrimaryKey(userId);
//            //邀请人名字
//            String fName=user.getNickName();
//            //群
//            ImGroupWithBLOBs group = commonService.selectByPrimaryKeyGroup(groupId);
//            //通知
//            cachedThreadPool.execute(() -> {
//                /********************************通知进群的*************************************/
//                if(memberSet.size()>0){
//                    //通知进群的
//                    commonService.noticeInIds(memberSet,userId,groupId,group.getName(),group.getHeadUrl(),fName);
//
//                }
//                /***********************通知无法进群的**********************************/
//                if(outSet.size()>0){
//                    commonService.noticeNotInIds(outSet,userId,groupId);
//                }
//            });
//
//            /*****************************和通知群里的有人进群*****************************/
//            JSONArray array=new JSONArray();
//            for(long uId:memberSet){
//                //消息组装
//                ImMessage message = msgFactory.groupSetNotice(userId, groupId, 0, MSG_TYPE_JOIN_GROUP, "");
//                ImUserWithBLOBs destUser=userService.selectByPrimaryKey(uId);
//                JSONObject userJson=new JSONObject();
//                userJson.put("name", destUser.getNickName());
//                userJson.put("headUrl", destUser.getHeadUrl());
//                userJson.put("groupMemberId", destUser.getId());
//                array.add(userJson);
//                JSONObject json=new JSONObject();
//                json.put("text",fName+"邀请"+destUser.getNickName()+"加入了群聊");
//                json.put("memberArray",array);
//
//
//
//
//                //通知群里的有人进群
//                //commonService.noticeInIdsToMember();
//
//            }
//        }else if(setOff.equals(flag)){
//
//        }
//
//
//    }


    /**
     * 邀请入群
     *
     * @param memberIds
     * @param groupId
     * @param userId
     * @param device
     * @param requestContent
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void inviteJoinGroup(Set<Long> memberIds, long groupId, long userId, int device, String requestContent) {
        //查找群
        ImGroupWithBLOBs group = cacheService.selectByPrimaryKeyGroup(groupId);
        //查找当前用户的对应的成员（邀请人）
        ImGroupMember groupMember = cacheService.getMember(userId, groupId);
        List<ImGroupMember> inMembers = new ArrayList<>();
        //群设置了群聊需群主确认
        //群没有设置群聊需确认或者设置了但是邀请人是群主或管理员
        if ((group.getIsInviteConfirm().equals(setOn) && (groupMember.getRole().equals(groupRoleOwner) || groupMember.getRole().equals(groupRoleManager))) || group.getIsInviteConfirm().equals(setOff)) {
            ImMessage msg = msgFactory.groupSetNotice(userId, groupId, device, MSG_TYPE_INVITE_GROUP_REQ, "");
            //被邀请人有设置群聊邀请需确认
            List<Long> inIds = new ArrayList<>();
            for (long inviteId : memberIds) {
                //判断被邀请人是否设置了群聊设置邀请确认
                ImUserWithBLOBs imUser = userService.selectByPrimaryKey(inviteId);
                if (imUser != null && imUser.getIsGroupedNeedAuth().equals(setOn)) {
                    //需要被邀请人审核的
                    commonService.noticeMyself(msg, noDevice, inviteId);
                    //添加到申请列表
                    commonService.addUserRequest(userId, inviteId, groupId, inviteId, groupType, "", requestValidStatus, waitStatus);
                } else {
                    inIds.add(inviteId);
                    //不需要被邀请人审核的
                    //把好友新增到群员表中
                    commonService.addGroupMemberList(inMembers, userId, groupId, inviteId, groupRoleMember);
                    //为加入群的成员创建会话
                    commonService.createSession(inviteId, groupId);
                    //失效其他请求
                    this.failReq(inviteId, groupId);
                }
            }

            if (!inIds.isEmpty()) {
                String str = YxUtil.listToString(inIds);
                Set<Long> memberSet = YxUtil.splitSet(str);
                ImUserWithBLOBs fromUser = cacheService.selectByPrimaryKeyUser(userId);
                cachedThreadPool.execute(() -> {
                    //给他推一个8 xx邀请xx加入了群
                    commonService.batchNoticeInviteMembers(inMembers, userId, groupId, groupMember.getId(), noDevice, MSG_TYPE_JOIN_GROUP, "加入了群聊");
                    //给他推一个14 xx邀请你加入了群
                    commonService.noticeInIds(memberSet, userId, groupId, groupMember.getId(), group.getName(), group.getHeadUrl(), fromUser.getNickName());

                });
            }
        } else if ((group.getIsInviteConfirm().equals(setOn) && groupMember.getRole().equals(groupRoleMember))) {
            //查找群主信息
            ImGroupMember imGroupMember = cacheService.getOwner(groupId);
            //批量 操作
            List<ImUserRequestAudit> requestList = new ArrayList<>();
            for (long uId : memberIds) {
                commonService.addUserRequestList(requestList, userId, uId, groupId, imGroupMember.getUserId(), groupType, requestContent, requestValidStatus, waitOwnerStatus);
            }
            int res = 0;
            //发送给群主的 申请列表
            if (requestList.size() > 0) {
                res = userRequestAuditService.batchInsertSelective(requestList);
            }
            if (res > 0) {
                //给群主、管理员发送一个有群成员邀请好友加入群聊的系统消息
                //通知管理员
                List<Long> list = cacheService.getManagerAndOwner(groupId);
                String userIds = YxUtil.listToString(list);
                cachedThreadPool.execute(() -> {
                    //给群主和管理员发送邀请好友加入群的系统消息
                    commonService.batchNoticeMembers(userIds, userId, groupId, noDevice, MSG_TYPE_INVITE_GROUP_CONFIRM, "");
                });
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateQuitGroup(int device, ImGroupMember member) {
        long groupId = member.getGroupId();
        long userId = member.getUserId();
        long memberId = member.getId();
        // 删除会话
        commonService.deleteSession(userId, groupId);
        //删除群成员信息
        groupMemberService.deleteByPrimaryKey(memberId);
        //删除缓存
        cacheService.removeMember(member);
        JSONObject json = new JSONObject();
        json.put("userId", userId);//操作人的用户id
        json.put("memberId", memberId);//操作人的群成员id
        cachedThreadPool.execute(() -> {
            //通知
            commonService.batchNoticeAllMembers(userId, groupId, device, MSG_TYPE_QUIT_GROUP, json.toJSONString());
            //通知离开房间
            chatServerFeign.leaveRoom(userId, groupId);
        });
        ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
        String fName = user.getNickName();
        json.put("name", fName);//操作人名称
        json.put("textTip", "name退出了群聊");//操作内容
        List<Long> list = cacheService.getManagerAndOwner(groupId);
        String userIds = YxUtil.listToString(list);
        cachedThreadPool.execute(() -> {
            //通知
            commonService.batchNoticeMembersSave(userIds, userId, groupId, device, MSG_TYPE_QUIT_GROUP_MSG, json.toJSONString());
        });
    }

    /**
     * 失效其他的请求
     *
     * @param userId  被邀请人id
     * @param groupId 群id
     * @return
     */
    public boolean failReq(Long userId, Long groupId) {
        boolean result = true;
        List<ImUserRequestAudit> requestList = commonService.getUserRequests(userId, groupId);
        if (requestList != null) {
            for (ImUserRequestAudit request : requestList) {
                int status = request.getAuditStatus();
                //将待群主审核和待被邀请人审核的审批都设置为失效
                //如果是群主审核的改为群审核失效-2
                //如果是被邀请人审核的改为被邀请人审核失效-1
                if (status == waitStatus) {
                    status = failStatus;
                }
                if (status == waitOwnerStatus) {
                    status = failGroupStatus;
                }
                //更新数据库
                ImUserRequestAudit audit = new ImUserRequestAudit();
                audit.setId(request.getId());
                audit.setAuditStatus(status);
                int count = userRequestAuditService.updateByPrimaryKeySelective(audit);
                if (count == fail) {
                    result = false;
                }
            }
        }
        return result;
    }

    @Override
    /**
     * 群成员邀请接口 用户进群审核接口（被邀请人）
     * @param
     * @param userId
     * @param devType
     * @param isAccept 1接受 0拒绝
     * @throws IOException
     */
    public void acceptInviteGroupReq(long userId, int devType, int isAccept, ImUserRequestAudit imUserRequestAudit) {
        long groupId = imUserRequestAudit.getDestId();
        long requestId = imUserRequestAudit.getFromId();
        //如果拒绝加入群
        if (isAccept == setOff) {
            //更新审批状态
            imUserRequestAudit.setAuditStatus(refuseStatus);
            //更新数据库
            imUserRequestAudit.setAuditTime(System.currentTimeMillis());
            userRequestAuditService.updateByPrimaryKeySelective(imUserRequestAudit);
        } else {
            //如果同意进群
            //添加到群里
            ImGroupMember groupMember = commonService.addGroupMember(userId, groupId, userId, groupRoleMember);
            //为加入群的成员创建会话
            commonService.createSession(userId, groupId);
            //更新审批状态
            imUserRequestAudit.setAuditStatus(agreeStatus);
            imUserRequestAudit.setAuditTime(System.currentTimeMillis());
            //更新数据库
            userRequestAuditService.updateByPrimaryKeySelective(imUserRequestAudit);
            //让其他的审批无效
            this.failReq(imUserRequestAudit.getUserId(), groupId);
            //通知
            Set<Long> memberSet = new HashSet<>();
            memberSet.add(groupMember.getUserId());
            List<ImGroupMember> inMembers = new ArrayList<>();
            inMembers.add(groupMember);
            ImGroupWithBLOBs group = cacheService.selectByPrimaryKeyGroup(groupId);
            ImUserWithBLOBs fromUser = cacheService.selectByPrimaryKeyUser(requestId);
            //邀请人的群成员信息
            ImGroupMember requestMember = cacheService.getMember(requestId, groupId);
            cachedThreadPool.execute(() -> {
                //给其他群成员推一个8 xx邀请xx加入了群
                commonService.batchNoticeInviteMembers(inMembers, requestId, groupId, requestMember.getId(), noDevice, MSG_TYPE_JOIN_GROUP, "加入了群聊");
                //给他推一个14 xx邀请你加入了群
                commonService.noticeInIds(memberSet, requestId, groupId, requestMember.getId(), group.getName(), group.getHeadUrl(), fromUser.getNickName());
            });
        }
    }


    /**
     * 群成员邀请接口 用户进群审核接口（群主）
     *
     * @param
     * @param userId
     * @param devType
     * @param isAccept 1接受 0拒绝
     * @throws IOException
     */
    @Override
    public void acceptInviteReq(long userId, int devType, int isAccept, ImUserRequestAudit imUserRequestAudit) {
        //查找申请列表
        long groupId = imUserRequestAudit.getDestId();
        long requestId = imUserRequestAudit.getFromId();
        long destUserId = imUserRequestAudit.getUserId();
        if (groupId > 0 && userId > 0) {
            //用户信息
            ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(destUserId);
            //群信息
            ImGroupWithBLOBs group = cacheService.selectByPrimaryKeyGroup(groupId);
            if (isAccept == setOff) {
                //更新审批状态
                imUserRequestAudit.setAuditStatus(ownerRefuseStatus);
                imUserRequestAudit.setAuditTime(System.currentTimeMillis());
                imUserRequestAudit.setAuditorId(userId);
                userRequestAuditService.updateByPrimaryKeySelective(imUserRequestAudit);
            } else {
                //二维码加群
                if (imUserRequestAudit.getDestType().equals(QRCodeType)) {
                    //添加到群里
                    ImGroupMember inMember = commonService.addGroupMember(userId, groupId, destUserId, groupRoleMember);
                    //为加入群的成员创建会话
                    commonService.createSession(destUserId, groupId);
                    //修改二维码次数
                    ImTempIdentity imTempIdentity = tempIdentityService.selectByIdentityId(groupId);
                    imTempIdentity.setCurrentTimes(imTempIdentity.getCurrentTimes() + 1);
                    tempIdentityService.updateByPrimaryKeySelective(imTempIdentity);
                    //通知

                    //组装消息
                    JSONObject json = new JSONObject();
                    json.put("groupId", groupId);
                    json.put("groupName", group.getName());
                    json.put("groupHeadUrl", group.getHeadUrl());
                    //组装消息
                    json.put("textTip", "name通过扫描二维码加入了群聊");
                    json.put("name", user.getNickName());
                    json.put("memberId", inMember.getId());
                    json.put("userId", destUserId);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.putAll(json);
                    JSONArray array = new JSONArray();
                    JSONObject userJson = new JSONObject();
                    userJson.put("name", user.getNickName());
                    userJson.put("headUrl", user.getHeadUrl());
                    userJson.put("memberId", inMember.getId());
                    array.add(userJson);
                    jsonObject.put("memberArray", array);
                    cachedThreadPool.execute(() -> {
                        //给进群的推一个14 name通过扫描二维码加入了群聊
                        commonService.noticeSingleAndSave(destUserId, groupId, noDevice, MSG_TYPE_ACCEPT_GROUP, json.toJSONString());
                        //给群里的人推一个8 name通过扫描二维码加入了群聊
                        commonService.batchNoticeAllMembersSave(destUserId, groupId, noDevice, MSG_TYPE_JOIN_GROUP, jsonObject.toJSONString());
                        //通知被邀请已经进群的ids加入房间
                        chatServerFeign.joinRoomOne(String.valueOf(destUserId), groupId);

                    });
                } else {
                    if (user.getIsGroupedNeedAuth().equals(setOn)) {
                        //需要审批
                        //新建一条给用户审核
                        commonService.addUserRequest(requestId, destUserId, groupId, destUserId, groupType, "", requestValidStatus, waitStatus);
                    } else {
                        //不需要审批
                        //添加到群里
                        ImGroupMember groupMember = commonService.addGroupMember(userId, groupId, destUserId, groupRoleMember);
                        //为加入群的成员创建会话
                        commonService.createSession(destUserId, groupId);
                        //通知对方
                        Set<Long> memberSet = new HashSet<>();
                        memberSet.add(destUserId);
                        List<ImGroupMember> inMembers = new ArrayList<>();
                        inMembers.add(groupMember);
                        //被邀请人名称
                        ImUserWithBLOBs fromUser = cacheService.selectByPrimaryKeyUser(requestId);
                        //邀请人的群成员信息
                        ImGroupMember requestMember = cacheService.getMember(requestId, groupId);
                        cachedThreadPool.execute(() -> {
                            //给其他群成员推一个8 xx邀请xx加入了群
                            commonService.batchNoticeInviteMembers(inMembers, requestId, groupId, requestMember.getId(), noDevice, MSG_TYPE_JOIN_GROUP, "加入了群聊");
                            //给他推一个14 xx邀请你加入了群
                            commonService.noticeInIds(memberSet, requestId, groupId, requestMember.getId(), group.getName(), group.getHeadUrl(), fromUser.getNickName());
                        });
                        //让其他的审批无效
                        this.failReq(destUserId, groupId);
                    }
                }
                //更新审批状态
                //保存
                imUserRequestAudit.setAuditStatus(ownerAgreeStatus);
                imUserRequestAudit.setAuditTime(System.currentTimeMillis());
                imUserRequestAudit.setAuditorId(userId);
                userRequestAuditService.updateByPrimaryKeySelective(imUserRequestAudit);
            }
        }
    }

    @Override
    @Caching(evict = {
            //群成员信息
            @CacheEvict(value = memberCache, key = "#groupId+':*'"),
            @CacheEvict(value = ownerCache, key = "#groupId"),
            //群成员数
            @CacheEvict(value = countMemberCache, key = "#groupId"),
            //群管理员数
            @CacheEvict(value = countManagerCache, key = "#groupId"),
            //群成员list
            @CacheEvict(value = memberListCache, key = "#groupId"),
            //全部群成员set
            @CacheEvict(value = allMemberSetCache, key = "#groupId"),
            //群管理员set id
            @CacheEvict(value = managerSetCache, key = "#groupId"),
            //群成员set id
            @CacheEvict(value = memberSetCache, key = "#groupId"),
            //群管理员和群主list
            @CacheEvict(value = managerAndOwnerListCache, key = "#groupId"),
            //群成员Set
            @CacheEvict(value = groupMemberSetCache, key = "#groupId"),
            //群缓存
            @CacheEvict(value = groupCache, key = "#groupId"),

            //群成员列表
            //@CacheEvict(value = memberListMapCache, key = "#groupId"),
            //群管理员列表
            //@CacheEvict(value = managerListMapCache, key = "#groupId"),


            //群成员 chat server
            @CacheEvict(value = memberCacheMap, key = "'*:'+#groupId"),
            //私密群组缓存 chat server
            @CacheEvict(value = secretMemberCacheMap, key = "'*:'+#groupId"),
            @CacheEvict(value = memberListCacheMap, key = "#groupId")


    })
    public int dismissGroup(long groupId, long userId, int devType) {
        ImGroupWithBLOBs group = cacheService.selectByPrimaryKeyGroup(groupId);
        // 删除会话（只删除自己的会话）
        commonService.deleteSession(userId, groupId);
        // 删除置顶
        commonService.deleteTop(userId, groupId, groupType);
        //修改群状态为解散 （暂不删除群成员）
        group.setGroupStatus(groupDissolve);
        int i = this.updateByPrimaryKeySelective(group);

        if (i > 0) {
            //获取所有群成员
            List<Long> memberIds = cacheService.getAllMembers(groupId);
            //清除chat server的缓存
            ImGroupMember member = new ImGroupMember();
            for (Long uId : memberIds) {
                member.setUserId(uId);
                //清除群缓存
                cacheService.removeGroupCache(uId);
                //清除私密群缓存
                cacheService.removeSecretGroupCache(member);
            }
            //更新群缓存
            cacheService.updateGroupCache(group);
            String str = YxUtil.listToString(memberIds);
            //通知解散群消息
            //最后 通知
            JSONObject json = new JSONObject();
            json.put("groupStatus", group.getGroupStatus());
            json.put("groupId", groupId);
            cachedThreadPool.execute(() -> {
                commonService.batchNoticeAllMembers(userId, groupId, noDevice, MSG_TYPE_GROUP_DISMISS, json.toJSONString());
                chatServerFeign.leaveRoomUserIds(str, groupId);
            });

        }
        return i;
    }


    @Override
    public void updateGroupMemberMark(ImGroupMember member, String markName, int device) {
        member.setMarkName(markName);
        int res = groupMemberService.updateByPrimaryKeySelective(member);
        if (res > 0) {
            //更新群成员缓存
            cacheService.updateGroupMemberCache(member);
            //最后 通知
            JSONObject json = new JSONObject();
            json.put("memberId", member.getId());
            json.put("groupId", member.getGroupId());
            json.put("markName", markName);
            //推送
            cachedThreadPool.execute(() -> {
                commonService.batchNoticeAllMembers(member.getUserId(), member.getGroupId(), noDevice, MSG_TYPE_MODIFY_GROUP_COMMENT, json.toJSONString());
            });
        }
    }

    /**
     * 移除群成员
     *
     * @param
     * @param destMember
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeFromGroup(ImGroupMember member, ImGroupMember destMember) {
        long fromUserId = member.getUserId();
        long destUserId = destMember.getUserId();
        long groupId = destMember.getGroupId();
        //删除会话
        //commonService.deleteSession(destUserId, groupId);
        //删除群成员 缓存
        cacheService.removeMember(destMember);
        int i = groupMemberService.deleteByPrimaryKey(destMember.getId());
        if (i > 0) {
            //组装消息removeFromGroup
            ImUserWithBLOBs fromUser = cacheService.selectByPrimaryKeyUser(fromUserId);
            String fName = fromUser.getNickName();
            ImUserWithBLOBs destUser = cacheService.selectByPrimaryKeyUser(destUserId);

            JSONObject json = new JSONObject();
            json.put("groupId", member.getGroupId());
            json.put("destName", destUser.getNickName());
            json.put("destUserId", destUserId);
            json.put("destMemberId", destMember.getId());
            List<Long> members = cacheService.getManagerAndOwner(groupId);
            String userIds = YxUtil.listToString(members);
            cachedThreadPool.execute(() -> {
                //给群里的人推一个删除群成员的
                commonService.batchNoticeAllMembers(fromUserId, groupId, noDevice, MSG_TYPE_REMOVE_GROUP, json.toJSONString());
                json.put("fromName", fName);
                json.put("fromUserId", fromUserId);
                json.put("fromMemberId", member.getId());
                json.put("textTip", "fromName将destName移出了群聊");
                //给群主管理员推一条 xx将xx 移出了群聊 保存聊天记录
                commonService.batchNoticeMembersSave(userIds, fromUserId, groupId, noDevice, MSG_TYPE_REMOVE_GROUP_MSG, json.toJSONString());
                //离开房间
                chatServerFeign.leaveRoom(destUserId, groupId);
            });
        }

    }

    /**
     * 批量移除群成员
     *
     * @param member
     * @param memberIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removeIdsFromGroup(ImGroupMember member, Set<Long> memberIds) {
        long groupId = member.getGroupId();
        long fromUserId = member.getUserId();
        List<Long> members = cacheService.getManagerAndOwner(groupId);
        String userIds = YxUtil.listToString(members);
        ImUserWithBLOBs formUser = cacheService.selectByPrimaryKeyUser(fromUserId);
        String fromName = formUser.getNickName();
        JSONObject object = new JSONObject();
        object.put("fromName", fromName);
        object.put("fromUserId", fromUserId);
        object.put("fromMemberId", member.getId());
        object.put("textTip", "fromName将destName移出了群聊");
        //实际删除
        for (long memberId : memberIds) {
            //获取被移除人的信息
            ImGroupMember destMember = groupMemberService.selectByPrimaryKey(memberId);
            long destUserId = destMember.getUserId();
            //删除缓存
            cacheService.removeMember(destMember);
            groupMemberService.deleteByPrimaryKey(memberId);
            //user
            ImUserWithBLOBs destUser = cacheService.selectByPrimaryKeyUser(destUserId);
            JSONObject json = new JSONObject();
            json.put("groupId", member.getGroupId());
            json.put("destName", destUser.getNickName());
            json.put("destUserId", destUserId);
            json.put("destMemberId", memberId);
            cachedThreadPool.execute(() -> {
                //给群里的人推一个删除群成员的
                commonService.batchNoticeAllMembers(fromUserId, groupId, noDevice, MSG_TYPE_REMOVE_GROUP, json.toJSONString());
                json.putAll(object);
                //给群主管理员推一条 xx将xx 移出了群聊 保存聊天记录
                commonService.batchNoticeMembersSave(userIds, fromUserId, groupId, noDevice, MSG_TYPE_REMOVE_GROUP_MSG, json.toJSONString());
                //离开房间
                chatServerFeign.leaveRoom(destUserId, groupId);
            });

        }

    }

    @Override
    public List<ImGroup> selectByConditions(ImGroup record) {
        return this.imGroupMapper.selectByConditions(record);
    }

    @Override
    //@Cacheable(cacheNames = friendReqListCache,key ="#map.get('userId')",unless = "#result == null")
    public List<Map<String, Object>> friendReqList(Map<String, Object> map) {
        return this.imGroupMapper.friendReqList(map);
    }

    @Override
    //@Cacheable(cacheNames = countFriendReqListCache, key = "#map.get('userId')",unless = "#result == null")
    public int countFriendReqList(Map<String, Object> map) {
        return imGroupMapper.countFriendReqList(map);
    }

    @Override
    //@Cacheable(cacheNames = inviteReqListCache, key = "#userId",unless = "#result == null")
    public List<Map<String, Object>> inviteReqList(long userId) {
        return this.imGroupMapper.inviteReqList(userId);
    }

    @Override
    //@Cacheable(cacheNames = countInviteReqListCache, key = "#userId",unless = "#result == null")
    public int countInviteReqList(long userId) {
        return imGroupMapper.countInviteReqList(userId);
    }

    @Override
    //@Cacheable(cacheNames = confirmReqListCache, key = "#userId",unless = "#result == null")
    public List<Map<String, Object>> confirmReqList(long userId) {
        return this.imGroupMapper.confirmReqList(userId);
    }

    @Override
    //@Cacheable(cacheNames = countConfirmReqListCache, key = "#userId",unless = "#result == null")
    public int countConfirmReqList(long userId) {
        return imGroupMapper.countConfirmReqList(userId);
    }

    @Override
    public List<Map<String, Object>> groupFriendQuery(Map<String, Object> map) {
        return this.imGroupMapper.groupFriendQuery(map);
    }

}
