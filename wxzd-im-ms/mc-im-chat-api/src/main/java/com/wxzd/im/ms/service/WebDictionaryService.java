package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.WebDictionary;

public interface WebDictionaryService {
    int deleteByPrimaryKey(Integer id);

    int insert(WebDictionary record);

    int insertSelective(WebDictionary record);

    WebDictionary selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(WebDictionary record);

    int updateByPrimaryKey(WebDictionary record);

    /**
     * 根据类型查找
     *
     * @param type
     * @return
     */
    WebDictionary selectByType(String type);
}
