package com.wxzd.im.ms.exception;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.wxzd.im.ms.parameter.ReturnConstant.sysErrorReturn;

@Configuration
public class ImExceptionResolver extends SimpleMappingExceptionResolver {

    Log logger = LogFactory.get();

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
                                              Exception ex) {
        logger.info("程序发生异常: {}", ex.toString());
        try {
            ModelAndView mv = new ModelAndView();
            /*	使用FastJson提供的FastJsonJsonView视图返回，不需要捕获异常	*/
            FastJsonJsonView view = new FastJsonJsonView();
            Map<String, Object> attributes = new HashMap<String, Object>();
            attributes.put("code", "-1");
            HashMap<String, Object> info = new HashMap<String, Object>();
            info.put("info", sysErrorReturn);
            logger.info(ex.getMessage());
            attributes.put("data", info);
            view.setAttributesMap(attributes);
            mv.setView(view);
            return mv;
            //response.sendRedirect(request.getContextPath()+"/exception");
        } catch (Exception e) {
            logger.info("异常后跳转又异常: {}", ex.toString());
        }
        return null;
    }
}
