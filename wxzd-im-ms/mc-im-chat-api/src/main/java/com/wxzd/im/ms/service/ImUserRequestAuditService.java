package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImUserRequestAudit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ImUserRequestAuditService {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserRequestAudit record);

    int insertSelective(ImUserRequestAudit record);

    ImUserRequestAudit selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserRequestAudit record);

    int updateByPrimaryKeyWithBLOBs(ImUserRequestAudit record);

    int updateByPrimaryKey(ImUserRequestAudit record);

    ImUserRequestAudit selectByCondition(ImUserRequestAudit record);

    List<ImUserRequestAudit> selectByConditions(ImUserRequestAudit record);

    ImUserRequestAudit isSentMsg(Map<String, Object> map);

    /**
     * 更新好友请求
     *
     * @param auditStatus
     * @param userId
     * @param time
     * @param friendId
     * @return
     */
    int updateByUserIdAndFriendId(Integer auditStatus, Long userId, Long time, Long friendId);

    /**
     * 获取未读的好友请求数量
     *
     * @param destId
     * @return
     */
    List<Map<String, Object>> unReadFriendReqCount(Long destId);

    /**
     * 获取未读的进群确认数量
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> unReadConfirmReqCount(Long userId);

    /**
     * 获取未读的群组邀请数量
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> unReadInviteReqCount(Long userId);

    /**
     * 读取请求
     *
     * @param ids
     * @param userId
     * @return
     */
    int readRequest(String ids, long userId);

    int deleteByUserIdAndFriendId(long userId, long destId);

    int batchInsertSelective(@Param("list") List<ImUserRequestAudit> list);

}