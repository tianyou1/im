package com.wxzd.im.ms.service;

import com.wxzd.im.ms.entity.ImUserSession;
import com.wxzd.im.ms.entity.ImUserSessionKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ImUserSessionService {
    int deleteByPrimaryKey(ImUserSessionKey key);

    int insert(ImUserSession record);

    int insertSelective(ImUserSession record);

    ImUserSession selectByPrimaryKey(ImUserSessionKey key);

    int updateByPrimaryKeySelective(ImUserSession record);

    int updateByPrimaryKey(ImUserSession record);

    int createSession(ImUserSession record);

    int deleteByCondition(ImUserSession imUserSession);

    int batchDelete(List<ImUserSession> list);

    /**
     * 读取会话
     *
     * @param userId
     * @param destId
     * @param destType
     * @return
     */
    int readSessionCount(Long userId, Long destId, Integer destType);

    /**
     * 删除会话
     *
     * @param userId
     * @param destId
     * @param destType
     * @return
     */
    int deleteSession(Long userId, Long destId, Integer destType);

    int batchCreateSession(@Param("list") List<ImUserSession> list);
}