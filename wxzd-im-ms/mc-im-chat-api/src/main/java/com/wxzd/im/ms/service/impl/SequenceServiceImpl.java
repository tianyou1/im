package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.Sequence;
import com.wxzd.im.ms.enums.SequenceEnum;
import com.wxzd.im.ms.mapper.SequenceMapper;
import com.wxzd.im.ms.service.SequenceService;
import com.wxzd.im.ms.utils.DistributeLockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SequenceServiceImpl implements SequenceService {

    @Autowired
    private SequenceMapper sequenceMapper;


    /**
     * 获取序列
     *
     * @return
     */
    private Sequence getSequence() {
        return sequenceMapper.selectByName("temp_id");
    }


    public long getTempId(String lockKey) {
        DistributeLockUtil.tryLock(lockKey, 1, 10);
        try {
            //获得锁，进行业务处理（处理过程中同样的SceneType.VALIDATE_MOBILE.name()+uuid 值在全局内不能再次进入，保证同步性）
            //业务代码块
            Sequence sequence = getSequence();
            if (sequence.getCurrentValue() == 0) {
                return SequenceEnum.tempIdentityId.getCode() + sequence.getInitialValue();
            } else {
                sequence.setCurrentValue(sequence.getCurrentValue() + 1);
                sequenceMapper.updateByPrimaryKey(sequence);
                return SequenceEnum.tempIdentityId.getCode() + sequence.getCurrentValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DistributeLockUtil.unlock(lockKey);
        }
        return 0;
    }
}
