package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImUserReports;
import com.wxzd.im.ms.entity.ImUserReportsWithBLOBs;
import com.wxzd.im.ms.mapper.ImUserReportsMapper;
import com.wxzd.im.ms.service.ImUserReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImUserReportsServiceImpl implements ImUserReportsService {
    @Autowired
    private ImUserReportsMapper imUserReportsMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return imUserReportsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImUserReportsWithBLOBs record) {
        return imUserReportsMapper.insert(record);
    }

    @Override
    public int insertSelective(ImUserReportsWithBLOBs record) {
        return imUserReportsMapper.insertSelective(record);
    }

    @Override
    public ImUserReportsWithBLOBs selectByPrimaryKey(Long id) {
        return imUserReportsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImUserReportsWithBLOBs record) {
        return imUserReportsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImUserReportsWithBLOBs record) {
        return imUserReportsMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImUserReports record) {
        return imUserReportsMapper.updateByPrimaryKey(record);
    }
}
