package com.wxzd.im.ms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.wxzd.im.ms.parameter.StaticConstant.*;

@Service
public class ReadyService {
    @Autowired
    private ImGroupService imGroupService;

    @Autowired
    private ImFriendService imFriendService;

    @Autowired
    private ImTopService imTopService;

    @Autowired
    private ImUserRequestAuditService imUserRequestAuditService;

    /**
     * 获取群列表
     *
     * @param userId
     * @param isSecret
     * @return
     */
    //@Cacheable(cacheNames = groupsInfoCache,key ="#p0+':'+#p1")
    public List<Map<String, Object>> getGroupsInfo(long userId, Integer isSecret) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        if (isSecret.equals(isSecretModeNo)) {
            map.put("isSecret", isSecret);
        }
        return imGroupService.getGroupsInfo(map);
    }

    /**
     * 获取好友列表
     *
     * @param userId
     * @param isSecret
     * @return
     */
    public List<Map<String, Object>> getFriendInfo(long userId, Integer isSecret) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        if (isSecret.equals(isSecretModeNo)) {
            map.put("isSecret", isSecret);
        }
        return imFriendService.getFriendInfo(map);
    }

    /**
     * 获取会话列表
     *
     * @param userId
     * @param isSecret
     * @return
     */
    public List<Map<String, Object>> getSessionList(long userId, Integer isSecret) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        if (isSecret.equals(isSecretModeNo)) {
            map.put("isSecret", isSecret);
        }
        return imFriendService.getSessionList(map);
    }

    /**
     * 获取置顶列表
     *
     * @param userId
     * @return
     */
    public List<Map<String, Object>> getTopList(long userId) {
        return imTopService.getTopList(userId);
    }

    /**
     * 获取未读的数量
     *
     * @param userId
     * @return
     */
    public int unReadReqCount(int reqType, long userId) {

        List<Map<String, Object>> list = new ArrayList<>();

        if (reqType == friendReqType) {
            list = imUserRequestAuditService.unReadFriendReqCount(userId);
        } else if (reqType == confirmReqType) {
            list = imUserRequestAuditService.unReadConfirmReqCount(userId);
        } else if (reqType == inviteReqType) {
            list = imUserRequestAuditService.unReadInviteReqCount(userId);
        }

        int count = 0;
        for (Map<String, Object> map : list) {
            Object o = map.get("isRead");
            String str = String.valueOf(o);
            if (o == null || o.equals("")) {
                count++;
                continue;
            }
            String[] strings = str.split(",");
            boolean result = Arrays.asList(strings).contains(String.valueOf(userId));
            if (!result) {
                count++;
            }
        }
        return count;
    }

}
