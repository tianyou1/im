package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImUserSession;
import com.wxzd.im.ms.entity.ImUserSessionKey;
import com.wxzd.im.ms.mapper.ImUserSessionMapper;
import com.wxzd.im.ms.service.ImUserSessionService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImUserSessionServiceImpl implements ImUserSessionService {

    @Autowired
    private ImUserSessionMapper imUserSessionMapper;

    @Override
    public int deleteByPrimaryKey(ImUserSessionKey key) {
        return this.imUserSessionMapper.deleteByPrimaryKey(key);
    }

    @Override
    public int insert(ImUserSession record) {
        return this.imUserSessionMapper.insert(record);
    }

    @Override
    public int insertSelective(ImUserSession record) {
        return this.imUserSessionMapper.insertSelective(record);
    }

    @Override
    public ImUserSession selectByPrimaryKey(ImUserSessionKey key) {
        return this.imUserSessionMapper.selectByPrimaryKey(key);
    }

    @Override
    public int updateByPrimaryKeySelective(ImUserSession record) {
        return this.imUserSessionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ImUserSession record) {
        return this.imUserSessionMapper.updateByPrimaryKey(record);
    }

    @Override
    public int createSession(ImUserSession record) {
        return this.imUserSessionMapper.createSession(record);
    }

    @Override
    public int deleteByCondition(ImUserSession imUserSession) {
        return this.imUserSessionMapper.deleteByCondition(imUserSession);
    }

    @Override
    public int batchDelete(List<ImUserSession> list) {
        return this.imUserSessionMapper.batchDelete(list);
    }

    @Override
    public int readSessionCount(Long userId, Long destId, Integer destType) {
        return imUserSessionMapper.readSessionCount(userId, destId, destType);
    }

    @Override
    public int deleteSession(Long userId, Long destId, Integer destType) {
        return imUserSessionMapper.deleteSession(userId, destId, destType);
    }

    @Override
    public int batchCreateSession(@Param("list") List<ImUserSession> list) {
        return this.imUserSessionMapper.batchCreateSession(list);
    }
}
