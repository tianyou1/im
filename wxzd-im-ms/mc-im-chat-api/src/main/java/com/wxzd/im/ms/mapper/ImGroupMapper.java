package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImGroup;
import com.wxzd.im.ms.entity.ImGroupMember;
import com.wxzd.im.ms.entity.ImGroupWithBLOBs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ImGroupMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImGroupWithBLOBs record);

    int insertSelective(ImGroupWithBLOBs record);

    ImGroupWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImGroupWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImGroupWithBLOBs record);

    int updateByPrimaryKey(ImGroup record);

    List<Map<String, Object>> getGroupsInfo(Map<String, Object> map);

    int batchUpdate(List<ImGroupMember> list);

    List<ImGroup> selectByConditions(ImGroup record);

    List<Map<String, Object>> friendReqList(Map<String, Object> map);

    int countFriendReqList(Map<String, Object> map);

    List<Map<String, Object>> inviteReqList(long userId);

    int countInviteReqList(long userId);

    List<Map<String, Object>> confirmReqList(long userId);

    int countConfirmReqList(long userId);

    List<Map<String, Object>> groupFriendQuery(Map<String, Object> map);

}