package com.wxzd.im.ms.api.controller;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.zxing.WriterException;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.parameter.LockKey;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.wxzd.im.ms.parameter.ReturnConstant.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;


@Controller
@Api(value = "用户信息")
@RequestMapping(value = "/user", method = {RequestMethod.POST})
public class UserController {
    @Autowired
    private ImUserSecondaryVerificationService secondService;
    @Autowired
    private SmsService smsService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private ImUserService userService;
    @Autowired
    private MessageFactory msgFactory;
    @Autowired
    private ChatServerFeign chatServerFeign;
    @Autowired
    private ImTempIdentityService tempIdentityService;
    @Autowired
    private SequenceService sequenceService;
    @Autowired
    private ImFriendService friendService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private CacheService cacheService;

    /**
     * 获取个人信息(用户ID、用户头像、用户昵称、用户性别、签名、地区、相册（可选）)
     */
    @ApiOperation("获取个人信息")
    @RequestMapping("/findUserInfo")
    @ResponseBody
    public ResponseData findUserInfo(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs myInfo = userService.getMyInfo(userId);
        return YxUtil.createSuccessData(myInfo);
    }

    /**
     * 自动注销设置查询接口 状态（成功/失败）、是否进行自动注销、自动注销时间（N月）
     */
    @ApiOperation("获取自动注销")
    @RequestMapping("/findAutoCancel")
    @ResponseBody
    public ResponseData findAutoCancel(@RequestHeader String token) {
        Long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs my = userService.selectByPrimaryKey(userId);
        JSONObject json = new JSONObject();
        json.put("isAutoCancel", my.getIsAutoCancel());
        json.put("autoCancelMonths", my.getAutoCancelMonths());
        return YxUtil.createSuccessData(json);
    }

    /**
     * 自动注销设置接口
     */
    @ApiOperation("设置自动注销")
    @RequestMapping("/setAutoCancel")
    @ResponseBody
    public ResponseData setAutoCancel(@RequestHeader String token,
                                      @ApiParam("自动注销，0不自动注销，1自动注销") @RequestParam int isAutoCancel,
                                      @ApiParam("自动注销时间，N月") @RequestParam(defaultValue = "0") int autoCancelMonths) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer devType = CommonUtils.getDevTypeByToken(token);
        if (autoCancelYes == isAutoCancel && autoCancelMonths == 0) {
            return YxUtil.createFail(destroyTimeReturn);
        }
        ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
        user.setIsAutoCancel(isAutoCancel);
        user.setAutoCancelMonths(autoCancelMonths);
        int count = userService.updateByPrimaryKeySelective(user);
        return getResult(count, user, devType, userId);
    }

    /**
     * 手动注销设置接口
     */
    @ApiOperation("用户密码验证接口")
    @RequestMapping("/userPasswordCheck")
    @ResponseBody
    public ResponseData userPasswordCheck(@RequestHeader String token,
                                          @ApiParam("用户密码") @RequestParam String password) {
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        //解密密码
        password = RSACipherUtil.decrypt(password);
        password = Utils.encrypt(password);
        if (!password.equals(imUser.getPwd())) {
            return YxUtil.createFail(pwdErrorReturn);
        }
        ImUserSecondaryVerification imUserSecondaryVerification = secondService.selectByUserId(userId);
        if (imUserSecondaryVerification != null && imUserSecondaryVerification.getIsSecondaryVerification().equals(isSecondaryYes)) {
            JSONObject json = new JSONObject();
            //生成临时token，指定五分钟有效
            json.put("cancelWand", TokenUtil.generateCancelWand(userId, devType));
            return YxUtil.createSuccessData(json);
        } else {
            imUser.setStatus(loseUser);
            int count = userService.updateByPrimaryKeySelective(imUser);
            return getResult(count, imUser, devType, userId);
        }
    }

    private ResponseData userSetResult(int devType, long userId) {
        if (devType > 0) {
            // 通知我的其它设备，设置已经修改
            ImMessage msg = msgFactory.userSetUpdateNotice(userId, userId, devType);
            chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
        }
        return YxUtil.createSimpleSuccess(setSuccessReturn);
    }

    /**
     * 手动注销设置接口
     */
    @ApiOperation("二次验证注销接口")
    @RequestMapping("/userSecondCancelCheck")
    @ResponseBody
    public ResponseData userSecondCancelCheck(@RequestHeader String token,
                                              @ApiParam("销毁令牌") @RequestParam String cancelWand,
                                              @ApiParam("验证码") @RequestParam String checkInfo) {

        if (StringUtils.isEmpty(checkInfo)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);

        String[] cancelWandArray = Utils.decrypt(cancelWand).split("\\|");
        //时间（token生成时间）
        long time = Long.parseLong(Utils.decrypt(cancelWandArray[0]));

        if (!Utils.decrypt(cancelWandArray[3]).equals(TokenEnum.cancelToken.getCode() + "")) {
            return YxUtil.createFail(checkInfoErrorReturn);
        }

        if (time < System.currentTimeMillis()) {
            return YxUtil.createFail(checkInfoTimeOutReturn);
        }

        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);

        //手机号验证
        if (typePhone.equals(secondaryVerification.getVerifyType())) {
            boolean b = smsService.equalValidate(secondaryVerification.getMobile(), checkInfo);
            return secondResult(userId, b, devType);
        }

        //邮箱验证
        if (typeEmail.equals(secondaryVerification.getVerifyType())) {
            boolean b = emailService.equalValidate(secondaryVerification.getEmail(), checkInfo);
            return secondResult(userId, b, devType);
        }
        //谷歌验证
        if (typeGoogle.equals(secondaryVerification.getVerifyType())) {
            boolean b = GoogleAuthenticator.authcode(checkInfo, secondaryVerification.getGoogleKey());
            return secondResult(userId, b, devType);
        }
        //二级密码验证
        if (typeSecond.equals(secondaryVerification.getVerifyType())) {
            boolean b = secondaryVerification.getSecondaryPwd().equalsIgnoreCase(Utils.encrypt(checkInfo));
            return secondResult(userId, b, devType);
        }
        return YxUtil.createSimpleSuccess(destroySuccessReturn);
    }

    //二次验证返回
    private ResponseData secondResult(long userId, boolean b, int devType) {
        if (b) {
            ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
            user.setStatus(loseUser);
            int count = userService.updateByPrimaryKeySelective(user);
            return getResult(count, user, devType, userId);
        } else {
            return YxUtil.createFail(checkInfoFailReturn);
        }
    }

    /**
     * 个人添加方式查询接口
     */
    @ApiOperation("个人添加方式查询接口")
    @RequestMapping("/findUserSearch")
    @ResponseBody
    public ResponseData findUserSearch(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUser imUser = userService.selectByPrimaryKey(userId);
        JSONObject json = new JSONObject();
        json.put("searchUserId", imUser.getSearchUserId());
        json.put("searchAccount", imUser.getSearchAccount());
        json.put("searchGroupId", imUser.getSearchGroupId());
        return YxUtil.createSuccessData(json);
    }

    /**
     * 个人添加方式设置接口
     */
    @ApiOperation("个人添加方式设置接口")
    @RequestMapping("/setUserSearch")
    @ResponseBody
    public ResponseData setUserSearch(@RequestHeader String token,
                                      @ApiParam("设置类型:1:用户ID、2:群名片、3:名片") @RequestParam int searchType,
                                      @ApiParam("设置开关:0不允许，1允许") @RequestParam int isSearch) {
        if (CheckUtil.checkSearchType(searchType)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);

        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        if (searchTypeUserId == searchType) {
            imUser.setSearchUserId(isSearch);
        }

        if (searchTypeGroupId == searchType) {
            imUser.setSearchGroupId(isSearch);
        }

        if (searchTypeAccount == searchType) {
            imUser.setSearchAccount(isSearch);
        }
        int count = userService.updateByPrimaryKeySelective(imUser);

        if (count > 0) {
            //更新用户缓存
            cacheService.updateUserCache(imUser);
            return userSetResult(devType, userId);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }

    }


    /**
     * 私密模式密码设置接口
     */
    @ApiOperation("隐私密码设置接口")
    @RequestMapping("/setSecretMode")
    @ResponseBody
    public ResponseData setSecretMode(@RequestHeader String token,
                                      @ApiParam("隐私密码") @RequestParam String secretModePwd) {
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserSecondaryVerification secondaryVerification = secondService.selectByUserId(userId);
        //隐私密码 解密
        secretModePwd = RSACipherUtil.decrypt(secretModePwd);
        if (StringUtils.isEmpty(secretModePwd)) {
            return YxUtil.createFail(paramErrorReturn);
        }

        if (secretModePwd.trim().length() != secretPwdL) {
            return YxUtil.createFail(paramErrorReturn);
        }

        //如果开启，判断是否验证时间超过了
        if (secondaryVerification != null && isSecondaryYes.equals(secondaryVerification.getIsSecondaryVerification())) {
            //有设置二级认证
            String checkToken = TokenUtil.getUserTokenCheck(token);
            if (StringUtils.isEmpty(checkToken)) {
                return YxUtil.createFail(tempOperationTimeOutReturn);
            } else {
                return setSecretModePwd(userId, secretModePwd, devType);
            }
        } else {
            //没有设置二级认证
            return setSecretModePwd(userId, secretModePwd, devType);
        }
    }

    private ResponseData setSecretModePwd(long userId, String secretModePwd, int devType) {
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        imUser.setSecretModePwd(Utils.encrypt(secretModePwd));
        int res = userService.updateByPrimaryKeySelective(imUser);
        if (res > 0) {
            //更新用户缓存
            cacheService.updateUserCache(imUser);
            //删除好友明细缓存
            friendService.deleteDetailCacheUserId(imUser.getId());
            return userSetResult(devType, userId);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }

    /**
     * 私密模式进入接口
     */
    @ApiOperation("私密模式进入接口")
    @RequestMapping("/enterSecretMode")
    @ResponseBody
    public ResponseData enterSecretMode(@RequestHeader String token,
                                        @ApiParam("隐私密码") @RequestParam String secretModePwd) {
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        if (StringUtils.isEmpty(secretModePwd)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //解密隐私密码
        secretModePwd = RSACipherUtil.decrypt(secretModePwd);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        if (CheckUtil.checkSecretPwd(imUser, secretModePwd)) {
            return YxUtil.createFail(secretPwdErrorReturn);
        }
        JSONObject data = new JSONObject();
        token = TokenUtil.generateUserToken(userId, devType, TokenEnum.tokenValidSecret.getCode());
        data.put("token", token);
        return YxUtil.createSuccessData(data);
    }

    /**
     * 私密模式退出接口
     */
    @ApiOperation("私密模式退出接口")
    @RequestMapping("/quitSecretMode")
    @ResponseBody
    public ResponseData quitSecretMode(@RequestHeader String token,
                                       @ApiParam("隐私密码") @RequestParam String secretModePwd) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer devType = CommonUtils.getDevTypeByToken(token);
        if (StringUtils.isEmpty(secretModePwd)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        //解密隐私密码
        secretModePwd = RSACipherUtil.decrypt(secretModePwd);
        ImUser imUser = userService.selectByPrimaryKey(userId);
        if (Utils.encrypt(secretModePwd).equals(imUser.getSecretModePwd())) {
            JSONObject data = new JSONObject();
            token = TokenUtil.generateUserToken(userId, devType, TokenEnum.tokenValid.getCode());
            data.put("token", token);
            List<ImGroupMember> imGroupMembers = cacheService.getAcceptSecretMember(userId, isCheck, isSecret);
            List<Long> roomIds = new ArrayList<>();
            for (ImGroupMember imGroupMember : imGroupMembers) {
                roomIds.add(imGroupMember.getGroupId());
            }
            //退出私密，移除非私密群聊
            chatServerFeign.leaveRooms(userId, CollUtil.join(roomIds, ","));
            return YxUtil.createSuccessData(data);
        } else {
            return YxUtil.createFail(secretPwdErrorReturn);
        }

    }

    /**
     * 查找用户设置接口
     */
    @ApiOperation("查找用户设置接口")
    @RequestMapping("/findUserSetting")
    @ResponseBody
    public ResponseData findUserSetting(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        JSONObject data = new JSONObject();
        data.put("isAutoCancel", imUser.getIsAutoCancel());
        data.put("searchUserId", imUser.getSearchUserId());
        data.put("searchGroupId", imUser.getSearchGroupId());
        data.put("searchAccount", imUser.getSearchAccount());
        data.put("secretModePwd", StringUtils.isEmpty(imUser.getSecretModePwd()) ? 0 : 1);
        data.put("newNotification", imUser.getIsNewMessageNotification());
        data.put("groupNewNotification", imUser.getIsNewGroupMessageNotification());
        data.put("isShowInputStatus", imUser.getIsShowInputStatus());
        data.put("needAuth", imUser.getNeedAuth());
        data.put("isShowUserid", imUser.getIsShowUserid());
        data.put("isAutoAddGroup", imUser.getIsGroupedNeedAuth());
        return YxUtil.createSuccessData(data);

    }

    /**
     * 用户设置接口
     */
    @ApiOperation("用户其它设置设置接口")
    @RequestMapping("/userSetOther")
    @ResponseBody
    public ResponseData userSetOther(@RequestHeader String token,
                                     @ApiParam("1:好友新消息通知、2:输入状态显示、3:加好友时需要验证，4：进群邀请确认,5:用户id显示,6:群新消息通知") @RequestParam int setType,
                                     @ApiParam("设置状态:0:否，1：是") @RequestParam int setStatus) {
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        if (newInfoNotify == setType) {
            imUser.setIsNewMessageNotification(setStatus);
            imUser.setNewNotification(setStatus);
        }
        if (inputStatus == setType) {
            imUser.setIsShowInputStatus(setStatus);
        }

        if (friendNeedAuth == setType) {
            imUser.setNeedAuth(setStatus);
        }

        if (groupedNeedAuth == setType) {
            imUser.setIsGroupedNeedAuth(setStatus);
        }
        if (showUserid == setType) {
            imUser.setIsShowUserid(setStatus);
        }
        if(newGroupInfoNotify==setType){
            imUser.setIsNewGroupMessageNotification(setStatus);
        }
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }

    /**
     * 用户设置接口
     */
    @ApiOperation("新消息通知")
    @RequestMapping("/userSetNewInfoNotify")
    @ResponseBody
    public ResponseData userSetNewInfoNotify(@RequestHeader String token,
                                             @ApiParam("设置状态:0:否，1：是") @RequestParam int setStatus) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        imUser.setNewNotification(setStatus);
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }

    private ResponseData getResult(int count, ImUserWithBLOBs imUser, int devType, long userId) {
        if (count > 0) {
            //更新用户缓存
            cacheService.updateUserCache(imUser);
            return userSetResult(devType, userId);
        } else {
            return YxUtil.createFail(setErrorReturn);
        }
    }

    /**
     * 用户设置接口
     */
    @ApiOperation("输入状态显示")
    @RequestMapping("/userSetIsShowInputStatus")
    @ResponseBody
    public ResponseData userSetIsShowInputStatus(@RequestHeader String token,
                                                 @ApiParam("设置状态:0:否，1：是") @RequestParam int setStatus) {
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        imUser.setIsShowInputStatus(setStatus);
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }


    /**
     * 用户临时身份id设置
     */
    @ApiOperation("用户临时身份id设置")
    @RequestMapping("/userSetTemporaryUserId")
    @ResponseBody
    public ResponseData userSetTemporaryUserId(@RequestHeader String token,
                                               @ApiParam("有效时长：yyyy-mm-dd hh24:mi") @RequestParam String expiryTime,
                                               @ApiParam("总次数") @RequestParam int effectiveTimes) {
        if (CheckUtil.checkQRCodeTimes(effectiveTimes)) {
            return YxUtil.createFail(QRCodeCurrentTimesReturn);
        }
        if (CheckUtil.checkQRCodeTime(expiryTime)) {
            return YxUtil.createFail(QRCodeCurrentTimeReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        return generateTemporary(userId, expiryTime, effectiveTimes, tempTypeID);
    }

    /**
     * 用户临时二维码设置接口
     */
    @ApiOperation("用户临时二维码设置接口")
    @RequestMapping("/userSetTemporaryQcode")
    @ResponseBody
    public ResponseData userSetTemporaryQcode(@RequestHeader String token,
                                              @ApiParam("有效时长：yyyy-mm-dd hh:mi") @RequestParam String expiryTime,
                                              @ApiParam("总次数") @RequestParam int effectiveTimes) {
        if (CheckUtil.checkQRCodeTimes(effectiveTimes)) {
            return YxUtil.createFail(QRCodeCurrentTimesReturn);
        }
        if (CheckUtil.checkQRCodeTime(expiryTime)) {
            return YxUtil.createFail(QRCodeCurrentTimeReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        return generateTemporary(userId, expiryTime, effectiveTimes, tempTypeQcode);

    }

    /**
     * 生成临时身份
     *
     * @param userId
     * @param expiryTime
     * @param effectiveTimes
     * @param tempType
     * @return
     */
    private ResponseData generateTemporary(long userId, String expiryTime, int effectiveTimes, Integer tempType) {
        ImTempIdentity imTempIdentity = tempIdentityService.selectTempIdentityByUserId(userId, identityTypeSingle, tempType);
        if (effectiveTimes <= 0) {
            return YxUtil.createFail(tempIdEffectiveTimeZeroReturn);
        }
        if (imTempIdentity == null) {
            String lockKey = LockKey.userTempKey + userId + "_" + identityTypeSingle + "_" + tempType;
            imTempIdentity = new ImTempIdentity(identityTypeSingle, userId, sequenceService.getTempId(lockKey), tempType, DateUtil.getDate(expiryTime, "yyyy-MM-dd HH:mm"), effectiveTimes, 0);
            tempIdentityService.insert(imTempIdentity);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        } else {
            updateUserTempIdentity(imTempIdentity, expiryTime, effectiveTimes);
            return YxUtil.createSimpleSuccess(setSuccessReturn);
        }
    }

    private void updateUserTempIdentity(ImTempIdentity imTempIdentity, String expiryTime, int effectiveTimes) {
        String lockKey = imTempIdentity.getIdentityId() + "_" + identityTypeSingle + "_" + imTempIdentity.getTempType();
        //删除tempId 缓存
        tempIdentityService.deleteByTempId(imTempIdentity.getTempId());
        imTempIdentity.setCurrentTimes(0);
        imTempIdentity.setEffectiveTimes(effectiveTimes);
        imTempIdentity.setExpiryTime(DateUtil.getDate(expiryTime, "yyyy-MM-dd HH:mm"));
        imTempIdentity.setTempId(sequenceService.getTempId(lockKey));
        tempIdentityService.updateByPrimaryKeySelective(imTempIdentity);
    }

    /**
     * 用户临时身份id获取接口
     */
    @ApiOperation("用户临时身份id获取接口")
    @RequestMapping("/findUserTemporaryId")
    @ResponseBody
    public ResponseData findUserTemporaryId(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImTempIdentity imTempIdentity = tempIdentityService.selectTempIdentityByUserId(userId, identityTypeSingle, tempTypeID);
        if (imTempIdentity == null) {
            return YxUtil.createFail(tempIdNoFoundReturn);
        }

        if (!CheckUtil.checkQRCodeCurrentTimes(imTempIdentity)) {
            return YxUtil.createFail(tempIdEndUseReturn);
        }

        if (!CheckUtil.checkQRCodeExpiryTime(imTempIdentity)) {
            return YxUtil.createFail(tempIdEndTimeReturn);
        }
        JSONObject json = new JSONObject();
        json.put("tempId", imTempIdentity.getTempId());
        return findUserTemporary(imTempIdentity, json);
    }

    /**
     * 用户临时二维码获取接口
     */
    @ApiOperation("用户临时二维码获取接口")
    @RequestMapping("/findUserTemporaryQcode")
    @ResponseBody
    public ResponseData findUserTemporaryQcode(@RequestHeader String token) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImTempIdentity imTempIdentity = tempIdentityService.selectTempIdentityByUserId(userId, identityTypeSingle, tempTypeQcode);
        if (imTempIdentity == null || imTempIdentity.getIdentityId() < 0) {
            return YxUtil.createFail(QRCodeManualExpiredReturn);
        }

        if (imTempIdentity.getCurrentTimes() >= imTempIdentity.getEffectiveTimes()) {
            return YxUtil.createFail(QRCodeEndUseReturn);
        }

        if (imTempIdentity.getExpiryTime().getTime() < System.currentTimeMillis()) {
            return YxUtil.createFail(QRCodeEndTimeReturn);
        }

        JSONObject json = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", QRCodePType);
            jsonObject.put("data", imTempIdentity.getTempId());
            String QRCode = QrCodeCreateUtil.createQrCode(JSON.toJSONString(jsonObject));
            json.put("qcode", QRCode);
            return findUserTemporary(imTempIdentity, json);
        } catch (WriterException | IOException e) {
            return YxUtil.createFail(getErrorReturn);
        }
    }

    private ResponseData findUserTemporary(ImTempIdentity imTempIdentity, JSONObject json) {
        json.put("expiryTime", DateUtil.getEnglishTimeString(imTempIdentity.getExpiryTime().getTime(), "yyyy-MM-dd HH:mm"));
        json.put("effectiveTimes", imTempIdentity.getEffectiveTimes());
        json.put("currentTimes", imTempIdentity.getCurrentTimes());
        json.put("residueTimes", imTempIdentity.getEffectiveTimes() - imTempIdentity.getCurrentTimes());
        return YxUtil.createSuccessData(json);
    }


    /**
     * 用户临时身份删除接口
     */
    @ApiOperation("用户临时身份删除接口")
    @RequestMapping("/deleteUserTemporary")
    @ResponseBody
    public ResponseData deleteUserTemporary(@RequestHeader String token,
                                            @ApiParam("临时身份类型，1：ID、2：二维码") @RequestParam int tempType) {
        long userId = CommonUtils.getUserIdByToken(token);
        ImTempIdentity imTempIdentity = tempIdentityService.selectTempIdentityByUserId(userId, identityTypeSingle, tempType);
        if (imTempIdentity == null) {
            return YxUtil.createFail(tempIdentityNoFoundReturn);
        }
        tempIdentityService.deleteTempIdentityByUserId(userId,identityTypeSingle,tempType);
        return YxUtil.createSimpleSuccess(operationSuccessReturn);
    }

    @ApiOperation("修改个人头像")
    @RequestMapping("/updateHead")
    @ResponseBody
    public ResponseData updateHead(
            @RequestHeader String token,
            @ApiParam("新头像的url地址") String headUrl) {
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs user = userService.selectByPrimaryKey(userId);
        if (user != null && StringUtils.isNotBlank(headUrl)) {
            user.setHeadUrl(headUrl);
            int res = userService.updateByPrimaryKeySelective(user);
            if (res > 0) {
                //更新用户缓存
                cacheService.updateUserCache(user);
                if (devType > 0) {
                    // 通知我的其它设备，设置已经修改
                    ImMessage msg = msgFactory.userSetUpdateNotice(userId, userId, devType);
                    msg.setDevType(devType);
                    chatServerFeign.notifyNew(JSONObject.toJSONString(msg), user.getId());
                }

                // 通知好友,我的资料已经修改
                List<ImFriend> friends = friendService.selectFriends(userId);
                for (ImFriend friend : friends) {
                    ImMessage msg = msgFactory.userModifyProfileNotice(userId, friend.getFriendId(), user);
                    chatServerFeign.notifyNew(JSONObject.toJSONString(msg), friend.getFriendId());
                }
            } else {
                return YxUtil.createFail(setErrorReturn);
            }


        }
        return YxUtil.createSimpleSuccess(setSuccessReturn);
    }

    @ApiOperation("修改个人信息")
    @RequestMapping("/updateProfile")
    @ResponseBody
    public ResponseData updateProfile(
            @RequestHeader String token,
            @ApiParam("昵称") @RequestParam(required = false) String nickName,
            @ApiParam("城市") @RequestParam(required = false) String city,
            @ApiParam("省份") @RequestParam(required = false) String province,
            @ApiParam("姓名") @RequestParam(required = false) String name,
            @ApiParam("签名") @RequestParam(required = false) String sign,
            @ApiParam("性别 女 男") @RequestParam(required = false) String sex,
            @ApiParam("地区") @RequestParam(required = false) String district) {
        if (!CheckUtil.checkNickName(nickName)) {
            return YxUtil.createFail(nickNameReturn);
        }
        if (!CheckUtil.checkSign(sign)) {
            return YxUtil.createFail(signReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs user = userService.updateProfile(userId, nickName, city, province, name, sign, sex, district, device);
        if (user != null) {
            JSONObject json = getUserData(user);
            return YxUtil.createSuccessData(json);
        }
        return YxUtil.createFail(operationErrorReturn);
    }

    private JSONObject getUserData(ImUserWithBLOBs user) {
        JSONObject json = new JSONObject();
        json.put("id", user.getId());
        json.put("headUrl", user.getHeadUrl());
        json.put("nickName", user.getNickName());
        json.put("sex", user.getSex());
        json.put("sign", user.getSign());
        json.put("city", user.getCity());
        json.put("province", user.getProvince());
        json.put("district", user.getDistrict());
        json.put("name", user.getName());
        return json;
    }


    @ApiOperation("用户消息通知提醒查询接口")
    @RequestMapping("/findMessageNotify")
    @ResponseBody
    public ResponseData findMessageNotify(
            @RequestHeader String token) {
        Long userId = CommonUtils.getUserIdByToken(token);
        ImUser user = userService.selectByPrimaryKey(userId);
        JSONObject json = new JSONObject();
        json.put("isVideoCallNotification", user.getIsVideoCallNotification());
        json.put("isNotificationShowDetail", user.getIsNotificationShowDetail());
        json.put("isVoiceReminder", user.getIsVoiceReminder());
        json.put("isVibrationReminder", user.getIsVibrationReminder());
        return YxUtil.createSuccessData(json);
    }

    @ApiOperation("用户消息通知提醒设置接口")
    @RequestMapping("/setMessageNotify")
    @ResponseBody
    public ResponseData setMessageNotify(
            @RequestHeader String token,
            @ApiParam("通知类型:1:语音和视频通话提醒、2:通知显示消息详情、3:声音提示、4:震动提示、") @RequestParam int notifyType,
            @ApiParam("设置开关:0：禁用、1：启用") @RequestParam int isOpen) {
        if (CheckUtil.checkNotifyType(notifyType)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        if (voiceAndVideoNotify == notifyType) {
            imUser.setIsVideoCallNotification(isOpen);
        }

        if (notifyShow == notifyType) {
            imUser.setIsNotificationShowDetail(isOpen);
        }

        if (voiceNotify == notifyType) {
            imUser.setIsVoiceReminder(isOpen);
        }

        if (vibrationNotify == notifyType) {
            imUser.setIsVibrationReminder(isOpen);
        }
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }

    @ApiOperation("用户会话显示查询接口")
    @RequestMapping("/findSessionShow")
    @ResponseBody
    public ResponseData findSessionShow(
            @RequestHeader String token) {
        Long userId = CommonUtils.getUserIdByToken(token);
        ImUser user = userService.selectByPrimaryKey(userId);
        JSONObject json = new JSONObject();
        json.put("isShowSessionMessage", user.getIsShowSessionMessage());
        return YxUtil.createSuccessData(json);
    }


    @ApiOperation("用户会话显示设置接口")
    @RequestMapping("/setSessionShow")
    @ResponseBody
    public ResponseData setSessionShow(
            @RequestHeader String token,
            @ApiParam("设置开关:0：禁用、1：启用") @RequestParam int isOpen) {
        Long userId = CommonUtils.getUserIdByToken(token);
        Integer devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        imUser.setIsShowSessionMessage(isOpen);
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }


    @ApiOperation("聊天记录加密查询接口（全局）")
    @RequestMapping("/finGlobalSecret")
    @ResponseBody
    public ResponseData finGlobalSecret(
            @RequestHeader String token) {
        Long userId = CommonUtils.getUserIdByToken(token);
        ImUser user = userService.selectByPrimaryKey(userId);
        JSONObject json = new JSONObject();
        json.put("isSecretHistory", user.getIsSecretHistory());
        json.put("isSecretGroupHistory", user.getIsSecretGroupHistory());
        return YxUtil.createSuccessData(json);
    }

    @ApiOperation("聊天记录加密设置开启接口（全局）")
    @RequestMapping("/openGlobalSecret")
    @ResponseBody
    public ResponseData openGlobalSecret(
            @RequestHeader String token,
            @ApiParam("通知类型:1.好友聊天记录加密设置、2.群聊聊天记录加密设置") @RequestParam int secretType) {
        if (CheckUtil.checkSecretType(secretType)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        if (secretFriendSet == secretType) {
            imUser.setIsSecretHistory(secretFriendSetOpen);
        }
        if (secretgroupSet == secretType) {
            imUser.setIsSecretGroupHistory(secretgroupSetOpen);
        }
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }

    @ApiOperation("聊天记录加密设置接口关闭接口（全局）")
    @RequestMapping("/closeGlobalSecret")
    @ResponseBody
    public ResponseData closeGlobalSecret(
            @RequestHeader String token,
            @ApiParam("通知类型:1.好友聊天记录加密设置、2.群聊聊天记录加密设置") @RequestParam int secretType,
            @ApiParam("隐私密码") @RequestParam String secretModePwd) {
        if (CheckUtil.checkSecretType(secretType)) {
            return YxUtil.createFail(paramErrorReturn);
        }
        long userId = CommonUtils.getUserIdByToken(token);
        int devType = CommonUtils.getDevTypeByToken(token);
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        //解密隐私密码
        secretModePwd = RSACipherUtil.decrypt(secretModePwd);
        if (CheckUtil.checkSecretPwd(imUser, secretModePwd)) {
            return YxUtil.createFail(secretPwdErrorReturn);
        }

        if (secretFriendSet == secretType) {
            imUser.setIsSecretHistory(secretFriendSetClose);
        }
        if (secretgroupSet == secretType) {
            imUser.setIsSecretGroupHistory(secretgroupSetClose);
        }
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, devType, userId);
    }


    @ApiOperation("获取socket的链接地址")
    @RequestMapping("/finSocketUrl")
    @ResponseBody
    public ResponseData finSocketUrl(
            @RequestHeader String token) {
        return YxUtil.createSuccessDataNoFilter(loginService.getSocketUrl());
    }

    @ApiOperation("隐私模式私密好友群聊标记")
    @RequestMapping("/markSecretFriendAndGroup")
    @ResponseBody
    public ResponseData markSecretFriendAndGroup(@RequestHeader String token,
                                                 @ApiParam("0关闭1开启") @RequestParam Integer turn) {
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        if (turn == null || !(turn.equals(setOn) || turn.equals(setOff))) {
            return YxUtil.createFail(paramErrorReturn);
        }
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        imUser.setIsMarkSecretFriends(turn);
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, device, userId);
    }

    @ApiOperation("隐私模式状态栏变化")
    @RequestMapping("/markSecretMode")
    @ResponseBody
    public ResponseData markSecretMode(@RequestHeader String token,
                                       @ApiParam("0关闭1开启") @RequestParam Integer turn) {
        long userId = CommonUtils.getUserIdByToken(token);
        int device = CommonUtils.getDevTypeByToken(token);
        if (turn == null || !(turn.equals(setOn) || turn.equals(setOff))) {
            return YxUtil.createFail(paramErrorReturn);
        }
        ImUserWithBLOBs imUser = userService.selectByPrimaryKey(userId);
        imUser.setIsMarkSecretMode(turn);
        int count = userService.updateByPrimaryKeySelective(imUser);
        return getResult(count, imUser, device, userId);
    }

}
