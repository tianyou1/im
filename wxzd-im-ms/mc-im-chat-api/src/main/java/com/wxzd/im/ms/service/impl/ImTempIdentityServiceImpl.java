package com.wxzd.im.ms.service.impl;

import com.wxzd.im.ms.entity.ImTempIdentity;
import com.wxzd.im.ms.mapper.ImTempIdentityMapper;
import com.wxzd.im.ms.service.ImTempIdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import static com.wxzd.im.ms.parameter.CacheConstants.*;

@Service
public class ImTempIdentityServiceImpl implements ImTempIdentityService {
    @Autowired
    private ImTempIdentityMapper tempIdentityMapper;

    @Override
    @Cacheable(cacheNames = identityByUserIdCache, key = "#userId+':'+#identityType+':'+#tempType", unless = "#result == null")
    public ImTempIdentity selectTempIdentityByUserId(Long userId, Integer identityType, Integer tempType) {
        return tempIdentityMapper.selectTempIdentityByUserId(userId, identityType, tempType);
    }


    @Override
    @CacheEvict(value = identityByUserIdCache, key = "#userId+':'+#identityType+':'+#tempType")
    public int deleteTempIdentityByUserId(Long userId, Integer identityType, Integer tempType) {
        return tempIdentityMapper.deleteTempIdentityByUserId(userId, identityType, tempType);
    }

    @Override
    public int insert(ImTempIdentity imTempIdentity) {
        return tempIdentityMapper.insert(imTempIdentity);
    }

    @Override
    @Caching(put = {
            @CachePut(value = identityByIdentityIdCache, key = "#imTempIdentity.identityId"),
            @CachePut(value = identityByTempIdCache, key = "#imTempIdentity.tempId"),
            @CachePut(value = identityByUserIdCache, key = "#imTempIdentity.identityId+':'+#imTempIdentity.identityType+':'+#imTempIdentity.tempType"),
    })
    public ImTempIdentity updateByPrimaryKeySelective(ImTempIdentity imTempIdentity) {
        tempIdentityMapper.updateByPrimaryKeySelective(imTempIdentity);
        return imTempIdentity;
    }

    @Override
    public int insertSelective(ImTempIdentity record) {
        return this.tempIdentityMapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return tempIdentityMapper.deleteByPrimaryKey(id);
    }

    @Override
    @Cacheable(cacheNames = identityByTempIdCache, key = "#tempId", unless = "#result == null")
    public ImTempIdentity selectByTempId(Long tempId) {
        return this.tempIdentityMapper.selectByTempId(tempId);
    }


    @Override
    @Cacheable(cacheNames = identityByIdentityIdCache, key = "#identityId", unless = "#result == null")
    public ImTempIdentity selectByIdentityId(Long identityId) {
        return this.tempIdentityMapper.selectByIdentityId(identityId);
    }

    @Override
    @CacheEvict(value = identityByIdentityIdCache, key = "#identityId")
    public int deleteByIdentityId(Long identityId) {
        return this.tempIdentityMapper.deleteByIdentityId(identityId);
    }

    @Override
    @CacheEvict(value = identityByTempIdCache, key = "#tempId")
    public void deleteByTempId(Long tempId) {
    }
}
