package com.wxzd.im.ms.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.ImMessage;
import com.wxzd.im.ms.entity.ImTop;
import com.wxzd.im.ms.mapper.ImTopMapper;
import com.wxzd.im.ms.parameter.IMConstants;
import com.wxzd.im.ms.service.ImTopService;
import com.wxzd.im.ms.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ImTopServiceImpl implements ImTopService {
    @Autowired
    private ImTopMapper imTopMapper;

    @Autowired
    private ChatServerFeign chatServerFeign;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imTopMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImTop record) {
        return this.imTopMapper.insert(record);
    }

    @Override
    public int insertSelective(ImTop record) {
        return this.imTopMapper.insertSelective(record);
    }

    @Override
    public ImTop selectByPrimaryKey(Long id) {
        return this.imTopMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImTop record) {
        return this.imTopMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ImTop record) {
        return this.imTopMapper.updateByPrimaryKey(record);
    }

    @Override
    public int deleteByCondition(ImTop record) {
        return this.imTopMapper.deleteByCondition(record);
    }

    @Override
    public List<Map<String, Object>> getTopList(Long userId) {
        return this.imTopMapper.getTopList(userId);
    }

    @Override
    public void isTop(Long userId, Long destId, Integer destType) {
        deleteByCondition(new ImTop()
                .setUserId(userId)
                .setDestId(destId)
                .setDestType(destType));
        ImTop top = new ImTop();
        top.setDestId(destId);
        top.setDestType(destType);
        top.setUserId(userId);
        this.insert(top);

        ImMessage msg = new ImMessage();
        msg.setContent("会话窗口设置置顶");
        msg.setFromType(IMConstants.MSG_FROM_SYS);
        msg.setMessageType(IMConstants.MSG_IS_TOP);
        msg.setMsgId(Utils.getUUID());
        msg.setSendTime(System.currentTimeMillis());
        msg.setDestId(destId);
        msg.setFromId(userId);
        chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
    }

    @Override
    public void cancelTop(Long userId, Long destId, Integer destType) {
        deleteByCondition(new ImTop()
                .setUserId(userId)
                .setDestId(destId)
                .setDestType(destType));
        ImMessage msg = new ImMessage();
        msg.setContent("会话窗口删除置顶");
        msg.setFromType(IMConstants.MSG_FROM_SYS);
        msg.setMessageType(IMConstants.MSG_CANCEL_TOP);
        msg.setMsgId(Utils.getUUID());
        msg.setSendTime(System.currentTimeMillis());
        msg.setDestId(destId);
        msg.setFromId(userId);
        chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
    }
}
