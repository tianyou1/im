package com.wxzd.im.ms.entity;

public class ImUserReportsWithBLOBs extends ImUserReports {
    private String reportedReasion;

    private String reportHandleMark;

    public String getReportedReasion() {
        return reportedReasion;
    }

    public void setReportedReasion(String reportedReasion) {
        this.reportedReasion = reportedReasion == null ? null : reportedReasion.trim();
    }

    public String getReportHandleMark() {
        return reportHandleMark;
    }

    public void setReportHandleMark(String reportHandleMark) {
        this.reportHandleMark = reportHandleMark == null ? null : reportHandleMark.trim();
    }
}