package com.wxzd.im.ms.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.api.feignclient.service.ChatServerFeign;
import com.wxzd.im.ms.entity.*;
import com.wxzd.im.ms.enums.TokenEnum;
import com.wxzd.im.ms.mapper.ImUserMapper;
import com.wxzd.im.ms.parameter.ResponseData;
import com.wxzd.im.ms.service.*;
import com.wxzd.im.ms.utils.*;
import org.apache.commons.lang.StringUtils;
import org.nutz.plugins.ip2region.DbSearcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wxzd.im.ms.parameter.CacheConstants.*;
import static com.wxzd.im.ms.parameter.StaticConstant.*;

@Service
public class ImUserServiceImpl implements ImUserService {

    @Autowired
    private ImUserMapper imUserMapper;
    @Autowired
    private MessageFactory msgFactory;

    @Autowired
    private ImFriendService imFriendService;

    @Autowired
    private ImUserSecondaryVerificationService imUserSecondaryVerificationService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private ChatServerFeign chatServerFeign;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CacheService cacheService;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imUserMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImUserWithBLOBs record) {
        return this.imUserMapper.insert(record);
    }

    @Override
    public int insertSelective(ImUserWithBLOBs record) {
        return this.imUserMapper.insertSelective(record);
    }

    @Override
    @Cacheable(cacheNames = userCache, key = "#id", unless = "#result == null")
    public ImUserWithBLOBs selectByPrimaryKey(Long id) {
        return this.imUserMapper.selectByPrimaryKey(id);
    }

    @Override
    @Cacheable(cacheNames = accountCache, key = "#account", unless = "#result==null")
    public ImUserWithBLOBs selectByAccount(String account) {
        return imUserMapper.selectByAccount(account);
    }

    @Override
    @Cacheable(cacheNames = countAccountCache, key = "#account", unless = "#result==null or #result==0")
    public int countByAccount(String account) {
        return imUserMapper.countByAccount(account);
    }

    @Override
    public List<Map<String, Object>> selectAllUser() {
        return imUserMapper.selectAllUser();
    }

    @Override
    public int updateByPrimaryKeySelective(ImUserWithBLOBs record) {
        return this.imUserMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    @Caching(
            put = {
                    @CachePut(value = userCache, key = "#record.id"),
                    @CachePut(value = accountCache, key = "#record.account"),
            },
            evict = {
                    @CacheEvict(value = userCacheMap, key = "#record.id"),
            }
    )
    public ImUserWithBLOBs updateUserCache(ImUserWithBLOBs record) {
        //删除好友明细缓存
        imFriendService.deleteDetailCacheFriendId(record.getId());
        return record;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ImUserWithBLOBs record) {
        return this.imUserMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public int updateByPrimaryKey(ImUser record) {
        return this.imUserMapper.updateByPrimaryKey(record);
    }

    public ImUserWithBLOBs updateProfile(Long userId, String nickName, String city, String province, String name, String sign,
                                         String sex, String district, int devType) {
        ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
        if (user != null) {
            if (!StringUtils.isBlank(nickName)) {
                user.setNickName(nickName);
            }
            if (!StringUtils.isBlank(district)) {
                user.setDistrict(district);
            }
            if (!StringUtils.isBlank(city)) {
                user.setCity(city);
            }
            if (!StringUtils.isBlank(province)) {
                user.setProvince(province);
            }
            if (!StringUtils.isBlank(name)) {
                user.setName(name);
            }
            if (!StringUtils.isBlank(sex)) {
                user.setSex(sex);
            }
            if (!StringUtils.isBlank(sign)) {
                user.setSign(sign);
            }
            int res = updateByPrimaryKeySelective(user);
            if (res > 0) {
                //更新用户缓存
                cacheService.updateUserCache(user);
                // user.setPwd("");

                if (devType > 0) {
                    // 通知 我的其它设备，个人资料已经修改
                    ImMessage msg = msgFactory.userSetUpdateNotice(userId, userId, devType);
                    msg.setDevType(devType);
                    chatServerFeign.notifyNew(JSONObject.toJSONString(msg), user.getId());
                }

                // 通知好友,我的资料已经修改
                List<ImFriend> friends = imFriendService.selectFriends(user.getId());
                for (ImFriend friend : friends) {
                    ImMessage msg = msgFactory.userModifyProfileNotice(user.getId(), friend.getFriendId(), user);
                    chatServerFeign.notifyNew(JSONObject.toJSONString(msg), friend.getFriendId());
                }
            } else {
                user = null;
            }
        }

        return user;
    }


    @Override
    //当信息变更时无法处理缓存
    //@Cacheable(cacheNames = managerListMapCache,key ="#map.get('groupId')",unless = "#result == null or #result.size()==0")
    public List<Map<String, Object>> queryGroupManagers(Map<String, Object> map) {
        return this.imUserMapper.queryGroupManagers(map);
    }

    @Override
    //当信息变更时无法处理缓存
    //@Cacheable(cacheNames = memberListMapCache,key ="#map.get('groupId')",unless = "#result == null or #result.size()==0")
    public List<Map<String, Object>> queryGroupMembers(Map<String, Object> map) {
        return this.imUserMapper.queryGroupMembers(map);
    }

    /**
     * 群成员拉黑
     *
     * @param userId
     * @param destId
     * @return
     */
    @Override
    public int memberBlackSet(long userId, long destId) {
        ImFriend friend = new ImFriend();
        friend.setCreaterId(userId);
        friend.setCreateTime(System.currentTimeMillis());
        friend.setFriendId(destId);
        friend.setIsBlack(setOn);//黑名单
        friend.setIsFriend(isFriendStranger);//陌生人
        friend.setUserId(userId);
        commonService.deleteFriend(destId, userId);
        int res = imFriendService.insertSelective(friend);
        if (res > 0) {
            //更新 缓存
            cacheService.friendNumChange(friend);
        }
        return res;
    }

    @Override
    public ResponseData doLoginMy(String account, String pwd, Integer device) {
        HashMap<String, String> data = new HashMap<>();

        if (StringUtils.isBlank(account)) {
            return YxUtil.createFail("请输入账号");
        }

        if (StringUtils.isBlank(pwd)) {
            return YxUtil.createFail("请输入密码");
        }

        if (device == null) {
            return YxUtil.createFail("设备类型错误");
        }

        ImUserWithBLOBs user = cacheService.selectByAccount(account);

        if (user == null) {
            return YxUtil.createFail("用户不存在");
        }

        if (loseUser.equals(user.getStatus())) {
            return YxUtil.createFail("用户已失效");
        }

        if (freezeUser.equals(user.getStatus())) {
            return YxUtil.createFail("用户已被冻结");
        }
        //解密密码
        pwd = RSACipherUtil.decrypt(pwd);
        String dbPwd = user.getPwd();
        if (!Utils.encrypt(pwd).equals(dbPwd)) {
            data.put("info", "账号或密码不正确，请重新输入。");
            return YxUtil.getResponse(3, data);
        }
        long userId = user.getId();
        //获取当前账号是否设置二级验证
        ImUserSecondaryVerification verification = imUserSecondaryVerificationService.selectByUserId(userId);
        //返回是否设置二级验证
        int isVerification;
        if (verification == null) {
            isVerification = isSecondaryNo;//没有设置二级验证
        } else {
            isVerification = verification.getIsSecondaryVerification();//是否启动二次验证，0：否，1：是
        }
        data.put("isVerification", String.valueOf(isVerification));
        //发送通知上线
        //如果是没有设置二级验证
        if (isSecondaryNo.equals(isVerification)) {
            ImMessage msg = msgFactory.userOnlineNotice(userId, userId, device);
            chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
            //如果当前设备在线 发送下线通知
            if (TokenUtil.getUserToken(userId, device) != null) {
                ImMessage imMessage = msgFactory.userOfflineNotice(userId, userId, device);
                chatServerFeign.notifyNew(JSONObject.toJSONString(imMessage), userId);
            }
        }
        String result;
        String token;
        //如果没有设置则无需验证   如果有设置则需二次验证
        if (isSecondaryYes.equals(isVerification)) {
            result = String.valueOf(TokenEnum.tokenTemporary.getCode());
            //生成临时用于二次验证的token
            token = TokenUtil.generateUserTokenTemp(userId, device, result);
        } else {
            result = String.valueOf(TokenEnum.tokenValid.getCode());
            //生成系统token
            token = TokenUtil.generateUserToken(userId, device, result);
            //更新登录的设备
            commonService.setOnline(userId, device);
        }
        data.put("token", token);
        //更新最后登录时间
        user.setIsOnline(online);
        user.setLastLoginTime(new Date());
        int res = updateByPrimaryKeySelective(user);
        if (res > 0) {
            //更新用户缓存
            cacheService.updateUserCache(user);
        }
        return YxUtil.createSuccessData(data);
    }

    @Override
    public void handleIP(HttpServletRequest request, ImUserWithBLOBs user) {
        DbSearcher searcher = new DbSearcher();
        String ip = loginService.getClientIP(request);
        String region = searcher.getRegion(ip);
        user.setLocation(region);
        int count = updateByPrimaryKeySelective(user);
        if (count > 0) {
            //更新用户缓存
            cacheService.updateUserCache(user);
        }

    }

    @Override
    public ResponseData updatePwd(String token, String password) {
        try {
            password = Utils.encrypt(password);

            if (token != null && token.length() > 0) {
                if (token.indexOf("|") <= 0) {
                    return YxUtil.createFail("无权限");
                }
                String preToken = token.split("\\|")[0];

                if (Long.parseLong(Utils.decrypt(preToken)) > System.currentTimeMillis()) {
                    return YxUtil.createFail("无权限");
                }
                long userId = CommonUtils.getUserIdByToken(token);
                ImUserWithBLOBs user = cacheService.selectByPrimaryKeyUser(userId);
                user.setPwd(password);
                int count = updateByPrimaryKeySelective(user);
                if (count > 0) {
                    //更新用户缓存
                    cacheService.updateUserCache(user);
                    //修改密码成功推送一个离线的socket
                    int device = CommonUtils.getDevTypeByToken(token);
                    //如果当前设备在线 发送下线通知
                    if (TokenUtil.getUserToken(user.getId(), device) != null) {
                        ImMessage imMessage = msgFactory.userUpdatePwdNotice(user.getId(), user.getId(), device);
                        chatServerFeign.notifyNew(JSONObject.toJSONString(imMessage), user.getId());
                    }
                    //删除redis token
                    String onlineDevice = RedisUtil.hget("onlineDevice", user.getId().toString());
                    String[] strings = YxUtil.split(onlineDevice);
                    for (String s : strings) {
                        RedisUtil.hdel("userToken", userId + "_" + s);
                    }
                    //删除在线设备
                    RedisUtil.hdel("onlineDevice", String.valueOf(userId));

                    return YxUtil.createSuccessDataNoFilter("修改密码成功");
                } else {
                    return YxUtil.createFail("修改密码失败");
                }
            } else {
                return YxUtil.createFail("无权限");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return YxUtil.createFail("数据格式或解析错误");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delUser(long userId, long destId, int devType) {
        // 删除会话
        commonService.deleteFriendSession(userId, destId, singleType);
        // 删除好友
        commonService.deleteFriend(destId, userId);
        // 删除置顶
        commonService.deleteTop(userId, destId, singleType);
        if (devType > 0) {
            // 通知我的其它设备
            ImMessage msg = msgFactory.delFriendNotice(destId, userId, devType);
            chatServerFeign.notifyNew(JSONObject.toJSONString(msg), userId);
        }

    }

    @Override
    @Cacheable(cacheNames = friendDetailCache, key = "#userId+':'+#friendId", unless = "#result == null or #result.size()==0")
    public List<Map<String, Object>> friendDetail(long userId, long friendId) {
        return this.imUserMapper.friendDetail(userId, friendId);
    }

    @Override
    public ImUserWithBLOBs getMyInfo(long userId) {
        ImUserWithBLOBs myInfo = cacheService.selectByPrimaryKeyUser(userId);
        myInfo.setPwd("");
        myInfo.setChatLogPwd("");
        myInfo.setNewNotification(myInfo.getIsNewMessageNotification());
        if (myInfo.getSecretModePwd() != null && myInfo.getSecretModePwd().length() > 0) {
            myInfo.setSecretModePwd(haveSecretPwd);
        } else {
            myInfo.setSecretModePwd(withoutSecretPwd);
        }
        return myInfo;
    }

    @Override
    public Map<String, Object> getImFriendInfo(long userId, long friendId) {
        return imUserMapper.getImFriendInfo(userId, friendId);
    }
}
