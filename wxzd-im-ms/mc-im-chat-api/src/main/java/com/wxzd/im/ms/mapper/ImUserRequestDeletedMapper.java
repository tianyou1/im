package com.wxzd.im.ms.mapper;

import com.wxzd.im.ms.entity.ImUserRequestDeleted;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ImUserRequestDeletedMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserRequestDeleted record);

    int insertSelective(ImUserRequestDeleted record);

    ImUserRequestDeleted selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserRequestDeleted record);

    int updateByPrimaryKey(ImUserRequestDeleted record);

    ImUserRequestDeleted selectByCondition(ImUserRequestDeleted record);

    int batchDeleteRequest(@Param("list") List<Long> list);

    int batchInsertRequestDeleted(@Param("list") List<ImUserRequestDeleted> list);
}