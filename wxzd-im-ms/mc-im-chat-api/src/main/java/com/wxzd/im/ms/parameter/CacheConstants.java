package com.wxzd.im.ms.parameter;

public class CacheConstants {
    /*************************暂不处理*******************************/

    /************************ImGroupMember*************************/
    /**
     * 群成员list Map
     */
    public static final String memberListMapCache = "memberListMapCache";

    /**
     * 群管理员list map
     */
    public static final String managerListMapCache = "managerListMapCache";

    /************************ImGroup*************************/

    /**
     * ready groupInfo
     */
    public static final String groupsInfoCache = "groupsInfoCache";

    /************************ImFriend*************************/
    /**
     * 好友信息 PrimaryKey
     */
    public static final String friendCache = "friendCache";

    /**
     * 群好友查询
     */
    public static final String groupFriendQueryCache = "groupFriendQueryCache";

    /**
     * 好友列表 list map ready
     */
    public static final String friendInfoListCache = "friendInfoListCache";
    /**
     * 好友 黑名单list
     */
    public static final String friendBlackListCache = "friendBlackListCache";

    /************************ImUserRequestAudit*************************/

    /**
     * 群组邀请列表
     */
    public static final String inviteReqListCache = "inviteReqListCache";

    /**
     * 群组邀请列表 count
     */
    public static final String countInviteReqListCache = "countInviteReqListCache";

    /**
     * 进群确认列表
     */
    public static final String confirmReqListCache = "confirmReqListCache";

    /**
     * 进群确认列表 count
     */
    public static final String countConfirmReqListCache = "countConfirmReqListCache";

    /**
     * 是否发送过邀请
     */
    public static final String userRequestCache = "userRequestCache";

    /**
     * 好友请求列表
     */
    public static final String friendReqListCache = "friendReqListCache";

    /**
     * 进群确认列表 count
     */
    public static final String countFriendReqListCache = "countFriendReqListCache";

    /**
     * 好友请求by条件
     */
    public static final String reqByConditionCache = "reqByConditionCache";


    /***********************已处理 单表****************************/
    /***********************二次验证****************************/
    /**
     * 二次验证 by userId
     */
    public static final String secByUserIdCache = "secByUserIdCache";

    /************************ImUser*************************/

    /**
     * 用户信息 PrimaryKey
     */
    public static final String userCache = "userCache";

    /**
     * 查找是否存在账号
     */
    public static final String accountCache = "accountCache";

    /**
     * 查找是否存在账号 count
     */
    public static final String countAccountCache = "countAccountCache";

    /**
     * 用户信息（Map结果集保存）chatServer
     */
    public static final String userCacheMap = "userCacheMap";

    /************************ImTempIdentity*************************/

    /**
     * 临时身份信息 ByTempId
     */
    public static final String identityByTempIdCache = "identityByTempIdCache";

    /**
     * 临时身份信息 ByIdentityId
     */
    public static final String identityByIdentityIdCache = "identityByIdentityIdCache";

    /**
     * 临时身份信息 by userId
     */
    public static final String identityByUserIdCache = "identityByUserIdCache";

    /************************ImGroup*************************/

    /**
     * 群信息 PrimaryKey
     */
    public static final String groupCache = "groupCache";

    /**
     * 群信息（Map结果集保存）chatServer
     */
    public static final String groupCacheMap = "groupCacheMap";


    /************************ImGroupMember*************************/

    /**
     * 群成员信息 PrimaryKey
     */
    public static final String groupsMemberCache = "groupsMemberCache";

    /**
     * 群成员缓存
     */
    public static final String memberCache = "memberCache";

    /**
     * 群主缓存
     */
    public static final String ownerCache = "ownerCache";

    /**
     * 群成员数
     */
    public static final String countMemberCache = "countMemberCache";

    /**
     * 群管理员数
     */
    public static final String countManagerCache = "countManagerCache";

    /**
     * 群成员list
     */
    public static final String memberListCache = "memberListCache";

    /**
     * 群管理员和群主list
     */
    public static final String managerAndOwnerListCache = "managerAndOwnerListCache";

    /**
     * 群成员set userId
     */
    public static final String groupMemberSetCache = "groupMemberSetCache";

    /***
     * 所有群成员set id
     */
    public static final String allMemberSetCache = "allMemberSetCache";

    /***
     * 群管理员set id
     */
    public static final String managerSetCache = "managerSetCache";

    /***
     * 普通群成员set id
     */
    public static final String memberSetCache = "managerSetCache";

    /**
     * 群成员缓存（Map结果集保存）chatServer
     */
    public static final String memberCacheMap = "memberCacheMap";

    /**
     * 私密群组成员缓存（Map结果集保存）chatServer
     */
    public static final String secretMemberCacheMap = "secretMemberCacheMap";

    /**
     * 非私密群聊列表（Map结果集保存）chatServer
     */
    public static final String noSecretMemberListCacheMap = "noSecretMemberListCacheMap";

    /**
     * 全部群聊列表 （Map结果集保存）chatServer
     */
    public static final String allMemberListCacheMap = "allMemberListCacheMap";

    /**
     * 群成员列表缓存（Map结果集保存）
     */
    public static final String memberListCacheMap = "memberListCacheMap";

    /************************ImFriend*************************/

    /**
     * 好友信息 by ImFriend
     */
    public static final String friendByFriendCache = "friendByFriendCache";

    /**
     * 好友明细
     */
    public static final String friendDetailCache = "friendDetailCache";

    /**
     * 好友 set
     */
    public static final String friendSetCache = "friendSetCache";

    /**
     * 好友信息（Map结果集保存）chatServer
     */
    public static final String userFriendCacheMap = "userFriendCacheMap";

    /**
     * 私密好友信息（Map结果集保存）chatServer
     */
    public static final String userSecretFriendCacheMap = "userSecretFriendCacheMap";

    /**
     * 好友数量
     */
    public static final String countFriendCache = "countFriendCache";

}
