package com.wxzd.im.ms.entity.result;

import java.util.List;

public class PageDataResult {

	//总记录数量
	private Integer count;
	//当前页数据列表
	private List<?> data;
	//返回值
	private Integer code;
	//返回消息
	private String msg;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
