package com.wxzd.im.ms.entity.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class BaseParam  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 业务编号（所有接口的必填参数）
	 */
	@ApiModelProperty(value="业务编号",name="businessCode",required=true,example="TestBusinessCode")
	private String businessCode;

	public String getBusinessCode() {
		return businessCode;
	}

	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}

}
