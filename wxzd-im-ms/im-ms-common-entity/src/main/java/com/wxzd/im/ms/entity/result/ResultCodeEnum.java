package com.wxzd.im.ms.entity.result;
/**
 * 接口返回编码
 */
public enum ResultCodeEnum {
	
	SERVER_LIMIT(-4,"SERVER_LIMIT"),		//访问被限制
	SERVER_MISSING(-3,"SERVER_MISSING"),	//服务未找到
	SERVER_BAN(-2,"SERVER_BAN"),			//服务停止使用
	SERVER_ERROR(-1,"SERVER_ERROR"),		//服务错误
	
	SUCCESS(0,"SUCCESS"),				//请求成功
	
	AUTH_FAILED(1,"AUTH_FAILED"),			//授权认证失败
	AUTH_INVALID(2,"AUTH_INVALID"),			//授权已过期
	AUTH_LIMIT(3,"AUTH_LIMIT"),				//权限不足
	
	PARAM_ERROR(6,"PARAM_ERROR"),			//请求参数有误
	PARAM_MISSING(7,"PARAM_MISSING"),		//必填参数未填
	RESULT_OVER_LIMIT(8,"RESULT_OVER_LIMIT"),		//返回记录数超出上限
	
	
	USER_LOCKED(11,"USER_LOCKED"),					//用户已锁定
	USER_NOT_EXIST(12,"USER_NOT_EXIST"),			//用户不存在
	USER_PASSWORD_ERROR(13,"USER_PASSWORD_ERROR"),	//用户密码错误
	USER_LOGINS(14,"USER_LOGINS"),					//多用户登录
	
	FILE_CONFIG_ERROR_MSG(15,"FILE_CONFIG_ERROR_MSG"), //文件存放路径配置错误，请检查！
	FILE_EMPTY(16,"FILE_EMPTY"),  //上传的文件错误！
	FILE_UPLOAD_SUCCESS_MSG(17,"FILE_UPLOAD_SUCCESS_MSG"), //文件上传成功！
	FILE_NOT_EXIST_MSG(18,"FILE_NOT_EXIST_MSG"), //下载文件不存在
	FILE_DOWNLOAD_ERROR_MSG(19,"FILE_DOWNLOAD_ERROR_MSG"), //文件下载失败！
	;
	
	
	private Integer code;
	private String message;

	private ResultCodeEnum(Integer code,String message){
		this.code = code;
		this.message = message;
	}

	public Integer getCode(){
		return this.code;
	}

	public String getMessage(){
		return this.message;
	}
	
}
