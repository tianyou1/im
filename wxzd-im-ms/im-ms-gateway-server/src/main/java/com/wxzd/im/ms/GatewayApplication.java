package com.wxzd.im.ms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GatewayApplication {
	
	private static final Logger log = LoggerFactory.getLogger(GatewayApplication.class);

	public static  void main(String[] args) {
		log.info("网关服务开始启动");
		SpringApplication.run(GatewayApplication.class, args);
		log.info("网关服务启动成功");
	}
	
}

