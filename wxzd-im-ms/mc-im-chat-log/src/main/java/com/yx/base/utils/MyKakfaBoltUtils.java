package com.yx.base.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yx.im.model.MsgProtos;
import com.yx.im.parameter.Constants;
import com.yx.im.parameter.IMConstants;

public class MyKakfaBoltUtils {

    /**
     * 生成群成员的消息
     * @param data
     * @param messageJson
     * @return
     */
    public static MsgProtos.testBuf.Builder generateMsgProtos(MsgProtos.testBuf data,JSONObject messageJson){
        MsgProtos.testBuf.Builder builder = MsgProtos.testBuf.newBuilder();
        builder.setId(data.getId());
        builder.setDestId(data.getDestId());
        builder.setDevType(data.getDevType());
        builder.setFromId(data.getFromId());
        builder.setFromType(data.getFromType());
        builder.setGeoId(data.getGeoId());
        builder.setMessageType(data.getMessageType());
        builder.setReceiveTime(data.getReceiveTime());
        builder.setSendTime(data.getSendTime());
        builder.setStatus(data.getStatus());
        builder.setVersion(data.getVersion());
        builder.setContent(data.getContent());
        builder.setFromName(data.getFromName());
        builder.setImageIconUrl(data.getImageIconUrl());
        builder.setMsgId(data.getMsgId());
        builder.setExt(data.getExt());
        builder.setSystemType(data.getSystemType());
        if (messageJson != null) {
            if(messageJson.getString("anonymousData")!=null){
                builder.setFromId(0);
            }
        }
        return builder;
    }


    //判断JSONArray是否包含某个值
    public static boolean checkMemberArray(long memberId,MsgProtos.testBuf data){
        if(data.getSystemType()== Constants.groupSystemType&&data.getMessageType()== IMConstants.MSG_TYPE_JOIN_GROUP){
            JSONArray array=JSONObject.parseObject(data.getContent()).getJSONArray("memberArray");
            for (int i = 0; i < array.size(); i++) {
                if(array.getJSONObject(i).getLong("groupMemberId")==memberId){
                    return true;
                }
            }
        }
        return false;
    }


}
