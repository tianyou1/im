package com.yx.base.service.config;

public class MsgStoreAndNotify {


    //当接收方不在线时，取消了加群，退出群的消息保存到redis
    public static int[] needSaveType = {2, 3, 4, 8, 14, 16, 17, 18, 19, 20, 22, 24, 28, 29, 30, 34, 35, 42, 43, 44, 45, 46, 56, 83, 60, 85, 105, 106, 111, 112, 115, 116, 120, 229, 230,239};

    //删除的类型，去掉会话条数用
    public static int[] deleteType = {115,116,239};

}
