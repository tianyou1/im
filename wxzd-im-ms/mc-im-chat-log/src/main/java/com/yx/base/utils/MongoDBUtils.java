package com.yx.base.utils;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MongoDBUtils {
    private static volatile MongoDatabase mongoDatabase = null;
    private static String MONGO_ADDRESS = "";
    private static String MONGO_DBNAME = "";
    private static int CONNECT_PERHOST;
    private static String AUTH_NAME = "";
    private static String AUTH_PWD = "";
    private static volatile  MongoClient mongoClient = null;
    private static MongoCredential mongoCredential = null;
    private static MongoClientOptions mongoClientOptions = null;
    private static List<ServerAddress> serverAddresses = null;

    static {
        //最好是用下面的读取配置文件
        try {
            //读取配置文件
            Properties prop = new Properties();
            prop.load(MongoDBUtils.class.getClassLoader().getResourceAsStream(
                    "db/mongo.properties"));
            // 根据 key 获取 value
            MONGO_ADDRESS = prop.getProperty("mongo_address");
            MONGO_DBNAME = prop.getProperty("mongo_dbname");
            AUTH_NAME = prop.getProperty("auth_name");
            AUTH_PWD = prop.getProperty("auth_pwd");
            CONNECT_PERHOST = Integer.parseInt(prop.getProperty("connections_perhost"));

            // 连接认证
            mongoCredential = MongoCredential.createScramSha1Credential(
                    AUTH_NAME, MONGO_DBNAME,
                    AUTH_PWD.toCharArray());

            // 客户端配置（连接数，副本集群验证）
            MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
            builder.connectionsPerHost(CONNECT_PERHOST);
            mongoClientOptions = builder.build();
            // MongoDB地址列表
            serverAddresses = new ArrayList<>();
            for (String address : MONGO_ADDRESS.split(",")) {
                String[] hostAndPort = address.split(":");
                String host = hostAndPort[0];
                Integer port = Integer.parseInt(hostAndPort[1]);
                ServerAddress serverAddress = new ServerAddress(host, port);
                serverAddresses.add(serverAddress);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("读取mongo.properties出错");
        }

    }

    private static MongoClient getMongoClient() {
        //首先判断是否为空
        if(mongoClient==null) {
            //可能多个线程同时进入到这一步进行阻塞等待
            synchronized(MongoDBUtils.class) {
                //第一个线程拿到锁，判断不为空进入下一步
                if(mongoClient==null) {
                    /**
                     * 由于编译器的优化、JVM的优化、操作系统处理器的优化，可能会导致指令重排（happen-before规则下的指令重排，执行结果不变，指令顺序优化排列）
                     * new Singleton3()这条语句大致会有这三个步骤：
                     * 1.在堆中开辟对象所需空间，分配内存地址
                     * 2.根据类加载的初始化顺序进行初始化
                     * 3.将内存地址返回给栈中的引用变量
                     *
                     * 但是由于指令重排的出现，这三条指令执行顺序会被打乱，可能导致3的顺序和2调换
                     * 👇
                     */
                    mongoClient = new MongoClient(serverAddresses, mongoCredential, mongoClientOptions);
                    mongoDatabase = mongoClient.getDatabase(MONGO_DBNAME);
                }
            }
        }
        return mongoClient;
    }


    public static void insertMongodbDocument(String collectionName,
                                             Document newDocument) {
        getMongoClient();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName);
        mongoCollection.insertOne(newDocument);
    }

    /**
     * @param collectionName
     * @param documentList
     */
    public static void insertMongodbDocument(String collectionName,
                                             List<Document> documentList) {
        getMongoClient();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName);
        mongoCollection.insertMany(documentList);
    }

    /**
     * update an document
     *
     * @param collectionName
     * @param query           target document
     * @param updatedDocument
     * @return
     */
    public static void updateDocument(String collectionName, Bson query, Bson updatedDocument) {
        getMongoClient();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName);
        mongoCollection.updateMany(query, updatedDocument);
    }

    /**
     * delete an document
     *
     * @param collectionName
     * @param query
     * @return
     */
    public static void deleteMongodbDocument(String collectionName, Bson query) {
        getMongoClient();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName);
        mongoCollection.deleteMany(query);
    }

    /**
     * selectLast an document
     *
     * @param collectionName
     * @param query
     * @return
     */
    public static FindIterable<Document> selectLastDocument(String collectionName, Bson query) {
        getMongoClient();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName);
        BasicDBObject dbObject = new BasicDBObject();
        //mongodb中按sendTime字段倒序查询（-1是倒序，1是正序）
        dbObject.put("sendTime",-1);
        return mongoCollection.find(query).sort(dbObject).limit(2);
    }

    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
        Bson bson = Filters.eq("msgId", "4ead90b4-ca69-48ca-b5ff-f1ef22c9ee63");
        Document d = new Document("$set", new Document("msgId", "333"));
        MongoDBUtils.updateDocument("ImMessageHistory", bson, d);
        System.out.println("添加成功");
    }

}
