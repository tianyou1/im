package com.yx.base.utils;

import com.yx.im.model.ImUserSession;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public final class MyDbUtils {// 拒绝继承
    public static QueryRunner queryRunner = new QueryRunner();

    public static String URL = "";
    public static String USER="";
    public static String PASSWORD = "";

    //    拒绝new一个实例
    private MyDbUtils() {
    }

    static {
        try {
//            调用该类时既注册驱动
            String className = "com.mysql.jdbc.Driver";
            Class.forName(className);
            //读取配置文件
            Properties prop = new Properties();
            prop.load(MyDbUtils.class.getClassLoader().getResourceAsStream(
                    "db/mysql.properties"));
            // 根据 key 获取 value
            URL = prop.getProperty("url");
            USER = prop.getProperty("user");
            PASSWORD = prop.getProperty("password");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }



    public static List<String> executeQuerySql(String sql) {
        new BasicRowProcessor();
        List<String> result = new ArrayList<>();
        try {
            List<Object[]> requstList = queryRunner.query(getConnection(), sql,
                    new ArrayListHandler(new BasicRowProcessor()));
//                @Override
//                public <Object> List<Object> toBeanList(ResultSet rs,Class<Object> type) throws SQLException {
//                  return super.toBeanList(rs, type);
//                }
            for (Object[] objects : requstList) {
                result.add(objects[0].toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static ImUserSession query(String sql, Object... params) {
        ImUserSession imUserSession = null;
        try {
            imUserSession = queryRunner.query(getConnection(), sql, new BeanHandler<>(ImUserSession.class), params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return imUserSession;
    }

    public static void update(String sql, Object... params) {
        try {
            Connection connection = getConnection();
            queryRunner.update(connection, sql, params);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //    获取连接
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    /**
     * 测试
     *
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
//        Connection connection = getConnection();
//        //查找
//        ImUserSession imUserSession = MyDbUtils.queryRunner.query(connection, "select * from im_user_session where userId=? and destType=? and destId=?", new BeanHandler<>(ImUserSession.class), 2, 1, 1);
//        System.out.println(JSONObject.toJSONString(imUserSession));

        //添加
//        MyDbUtils.queryRunner.update(connection,
//                "replace into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
//                1, "21222", "12312412411231", "2",
//                "13124124123123", 0, System.currentTimeMillis(),"aa",
//                System.currentTimeMillis(), "123123124");
//        System.out.println(JSONObject.toJSONString(imUserSession));
//        System.out.println(imUserSession == null);
//        connection.close();



    }
}