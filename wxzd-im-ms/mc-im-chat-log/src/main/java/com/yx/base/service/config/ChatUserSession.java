package com.yx.base.service.config;

import com.yx.im.parameter.IMConstants;


public class ChatUserSession {

    public static String getSessionContent(String content, int messageType) {
        String msgContent;
        switch (messageType) {
            case IMConstants.MSG_TYPE_TEXT:
                msgContent = content;
                break;
            case IMConstants.MSG_TYPE_IMG:
                msgContent = "[图片]";
                break;
            case IMConstants.MSG_TYPE_FILE:
                msgContent = "[文件]";
                break;
            case IMConstants.MSG_TYPE_VOICE:
                msgContent = "[离线语音]";
                break;
            case IMConstants.MSG_TYPE_SEND_CARD:
                msgContent = "[发送名片]";
                break;
            case IMConstants.MSG_TYPE_SEND_LOCATION:
                msgContent = "[位置分享]";
                break;
            case IMConstants.MSG_TYPE_SEND_VIDEO:
                msgContent = "[小视频]";
                break;
            case IMConstants.MSG_TYPE_EMOJI_YUN:
                msgContent = "[表情]";
                break;
//            case IMConstants.MSG_TYPE_AT:
//                msgContent = "[有人@你]";
//                break;
            case IMConstants.MSG_TYPE_PROFILE:
                msgContent = "[名片]";
                break;
            case IMConstants.BATCH_TRANSMIT:
                msgContent = "[转发]";
                break;
            default:
                msgContent = content;
                break;
        }
        return msgContent;
    }

}
