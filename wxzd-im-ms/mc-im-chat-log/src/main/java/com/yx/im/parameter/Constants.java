package com.yx.im.parameter;

public class Constants {

	//默认值，正常处理
	public static final Integer defaultSystemType=0;
	//系统通知，群聊单人
	public static final Integer singleSystemType=1;
	//系统通知，群聊
	public static final Integer groupSystemType=2;
	//系统通知，自己
	public static final Integer systemTypeSend=3;
	//单聊单条
	public static final Integer p2PSingleSystemType=4;
	//阅后即焚消息开启
	public static final Integer isEphemeralChatYes=1;

}
