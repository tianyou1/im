package com.yx.chatlog;

import org.apache.commons.lang.StringUtils;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImMyKafkaTopologyApplication {

    private static final Logger log = LoggerFactory.getLogger(ImMyKafkaTopologyApplication.class);

    public static void main(String[] args) {
        String name = "";

        TopologyBuilder builder = new TopologyBuilder();

        // 设置5个线程接收数据
        builder.setSpout("MyKafkaSport", MyKafkaSport.getInstance().getKafkaSport(), 5);

        //设置15个线程处理数据
        builder.setBolt("MyKakfaBolt", new MyKakfaBolt(), 15)
                .shuffleGrouping("MyKafkaSport");
        builder.setBolt("MyKakfaBolt2", new MyKakfaBolt2(), 15)
                .shuffleGrouping("MyKafkaSport");

        Config config = new Config();
        if (StringUtils.isNotEmpty(name)) {
            // 关闭调试模式
            config.setDebug(false);
            // 集群提交模式
            config.setMaxSpoutPending(200);
            config.setMessageTimeoutSecs(300);
            try {
                StormSubmitter.submitTopology(name, config, builder.createTopology());
            } catch (AlreadyAliveException | InvalidTopologyException | AuthorizationException e) {
                e.printStackTrace();
                log.error("storm集群模式拓扑任务启动异常！");
            }
            log.info("storm集群模式拓扑任务启动成功！");
        } else {
            // 本地测试模式
            config.setDebug(true);
            // 设置3个进程
            config.setNumWorkers(3);
            config.setMaxSpoutPending(200);
            config.setMessageTimeoutSecs(300);
            try {
                // 本地模式提交拓扑
                LocalCluster cluster = new LocalCluster();
                cluster.submitTopology("ImMyKafkaTopology", config, builder.createTopology());
                //cluster.close();
            } catch (Exception e) {
                e.printStackTrace();
                log.error("storm本地模式拓扑任务启动异常！");
            } finally {

            }
            log.info("storm本地模式拓扑任务启动成功！");
        }
    }

}


