package com.yx.chatlog;

import com.alibaba.fastjson.JSONObject;
import com.yx.base.service.config.ChatUserSession;
import com.yx.base.service.config.MsgStoreAndNotify;
import com.yx.base.utils.MyDbUtils;
import com.yx.base.utils.MyKakfaBoltUtils;
import com.yx.im.model.ImMessageHistory;
import com.yx.im.model.MsgProtos;
import com.yx.im.parameter.Constants;
import com.yx.im.parameter.IMConstants;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 处理聊天消息
 */
public class MyKakfaBolt extends BaseBasicBolt {

//    private static final Logger log = LoggerFactory.getLogger(MyKakfaBolt.class);
    /**
     *
     */
    private static final long serialVersionUID = -191372668602639496L;


    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        String tupleStr = tuple.getString(4);
//        log.info("接收到消息：" + tupleStr);
        Connection connection = null;
        try {
            long time = System.currentTimeMillis();
//            //额外字段（阅后即焚字段）
            int isEphemeralChat = 0;
//            //解析谷歌序列化数据
            MsgProtos.testBuf data = MsgProtos.testBuf.parseFrom(tupleStr.getBytes(StandardCharsets.ISO_8859_1));
            if (data != null) {
                //如果为撤回的消息，不更新会话
                if(IMConstants.MSG_TYPE_REBACK==data.getMessageType()){
                    return;
                }

                connection = MyDbUtils.getConnection();

                //如果是标记未读的会话
                if (IMConstants.MSG_TYPE_UNREAD == data.getMessageType()) {
                    //如果会话不存在先创建会话记录
                    List<Map<String, Object>> record = MyDbUtils.queryRunner.query(connection, "select 1 from im_user_session where userId=? and destType=? and destId=?", new MapListHandler(), data.getFromId(), data.getFromType(), data.getDestId());
                    if(!record.isEmpty()){
                        String sql = "update im_user_session set unreadCount=1,fromName=?,lastTime=? where userId=? and destId=? and destType=?";
                        MyDbUtils.queryRunner.update(connection,
                                sql, data.getFromName(), System.currentTimeMillis(), data.getFromId(), data.getDestId(), data.getFromType());
                    }
                    return;
                }


//                JSONObject messageJson = JSONObject.parseObject(data.getExt());
//                MsgProtos.testBuf.Builder builder = MyKakfaBoltUtils.generateMsgProtos(data,messageJson);
//                if (messageJson != null) {
//                    if(messageJson.getInteger("isEphemeralChat")!=null){
//                        isEphemeralChat=messageJson.getInteger("isEphemeralChat");
//                    }
//                    if(messageJson.getInteger("ephemeralChatTime")!=null){
//                        ephemeralChatTime=messageJson.getInteger("ephemeralChatTime");
//                    }
//                }
//                MsgProtos.testBuf memberData = builder.build();
//                MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
//
                String content = ChatUserSession.getSessionContent(data.getContent(), data.getMessageType());

                //如果是撤回消息，删除聊天记录
//                if (data.getMessageType() == IMConstants.MSG_TYPE_REBACK) {
//                    JSONObject msgContent = JSON.parseObject(data.getContent());
//                    if (data.getFromType() == IMConstants.MSG_FROM_P2P) {
//                        MyDbUtils.queryRunner.update(connection, "delete from im_message_history where msgId=?", msgContent.get("msgid"));
//
//                    } else if (data.getFromType() == IMConstants.MSG_FROM_GROUP) {
//                        MyDbUtils.queryRunner.update(connection, "delete from im_message_history where msgId=?", msgContent.get("msgid"));
//                    }
//                    return;
//                }

                JSONObject messageJson = JSONObject.parseObject(data.getExt());

                if (messageJson != null) {
                    if(messageJson.getInteger("isEphemeralChat")!=null){
                        isEphemeralChat=messageJson.getInteger("isEphemeralChat");
                    }

                    if(Constants.isEphemeralChatYes==isEphemeralChat){
                        content="[阅后即焚消息]";
                    }
                }

                if (IMConstants.MSG_FROM_P2P == data.getFromType() || IMConstants.MSG_FROM_GROUP == data.getFromType()) {
                    //给发送者更新会话
                    if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                        MyDbUtils.queryRunner.update(connection,
                                "replace into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
                                data.getMessageType(), data.getFromName(), data.getFromId(), data.getFromType(),
                                data.getDestId(), 0, System.currentTimeMillis(), content,
                                System.currentTimeMillis(), data.getFromId());

                    }
                }

                if (IMConstants.MSG_FROM_P2P == data.getFromType()) {
                    // save to myself
//                    saveSingleMessage(connection,data,isEphemeralChat,ephemeralChatTime,time);

                    //表示用户读取了信息，更新记录
                    if (IMConstants.MSG_TYPE_READED == data.getMessageType()) {
                        //会话未读改为已读
                        MyDbUtils.queryRunner.update(connection, "update im_user_session set unreadCount=0 where userId=? and destId=? and destType=?", data.getFromId(), data.getDestId(), data.getFromType());

                        //批量更新已读消息数据(修改对方发过来的信息)
//                        MyDbUtils.queryRunner.update(connection, "update im_message_history set status=?,readTime=? where belongUserId=? and (fromId=? or destId=?) and status=? and fromType=?", ImMessageHistory.readStatus, System.currentTimeMillis(), data.getFromId(), data.getDestId(), data.getDestId(), ImMessageHistory.unReadStatus, data.getFromType());
                        return;
                    }

                    long toId = data.getDestId();

                    //为朋友保存消息记录
//                    if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
//                        MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
//                        MyDbUtils.queryRunner.update(connection,
//                                "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,imageIconUrl,destId,fromName,content,messageType,sendTime,is_ephemeral_chat,ephemeral_chat_time,status,receiveTime,readTime)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
//                                toId, data.getDevType(), data.getMsgId(), data.getFromId(), data.getFromType(), data.getImageIconUrl(), data.getDestId(), data.getFromName(), data.getContent(), data.getMessageType(), data.getSendTime(), isEphemeralChat, ephemeralChatTime, ImMessageHistory.unReadStatus, data.getReceiveTime(), time);
//                    }

                    //给接收者更新会话,更新会话
                    long destId = data.getFromId();

                    //如果会话不存在先创建会话记录
                    List<Map<String, Object>> record = MyDbUtils.queryRunner.query(connection, "select 1 from im_user_session where userId=? and destType=? and destId=?", new MapListHandler(), toId, data.getFromType(), destId);


                    if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                        if (record.isEmpty()) {
                            int unreadcount = 1;
                            //如果会话不存在先创建会话记录
                            MyDbUtils.queryRunner.update(connection,
                                    "insert into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
                                    data.getMessageType(), data.getFromName(), toId, data.getFromType(),
                                    destId, unreadcount, System.currentTimeMillis(), content, System.currentTimeMillis(), data.getFromId());
                        } else {
                            String sql = "update im_user_session set unreadCount=unreadCount+1,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?";
                            MyDbUtils.queryRunner.update(connection,
                                    sql, data.getMessageType(), data.getFromName(), content, System.currentTimeMillis(), toId, destId, data.getFromType());
                        }
                    }

                } else if (IMConstants.MSG_FROM_GROUP == data.getFromType()) {

                    //表示用户读取了信息，更新记录
                    if (IMConstants.MSG_TYPE_READED == data.getMessageType()) {
                        //会话未读改为已读
                        MyDbUtils.queryRunner.update(connection, "update im_user_session set unreadCount=0 where userId=? and destId=? and destType=?", data.getFromId(), data.getDestId(), data.getFromType());

                        //批量更新已读消息数据(修改对方发过来的信息)
                        MyDbUtils.queryRunner.update(connection, "update im_message_history set status=?,readTime=? where belongUserId=? and destId=? and status=? and fromType=?", ImMessageHistory.readStatus, System.currentTimeMillis(), data.getFromId(), data.getDestId(), ImMessageHistory.unReadStatus, data.getFromType());
                        return;
                    }

                    // 群聊天
//                    dealGroupMemberData(connection,data,memberData,isEphemeralChat,ephemeralChatTime,time,content);
                    dealGroupMemberData(connection, data, content);

                } else if (IMConstants.MSG_FROM_SYS == data.getFromType()) {
                    String lastContent = data.getContent();

                    if(IMConstants.MSG_TYPE_FRIEND_CHATHISTORY_DELETE==data.getMessageType()||IMConstants.MSG_TYPE_FRIEND_CHATHISTORY_BOTH_DELETE==data.getMessageType()||IMConstants.MSG_TYPE_GROUP_DELETE_HISTORY==data.getMessageType()){
                        lastContent="";
                    }

                    //系统消息群聊操作
                    if (Constants.groupSystemType == data.getSystemType()) {
                        //给发送者更新会话
                        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                            MyDbUtils.queryRunner.update(connection,
                                    "replace into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
                                    data.getMessageType(), data.getFromName(), data.getFromId(), IMConstants.MSG_FROM_GROUP,
                                    data.getDestId(), 0, System.currentTimeMillis(), lastContent,
                                    System.currentTimeMillis(), data.getFromId());
                        }
//                        dealGroupMemberData(connection,data,memberData,0,5,time,content);
                        dealSystemGroupMemberData(connection, data, lastContent);
                    }
                    //如果系统并且单发的时候
                    if (Constants.singleSystemType == data.getSystemType()) {
                        if (data.getGeoId() != 0) {
                            if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                                insertOrUpdateSession(connection, IMConstants.MSG_FROM_GROUP, data, lastContent);
                            }
                        }

                    }

                    if (Constants.p2PSingleSystemType == data.getSystemType()) {
                        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                            if (data.getGeoId() != 0) {
                                insertOrUpdateSession(connection, IMConstants.MSG_FROM_P2P, data, lastContent);
                            } else {
                                //如果会话不存在先创建会话记录
                                List<Map<String, Object>> record = MyDbUtils.queryRunner.query(connection, "select 1 from im_user_session where userId=? and destType=? and destId=?", new MapListHandler(), data.getFromId(), IMConstants.MSG_FROM_P2P, data.getDestId());
                                if (record.isEmpty()) {
                                    //如果会话不存在先创建会话记录
                                    MyDbUtils.queryRunner.update(connection,
                                            "insert into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
                                            data.getMessageType(), data.getFromName(), data.getFromId(), IMConstants.MSG_FROM_P2P,
                                            data.getDestId(), 0, System.currentTimeMillis(), lastContent,
                                            System.currentTimeMillis(), data.getFromId());
                                } else {

                                    if(ArrayUtils.contains(MsgStoreAndNotify.deleteType, data.getMessageType())){
                                        String sql2 = "update im_user_session set unreadCount=?,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?";
                                        MyDbUtils.queryRunner.update(connection,
                                                sql2,0, IMConstants.MSG_TYPE_TEXT, data.getFromName(), lastContent, System.currentTimeMillis(), data.getFromId(), data.getDestId(), IMConstants.MSG_FROM_P2P);
                                    }else {
                                        String sql = "update im_user_session set unreadCount=unreadCount,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?";
                                        MyDbUtils.queryRunner.update(connection,
                                                sql, data.getMessageType(), data.getFromName(), lastContent, System.currentTimeMillis(), data.getFromId(), data.getDestId(), IMConstants.MSG_FROM_P2P);
                                    }

                                }
                            }
                        }
                    }

                    //系统消息，单条操作
//                    if(data.getSystemType()== Constants.singleSystemType){
//                        //如果没有赋值，代表是fromId放的
//                        if(data.getGeoId()!=0){
//                            // save to myself
//                            if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
//                                MyDbUtils.queryRunner.update(connection,
//                                        "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,imageIconUrl,destId,fromName,content,messageType,sendTime,is_ephemeral_chat,ephemeral_chat_time,status,readTime)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
//                                        data.getGeoId(), data.getDevType(), data.getMsgId(), data.getFromId(), data.getFromType(), data.getImageIconUrl(),
//                                        data.getDestId(), data.getFromName(), data.getContent(), data.getMessageType(), data.getSendTime(), isEphemeralChat, ephemeralChatTime, ImMessageHistory.readStatus, time);
//                            }
//                        }else {
//                            saveSingleMessage(connection,data,isEphemeralChat,ephemeralChatTime,time);
//                        }
//
//                    }
//
//                    if(data.getSystemType()== Constants.defaultSystemType){
//                        saveSingleMessage(connection,data,isEphemeralChat,ephemeralChatTime,time);
//                    }
                }
            }

//            log.info("消息消费时间1：" + (System.currentTimeMillis()-time));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }

    private void insertOrUpdateSession(Connection connection, int fromType, MsgProtos.testBuf data, String content) throws SQLException {
        long destId = data.getDestId();
        if (data.getGeoId() == data.getDestId()) {
            destId = data.getFromId();
        }
        //如果会话不存在先创建会话记录
        List<Map<String, Object>> record = MyDbUtils.queryRunner.query(connection, "select 1 from im_user_session where userId=? and destType=? and destId=?", new MapListHandler(), data.getGeoId(), fromType, destId);
        if (record.isEmpty()) {
            //如果会话不存在先创建会话记录
            MyDbUtils.queryRunner.update(connection,
                    "insert into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
                    data.getMessageType(), data.getFromName(), data.getGeoId(), fromType,
                    destId, 0, System.currentTimeMillis(), content,
                    System.currentTimeMillis(), data.getFromId());
        } else {

            if(ArrayUtils.contains(MsgStoreAndNotify.deleteType, data.getMessageType())){
                String sql2 = "update im_user_session set unreadCount=?,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?";
                MyDbUtils.queryRunner.update(connection,
                        sql2,0, IMConstants.MSG_TYPE_TEXT, data.getFromName(), content, System.currentTimeMillis(), data.getGeoId(), destId, fromType);
            }else {
                String sql = "update im_user_session set unreadCount=unreadCount,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?";
                MyDbUtils.queryRunner.update(connection,
                        sql, data.getMessageType(), data.getFromName(), content, System.currentTimeMillis(), data.getGeoId(), destId, fromType);
            }



        }
    }


    //创建单聊消息
//    private void saveSingleMessage(Connection connection,MsgProtos.testBuf data,int isEphemeralChat,int ephemeralChatTime,long time) throws SQLException {
//        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
//            MyDbUtils.queryRunner.update(connection,
//                    "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,imageIconUrl,destId,fromName,content,messageType,sendTime,is_ephemeral_chat,ephemeral_chat_time,status,readTime)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
//                    data.getFromId(), data.getDevType(), data.getMsgId(), data.getFromId(), data.getFromType(), data.getImageIconUrl(),
//                    data.getDestId(), data.getFromName(), data.getContent(), data.getMessageType(), data.getSendTime(), isEphemeralChat, ephemeralChatTime, ImMessageHistory.readStatus, time);
//        }
//    }


//    private void dealGroupMemberData(Connection connection, MsgProtos.testBuf data, MsgProtos.testBuf memberData, int isEphemeralChat, int ephemeralChatTime, long time, String scontent) {
//        // 群聊天
//        List<Map<String, Object>> list;
////        int size;
////        int valueCount=0;
//        try {
//            list = MyDbUtils.queryRunner.query(connection, "select id,userId from im_group_member where groupId=? and isAccept=1 group by userId", new MapListHandler(), data.getDestId());
////            size=list.size();
//            if (!list.isEmpty()) {
////                if(data.getSystemType()== Constants.groupSystemType){
////                    JSONObject jsonObject=JSONObject.parseObject(data.getContent());
////                    if(jsonObject!=null){
////                        JSONArray array=jsonObject.getJSONArray("memberArray");
////                        if(array!=null){
////                            size=list.size()-array.size();
////                        }
////                    }
////                }
//
//                //过滤数据
////                Object[][] values = new Object[size][];
//                List<Map<String, Object>> updateSessionList = new ArrayList<>();
//
//                for (Map<String, Object> stringObjectMap : list) {
//                    long userId = Long.parseLong(stringObjectMap.get("userId").toString());
//                    long memberId = Long.parseLong(stringObjectMap.get("id").toString());
//                    if (data.getFromId() == userId) {
//                        //保存发送人的数据
////                        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, memberData.getMessageType())) {
////                            values[valueCount] = new Object[]{userId, memberData.getDevType(), memberData.getMsgId(), memberData.getFromId(), memberData.getFromType(), memberData.getImageIconUrl(), memberData.getDestId(), memberData.getFromName(), memberData.getContent(), memberData.getMessageType(), memberData.getSendTime(), isEphemeralChat, ephemeralChatTime, ImMessageHistory.readStatus, memberData.getReceiveTime(), time};
////                        }
////                        valueCount++;
//                        if (StringUtils.isNotEmpty(data.getContent())) {
//                            String content;
//                            if (!data.getContent().contains("msgString")) {
//                                JSONObject json = new JSONObject();
//                                json.put("msgString", data.getContent());
//                                content = json.toJSONString();
//                            } else {
//                                content = data.getContent();
//                            }
//                            //保存群内发送人数据，方便后台查看
//                            MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
//                            MyDbUtils.queryRunner.update(connection, "insert into im_message_history_less(devType,msgId,fromId,fromType,imageIconUrl,destId,fromName,content,messageType,sendTime,status,receiveTime)  values(?,?,?,?,?,?,?,?,?,?,?,?)",
//                                    data.getDevType(), data.getMsgId(), data.getFromId(), data.getFromType(), data.getImageIconUrl(),
//                                    data.getDestId(), data.getFromName(), content, data.getMessageType(),
//                                    data.getSendTime(), data.getStatus(), data.getReceiveTime());
//                        }
//                    } else {
//                        //判断刚邀请进群的人，不做保存进入群聊的数据
//                        if (MyKakfaBoltUtils.checkMemberArray(memberId, data)) {
//                            continue;
//                        }
//                        // save to member
////                        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, memberData.getMessageType())) {
////                            values[valueCount] = new Object[]{userId, memberData.getDevType(), memberData.getMsgId(), memberData.getFromId(), memberData.getFromType(), memberData.getImageIconUrl(), memberData.getDestId(), memberData.getFromName(), memberData.getContent(), memberData.getMessageType(), memberData.getSendTime(), isEphemeralChat, ephemeralChatTime, ImMessageHistory.unReadStatus, memberData.getReceiveTime(), time};
////                        }
////                        valueCount++;
//                        //update or insert userSession
//                        List<Map<String, Object>> record = MyDbUtils.queryRunner.query(connection, "select 1 from im_user_session where userId=? and destType=? and destId=?", new MapListHandler(), userId, IMConstants.MSG_FROM_GROUP, data.getDestId());
//
//                        if (record.isEmpty()) {
//                            MyDbUtils.queryRunner.update(connection,
//                                    "insert into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
//                                    data.getMessageType(), data.getFromName(), userId, IMConstants.MSG_FROM_GROUP,
//                                    data.getDestId(), 0, System.currentTimeMillis(), scontent, System.currentTimeMillis(), data.getFromId());
//                        } else {
//                            updateSessionList.add(stringObjectMap);
//                        }
//                    }
//                }
//
//
//                //批量添加会话数据
//                Object[][] sessionValues = new Object[updateSessionList.size()][];
//                for (int j = 0; j < updateSessionList.size(); j++) {
//                    long userId = Long.parseLong(updateSessionList.get(j).get("userId").toString());
//                    sessionValues[j] = new Object[]{data.getMessageType(), data.getFromName(), scontent,
//                            System.currentTimeMillis(), userId, data.getDestId(), data.getFromType()};
//                }
//
////                MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
//                //批量处理数据
////                MyDbUtils.queryRunner.batch(connection,
////                        "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,imageIconUrl,destId,fromName,content,messageType,sendTime,is_ephemeral_chat,ephemeral_chat_time,status,receiveTime,readTime)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
////                        values);
//
//                //批量更新会话
//                MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
//                MyDbUtils.queryRunner.batch(connection,
//                        "update im_user_session set unreadCount=unreadCount+1,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?",
//                        sessionValues);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    //群发
    private void dealGroupMemberData(Connection connection, MsgProtos.testBuf data, String scontent) {
        // 群聊天
        List<Map<String, Object>> list;
        try {
            list = MyDbUtils.queryRunner.query(connection, "select id,userId from im_group_member where groupId=? and isAccept=1 group by userId", new MapListHandler(), data.getDestId());
            if (!list.isEmpty()) {
                List<Map<String, Object>> updateSessionList = new ArrayList<>();
                for (Map<String, Object> stringObjectMap : list) {
                    long userId = Long.parseLong(stringObjectMap.get("userId").toString());
                    if (data.getFromId() == userId) {
                        //添加后台群数据
                        saveBackManagerGroupData(data,connection);
                    } else {
                        //更新其他成员会话
                        updateGroupMemberSession(connection,userId,data,scontent,stringObjectMap,updateSessionList);
                    }
                }
                //批量添加会话数据
                batchUpdateGroupMemberSession(connection,updateSessionList,data,scontent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //系统消息群发
    private void dealSystemGroupMemberData(Connection connection, MsgProtos.testBuf data, String scontent) {
        // 群聊天
        List<Map<String, Object>> list;
        try {
            list = MyDbUtils.queryRunner.query(connection, "select id,userId from im_group_member where groupId=? and isAccept=1 group by userId", new MapListHandler(), data.getDestId());
            if (!list.isEmpty()) {
                List<Map<String, Object>> updateSessionList = new ArrayList<>();
                for (Map<String, Object> stringObjectMap : list) {
                    long userId = Long.parseLong(stringObjectMap.get("userId").toString());
                    long memberId = Long.parseLong(stringObjectMap.get("id").toString());
                    if (data.getFromId() == userId) {
                        //添加后台群数据
                        saveBackManagerGroupData(data,connection);
                    } else {
                        //判断刚邀请进群的人，不做保存进入群聊的数据
                        if (MyKakfaBoltUtils.checkMemberArray(memberId, data)) {
                            continue;
                        }

                        //更新其他成员会话
                        updateGroupMemberSession(connection,userId,data,scontent,stringObjectMap,updateSessionList);
                    }
                }

                //批量添加会话数据
                batchUpdateGroupMemberSession(connection,updateSessionList,data,scontent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void saveBackManagerGroupData(MsgProtos.testBuf data,Connection connection) throws SQLException {
        if (StringUtils.isNotEmpty(data.getContent())) {
            String content;
            if (!data.getContent().contains("msgString")) {
                JSONObject json = new JSONObject();
                json.put("msgString", data.getContent());
                content = json.toJSONString();
            } else {
                content = data.getContent();
            }
            //保存群内发送人数据，方便后台查看
            MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
            MyDbUtils.queryRunner.update(connection, "insert into im_message_history_less(devType,msgId,fromId,fromType,imageIconUrl,destId,fromName,content,messageType,sendTime,status,receiveTime)  values(?,?,?,?,?,?,?,?,?,?,?,?)",
                    data.getDevType(), data.getMsgId(), data.getFromId(), data.getFromType(), data.getImageIconUrl(),
                    data.getDestId(), data.getFromName(), content, data.getMessageType(),
                    data.getSendTime(), data.getStatus(), data.getReceiveTime());
        }
    }

    private void updateGroupMemberSession(Connection connection,long userId,MsgProtos.testBuf data,String scontent,Map<String, Object> stringObjectMap,List<Map<String, Object>> updateSessionList) throws SQLException {
        List<Map<String, Object>> record = MyDbUtils.queryRunner.query(connection, "select 1 from im_user_session where userId=? and destType=? and destId=?", new MapListHandler(), userId, IMConstants.MSG_FROM_GROUP, data.getDestId());

        if (record.isEmpty()) {
            MyDbUtils.queryRunner.update(connection,
                    "insert into im_user_session(msgType,fromName,userId,destType,destId,unreadCount,createTime,lastContent,lastTime,fromId)  values(?,?,?,?,?,?,?,?,?,?)",
                    data.getMessageType(), data.getFromName(), userId, IMConstants.MSG_FROM_GROUP,
                    data.getDestId(), 0, System.currentTimeMillis(), scontent, System.currentTimeMillis(), data.getFromId());
        } else {
            updateSessionList.add(stringObjectMap);
        }
    }

    private void batchUpdateGroupMemberSession(Connection connection,List<Map<String, Object>> updateSessionList,MsgProtos.testBuf data,String scontent) throws SQLException {
        //批量添加会话数据
        Object[][] sessionValues = new Object[updateSessionList.size()][];
        for (int j = 0; j < updateSessionList.size(); j++) {
            long userId = Long.parseLong(updateSessionList.get(j).get("userId").toString());
            if(ArrayUtils.contains(MsgStoreAndNotify.deleteType, data.getMessageType())){
                sessionValues[j] = new Object[]{IMConstants.MSG_TYPE_TEXT, data.getFromName(), scontent,
                        System.currentTimeMillis(), userId, data.getDestId(), IMConstants.MSG_FROM_GROUP};
            }else {
                sessionValues[j] = new Object[]{data.getMessageType(), data.getFromName(), scontent,
                        System.currentTimeMillis(), userId, data.getDestId(), IMConstants.MSG_FROM_GROUP};
            }

        }

        //批量更新会话
        MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");

        if(ArrayUtils.contains(MsgStoreAndNotify.deleteType, data.getMessageType())){
            MyDbUtils.queryRunner.batch(connection,
                    "update im_user_session set unreadCount=0,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?",
                    sessionValues);
        }else {
            MyDbUtils.queryRunner.batch(connection,
                    "update im_user_session set unreadCount=unreadCount+1,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?",
                    sessionValues);
        }

    }
}
