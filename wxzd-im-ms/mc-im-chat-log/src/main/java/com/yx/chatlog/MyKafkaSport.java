package com.yx.chatlog;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;

import java.util.Properties;

public class MyKafkaSport {

	// 消息来源
	private volatile static MyKafkaSport instance;
	
	// kafkaspout消息来源
	private final KafkaSpout<String, String> kafkaspout;

	private MyKafkaSport() {
		// kafkaIp地址
		String kafkaIp =  MyKafkaSportConfig.KAFKA_IPS;
		KafkaSpoutConfig.Builder<String, String> kafkaBuilder = 
				KafkaSpoutConfig.builder(kafkaIp, MyKafkaSportConfig.KAFKA_TOPIC)
				.setProp(getKafkaProperties());
		// 创建kafkaspoutConfig
		KafkaSpoutConfig<String, String> build = kafkaBuilder.build();
		// 通过kafkaspoutConfig获得kafkaspout
		kafkaspout = new KafkaSpout<String, String>(build);
	}

	public static MyKafkaSport getInstance() {
		if (instance == null) {
			synchronized (MyKafkaSport.class) {
				if (instance == null) {
					instance = new MyKafkaSport();
				}
			}
		}
		return instance;
	}
	
	public KafkaSpout<String, String> getKafkaSport() {
		return kafkaspout;
	}
	
	/**
	 * 获取kafka配置
	 * @return
	 */
	public static Properties getKafkaProperties() {
		Properties props = new Properties();
		props.put(ConsumerConfig.GROUP_ID_CONFIG, MyKafkaSportConfig.KAFKA_GROUPID);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		return props;
	}

}
