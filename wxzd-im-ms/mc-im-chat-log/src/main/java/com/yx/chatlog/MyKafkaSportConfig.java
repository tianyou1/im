package com.yx.chatlog;

import java.io.IOException;
import java.util.Properties;

/**
 *  Kafka配置类
 */
public class MyKafkaSportConfig {

	//kafka体温填报主题
	public static String KAFKA_IPS;
	
	//kafka体温填报主题
	public static String KAFKA_TOPIC;
	
	//kafka体温填报消费组
	public static String KAFKA_GROUPID;
	
	static {
        //最好是用下面的读取配置文件
        try {
            //读取配置文件
            Properties prop = new Properties();
            prop.load(MyKafkaSportConfig.class.getClassLoader().getResourceAsStream(
                    "kafka.properties"));
            // 根据 key 获取 value
            KAFKA_IPS = prop.getProperty("kafka_ips");
            KAFKA_TOPIC = prop.getProperty("kafka_topic");
            KAFKA_GROUPID = prop.getProperty("kafka_groupid");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("读取kafka.properties出错");
        }
      }
	
}
