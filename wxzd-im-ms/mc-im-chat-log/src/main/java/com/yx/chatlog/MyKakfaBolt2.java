package com.yx.chatlog;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.yx.base.service.config.ChatUserSession;
import com.yx.base.service.config.MsgStoreAndNotify;
import com.yx.base.utils.MongoDBUtils;
import com.yx.base.utils.MyDbUtils;
import com.yx.base.utils.MyKakfaBoltUtils;
import com.yx.im.model.ImMessageHistory;
import com.yx.im.model.MsgProtos;
import com.yx.im.parameter.Constants;
import com.yx.im.parameter.IMConstants;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.lang.ArrayUtils;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Tuple;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.yx.im.model.ImMessageHistory.*;

/**
 * 处理mongdb消息
 */
public class MyKakfaBolt2 extends BaseBasicBolt {

//    private static final Logger log = LoggerFactory.getLogger(MyKakfaBolt2.class);
    /**
     *
     */
    private static final long serialVersionUID = -191372668602639496L;

    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        String tupleStr = tuple.getString(4);
//        log.info("接收到消息：" + tupleStr);
        Connection connection = null;
        try {


            long time = System.currentTimeMillis();
            //额外字段（阅后即焚字段）
            int isEphemeralChat = 0;
            int ephemeralChatTime = 0;
            connection = MyDbUtils.getConnection();
            MsgProtos.testBuf data = MsgProtos.testBuf.parseFrom(tupleStr.getBytes(StandardCharsets.ISO_8859_1));
            if (data != null) {
                //如果是标记已读消息，不做保存历史记录处理
                if (IMConstants.MSG_TYPE_UNREAD == data.getMessageType()) {
                    return;
                }

                JSONObject messageJson = JSONObject.parseObject(data.getExt());
                MsgProtos.testBuf.Builder builder = MyKakfaBoltUtils.generateMsgProtos(data, messageJson);
                if (messageJson != null) {
                    if (messageJson.getInteger("isEphemeralChat") != null) {
                        isEphemeralChat = messageJson.getInteger("isEphemeralChat");
                    }
                    if (messageJson.getInteger("ephemeralChatTime") != null) {
                        ephemeralChatTime = messageJson.getInteger("ephemeralChatTime");
                    }
                }
                MsgProtos.testBuf memberData = builder.build();

                //如果是撤回消息，删除聊天记录
                if (IMConstants.MSG_TYPE_REBACK == data.getMessageType()) {
                    JSONObject msgContent = JSON.parseObject(data.getContent());

                    List<Integer> statusList = Arrays.asList(readStatus, unReadStatus);
                    Bson messageBson = Filters.and(Filters.eq("belongUserId", data.getFromId()), Filters.eq("fromType", data.getFromType()), Filters.in("status", statusList), Filters.or(Filters.eq("fromId", data.getDestId()), Filters.eq("destId", data.getDestId())));
                    FindIterable<Document> lastDocument = MongoDBUtils.selectLastDocument("ImMessageHistory", messageBson);
                    Document document1 = lastDocument.limit(1).first();
                    Document document2 = lastDocument.skip(1).first();

                    if (document1 != null) {
                        if (document1.get("msgId").equals(msgContent.get("msgid"))) {
                            String sql = "update im_user_session set msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?";
                            String lastContent = "";
                            int messageType = IMConstants.MSG_TYPE_TEXT;
                            if (document2 != null) {
                                //查找消息的类型
                                messageType = document2.getInteger("messageType");
                                //根据类型和内容获取会话的内容
                                lastContent = ChatUserSession.getSessionContent(document2.getString("content"), messageType);
                            }

                            if (IMConstants.MSG_FROM_P2P == data.getFromType()) {
                                //如果撤回的是最后一条消息则修改两边会话
                                MyDbUtils.queryRunner.update(connection,
                                        sql, messageType, data.getFromName(), lastContent, System.currentTimeMillis(), data.getFromId(), data.getDestId(), data.getFromType());
                                MyDbUtils.queryRunner.update(connection,
                                        sql, messageType, data.getFromName(), lastContent, System.currentTimeMillis(), data.getDestId(), data.getFromId(), data.getFromType());
                            } else if (IMConstants.MSG_FROM_GROUP == data.getFromType()) {
                                List<Map<String, Object>> list = MyDbUtils.queryRunner.query(connection, "select userId from im_group_member where groupId=? and isAccept=1 group by userId", new MapListHandler(), data.getDestId());
                                Object[][] sessionValues = new Object[list.size()][];
                                for (int j = 0; j < list.size(); j++) {
                                    long userId = Long.parseLong(list.get(j).get("userId").toString());
                                    sessionValues[j] = new Object[]{messageType, data.getFromName(), lastContent,
                                            System.currentTimeMillis(), userId, data.getDestId(), data.getFromType()};
                                }
                                //批量更新会话
                                MyDbUtils.queryRunner.execute(connection, "set names utf8mb4");
                                MyDbUtils.queryRunner.batch(connection,
                                        "update im_user_session set unreadCount=unreadCount+1,msgType=?,fromName=?,lastContent=?,lastTime=? where userId=? and destId=? and destType=?",
                                        sessionValues);
                            }
                        }
                    }

                    //删除mongo中ImMessageHistory表对应的msgId
                    Bson bson = Filters.and(Filters.eq("msgId", msgContent.get("msgid")));
                    Document d = new Document("$set", new Document("status", rebackStatus));
                    MongoDBUtils.updateDocument("ImMessageHistory", bson, d);
                    return;
                }

                if (IMConstants.MSG_FROM_P2P == data.getFromType()) {

                    //表示用户读取了信息，更新记录
                    if (IMConstants.MSG_TYPE_READED == data.getMessageType()) {
                        //修改mongo中ImMessageHistory表的数据
                        Document d = new Document("$set", new Document("status", readStatus).append("readTime", System.currentTimeMillis()));
                        if (data.getContent().contains("msgIds")) {
                            JSONObject msgIdJson = JSONObject.parseObject(data.getContent());
                            if (msgIdJson != null) {
                                String[] msgIds = msgIdJson.getString("msgIds").split(",");
                                if (msgIds.length > 0) {
                                    for (String msgId : msgIds) {
                                        Bson bson = Filters.and(Filters.eq("msgId", msgId));
                                        MongoDBUtils.updateDocument("ImMessageHistory", bson, d);
                                    }
                                }
                            }
                        } else {
                            Bson bson = Filters.and(Filters.eq("belongUserId", data.getFromId()), Filters.eq("status", unReadStatus), Filters.eq("fromType", data.getFromType()), Filters.or(Filters.eq("fromId", data.getDestId()), Filters.eq("destId", data.getDestId())));
                            MongoDBUtils.updateDocument("ImMessageHistory", bson, d);
                        }
                        return;
                    }
                    //save to myself
                    if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                        //保存到mongodb
                        Document messageHistoryMong = createImMessageMongoData(data.getFromId(), unReadStatus, data.getFromType(), data, isEphemeralChat, ephemeralChatTime, time);
                        MongoDBUtils.insertMongodbDocument("ImMessageHistory", messageHistoryMong);
                    }


                    //为朋友保存消息记录
                    if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                        long toId = data.getDestId();
                        //保存到mongodb
                        Document messageHistoryMong = createImMessageMongoData(toId, unReadStatus, IMConstants.MSG_FROM_P2P, data, isEphemeralChat, ephemeralChatTime, time);

                        MongoDBUtils.insertMongodbDocument("ImMessageHistory", messageHistoryMong);
                    }
                } else if (IMConstants.MSG_FROM_GROUP == data.getFromType()) {

                    //表示用户读取了信息，更新记录
                    if (IMConstants.MSG_TYPE_READED == data.getMessageType()) {
                        //修改mongo中ImMessageHistory表的数据
                        Bson bson = Filters.and(Filters.eq("belongUserId", data.getFromId()), Filters.eq("status", unReadStatus), Filters.eq("fromType", data.getFromType()), Filters.eq("destId", data.getDestId()));
                        Document d = new Document("$set", new Document("status", readStatus).append("readTime", System.currentTimeMillis()));
                        MongoDBUtils.updateDocument("ImMessageHistory", bson, d);
                        return;
                    }

                    //save to myself
                    if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                        //保存到mongodb
                        Document messageHistoryMong = createImMessageMongoData(data.getFromId(), readStatus, data.getFromType(), data, isEphemeralChat, ephemeralChatTime, time);
                        MongoDBUtils.insertMongodbDocument("ImMessageHistory", messageHistoryMong);
                    }

                    dealGroupMemberData(connection, data, memberData, isEphemeralChat, ephemeralChatTime, time);
                } else if (IMConstants.MSG_FROM_SYS == data.getFromType()) {
                    if (Constants.groupSystemType == data.getSystemType()) {
                        //save to myself
                        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                            //保存到mongodb
                            Document messageHistoryMong = createImMessageMongoData(data.getFromId(), readStatus, IMConstants.MSG_FROM_GROUP, data, isEphemeralChat, ephemeralChatTime, time);
                            MongoDBUtils.insertMongodbDocument("ImMessageHistory", messageHistoryMong);
                        }
                        dealSystemGroupMemberData(connection, data, memberData, time);
                    }
                    //如果系统并且单发的时候
                    if (Constants.singleSystemType == data.getSystemType() || Constants.p2PSingleSystemType == data.getSystemType()) {
                        if (ArrayUtils.contains(MsgStoreAndNotify.needSaveType, data.getMessageType())) {
                            int fromType = 0;
                            if (Constants.singleSystemType == data.getSystemType()) {
                                fromType = IMConstants.MSG_FROM_GROUP;
                            }
                            if (Constants.p2PSingleSystemType == data.getSystemType()) {
                                fromType = IMConstants.MSG_FROM_P2P;
                            }
                            //如果没有赋值，代表是fromId放的
                            if (data.getGeoId() != 0) {
                                //保存到mongodb
                                Document messageHistoryMong = createImMessageMongoData(data.getGeoId(), readStatus, fromType, data, isEphemeralChat, ephemeralChatTime, time);
                                MongoDBUtils.insertMongodbDocument("ImMessageHistory", messageHistoryMong);
                            } else {
                                //保存到mongodb
                                Document messageHistoryMong = createImMessageMongoData(data.getFromId(), readStatus, fromType, data, isEphemeralChat, ephemeralChatTime, time);
                                MongoDBUtils.insertMongodbDocument("ImMessageHistory", messageHistoryMong);
                            }
                        }
                    }

                }
            }

//            log.info("消息消费时间2：" + (System.currentTimeMillis()-time));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                DbUtils.close(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }


    //处理群成员数据
    private void dealGroupMemberData(Connection connection, MsgProtos.testBuf data, MsgProtos.testBuf memberData, int isEphemeralChat, int ephemeralChatTime, long time) {
        List<Document> imMessageHistories = new ArrayList<>();
        try {
            List<Map<String, Object>> list = MyDbUtils.queryRunner.query(connection, "select id,userId from im_group_member where groupId=? and isAccept=1 group by userId", new MapListHandler(), data.getDestId());
            for (Map<String, Object> map : list) {
                long userId = Long.parseLong(map.get("userId").toString());
                if (data.getFromId() != userId) {
                    //创建存入mongodb的数据
                    Document messageHistoryMongo = createImMessageMongoData(userId, unReadStatus, IMConstants.MSG_FROM_GROUP, memberData, isEphemeralChat, ephemeralChatTime, time);
                    imMessageHistories.add(messageHistoryMongo);
                }
            }
            if (!imMessageHistories.isEmpty()) {
                //批量存入mongodb（群内其他成员）
                MongoDBUtils.insertMongodbDocument("ImMessageHistory", imMessageHistories);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //系统消息群发
    private void dealSystemGroupMemberData(Connection connection, MsgProtos.testBuf data, MsgProtos.testBuf memberData, long time) {
        // 群聊天
        List<Document> imMessageHistories = new ArrayList<>();
        try {
            List<Map<String, Object>> list = MyDbUtils.queryRunner.query(connection, "select id,userId from im_group_member where groupId=? and isAccept=1 group by userId", new MapListHandler(), data.getDestId());
            for (Map<String, Object> map : list) {
                long userId = Long.parseLong(map.get("userId").toString());
                long memberId = Long.parseLong(map.get("id").toString());
                //判断刚邀请进群的人，不做保存进入群聊的数据
                if (MyKakfaBoltUtils.checkMemberArray(memberId, data)) {
                    continue;
                }
                if (data.getFromId() != userId) {
                    //创建存入mongodb的数据
                    Document messageHistoryMongo = createImMessageMongoData(userId, unReadStatus, IMConstants.MSG_FROM_GROUP, memberData, 0, 5, time);
                    imMessageHistories.add(messageHistoryMongo);
                }
            }
            if (!imMessageHistories.isEmpty()) {
                //批量存入mongodb（群内其他成员）
                MongoDBUtils.insertMongodbDocument("ImMessageHistory", imMessageHistories);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建存入mongoDB的数据
     *
     * @param belongUserId      来自用户
     * @param status            状态
     * @param data              接收数据
     * @param isEphemeralChat   是否开启阅后即焚
     * @param ephemeralChatTime 阅后即焚时间
     * @param time              当前时间
     * @return messageHistory
     */
    private Document createImMessageMongoData(long belongUserId, int status, int fromType, MsgProtos.testBuf data, int isEphemeralChat, int ephemeralChatTime, long time) {
        Document document = new Document("content", data.getContent())
                .append("belongUserId", belongUserId)
                .append("destId", data.getDestId())
                .append("devType", data.getDevType())
                .append("fromId", data.getFromId())
                .append("fromName", data.getFromName())
                .append("fromType", fromType)
                .append("imageIconUrl", data.getImageIconUrl())
                .append("messageType", data.getMessageType())
                .append("msgId", data.getMsgId())
                .append("sendTime", data.getSendTime())
                .append("isEphemeralChat", isEphemeralChat)
                .append("ephemeralChatTime", ephemeralChatTime)
                .append("status", status)
                .append("readTime", time)
                .append("receiveTime", data.getReceiveTime())
                .append("version", data.getVersion());
        if (ArrayUtils.contains(MsgStoreAndNotify.deleteType, data.getMessageType())) {
            document.append("status", ImMessageHistory.deleteStatus);
        }

        return document;
    }

}
