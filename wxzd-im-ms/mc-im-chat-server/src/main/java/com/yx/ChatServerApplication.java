package com.yx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@org.springframework.context.annotation.Configuration
@EnableDiscoveryClient(autoRegister = true)
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.yx", "com.wxzd.im.ms"})
@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableCaching
public class ChatServerApplication {

    public static void main(String[] args) {
        System.out.println("聊天服务开始启动！");
        SpringApplication.run(ChatServerApplication.class, args);
        System.out.println("聊天服务启动成功！");
    }
}
