package com.yx.im.utils;

import com.yx.im.model.ImNode;

import static com.yx.im.constants.StaticConstant.isSecretModeYes;

public class CheckUtil {

    /**
     * 是否是私密模式
     *
     * @param token
     * @return
     */
    public static boolean checkSecretModel(String token) {
        return Integer.valueOf(Utils.decrypt(token.split("\\|")[3])).equals(isSecretModeYes);
    }

    /**
     * 获取节点和ip和端口号是否相同
     *
     * @param node
     * @param hostPort
     * @return
     */
    public static boolean checkHostPort(ImNode node, String hostPort) {
        return hostPort.equals((node.getHost() + ":" + node.getSocketPort()));
    }

}
