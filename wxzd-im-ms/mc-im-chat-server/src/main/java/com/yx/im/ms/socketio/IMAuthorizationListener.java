package com.yx.im.ms.socketio;


import cn.hutool.log.StaticLog;
import com.corundumstudio.socketio.AuthorizationListener;
import com.corundumstudio.socketio.HandshakeData;
import com.wxzd.im.ms.utils.RedisPoolUtil;
import com.yx.im.constants.RedisKey;
import com.yx.im.utils.Utils;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class IMAuthorizationListener implements AuthorizationListener {

    @Override
    public boolean isAuthorized(HandshakeData data) {
        String userStr = Utils.decrypt(data.getSingleUrlParam("token").split("\\|")[0]);
        StaticLog.info("socket token1=" + userStr);
        String deviceStr = data.getSingleUrlParam("device");
        Jedis jedis = RedisPoolUtil.getConn();

        try {
            if (deviceStr != null) {
                String ipValue = data.getAddress().getAddress().getHostAddress();

                byte[] socketCountExpireKey = (RedisKey.socketCountExpire + userStr + "_" + deviceStr + "_" + ipValue).getBytes();

                long count = jedis.incr(socketCountExpireKey);
                if (RedisKey.socketRefuseConnect<=count) {
                    return false;
                }
                if (RedisKey.socketConnectCount==count - 1) {
                    jedis.expire(socketCountExpireKey, RedisKey.socketKeySaveTime);
                    return false;
                }
                //计算次数
                jedis.expire(socketCountExpireKey, RedisKey.socketCountTimeout);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisPoolUtil.closeConn();
        }

        StaticLog.info("验证失败，拒绝连接");
        return false;
    }

}
