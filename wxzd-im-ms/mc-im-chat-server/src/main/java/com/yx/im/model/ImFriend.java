package com.yx.im.model;
// Generated 2019-8-26 16:22:07 by Hibernate Tools 4.3.5.Final

/**
 * ImFriend generated by hbm2java
 */
public class ImFriend implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8957242998273224578L;
	
	private Long id;
    private Long userId;
    private Long friendId;
    private String remark;
    private Long createrId;
    private Long createTime;
    private Integer isBlack;
    private Integer isFriend;
    private Integer receiveTip;
    private Integer isSecretFriend;
    private Integer isScreenshotTip;
    private Integer isEphemeralChat;
    private Integer ephemeralChatTime;
    private Integer sourceType;
    private Integer isSecretHistory;

    /**
     * 好友来源方式，好友来源方式，1：通过名片添加，2通过用户ID添加，3通过临时ID添加，4对方通过名片添加，5对方通过用户ID添加，6对方通过临时ID添加
     */
    public static final int businessCardType = 1;
    public static final int idType = 2;
    public static final int temporaryIdType = 3;
    public static final int otherBusinessCardType = 4;
    public static final int otherIdType = 5;
    public static final int otherTemporaryIdType = 6;

    /**
     * 确认状态：1同意，2拒绝
     */
    public static final int agreeState = 1;
    public static final int refuseState = 2;

    /**
     * 是否黑名单：0不是黑名单 1是黑名单
     */
    public static final String unblack = "0";
    public static final String isblack = "1";

    /**
     * 好友和群聊的消息通知
     */
    public static final Integer newMessageNotificationClose = 0;
    public static final Integer newMessageNotificationOpen = 1;


    public Integer getIsSecretFriend() {
        return isSecretFriend;
    }

    public void setIsSecretFriend(Integer isSecretFriend) {
        this.isSecretFriend = isSecretFriend;
    }

    public Integer getIsScreenshotTip() {
        return isScreenshotTip;
    }

    public void setIsScreenshotTip(Integer isScreenshotTip) {
        this.isScreenshotTip = isScreenshotTip;
    }

    public Integer getIsEphemeralChat() {
        return isEphemeralChat;
    }

    public void setIsEphemeralChat(Integer isEphemeralChat) {
        this.isEphemeralChat = isEphemeralChat;
    }

    public Integer getEphemeralChatTime() {
        return ephemeralChatTime;
    }

    public void setEphemeralChatTime(Integer ephemeralChatTime) {
        this.ephemeralChatTime = ephemeralChatTime;
    }

    public Integer getIsSecretHistory() {
        return isSecretHistory;
    }

    public void setIsSecretHistory(Integer isSecretHistory) {
        this.isSecretHistory = isSecretHistory;
    }

    public ImFriend() {
    }

    public ImFriend(Long userId, Long friendId, String remark, Long createrId, Long createTime, Integer isBlack,
                    Integer isFriend, Integer receiveTip, Integer isSecretFriend, Integer isScreenshotTip, Integer isEphemeralChat, Integer ephemeralChatTime, Integer sourceType, Integer isSecretHistory) {
        this.userId = userId;
        this.friendId = friendId;
        this.remark = remark;
        this.createrId = createrId;
        this.createTime = createTime;
        this.isBlack = isBlack;
        this.isFriend = isFriend;
        this.receiveTip = receiveTip;
        this.isSecretFriend = isSecretFriend;
        this.isScreenshotTip = isScreenshotTip;
        this.isEphemeralChat = isEphemeralChat;
        this.ephemeralChatTime = ephemeralChatTime;
        this.sourceType = sourceType;
        this.isSecretHistory = isSecretHistory;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFriendId() {
        return this.friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreaterId() {
        return this.createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public Long getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getIsBlack() {
        return this.isBlack;
    }

    public void setIsBlack(Integer isBlack) {
        this.isBlack = isBlack;
    }

    public Integer getIsFriend() {
        return this.isFriend;
    }

    public void setIsFriend(Integer isFriend) {
        this.isFriend = isFriend;
    }

    public Integer getReceiveTip() {
        return this.receiveTip;
    }

    public void setReceiveTip(Integer receiveTip) {
        this.receiveTip = receiveTip;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }
}
