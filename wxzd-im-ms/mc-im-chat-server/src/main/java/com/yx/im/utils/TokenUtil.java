package com.yx.im.utils;

import com.wxzd.im.ms.utils.RedisUtil;
import com.yx.im.model.Constants;

public class TokenUtil {

    /**
     * 获取token
     *
     * @param userId
     * @param device
     * @return
     */
    public static String getUserToken(Object userId, Object device) {
        return RedisUtil.hget(Constants.USER_TOKEN, userId + "_" + device);
    }

    /**
     * 获取是否私密
     * @param token
     * @return
     */
    public static String  getSecretByToken(String token) {
        return Utils.decrypt(token.split("\\|")[3]);
    }


}
