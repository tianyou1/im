package com.yx.im.ms.socketio;

import cn.hutool.log.StaticLog;
import com.corundumstudio.socketio.SocketConfig;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;
import com.yx.im.distributed.ImWorker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;

/**
 * SocketIo配置创建
 *
 * @author: zyu
 * @description:
 * @date: 2019/4/23 10:40
 */
@Configuration
public class SocketIOConfig {

	@Value("${socketio.host}")
	private String host;

	@Value("${socketio.port}")
	private Integer port;

	@Value("${socketio.bossCount}")
	private int bossCount;

	@Value("${socketio.workCount}")
	private int workCount;

	@Value("${socketio.allowCustomRequests}")
	private boolean allowCustomRequests;

	@Value("${socketio.upgradeTimeout}")
	private int upgradeTimeout;

	@Value("${socketio.pingTimeout}")
	private int pingTimeout;

	@Value("${socketio.pingInterval}")
	private int pingInterval;

	@Autowired
	IMAuthorizationListener imAuthorizationListener;

	@Autowired
	Environment environment;
	
	@Autowired
	private ApplicationArguments arguments;

	public int getPort() {
		return Integer.parseInt(environment.getProperty("server.port"));
	}

	/**
	 * 以下配置在上面的application.properties中已经注明
	 * 
	 * @return
	 */
	@Bean
	@DependsOn("curatorFramework")
	public SocketIOServer socketIOServer() {
		SocketConfig socketConfig = new SocketConfig();
		socketConfig.setTcpNoDelay(true);
		socketConfig.setSoLinger(0);
		socketConfig.setReuseAddress(true);
		com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
		config.setSocketConfig(socketConfig);
		//如果启动的时候有指定IP端口以启动的时候指定的为准
		String[] args = arguments.getSourceArgs();
		if (args.length == 2) {
			host = args[0];
			port = Integer.parseInt(args[1]);
        }
    	config.setHostname(host);
		config.setPort(port);
		config.setBossThreads(bossCount);
		config.setWorkerThreads(workCount);
		config.setAllowCustomRequests(allowCustomRequests);
		config.setUpgradeTimeout(upgradeTimeout);
		config.setTransports(Transport.WEBSOCKET, Transport.POLLING);
		config.setPingTimeout(pingTimeout);
		config.setPingInterval(pingInterval);
		// 根据系统类型判断是否启用epoll
		if (isLinux()) {
			config.setUseLinuxNativeEpoll(true);
		}
//      config.setKeyStorePassword("67d7ww8ghw2j");
//		InputStream stream = ChatServerApplication.class.getResourceAsStream("/leanonos.jks");
//      config.setKeyStore(stream);
		// 授权拦截
		config.setAuthorizationListener(imAuthorizationListener);
		SocketIOServer socketIOServer = new SocketIOServer(config);
		long nodeId;
		try {
			nodeId = ImWorker.getInst().getId();
			if (nodeId == 0) {
				// 构建节点
				ImWorker.getInst().setLocalNode(host, getPort(), port);
				// 启动节点
				ImWorker.getInst().init();
				// 添加节点链接数量（做负载用）
				ImWorker.getInst().incBalance();
			}
		} catch (Exception e) {
			// 构建节点
			ImWorker.getInst().setLocalNode(host, getPort(), port);
			// 启动节点
			ImWorker.getInst().init();
			// 添加节点链接数量（做负载用）
			ImWorker.getInst().incBalance();
		}
		return socketIOServer;
	}

	private static boolean isLinux() {
		return System.getProperty("os.name").toLowerCase().indexOf("linux") >= 0;
	}

}