package com.yx.im.thread;

import cn.hutool.log.StaticLog;
import com.yx.im.push.AndroidNotification;
import com.yx.im.push.PushClient;
import com.yx.im.push.android.AndroidCustomizedcast;
import com.yx.im.push.ios.IOSCustomizedcast;
import org.springframework.stereotype.Component;

@Component
public class UMentMsgRunner {

    private static PushClient client = new PushClient();
    private static AndroidCustomizedcast androidCustomizedcast;
    private static IOSCustomizedcast ioscustomizedcast;

    static {
        try {
            androidCustomizedcast = new AndroidCustomizedcast("5fa8aa3ce91fe51466bfe01d", "jo3cvcbeztcpmumllo6nzetgpxo1nv7n");
            ioscustomizedcast = new IOSCustomizedcast("5fbb59d90fe0ee328537bf8b", "egzlqpuz1ox0lw87hugc9egg6aojfec8");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 推送安卓
     *
     * @param cid
     * @param title
     * @param content
     * @param messageType
     */
    public void sendAndroidUment(String cid, String title, String content, int messageType,boolean playVibrate,boolean playSound) {
        //获取推送内容
        String text = PushUtils.getSessionContent(content, messageType);
        sendAndroid(cid, title, text,playVibrate,playSound);
    }

    /**
     * 推送苹果
     *
     * @param cid
     * @param title
     * @param content
     * @param messageType
     */
    public void sendIOSUment(String cid, String title, String content, int messageType,boolean playSound) {
        //获取推送内容
        String text = PushUtils.getSessionContent(content, messageType);
        sendIOS(cid, title, text,playSound);
    }


    /**
     * 推送安卓和苹果（格式只适用于群）
     *
     * @param cids
     * @param title
     * @param subllTitle
     * @param content
     * @param messageType
     */
    public void sendGroupUment(String cids, String title, String subllTitle, String content, int messageType,boolean playVibrate,boolean playSound) {
        //获取推送内容
        String text = PushUtils.getSessionContent(content, messageType);
        sendAndroid(cids, title, subllTitle + ":" + text,playVibrate,playSound);
        sendIOS(cids, title, subllTitle + ":" + text,playSound);

    }


    private void sendAndroid(String cids, String title, String text,boolean playVibrate,boolean playSound) {
        try {
            //安卓
            androidCustomizedcast.setAlias(cids, "im_android_cid");
            androidCustomizedcast.setTitle(title);
            androidCustomizedcast.setTicker("");
            androidCustomizedcast.setText(text);
            androidCustomizedcast.goAppAfterOpen();
            androidCustomizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            // TODO Set 'production_mode' to 'false' if it's a test device.
            // For how to register a test device, please see the developer doc.
            androidCustomizedcast.setProductionMode();
            //厂商通道相关参数
            androidCustomizedcast.setChannelActivity("your channel activity");
            androidCustomizedcast.setChannelProperties("abc");
            androidCustomizedcast.setPlayLights(false);
            androidCustomizedcast.setPlayVibrate(playVibrate);
            androidCustomizedcast.setPlaySound(playSound);
            client.send(androidCustomizedcast);
        } catch (Exception e) {
            e.printStackTrace();
            StaticLog.info("友盟ANDROID推送失败=" + e.getMessage());
        }
    }

    private void sendIOS(String cids, String title, String text,boolean playSound) {
        try {
            //IOS推送
            ioscustomizedcast.setAlias(cids, "leme");
            //alert的值设置为字典
            ioscustomizedcast.setAlert(title, "", text);
            ioscustomizedcast.setBadge(0);
            ioscustomizedcast.setCustomizedField("display_type", "notification");
            // TODO set 'production_mode' to 'true' if your app is under production mode
            ioscustomizedcast.setTestMode();
            if(playSound){
                ioscustomizedcast.setSound("default");
            }else {
                ioscustomizedcast.setSound("");
            }
            client.send(ioscustomizedcast);
        } catch (Exception e) {
            e.printStackTrace();
            StaticLog.info("友盟IOS推送失败=" + e.getMessage());
        }
    }


}
