package com.yx.im.constants;

public class StaticConstant {
    /**
     * 聊天文件文件夹id
     */
    public static final String chatFolderId = "1";

    /**
     * 头像文件夹id
     */
    public static final String headImageFolderId = "2";

    /**
     * 举报附件文件夹id
     */
    public static final String reportFolderId = "3";

    /**
     * 临时文件夹
     */
    public static final String tempDir = "D:temp";

    /**
     * 匿名头像默认头像
     */
    public static final String anonymousHeadUrl = "file/show?fileId=4f4e7de9efe44aad9fc1a362581c9f12";

    /**
     * 默认群头像的url
     */
    public static final String groupHeadUrl = "file/show?fileId=aa44cbf6e32344f6bbac0f13d92b3ea7";

    /**
     * 设置开启关闭
     */
    public static final Integer setOn = 1;
    public static final Integer setOff = 0;

    /**
     * 账号密码长度限制
     */
    public static final Integer accountMinL = 4;
    public static final Integer accountMaxL = 32;
    public static final Integer pwdMinL = 8;
    public static final Integer pwdMaxL = 16;

    /**
     * 0离线，1在线
     */
    public static final Integer offline = 0;
    public static final Integer online = 1;

    /**
     * 是否进入私密模式，1：否，2：是
     */
    public static final Integer isSecretModeNo = 1;
    public static final Integer isSecretModeYes = 2;

    /**
     * 是否为当前用户是自己
     */
    public static final Integer notCurrentUser = 0;
    public static final Integer isCurrentUser = 1;

    /**
     * 是否有私密模式密码
     */
    public static final String withoutSecretPwd = "0";
    public static final String haveSecretPwd = "1";

    /**
     * 申请类型:1好友申请2进群确认申请3加群申请
     */
    public static final Integer friendReqType = 1;
    public static final Integer confirmReqType = 2;
    public static final Integer inviteReqType = 3;


    /**
     * 目标类型:1好友2群组3二维码加群
     */
    public static final Integer singleType = 1;
    public static final Integer groupType = 2;
    public static final Integer QRCodeType = 3;

    /**
     * 好友来源方式，好友来源方式，1：通过名片添加，2通过用户ID添加，3通过群成员id添加，4对方通过名片添加，5对方通过用户ID添加，6对方通过群成员id添加
     */
    public static final Integer businessCardType = 1;
    public static final Integer idType = 2;
    public static final Integer temporaryIdType = 3;
    public static final Integer otherBusinessCardType = 4;
    public static final Integer otherIdType = 5;
    public static final Integer otherTemporaryIdType = 6;

    /**
     * 确认状态：1同意，2拒绝
     */
    public static final Integer agreeState = 1;
    public static final Integer refuseState = 2;

    /**
     * 是否黑名单：0不是黑名单 1是黑名单
     */
    public static final Integer unBlack = 0;
    public static final Integer isBlack = 1;

    /**
     * 1接收提示0不接收提示
     */
    public static final Integer noTipMessage = 0;
    public static final Integer tipMessage = 1;

    /**
     * 私密模式或者私密群聊，0否，1是
     */
    public static final Integer noSecret = 0;
    public static final Integer isSecret = 1;

    /**
     * 聊天记录是否加密，0否，1是
     */
    public static final Integer noHistorySecret = 0;
    public static final Integer historySecret = 1;

    /**
     * 截屏通知，0不通知，1通知
     */
    public static final Integer noScreenshotTip = 0;
    public static final Integer screenshotTip = 1;

    /**
     * 是否启动阅后即焚，0禁用，1启用
     */
    public static final Integer noEphemeralChat = 0;
    public static final Integer ephemeralChat = 1;

    /**
     * 消息保留时间
     */
    public static final Integer defaultChatTime = 5;

    /**
     * 0单向1双向2陌生
     */
    public static final Integer isFriendSignal = 0;
    public static final Integer isFriendBoth = 1;
    public static final Integer isFriendStranger = 2;

    /**
     * 群名称长度设置 公告长度设置
     */
    public static final Integer groupNameLength = 16;
    public static final Integer groupDescriptionsLength = 80;

    /**
     * 群状态（0：解散，1：正常，2：冻结）
     */
    public static final Integer groupDissolve = 0;
    public static final Integer groupNormal = 1;
    public static final Integer groupFreeze = 2;

    /**
     * 禁止群成员互加，0禁用，1启用
     */
    public static final Integer isFriendEachOtherClose = 0;
    public static final Integer isFriendEachOtherOpen = 1;

    /**
     * 群角色
     */
    public static final Integer groupRoleOwner = 1;
    public static final Integer groupRoleManager = 2;
    public static final Integer groupRoleMember = 3;

    /**
     * 管理员设置与取消
     */
    public static final Integer groupManagerSet = 2;
    public static final Integer groupManagerCancel = 3;

    /**
     * 管理员人数
     */
    public static final Integer managerMaxNum = 12;

    /**
     * 消息状态：1已读2未读3撤回4删除
     */
    public static final Integer readStatus = 1;
    public static final Integer unReadStatus = 2;
    public static final Integer rebackStatus = 3;
    public static final Integer deleteStatus = 4;

    /**
     * 删除类型：（1：单向服务器删除， 2：双向同步及服务器删除）
     */
    public static final Integer signalDeleteType = 1;
    public static final Integer bothDeleteType = 2;

    /**
     * 二维码类型
     */
    public static final Integer QRCodePType = 1;
    public static final Integer QRCodeGType = 2;
    public static final Integer QRCodePCType = 3;

    /**
     * 身份类型，0用户，1群聊
     */
    public static final Integer identityTypeSingle = 0;
    public static final Integer identityTypeGroup = 1;

    /**
     * 临时身份类型，1：ID、2：二维码
     */
    public static final Integer tempTypeID = 1;
    public static final Integer tempTypeQcode = 2;

    /**
     * 通过名片（账号）添加，0不允许，1允许
     */
    public static final Integer searchAccountClose = 0;
    public static final Integer searchAccountOpen = 1;

    /**
     * 通过ID添加，0不允许，1允许
     */
    public static final Integer searchUserIdClose = 0;
    public static final Integer searchUserIdOpen = 1;

    /**
     * 通过群聊添加，0不允许，1允许
     */
    public static final Integer searchGroupIdClose = 0;
    public static final Integer searchGroupIdOpen = 1;

    /**
     * 1有效0无效2冻结
     */
    public static final Integer loseUser = 0;
    public static final Integer vaildUser = 1;
    public static final Integer freezeUser = 2;

    /**
     * 自动注销，0不自动注销，1自动注销
     */
    public static final Integer autoCancelNo = 0;
    public static final Integer autoCancelYes = 1;

    /**
     * 私聊全局加密管理
     */
    public static final Integer secretFriendSetClose = 0;
    public static final Integer secretFriendSetOpen = 1;

    /**
     * 群聊全局加密管理
     */
    public static final Integer secretGroupSetClose = 0;
    public static final Integer secretGroupSetOpen = 1;

    /**
     * 举报说明字数
     */
    public static final Integer report_descriptions_length = 200;

    /**
     * 被举报对象类型(1：单聊，2：群聊，3：公众号，4：群成员，)
     */
    public static final Integer signalReportedType = 1;

    public static final Integer groupReportedType = 2;

    public static final Integer officalReportedType = 3;

    public static final Integer groupMemberReportedType = 4;

    /**
     * 申请：1有效2已删除
     */
    public static final Integer requestValidStatus = 1;
    public static final Integer requestDeleteStatus = 2;
    public static final Integer requestUnuseStatus = 3;

    /**
     * -1用户审核失效-2群审核失效1待用户审核2审核通过3用户拒绝 4.待群主/管理员审核 5.群主/管理员拒绝6群主/管理员同意
     */
    public static final Integer failStatus = -1;
    public static final Integer failGroupStatus = -2;
    public static final Integer waitStatus = 1;
    public static final Integer agreeStatus = 2;
    public static final Integer refuseStatus = 3;
    public static final Integer waitOwnerStatus = 4;
    public static final Integer ownerRefuseStatus = 5;
    public static final Integer ownerAgreeStatus = 6;



    /**
     * 是否启动二次验证，0：否，1：是
     */
    public static final Integer isSecondaryNo = 0;
    public static final Integer isSecondaryYes = 1;

    /**
     * 二次验证类型，1：手机，2：邮箱，3：谷歌，4：二级密码
     */
    public static final Integer typePhone = 1;
    public static final Integer typeEmail = 2;
    public static final Integer typeGoogle = 3;
    public static final Integer typeSecond = 4;

    /**
     * 1成功 0失败
     */
    public static final int success=1;
    public static final int fail=0;

    /**
     * 好友备注最大长度16
     */
    public static final int remarkMaxL=16;

    /**
     * 意见反馈内容最大为200个字
     */
    public static final int suggestionMaxL=200;

    /**
     *
     * 昵称最大长度
     */
    public static final int nickNameMaxLength=16;

    /**
     * 签名最大长度
     */
    public static final int signMaxLength=40;

    /**
     * 好友申请理由
     */
    public static final int friendReqMaxL=40;

    /**
     * 密保口令最大长度
     */
    public static final int secondaryPwdMaxL=16;

    /**
     *未处理0 已处理
     */
    public static final int unHandle=0;
    public static final int handle=1;

    /**
     * 群聊全局加密管理
     */
    public static final Integer secretgroupSetClose = 0;
    public static final Integer secretgroupSetOpen = 1;

    /**
     * 1.好友聊天记录加密设置、2.群聊聊天记录加密设置
     */
    public static final Integer secretFriendSet = 1;
    public static final Integer secretgroupSet = 2;


    /**
     * 1:新消息通知、2:输入状态显示、3:加好友时需要验证，4.进群邀请确认,5.用户id显示
     */
    public static final int newInfoNotify = 1;
    public static final int inputStatus = 2;
    public static final int friendNeedAuth = 3;
    public static final int groupedNeedAuth = 4;
    public static final int showUserid = 5;

    /**
     * 1:新消息通知、1:语音和视频通话提醒、2:通知显示消息详情、3:声音提示、4:震动提示、
     */
    public static final Integer voiceAndVideoNotify = 1;
    public static final Integer notifyShow = 2;
    public static final Integer voiceNotify = 3;
    public static final Integer vibrationNotify = 4;

    /**
     * 1:用户ID、2:群名片、3:名片
     */
    public static final int searchTypeUserId = 1;
    public static final int searchTypeGroupId = 2;
    public static final int searchTypeAccount = 3;

    /**
     * 群内截屏提醒，0不通知，1通知
     */
    public static final Integer isScreenshotTipClose = 0;
    public static final Integer isScreenshotTipOpen = 1;
    /**
     * 群组用户的isAccept:0未审核1已审核2已拒绝，3待用户审核、4用户拒绝
     */
    public static final Integer isCheck = 1;

    /**
     * 系统消息的区分，1：群聊单条，2群聊全部，3需要推送的消息,4好友
     */
    public static final Integer singleSystemType = 1;
    public static final Integer groupSystemType = 2;
    public static final Integer systemTypeSend = 3;
    public static final Integer p2PSingleSystemType=4;

    /**
     * 声音提示开启和关闭
     */
    public static final Integer voiceNotifyOpen = 1;
    public static final Integer voiceNotifyClose = 0;

    /**
     * 震动提示开启和关闭
     */
    public static final Integer vibrationNotifyOpen = 1;
    public static final Integer vibrationNotifyClose = 0;

    /**
     * 会话消息显示，0否，1是
     */
    public static final Integer isShowSessionMessageNo=0;
    public static final Integer isShowSessionMessageYes=1;


}
