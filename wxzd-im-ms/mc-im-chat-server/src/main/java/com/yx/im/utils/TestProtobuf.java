package com.yx.im.utils;

import com.alibaba.fastjson.JSONObject;
import com.yx.im.model.ImMessage;
import com.yx.im.model.MsgProtos;

import java.io.IOException;

public class TestProtobuf {

    public static void main(String[] args) throws IOException {
        MsgProtos.testBuf.Builder builder = MsgProtos.testBuf.newBuilder();
        builder.setId(12211);
        builder.setDestId(12312);
        builder.setDevType(2);
        builder.setFromId(123);
        builder.setFromType(1);
        builder.setGeoId(121);
        builder.setMessageType(2);
        builder.setReceiveTime(2312312311L);
        builder.setSendTime(2312312311L);
        builder.setStatus(1);
        builder.setVersion(1);
        builder.setContent("23123");
        builder.setFromName("发到");
        builder.setImageIconUrl("dsajjda");
        builder.setMsgId("ddasd-qwdqwd");
        JSONObject json = new JSONObject();
        json.put("aa", "11");
        json.put("bb", "22");
        builder.setExt(json.toJSONString());
        MsgProtos.testBuf info = builder.build();
        byte[] result = info.toByteArray();
        System.out.println("google protobuf=====" + result.length);
        MsgProtos.testBuf testBuf = MsgProtos.testBuf.parseFrom(result);

//        System.out.println("1:"+JSONObject.toJSONString(testBuf));
        ImMessage message = new ImMessage();
        message.setFromName(testBuf.getFromName());
        message.setMessageType(testBuf.getMessageType());
        message.setContent(testBuf.getContent());
        message.setDestId(testBuf.getDestId());
        message.setFromId(testBuf.getFromId());
        message.setSendTime(testBuf.getSendTime());
        message.setFromType(testBuf.getFromType());
        message.setImageIconUrl(testBuf.getImageIconUrl());
        message.setMsgId(testBuf.getMsgId());
        message.setReceiveTime(testBuf.getReceiveTime());
        message.setDevType(testBuf.getDevType());
        message.setExt(testBuf.getExt());
        message.setGeoId(testBuf.getGeoId());
        message.setId(testBuf.getId());
        message.setStatus(testBuf.getStatus());
        message.setVersion(testBuf.getVersion());
        System.out.println(JSONObject.toJSONString(message).getBytes().length);
    }
}
