package com.yx.im.model;

public class Constants {

    public final static String USERID_IN_COOKIE = "user_in_cookie";

    public static final Integer COOKIE_EXPIRY = 7 * 24 * 60 * 60;

    public static final String dbsource1 = "dataSource1";

    public static final String dbsource2 = "dataSource2";

    public static final int SUCCESS_CODE = 1;

    public static final int FAIL_CODE = -1;

    public static final String SMS_KEY = "smsKey:";

    public static final String SMS_EMAIL = "smsEmailKey:";

    public static final int RED_PACKET_TYPE_RANDOM = 1;

    public static final int RED_PACKET_TYPE_AVG = 2;

    public static final String USER_IP_KEY = "user_ip_";

    public static final int TRANS_PENDING = 1;

    public static final int TRANS_RECEIVE = 2;

    public static final int TRANS_BACK = 3;

    /**
     * 用户保存到redis的token
     */
    public static final String USER_TOKEN = "userToken";

    /**
     * 用户保存到redis的私密消息
     */
    public static final String USER_SECRET = "userSecret";

    /**
     * 用户保存到redis的token
     */
    public static final String USER_CODE = "userCode:";

    /**
     * 修改密码，修改私密模式密码，修改二次验证，修改聊天记录密码
     */
    public static final String USER_TOKEN_CHECK = "userTokenCheck:";

    /**
     * 系统通知用
     */
    public static final String SYSTEM_NOTIFY = "systemNotify";

    /**
     * kafka推送消息topic
     */
    public static final String KAFKA_IM_TOPIC = "im_chat_log";
}
