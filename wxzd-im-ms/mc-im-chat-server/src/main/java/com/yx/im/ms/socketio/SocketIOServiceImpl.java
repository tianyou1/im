package com.yx.im.ms.socketio;

import com.alibaba.fastjson.JSONObject;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.wxzd.im.ms.utils.RedisPoolUtil;
import com.yx.base.service.chat.ChatService;
import com.yx.base.service.chat.ClientMapService;
import com.yx.im.constants.IMConstants;
import com.yx.im.constants.RedisKey;
import com.yx.im.distributed.ImWorker;
import com.yx.im.model.ImMessage;
import com.yx.im.utils.TokenEnum;
import com.yx.im.utils.TokenUtil;
import com.yx.im.utils.Utils;
import com.yx.im.utils.YxUtil;

import cn.hutool.log.StaticLog;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;

@Service(value = "socketIOService")
public class SocketIOServiceImpl implements SocketIOService {

	@Autowired
	private SocketIOServer socketIOServer;
	
	public static SocketIOServer server;

	@Autowired
	private ClientMapService clientService;

	@Autowired
	private ChatService chatService;

	private static String LISTEN_HOSTS;

	/**
	 * Spring IoC容器创建之后，在加载SocketIOServiceImpl Bean之后启动
	 *
	 * @throws Exception
	 */
	@PostConstruct
	private void autoStartup() throws Exception {
		LISTEN_HOSTS = socketIOServer.getConfiguration().getHostname() + ":"
				+ socketIOServer.getConfiguration().getPort();
		start();
	}

	/**
	 * Spring IoC容器在销毁SocketIOServiceImpl Bean之前关闭,避免重启项目服务端口占用问题
	 *
	 * @throws Exception
	 */
	@PreDestroy
	private void autoStop() throws Exception {
		stop();
	}

	@Override
	public void start() throws Exception {
		// 监听客户端连接
		socketIOServer.addConnectListener(client -> {
			String userIdStr = Utils.decrypt(getParamsByClient(client, "token").split("\\|")[0]);
			String deviceStr = getParamsByClient(client, "device");

			long userId = Long.parseLong(userIdStr);
			int device = Integer.parseInt(deviceStr);

			String userToken = TokenUtil.getUserToken(userIdStr, deviceStr);
			if (StringUtils.isNotEmpty(userToken)) {
				// 如果token为私密的
				if (TokenEnum.tokenValidSecret.getCode()
						.equals(TokenUtil.getSecretByToken(userToken))) {
					// 获取全部的群（包括私密的群）
					chatService.joinClientRoom(userId, client);
				} else {
					// 获取非私密的群
					chatService.joinSecretClientRoom(userId, client);
				}
			}

			// 添加节点链接数量（做负载用）
			ImWorker.getInst().incBalance();

			//链接活跃度
			Jedis jedis = RedisPoolUtil.getConn();
			String key = RedisKey.userSocketCount;
			jedis.setbit(key, userId, "1");
			RedisPoolUtil.closeConn();

			// 推送清零
			StaticLog.info("--用户：" + userIdStr + ",device=" + deviceStr + " 连接成功");
			clientService.addClient(userId, device, LISTEN_HOSTS, client);

			// 推送离线消息
//			String key = "msg_" + userId;
//            chatService.sendOnlineAndOffLineMessage(key, userId);
		});

		// 监听聊天事件
		socketIOServer.addEventListener(IMConstants.IM_EVENT_CHAT, ImMessage.class, (client, data, ackRequest) -> {
			if (StringUtils.isBlank(data.getMsgId()) || data.getFromType() == null || data.getDestId() == null
					|| StringUtils.isBlank(data.getFromName()) || StringUtils.isBlank(data.getImageIconUrl())
					|| StringUtils.isBlank(data.getContent()) || data.getMessageType() == null) {
				ackRequest.sendAckData(YxUtil.createFail("必要条件缺少"));
				StaticLog.info("收到异常信息。。" + JSONObject.toJSONString(data));
				return;
			}
			StaticLog.info("服务器收到信息。。" + JSONObject.toJSONString(data));
			ackRequest.sendAckData(YxUtil.createSimpleSuccess(data.getMsgId()));
			StaticLog.info("服务器回复ack=。。" + data.getMsgId());

			// 处理数据
			chatService.handleRecMessage(client, data);
		});

		// 监听心跳事件
		socketIOServer.addEventListener(IMConstants.IM_EVENT_HEART, ImMessage.class, (client, data, ackRequest) -> {

			// 如果是新连接，则放入session list
			if (ClientMapService.clientMap.get(data.getFromId(), data.getDevType()) != client) {
				StaticLog.info("新的心跳连接被加入：" + data.getFromId());
				clientService.addClient(data.getFromId(), data.getDevType(), LISTEN_HOSTS, client);
			}

			StaticLog.info(data.getFromId() + " 的心跳");

		});

		// 监听客户端断开连接
		socketIOServer.addDisconnectListener(client -> {
			long userId = clientService.getUserId(client);
			StaticLog.info("下线:" + userId);
			clientService.removeClient(client);
			// 减少节点链接数量（做负载用）
			ImWorker.getInst().decrBalance();
			Jedis jedis = RedisPoolUtil.getConn();
			String key = RedisKey.userSocketCount;
			jedis.setbit(key, userId, "0");
			RedisPoolUtil.closeConn();
		});

		socketIOServer.start();
		server=socketIOServer;
        StaticLog.info("SocketIo服务启动成功！正在监听："+LISTEN_HOSTS);
	}

	@Override
	public void stop() {
		if (socketIOServer != null) {
			socketIOServer.stop();
			socketIOServer = null;
		}
	}

	/**
	 * 此方法为获取client连接中的参数，可根据需求更改
	 * 
	 * @param client
	 * @return
	 */
	private String getParamsByClient(SocketIOClient client, String paramName) {
		// 从请求的连接中拿出参数（这里的loginUserNum必须是唯一标识）
		Map<String, List<String>> params = client.getHandshakeData().getUrlParams();
		List<String> list = params.get(paramName);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
}