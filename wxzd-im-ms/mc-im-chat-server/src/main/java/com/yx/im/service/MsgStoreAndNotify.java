package com.yx.im.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wxzd.im.ms.utils.RedisUtil;
import com.yx.base.service.chat.GenerateKafkaData;
import com.yx.base.service.group.GroupService;
import com.yx.base.service.group.UserService;
import com.yx.im.constants.IMConstants;
import com.yx.im.model.*;
import com.yx.im.thread.UMentMsgRunner;
import com.yx.im.utils.CheckUtil;
import com.yx.im.utils.TokenEnum;
import com.yx.im.zookeeper.ImLoadBalance;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.yx.im.constants.StaticConstant.*;

@Service
public class MsgStoreAndNotify {
    @Autowired
    private ImLoadBalance imLoadBalance;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private UMentMsgRunner uMentMsgRunner;
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    //int[] needNoticeType = { 7, 8, 9, 10, 11, 12, 13, 14,17,18,19,20,21,22, 26, 36, 37, 38, 39, 40, 51 };
    //当接收方不在线时，取消了加群，退出群的消息保存到redis
//    public int[] needNoticeType = {10, 11, 12, 13, 17, 18, 19, 20, 21, 22, 26, 32, 36, 37, 38, 39, 40, 51, 119, 120};
    public static int[] jiguangNoticeType = {2, 3, 4, 11, 16, 29, 30, 34, 35, 44, 42, 46, 85};
    public static int[] singleTypeCheck = {2, 3, 4, 28, 29, 30, 34, 42, 44, 85};


    /**
     * 同意好友时发送（发送消息判断私密并且保存消息）
     *
     * @return
     */
    public void storeMsgAndNotifyImServerAgreeFriend(ImMessage msgToFrend, ImMessage msgToSelf) {

        checkSecretModeMessageSend(msgToFrend, msgToFrend.getDestId());
        checkSecretModeMessageSend(msgToSelf, msgToSelf.getDestId());
        //save to msg to history（先保存自己，在保存朋友的）
        saveHistoryMessageSingle(msgToSelf);
        saveHistoryMessageSingle(msgToFrend);
    }

    /**
     * 保存消息（单向）
     *
     * @param message
     */
    private void saveHistoryMessageSingle(ImMessage message) {
        //save to msg to history
        saveToFromUser(message);
    }

    public void storeMsgAndNotifyImServerSaveBoth(ImMessage message, long userId) {

        checkSecretModeMessageSend(message, userId);

        //save to msg to history
        saveHistoryMessage(message);

    }

    //发送消息并且保存数据
    public void storeMsgAndNotifyImServerSaveSingle(ImMessage message, long userId) {

        checkSecretModeMessageSend(message, userId);

        //save to msg to history
        saveHistoryMessageSingle(message);

    }


    //发送消息
    public void storeMsgAndNotifyImServerUsers(ImMessage message, List<String> userIds) {
        for (String userId : userIds) {
            checkSecretModeMessageSend(message, Long.parseLong(userId));
        }
    }


    //发送消息并且保存数据
    public void storeMsgAndNotifyImServerUsersSaveSingle(ImMessage message, List<String> userIds) {
        for (String userId : userIds) {
            long uId = Long.parseLong(userId);
            checkSecretModeMessageSend(message, uId);
            message.setGeoId(uId);
            //save to msg to history
            saveHistoryMessageSingle(message);
        }
    }

    /**
     * 发送消息判断私密
     *
     * @param message
     * @param userId
     * @return
     */
    public void storeMsgAndNotifyImServer(ImMessage message, long userId) {
        checkSecretModeMessageSend(message, userId);
    }

    /**
     * 保存消息（双向）
     *
     * @param message
     */
    private void saveHistoryMessage(ImMessage message) {
        //save to from user
        saveToFromUser(message);
        //save to dest user
        saveToDestUser(message);

    }


    private void saveToFromUser(ImMessage message) {
        message.setStatus(unReadStatus);
        if (message.getGeoId() == null || message.getGeoId() == 0) {
            message.setGeoId(message.getFromId());
        }
        //保存到mongodb
        kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(message), StandardCharsets.ISO_8859_1));
        //重新换成0
        message.setGeoId(0L);
    }

    private void saveToDestUser(ImMessage message) {
        message.setStatus(unReadStatus);
        if (message.getGeoId() == null || message.getGeoId() == 0) {
            message.setGeoId(message.getDestId());
        }
        //保存到mongodb
        kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(message), StandardCharsets.ISO_8859_1));
    }


    /**
     * 判断私密模式发送消息
     *
     * @param message
     * @param userId
     * @return
     */
    private void checkSecretModeMessageSend(ImMessage message, long userId) {
        Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
        // 有设备在线时，通知在线目标用户
        if (devMap.size() > 0) {
            Iterator<String> iterator = devMap.keySet().iterator();
            while (iterator.hasNext()) {
                String deviceId = iterator.next();
                //单聊消息（私密）,并且不是通知自己
                if (message.getFromType().equals(IMConstants.MSG_FROM_P2P) && userId != message.getFromId()) {
                    //添加私密时消息不接收（相当于离线消息处理）
                    if (checkSendFriendMsg(message, userId, deviceId)) {
                        continue;
                    }
                }
                //群聊消息,并且不是通知自己
                else if (message.getFromType().equals(IMConstants.MSG_FROM_GROUP) && userId != message.getFromId()) {//群聊消息
                    if (checkSendGroupMsg(message, userId, deviceId)) {
                        continue;
                    }
                }
                //系统消息推送
                else if (message.getFromType().equals(IMConstants.MSG_FROM_SYS) && userId != message.getFromId()) {
                    if (message.getSystemType() != null) {
                        if (message.getSystemType().equals(p2PSingleSystemType)) {
                            //系统消息单聊的处理
                            if (checkSendFriendMsg(message, userId, deviceId)) {
                                continue;
                            }
                        }
                        //系统消息群聊的处理
                        if (message.getSystemType().equals(singleSystemType)) {
                            if (checkSendGroupMsg(message, userId, deviceId)) {
                                continue;
                            }
                        }
                        //系统消息需要发送的处理
                        if (message.getSystemType().equals(systemTypeSend)) {
                            notifyImServerMessage(message, userId, Integer.parseInt(deviceId));
                            continue;
                        }
                    }

                }
                // 加入通知列表，通知所在服务器
                notifyImServerMessage(message, userId, Integer.parseInt(deviceId));
            }
        } else {
            // 没有设备在线时，只保留部分消息。
//            if (message != null && message.getMessageType() != null && message.getDevType() != null
//                    && message.getDevType() > 0 && (ArrayUtil.contains(needNoticeType, message.getMessageType()))) {
//                RedisUtil.lpush("msg_" + userId, JSONObject.toJSONString(message));
//            }

            // 极光推送
            if (message != null && message.getMessageType() != null && ArrayUtil.contains(jiguangNoticeType, message.getMessageType())) {
                Map<String, Object> user = userService.getUserById(userId);
                if (message.getFromType().equals(IMConstants.MSG_FROM_P2P)) {
                    if (user != null) {
                        if (ImFriend.newMessageNotificationClose.equals(user.get("is_new_message_notification"))) {
                            //如果用户的好友消息提示没有开启，则获取下一个
                            return;
                        }


                        //查询安卓的是否有登录
                        boolean androidIsLogin = RedisUtil.hexists(Constants.USER_TOKEN, userId + "_" + TokenEnum.tokenAndroidDevice.getCode());
                        //查询IOS的是否有登录
                        boolean iosIsLogin = RedisUtil.hexists(Constants.USER_TOKEN, userId + "_" + TokenEnum.tokenIOSDevice.getCode());
                        sendUmentMessage(androidIsLogin, iosIsLogin, user, userId, message);
                    }
                }

            }
        }
    }


    private void sendUmentMessage(boolean androidIsLogin, boolean iosIsLogin, Map<String, Object> user, long userId, ImMessage message) {
        String content=message.getContent();
        Map<String, Object> map = userService.findByUserIdAndFriendId(message.getDestId(), message.getFromId());
        //判断全局会话显示如果全局不显示或者（判断全局会话显示如果全局是显示并且如果单独的是不显示）
        if (isShowSessionMessageNo.equals(user.get("is_show_session_message")) || (isShowSessionMessageYes.equals(user.get("is_show_session_message")) && isShowSessionMessageNo.equals(map.get("is_show_session_message")))) {
            content="您有未读消息";
        }

        if (voiceNotifyOpen.equals(user.get("is_voice_reminder"))) {
            if (vibrationNotifyOpen.equals(user.get("is_vibration_reminder"))) {
                if (androidIsLogin) {
                    uMentMsgRunner.sendAndroidUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), true, true);
                }
                if (iosIsLogin) {
                    uMentMsgRunner.sendIOSUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), true);
                }
            }
            if (vibrationNotifyClose.equals(user.get("is_vibration_reminder"))) {
                if (androidIsLogin) {
                    uMentMsgRunner.sendAndroidUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), false, true);
                }
                if (iosIsLogin) {
                    uMentMsgRunner.sendIOSUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), true);
                }
            }
        }


        if (voiceNotifyClose.equals(user.get("is_voice_reminder"))) {
            if (vibrationNotifyOpen.equals(user.get("is_vibration_reminder"))) {
                if (androidIsLogin) {
                    uMentMsgRunner.sendAndroidUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), true, false);
                }
                if (iosIsLogin) {
                    uMentMsgRunner.sendIOSUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), false);
                }
            }

            if (vibrationNotifyClose.equals(user.get("is_vibration_reminder"))) {
                if (androidIsLogin) {
                    uMentMsgRunner.sendAndroidUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), false, false);
                }
                if (iosIsLogin) {
                    uMentMsgRunner.sendIOSUment(String.valueOf(userId), message.getFromName(), content, message.getMessageType(), false);
                }
            }
        }
    }

    private boolean checkSendFriendMsg(ImMessage message, Long userId, String deviceId) {
        String devToken = RedisUtil.hget(Constants.USER_TOKEN, userId + "_" + deviceId);
        if (StringUtils.isNotEmpty(devToken)) {
            Map<String, Object> map = userService.findBySecretFriend(message.getDestId(), message.getFromId());

            if (!CheckUtil.checkSecretModel(devToken) //用户不在私密模式下面
                    && map != null//是私密好友发送消息
            ) {
                //添加视频或者语音在私密模式下的处理
//                if (message.getMessageType() == IMConstants.MSG_TYPE_VOICE_CHAT || message.getMessageType() == IMConstants.MSG_TYPE_VIDEO_CHAT) {
//                    message.setContent("📞对方已取消");
//                    message.setMessageType(IMConstants.MSG_TYPE_TEXT);
//                    //好友没在私密模式并且你是他的私密好友，语音和视频未接通时，保存发送人的数据
//                    return true;
//                }
                //（修改，私密消息不做保存）
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    private boolean checkSendGroupMsg(ImMessage message, Long userId, String deviceId) {
        String devToken = RedisUtil.hget(Constants.USER_TOKEN, userId + "_" + deviceId);
        if (StringUtils.isNotBlank(devToken)) {
            if (StringUtils.isNotEmpty(devToken)) {
                Map<String, Object> map = groupService.getSecretGroupMember(userId, message.getDestId());
                //不再私密模式，是私密群组的
                //（修改，私密消息不做保存）
                return !CheckUtil.checkSecretModel(devToken) && map != null;
            }
        } else {
            return true;
        }
        return false;
    }

    /**
     * 同步消息到其它设备
     *
     * @param message  消息
     * @param userId   目标id
     * @param deviceId 当前设备id
     * @return
     */
    public void synchronOtherDev(ImMessage message, long userId, int deviceId) {
        Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
        for (String devId : devMap.keySet()) {
            if (!devId.equals(String.valueOf(deviceId))) {
                notifyImServerMessage(message, userId, Integer.parseInt(devId));
            }
        }
    }

    /**
     * 通知目标服务器
     */
//    private void notifyImServer(long userId, int device) {
//        String fromHost = RedisUtil.hget(Constants.USER_IP_KEY + userId, String.valueOf(device));
//        List<ImNode> nodes = imLoadBalance.getWorkers();
//        for (ImNode node : nodes) {
//            if (CheckUtil.checkHostPort(node, fromHost)) {
//                String url = "http://" + node.getHost() + ":" + node.getPort() + "/chat/notifyNewMessage?userId=" + userId;
//                StaticLog.info("通知服务器：" + url);
//                httpRequest(url);
//            }
//        }
//    }

    /**
     * 通知目标服务器
     */
    private void notifyImServerMessage(ImMessage data, long userId, int device) {
        String fromHost = RedisUtil.hget(Constants.USER_IP_KEY + userId, String.valueOf(device));
        List<ImNode> nodes = imLoadBalance.getWorkers();
        for (ImNode node : nodes) {
            if (CheckUtil.checkHostPort(node, fromHost)) {
                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("data", JSONObject.toJSONString(data));
                paramMap.put("userId", userId);
                paramMap.put("device", device);
                HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/notifySingleNewMessage", paramMap);
            }
        }
    }


    /**
     * 通知目标服务器（除了自己的所有群组）
     */
    public void notifyGroupImServerExcludeFromId(ImMessage data) {
        //广播到其他服务
        String fromHost = RedisUtil.hget(Constants.USER_IP_KEY + data.getFromId(), String.valueOf(data.getDevType()));
        List<ImNode> nodes = imLoadBalance.getWorkers();
        if (!nodes.isEmpty()) {
            for (ImNode node : nodes) {
                if (StringUtils.isNotEmpty(fromHost)) {
                    if (!CheckUtil.checkHostPort(node, fromHost)) {
                        sendGroupMessage(data, node);
                    }
                }
            }
        }

    }


    /**
     * 通知目标服务器（所有群组）
     */
    public void notifyGroupImServerAll(ImMessage data) {
        //广播到其他服务
        List<ImNode> nodes = imLoadBalance.getWorkers();
        if (!nodes.isEmpty()) {
            for (ImNode node : nodes) {
                sendGroupMessage(data, node);
            }
        }
    }

    /**
     * 通知目标服务器并且保存历史（所有群组）
     */
    public void notifyGroupImServerAllAndSaveHistory(ImMessage data) {
        //广播到其他服务
        List<ImNode> nodes = imLoadBalance.getWorkers();
        if (!nodes.isEmpty()) {
            for (ImNode node : nodes) {
                sendGroupMessage(data, node);
            }
        }

        kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(data), StandardCharsets.ISO_8859_1));
    }

    /**
     * 公共推送部分
     *
     * @param data
     * @param node
     */
    private void sendGroupMessage(ImMessage data, ImNode node) {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("data", JSONObject.toJSONString(data));
        String result1 = HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/notifyGroupNew", paramMap);
        StaticLog.info("请求结果{}", result1);
    }


    /**
     * 解散群，通知
     *
     * @param userIds
     * @param roomId
     */
    public void notifyLeaveRoomUserIds(List<String> userIds, long roomId) {
        //群内的成员
        for (String userId : userIds) {
            //所在线的设备
            Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
            for (String value : devMap.values()) {
                //所有的节点
                List<ImNode> nodes = imLoadBalance.getWorkers();
                if (!nodes.isEmpty()) {
                    for (ImNode node : nodes) {
                        //查看节点的ip是否和链接的ip相同
                        if (CheckUtil.checkHostPort(node, value)) {
                            HashMap<String, Object> paramMap = new HashMap<>();
                            paramMap.put("userIds", CollUtil.join(userIds, ","));
                            paramMap.put("roomId", roomId);
                            StaticLog.info("通知{}离开房间{}", JSON.toJSONString(userIds), roomId);
                            String result1 = HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/leaveRoomForUserIds", paramMap);
                            StaticLog.info("请求结果{}", result1);
                        }
                    }
                }
            }
        }
    }

    /**
     * 退出私密去除非私密群聊
     *
     * @param userId
     * @param roomIds
     */
    public void notifyLeaveRooms(Long userId, List<String> roomIds) {
        //所在线的设备
        Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
        for (String value : devMap.values()) {
            //所有的节点
            List<ImNode> nodes = imLoadBalance.getWorkers();
            if (!nodes.isEmpty()) {
                for (ImNode node : nodes) {
                    if (CheckUtil.checkHostPort(node, value)) {
                        //查看节点的ip是否和链接的ip相同
                        HashMap<String, Object> paramMap = new HashMap<>();
                        paramMap.put("userId", userId);
                        paramMap.put("roomIds", CollUtil.join(roomIds, ","));
                        StaticLog.info("通知{}离开房间{}", userId, JSON.toJSONString(roomIds));
                        if (!roomIds.isEmpty()) {
                            String result1 = HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/leaveForRooms", paramMap);
                            StaticLog.info("请求结果{}", result1);
                        }
                    }

                }
            }
        }
    }

    /**
     * 离开群组
     *
     * @param userId
     * @param roomId
     */
    public void notifyLeaveRoom(Long userId, long roomId) {
        //所在线的设备
        Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
        for (String value : devMap.values()) {
            //所有的节点
            List<ImNode> nodes = imLoadBalance.getWorkers();
            if (!nodes.isEmpty()) {
                for (ImNode node : nodes) {
                    //查看节点的ip是否和链接的ip相同
                    if (CheckUtil.checkHostPort(node, value)) {
                        HashMap<String, Object> paramMap = new HashMap<>();
                        paramMap.put("userId", userId);
                        paramMap.put("roomId", roomId);
                        StaticLog.info("通知{}离开房间{}", userId, roomId);
                        String result1 = HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/leaveForRoom", paramMap);
                        StaticLog.info("请求结果{}", result1);
                    }
                }
            }
        }
    }


    /**
     * 同意申请
     *
     * @param userId
     * @param roomId
     */
    public void notifyJoinRoomOne(String userId, long roomId) {
        //所在线的设备
        Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
        for (String value : devMap.values()) {
            //所有的节点
            List<ImNode> nodes = imLoadBalance.getWorkers();
            if (!nodes.isEmpty()) {
                for (ImNode node : nodes) {
                    //查看节点的ip是否和链接的ip相同
                    if (CheckUtil.checkHostPort(node, value)) {
                        HashMap<String, Object> paramMap = new HashMap<>();
                        paramMap.put("userId", userId);
                        paramMap.put("roomId", roomId);
                        StaticLog.info("通知{}加入房间{}", JSON.toJSONString(userId), roomId);
                        String result1 = HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/joinForRoomOne", paramMap);
                        StaticLog.info("请求结果{}", result1);
                    }
                }
            }
        }
    }

    /**
     * 创建群聊通知加入房间
     *
     * @param userIds
     * @param roomId
     */
    public void notifyJoinRoom(List<String> userIds, long roomId) {
        //群内的成员
        for (String userId : userIds) {
            //所在线的设备
            Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
            for (String value : devMap.values()) {
                //所有的节点
                List<ImNode> nodes = imLoadBalance.getWorkers();
                if (!nodes.isEmpty()) {
                    for (ImNode node : nodes) {
                        //查看节点的ip是否和链接的ip相同
                        if (CheckUtil.checkHostPort(node, value)) {
                            HashMap<String, Object> paramMap = new HashMap<>();
                            paramMap.put("userIds", CollUtil.join(userIds, ","));
                            paramMap.put("roomId", roomId);
                            StaticLog.info("通知{}加入房间{}", JSON.toJSONString(userIds), roomId);
                            String result1 = HttpUtil.post("http://" + node.getHost() + ":" + node.getPort() + "/chat/joinForRoom", paramMap);
                            StaticLog.info("请求结果{}", result1);
                        }
                    }
                }
            }
        }
    }

}
