package com.yx.im.constants;

/**
 * create by
 **/
public class ServerConstants {

    //工作节点的父路径
    public static final String MANAGE_PATH = "/im/nodes";

    //工作节点的路径前缀
    public static final String PATH_PREFIX = MANAGE_PATH + "/seq-";

}
