package com.yx.im.constants;

public class CacheConstants {

    /**
     * 群成员缓存
     */
    public static  final  String memberCache ="memberCache";

    /**
     * 群成员缓存（Map结果集保存）
     */
    public static  final  String memberCacheMap ="memberCacheMap";

    /**
     * 群成员列表缓存（Map结果集保存）
     */
    public static  final  String memberListCacheMap ="memberListCacheMap";

    /**
     * 私密群组成员缓存（Map结果集保存）
     */
    public static  final  String secretMemberCacheMap ="secretMemberCacheMap";

    /**
     * 群主缓存
     */
    public static  final  String ownerCache ="ownerCache";

    /**
     * 用户信息
     */
    public static  final  String userCache ="userCache";

    /**
     * 用户信息（Map结果集保存）
     */
    public static  final  String userCacheMap ="userCacheMap";

    /**
     * 群信息
     */
    public static  final  String groupCache ="groupCache";

    /**
     * 群信息（Map结果集保存）
     */
    public static  final  String groupCacheMap ="groupCacheMap";


    /**
     * 好友信息（Map结果集保存）
     */
    public static  final  String userFriendCacheMap ="userFriendCacheMap";

    /**
     * 私密好友信息（Map结果集保存）
     */
    public static  final  String userSecretFriendCacheMap ="userSecretFriendCacheMap";

    /**
     * 非私密群聊列表（Map结果集保存）
     */
    public static  final  String noSecretMemberListCacheMap ="noSecretMemberListCacheMap";

    /**
     * 全部群聊列表
     */
    public static  final  String allMemberListCacheMap ="allMemberListCacheMap";


}
