package com.yx.im.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.yx.base.config.chatserver.ChatServerProperties;

@Configuration
@ConditionalOnClass(Config.class)
@EnableConfigurationProperties(ChatServerProperties.class)
public class ZKclientAutoConfiguration {

    @Autowired
    private ChatServerProperties chatServerProperties;

    /**
     * 	自动装配
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "redisson", name = "run-type", havingValue = "single", matchIfMissing = true)
    CuratorFramework curatorFramework() {
    	//创建客户端
    	CuratorFramework client = ClientFactory.createSimple(chatServerProperties.getZookeeperAddress());
        //启动客户端实例,连接服务器
        client.start();
        ZKclient.setClient(client);
        return client;
    }

}