package com.yx.im.utils;

/**
 * @ClassName: TokenEnum
 * @Description: token枚举类
 * @author cx
 * @date 2020.08.13
 */
public enum TokenEnum {
    tokenAndroidDevice("1","安卓设备"),
    tokenIOSDevice("2", "苹果设备"),
    tokenPcDevice("3", "PC设备"),

    tokenTemporary("0", "临时token"),
    tokenValid("1", "有效token"),
    tokenValidSecret("2", "私密token"),
    cancelToken("3", "账号注销token"),
    msgHistoryToken("4", "聊天记录密码token");




    private String code;
    private String name;
    private TokenEnum(String code, String name) {
        this.code = code;
        this.name = name();
    }

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public void setName(String name) {
        this.name = name;
    }


}
