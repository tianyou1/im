package com.yx.im.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yx.base.service.chat.ChatService;
import com.yx.im.model.ImMessage;
import com.yx.im.service.MsgStoreAndNotify;
import com.yx.im.utils.ResponseData;
import com.yx.im.utils.YxUtil;
import com.yx.im.zookeeper.ImLoadBalance;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/chat", method = RequestMethod.POST)
public class ChatController {

    @Autowired
    private ChatService chatService;
    @Autowired
    private ImLoadBalance imLoadBalance;
    @Autowired
    private MsgStoreAndNotify msgStoreAndNotify;


    /**
     * 创建群组
     *
     * @param userIds
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/joinRoom")
    public ResponseData joinRoom(String userIds, long roomId) {
        StaticLog.info("参数{}", JSON.toJSONString(userIds));
        if (StringUtils.isNotBlank(userIds)) {
            msgStoreAndNotify.notifyJoinRoom(CollUtil.newArrayList(userIds.split(",")), roomId);
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 创建群组（自身调用）
     *
     * @param userIds
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/joinForRoom")
    public ResponseData joinForRoom(String userIds, long roomId) {
        StaticLog.info("参数{}", JSON.toJSONString(userIds));
        if (StringUtils.isNotBlank(userIds)) {
            chatService.joinRoom(CollUtil.newArrayList(userIds.split(",")), roomId);
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 同意申请进群
     *
     * @param userId
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/joinRoomOne")
    public ResponseData joinRoomOne(String userId, long roomId) {
        StaticLog.info("参数{}", JSON.toJSONString(userId));
        if (StringUtils.isNotBlank(userId)) {
            msgStoreAndNotify.notifyJoinRoomOne(userId, roomId);
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }


    /**
     * 同意申请进群（自身调用）
     *
     * @param userId
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/joinForRoomOne")
    public ResponseData joinForRoomOne(String userId, long roomId) {
        StaticLog.info("参数{}", JSON.toJSONString(userId));
        if (StringUtils.isNotBlank(userId)) {
            chatService.joinRoomOne(userId, roomId);
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 离开群组
     *
     * @param userId
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/leaveRoom")
    public ResponseData leaveRoom(long userId,
                                  long roomId) {
        msgStoreAndNotify.notifyLeaveRoom(userId, roomId);
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 离开群组（自身调用）
     *
     * @param userId
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/leaveForRoom")
    public ResponseData leaveForRoom(long userId,
                                     long roomId) {
        chatService.leaveRoom(userId, roomId);
        return YxUtil.createSimpleSuccess("转发成功");
    }


    /**
     * 退出私密，移除非私密的群组
     *
     * @param userId
     * @param roomIds
     * @return
     */
    @RequestMapping(value = "/leaveRooms")
    public ResponseData leaveRooms(long userId,
                                   String roomIds) {
        if (StringUtils.isNotBlank(roomIds)) {
            msgStoreAndNotify.notifyLeaveRooms(userId, CollUtil.newArrayList(roomIds.split(",")));
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 退出私密，移除非私密的群组（自身调用）
     *
     * @param userId
     * @param roomIds
     * @return
     */
    @RequestMapping(value = "/leaveForRooms")
    public ResponseData leaveForRooms(long userId,
                                      String roomIds) {
        if (StringUtils.isNotBlank(roomIds)) {
            chatService.leaveRooms(userId, CollUtil.newArrayList(roomIds.split(",")));
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }


    /**
     * 都离开，解散群
     *
     * @param userIds
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/leaveRoomUserIds")
    public ResponseData leaveRoomUserIds(String userIds,
                                         long roomId) {
        if (StringUtils.isNotBlank(userIds)) {
            msgStoreAndNotify.notifyLeaveRoomUserIds(CollUtil.newArrayList(userIds.split(",")), roomId);
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 都离开，解散群（自身调用）
     *
     * @param userIds
     * @param roomId
     * @return
     */
    @RequestMapping(value = "/leaveRoomForUserIds")
    public ResponseData leaveRoomForUserIds(String userIds,
                                            long roomId) {
        if (StringUtils.isNotBlank(userIds)) {
            chatService.leaveRoomUserIds(CollUtil.newArrayList(userIds.split(",")), roomId);
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 发送消息（好友）
     *
     * @param data
     * @param userId
     * @return
     */
    @RequestMapping(value = "/notifyNew")
    public ResponseData notifyNew(String data, long userId) {
        msgStoreAndNotify.storeMsgAndNotifyImServer(JSONObject.parseObject(data, ImMessage.class), userId);
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 发送消息并保存两边数据（好友）
     *
     * @param data
     * @param userId
     * @return
     */
    @RequestMapping(value = "/notifyImServerSaveBoth")
    public ResponseData notifyImServerSaveBoth(String data, long userId) {
        msgStoreAndNotify.storeMsgAndNotifyImServerSaveBoth(JSONObject.parseObject(data, ImMessage.class), userId);
        return YxUtil.createSimpleSuccess("转发成功");
    }


    /**
     * 发送消息并保存单边数据（好友）
     *
     * @param data
     * @param userId
     * @return
     */
    @RequestMapping(value = "/notifyImServerSaveSingle")
    public ResponseData notifyImServerSaveSingle(String data, long userId) {
        msgStoreAndNotify.storeMsgAndNotifyImServerSaveSingle(JSONObject.parseObject(data, ImMessage.class), userId);
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 发送消息并保存两边数据
     *
     * @param data
     * @param userIds
     * @return
     */
    @RequestMapping(value = "/notifyImServerUsersSaveSingle")
    public ResponseData notifyImServerUsersSaveSingle(String data, String userIds) {
        if (StringUtils.isNotBlank(userIds)) {
            msgStoreAndNotify.storeMsgAndNotifyImServerUsersSaveSingle(JSONObject.parseObject(data, ImMessage.class), CollUtil.newArrayList(userIds.split(",")));
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 发送消息
     *
     * @param data
     * @param userIds
     * @return
     */
    @RequestMapping(value = "/notifyImServerUsers")
    public ResponseData notifyImServerUsers(String data, String userIds) {
        if (StringUtils.isNotBlank(userIds)) {
            msgStoreAndNotify.storeMsgAndNotifyImServerUsers(JSONObject.parseObject(data, ImMessage.class), CollUtil.newArrayList(userIds.split(",")));
        }
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 通知好友同意
     *
     * @param msgToFriend
     * @param msgToSelf
     * @return
     */
    @RequestMapping(value = "/notifyImServerAgreeFriend")
    public ResponseData notifyImServerAgreeFriend(String msgToFriend, String msgToSelf) {
        msgStoreAndNotify.storeMsgAndNotifyImServerAgreeFriend(JSONObject.parseObject(msgToFriend, ImMessage.class), JSONObject.parseObject(msgToSelf, ImMessage.class));
        return YxUtil.createSimpleSuccess("转发成功");
    }

//    /**
//     * 自身调用其他服务用（推送个人消息）
//     *
//     * @param userId
//     * @return
//     */
//    @RequestMapping(value = "/notifyNewMessage")
//    public ResponseData notifyNewMessage(long userId) {
//        chatService.pushMessage(userId);
//        return YxUtil.createSimpleSuccess("转发成功");
//    }


    /**
     * 推送群消息(全部人)
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/notifyGroupAll")
    public ResponseData notifyGroupAll(String data) {
        msgStoreAndNotify.notifyGroupImServerAll(JSONObject.parseObject(data, ImMessage.class));
        return YxUtil.createSimpleSuccess("转发成功");
    }


    /**
     * 推送群消息(全部人)
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/notifyGroupAllAndSaveHistory")
    public ResponseData notifyGroupAllAndSaveHistory(String data) {
        msgStoreAndNotify.notifyGroupImServerAllAndSaveHistory(JSONObject.parseObject(data, ImMessage.class));
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 自身调用其他服务用（推送群消息）
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/notifyGroupNew")
    public ResponseData notifyGroupNew(String data) {
        chatService.pushGroupMessage(JSONObject.parseObject(data, ImMessage.class));
        return YxUtil.createSimpleSuccess("转发成功");
    }

    /**
     * 自身调用其他服务用（推送个人消息）
     *
     * @param data
     * @return
     */
    @RequestMapping(value = "/notifySingleNewMessage")
    public ResponseData notifySingleNewMessage(String data, long userId, int device) {
        chatService.pushSingleMessage(JSONObject.parseObject(data, ImMessage.class), userId, device);
        return YxUtil.createSimpleSuccess("转发成功");
    }


    /**
     * 获取socket最小负载
     *
     * @return
     */
    @RequestMapping(value = "/getSocketUrl")
    public ResponseData getSocketUrl() {
        return YxUtil.createSuccessData(imLoadBalance.getBestWorker());
    }


}
