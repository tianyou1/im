package com.yx.im.constants;

public class RedisKey {

    //链接数量
    public static final String socketCountExpire = "socket_count_expire:";

    //活跃人数
    public static final String userSocketCount = "user_socket_count";

    //设置检查链接数量(按秒)
    public static final Integer socketCountTimeout = 3;

    //设置到多少数量进行检查时间
    public static final Integer socketConnectCount = 10;

    //设置为数量为12的时候，拒绝链接
    public static final Integer socketRefuseConnect = 12;

    //设置保存时长
    public static final Integer socketKeySaveTime = 24 * 60 * 60;

}
