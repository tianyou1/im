package com.yx.im.thread;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yx.im.constants.IMConstants;
import org.apache.commons.lang.StringUtils;

/**
 * 推送的公共数据处理
 */
class PushUtils {

    static String getSessionContent(String content, int messageType) {
        String msgContent = "";
        switch (messageType) {
            case IMConstants.MSG_TYPE_TEXT:
                msgContent = content;
                break;
            case IMConstants.MSG_TYPE_IMG:
                msgContent = "[图片]";
                break;
            case IMConstants.MSG_TYPE_FILE:
                msgContent = "[文件]";
                break;
            case IMConstants.MSG_TYPE_VOICE:
                msgContent = "[离线语音]";
                break;
            case IMConstants.MSG_TYPE_SEND_CARD:
                msgContent = "[发送名片]";
                break;
            case IMConstants.MSG_TYPE_SEND_LOCATION:
                msgContent = "[位置分享]";
                break;
            case IMConstants.MSG_TYPE_SEND_VIDEO:
                msgContent = "[小视频]";
                break;
            case IMConstants.MSG_TYPE_EMOJI_YUN:
                msgContent = "[表情]";
                break;
            case IMConstants.MSG_TYPE_AT:
                if (!content.contains("您有未读消息")) {
                    JSONObject contentJson=JSONObject.parseObject(content);
                    JSONArray array = contentJson.getJSONArray("textArray");
                    StringBuilder atContent = new StringBuilder();
                    for (Object o : array) {
                        JSONObject json = JSONObject.parseObject(o.toString());
                        String name = json.getString("name");
                        String text = json.getString("text");
                        if (StringUtils.isNotBlank(name)) {
                            atContent.append("@").append(name);
                        }
                        if (StringUtils.isNotBlank(text)) {
                            atContent.append(text);
                        }
                    }
                    msgContent = String.valueOf(atContent);
                } else {
                    msgContent = content;
                }
                break;
            case IMConstants.MSG_TYPE_PROFILE:
                msgContent = "[名片]";
                break;
            case IMConstants.BATCH_TRANSMIT:
                msgContent = "[转发]";
                break;
        }
        return msgContent;
    }
}
