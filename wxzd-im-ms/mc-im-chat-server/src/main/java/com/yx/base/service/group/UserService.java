package com.yx.base.service.group;

import com.yx.im.constants.CacheConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserService {
    @Autowired
    private JdbcTemplate jdbc;

    /**
     * 根据用户id查询
     *
     * @param userId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.userCacheMap, key = "#userId", unless = " #result==null or #result.size()==0")
    public Map<String, Object> getUserById(long userId) {
        List<Map<String, Object>> mapList = jdbc.queryForList("select isBanned,nickname,headUrl,is_new_message_notification,is_new_group_message_notification,is_voice_reminder,is_vibration_reminder,is_show_session_message,is_show_group_session_message from im_user where id=?", userId);
        if (mapList.isEmpty()) {
            return null;
        } else {
            return mapList.get(0);
        }
    }

    /**
     * 好友查询
     *
     * @param userId
     * @param friendId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.userFriendCacheMap, key = "#userId+':'+#friendId", unless = " #result==null or #result.size()==0")
    public Map<String, Object> findByUserIdAndFriendId(long userId, long friendId) {
        List<Map<String, Object>> mapList = jdbc.queryForList("select id,isBlack,is_ephemeral_chat,ephemeral_chat_time,is_show_session_message from im_friend where userId=? and friendId=? and isFriend=1", userId, friendId);
        if (mapList.isEmpty()) {
            return null;
        } else {
            return mapList.get(0);
        }
    }

    /**
     * 私密好友查询
     *
     * @param destId
     * @param fromId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.userSecretFriendCacheMap, key = "#destId+':'+#fromId", unless = " #result==null or #result.size()==0")
    public Map<String, Object> findBySecretFriend(long destId, long fromId) {
        List<Map<String, Object>> mapList = jdbc.queryForList("select id from im_friend where userId=? and friendId=? and is_secret_friend=1", destId, fromId);
        if (mapList.isEmpty()) {
            return null;
        } else {
            return mapList.get(0);
        }
    }


}
