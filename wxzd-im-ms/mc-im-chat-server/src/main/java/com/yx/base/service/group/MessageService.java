package com.yx.base.service.group;

import com.yx.im.model.ImMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    private JdbcTemplate jdbc;


    //添加目标消息
    public void saveDestMessageHistory(ImMessage message) {
        jdbc.execute("set names utf8mb4");
        jdbc.update(
                "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,destId,content,messageType,sendTime)  values(?,?,?,?,?,?,?,?,?)",
                message.getDestId(), message.getDevType(), message.getMsgId(), message.getFromId(), message.getFromType(),
                message.getDestId(), message.getContent(), message.getMessageType(), message.getSendTime());
    }

    //添加发送人消息
    public void saveFromMessageHistory(ImMessage message) {
        jdbc.execute("set names utf8mb4");
        jdbc.update(
                "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,destId,content,messageType,sendTime)  values(?,?,?,?,?,?,?,?,?)",
                message.getFromId(), message.getDevType(), message.getMsgId(), message.getFromId(), message.getFromType(),
                message.getDestId(), message.getContent(), message.getMessageType(), message.getSendTime());
    }


    /**
     * 批量保存群的消息
     *
     * @param fromValues
     * @param destValues
     */
    public void batchGroupMessage(List<Object[]> fromValues, List<Object[]> destValues) {
        jdbc.execute("set names utf8mb4");
        jdbc.batchUpdate(
                "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,destId,content,messageType,sendTime)  values(?,?,?,?,?,?,?,?,?)",
                fromValues);
        jdbc.batchUpdate(
                "insert into im_message_history(belongUserId,devType,msgId,fromId,fromType,destId,content,messageType,sendTime)  values(?,?,?,?,?,?,?,?,?)",
                destValues);
    }
}


