package com.yx.base.service.group;

import com.yx.im.constants.CacheConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class GroupService {

    @Autowired
    private JdbcTemplate jdbc;

    /**
     * 获取群组信息
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.groupCacheMap, key = "#groupId", unless = " #result==null or #result.size()==0")
    public Map<String, Object> getGroupById(long groupId) {
        List<Map<String, Object>> mapList = jdbc.queryForList("select name,isBanned,group_status,is_anonymous from im_group where id=?", groupId);
        if (mapList.isEmpty()) {
            return null;
        } else {
            return mapList.get(0);
        }
    }

    /**
     * 查询全部群成员
     *
     * @param groupId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.memberListCacheMap, key = "#groupId", unless = " #result==null or #result.size()==0")
    public List<Map<String, Object>> getAllMemberFromCache(long groupId) {
        return jdbc.queryForList("select userId,is_show_session_message from im_group_member where groupId=? and isAccept=1",groupId);
    }

    public boolean isGroupAdmin(Map<String, Object> groupMember) {
        return !groupMember.get("role").equals(3);
    }

    public boolean isMemberBanned(Map<String, Object> groupMember) {
        return System.currentTimeMillis() <= (long) groupMember.get("isBanned");
    }

    /**
     * 根据用户ID
     *
     * @param groupId
     * @param userId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.memberCacheMap, key = "#userId+':'+#groupId", unless = " #result==null or #result.size()==0")
    public Map<String, Object> getGroupMemberId(long userId, long groupId) {
        List<Map<String, Object>> mapList = jdbc.queryForList("select isBanned,role,isAnonymous,anonymousHeaderUrl,anonymousName from im_group_member where userId=? and groupId=?", userId, groupId);
        if (mapList.isEmpty()) {
            return null;
        } else {
            return mapList.get(0);
        }
    }

    /**
     * 获取非私密群聊
     *
     * @param userId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.noSecretMemberListCacheMap, key = "#userId", unless = " #result==null or #result.size()==0")
    public List<Map<String, Object>> getNoSecretGroup(long userId) {
        return jdbc.queryForList("select groupId from im_group_member where userId=? and isAccept=1 and is_secret_group=0  group by groupId", userId);
    }

    /**
     * 获取全部群聊
     *
     * @param userId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.allMemberListCacheMap, key = "#userId", unless = " #result==null or #result.size()==0")
    public List<Map<String, Object>> getAllGroup(long userId) {
        return jdbc.queryForList("select groupId from im_group_member where userId=? and isAccept=1  group by groupId", userId);
    }

    /**
     * 查询私密群聊
     *
     * @param userId
     * @param destId
     * @return
     */
    @Cacheable(cacheNames = CacheConstants.secretMemberCacheMap, key = "#userId+':'+#destId", unless = " #result==null or #result.size()==0")
    public Map<String, Object> getSecretGroupMember(long userId, long destId) {
        List<Map<String, Object>> mapList = jdbc.queryForList("select id from im_group_member where userId=? and groupId=? and is_secret_group=1", userId, destId);
        if (mapList.isEmpty()) {
            return null;
        } else {
            return mapList.get(0);
        }
    }

}
