package com.yx.base.service.chat;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ConcurrentHashSet;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.corundumstudio.socketio.AckCallback;
import com.corundumstudio.socketio.BroadcastAckCallback;
import com.corundumstudio.socketio.SocketIOClient;
import com.yx.base.service.group.GroupService;
import com.yx.base.service.group.UserService;
import com.yx.im.constants.IMConstants;
import com.yx.im.model.*;
import com.yx.im.ms.socketio.SocketIOServiceImpl;
import com.yx.im.service.MsgStoreAndNotify;
import com.yx.im.service.UserRedisMap;
import com.yx.im.thread.UMentMsgRunner;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.yx.im.constants.StaticConstant.*;

@Service
public class ChatService {
    @Autowired
    private ClientMapService clientService;
    @Autowired
    private UserRedisMap userRedisMap;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private MsgStoreAndNotify msgNotify;
    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private UMentMsgRunner uMentMsgRunner;

    private ExecutorService cachedThreadPool = Executors.newFixedThreadPool(1000);

    public void joinRoom(List<String> userIds, long roomId) {
        for (String userId : userIds) {
            Map<Integer, SocketIOClient> clients = clientService.getClients(Long.parseLong(userId));
            for (SocketIOClient client : clients.values()) {
                client.joinRoom("room_" + roomId);
            }
        }
    }

    public void joinRoomOne(String userId, long roomId) {
        Map<Integer, SocketIOClient> clients = clientService.getClients(Long.parseLong(userId));
        for (SocketIOClient client : clients.values()) {
            client.joinRoom("room_" + roomId);
        }
    }

    public void leaveRoom(Long userId, long roomId) {
        Map<Integer, SocketIOClient> clients = clientService.getClients(userId);
        for (SocketIOClient client : clients.values()) {
            client.leaveRoom("room_" + roomId);
        }
    }

    public void leaveRooms(Long userId, List<String> roomIds) {
        Map<Integer, SocketIOClient> clients = clientService.getClients(userId);
        for (SocketIOClient client : clients.values()) {
            for (String roomId : roomIds) {
                client.leaveRoom("room_" + roomId);
            }
        }
    }

    public void leaveRoomUserIds(List<String> userIds, long roomId) {
        for (String userId : userIds) {
            Map<Integer, SocketIOClient> clients = clientService.getClients(Long.parseLong(userId));
            for (SocketIOClient client : clients.values()) {
                client.leaveRoom("room_" + roomId);
            }
        }
    }

    public void joinClientRoom(long userId, SocketIOClient client) {
        List<Map<String, Object>> list = groupService.getAllGroup(userId);
        if (!list.isEmpty()) {
            for (Map<String, Object> group : list) {
                if (!group.isEmpty()) {
                    client.joinRoom("room_" + group.get("groupId").toString());
                }
            }
        }
    }

    public void joinSecretClientRoom(long userId, SocketIOClient client) {
        List<Map<String, Object>> list = groupService.getNoSecretGroup(userId);
        if (!list.isEmpty()) {
            for (Map<String, Object> group : list) {
                if (!group.isEmpty()) {
                    client.joinRoom("room_" + group.get("groupId").toString());
                }
            }
        }

    }

    /**
     * 推送消息
     */
//    public void pushMessage(long userId) {
//        Map<Integer, SocketIOClient> cols = clientService.getClients(userId);
//        for (int deviceId : cols.keySet()) {
//            try {
//                SocketIOClient client = ClientMapService.clientMap.get(userId, deviceId);
//                if (client == null) {
//                    ClientMapService.clientMap.remove(userId, deviceId);
//                    continue;
//                }
//                sendAndRemove(userId, deviceId, client);
//            } catch (Exception e) {
//                e.printStackTrace();
//                StaticLog.info("推送消息时，取不到client对象");
//            }
//        }
//    }

    /**
     * 推送群广播消息
     */
    public void pushGroupMessage(ImMessage data) {
        //群广播群消息
        SocketIOServiceImpl.server.getRoomOperations("room_" + data.getDestId()).sendEvent(IMConstants.IM_EVENT_CHAT, data,
                new BroadcastAckCallback<String>(String.class, 5000) {

                    public void onClientTimeout(SocketIOClient client) {
                        // 清理内存
                        clientService.removeClient(client);
                        client.disconnect();
                        StaticLog.info("超时的用户可能已经断开");
                    }

                });
    }

    /**
     * 推送群广播消息（过滤自己的）
     */
    private void pushGroupMessageExcludedClient(ImMessage data, SocketIOClient client) {
        //群广播群消息
        SocketIOServiceImpl.server.getRoomOperations("room_" + data.getDestId()).sendEvent(IMConstants.IM_EVENT_CHAT, data, client,
                new BroadcastAckCallback<String>(String.class, 5000) {

                    public void onClientTimeout(SocketIOClient client) {
                        // 清理内存
                        clientService.removeClient(client);
                        client.disconnect();
                        StaticLog.info("超时的用户可能已经断开");
                    }

                });
    }

    /**
     * 获取在线消息
     *
     * @param userId   用户id
     * @param deviceId 设备
     * @return List<String>
     */
//    private List<String> getOnlineMessage(long userId, int deviceId) {
//        // 在线消息
//        String key = "msg_" + userId + "_" + deviceId;
//        long start = 0;
//        long end = -1;
//        return RedisUtil.lrange(key, start, end);
//    }

    /**
     * 推送离线消息
     *
     * @param key
     * @param userId
     */
//    public void sendOnlineAndOffLineMessage(String key, Long userId) {
//        long start = 0;
//        long end = -1;
//        List<String> list = RedisUtil.lrange(key, start, end);
//        for (String it : list) {
//            ImMessage msg = JSON.parseObject(it, ImMessage.class);
//            msgNotify.storeMsgAndNotifyImServer(msg, userId);
//            RedisUtil.rpop(key);
//        }
//    }

    /**
     * 推送消息--子方法，并且成功后移除redis消息
     *
     * @param userId 用户id
     * @param client 客服端
     */
//    private void sendAndRemove(long userId, int deviceId, SocketIOClient client) {
//
//        String key2 = "msg_" + userId + "_" + deviceId;
//        List<String> list2 = getOnlineMessage(userId, deviceId);
//        sendAndRemoveCommon(list2, userId, deviceId, key2, client);
//
//    }


//    private void sendAndRemoveCommon(List<String> list, long userId, int deviceId, String key, SocketIOClient client) {
//        if (list != null) {
//            for (int i = list.size() - 1; i >= 0; i--) {
//                ImMessage msg = JSON.parseObject(list.get(i), ImMessage.class);
//                StaticLog.info(userId + "取消息出队列，开始推送消息，需要消息确认_" + deviceId + JSONObject.toJSONString(msg));
//                //弹出消息
//                RedisUtil.rpop(key);
//
//                client.sendEvent(IMConstants.IM_EVENT_CHAT, new AckCallback<String>(String.class, 5000) {
//                    @Override
//                    public void onSuccess(String result) {
////                        StaticLog.info("确认收到消息msgId:" + result);
//                    }
//
//                    @Override
//                    public void onTimeout() {
//                        StaticLog.info("推送消息超时18。。。重新放回队列" + userId);
//                        RedisUtil.rpush(key, JSON.toJSONString(msg));
//                        // 清理redis在线设备
//                        userRedisMap.removeUserIpFromRedis(userId, deviceId);
//                        // 清理内存
//                        clientService.removeClient(client);
//
//                        client.disconnect();
//                        StaticLog.info(userId + "超时的用户可能已经断开");
//                    }
//                }, msg);
//
//            }
//        }
//    }


    /**
     * 推送单聊消息
     */
    public void pushSingleMessage(ImMessage data, long userId, int device) {
        try {
            SocketIOClient client = ClientMapService.clientMap.get(userId, device);
            if (client == null) {
                ClientMapService.clientMap.remove(userId, device);
                return;
            }
            client.sendEvent(IMConstants.IM_EVENT_CHAT, new AckCallback<String>(String.class, 5000) {
                @Override
                public void onSuccess(String result) {
                }

                @Override
                public void onTimeout() {
                    // 清理redis在线设备
                    userRedisMap.removeUserIpFromRedis(userId, device);
                    // 清理内存
                    clientService.removeClient(client);
                    client.disconnect();
                    StaticLog.info(userId + "超时的用户可能已经断开");
                }
            }, data);
        } catch (Exception e) {
            e.printStackTrace();
            StaticLog.info("推送消息时，取不到client对象");
        }
    }

    /**
     * 处理websocket收到的消息
     *
     * @param client 客户端
     * @param data   接收的聊天数据
     */
    public void handleRecMessage(SocketIOClient client, ImMessage data) {
        //添加到kafaka
        if (data != null) {

            long time = System.currentTimeMillis();

            data.setSendTime(time);
            data.setReceiveTime(time);


            JSONObject json = new JSONObject();
            //额外字段（阅后即焚字段）
            int isEphemeralChat = 0;
            int ephemeralChatTime = 0;
            long expireTime = 0;

            ImMessage memberMessage = new ImMessage();


            //标记未读
            if(IMConstants.MSG_TYPE_UNREAD==data.getMessageType()){
                //同步自己其他设备
                msgNotify.synchronOtherDev(data,data.getFromId(),data.getDevType());
                //发送数据到卡夫卡
                kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(data), StandardCharsets.ISO_8859_1));
                return;
            }

            Map<String, Object> user = userService.getUserById(data.getFromId());
            if (user != null && StringUtils.isNotEmpty(user.get("isBanned").toString())) {
                if (time - Long.valueOf(user.get("isBanned").toString()) <= 0) {
                    //告诉用户已经被系统禁言
                    data.setMessageType(IMConstants.MSG_TYPE_IS_BANNED);
                    data.setContent("系统禁言");
                    msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    return;
                }
            }

            if (data.getFromType() == IMConstants.MSG_FROM_P2P) {
                long toId = data.getDestId();
                long fromId = data.getFromId();

                Map<String, Object> imFriend = userService.findByUserIdAndFriendId(toId, fromId);

                if (ArrayUtils.contains(MsgStoreAndNotify.singleTypeCheck, data.getMessageType())) {
                    //是否在对方黑名单
                    if (imFriend != null && ImFriend.isblack.equals(imFriend.get("isBlack").toString())) {
                        StaticLog.info("是黑名单");
                        data.setMessageType(IMConstants.MSG_TYPE_IS_BLACK);
                        data.setContent("你的消息已被对方拒收");
                        msgNotify.storeMsgAndNotifyImServer(data, fromId);
                        return;
                    }

                    //是否是好友
                    if (imFriend == null) {
                        data.setMessageType(IMConstants.MSG_TYPE_NOT_FRIEND);
                        data.setContent("你不在对方的好友列表，可能已经把你移除");
                        msgNotify.storeMsgAndNotifyImServer(data, fromId);
                        return;
                    }

                    //删除好友发送消息的判断
                    Map<String, Object> imFriend1 = userService.findByUserIdAndFriendId(fromId, toId);
                    if (imFriend1 == null) {
                        data.setMessageType(IMConstants.MSG_TYPE_NOT_FRIEND);
                        data.setContent("你已经删除好友，无法发送消息");
                        msgNotify.storeMsgAndNotifyImServer(data, fromId);
                        return;
                    }

                }

                //获取阅后即焚时间
                if (imFriend != null) {
                    isEphemeralChat = Integer.valueOf(imFriend.get("is_ephemeral_chat").toString());
                    ephemeralChatTime = Integer.valueOf(imFriend.get("ephemeral_chat_time").toString());
                    //将到期时间算好返给前端
                    expireTime = System.currentTimeMillis() / 1000 + ephemeralChatTime;
                }


                json.put("ephemeralChatTime", ephemeralChatTime);
                json.put("isEphemeralChat", isEphemeralChat);
                json.put("expireTime", expireTime);
                //封装阅后即焚的时间给前端

            } else if (data.getFromType() == IMConstants.MSG_FROM_GROUP) {

                Map<String, Object> groupMember = groupService.getGroupMemberId(data.getFromId(), data.getDestId());
                if (groupMember == null) {
                    StaticLog.info("你已经不再群内");
                    data.setMessageType(IMConstants.MSG_TYPE_REFUSE_MSG);
                    data.setContent("你已经不再群内，消息发送失败");
                    msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    return;
                }

                Map<String, Object> groupMap = groupService.getGroupById(data.getDestId());
                boolean isMyBanned = groupService.isMemberBanned(groupMember);

                if (groupMap != null && (groupMap.get("isBanned").equals(ImGroup.groupIsBanned) || isMyBanned) && !groupService.isGroupAdmin(groupMember) && IMConstants.MSG_TYPE_READED != data.getMessageType()) {
                    StaticLog.info("群禁言或者群内被禁言");
                    data.setMessageType(IMConstants.MSG_TYPE_REFUSE_MSG);
                    data.setContent("已经禁言，消息发送失败");
                    msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    return;
                }


                if (groupMap != null && groupMap.get("group_status").equals(ImGroup.group_freeze) && IMConstants.MSG_TYPE_READED != data.getMessageType()) {
                    StaticLog.info("群被冻结");
                    data.setMessageType(IMConstants.MSG_TYPE_REFUSE_MSG);
                    data.setContent("群已冻结，消息发送失败");
                    msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    return;
                }

                if (groupMap != null && groupMap.get("group_status").equals(ImGroup.group_dissolve) && IMConstants.MSG_TYPE_READED != data.getMessageType()) {
                    StaticLog.info("群已解散");
                    data.setMessageType(IMConstants.MSG_TYPE_REFUSE_MSG);
                    data.setContent("群已解散，消息发送失败");
                    msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    return;
                }

                BeanUtils.copyProperties(data, memberMessage);

                //修改匿名数据
                if (groupMap != null && groupMap.get("is_anonymous").equals(ImGroup.isAnonymousYes) && groupMember.get("isAnonymous").equals(ImGroupMember.isAnonymousYes)) {

                    //发送给其他群成员的数据
                    memberMessage.setFromId(0L);
                    memberMessage.setFromName((String) groupMember.get("anonymousName"));
                    memberMessage.setImageIconUrl((String) groupMember.get("anonymousHeaderUrl"));
                    //添加为匿名数据
                    json.put("anonymousData", "1");
                    data.setImageIconUrl((String) groupMember.get("anonymousHeaderUrl"));
                    data.setFromName((String) groupMember.get("anonymousName"));
                }
            }

            data.setExt(JSONObject.toJSONString(json));

            //如果是撤回消息，删除聊天记录
            if (data.getMessageType() == IMConstants.MSG_TYPE_REBACK) {
                JSONObject msgContent = JSON.parseObject(data.getContent());
                StaticLog.info("撤回的ID=" + msgContent.get("msgid"));
                if (data.getFromType() == IMConstants.MSG_FROM_P2P) {
                    ImMessageHistoryMong messageHistoryMong = mongoTemplate.findOne(Query.query(Criteria.where("msgId").is(msgContent.get("msgid")).and("fromId").is(data.getFromId()).and("destId").is(data.getDestId())), ImMessageHistoryMong.class);
                    if (messageHistoryMong != null) {
                        //通知用户消息撤回
                        msgNotify.storeMsgAndNotifyImServer(data, data.getDestId());
                        //同步自己其他设备
                        msgNotify.synchronOtherDev(data,data.getFromId(),data.getDevType());
                    } else {
                        StaticLog.info("撤回失败，不能撤回别人的消息");
                        data.setMessageType(IMConstants.MSG_TYPE_REFUSE_MSG);
                        data.setContent("撤回失败，不能撤回别人的消息");
                        msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    }
                } else if (data.getFromType() == IMConstants.MSG_FROM_GROUP) {
                    ImMessageHistoryMong messageHistoryMong = mongoTemplate.findOne(Query.query(Criteria.where("msgId").is(msgContent.get("msgid")).and("fromId").is(data.getFromId()).and("destId").is(data.getDestId())), ImMessageHistoryMong.class);
                    if (messageHistoryMong != null) {
                        //群广播群消息
                        pushGroupMessageExcludedClient(data, client);
                        //广播到其他服务
                        msgNotify.notifyGroupImServerExcludeFromId(data);
                    } else {
                        StaticLog.info("撤回失败，不能撤回别人的消息");
                        data.setMessageType(IMConstants.MSG_TYPE_REFUSE_MSG);
                        data.setContent("撤回失败，不能撤回别人的消息");
                        msgNotify.storeMsgAndNotifyImServer(data, data.getFromId());
                    }
                }
                //发送数据到卡夫卡
                kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(data), StandardCharsets.ISO_8859_1));
                return;
            }

            if (data.getMessageType() == 1) {
                StaticLog.info("又发了一次上线，取消ready,用户：" + data.getFromId() + "上线");
            } else if (data.getFromType() == IMConstants.MSG_FROM_P2P) {

                //表示用户读取了信息，更新记录
//                if (data.getMessageType() == IMConstants.MSG_TYPE_READED) {
//                    //发送数据到卡夫卡
//                    kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(data), StandardCharsets.ISO_8859_1));
//                    return;
//                }

                //单聊同步其他设备
                int deviceId = clientService.getDeviceId(client);

                // 同步消息到我的其它设备
                msgNotify.synchronOtherDev(data, data.getFromId(), deviceId);

                long toId = data.getDestId();

                //通知用户所在服务器
                msgNotify.storeMsgAndNotifyImServer(data, toId);
            } else if (data.getFromType() == IMConstants.MSG_FROM_GROUP) {

                //表示用户读取了信息，更新记录
                if (data.getMessageType() == IMConstants.MSG_TYPE_READED) {
                    //发送数据到卡夫卡
                    kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(data), StandardCharsets.ISO_8859_1));
                    return;
                }
                long startTime = System.currentTimeMillis();
                //群广播群消息
                pushGroupMessageExcludedClient(memberMessage, client);
                //广播到其他服务
                msgNotify.notifyGroupImServerExcludeFromId(memberMessage);
                String fromName = data.getFromName();
                int messageType = data.getMessageType();
                //会话显示
                String content=data.getContent();
                //会话不显示
                String content1="您有未读消息";
                cachedThreadPool.execute(() -> {
                    try {
                        List<Map<String, Object>> list = groupService.getAllMemberFromCache(data.getDestId());
                        //声音和震动会话显示
                        Set<String> send1 = new ConcurrentHashSet<>();
                        //声音和震动会话不显示
                        Set<String> send11 = new ConcurrentHashSet<>();
                        //有声音没震动会话显示
                        Set<String> send2 = new ConcurrentHashSet<>();
                        //有声音没震动会话不显示
                        Set<String> send22 = new ConcurrentHashSet<>();
                        //没声音有震动会话显示
                        Set<String> send3 = new ConcurrentHashSet<>();
                        //没声音有震动会话不显示
                        Set<String> send33 = new ConcurrentHashSet<>();
                        //没声音没震动会话显示
                        Set<String> send4 = new ConcurrentHashSet<>();
                        //没声音没震动会话不显示
                        Set<String> send44 = new ConcurrentHashSet<>();
                        Map<String, Object> group = groupService.getGroupById(data.getDestId());
                        String groupName = (String) group.get("name");
                        if (ArrayUtil.contains(MsgStoreAndNotify.jiguangNoticeType, data.getMessageType())) {
                            for (Map<String, Object> member : list) {
                                long uId = Long.parseLong(member.get("userId").toString());
                                //判断群成员是否在线
                                if (ClientMapService.clientMap.row(uId).isEmpty()) {
                                    //判断群成员是否开启推送提示
                                    Map<String, Object> memberUser = userService.getUserById(uId);
                                    if (ImFriend.newMessageNotificationClose.equals(memberUser.get("is_new_group_message_notification"))) {
                                        //如果用户的群消息消息提示没有开启的不进行推送
                                        continue;
                                    }
                                    //添加推送判断是否开启声音和震动
                                    addSendUserId(member,memberUser,send1,send11,send2,send22,send3,send33,send4,send44);
                                }
                            }
                        }

                        if(!send1.isEmpty()){
                            //友盟推送安卓和IOS
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send1, ","), groupName, fromName, content, messageType,true,true);
                        }
                        if(!send11.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send11, ","), groupName, fromName, content1, messageType,true,true);
                        }
                        if(!send2.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send2, ","), groupName, fromName, content, messageType,true,false);
                        }
                        if(!send22.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send22, ","), groupName, fromName, content1, messageType,true,false);
                        }
                        if(!send3.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send3, ","), groupName, fromName, content, messageType,false,true);
                        }
                        if(!send33.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send33, ","), groupName, fromName, content1, messageType,false,true);
                        }
                        if(!send4.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send4, ","), groupName, fromName, content, messageType,false,false);
                        }
                        if(!send44.isEmpty()){
                            uMentMsgRunner.sendGroupUment(CollUtil.join(send44, ","), groupName, fromName, content1, messageType,false,false);
                        }

                    } catch (Exception e) {
                        StaticLog.error(e);
                    }
                });

                StaticLog.info("for1消费时间：" + (System.currentTimeMillis() - startTime));
            }

            //发送数据到卡夫卡
            kafkaTemplate.send(Constants.KAFKA_IM_TOPIC, new String(GenerateKafkaData.sendKafkaMsgProtos(data), StandardCharsets.ISO_8859_1));
        }
    }

    /**
     * 添加推送信息
     * @param member
     * @param memberUser
     * @param send1
     * @param send11
     * @param send2
     * @param send22
     * @param send3
     * @param send33
     * @param send4
     * @param send44
     */
    private void addSendUserId(Map<String, Object> member,Map<String, Object> memberUser,Set<String> send1,Set<String> send11,Set<String> send2,Set<String> send22,Set<String> send3,Set<String> send33,Set<String> send4,Set<String> send44){
        //判断全局会话显示如果全局不显示或者（判断全局会话显示如果全局是显示并且如果单独的是不显示）
        if(isShowSessionMessageNo.equals(memberUser.get("is_show_group_session_message"))||(isShowSessionMessageYes.equals(memberUser.get("is_show_group_session_message"))&&isShowSessionMessageNo.equals(member.get("is_show_session_message")))){
            commCheck(member.get("userId").toString(), memberUser, send11, send22, voiceNotifyOpen);
            commCheck(member.get("userId").toString(), memberUser, send33, send44, voiceNotifyClose);
        }else {
            commCheck(member.get("userId").toString(), memberUser, send1, send2, voiceNotifyOpen);
            commCheck(member.get("userId").toString(), memberUser, send3, send4, voiceNotifyClose);
        }
    }

    private void commCheck(String userId, Map<String, Object> memberUser, Set<String> send1, Set<String> send2, Integer voiceNotifyOpen) {
        if(voiceNotifyOpen.equals(memberUser.get("is_voice_reminder"))&& vibrationNotifyOpen.equals(memberUser.get("is_vibration_reminder"))){
            send1.add(userId);
        }
        if(voiceNotifyOpen.equals(memberUser.get("is_voice_reminder"))&& vibrationNotifyClose.equals(memberUser.get("is_vibration_reminder"))){
            send2.add(userId);
        }
    }

}
