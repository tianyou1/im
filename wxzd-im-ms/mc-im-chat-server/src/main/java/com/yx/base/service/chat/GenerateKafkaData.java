package com.yx.base.service.chat;

import com.yx.im.model.ImMessage;
import com.yx.im.model.MsgProtos;
import org.apache.commons.lang.StringUtils;

public class GenerateKafkaData {


    public static byte[] sendKafkaMsgProtos(ImMessage data) {
        MsgProtos.testBuf.Builder builder = MsgProtos.testBuf.newBuilder();
        if (data.getId() == null) {
            builder.setId(0);
        } else {
            builder.setId(data.getId());
        }
        builder.setDestId(data.getDestId());
        if(data.getDevType()==null){
            builder.setDevType(0);
        }else {
            builder.setDevType(data.getDevType());
        }
        builder.setFromId(data.getFromId());
        builder.setFromType(data.getFromType());
        if (data.getGeoId() == null) {
            builder.setGeoId(0);
        } else {
            builder.setGeoId(data.getGeoId());
        }
        builder.setMessageType(data.getMessageType());
        if(data.getReceiveTime()==null){
            builder.setReceiveTime(System.currentTimeMillis());
        }else {
            builder.setReceiveTime(data.getReceiveTime());
        }
        builder.setSendTime(data.getSendTime());
        if (data.getStatus() == null) {
            builder.setStatus(0);
        } else {
            builder.setStatus(data.getStatus());
        }
        if (data.getVersion() == null) {
            builder.setVersion(0);
        } else {
            builder.setVersion(data.getVersion());
        }
        if (StringUtils.isNotEmpty(data.getContent())) {
            builder.setContent(data.getContent());
        } else {
            builder.setContent("");
        }
        if(StringUtils.isBlank(data.getFromName())){
            builder.setFromName("");
        }else {
            builder.setFromName(data.getFromName());
        }
        if(StringUtils.isBlank(data.getImageIconUrl())){
            builder.setImageIconUrl("");
        }else {
            builder.setImageIconUrl(data.getImageIconUrl());
        }
        builder.setMsgId(data.getMsgId());
        if (StringUtils.isNotEmpty(data.getExt())) {
            builder.setExt(data.getExt());
        }
        if (data.getSystemType() == null) {
            builder.setSystemType(0);
        } else {
            builder.setSystemType(data.getSystemType());
        }
        if(StringUtils.isBlank(data.getExt())){
            builder.setExt("");
        }else {
            builder.setExt(data.getExt());
        }
        if(data.getVersion()==null){
            builder.setVersion(0);
        }else {
            builder.setVersion(data.getVersion());
        }
        MsgProtos.testBuf info = builder.build();
        return info.toByteArray();
    }
}
