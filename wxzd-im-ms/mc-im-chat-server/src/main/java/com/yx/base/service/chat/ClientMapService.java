package com.yx.base.service.chat;

import cn.hutool.log.StaticLog;
import com.corundumstudio.socketio.SocketIOClient;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import com.wxzd.im.ms.utils.RedisUtil;
import com.yx.im.model.Constants;
import com.yx.im.service.UserRedisMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ClientMapService {

    @Autowired
    private UserRedisMap userRedisMap;

    public final static Table<Long, Integer, SocketIOClient> clientMap = HashBasedTable.create();

    public Map<Integer, SocketIOClient> getClients(long userId) {
        Map<Integer, SocketIOClient> list = new HashMap<Integer, SocketIOClient>();
        if (clientMap.containsRow(userId)) {
            list = clientMap.row(userId);
        }
        return list;
    }

    public void addClient(final long userId, final int device, String ip, SocketIOClient client) {

        userRedisMap.addUserIpToRedis(userId, device, ip);
        if (userId < 0 || client == null) {
            StaticLog.info("添加 到client map时参数不对=" + userId + "," + client);
            return;
        }

        ClientMapService.clientMap.put(userId, device, client);

    }


    public void removeClient(SocketIOClient client) {
        ImmutableList<Long> rows = ImmutableList.copyOf(ClientMapService.clientMap.rowKeySet());
        ImmutableList<Integer> cols = ImmutableList.copyOf(ClientMapService.clientMap.columnKeySet());
        for (long userId : rows) {
            for (int deviceId : cols) {
                SocketIOClient temp = ClientMapService.clientMap.get(userId, deviceId);
                if (temp != null && temp.equals(client)) {
                    // 清理redis的在线用户
                    userRedisMap.removeUserIpFromRedis(userId, deviceId);
                    // 清理内存列表
                    ClientMapService.clientMap.remove(userId, deviceId);
                    return;
                }
            }
        }
        rows = null;
        cols = null;
    }

    public int getDeviceId(SocketIOClient client) {
        ImmutableList<Integer> cols = ImmutableList.copyOf(ClientMapService.clientMap.columnKeySet());
        ImmutableList<Long> rows = ImmutableList.copyOf(ClientMapService.clientMap.rowKeySet());
        for (long userId : rows) {
            for (int deviceId : cols) {
                SocketIOClient temp = ClientMapService.clientMap.get(userId, deviceId);
                if (temp != null && temp.equals(client)) {
                    return deviceId;
                }
            }
        }
        rows = null;
        cols = null;
        return -1;
    }

    public long getUserId(SocketIOClient client) {
        ImmutableList<Integer> cols = ImmutableList.copyOf(ClientMapService.clientMap.columnKeySet());
        ImmutableList<Long> rows = ImmutableList.copyOf(ClientMapService.clientMap.rowKeySet());
        for (long userId : rows) {
            for (int deviceId : cols) {
                SocketIOClient temp = ClientMapService.clientMap.get(userId, deviceId);
                if (temp != null && temp.equals(client)) {
                    return userId;
                }
            }
        }
        rows = null;
        cols = null;
        return 0l;
    }

    /**
     * 获取其它在线的终端
     *
     * @param userId
     * @param client
     * @return
     */
    public List<Integer> getOtherClient(long userId, SocketIOClient client) {
        List<Integer> clients = new ArrayList<Integer>();

        int currentDevId = this.getDeviceId(client);
        Map<String, String> devMap = RedisUtil.hgetAll(Constants.USER_IP_KEY + userId);
        for (String dev : devMap.keySet()) {
            int val = Integer.parseInt(dev);
            if (currentDevId != val) {
                clients.add(val);
            }
        }
        return clients;
    }

}
