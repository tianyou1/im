package com.wxzd.im.ms.basefile.service;

import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.basefile.entity.BaseFileVO;

public interface BaseFileService {
    int deleteByPrimaryKey(String id);

    int insert(BaseFile record);

    int insertSelective(BaseFile record);

    BaseFile selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseFile record);

    int updateByPrimaryKey(BaseFile record);

    /**
     * @Title: queryFileByMd5
     * @Description: 根据文件MD5值查询文件
     * @param md5
     * @return BaseFile 返回类型
     * @throws
     */
    BaseFile queryFileByMd5(String md5);

    BaseFileVO selectById(String id);
}