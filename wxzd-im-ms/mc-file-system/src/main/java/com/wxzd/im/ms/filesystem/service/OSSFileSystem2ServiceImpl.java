package com.wxzd.im.ms.filesystem.service;

import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.basefile.service.BaseFileService;
import com.wxzd.im.ms.filesystem.config.FileConfigProperties;
import com.wxzd.im.ms.filesystem.config.FileOSSConfigProperties;
import com.wxzd.im.ms.filesystem.utils.FileSystemUtil;
import com.wxzd.im.ms.filesystem.utils.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
@Component
public class OSSFileSystem2ServiceImpl extends  OSSFileSystemServiceImpl{

    @Autowired
    FileOSSConfigProperties configProperties; //注入的对象

    @Autowired
    BaseFileService baseFileService;

    @Autowired
    FileConfigProperties fileConfigProperties;

    private static BaseFileService fileService;
    private static FileOSSConfigProperties fileOSSConfig;  //静态对象

    @PostConstruct
    public void init(){
        fileOSSConfig = this.configProperties;  //将注入的对象交给静态对象管理
        fileService=this.baseFileService;
    }

    @Override
    public void show(HttpServletResponse response , String fileId) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        FileSystemUtil.sentOss(fileEntity,fileOSSConfig.getDir(),response);
    }

    @Override
    public ResultBean download(String fileId , HttpServletResponse response) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        FileSystemUtil.sentOss(fileEntity,fileOSSConfig.getDir(),response);
        return null;
    }



    @Override
    public void playVideo(HttpServletResponse response, HttpServletRequest request, String fileId) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        FileSystemUtil.sentOss(fileEntity,fileOSSConfig.getDir(),response);
    }

}
