package com.wxzd.im.ms.basefile.mapper;

import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.basefile.entity.BaseFileVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BaseFileMapper {
    int deleteByPrimaryKey(String id);

    int insert(BaseFile record);

    int insertSelective(BaseFile record);

    BaseFile selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseFile record);

    int updateByPrimaryKey(BaseFile record);

    List<BaseFile> findByMd5(String md5);

    BaseFileVO selectById(String id);
}