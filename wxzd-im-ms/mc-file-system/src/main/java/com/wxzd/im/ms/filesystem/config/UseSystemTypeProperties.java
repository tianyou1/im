package com.wxzd.im.ms.filesystem.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取当前使用的文件系统类别
 */
@ConfigurationProperties(prefix="use-system")
@Component
public class UseSystemTypeProperties {
    /**
     * 启用文件系统的类别
     */
    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
