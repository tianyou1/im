package com.wxzd.im.ms.basefile.entity;

public class BaseFileVO extends BaseFile {

    /**
     * 访问前缀
     */
    private String visitPrefix;

    public String getVisitPrefix() {
        return visitPrefix;
    }

    public void setVisitPrefix(String visitPrefix) {
        this.visitPrefix = visitPrefix;
    }
}
