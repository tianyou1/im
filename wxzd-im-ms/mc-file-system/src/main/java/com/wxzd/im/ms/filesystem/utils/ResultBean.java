package com.wxzd.im.ms.filesystem.utils;

import com.wxzd.im.ms.filesystem.enums.ResultStatusCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * @ClassName: ResultBean 
 * @Description: 结果集
 * @author cys 
 * @date 2019年2月28日 下午4:04:00
 * @param <T>
 */
@ApiModel(value = "ResultBean", description = "结果集Bean")
@Data
@NoArgsConstructor
public class ResultBean<T> implements Serializable{
	private static final long serialVersionUID = 556890045016272879L;
	/**
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	 */


	/**
	 * 状态码
	 */
	@ApiModelProperty(name = "code", value = "状态码", notes = "200--操作成功，500--操作失败")
	private int code = ResultStatusCode.SUCCESS.getErrorCode();
	
	/**
	 * 状态信息
	 */
	@ApiModelProperty(name = "message", value = "状态信息")
	private String message;
	
	/**
	 * 其他信息
	 */
	@ApiModelProperty(name = "obj", value = "结果值")
	private T obj;
	
	/**
	 * 创建一个新的实例 ResultBean
	 * @param
	 * @param message
	 * @param obj
	 */
	public ResultBean(int code, String message, T obj){
		this.code = code;
		this.message = message;
		this.obj = obj;
	}
	
	/**
	 * @Description: 成功构造函数
	 * @param @param obj    设定文件
	 */
	public ResultBean(T obj) {
		this.code = ResultStatusCode.SUCCESS.getErrorCode();
		this.message = ResultStatusCode.SUCCESS.getErrorMsg();
		this.obj = obj;
	}
	
	public ResultBean(ResultStatusCode sc, T object) {
		this.code = sc.getErrorCode();
		this.message = sc.getErrorMsg();
		this.obj = obj;
	}
	
	/**
	 * @Title: getErrorData 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param 设定文件 
	 * @return ResultData    返回类型 
	 * @throws
	 */
	public static ResultBean getErrorData(String errorMsg, Object... objs) {
		errorMsg = StringUtils.isEmpty(errorMsg)?ResultStatusCode.SYSTEM_ERR.getErrorMsg():errorMsg;
		Object o = objs != null && objs.length > 0?objs[0]:null;
		return new ResultBean(ResultStatusCode.SYSTEM_ERR.getErrorCode(), errorMsg, o);
	}
	
	/**
	 * @Title: success
	 * @Description: 操作成功
	 * @param message
	 * @param obj
	 * @return ResultBean 返回类型
	 * @throws
	 */
	public static ResultBean success(String message, Object... objs) {
		message = StringUtils.isEmpty(message)?ResultStatusCode.SUCCESS.getErrorMsg():message;
		Object o = objs != null && objs.length > 0?objs[0]:null;
		return new ResultBean(ResultStatusCode.SUCCESS.getErrorCode(), message, o);
	}
}