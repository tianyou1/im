package com.wxzd.im.ms.baselog.service;

import com.wxzd.im.ms.baselog.entity.BaseLog;

public interface BaseLogService {
    int deleteByPrimaryKey(String id);

    int insert(BaseLog record);

    int insertSelective(BaseLog record);

    BaseLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseLog record);

    int updateByPrimaryKey(BaseLog record);
}