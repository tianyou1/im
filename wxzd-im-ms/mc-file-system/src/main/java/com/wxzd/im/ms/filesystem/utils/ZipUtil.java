/**
 * @Title: ZipUtil.java
 * @Package com.jade.filesystem.utils
 * @Description: TODO(用一句话描述该文件做什么)
 * @author cys
 * @date 2019年7月29日
 * @version V1.0
 */
package com.wxzd.im.ms.filesystem.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @ClassName: ZipUtil
 * @Description: 打包工具类
 * @author cys
 * @date 2019年7月29日
 */
@Slf4j
public class ZipUtil {

	// 最大的流为100M
	public static final int MAX_BYTE = 100 * 1024 * 1024;
	
	/**
	 * @Title: zipFiles
	 * @Description: 打包文件列表
	 * @param files
	 * @param outputStream
	 * @throws IOException
	 * @throws ServletException
	 * @return void 返回类型
	 */
	public static void zipFiles(List<File> files, ZipOutputStream outputStream) {
		if (files == null || files.isEmpty()) {
			return;
		}
		for (File file : files) {
			zipFile(file, outputStream);
		}
	}

	public static void zipFile(File inputFile, ZipOutputStream outputstream) {
		FileInputStream inStream = null;
		BufferedInputStream bInStream = null;
		try {
			if (inputFile.exists() && inputFile.isFile()) {
				inStream = new FileInputStream(inputFile);
				bInStream = new BufferedInputStream(inStream);
				ZipEntry entry = new ZipEntry(inputFile.getName());
				outputstream.putNextEntry(entry);

				long streamTotal = 0; // 接受流的容量
				int streamNum = 0; // 流需要分开的数量
				int leaveByte = 0; // 文件剩下的字符数
				byte[] inOutbyte; // byte数组接受文件的数据

				streamTotal = bInStream.available(); // 通过available方法取得流的最大字符数
				streamNum = (int) Math.floor(streamTotal / MAX_BYTE); // 取得流文件需要分开的数量
				leaveByte = (int) streamTotal % MAX_BYTE; // 分开文件之后,剩余的数量
				if (streamNum > 0) {
					for (int j = 0; j < streamNum; ++j) {
						inOutbyte = new byte[MAX_BYTE];
						// 读入流,保存在byte数组
						bInStream.read(inOutbyte, 0, MAX_BYTE);
						outputstream.write(inOutbyte, 0, MAX_BYTE); // 写出流
					}
				}
				// 写出剩下的流数据
				inOutbyte = new byte[leaveByte];
				bInStream.read(inOutbyte, 0, leaveByte);
				outputstream.write(inOutbyte);
				outputstream.closeEntry();
				bInStream.close();
				inStream.close();
			}
		} catch (Exception ex) {
			log.error("文件打包失败", ex.getMessage());
		} finally {
			try {
				if(bInStream != null) {
					bInStream.close();
					bInStream = null;
				}
			}catch(Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				if(inStream != null) {
					inStream.close();
					inStream = null;
				}
			}catch(Exception ex) {
				log.error(ex.getMessage());
			}
		}
	}
}