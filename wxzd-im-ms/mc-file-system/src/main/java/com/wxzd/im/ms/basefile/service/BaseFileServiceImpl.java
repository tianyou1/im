package com.wxzd.im.ms.basefile.service;

import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.basefile.entity.BaseFileVO;
import com.wxzd.im.ms.basefile.mapper.BaseFileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class BaseFileServiceImpl implements BaseFileService {

    @Autowired
    private BaseFileMapper baseFileMapper;


    @Override
    public int deleteByPrimaryKey(String id) {
        return 0;
    }

    @Override
    public int insert(BaseFile record) {
        return 0;
    }

    @Override
    public int insertSelective(BaseFile record) {
        return this.baseFileMapper.insertSelective(record);
    }

    @Override
    public BaseFile selectByPrimaryKey(String id) {
        return this.baseFileMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(BaseFile record) {
        return this.baseFileMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(BaseFile record) {
        return 0;
    }

    @Override
    public BaseFile queryFileByMd5(String md5) {
        BaseFile bf = null;
        List<BaseFile> fList = baseFileMapper.findByMd5(md5);
        if(fList != null && !fList.isEmpty()) {
            for(BaseFile f:fList) {
                if(f ==  null || StringUtils.isEmpty(f.getFileUrl())) {
                    continue;
                }
                bf = f;
                break;
            }
        }
        return bf;
    }

    @Override
    public BaseFileVO selectById(String id) {
        return this.baseFileMapper.selectById(id);
    }

}
