package com.wxzd.im.ms.basefolder.mapper;

import com.wxzd.im.ms.basefolder.entity.BaseFolder;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface BaseFolderMapper {
    int deleteByPrimaryKey(String id);

    int insert(BaseFolder record);

    int insertSelective(BaseFolder record);

    BaseFolder selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseFolder record);

    int updateByPrimaryKey(BaseFolder record);

    BaseFolder findByCode(String code);

    List<Map<String,Object>> queryList();
}