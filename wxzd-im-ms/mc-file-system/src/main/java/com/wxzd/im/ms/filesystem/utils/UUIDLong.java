package com.wxzd.im.ms.filesystem.utils;

import java.util.UUID;

/**
 * @ClassName: UUIDLong
 * @Description: 获取UUIDLong
 * @author cys
 * @date 2019年7月25日
 */
public class UUIDLong {
	/**
	 * @Title: longUUID 
	 * @Description: 获取长整型ID
	 * @param 设定文件 
	 * @return long    返回类型 
	 * @throws
	 */
	public static long longUUID(){
		return UUID.randomUUID().getMostSignificantBits();
	}
	
	/**
	 * @Title: stringUUID
	 * @Description: 获取字符型UUID
	 * @return
	 * @return String 返回类型
	 * @throws
	 */
	public static String stringUUID() {
		return String.valueOf(longUUID());
	}
	
	/**
	 * @Title: absLongUUID 
	 * @Description: 获取正的长整型ID
	 * @param 设定文件 
	 * @return long    返回类型 
	 * @throws
	 */
	public static long absLongUUID() {
		while(true){
			long r = longUUID();
			if(r > 0){
				return r;
			}
		}
	}
}