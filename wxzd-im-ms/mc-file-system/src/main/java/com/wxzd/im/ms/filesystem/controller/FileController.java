/**
 * @Title: FileController.java
 * @Package com.jade.filesystem.controller
 * @Description: TODO(用一句话描述该文件做什么)
 * @author cys
 * @date 2019年7月25日
 * @version V1.0
 */
package com.wxzd.im.ms.filesystem.controller;

import com.wxzd.im.ms.basefolder.service.BaseFolderService;
import com.wxzd.im.ms.filesystem.aspect.ControllerMonitor;
import com.wxzd.im.ms.filesystem.config.FileConfigProperties;
import com.wxzd.im.ms.filesystem.utils.Canstant;
import com.wxzd.im.ms.filesystem.utils.ResultBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.wxzd.im.ms.filesystem.utils.FileSystemUtil.fileSystem;

/**
 * @author cx
 * @ClassName: FileController
 * @Description: 文件管理控制类
 * @date 2020年9月18日
 */
@Api(tags = "FileController", value = "文件管理控制器")
@RestController
@RequestMapping(value = "/file")
public class FileController {

    @Autowired
    private FileConfigProperties fileConfig;

    @Autowired
    private BaseFolderService folderService;

    private static Map<String, Object> folderMap = new HashMap<>();

    @PostConstruct
    public void init() {
        folderMap = folderService.queryList();
    }


    /**
     * @param
     * @return Object    返回类型
     * @throws
     * @Title: upload
     * @Description: 文件上传
     */
    @ApiOperation(value = "单文件上传", notes = "提供单个文件上传接口功能")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "referenceId", value = "关联ID", required = false, paramType = "query"),
            @ApiImplicitParam(name = "folder_code", value = "文件夹编码", required = false, paramType = "header")
    })
    @RequestMapping(value = "/upload", method = {RequestMethod.POST})
    @ControllerMonitor(description = "文件上传", operType = 2)
    public ResultBean upload(@RequestPart("file") MultipartFile file,
                             @RequestParam(value = "referenceId", required = false) String referenceId,
                             @RequestParam(value = "folderId", required = false) String folderId) {
        //获取文件夹id
        if (folderId == null || folderId.equals("") || !folderMap.containsKey(folderId)) {
            folderId = fileConfig.getDefaultFolderId();
        }
        //获取对应id的文件夹code
        String direction = folderMap.get(folderId).toString();
        //判断文件是否为空
        if (file == null ) {
            return ResultBean.getErrorData(Canstant.FILE_EMPTY);
        }
        //判断文件名是否为空
        String fileName = file.getOriginalFilename();
        if (StringUtils.isEmpty(fileName)) {
            return ResultBean.getErrorData(Canstant.FILE_EMPTY);
        }
        return fileSystem.upload(file, direction, referenceId, folderId);
    }


    /**
     * 展示图片
     *
     * @param response 返回
     * @param fileId   文件id
     */
    @ControllerMonitor(description = "图片显示")
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public void show(HttpServletResponse response, @RequestParam(value = "fileId", required = true) String fileId) {
        fileSystem.show(response, fileId);
    }

    /**
     * 播放视频
     *
     * @param response 返回
     * @param fileId   文件id
     */
    @ControllerMonitor(description = "视频播放显示")
    @RequestMapping(value = "/playVideo", method = RequestMethod.GET)
    public void playVideo(HttpServletResponse response, HttpServletRequest request, @RequestParam(value = "fileId", required = true) String fileId) {
        fileSystem.playVideo(response, request, fileId);
    }

    /**
     * @param
     * @return String    返回类型
     * @throws
     * @Title: download
     * @Description: 文件下载
     */
    @ApiOperation(value = "单文件下载", notes = "提供根据ID下载单个文件接口功能")
    @ApiImplicitParam(name = "fileId", value = "文件ID", required = true, paramType = "query")
    @ControllerMonitor(description = "单文件下载")
    @RequestMapping(value = "/download", method = {RequestMethod.POST, RequestMethod.GET})
    public ResultBean download(@RequestParam(value = "fileId", required = true) String fileId, HttpServletResponse response) {
        return fileSystem.download(fileId, response);
    }

    /**
     * 获取前缀
     */
    @ControllerMonitor(description = "获取文件系统前缀")
    @RequestMapping(value = "/getVisitPrefix", method = {RequestMethod.POST})
    public ResultBean getVisitPrefix() {
        return fileSystem.getVisitPrefix();
    }
}