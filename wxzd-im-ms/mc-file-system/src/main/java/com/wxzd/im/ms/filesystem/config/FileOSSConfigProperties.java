package com.wxzd.im.ms.filesystem.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取阿里云OSS文件系统的配置需求
 */
@ConfigurationProperties(prefix="file.oss")
@Component
public class FileOSSConfigProperties {
    /**
     * 文件存放目录
     */
    private String dir;

    /**
     * 阿里云SSO URL
     */
    private String ssoUrl;

    /**
     * 阿里云 节点
     */
    private String endPoint;

    /**
     * 阿里云 accessKeyId
     */
    private String accessKeyId;

    /**
     * 阿里云 accessKeySecret
     */
    private String accessKeySecret;

    /**
     * 阿里云 存放桶名
     */
    private String bucketName;

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getSsoUrl() {
        return ssoUrl;
    }

    public void setSsoUrl(String ssoUrl) {
        this.ssoUrl = ssoUrl;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
