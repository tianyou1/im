package com.wxzd.im.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName: FileSystemApplication
 * @Description: 文件管理系统Application
 * @author cys
 * @date 2019年7月24日
 */
@SpringBootApplication
@EnableDiscoveryClient(autoRegister = true)
public class FileSystemApplication {

	public static void main(String[] args) {
		System.out.println("文件系统开始启动！");
		SpringApplication.run(FileSystemApplication.class, args);
		System.out.println("文件系统启动成功！");
	}
}