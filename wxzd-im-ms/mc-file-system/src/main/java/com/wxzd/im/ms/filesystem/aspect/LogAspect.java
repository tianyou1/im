/**
 * @Title: LogAspect.java
 * @Package com.jade.filesystem.aspect
 * @Description: TODO(用一句话描述该文件做什么)
 * @author cys
 * @date 2019年7月30日
 * @version V1.0
 */
package com.wxzd.im.ms.filesystem.aspect;


import com.wxzd.im.ms.baselog.entity.BaseLog;
import com.wxzd.im.ms.baselog.service.BaseLogService;
import com.wxzd.im.ms.filesystem.enums.OperTypeEnum;
import com.wxzd.im.ms.filesystem.utils.DateUtil;
import com.wxzd.im.ms.filesystem.utils.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

// TODO: 2020/11/20 新版
@Slf4j
@Aspect
@Component
public class LogAspect {
	
	/**
	 * 注入logService
	 */
	@Autowired
	private BaseLogService logService;

	@Pointcut("execution(public * com.wxzd.im.ms.filesystem.controller.*.*(..))")
	public void aspect() {
		
	}
	
	@Around(value = "aspect()")
	public Object arount(ProceedingJoinPoint joinPoint) throws Throwable {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed(joinPoint.getArgs());
        long endTime = System.currentTimeMillis();
        // 执行耗时
        long totalTime = endTime - startTime;
        //从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        // 获取切入点方法
        Method targetMethod = methodSignature.getMethod();
        //获取请求的类名
        String className = joinPoint.getTarget().getClass().getName();
        //获取请求的方法名
        String methodName = targetMethod.getName();
        String method = className + "." + methodName + "()";
        // 请求URI
        String uri = request.getRequestURI();
        //请求的参数
        Object[] args = joinPoint.getArgs();
        String params = Arrays.toString(args);
        BaseLog baseLog = new BaseLog();
        String desc = "";
        String oper = "";
        if(targetMethod.isAnnotationPresent(ControllerMonitor.class)) {
        	ControllerMonitor monitor = targetMethod.getAnnotation(ControllerMonitor.class);
        	if(monitor != null) {
        		desc = monitor.description();
        		baseLog.setOperType(monitor.operType());
        		oper = OperTypeEnum.getVal(monitor.operType());
        		baseLog.setOperTypeDesc(oper);
        	}
        }
        log.info("**********Method: {}, Description:{}, Operation:{}, Start: {}, End: {}, Total: {}ms**********", 
        		method, desc, oper, DateUtil.transferLongToDate(startTime), DateUtil.transferLongToDate(endTime), totalTime);
        baseLog.setIp(IpUtil.getClientIp(request));
        baseLog.setMethod(method);
        baseLog.setRequestUrl(uri);
        baseLog.setRequestParam(params);
        baseLog.setDescription(desc);
        baseLog.setCostTime(totalTime);
        baseLog.setCreateTime(new Date());
        baseLog.setUpdateTime(new Date());
        logService.insertSelective(baseLog);
        return result;
	}
}