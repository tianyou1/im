/**
 * @Title: Canstant.java
 * @Package com.jade.filesystem.utils
 * @Description: TODO(用一句话描述该文件做什么)
 * @author cys
 * @date 2019年7月26日
 * @version V1.0
 */
package com.wxzd.im.ms.filesystem.utils;

/**
 * @ClassName: Canstant
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author cys
 * @date 2019年7月26日
 */
public class Canstant {
	
	public static final String SAVE_UPDATE_ENTITY_NULL_MSG = "新增或者修改内容不存在！";
	
	/**
	 * 文件上传成功信息
	 */
	public static final String FILE_UPLOAD_SUCCESS_MSG = "文件上传成功！";
	
	/**
	 * 文件存放路径配置错误信息
	 */
	public static final String FILE_CONFIG_ERROR_MSG = "文件存放路径配置错误，请检查！";
	
	/**
	 * 文件错误
	 */
	public static final String FILE_EMPTY = "上传的文件错误！";

	/**
	 * 文件上传失败MSG
	 */
	public static final String FILE_UPLOAD_ERROR_MSG = "文件上传失败！";
	
	/**
	 * 下载文件参数错误
	 */
	public static final String FILE_ID_ERROR_MSG = "下载文件参数错误！";
	
	/**
	 * 下载文件不存在
	 */
	public static final String FILE_NOT_EXIST_MSG = "下载文件不存在！";
	
	/**
	 * 文件下载成功
	 */
	public static final String FILE_DOWNLOAD_SUCCESS_MSG = "文件下载成功！";
	
	/**
	 * 文件下载失败MSG
	 */
	public static final String FILE_DOWNLOAD_ERROR_MSG = "文件下载失败！";
	
	/**
	 * 删除文件参数错误
	 */
	public static final String DEL_FILE_ERROR_MSG = "删除文件参数错误！";
	
	/**
	 * 删除文件成功
	 */
	public static final String DEL_FILE_SUCCESS_MSG = "删除文件成功！";
	
	/**
	 * 删除文件失败
	 */
	public static final String DEL_FILE_ERROER_MSG = "删除文件失败！";
	
	/**
	 * 图片文件整合失败
	 */
	public static final String FILE_MERGE_ERROR_MSG = "图片文件整合失败！";
	
	/**
	 * 图片文件整合成功
	 */
	public static final String FILE_MERGE_SUCCESS_MSG = "图片文件整合成功！";
	
	/**
	 * 整合图片不存在
	 */
	public static final String FILE_MERGE_PARAM_ERROR_MSG = "整合图片不存在！";

	/**
	 * 图片找不到
	 */
	public static final String FILE_NO_FONUD_ERROR_MSG = "图片找不到！";

	/**
	 * 图片展示成功
	 */
	public static final String FILE_SHOW_SECCESS_MSG = "图片展示成功！";
}
