package com.wxzd.im.ms.basefolder.service;

import com.wxzd.im.ms.basefolder.entity.BaseFolder;

import java.util.Map;

public interface BaseFolderService {
    int deleteByPrimaryKey(String id);

    int insert(BaseFolder record);

    int insertSelective(BaseFolder record);

    BaseFolder selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseFolder record);

    int updateByPrimaryKey(BaseFolder record);

    BaseFolder findByCode(String code);

    Map<String,Object> queryList();
}