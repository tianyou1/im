package com.wxzd.im.ms.baselog.service;

import com.wxzd.im.ms.baselog.entity.BaseLog;
import com.wxzd.im.ms.baselog.mapper.BaseLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaseLogServiceImpl implements BaseLogService {
    @Autowired
    private BaseLogMapper baseLogMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return this.baseLogMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(BaseLog record) {
        return this.baseLogMapper.insert(record);
    }

    @Override
    public int insertSelective(BaseLog record) {
        return this.baseLogMapper.insertSelective(record);
    }

    @Override
    public BaseLog selectByPrimaryKey(String id) {
        return this.baseLogMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(BaseLog record) {
        return this.baseLogMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(BaseLog record) {
        return this.baseLogMapper.updateByPrimaryKey(record);
    }
}
