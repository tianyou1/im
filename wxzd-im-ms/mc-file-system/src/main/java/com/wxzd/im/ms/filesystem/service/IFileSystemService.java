package com.wxzd.im.ms.filesystem.service;


import com.wxzd.im.ms.filesystem.utils.ResultBean;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IFileSystemService {

	ResultBean upload(MultipartFile file, String direction, String referenceId, String folderId);

	void show(HttpServletResponse response, String fileId);

	void playVideo(HttpServletResponse response,HttpServletRequest request, String fileId);

	ResultBean download(String fileId, HttpServletResponse response);

	ResultBean getVisitPrefix();
}
