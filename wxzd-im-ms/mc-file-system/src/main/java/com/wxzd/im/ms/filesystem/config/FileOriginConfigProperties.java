package com.wxzd.im.ms.filesystem.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取原文件系统的配置需求
 */
@ConfigurationProperties(prefix="file.origin")
@Component
public class FileOriginConfigProperties {
    /**
     * 文件存放目录
     */
    private String dir;

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
