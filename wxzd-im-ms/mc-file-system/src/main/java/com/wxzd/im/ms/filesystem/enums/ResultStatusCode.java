package com.wxzd.im.ms.filesystem.enums;

/**
 * @ClassName: ResultStatusCode 
 * @Description: 错误码枚举
 * @author cys 
 * @date 2018年3月9日 下午5:05:29
 */
public enum ResultStatusCode {
	SUCCESS("操作成功", 200),
	SYSTEM_ERR("系统错误，请联系管理员！", 500),
	EMPTY_PARAM("请求参数为空", 500);
	
	private int errorCode;
	
	private String errorMsg;
	
	private ResultStatusCode(String errorMsg, int errorCode){
		this.errorMsg = errorMsg;
		this.errorCode = errorCode;
	}
	
	public static String getName(int ec) {  
        for (ResultStatusCode sc : ResultStatusCode.values()) {  
            if (sc.errorCode == ec) {  
                return sc.errorMsg;  
            }  
        }  
        return null;  
    }
	
	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	} 
}