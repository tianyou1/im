package com.wxzd.im.ms.filesystem.service;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.basefile.entity.BaseFileVO;
import com.wxzd.im.ms.basefile.service.BaseFileService;
import com.wxzd.im.ms.filesystem.config.FileConfigProperties;
import com.wxzd.im.ms.filesystem.config.FileOSSConfigProperties;
import com.wxzd.im.ms.filesystem.utils.Canstant;
import com.wxzd.im.ms.filesystem.utils.FileSystemUtil;
import com.wxzd.im.ms.filesystem.utils.ResultBean;
import com.wxzd.im.ms.filesystem.utils.UUIDLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import static com.wxzd.im.ms.filesystem.utils.FileSystemUtil.*;
import static org.apache.tomcat.util.http.fileupload.IOUtils.copy;

@Service
@Component
public class OSSFileSystemServiceImpl implements IFileSystemService{

    @Autowired
    FileOSSConfigProperties configProperties; //注入的对象

    @Autowired
    BaseFileService baseFileService;

    @Autowired
    FileConfigProperties fileConfigProperties;

    private static BaseFileService fileService;
    private static FileOSSConfigProperties fileOSSConfig;  //静态对象
    private static FileConfigProperties config;

    @PostConstruct
    public void init(){
        fileOSSConfig = this.configProperties;  //将注入的对象交给静态对象管理
        fileService=this.baseFileService;
        config=this.fileConfigProperties;
    }



    @Override
    public ResultBean upload(MultipartFile file, String direction, String referenceId, String folderId) {
        try {
            String fileName = file.getOriginalFilename();
            String suffix = getSuffix(fileName);
            referenceId = StringUtils.isEmpty(referenceId) ? UUIDLong.stringUUID() : referenceId;
            String mimeType = file.getContentType();
            //获取对应输入流的md5值
            String md5 = DigestUtils.md5DigestAsHex(file.getInputStream());
            md5=BaseFile.ossType +","+md5;
            //根据md5判断文件是否存在
            BaseFile exitFile=fileService.queryFileByMd5(md5);
            String objectName =FileSystemUtil.getFileName(direction,suffix);
            if (exitFile != null) {// 根据文件MD5值判断，已经存在，不需要再存多份文件
                objectName = exitFile.getFileUrl();
            } else {
                String accessKeyId=fileOSSConfig.getAccessKeyId();
                String accessKeySecret=fileOSSConfig.getAccessKeySecret();
                String endpoint=fileOSSConfig.getEndPoint();
                String bucketName=fileOSSConfig.getBucketName();
                //创建Client
                OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
                byte[] fileByte=file.getBytes();
                InputStream inputStream=new ByteArrayInputStream(fileByte);
                //创建上传Object的Metadata
                ObjectMetadata objectMetadata = new ObjectMetadata();
                objectMetadata.setContentLength(inputStream.available());
                objectMetadata.setCacheControl("no-cache");
                objectMetadata.setHeader("Pragma", "no-cache");
                objectMetadata.setContentType(file.getContentType());
                objectMetadata.setContentDisposition("inline;filename=" + objectName);
                // 指定上传文件操作时是否覆盖同名Object。
                // 不指定x-oss-forbid-overwrite时，默认覆盖同名Object。
                // 指定x-oss-forbid-overwrite为false时，表示允许覆盖同名Object。
                // 指定x-oss-forbid-overwrite为true时，表示禁止覆盖同名Object，如果同名Object已存在，程序将报错。
                objectMetadata.setHeader("x-oss-forbid-overwrite", "false");
                ossClient.putObject(bucketName,objectName,inputStream,objectMetadata);
                // 关闭OSSClient。
                ossClient.shutdown();
            }

            BaseFile fileEntity = new BaseFile();
            fileEntity.setReferenceId(referenceId);
            fileEntity.setFileName(fileName);
            fileEntity.setFileUrl(objectName);
            fileEntity.setMimeType(mimeType);
            fileEntity.setFileSize(file.getSize());
            fileEntity.setSuffix(suffix);
            fileEntity.setFolderId(folderId);
            fileEntity.setMd5(md5);
            fileEntity.setCreateTime(new Date());
            fileEntity.setUpdateTime(new Date());
            //保存
            fileService.insertSelective(fileEntity);
            fileEntity.setVisitUrl(setVisitUrl(mimeType)+fileEntity.getId());
            if(isImage(mimeType)) {
                fileEntity.setThumbnailUrl("file/thumbnail?fileId=" + fileEntity.getId() + "&img-weight={weight}&img-height={height}&img-scale={scale}&keepAspectRatio={keepAspectRatio}");
            }
            //保存
            fileService.updateByPrimaryKeySelective(fileEntity);
            //返回体
            BaseFileVO rsFile=fileService.selectById(fileEntity.getId());
            rsFile.setVisitPrefix(config.getVisitPrefix());
            return ResultBean.success(Canstant.FILE_UPLOAD_SUCCESS_MSG, rsFile);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultBean.getErrorData(Canstant.FILE_UPLOAD_ERROR_MSG, e.getMessage());
        }
    }

    @Override
    public void show(HttpServletResponse response , String fileId) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        if (fileEntity == null || StringUtils.isEmpty(fileEntity.getFileUrl())) {
            //图片找不到 暂不处理
        } else {
            //读取property文件，获取最新下载地址
            InputStream is = null;
            OutputStream os = null;
            try {
                response.setContentType("image/jpeg");
                URL url = new URL(fileOSSConfig.getDir() + fileEntity.getFileUrl());
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(20 * 1000);//20秒超时
                InputStream inStream = conn.getInputStream();//通过输入流获取图片数据
                is = inStream;
                os = response.getOutputStream();
                copy(is, os);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeCon(is,os,null);
            }
        }
    }

    @Override
    public void playVideo(HttpServletResponse response, HttpServletRequest request, String fileId) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);

        if (fileEntity == null || StringUtils.isEmpty(fileEntity.getFileUrl())) {
            //视频找不到 暂不处理

        } else {
            try {
                URL url = new URL(fileOSSConfig.getDir() + fileEntity.getFileUrl());
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(20 * 1000);//20秒超时
                InputStream inStream = conn.getInputStream();//通过输入流获取视频数据
                ByteArrayOutputStream output = new ByteArrayOutputStream();

                copy(inStream,output);
                byte[] bytes =output.toByteArray();
                response.setContentType("video/mp4");
                response.setContentLength(bytes.length);
                try {
                    response.getOutputStream().write(bytes);
                } catch (IOException e) {
                    System.out.println("IO异常----");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ResultBean download(String fileId , HttpServletResponse response) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        byte[] buff = new byte[1024];
        OutputStream os=null;
        BufferedInputStream bis=null;
        try{
            //获取文件数据
            URL url = new URL(fileOSSConfig.getDir() + fileEntity.getFileUrl());
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(20 * 1000);//20秒超时
            InputStream inStream = conn.getInputStream();//通过输入流获取数据
            String mimeType = StringUtils.isEmpty(fileEntity.getMimeType()) ? "application/octet-stream" : fileEntity.getMimeType();
            response.setHeader("content-type", mimeType);
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileEntity.getFileName().getBytes(), "ISO-8859-1"));
            os = response.getOutputStream();
            bis = new BufferedInputStream(inStream);
            int i = bis.read(buff);
            while (i != -1) {
                os.write(buff, 0, i);
                i = bis.read(buff);
            }
        }catch (IOException e){
            return ResultBean.getErrorData(Canstant.FILE_DOWNLOAD_ERROR_MSG);
        }finally {
            closeCon(null,os,bis);
        }
        return null;
    }

    @Override
    public ResultBean getVisitPrefix() {
        return ResultBean.success("",config.getVisitPrefix());
    }
}
