package com.wxzd.im.ms.filesystem.utils;


import com.wxzd.im.ms.filesystem.config.FileMinIOConfigProperties;
import com.wxzd.im.ms.filesystem.config.UseSystemTypeProperties;
import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.entity.result.ResponseResult;
import com.wxzd.im.ms.filesystem.service.*;
import io.micrometer.core.instrument.util.StringUtils;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;

@Component
public class FileSystemUtil {

    @Autowired
    UseSystemTypeProperties useSystemTypeProperties; //注入的对象

    @Autowired
    FileMinIOConfigProperties fileMinIOConfigProperties;

    private static UseSystemTypeProperties typeProperties;

    public static IFileSystemService fileSystem ;

    public static FileMinIOConfigProperties fileMinIOConfig;

    @PostConstruct
    public void init(){
        typeProperties=this.useSystemTypeProperties;
        fileMinIOConfig=this.fileMinIOConfigProperties;
        String systemType=typeProperties.getType();
        switch (systemType) {
            //1:Origin、2：MinIo、3：OSS
            case "1":
                fileSystem = new OriginFileSystemServiceImpl();
                break;
            case "2":
                fileSystem = new MinIOFileSystemServiceImpl();
                try{
                    //创建一个minio客户端
                    MinioClient minioClient = new MinioClient(fileMinIOConfig.getUrl(), fileMinIOConfig.getName(), fileMinIOConfig.getPass());
                    //判断配置的桶是否存在
                    boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(fileMinIOConfig.getBucketName()).build());
                    if(isExist) {
                        System.out.println(fileMinIOConfig.getBucketName()+" Bucket already exists.");
                    } else {
                        // Make a new bucket called asiatrip to hold a zip file of photos.
                        minioClient.makeBucket(MakeBucketArgs.builder().bucket(fileMinIOConfig.getBucketName()).build());
                        System.out.println(fileMinIOConfig.getBucketName()+" Bucket created successfully");
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case "3":
                fileSystem = new OSSFileSystemServiceImpl();
                break;
            case "4":
                fileSystem = new OSSFileSystem2ServiceImpl();
                break;
            default:
                fileSystem = new OSSFileSystem2ServiceImpl();
                break;
        }

    }

    /**
     * 通用方法
     */

    public static String getFileName(String direction,String suffix){
        //根据命名规则重新生成文件名
        Date currentDate = new Date();
        String currentDateStr = DateUtil.dateToString(currentDate, "yyyyMMdd");
        return direction+"/" + currentDateStr + "/" + UUIDLong.stringUUID() +  (suffix != null ? suffix : "");
    }

    public static void show(BaseFile fileEntity, String dir, HttpServletResponse response){
        if (fileEntity == null || StringUtils.isEmpty(fileEntity.getFileUrl())) {
            //图片找不到 暂不处理

        } else {
            //读取property文件，获取最新下载地址
            InputStream is = null;
            OutputStream os = null;
            try {
                response.setContentType("image/jpeg");
                File file = new File(dir + fileEntity.getFileUrl());
                response.addHeader("Content-Length", "" + file.length());
                is = new FileInputStream(file);
                os = response.getOutputStream();
                IOUtils.copy(is, os);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                closeCon(is,os,null);
            }
        }
    }

    public static void closeCon(InputStream is, OutputStream os, BufferedInputStream bis){
        try {
            if (os != null) {
                os.flush();
                os.close();
                os=null;
            }
            if (is != null) {
                is.close();
                is=null;
            }
            if (bis != null) {
                bis.close();
                bis=null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void playVideo(BaseFile fileEntity, String dir, HttpServletResponse response){

        if (fileEntity == null || org.springframework.util.StringUtils.isEmpty(fileEntity.getFileUrl())) {
            //视频找不到 暂不处理

        } else {
            byte[] bytes = file2byte(dir + fileEntity.getFileUrl());
            response.setContentType("video/mp4");
            response.setContentLength(bytes.length);
            try {
                response.getOutputStream().write(bytes);
            } catch (IOException e) {
                System.out.println("IO异常----");
            }
        }
    }

    public static void sentOss(BaseFile fileEntity, String dir, HttpServletResponse response){
        if (fileEntity == null || org.springframework.util.StringUtils.isEmpty(fileEntity.getFileUrl())) {
            //视频找不到 暂不处理

        } else {
            try {
                String url=dir+fileEntity.getFileUrl();
                response.sendRedirect(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    public static void sendVideo(HttpServletRequest request, HttpServletResponse response, BaseFile fileEntity, String dir) {
        if (fileEntity == null || StringUtils.isEmpty(fileEntity.getFileUrl())) {
            //文件找不到 暂不处理

        } else {
            try{
                File file=new File(dir+fileEntity.getFileUrl());
                RandomAccessFile randomFile = new RandomAccessFile(file, "r");//只读模式
                long contentLength = randomFile.length();
                String range = request.getHeader("Range");
                int start = 0, end = 0;
                if (range != null && range.startsWith("bytes=")) {
                    String[] values = range.split("=")[1].split("-");
                    start = Integer.parseInt(values[0]);
                    if (values.length > 1) {
                        end = Integer.parseInt(values[1]);
                    }
                }
                int requestSize = 0;
                if (end != 0 && end > start) {
                    requestSize = end - start + 1;
                } else {
                    requestSize = Integer.MAX_VALUE;
                }

                byte[] buffer = new byte[4096];
                response.setContentType("video/mp4");
                response.setHeader("Accept-Ranges", "bytes");
                response.setHeader("ETag", fileEntity.getFileName());
                response.setHeader("Last-Modified", new Date().toString());
                //第一次请求只返回content length来让客户端请求多次实际数据
                if (range == null) {
                    response.setHeader("Content-length", contentLength + "");
                } else {
                    //以后的多次以断点续传的方式来返回视频数据
                    response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);//206
                    long requestStart = 0, requestEnd = 0;
                    String[] ranges = range.split("=");
                    if (ranges.length > 1) {
                        String[] rangeDatas = ranges[1].split("-");
                        requestStart = Integer.parseInt(rangeDatas[0]);
                        if (rangeDatas.length > 1) {
                            requestEnd = Integer.parseInt(rangeDatas[1]);
                        }
                    }
                    long length = 0;
                    if (requestEnd > 0) {
                        length = requestEnd - requestStart + 1;
                        response.setHeader("Content-length", "" + length);
                        response.setHeader("Content-Range", "bytes " + requestStart + "-" + requestEnd + "/" + contentLength);
                    } else {
                        length = contentLength - requestStart;
                        response.setHeader("Content-length", "" + length);
                        response.setHeader("Content-Range", "bytes " + requestStart + "-" + (contentLength - 1) + "/" + contentLength);
                    }
                }
                ServletOutputStream out = response.getOutputStream();
                int needSize = requestSize;
                randomFile.seek(start);
                while (needSize > 0) {
                    int len = randomFile.read(buffer);
                    if (needSize < buffer.length) {
                        out.write(buffer, 0, needSize);
                    } else {
                        out.write(buffer, 0, len);
                        if (len < buffer.length) {
                            break;
                        }
                    }
                    needSize -= buffer.length;
                }
                randomFile.close();
                out.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static byte[] file2byte(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    public static String setVisitUrl(String mimeType){
        String visitUrl="";
        if(isImage(mimeType)) {
            visitUrl="file/show?fileId=";
        }else if(isVideo(mimeType)) {
            visitUrl="file/playVideo?fileId=";
        }else {
            visitUrl="file/download?fileId=";
        }

        return visitUrl;
    }

    public static boolean isImage(String mimeType) {
        boolean isImage = false;
        if (!org.springframework.util.StringUtils.isEmpty(mimeType)) {
            String type = mimeType.split("/")[0];
            isImage = type.toLowerCase().equals("image");
        }
        return isImage;
    }

    public static boolean isVideo(String mimeType) {
        boolean isVideo = false;
        if (!org.springframework.util.StringUtils.isEmpty(mimeType)) {
            String type = mimeType.split("/")[0];
            isVideo = type.toLowerCase().equals("video");
        }
        return isVideo;
    }

    public static ResultBean download(BaseFile fileEntity, String dir, HttpServletResponse response){
        ResponseResult rs=new ResponseResult();
        byte[] buff = new byte[1024];
        BufferedInputStream bis = null;
        OutputStream os = null;
        if (StringUtils.isEmpty(dir)) {
            return ResultBean.getErrorData(Canstant.FILE_CONFIG_ERROR_MSG);
        }
        if (fileEntity == null || StringUtils.isEmpty(fileEntity.getFileUrl())) {
            return ResultBean.getErrorData(Canstant.FILE_NOT_EXIST_MSG);
        }
        File file = new File(dir + fileEntity.getFileUrl());
        if (!file.exists()) {
            return ResultBean.getErrorData(Canstant.FILE_NOT_EXIST_MSG);
        }
        try {
            String mimeType = StringUtils.isEmpty(fileEntity.getMimeType()) ? "application/octet-stream" : fileEntity.getMimeType();
            response.setHeader("content-type", mimeType);
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileEntity.getFileName().getBytes(), "ISO-8859-1"));
            os = response.getOutputStream();
            bis = new BufferedInputStream(new FileInputStream(file));
            int i = bis.read(buff);
            while (i != -1) {
                os.write(buff, 0, i);
                i = bis.read(buff);
            }
        } catch (Exception ex) {
            return ResultBean.getErrorData(Canstant.FILE_UPLOAD_ERROR_MSG);
        } finally {
            closeCon(null,os,bis);
        }
        return null;
    }

    public static String getSuffix(String name) {
        if (StringUtils.isEmpty(name)) {
            return "";
        }
        int i = name.lastIndexOf(".");
        if (i >= 0) {
            return name.substring(i);
        }
        return "";
    }
}
