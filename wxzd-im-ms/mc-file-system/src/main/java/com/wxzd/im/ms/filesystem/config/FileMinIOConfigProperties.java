package com.wxzd.im.ms.filesystem.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取MinIO文件系统的配置需求
 */
@ConfigurationProperties(prefix="file.minio")
@Component
public class FileMinIOConfigProperties {
    /**
     * 文件存放目录
     */
    private String dir;

    /**
     * 访问路径
     */
    private String url;

    /**
     * minIO账号
     */
    private String name;

    /**
     * minIO密码
     */
    private String pass;

    /**
     * minIO存放桶名
     */
    private String bucketName;

    /**
     * 如果是true，则用的是https而不是http,默认值是true
     */
    private Boolean isHttps;

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public Boolean getHttps() {
        return isHttps;
    }

    public void setHttps(Boolean https) {
        isHttps = https;
    }
}
