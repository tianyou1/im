package com.wxzd.im.ms.filesystem.service;

import com.wxzd.im.ms.basefile.entity.BaseFile;
import com.wxzd.im.ms.basefile.entity.BaseFileVO;
import com.wxzd.im.ms.basefile.service.BaseFileService;
import com.wxzd.im.ms.filesystem.config.FileConfigProperties;
import com.wxzd.im.ms.filesystem.config.FileOriginConfigProperties;
import com.wxzd.im.ms.filesystem.utils.Canstant;
import com.wxzd.im.ms.filesystem.utils.FileSystemUtil;
import com.wxzd.im.ms.filesystem.utils.ResultBean;
import com.wxzd.im.ms.filesystem.utils.UUIDLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Date;

import static com.wxzd.im.ms.filesystem.utils.FileSystemUtil.isImage;
import static com.wxzd.im.ms.filesystem.utils.FileSystemUtil.setVisitUrl;

@Service
@Component
public class OriginFileSystemServiceImpl implements IFileSystemService{

    @Autowired
    FileOriginConfigProperties configProperties; //注入的对象

    @Autowired
    BaseFileService baseFileService;

    @Autowired
    FileConfigProperties fileConfigProperties;


    private static BaseFileService fileService;
    private static FileOriginConfigProperties fileOriginConfig;  //静态对象
    private static FileConfigProperties config;


    @PostConstruct
    public void init(){
        fileOriginConfig = this.configProperties;  //将注入的对象交给静态对象管理
        fileService=this.baseFileService;
        config=this.fileConfigProperties;
    }




    @Override
    public ResultBean upload(MultipartFile file, String direction, String referenceId,String folderId) {
        try {
            //获取文件名
            String fileName = file.getOriginalFilename();
            //获取后缀
            String suffix = FileSystemUtil.getSuffix(fileName);
            //获取关联id
            referenceId = StringUtils.isEmpty(referenceId) ? UUIDLong.stringUUID() : referenceId;
            //获取对应输入流的md5值
            String md5 = DigestUtils.md5DigestAsHex(file.getInputStream());
            md5=BaseFile.orginType +","+md5;
            //根据md5判断文件是否存在
            BaseFile exitFile=fileService.queryFileByMd5(md5);
            String fileUrl = FileSystemUtil.getFileName(direction,suffix);
            if (exitFile != null) {// 根据文件MD5值判断，已经存在，不需要再存多份文件
                fileUrl = exitFile.getFileUrl();
            } else {
                File dest = new File(fileOriginConfig.getDir() + fileUrl);
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                file.transferTo(dest);
            }
            //获取contentType
            String mimeType = file.getContentType();
            //保存数据库
            BaseFile fileEntity = new BaseFile();
            fileEntity.setReferenceId(referenceId);
            fileEntity.setFileName(fileName);
            fileEntity.setFileUrl(fileUrl);
            fileEntity.setMimeType(mimeType);
            fileEntity.setFileSize(file.getSize());
            fileEntity.setSuffix(suffix);
            fileEntity.setFolderId(folderId);
            fileEntity.setMd5(md5);

            fileEntity.setCreateTime(new Date());
            fileEntity.setUpdateTime(new Date());
            //保存
            fileService.insertSelective(fileEntity);
            fileEntity.setVisitUrl(setVisitUrl(mimeType)+fileEntity.getId());
            if(isImage(mimeType)) {
                fileEntity.setThumbnailUrl("file/thumbnail?fileId=" + fileEntity.getId() + "&img-weight={weight}&img-height={height}&img-scale={scale}&keepAspectRatio={keepAspectRatio}");
            }
            //保存
            fileService.updateByPrimaryKeySelective(fileEntity);
            //返回体
            BaseFileVO rsFile=fileService.selectById(fileEntity.getId());
            rsFile.setVisitPrefix(config.getVisitPrefix());
            return ResultBean.success(Canstant.FILE_UPLOAD_SUCCESS_MSG, rsFile);
        }catch (Exception e){
            e.printStackTrace();
            return ResultBean.getErrorData(Canstant.FILE_UPLOAD_ERROR_MSG, e.getMessage());
        }

    }

    @Override
    public void show(HttpServletResponse response, String fileId) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        FileSystemUtil.show(fileEntity,fileOriginConfig.getDir(),response);
    }

    @Override
    public void playVideo(HttpServletResponse response, HttpServletRequest request, String fileId) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        FileSystemUtil.sendVideo(request,response,fileEntity,fileOriginConfig.getDir());
        //FileSystemUtil.playVideo(fileEntity,fileOriginConfig.getDir(),response);
    }

    @Override
    public ResultBean download(String fileId, HttpServletResponse response) {
        BaseFile fileEntity = fileService.selectByPrimaryKey(fileId);
        return FileSystemUtil.download(fileEntity,fileOriginConfig.getDir(),response);
    }

    @Override
    public ResultBean getVisitPrefix() {
        return ResultBean.success("",config.getVisitPrefix());
    }
}
