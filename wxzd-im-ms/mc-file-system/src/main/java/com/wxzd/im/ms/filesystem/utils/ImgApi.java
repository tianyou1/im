/**
 * @Title: ImgApi.java
 * @Package com.jade.filesystem.utils
 * @Description: TODO(用一句话描述该文件做什么)
 * @author cys
 * @date 2019年8月1日
 * @version V1.0
 */
package com.wxzd.im.ms.filesystem.utils;

import com.wxzd.im.ms.filesystem.vo.ImageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Base64Utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: ImgApi
 * @Description: 图片列表整合
 * @author cys
 * @date 2019年8月1日
 */
@Slf4j
public class ImgApi {

	/**
	 * @Title: mergeImgs
	 * @Description: 图片文件整合
	 * @param files
	 * @param imgVo
	 * @param path
	 * @throws IOException
	 * @return String 返回类型
	 * @throws
	 */
	public static String mergeImgs(List<File> files, ImageVo imgVo, String path) throws IOException {
		String result = "";
		if(files == null || files.isEmpty()) {
			return result;
		}
		int size = files.size();
		imgVo = ImageVo.arrangeImgVo(imgVo, size);
		int imgWidth = 0;
		int imgHeight = 0;
		int x = 0;
		int y = 0;
		int space = imgVo.getSpace();
		int oNum = (int) Math.ceil(size * 1.0 / imgVo.getNum());
		BufferedImage imageNew = new BufferedImage(imgVo.getWidth(), imgVo.getHeight(), BufferedImage.TYPE_INT_BGR);
		for(int i = 0; i < imgVo.getWidth(); i ++) {
			for(int j = 0; j < imgVo.getHeight(); j ++) {
				imageNew.setRGB(i, j, 0xFFFFFF);
			}
		}
		int fIndex = 0;
		switch(imgVo.getDirect()) {
			case "X":// 横向
				imgWidth = (imgVo.getWidth() - (imgVo.getNum() - 1) * space)/imgVo.getNum();
				imgHeight = (imgVo.getHeight() - (oNum - 1) * space)/oNum;
				for(int i = 0; i < oNum; i++) {
					x = 0;
					y = i * (imgHeight + space);
					for(int j = 0; j < imgVo.getNum(); j++) {
						File file = files.get(fIndex);
					    BufferedImage image = ImageIO.read(file);
						image = zoom(image, imgWidth, imgHeight);
						int[] imageArrayOne = new int[imgWidth * imgHeight];
						imageArrayOne = image.getRGB(0, 0, imgWidth, imgHeight, imageArrayOne, 0, imgWidth);
						imageNew.setRGB(x, y, imgWidth, imgHeight, imageArrayOne, 0, imgWidth);//设置左半部分的RGB
						fIndex++;
						if(fIndex >= size) {
							break;
						}
						x = (j+1) * (imgWidth + space);
					}
					if(fIndex >= size) {
						break;
					}
				}
				break;
			case "Y":// 纵向
				imgWidth = (imgVo.getWidth() - (oNum - 1) * space)/oNum;
				imgHeight = (imgVo.getHeight() - (imgVo.getNum() - 1) * space)/imgVo.getNum();
				for(int i = 0; i < oNum; i++) {
					y = 0;
					x = i * (imgWidth + space);
					for(int j = 0; j < imgVo.getNum(); j++) {
						File file = files.get(fIndex);
					    BufferedImage image = ImageIO.read(file);
						image = zoom(image, imgWidth, imgHeight);
						int[] imageArrayOne = new int[imgWidth * imgHeight];
						imageArrayOne = image.getRGB(0, 0, imgWidth, imgHeight, imageArrayOne, 0, imgWidth);
						imageNew.setRGB(x, y, imgWidth, imgHeight, imageArrayOne, 0, imgWidth);//设置左半部分的RGB
						fIndex++;
						if(fIndex >= size) {
							break;
						}
						y = (j+1) * (imgHeight + space);
					}
					if(fIndex >= size) {
						break;
					}
				}
		}
		
		if(imgVo.isBase64()) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
	            ImageIO.write(imageNew, "png", baos);
	            String base64Img = Base64Utils.encodeToString(baos.toByteArray());
	            result = "data:image/jpg;base64," + base64Img.toString();
	            baos.close();
			}catch(Exception ex) {
				log.error("mergeImgs", ex.getMessage());
			}finally {
				try {
					if(baos != null) {
						baos.close();
						baos = null;
					}
				}catch(Exception ex) {
					log.error(ex.getMessage());
				}
			}
		}else {
			Date currentDate = new Date();
			String currentDateStr = DateUtil.dateToString(currentDate, "yyyyMMdd");
			String fileUrl = currentDateStr + File.separator + UUIDLong.stringUUID() + ".png";
			File outFile = new File(path + fileUrl);
	        if (!outFile.getParentFile().exists()) {
	        	outFile.getParentFile().mkdirs();
	        }
	        ImageIO.write(imageNew, "png", outFile);
	        result = "image/" + fileUrl;
		}
		return result;
	}
	
	/**
	 * @Title: zoom
	 * @Description: 图片缩放
	 * @param sourceImage
	 * @param width
	 * @param height
	 * @return BufferedImage 返回类型
	 * @throws
	 */
	public static BufferedImage zoom(BufferedImage sourceImage, int width, int height) {
		BufferedImage zoomImage = new BufferedImage(width, height, sourceImage.getType());
		Image image = sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		Graphics gc = zoomImage.getGraphics();
		gc.setColor(Color.WHITE);
		gc.drawImage(image, 0, 0, null);
		return zoomImage;
	}
	
	public static void main(String[] args) throws IOException {
		File f = new File("F:\\group");
		File[] fileArr = f.listFiles();
		ImageVo vo = new ImageVo();
		vo.setNum(2);
		mergeImgs(Arrays.asList(fileArr), vo, "F:\\group\\");
	}
}