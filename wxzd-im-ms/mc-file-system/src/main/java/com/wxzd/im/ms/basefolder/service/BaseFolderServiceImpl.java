package com.wxzd.im.ms.basefolder.service;

import com.wxzd.im.ms.basefolder.entity.BaseFolder;
import com.wxzd.im.ms.basefolder.mapper.BaseFolderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BaseFolderServiceImpl implements BaseFolderService {
    @Autowired
    private BaseFolderMapper baseFolderMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return 0;
    }

    @Override
    public int insert(BaseFolder record) {
        return 0;
    }

    @Override
    public int insertSelective(BaseFolder record) {
        return 0;
    }

    @Override
    public BaseFolder selectByPrimaryKey(String id) {
        return this.baseFolderMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(BaseFolder record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(BaseFolder record) {
        return 0;
    }

    @Override
    public BaseFolder findByCode(String code) {
        return this.baseFolderMapper.findByCode(code);
    }

    @Override
    public Map<String,Object> queryList(){
        Map<String,Object> map=new HashMap<>();
        List<Map<String,Object>> list=this.baseFolderMapper.queryList();
        if(list.size()>0){
            for(Map<String,Object> objectMap:list){
                map.put(objectMap.get("id").toString(),objectMap.get("code"));
            }
        }
        return map;
    }
}
