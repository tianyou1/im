/**
 * @Title: ImageVo.java
 * @Package com.jade.filesystem.vo
 * @Description: TODO(用一句话描述该文件做什么)
 * @author cys
 * @date 2019年8月12日
 * @version V1.0
 */
package com.wxzd.im.ms.filesystem.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @ClassName: ImageVo
 * @Description: 图片整合VO
 * @author cys
 * @date 2019年8月12日
 */
@Data
@ApiModel(value = "ImageVo", description = "图片整合VO")
public class ImageVo {
	
	/**
	 * 初始宽度
	 */
	public static final int INIT_WIDTH = 132;
	
	/**
	 * 初始高度
	 */
	public static final int INIT_HEIGHT = 132;
	
	/**
	 * 初始整合方向
	 */
	public static final String INIT_DIRECT = "X";
	
	/**
	 * 按整个方向显示图片初始数据
	 */
	public static final int INIT_NUM = 1;
	
	/**
	 * 初始间隔像素，默认为8px
	 */
	public static final int INIT_SPACE = 8;

	/**
	 * 图片整合后宽度
	 */
	@ApiModelProperty(name = "width", value = "图片整合后高度，默认132")
	private int width = INIT_WIDTH;
	
	/**
	 * 图片整合后高度
	 */
	@ApiModelProperty(name = "height", value = "图片整合后高度，默认132")
	private int height = INIT_HEIGHT;
	
	/**
	 * 图片整合以哪个方向为准
	 */
	@ApiModelProperty(name = "direct", value = "图片整合以哪个方向为准，默认为X方向，可传值x或者X、y或者Y")
	private String direct = INIT_DIRECT;
	
	/**
	 * 图片整合标准方向上显示多少图片
	 */
	@ApiModelProperty(name = "num", value = "图片整合标准方向上显示多少图片，默认为1")
	private int num = INIT_NUM;
	
	/**
	 * 间隔像素
	 */
	@ApiModelProperty(name = "space", value = "间隔像素，默认为8px")
	private int space = INIT_SPACE;
	
	/**
	 * 需要整合图片ID
	 */
	@ApiModelProperty(name = "fileIds", value = "需要整合图片IDs、文件夹IDs、关联IDs，必填", required = true)
	private List<String> fileIds;
	
	/**
	 * 是否返回base64文件字符码
	 */
	@ApiModelProperty(name = "base64", value = "是否返回base64文件字符码，默认false")
	private boolean base64 = false;
	
	/**
	 * @Title: arrangeImgVo
	 * @Description: 整理图片整合数据
	 * @param vo
	 * @return ImageVo 返回类型
	 * @throws
	 */
	public static ImageVo arrangeImgVo(ImageVo vo, int fileSize) {
		vo = vo == null?new ImageVo():vo;
		if(vo.getWidth() <= INIT_WIDTH) {
			vo.setWidth(INIT_WIDTH);
		}
		if(vo.getHeight() <= INIT_WIDTH) {
			vo.setWidth(INIT_WIDTH);
		}
		if(StringUtils.isEmpty(vo.getDirect())) {
			vo.setDirect(INIT_DIRECT);
		}
		vo.setDirect(vo.getDirect().toUpperCase());
		if(!vo.getDirect().equals("X") || !vo.getDirect().equals("Y")) {
			vo.setDirect(INIT_DIRECT);
		}
		if(vo.getNum() < INIT_NUM) {
			vo.setNum(INIT_NUM);
		}
		if(fileSize < vo.getNum()) {
			vo.setNum(fileSize);
		}
		if(vo.getSpace() <= 0) {
			vo.setSpace(INIT_SPACE);
		}
		return vo;
	}
}
