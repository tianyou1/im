package com.wxzd.im.ms.filesystem.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取一些通用的配置
 */
@ConfigurationProperties(prefix="file.config")
@Component
public class FileConfigProperties {
    /**
     * 默认文件夹编码
     */
    private String defaultFolderId;

    private String visitPrefix;

    public String getDefaultFolderId() {
        return defaultFolderId;
    }

    public void setDefaultFolderId(String defaultFolderId) {
        this.defaultFolderId = defaultFolderId;
    }

    public String getVisitPrefix() {
        return visitPrefix;
    }

    public void setVisitPrefix(String visitPrefix) {
        this.visitPrefix = visitPrefix;
    }
}
