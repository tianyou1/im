package com.wxzd.im.ms.baselog.mapper;

import com.wxzd.im.ms.baselog.entity.BaseLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseLogMapper {
    int deleteByPrimaryKey(String id);

    int insert(BaseLog record);

    int insertSelective(BaseLog record);

    BaseLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseLog record);

    int updateByPrimaryKey(BaseLog record);
}