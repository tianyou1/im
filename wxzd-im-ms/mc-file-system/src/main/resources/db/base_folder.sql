/*
 Navicat Premium Data Transfer

 Source Server         : jade_file
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 119.29.151.20:18306
 Source Schema         : jade_file

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 15/12/2020 21:50:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_folder
-- ----------------------------
DROP TABLE IF EXISTS `base_folder`;
CREATE TABLE `base_folder`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一标识',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件夹名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件编号',
  `verified` smallint(6) NULL DEFAULT NULL COMMENT '是否需要token验证',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_alja2pnf23rmupw5r3re1t5o`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件夹基本信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of base_folder
-- ----------------------------
INSERT INTO `base_folder` VALUES ('1', '聊天', NULL, NULL, 'chat', NULL);
INSERT INTO `base_folder` VALUES ('2', '头像', NULL, NULL, 'headImage', NULL);
INSERT INTO `base_folder` VALUES ('3', '举报', NULL, NULL, 'report', NULL);
INSERT INTO `base_folder` VALUES ('4', '默认', NULL, NULL, 'default', NULL);

SET FOREIGN_KEY_CHECKS = 1;
