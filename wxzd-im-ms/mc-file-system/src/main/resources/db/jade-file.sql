/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50643
Source Host           : 127.0.0.1:3306
Source Database       : jade-file

Target Server Type    : MYSQL
Target Server Version : 50643
File Encoding         : 65001

Date: 2019-09-02 10:13:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_file
-- ----------------------------
DROP TABLE IF EXISTS `base_file`;
CREATE TABLE `base_file` (
  `id` varchar(32) NOT NULL COMMENT '唯一标识',
  `file_name` varchar(200) DEFAULT NULL COMMENT '文件名称',
  `file_size` bigint(20) DEFAULT NULL COMMENT '文件大小',
  `mime_type` varchar(100) DEFAULT NULL COMMENT '文件类型',
  `file_url` varchar(100) DEFAULT NULL COMMENT '文件存放路径',
  `suffix` varchar(100) DEFAULT NULL COMMENT '文件后缀',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `folder_id` varchar(32) DEFAULT NULL COMMENT '文件夹ID',
  `reference_id` varchar(255) DEFAULT NULL COMMENT '关联ID',
  `visit_url` varchar(255) DEFAULT NULL COMMENT '访问地址',
  `thumbnail_url` varchar(255) DEFAULT NULL COMMENT '缩略图地址',
  `md5` varchar(255) DEFAULT NULL COMMENT 'MD5值',
  PRIMARY KEY (`id`),
  KEY `FKca5cv99m4bjhocbky7o5i8aio` (`folder_id`),
  CONSTRAINT `FKca5cv99m4bjhocbky7o5i8aio` FOREIGN KEY (`folder_id`) REFERENCES `base_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for base_folder
-- ----------------------------
DROP TABLE IF EXISTS `base_folder`;
CREATE TABLE `base_folder` (
  `id` varchar(32) NOT NULL COMMENT '唯一标识',
  `name` varchar(200) DEFAULT NULL COMMENT '文件夹名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `code` varchar(100) DEFAULT NULL COMMENT '文件编号',
  `verified` smallint(6) DEFAULT NULL COMMENT '是否需要token验证',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for base_log
-- ----------------------------
DROP TABLE IF EXISTS `base_log`;
CREATE TABLE `base_log` (
  `id` varchar(32) NOT NULL COMMENT '唯一标识',
  `ip` varchar(100) DEFAULT NULL COMMENT 'IP',
  `method` varchar(100) DEFAULT NULL COMMENT '请求方法',
  `request_url` varchar(255) DEFAULT NULL COMMENT '请求路径',
  `request_param` varchar(255) DEFAULT NULL COMMENT '请求参数',
  `cost_time` bigint(20) DEFAULT NULL COMMENT '耗时',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `oper_type` smallint(6) DEFAULT NULL COMMENT '操作类型',
  `oper_type_desc` varchar(100) DEFAULT NULL COMMENT '操作类型描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
