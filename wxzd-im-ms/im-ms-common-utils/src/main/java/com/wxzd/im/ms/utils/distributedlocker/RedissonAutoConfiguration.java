package com.wxzd.im.ms.utils.distributedlocker;


import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wxzd.im.ms.utils.DistributeLockUtil;

@Configuration
@ConditionalOnClass(Config.class)
@EnableConfigurationProperties(RedissonProperties.class)
public class RedissonAutoConfiguration {

    @Autowired
    private RedissonProperties redssionProperties;

    /**
     * 	哨兵模式自动装配
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "redisson", name = "run-type", havingValue = "sentinel", matchIfMissing = false)
    RedissonClient redissonSentinel() {
        Config config = new Config();
        SentinelServersConfig serverConfig = config.useSentinelServers().addSentinelAddress(redssionProperties.getSentinelAddresses())
                .setMasterName(redssionProperties.getMasterName())
                .setTimeout(redssionProperties.getTimeout())
                .setMasterConnectionPoolSize(redssionProperties.getMasterConnectionPoolSize())
                .setSlaveConnectionPoolSize(redssionProperties.getSlaveConnectionPoolSize())
                .setDatabase(redssionProperties.getDatabase());
        
        if(StringUtils.isNotBlank(redssionProperties.getPassword())) {
            serverConfig.setPassword(redssionProperties.getPassword());
        }
        
        return Redisson.create(config);
    }

    /**
     *	 单机模式自动装配
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "redisson", name = "run-type", havingValue = "single", matchIfMissing = true)
    RedissonClient redissonSingle() {
        Config config = new Config();
        SingleServerConfig serverConfig = config.useSingleServer()
                .setAddress(redssionProperties.getAddress())
                .setTimeout(redssionProperties.getTimeout())
                .setConnectionPoolSize(redssionProperties.getConnectionPoolSize())
                .setConnectionMinimumIdleSize(redssionProperties.getConnectionMinimumIdleSize())
                .setDatabase(redssionProperties.getDatabase());
        
        if(StringUtils.isNotBlank(redssionProperties.getPassword())) {
            serverConfig.setPassword(redssionProperties.getPassword());
        }

        return Redisson.create(config);
    }

    /**
     * 	装配locker类，并将实例注入到DistributeLockUtil中
     * @return
     */
    @Bean
    IDistributedLock distributedLocker(RedissonClient redissonClient) {
    	RedissonDistributedLockImpl locker = new RedissonDistributedLockImpl();
        locker.setRedissonClient(redissonClient);
        DistributeLockUtil.setLocker(locker);
        return locker;
    }

}