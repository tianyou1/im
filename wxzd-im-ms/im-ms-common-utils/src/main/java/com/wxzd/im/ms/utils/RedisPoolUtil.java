package com.wxzd.im.ms.utils;

import com.wxzd.im.ms.utils.redisspool.IRedisPool;

import redis.clients.jedis.Jedis;
 
/**
 * Redis连接池工具类
 */
public class RedisPoolUtil {
	
	private static IRedisPool redisPool;
	public static void setRedisPool(IRedisPool pool) {
		redisPool = pool;
    }
	
	/**
	 * 获得连接
	 * @return Jedis
	 */
	public static Jedis getConn() { 
		return redisPool.getConn();  
    }
	
	//新版本用close归还连接
	public static void closeConn(){
		redisPool.closeConn();  
	}
	
}