package com.wxzd.im.ms.utils.redisspool;


import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wxzd.im.ms.utils.DistributeLockUtil;
import com.wxzd.im.ms.utils.RedisPoolUtil;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@ConditionalOnClass(Config.class)
@EnableConfigurationProperties(JedisProperties.class)
public class JedissAutoConfiguration {

    @Autowired
    private JedisProperties jedisProperties;

    /**
     * 	哨兵模式自动装配
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "redisson", name = "run-type", havingValue = "sentinel", matchIfMissing = false)
    RedissonClient redissonSentinel() {
        Config config = new Config();
		/*
		 * SentinelServersConfig serverConfig =
		 * config.useSentinelServers().addSentinelAddress(redssionProperties.
		 * getSentinelAddresses()) .setMasterName(redssionProperties.getMasterName())
		 * .setTimeout(redssionProperties.getTimeout())
		 * .setMasterConnectionPoolSize(redssionProperties.getMasterConnectionPoolSize()
		 * )
		 * .setSlaveConnectionPoolSize(redssionProperties.getSlaveConnectionPoolSize())
		 * .setDatabase(redssionProperties.getDatabase());
		 * 
		 * if(StringUtils.isNotBlank(redssionProperties.getPassword())) {
		 * serverConfig.setPassword(redssionProperties.getPassword()); }
		 */
        
        return Redisson.create(config);
    }

    /**
     *	 单机模式自动装配
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "jedis", name = "run-type", havingValue = "single", matchIfMissing = true)
    JedisPool jedissSingle() {
    	// 创建jedis池配置实例
		JedisPoolConfig config = new JedisPoolConfig();
		// 设置池配置项值
		config.setMaxTotal(jedisProperties.getPoolMaxActive());
		config.setMaxIdle(jedisProperties.getPoolMaxIdle());
		config.setMaxWaitMillis(jedisProperties.getPoolMaxWait());
		config.setTestOnBorrow(jedisProperties.isPoolTestOnBorrow());
		config.setTestOnReturn(jedisProperties.isPoolTestOnReturn());
		// 根据配置实例化jedis池
		JedisPool jedisPool = new JedisPool(config, jedisProperties.getRedisIp(),
				jedisProperties.getRedisPort(),
				jedisProperties.getTimeout(),
				jedisProperties.getPassword(),
				jedisProperties.getDatabase()
				);
		return jedisPool;
    }

    /**
     * 	装配locker类，并将实例注入到DistributeLockUtil中
     * @return
     */
    @Bean
    IRedisPool redisPool(JedisPool jedisPool) {
    	JedisRedisPoolImpl pool = new JedisRedisPoolImpl();
    	pool.setJedisPool(jedisPool);
        RedisPoolUtil.setRedisPool(pool);
        return pool;
    }

}