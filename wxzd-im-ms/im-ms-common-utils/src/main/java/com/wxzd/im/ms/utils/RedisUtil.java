package com.wxzd.im.ms.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;

public class RedisUtil {
  
  public static byte[] get(byte[] key) {
    byte[] value = null;
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      value = jedis.get(key);
    } finally {
    	RedisPoolUtil.closeConn();
    } 
    return value;
  }
  
  public static String flushDB() {
    String value = null;
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      value = jedis.flushDB();
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return value;
  }
  
  public static byte[] set(byte[] key, byte[] value) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      jedis.set(key, value);
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return value;
  }
  
  public static long geoadd(byte[] key, double longitude, double latitude, byte[] member) {
    Jedis jedis = null;
    long ret = 0L;
    try {
      jedis = RedisPoolUtil.getConn();
      ret = jedis.geoadd(key, longitude, latitude, member).longValue();
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return ret;
  }
  
  public static List<GeoRadiusResponse> georadius(byte[] key, double longitude, double latitude, double radius, GeoUnit unit) {
    Jedis jedis = null;
    List<GeoRadiusResponse> list = new ArrayList<>();
    try {
      jedis = RedisPoolUtil.getConn();
      list = jedis.georadius(key, longitude, latitude, radius, unit);
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return list;
  }
  
  public static Double geodist(byte[] key, byte[] member1, byte[] member2, GeoUnit unit) {
    Jedis jedis = null;
    Double dist = Double.valueOf(-1.0D);
    try {
      jedis = RedisPoolUtil.getConn();
      dist = jedis.geodist(key, member1, member2, unit);
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return dist;
  }
  
  public static List<GeoCoordinate> geopos(byte[] key, byte[]... members) {
    Jedis jedis = null;
    List<GeoCoordinate> ret = new ArrayList<>();
    try {
      jedis = RedisPoolUtil.getConn();
      ret = jedis.geopos(key, members);
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return ret;
  }
  
  public static Long incr(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.incr(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long decr(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.decr(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Set<byte[]> keys(byte[] pattern) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.keys(pattern);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Boolean exists(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.exists(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static byte[] lpop(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.lpop(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static long llen(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.llen(key).longValue();
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static long llen(String key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.llen(key).longValue();
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static List<byte[]> lrange(byte[] key, long start, long end) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.lrange(key, start, end);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static List<String> lrange(String key, long start, long end) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.lrange(key, start, end);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static String ltrim(byte[] key, long start, long end) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.ltrim(key, start, end);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static byte[] rpop(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.rpop(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static String rpop(String key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.rpop(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long lpush(byte[] key, byte[] d) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.lpush(key, new byte[][] { d });
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long lpush(String key, String d) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.lpush(key, new String[] { d });
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long rpush(byte[] key, byte[] d) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.rpush(key, new byte[][] { d });
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long rpush(String key, String d) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.rpush(key, new String[] { d });
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long hlen(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hlen(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static List<byte[]> hmget(byte[] key, byte[]... fields) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hmget(key, fields);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static List<byte[]> hvals(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hvals(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Map<byte[], byte[]> hgetAll(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hgetAll(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Map<String, String> hgetAll(String key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hgetAll(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static byte[] hget(byte[] key, byte[] field) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hget(key, field);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static String hget(String key, String field) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hget(key, field);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static String type(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.type(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static <T> Long hset(byte[] key, byte[] field, byte[] value) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hset(key, field, value);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static <T> Long hset(String key, String field, String value) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hset(key, field, value);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long hdel(byte[] key, byte[]... fields) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hdel(key, fields);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long hdel(String key, String... fields) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hdel(key, fields);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static String hmset(byte[] key, Map<byte[], byte[]> hash) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hmset(key, hash);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static <T> Long hsetnx(byte[] key, byte[] field, byte[] value) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hsetnx(key, field, value);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static boolean hexists(String key, String field) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hexists(key, field);
    } finally {
      RedisPoolUtil.closeConn();
    }
  }
  
  public static Long sadd(byte[] key, byte[]... members) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.sadd(key, members);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long sadd(String key, String... members) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.sadd(key, members);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Set<byte[]> smembers(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.smembers(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static byte[] srandmember(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.srandmember(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Set<String> smembers(String key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.smembers(key);
    } catch (Exception e) {
      RedisPoolUtil.closeConn();
      e.printStackTrace();
    } finally {
      RedisPoolUtil.closeConn();
    } 
    return null;
  }
  
  public static Set<byte[]> sinter(byte[]... keys) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.sinter(keys);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Set<String> sinter(String... keys) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.sinter(keys);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static long srem(String key, String... members) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.srem(key, members).longValue();
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static long srem(byte[] key, byte[]... members) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.srem(key, members).longValue();
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Boolean sismember(byte[] key, byte[] member) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.sismember(key, member);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Set<byte[]> hkeys(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.hkeys(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long scard(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.scard(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long setnx(byte[] key, byte[] value) throws Exception {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.setnx(key, value);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static String watch(byte[]... keys) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.watch(keys);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long delete(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.del(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long expired(byte[] key, int seconds) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.expire(key, seconds);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long zadd(byte[] key, double score, byte[] member) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zadd(key, score, member);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long zcard(byte[] key) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zcard(key);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long zcount(byte[] key, double min, double max) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zcount(key, min, max);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long zrem(byte[] key, byte[]... members) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zrem(key, members);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Set<byte[]> zrevrange(byte[] key, int start, int end) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zrevrange(key, start, end);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Long zremrangeByRank(byte[] key, int start, int end) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zremrangeByRank(key, start, end);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static Double zscore(byte[] key, byte[] member) {
    Jedis jedis = null;
    try {
      jedis = RedisPoolUtil.getConn();
      return jedis.zscore(key, member);
    } finally {
      RedisPoolUtil.closeConn();
    } 
  }
  
  public static synchronized Long genIdentify(byte[] key) {
    Long id = null;
    byte[] oldIdByte = get(key);
    if (oldIdByte == null) {
      id = Long.valueOf(0L);
    } else {
      id = new Long(new String(get(key)));
    } 
    id = Long.valueOf(id.longValue() + 1L);
    set(key, id.toString().getBytes());
    return id;
  }
  
}
