package com.wxzd.im.ms.utils.distributedlocker;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public interface IDistributedLock {
	
	/**
     * 	加锁
     * @param lockKey
     * @return
     */
	Lock lock(String lockKey);
	
	/**
     *	 带超时的锁
     * @param lockKey
     * @param timeout 超时时间   单位：秒
     */
	Lock lock(String lockKey, int timeout);
	
	/**
     * 	带超时的锁
     * @param lockKey
     * @param unit 时间单位
     * @param timeout 超时时间
     */
	Lock lock(String lockKey, TimeUnit unit, int timeout);
    
	/**
     * 	尝试获取锁 (单位秒：s)
     * @param lockKey
     * @param waitTime 最多等待时间
     * @param leaseTime 上锁后自动释放锁时间
     * @return
     */
	boolean tryLock(String lockKey, int waitTime, int leaseTime);
	
	/**
     * 	尝试获取锁
     * @param lockKey
     * @param unit 时间单位
     * @param waitTime 最多等待时间
     * @param leaseTime 上锁后自动释放锁时间
     * @return
     */
	boolean tryLock(String lockKey, TimeUnit unit, int waitTime, int leaseTime);
	
	/**
     * 	释放锁
     * @param lockKey
     */
	void unlock(String lockKey);
	
	/**
     *	 释放锁
     * @param lock
     */
	void unlock(Lock lock);
	
}