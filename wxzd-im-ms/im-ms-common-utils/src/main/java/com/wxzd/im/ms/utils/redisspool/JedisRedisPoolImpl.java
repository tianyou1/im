package com.wxzd.im.ms.utils.redisspool;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class JedisRedisPoolImpl implements IRedisPool {
	
	private static JedisPool jedisPool;

	// 把redis连接对象放到本地线程中
	private static ThreadLocal<Jedis> local = new ThreadLocal<Jedis>();

	/**
	 * 获得连接
	 * 
	 * @return Jedis
	 */
	@Override
	public Jedis getConn() {
		// Redis对象
		Jedis jedis = local.get();
		if (jedis == null) {
			jedis = jedisPool.getResource();
			local.set(jedis);
		}
		return jedis;
	}

	/**
	 * 归还连接
	 */
	@Override
	public void closeConn() {
		// 从本地线程中获取
		Jedis jedis = local.get();
		if (jedis != null) {
			jedis.close();
		}
		local.set(null);
	}
	
	public void setJedisPool(JedisPool pool) {
		jedisPool = pool;
    }

}
