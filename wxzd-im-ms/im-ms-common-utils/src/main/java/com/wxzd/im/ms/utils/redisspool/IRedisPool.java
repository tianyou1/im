package com.wxzd.im.ms.utils.redisspool;

import redis.clients.jedis.Jedis;

public interface IRedisPool {
	
	Jedis getConn();
	
	void closeConn();
	
}
