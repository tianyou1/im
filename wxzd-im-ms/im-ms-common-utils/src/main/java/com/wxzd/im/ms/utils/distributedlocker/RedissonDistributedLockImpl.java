package com.wxzd.im.ms.utils.distributedlocker;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class RedissonDistributedLockImpl implements IDistributedLock {
    
    private RedissonClient redissonClient;

    @Override
    public Lock lock(String lockKey) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.lock();
        return lock;
    }

    @Override
    public Lock lock(String lockKey, int leaseTime) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.lock(leaseTime, TimeUnit.SECONDS);
        return lock;
    }
    
    @Override
    public Lock lock(String lockKey, TimeUnit unit ,int timeout) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.lock(timeout, unit);
        return lock;
    }
    
    @Override
    public boolean tryLock(String lockKey, int waitTime, int leaseTime) {
        RLock lock = redissonClient.getLock(lockKey);
        try {
            return lock.tryLock(waitTime, leaseTime, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            return false;
        }
    }
    
    @Override
    public boolean tryLock(String lockKey, TimeUnit unit, int waitTime, int leaseTime) {
        RLock lock = redissonClient.getLock(lockKey);
        try {
            return lock.tryLock(waitTime, leaseTime, unit);
        } catch (InterruptedException e) {
            return false;
        }
    }
    
    @Override
    public void unlock(String lockKey) {
    	//获取所对象
        RLock lock = redissonClient.getLock(lockKey);
        //释放锁（解锁）
        if(lock.isLocked()){
            if(lock.isHeldByCurrentThread()){
            	lock.unlock();
            }
        }
    }
    
    @Override
    public void unlock(Lock lock) {
    	RLock lockr = (RLock) lock;
    	//释放锁（解锁）
        if(lockr.isLocked()){
            if(lockr.isHeldByCurrentThread()){
            	lockr.unlock();
            }
        }
    }

    public void setRedissonClient(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }
}