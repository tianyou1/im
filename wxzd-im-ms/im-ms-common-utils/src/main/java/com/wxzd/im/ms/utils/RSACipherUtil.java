package com.wxzd.im.ms.utils;

import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

/**
 * @author ratel
 * RSA加解密工具类
 */
public class RSACipherUtil {
    private final static String CRYPTO_METHOD = "RSA";
//    private final static String CYPHER = "RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING";
    private final static String CYPHER = "RSA";
    private static String PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjkdXEmh/i+KCz5IFQgkXeXGajrY2kPZ1SROlaxKDPIqGJISePAXnChNd96XeLL34vHwpO6dTjlL3Z6aZoP6VlLVbAId6/Qypo5bwKL+n+EZEM69OhdhxIj9nHZhHaI2Boz8mVBU/2uYGzqoJBHLSiBBUOL/1wci5GwP54WNFmKUjkU1bKlNqVDoRKJKaa1dQyyRIxnmKxrSRY65/MK91sO89cW9lkCAxJJP31kC2dvyyNevsbhjy1cZ+nkDlLDjIdT7HuIOM7QPfG1P6B97EBVm0FKUlZKmPibhjZcRNyptNtwc1w9ys0VptClkAPDlkG83HSYzCEvGFA2Ve874WNwIDAQAB";
    private static String PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCOR1cSaH+L4oLPkgVCCRd5cZqOtjaQ9nVJE6VrEoM8ioYkhJ48BecKE133pd4svfi8fCk7p1OOUvdnppmg/pWUtVsAh3r9DKmjlvAov6f4RkQzr06F2HEiP2cdmEdojYGjPyZUFT/a5gbOqgkEctKIEFQ4v/XByLkbA/nhY0WYpSORTVsqU2pUOhEokpprV1DLJEjGeYrGtJFjrn8wr3Ww7z1xb2WQIDEkk/fWQLZ2/LI16+xuGPLVxn6eQOUsOMh1Pse4g4ztA98bU/oH3sQFWbQUpSVkqY+JuGNlxE3Km023BzXD3KzRWm0KWQA8OWQbzcdJjMIS8YUDZV7zvhY3AgMBAAECggEAL7+NXyYmt+FamSKeFR1L/Xu8rOxFv7yWe+HV/+sElArBHfxLxkUM1nrNbFEEEOvoUAkzJUvwZLE/Sk/E+xy1QmTil3jgM5BOVhJPor0/N7kNJnNM3OpYfW/dwTbbsCDdQtXsnQdMmL/dxPmXcSC/ut4QBcOWTGR6irB46cURFDrDvA5/sim1o1a0y3boGLVGwNOMSHBNvA3Qbx3C5w7/8YCH/d95fgr2hdRB+TCIvvzFx+gjONch6/wuOvOlrAsr+zsJvLWtoohRxxYxucSrWSZGICfndwi5iWDnWQErHTF+2a3GCCi+SE3TxZvx/4pyCADSuMGPamKkj190+BN0kQKBgQDyhDf2j62vPE2yEpSeHFsIYHGFbTv7jmJFENSsEDNOR2N9QYvNyfEBZZDVgMUvDCyOdLbrU20DAAeykjku6fQkJQcoXOWeJWrr2uq4fB69WuHYQc0b62iB0gZnrCFJy65i2dc/7bda4ni0P+Iy/lgnBevuj6mSFH19fUHB4voQCwKBgQCWMGsh+L+SqzS+mRSAZgQKhDtgBFW4myV68Nf8RlaAAbFCW+SICwndEqsMY1/N6W78tTIspeBaOwgH0IHHuMWneYCjP5iBM951OcDwWVKxyuCHpDcu2VMci42Q/dybTjrs1D2sz5JyfQXByH0Xdh3uoTiHiJo0oIt13dq+/RESBQKBgHEzVGgLsvfr/xxS3fpbpAZp9jNgxLHsSWSRsaPLQBeW7RKniNl7HLsZLJzsKZ6HYTjxs/xuiaDv36fz5ytF20cP3aJykl/aeWk0W4HXtgWPKYJnXJVesf5/CaxUxYkLwQ+1PWtCy+Zpdgw1EaHZ5U35zqIhfvaIB/eOgyY8i5r3AoGBAIaOLH1MGX4yytdz2E1MmJQ/cd6NI6t3vDWP70GCVt7qJUA5id5kXVrl7b70jRGy8I+RWrlrhzvvpTszEAK6RMLNdXVMdbpTmRtXOcdbj13gUBbWBeaAKBjr4lIjTnEJvnsNj4/yi9bssaqyj3fkh96SLE4pEVbS+v8HW4I+wfttAoGBAJCVCv1rYC6VUx4Zy02BIUvTf3BNDoUF3cpDzGVJKMSA3tjmay6Cf0wHJXUP7EbhAo+sHPXnGKhq8mrVHztpL5/Hp3lq7pykJW2uXMttviDWZuf3aiGEqztv2jwCgLkpql4BZBvRpWYlDlJcUlU3kfhKhJ9fFFH0P9U1BhhhvmpF";
    private static String CHARSET = "UTF-8";
    

    /**
     * @param clearText 需加密的字符串
     * @return 加密后的字符串
     */
    public static String encrypt(String clearText) {
        String encryptedBase64 = "";
        try {
            KeyFactory keyFac = KeyFactory.getInstance(CRYPTO_METHOD);
            KeySpec
                    keySpec =
                    new X509EncodedKeySpec(Base64Util.decode(PUB_KEY.trim()));
            Key key = keyFac.generatePublic(keySpec);
            final Cipher cipher = Cipher.getInstance(CYPHER);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encryptedBytes = cipher.doFinal(clearText.getBytes(CHARSET));
            encryptedBase64 = new String(Base64Util.encode(encryptedBytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedBase64.replaceAll("(\\r|\\n)", "");
    }

    /**
     * @param encryptedBase64 加密后的字符串
     * @return 解密后的字符串
     */
    public static String decrypt(String encryptedBase64) {
    	encryptedBase64 = encryptedBase64.replaceAll(" ", "+");
        String decryptedString = "";
        try {
            KeyFactory keyFac = KeyFactory.getInstance(CRYPTO_METHOD);
            KeySpec keySpec = new PKCS8EncodedKeySpec(
            		Base64Util.decode(PRIVATE_KEY.trim()));
            Key key = keyFac.generatePrivate(keySpec);
            final Cipher cipher = Cipher.getInstance(CYPHER);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedBytes = Base64Util.decode(encryptedBase64);
            byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
            decryptedString = new String(decryptedBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedString;
    }
    
}