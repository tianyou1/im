package com.wxzd.im.ms.utils.redisspool;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "jedis")
public class JedisProperties {

    private String redisIp;
    
    private int redisPort;
    
    private String masterName;
    
    private String[] sentinelAddresses;

    private String password;
    
    private int database = 8;
    
    private int timeout = 1000;
    
    private int poolMaxActive = 50;
    
    private int poolMaxIdle= 50;

    private int poolMinIdle = 50;

    private long poolMaxWait = 500;
    
    private boolean poolTestOnBorrow = true;
    
    private boolean poolTestOnReturn = true;

	public String getRedisIp() {
		return redisIp;
	}

	public void setRedisIp(String redisIp) {
		this.redisIp = redisIp;
	}

	public int getRedisPort() {
		return redisPort;
	}

	public void setRedisPort(int redisPort) {
		this.redisPort = redisPort;
	}

	public String getMasterName() {
		return masterName;
	}

	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	public String[] getSentinelAddresses() {
		return sentinelAddresses;
	}

	public void setSentinelAddresses(String[] sentinelAddresses) {
		this.sentinelAddresses = sentinelAddresses;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getDatabase() {
		return database;
	}

	public void setDatabase(int database) {
		this.database = database;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getPoolMaxActive() {
		return poolMaxActive;
	}

	public void setPoolMaxActive(int poolMaxActive) {
		this.poolMaxActive = poolMaxActive;
	}

	public int getPoolMaxIdle() {
		return poolMaxIdle;
	}

	public void setPoolMaxIdle(int poolMaxIdle) {
		this.poolMaxIdle = poolMaxIdle;
	}

	public int getPoolMinIdle() {
		return poolMinIdle;
	}

	public void setPoolMinIdle(int poolMinIdle) {
		this.poolMinIdle = poolMinIdle;
	}

	public long getPoolMaxWait() {
		return poolMaxWait;
	}

	public void setPoolMaxWait(long poolMaxWait) {
		this.poolMaxWait = poolMaxWait;
	}

	public boolean isPoolTestOnBorrow() {
		return poolTestOnBorrow;
	}

	public void setPoolTestOnBorrow(boolean poolTestOnBorrow) {
		this.poolTestOnBorrow = poolTestOnBorrow;
	}

	public boolean isPoolTestOnReturn() {
		return poolTestOnReturn;
	}

	public void setPoolTestOnReturn(boolean poolTestOnReturn) {
		this.poolTestOnReturn = poolTestOnReturn;
	}
	
}
