package com.wxzd.im.ms.utils;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import com.wxzd.im.ms.utils.distributedlocker.IDistributedLock;


/**
 * 	分布式锁工具类
 * @author ratel
 * 
 */
public class DistributeLockUtil {
    private static IDistributedLock distributelocker;
    
    public static void setLocker(IDistributedLock locker) {
        distributelocker = locker;
    }
    
    /**
     * 	加锁
     * @param lockKey
     * @return
     */
    public static Lock lock(String lockKey) {
        return distributelocker.lock(lockKey);
    }

    /**
     * 	带超时的锁
     * @param lockKey
     * @param timeout 超时时间   单位：秒
     */
    public static Lock lock(String lockKey, int timeout) {
        return distributelocker.lock(lockKey, timeout);
    }
    
    /**
     *	 带超时的锁
     * @param lockKey
     * @param unit 时间单位
     * @param timeout 超时时间
     */
    public static Lock lock(String lockKey, TimeUnit unit ,int timeout) {
        return distributelocker.lock(lockKey, unit, timeout);
    }
    
    /**
     * 	尝试获取锁
     * @param lockKey
     * @param waitTime 最多等待时间
     * @param leaseTime 上锁后自动释放锁时间
     * @return
     */
    public static boolean tryLock(String lockKey, int waitTime, int leaseTime) {
        return distributelocker.tryLock(lockKey, TimeUnit.SECONDS, waitTime, leaseTime);
    }
    
    /**
     *	 尝试获取锁
     * @param lockKey
     * @param unit 时间单位
     * @param waitTime 最多等待时间
     * @param leaseTime 上锁后自动释放锁时间
     * @return
     */
    public static boolean tryLock(String lockKey, TimeUnit unit, int waitTime, int leaseTime) {
        return distributelocker.tryLock(lockKey, unit, waitTime, leaseTime);
    }
    
    /**
     * 	释放锁
     * @param lockKey
     */
    public static void unlock(String lockKey) {
        distributelocker.unlock(lockKey);
    }
    
    /**
     * 	释放锁
     * @param lock
     */
    public static void unlock(Lock lock) {
        distributelocker.unlock(lock);
    }
    
}