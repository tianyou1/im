/*弹出层*/
/*
	参数解释：
	title	标题
	url		请求的url
	id		需要操作的数据id
	w		弹出层宽度（缺省调默认值）
	h		弹出层高度（缺省调默认值）
*/
function layer_show(title,url,w,h){
    if (title == null || title == '') {
        title=false;
    };
    if (url == null || url == '') {
        url="404.html";
    };
    if (w == null || w == '') {
        w=800;
    };
    if (h == null || h == '') {
        h=($(window).height() - 50);
    };
    layer.open({
        type: 2,
        area: [w+'px', h +'px'],
        fix: false, //不固定
        maxmin: true,
        shade:0.4,
        title: title,
        content: url
    });
}
/*关闭弹出框口*/
function layer_close(){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}


function resolvingDate(date){
//date是传入的时间
    if(date==null){
        return "";
    }
    var d = new Date(date);
    var month = (d.getMonth() + 1) < 10 ? '0'+(d.getMonth() + 1) : (d.getMonth() + 1);
    var day = d.getDate()<10 ? '0'+d.getDate() : d.getDate();
    var hours = d.getHours()<10 ? '0'+d.getHours() : d.getHours();
    var min = d.getMinutes()<10 ? '0'+d.getMinutes() : d.getMinutes();
    var sec = d.getSeconds()<10 ? '0'+d.getSeconds() : d.getSeconds();
    var times=d.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + min + ':' + sec;
    return times
}


function resolvingSomeDate(date){
//date是传入的时间
    var d = new Date(date);
    var month = (d.getMonth() + 1) < 10 ? '0'+(d.getMonth() + 1) : (d.getMonth() + 1);
    var day = d.getDate()<10 ? '0'+d.getDate() : d.getDate();
    var times=d.getFullYear() + '-' + month + '-' + day ;
    return times
}

/**
 * 历史记录处理
 * @param d
 * @returns {string}
 */
function handleContent(d) {
    var res=d.content;
    var type=d.messagetype;
    //如果是msgString
    var html='';
    var content='';
    content=$.parseJSON(res);
    //文本及表情消息
    if(type==2){
        if(res.includes("\"msgString\"\:")){
            html=content.msgString;
        }else {
            html=content;
        }
    }
    //图片消息
    if(type==3){
        html= '<img style="display: inline-block; height: 100%;" src= "' +fileSystemServer+content.msgString + '">';
    }
    //文件（content:{fileName:文件名,fileSize:文件大小(string),fileUrl:文件路径，fileType:文件类型，filished：是否下载（boolean）}）
    if(type==4){
        html= '<a href="' +fileSystemServer+ content.fileUrl + '"  download="">文件下载</a>';
    }

    //离线语音(消息的content:{url:xxx,time:2})
    if(type==16){
        html= '<audio src="' + content.url + '" controls="controls"></audio>';
    }

    //发送小视频 ( content{img:http, time:2, video:http})
    if(type==30){
        html= '<video width="320" height="240" controls="controls">' +
            '<source src="' + content.vedioUrl + '" type="video/mov"/>' +
            '</video>';
    }

    //表情云（content:{msgCodes:    ,msgString:     ,msgType:     ,aiteId:  list<Long>}）
    if(type==34){
        html=content.msgString;
    }

    //请求视频通话（content 为1请求，content为0 取消（接通前取消））
    if(type==44){
        if(content==1){
            html="视频通话请求";
        }else if(content==0) {
            html="视频通话请求，未接通取消";
        }else if(content==3){
            html="视频通话挂断";
        }else {
            html="未知";
        }
    }

    //阅后即焚
    if(type==46){

    }

    if(html==''){
        html=res;
    }
    return html;
}



var start = {
    elem: "#start",
    format: "YYYY-MM-DD hh:mm:ss",
    /* min: laydate.now(),*/
    max: "2099-06-16 23:59:59",
    istime: true,
    istoday: false,
    choose: function (datas) {
        end.min = datas;
        end.start = datas
    }
};
var end = {
    elem: "#end",
    format: "YYYY-MM-DD hh:mm:ss",
    //min: laydate.now(),
    max: "2099-06-16 23:59:59",
    istime: true,
    istoday: false,
    choose: function (datas) {
        start.max = datas
    }
};



/**
 *
 */
//扩展Date的format方法
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    }
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}
/**
 *转换日期对象为日期字符串
 * @param date 日期对象
 * @param isFull 是否为完整的日期数据,
 *               为true时, 格式如"2000-03-05 01:05:04"
 *               为false时, 格式如 "2000-03-05"
 * @return 符合要求的日期字符串
 */
function getSmpFormatDate(date, isFull) {
    var pattern = "";
    if (isFull == true || isFull == undefined) {
        pattern = "yyyy-MM-dd hh:mm:ss";
    } else {
        pattern = "yyyy-MM-dd";
    }
    return getFormatDate(date, pattern);
}
/**
 *转换当前日期对象为日期字符串
 * @param date 日期对象
 * @param isFull 是否为完整的日期数据,
 *               为true时, 格式如"2000-03-05 01:05:04"
 *               为false时, 格式如 "2000-03-05"
 * @return 符合要求的日期字符串
 */

function getSmpFormatNowDate(isFull) {
    return getSmpFormatDate(new Date(), isFull);
}
/**
 *转换long值为日期字符串
 * @param l long值
 * @param isFull 是否为完整的日期数据,
 *               为true时, 格式如"2000-03-05 01:05:04"
 *               为false时, 格式如 "2000-03-05"
 * @return 符合要求的日期字符串
 */

function getSmpFormatDateByLong(l, isFull) {
    return getSmpFormatDate(new Date(l), isFull);
}
/**
 *转换long值为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
 * @return 符合要求的日期字符串
 */

function getFormatDateByLong(l, pattern) {
    return getFormatDate(new Date(l), pattern);
}
/**
 *转换日期对象为日期字符串
 * @param l long值
 * @param pattern 格式字符串,例如：yyyy-MM-dd hh:mm:ss
 * @return 符合要求的日期字符串
 */
function getFormatDate(date, pattern) {
    if (date == undefined) {
        date = new Date();
    }
    if (pattern == undefined) {
        pattern = "yyyy-MM-dd hh:mm:ss";
    }
    return date.format(pattern);
}