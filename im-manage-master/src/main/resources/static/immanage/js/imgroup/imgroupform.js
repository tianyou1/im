/**
 * 
 */
layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'form','layer' ], function() {
	var $ = layui.$, form = layui.form,layer=layui.layer;
    //获取父窗口的type
    var type=parent.layui.$("#type").val();
	var param = {};
	param.id = parent.layui.$("#edit_id").val();
	$.ajax({
		type : 'POST',
		url : '/imgroup/read',
		data : param,
		success : function(res) {
            //定位选项卡
            $("#liOne").removeClass('layui-this');
            $("#liTwo").removeClass('layui-this');
            $("#liThree").removeClass('layui-this');
            //获取父窗口的type
            var type=parent.layui.$("#type").val();
		    if(type==3){
                $("#liThree").addClass('layui-this');
                //模拟点击
                var liThree=document.getElementById('liThree');
                liThree.click();
            }else {
                $("#liOne").addClass('layui-this');
            }
            //给第一个form 填充数据
			form.val('lay-table-form', res.data);
		    //其他操作
            $("#headurl").attr("src",fileSystemServer+res.data.headurl);
            //阅后即焚状态
            var readDelStatus=res.data.isEphemeralChat;
            if(readDelStatus==1){
				$("#time").css('display','block')
			}else {
                $("#time").css('display','none')
			}
			$("#groupId").val(res.data.id);
		}
	});
	form.on('submit(lay-table-submit)', function(data) {
		var field = data.field; // 获取提交的字段
		var url = "";
		if ( field.id==="" ){
			url = '/imgroup/add';
		} else {
			url = '/imgroup/update';
		}
		// 提交 Ajax 成功后，静态更新表格中的数据
		$.ajax({
			type : 'POST',
			url : url,
			data : field,
			success : function(res) {
				layer.msg(res.msg);
			}
		});

	});
});

/**
 * 通过群成员id查找在群内的聊天记录
 */
function queryChatHistory(memberId) {
    queryMemberList(3);
    // 提交 Ajax 成功后，静态更新表格中的数据
    var $ = layui.$, form = layui.form,layer=layui.layer,table=layui.table;
    $("#content").val("");
    $("#sendTime").val("");
    $("#memberId").val(memberId);
    layui.config({
        base : '../../layuiadmin/' // 静态资源所在路径
    }).use([ 'index', 'table' ], function() {
        var $ = layui.$, form = layui.form, table = layui.table;
        var param = {};
        param.groupId = parent.layui.$("#edit_id").val();//groupId
        param.id = $("#memberId").val();
        table.render({
            id: 'lay-table-sec',
            elem: '#lay-table-sec',
            url: '/imgroup/queryHistory', // 模拟接口
            where: param,
            method: 'post',
            //toolbar : 'false',
            //defaultToolbar: ['filter', 'exports', 'print'],
            cols: [[ {
                type: 'numbers',
                title: '序号'
            }, {
                field: 'imageiconurl',
                title: '头像',
                templet: function (d) {
                    return '<img style="display: inline-block; height: 100%;" src= "' + fileSystemServer+d.imageiconurl + '">';
                }
            }, {
                field: 'fromname',
                title: '用户'
            }, {
                field: 'fromid',
                title: 'ID号'
            }, {
                field: 'content',
                title: '内容',
                templet:  function (d) {
                   return handleContent(d);
                }
            }, {
                field: 'sendtime',
                width: 160,
                title: '发送时间',
                templet: '<div>{{ layui.util.toDateString(d.sendtime, "yyyy-MM-dd HH:mm:ss") }}</div>'
            }, {
                title : '操作',
                width : 100,
                align : 'center',
                fixed : 'right',
                toolbar : '#lay-table-tool'
            }]],
            page: true,
            limit: 10,
            height: 'full-200',
            text: '对不起，加载出现异常！'
        });


        // 监听行工具事件
        table.on('tool(lay-table-sec)', function(obj) {
            var data = obj.data // 获得当前行数据
                , layEvent = obj.event; // 获得 lay-event 对应的值
            if (layEvent === 'viewConText') {
                active.viewConText(data.id,data.fromid);
            }
        });

        //监听搜索
        form.on('submit(lay-table-search-sec)', function (data) {
            var field = data.field;
            // 执行重载
            table.reload('lay-table-sec', {
                where: field
            });
            return false;
        });

        //事件
        var active = {
            //查看上下文
            viewConText:function(id,fromid){
                $("#groupId").val(parent.layui.$("#edit_id").val());
                $("#msgId").val(id);
                $("#userId").val(fromid);
                viewContextOpt('查看上下文','/impages/group/memberhistorylist','800','500');

            },
            reload: function() {
                table.reload('lay-table', {
                    where: {}
                });
            }
        }
    });


    layui.use('laydate', function() {
        var laydate = layui.laydate;
        laydate.render({
            elem: '#sendTime', //指定元素
            format: 'yyyy-MM-dd',
            type: 'date'
        });
    });
}

/**
 * 查看上下文
 * @param title
 * @param url
 * @param id
 * @param w
 * @param h
 */
function viewContextOpt(title, url,  w, h) {
    layer_show(title, url, w, h);
}



/**
 * 通过群成员id获取群成员资料
 */
function queryMemberDetail(id) {

    // 提交 Ajax 成功后，静态更新表格中的数据
    var $ = layui.$, form = layui.form,layer=layui.layer;
    $.ajax({
        type : 'POST',
        url : '/imgroup/queryMemberDetail',
        data : {id:id},
        success : function(res) {
            console.log(res);
        	var name=res.data.nickName;
            var memberId=res.data.id;
            var sex=res.data.sex;
            var mobile=res.data.mobile;
            var sign=res.data.sign;
            if(sex==null){
            	sex='';
			}
            if(mobile==null){
                mobile='';
            }
            if(sign==null){
                sign='';
            }
        	var html='';
            html+='<div class="right-member">';
        	html+='昵称：'+name+'<br>'+'成员ID：'+memberId+'<br>'+'性别：'+sex+'<br>'+'手机号码：'+mobile+'<br>'+'签名：'+sign+'<br><br>';

            //html+='<input class="btn-history" type="button"   id="queryHistory" lay-event="chatHistory" onclick="queryChatHistory('+memberId+')"  value="查看聊天记录">';
        	html+='</div>';

        	$("#memberDetail").html(html);
        }
    });
}
/**
 * 通过输入的值对应到id或昵称查找群成员
 */
function queryMemberList(type) {
    // 提交 Ajax 成功后，静态更新表格中的数据
    var $ = layui.$, form = layui.form,layer=layui.layer;
	var groupId=parent.layui.$("#edit_id").val();
    var searchValue='';
	if(type==3){
        searchValue=$("#searchVal").val();
    }else {
        searchValue=$("#searchValue").val();
    }
    $.ajax({
        type : 'POST',
        url : '/imgroup/queryMemberList',
        data : {page:1,limit:2000,groupId:groupId,searchValue:searchValue},
        success : function(res) {
            $('ol li').remove();
            var members= new Array();
            members=res.data;
            var url='';
            var name='';
            var id='';
            var roleCode='';
            console.log(res);
			for(var i=0;i<members.length;i++){
				//增加li
				var li=document.createElement("li");
				url=members[i].headUrl;
				name=members[i].nickName;
				id=members[i].id;
                roleCode=members[i].role;
                var role='';
                if(roleCode==1){
                	role='<span class="li-span">群主</span>';
				}else if(roleCode==2){
                	role='<span class="li-span">管理员</span>';
				}
				//3查询聊天记录2查询成员详情
				if(type==3){
                    li.innerHTML='<div onclick="queryChatHistory('+id+')"  class="li-div"><div class="li-div-left"><image  class="li-img" src="'+fileSystemServer+url+'"> </div>'+'<div class="li-div-right" ">'+name+'(成员id:'+id+') </div>'+'<div class="li-div-right">'+role+'</div>' +'</div>';
				    document.getElementById('memberLists').appendChild(li);
                }else if(type==2) {
                    li.innerHTML='<div onclick="queryMemberDetail('+id+')"  class="li-div"><div class="li-div-left"><image  class="li-img" src="'+fileSystemServer+url+'"> </div>'+'<div class="li-div-right" ">'+name+'(成员id:'+id+') </div>'+'<div class="li-div-right">'+role+'</div>' +'</div>';
                    document.getElementById('memberList').appendChild(li);
                }
			}


        }
    });
}

