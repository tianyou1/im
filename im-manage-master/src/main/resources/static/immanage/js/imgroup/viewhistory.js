/**
 * 
 */
function bindData() {
    var param = {};
    //群id
    param.groupId = parent.layui.$("#groupId").val();
    //群成员id
    param.id=0;
    param.page=1;
    param.limit=0;
    //选择的msgId
    var selectMsgId=parent.layui.$("#msgId").val();
    var selecUserId=parent.layui.$("#userId").val();
    //获取全部的聊天记录
    $.ajax({
        type : 'POST',
        url : '/imgroup/queryHistory',
        data : param,
        success : function(res) {

            var resArray=new Array();
            //获取聊天记录数组
            resArray=res.data;
            var html='';
            var time=0;
            for(var i=0;i<resArray.length;i++){
                //判断是别人的消息还是自己的消息
                var sendTime=getFormatDateByLong(resArray[i].sendtime,"yyyy-MM-dd hh:mm:ss");


                var div=document.createElement("div");
                //时间差
                var diffTime=resArray[i].sendtime-time;

                if(diffTime>=0){
                    if(i>0){
                        html+='<div class="time" style="margin-top: 72px">'+sendTime+'</div>';
                    }else {
                        html+='<div class="time" style="margin-top: 20px">'+sendTime+'</div>';
                    }
                }
                if(resArray[i].fromid==selecUserId){
                    //如果是自己的消息放在右侧
                    html+='<div class="list me">';

                }else {
                    //如果是别人的消息放在左侧
                    html+='<div class="list">';

                }

                if(selectMsgId==resArray[i].id){
                    html+='<div class="content select" id="msg-'+resArray[i].id+'">'+handleContent(resArray[i])+'</div>';
                    html+='<div class="tri select"></div>';
                }else {
                    if(resArray[i].fromid==selecUserId){
                        html+='<div class="content"  id="msg-'+resArray[i].id+'">'+handleContent(resArray[i])+'</div>';
                        html+='<div class="tri"></div>';
                    }else {
                        html+='<div class="content" style="margin-top: inherit;" id="msg-'+resArray[i].id+'">'+handleContent(resArray[i])+'</div>';
                        html+='<div class="tri"></div>';
                    }


                }
                html+='<div class="headImg">'+'<img style="display: inline-block; height: 100%;" src= "' +fileSystemServer+ resArray[i].imageiconurl + '">'+'</div>';

                html+='</div>';

                div.innerHTML=html;
                html='';
                time=resArray[i].sendtime;
                document.getElementById('chatBox').appendChild(div);
            }

            window.location.hash = "#msg-"+selectMsgId;

        }
    });
}

bindData();

