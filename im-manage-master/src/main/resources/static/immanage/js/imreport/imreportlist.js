/**
 *
 */

layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'table' ], function() {
	var $ = layui.$, form = layui.form, table = layui.table;
	// 引入字典
    var DicTool = new XcTableDicTool();
    var dicTempMap1 =  []; // 缓存字典对应数据
    var dicTempMap2 =  [];
    var dicTempMap3 =  [];
	table.render({
		id : 'lay-table',
		elem : '#lay-table',
		url : '/imreport/queryList', // 模拟接口
		method : 'post',
		//toolbar : 'false',
		//defaultToolbar: ['filter', 'exports', 'print'],
		cols : [ [ {
            field : 'strId',
			title : '举报编号'
		}, {
			field : 'reportedDate',
			title : '举报时间',
			width:180,
            templet: function(d){
				return resolvingDate(d.reportedDate);
			}
		}, {
            field : 'nickName',
            title : '举报人'
        }, {
            field : 'objName',
            title : '举报对象'
        }, {
            field : 'reportedType',
            title : '举报对象类型',
            templet: function(d){
                if (dicTempMap1.length === 0) { // 没有字典缓存则需要去后台读取
                    dicTempMap1 = DicTool.getDicDatasByType("REPORT_OBJ_TYPE");
                }
                // 匹配字典值
                return DicTool.matchDicValueById(d.reportedType, dicTempMap1);
            }
        }, {
            field : 'reportType',
            title : '举报类型',
            templet: function(d){
                if (dicTempMap2.length === 0) { // 没有字典缓存则需要去后台读取
                    dicTempMap2 = DicTool.getDicDatasByType("IM_REPORT_TYPE");
                }
                // 匹配字典值
                return DicTool.matchDicValueById(d.reportType, dicTempMap2);
            }
        }, {
            field : 'reportStatus',
            title : '举报状态',
            templet: function(d){
                if (dicTempMap3.length === 0) { // 没有字典缓存则需要去后台读取
                    dicTempMap3 = DicTool.getDicDatasByType("IM_REPORT_STATUS");
                }
                // 匹配字典值
                return DicTool.matchDicValueById(d.reportStatus, dicTempMap3);
            }
        }, {
            field : 'reportedReasion',
            title : '举报说明'
        }, {
            field : 'reportFilePaths',
            title : '举报说明图片',
            templet: function(data){
                if (data.reportFilePaths!=null){
                    var html = "<a title=\"举报图片\" href=\"javascript:;\" class=\"underline\" onclick=\"fileShowOpt(\'图片列表\',\'/impages/report/picshow?pics=" + data.reportFilePaths + '\',\'1000\',\'600\')">照片轮播</a>';

                    return html
                }else {
                    return "没有可视图片"
                }
            }
        }, {
			title : '操作',
			width : 250,
			align : 'center',
			fixed : 'right',
			toolbar : '#lay-table-tool'
		} ] ],
		page : true,
		height : 'full-180',
		text : '对不起，加载出现异常！'
	});
    var xcSelect = new XcSelect();
    var param = {
        selector: "reportedType",
        tips: "请选择状态",
        page : 0,
        limit : 0,
        type : "REPORT_OBJ_TYPE"
    };
    var param2={
        selector: "reportType",
        tips: "请选择状态",
        page : 0,
        limit : 0,
        type : "IM_REPORT_TYPE"
	};

    var def=[ {
        "id" : "",
        "value" : "",
        "name" : "请选择举报对象类型"
    } ];
    var def2=[ {
        "id" : "",
        "value" : "",
        "name" : "请选择举报类型"
    } ];

    xcSelect.initSelect("reportedType",param,def,'');
    xcSelect.initSelect("reportType",param2,def2,'');

    // 监听行工具事件
    table.on('tool(lay-table)', function(obj) {
        var data = obj.data // 获得当前行数据
            , layEvent = obj.event; // 获得 lay-event 对应的值
        if (layEvent === 'detail') {
            active.view(data.strId,1);
        } else if (layEvent === 'del') {
            layer.confirm('真的删除行么', function(index) {
                obj.del(); // 删除对应行（tr）的DOM结构
                layer.close(index);
                active.del(data);
                // 向服务端发送删除指令
            });
        } else if (layEvent === 'edit') {
            active.update(data.strId);
        } else if (layEvent === 'permission') {
            operationlogpermission(data.id);
        }else if(layEvent==='ignore'){
            active.status(data.strId,1);
        }else if(layEvent==='mute'){
            active.mute(data.strId);
        }else if(layEvent==='freeze'){
            active.status(data.strId,3);
        }
    });
	//监听搜索
	form.on('submit(lay-table-search)', function(data) {
		var field = data.field;
		console.log(field);
		// 执行重载
		table.reload('lay-table', {
			where : field
		});
	});

	//事件
	var active = {
	    batchdel: function() {
	        var checkStatus = table.checkStatus('lay-table'),
	        checkData = checkStatus.data; // 得到选中的数据
	        if (checkData.length === 0) {
	            return layer.msg('请选择数据');
	        }

	        layer.prompt({
	            formType: 1,
	            title: '敏感操作，请验证口令'
	        },
	        function(value, index) {
	            layer.close(index);

	            layer.confirm('确定删除吗？',
	            function(index) {
	                var delIds = new Array();
	                checkData.forEach((item, index, array) => {
	                    // 执行代码
	                    delIds.push(item.id);
	                });
	                var param = {};
	                param.delIds = delIds;
	                // 执行 Ajax 后重载
	                $.ajax({
	                    type: 'POST',
	                    url: '/imgroup/batchdel',
	                    data: param,
	                    success: function(res) {
	                        layer.msg(res.msg);
	                        active.reload();
	                    }
	                });
	            });
	        });
	    },
	    del: function(item) {
	        var delIds = new Array();
	        delIds.push(item.id);
	        var param = {};
	        param.delIds = delIds;
	        // 执行 Ajax 后重载
	        $.ajax({
	            type: 'POST',
	            url: '/imgroup/batchdel',
	            data: param,
	            success: function(res) {
	                layer.msg(res.msg);
	                active.reload();
	            }
	        });
	    },
	    add: function() {
	        layer.open({
	            type: 2,
	            title: '添加用户',
	            content: 'imgroupform',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            yes: function(index, layero) {
	                var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    update: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'imgroupform',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    view: function(id,type) {
	    	//groupId
	        $("#edit_id").val(id);
            $("#type").val(type);
	        layer.open({
	            type: 2,
	            title: '查看群组消息',
	            content: 'imgroupform',
	            maxmin: true,
	            area: ['970px', '720px'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	//active.reload();//刷新数据
	            }
	        });
	    },
		//举报状态（1：忽略，2：禁言，3：冻结）
        status: function(id,status) {
	        console.log(id);
            var content='';
            if(status===1){
                content='真的要忽略吗？';
			}else if(status===2){
                content='真的要禁言吗？';
			}else if(status===3){
                content='真的要冻结吗？';
            }
            param=status;
            layer.confirm(content, function(index){
                // 执行 Ajax 后重载
                $.ajax({
                    type: 'POST',
                    url: '/imreport/update',
                    data: {id:id,reportStatus:param},
                    success: function(res) {
                        layer.msg(res.msg);
                        active.reload();
                    }
                });
            })
        },
        mute: function(id) {
            console.log(id);
            var content='';
            // 引入字典
            var DicTool = new XcTableDicTool();
            var dicTempMap =  [];
            dicTempMap=DicTool.getDicDatasByType("IM_BANNED_TIME");
            console.log(dicTempMap);
            var options='';
            for(var i=0;i<dicTempMap.length;i++){
                options+=  '<option value="'+dicTempMap[i].id+'">'+dicTempMap[i].name+'</option>\n' ;
            }
            content+='<label for="TYPE" class="sr-only">禁言时长：</label>' +
                '<select name="bannedTime" id="bannedTime" >\n' +
                options+
                '</select>';
            //弹框处理
            layer.confirm(content,
                {title:"设置"},function(index){
                    var myselect = document.getElementById("bannedTime");　　//获取select对象
                    var index = myselect.selectedIndex;　　　　　　　　　//获取被选中的索引
                    var bannedTime = myselect.options[index].value;　　　　　　//获取被选中的值
                    // 执行 Ajax 后重载
                    $.ajax({
                        type: 'POST',
                        url: '/imreport/update',
                        data: {id:id,reportStatus:2,bannedTime:bannedTime},
                        success: function(res) {
                            layer.msg(res.msg);
                            table.reload('lay-table', {
                                where: {}
                            });
                        }
                    });
                })
        },
	    reload: function() {
	        table.reload('lay-table', {
	            where: {}
	        });
	    }
	};

});

layui.use('laydate', function() {
    var laydate = layui.laydate;
    laydate.render({
        elem: '#reporteddate', //指定元素
        format: 'yyyy-MM-dd',
        type: 'date'
    });
});


/*图片显示*/
function fileShowOpt(title,url,w,h){
    layer_show(title,url,w,h);
}
