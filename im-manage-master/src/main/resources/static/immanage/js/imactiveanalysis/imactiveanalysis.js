/**
 * 获取活跃分析
 */
function getTrafficFlow(time,type){
    var flag=0;
    $.ajax({
        cache: true,
        type: "POST",
        url:"/imanalysis/activeAnalysis",
        data:{type:type,time:time},
        dataType:"json",
        async: false,
        error: function(data) {
            layer.msg("获取活跃分析异常！！！");
        },
        success: function(data) {
            var str=new Array();
            str=data.data.array;
            var str2=new Array();
            str2=data.data.array2;
            lineChart(str,str2,type,flag);
        }
    });
}


function lineChart(xData,sData) {
    var id = document.getElementById("map_line");
    var myChart4 = echarts.init(id);
    myChart4.setOption({

        color: ['#3398DB'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '直接访问',
                type: 'bar',
                barWidth: '60%',
                data: [10, 52, 200, 334, 390, 330, 220]
            }
        ]
        });
}

lineChart(1,1);