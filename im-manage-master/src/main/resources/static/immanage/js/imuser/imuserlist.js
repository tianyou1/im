/**
 * 
 */

layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'table' ], function() {
	var $ = layui.$, form = layui.form, table = layui.table;
	// 用户管理
	var DicTool = new XcTableDicTool();
	var dicTempMap1 =  []; // 缓存字典对应数据
	// 用户管理
	table.render({
		id : 'lay-table',
		elem : '#lay-table',
		url : '/imuser/queryList', // 模拟接口
		method : 'post',
		//toolbar : 'false',
		//defaultToolbar: ['filter', 'exports', 'print'],
		cols : [ [ {
			type : 'checkbox',
			fixed : 'left'
		}, {
			type : 'numbers',
			title : '序号'
		}, {
			field : 'id',
			title : '用户ID'
		}, {
			field : 'headUrl',
			title : '头像',
			templet: function(d){
				// 匹配字典值
                return '<img style="display: inline-block; height: 100%;" src= "'+fileSystemServer+d.headurl+'">';
            }
		}, {
			field : 'nickname',
			title : '昵称'
		}, {
			field : 'sex',
			title : '性别'
		},{
			field : 'status',
			title : '状态',
			templet: function(d){
				if (dicTempMap1.length === 0) { // 没有字典缓存则需要去后台读取
					dicTempMap1 = DicTool.getDicDatasByType("IM_USER_STATUS");
				}
				// 匹配字典值
				return DicTool.matchDicValueById(d.status, dicTempMap1);
			}
		},  {
			field : 'createTime',
			title : '创建时间' ,
			templet:'<div>{{ layui.util.toDateString(d.createTime, "yyyy-MM-dd HH:mm:ss") }}</div>'
		}, {
			title : '操作',
			width : 200,
			align : 'center',
			fixed : 'right',
			toolbar : '#lay-table-tool'
		} ] ],
		page : true,
		height : 'full-180',
		text : '对不起，加载出现异常！'
	});
	// 监听头工具栏事件
	table.on('toolbar(lay-table)',function(obj) {
		var checkStatus = table.checkStatus(obj.config.id), data = checkStatus.data; // 获取选中的数据
		switch (obj.event) {
		case 'add':
			active.add();
			break;
		case 'update':
			if (data.length === 0) {
				layer.msg('请选择一行');
			} else if (data.length > 1) {
				layer.msg('只能同时编辑一个');
			} else {
				layer.msg('编辑 [id]：' + checkStatus.data[0].id);
				active.update(checkStatus.data[0].id);
			}
			break;
		case 'delete':
			active.batchdel();
			break;
		}
	});

	var xcSelect = new XcSelect();
	var param = {
		selector: "messagetype",
		tips: "请选择类型",
		page : 0,
		limit : 0,
		type : "IM_MESSAGE_TYPE"
	};
	xcSelect.initSelect("messagetype",param,true,'');
	// 监听行工具事件
	table.on('tool(lay-table)', function(obj) {
		var data = obj.data // 获得当前行数据
		, layEvent = obj.event; // 获得 lay-event 对应的值
		if (layEvent === 'detail') {
			active.view(data.id);
		} else if (layEvent === 'del') {
			layer.confirm('真的删除行么', function(index) {
				obj.del(); // 删除对应行（tr）的DOM结构
				layer.close(index);
				active.del(data);
				// 向服务端发送删除指令
			});
		} else if (layEvent === 'edit') {
			active.update(data.id);
		} else if (layEvent === 'permission') {
			operationlogpermission(data.id);
		}else if(layEvent==='freeze'){
            active.freeze(data.id,2);
        }else if(layEvent==='unfreeze'){
            active.freeze(data.id,1);
        }
	});
	//监听搜索
	form.on('submit(lay-table-search)', function(data) {
		var nickname = data.field.nickname;
		var id = data.field.id;
		var page=1;
		// 执行重载
		table.reload('lay-table', {
			where : {nickname:nickname,id:id,page:page}
		});
	});
	
	//事件
	var active = {
	    batchdel: function() {
	        var checkStatus = table.checkStatus('lay-table'),
	        checkData = checkStatus.data; // 得到选中的数据
	        if (checkData.length === 0) {
	            return layer.msg('请选择数据');
	        }

	        layer.prompt({
	            formType: 1,
	            title: '敏感操作，请验证口令'
	        },
	        function(value, index) {
	            layer.close(index);

	            layer.confirm('确定删除吗？',
	            function(index) {
	                var delIds = new Array();
	                checkData.forEach((item, index, array) => {
	                    // 执行代码
	                    delIds.push(item.id);
	                });
	                var param = {};
	                param.delIds = delIds;
	                // 执行 Ajax 后重载
	                $.ajax({
	                    type: 'POST',
	                    url: '/imuser/batchdel',
	                    data: param,
	                    success: function(res) {
	                        layer.msg(res.msg);
	                        active.reload();
	                    }
	                });
	            });
	        });
	    },
	    del: function(item) {
	        var delIds = new Array();
	        delIds.push(item.id);
	        var param = {};
	        param.delIds = delIds;
	        // 执行 Ajax 后重载
	        $.ajax({
	            type: 'POST',
	            url: '/imuser/batchdel',
	            data: param,
	            success: function(res) {
	                layer.msg(res.msg);
	                active.reload();
	            }
	        });
	    },
	    add: function() {
	        layer.open({
	            type: 2,
	            title: '添加用户',
	            content: 'imuserform',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            yes: function(index, layero) {
	                var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    update: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'imuserform',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    view: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '详情',
	            content: 'imuserform',
	            maxmin: true,
	            area: ['1020px', '770px'],
	            success: function(layero, index) {
				},
	            yes: function(index, layero) {
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	//active.reload();//刷新数据
	            }
	        });
	    },
        //用户状态（1：正常，2：冻结）
        freeze: function(id,status) {
            $("#edit_id").val(id);
            var content='';
            if(status===1){
                content='真的要解冻吗？';
            }else if(status===2){
                content='真的要冻结吗？';
            }
            param=status;
            layer.confirm(content, function(index){
                // 执行 Ajax 后重载
                $.ajax({
                    type: 'POST',
                    url: '/imuser/update',
                    data: {id:id,status:param},
                    success: function(res) {
                        layer.msg(res.msg);
                        active.reload();
                    }
                });
            })
        },
	    reload: function() {
	        table.reload('lay-table', {
	            where: {}
	        });
	    }
	};

});




