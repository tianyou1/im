/**
 *
 */
layui.config({
    base: '../../layuiadmin/' // 静态资源所在路径
}).extend({
    index: 'lib/index' // 主入口模块
}).use(['index', 'form', 'layer'], function () {
    var $ = layui.$, form = layui.form, layer = layui.layer, table = layui.table;
    var param = {};
    param.id = parent.layui.$("#edit_id").val();

    // 用户管理
    var DicTool = new XcTableDicTool();
    var dicTempMap1 = []; // 缓存字典对应数据
    $.ajax({
        type: 'POST',
        url: '/imuser/read',
        data: param,
        success: function (res) {
            form.val('lay-table-form', res.data);
            document.getElementById("headUrl").src = fileSystemServer+res.data.imUser.headurl;
            $("#lastLoginTime").val(getMyDate(res.data.imUser.lastLoginTime));
            $("#id").val(res.data.imUser.id);
            $("#uid").val(res.data.imUser.id);
            $("#name").val(res.data.imUser.nickname);
            $("#sex").val(res.data.imUser.sex);
            $("#mobile").val(res.data.imUser.mobile);
            $("#sign").val(res.data.imUser.sign);

            if (dicTempMap1.length === 0) { // 没有字典缓存则需要去后台读取
                dicTempMap1 = DicTool.getDicDatasByType("IM_USER_SV");
            }
            // 匹配字典值
            if (res.data.imUsersv != null) {
                var statusText = DicTool.matchDicValueById(res.data.imUsersv.isSecondaryVerification, dicTempMap1);

                if (res.data.imUsersv.verifyType == 1) {
                    $("#mobileStatus").val(statusText);
                    $("#scmobile").val(res.data.imUsersv.mobile);
                } else {
                    $("#mobileStatus").val(DicTool.matchDicValueById(0, dicTempMap1));
                    $("#scmobile").val("");
                }

                if (res.data.imUsersv.verifyType == 2) {
                    $("#emailStatus").val(statusText);
                    $("#scemail").val(res.data.imUsersv.email);
                } else {
                    $("#emailStatus").val(DicTool.matchDicValueById(0, dicTempMap1));
                    $("#scemail").val("");
                }

                if (res.data.imUsersv.verifyType == 3) {
                    $("#googleStatus").val(statusText);
                    $("#googleAccount").val("");
                    $("#scgoogle").val(res.data.imUsersv.googleKey);
                } else {
                    $("#googleStatus").val(DicTool.matchDicValueById(0, dicTempMap1));
                    $("#googleAccount").val("");
                    $("#scgoogle").val("");
                }

                if (res.data.imUsersv.verifyType == 4) {
                    $("#secondStatus").val(statusText);
                } else {
                    $("#secondStatus").val(DicTool.matchDicValueById(0, dicTempMap1));
                }
            }

        }
    });


})

/**
 * 通过成员id获取群成员资料
 */
function queryMemberDetail(userId, friendId) {
    // 提交 Ajax 成功后，静态更新表格中的数据
    var $ = layui.$, form = layui.form, layer = layui.layer;
    $.ajax({
        type: 'POST',
        url: '/imfriend/queryMemberDetail',
        data: {userid: userId, friendid: friendId},
        success: function (res) {
            var nickname = res.data.nickname;
            var headUrl = fileSystemServer+res.data.headUrl;
            var remark = res.data.remark;
            var memberId = res.data.friendid;
            var userId = res.data.userid;
            var sex = res.data.sex;
            var mobile = res.data.mobile;
            var sign = res.data.sign;
            var isSecretFriend = res.data.isSecretFriend;
            if (sex == null) {
                sex = '';
            }
            if (mobile == null) {
                mobile = '';
            }
            if (sign == null) {
                sign = '';
            }
            if (remark == null) {
                remark = '';
            }
            if (isSecretFriend == 0) {
                isSecretFriend = '否';
            } else {
                isSecretFriend = '是';
            }


            var html = '';
            html += '<div class="right-member">';
            html += '头像：<image  class="li-img" src="' + headUrl + '"><br>' + '用户ID：<label id="friendId">' + memberId + '</label><br>' + '昵称：' + nickname + '<br>' + '备注名称：' + remark + '<br>' + '性别：' + sex + '<br>' + '手机号码：' + mobile + '<br>' + '私密联系人：' + isSecretFriend + '<br>' + '签名：' + sign + '<br><div id="userId" hidden>' + userId + '</div><br>';

            html += '</div>';

            $("#memberDetail").html(html);

            if ($("#itemOne").is(':hidden')) {
                titleTwodeal();
            }

        }
    });
}

/**
 * 通过输入的值对应到id或昵称查找群成员
 */
function queryMemberList() {
    // 提交 Ajax 成功后，静态更新表格中的数据
    var $ = layui.$, form = layui.form, layer = layui.layer;
    var id = $("#id").val();
    var searchValue = $("#searchValue").val();
    $.ajax({
        type: 'POST',
        url: '/imfriend/queryListFriend',
        data: {userid: id, searchValue: searchValue},
        success: function (res) {
            $('ol li').remove();
            var members = res.data;
            var url = '';
            var name = '';
            var userId = '';
            var friendId = '';

            for (var i = 0; i < members.length; i++) {
                //增加li
                var li = document.createElement("li");
                url = fileSystemServer+members[i].headUrl;
                name = members[i].nickname;
                userId = members[i].userid;
                friendId = members[i].friendid;

                li.innerHTML = '<div onclick="queryMemberDetail(' + userId + ',' + friendId + ')"  class="li-div"><div class="li-div-left"><image  class="li-img" src="' + url + '"> </div>' + '<div class="li-div-right" ">' + name + '(好友id:' + friendId + ') </div>' + '</div>';
                document.getElementById('memberList').appendChild(li);
            }


        }
    });
}


function getMyDate(str) {
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth() + 1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        oTime = oYear + '-' + getzf(oMonth) + '-' + getzf(oDay) + ' ' + getzf(oHour) + ':' + getzf(oMin) + ':' + getzf(oSen);//最后拼接时间
    return oTime;
};

//补0操作
function getzf(num) {
    if (parseInt(num) < 10) {
        num = '0' + num;
    }
    return num;
}


function titleOnedeal() {
    $("#itemOne").show();
    $("#itemTwo").hide();
}

function titleTwodeal() {


    layui.use(['index', 'table'], function () {
        var $ = layui.$, form = layui.form, table = layui.table;
        var userid = $("#userId").text();
        var friendid = $("#friendId").text();
        if (friendid == null || friendid == '') {
            return layer.msg('请先选择好友');
        }else {
            $("#recordId").text('');
        }
        var sendTime = $("#sendTime").val();

        var content = $("#content").val();

        var messagetype = $("#messagetype").val();
        $("#oneself").val(userid);
        $("#otherper").val(friendid);

        var fromid = $("#fromid option:selected").val();

        // if(fromid==''){
        //     $(".layui-table-col-special").hide();
        // }

        $("#itemTwo").show();
        $("#itemOne").hide();

        // 聊天管理
        var DicTool = new XcTableDicTool();
        var dicTempMap1 =  []; // 缓存字典对应数据
        table.render({
            id: 'lay-table',
            elem: '#lay-table',
            url: '/immsghistory/queryUserList', // 模拟接口
            method: 'post',
            where: {
                belonguserid: userid,
                destid: friendid,
                sendTime: sendTime,
                content: content,
                messagetype: messagetype,
                fromid: fromid
            },
            //toolbar : 'false',
            //defaultToolbar: ['filter', 'exports', 'print'],
            cols: [[{
                field: 'imageiconurl',
                title: '头像',
                templet: function (d) {
                    // 匹配字典值
                    return '<img style="display: inline-block; height: 100%;" src= "' + fileSystemServer+d.imageiconurl + '">';
                }
            }, {
                field: 'fromid',
                title: '用户ID'
            }, {
                field: 'content',
                title: '内容',
                templet: function(d){
                    if(d.messagetype==2){
                        return d.content;
                    }else if(d.messagetype==3){
                        return '<img style="display: inline-block; height: 100%;" src= "' + fileSystemServer+d.content + '" />';
                    }else if(d.messagetype==4){
                        return '<a href="' + d.content + '"  download="">文件下载</a>';
                    }else if(d.messagetype==30){
                        return '<video width="320" height="240" controls="controls">' +
                            '<source src="' + d.content + '" type="video/mov"/>' +
                            '</video>';
                    }else if(d.messagetype==16) {
                        return '<audio src="' + d.content + '" controls="controls"></audio>';
                    }else {
                        return d.content;
                    }
                }
            }, {
                field: 'messagetype',
                title: '消息类型',
                templet: function(d){
                    if (dicTempMap1.length === 0) { // 没有字典缓存则需要去后台读取
                        dicTempMap1 = DicTool.getDicDatasByType("IM_MESSAGE_TYPE");
                    }
                    // 匹配字典值
                    return DicTool.matchDicValueById(d.messagetype, dicTempMap1);
                }
            }, {
                field: 'sendtime',
                title: '发送时间',
                templet: '<div>{{ layui.util.toDateString(d.sendtime, "yyyy-MM-dd HH:mm:ss") }}</div>'
            }, {
                title: '操作',
                width: 200,
                align: 'center',
                fixed: 'right',
                toolbar: '#lay-table-tool'
            }

            ]],
            page: true,
            height: 'full-180',
            text: '对不起，加载出现异常！'
        });

        // 监听行工具事件
        table.on('tool(lay-table)', function (obj) {
            var data = obj.data // 获得当前行数据
                , layEvent = obj.event; // 获得 lay-event 对应的值
            if (layEvent === 'location') {
                active.location(data.id);
            }
        });

        //事件
        var active = {
            location: function (id) {
                $.ajax({
                    type: 'POST',
                    url: '/immsghistory/location',
                    data: {id: id, belonguserid: userid, destid: friendid},
                    success: function (res) {
                        $("#fromid").val("");
                        $("#recordId").text(res.data.id);
                        table.reload('lay-table', {
                            where: {fromid: ''},
                            data: {start: res.data.page},
                            page: {curr: res.data.page}
                        });

                    }
                });

            },
            reload: function () {
                table.reload('lay-table', {
                    where: {}
                });
            }
        };


    }).use('laydate', function () {

        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#sendTime'
        });
    });


}





