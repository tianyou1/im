/**
 *
 */
layui.config({
    base: '../../layuiadmin/' // 静态资源所在路径
}).extend({
    index: 'lib/index' // 主入口模块
}).use(['index', 'form', 'layer'], function () {
    var $ = layui.$, form = layui.form, layer = layui.layer;
    form.on('submit(lay-table-submit)', function (data) {
        var field = data.field; // 获取提交的字段
        var url = '/imnotice/add';
        // 提交 Ajax 成功后，静态更新表格中的数据
        $.ajax({
            type: 'POST',
            url: '/imnotice/add',
            data: field,
            success: function (res) {
                layer.msg(res.msg);
            }
        });

    });
});
