/**
 * 
 */

layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'table' ], function() {
	var $ = layui.$, form = layui.form, table = layui.table;
	// 用户管理
	var DicTool = new XcTableDicTool();
	var dicTempMap1 =  []; // 缓存字典对应数据
	// 用户管理
	table.render({
		id : 'lay-table',
		elem : '#lay-table',
		url : '/imuser/queryList', // 模拟接口
		method : 'post',
		//toolbar : 'false',
		//defaultToolbar: ['filter', 'exports', 'print'],
		cols : [ [ {
			type : 'checkbox',
			fixed : 'left'
		}, {
			type : 'numbers',
			title : '序号'
		}, {
			field : 'id',
			title : '用户ID'
		}, {
			field : 'headUrl',
			title : '头像',
			templet: function(d){
				// 匹配字典值
                return '<img style="display: inline-block; height: 100%;" src= "'+d.headurl+'">';
            }
		}, {
			field : 'nickname',
			title : '昵称'
		}, {
			field : 'sex',
			title : '性别'
		}, {
			title : '操作',
			width : 200,
			align : 'center',
			fixed : 'right',
			toolbar : '#lay-table-tool'
		} ] ],
		page : true,
		height : 'full-180',
		text : '对不起，加载出现异常！'
	});
	// 监听头工具栏事件
	table.on('toolbar(lay-table)',function(obj) {
		var checkStatus = table.checkStatus(obj.config.id), data = checkStatus.data; // 获取选中的数据
		switch (obj.event) {
		case 'add':
			active.add();
			break;
		case 'update':
			if (data.length === 0) {
				layer.msg('请选择一行');
			} else if (data.length > 1) {
				layer.msg('只能同时编辑一个');
			} else {
				layer.msg('编辑 [id]：' + checkStatus.data[0].id);
				active.update(checkStatus.data[0].id);
			}
			break;
		case 'delete':
			active.batchdel();
			break;
		}
		;
	});
	// 监听行工具事件
	table.on('tool(lay-table)', function(obj) {
		var data = obj.data // 获得当前行数据
		, layEvent = obj.event; // 获得 lay-event 对应的值
		if (layEvent === 'detail') {
			active.view(data.id);
		} else if (layEvent === 'del') {
			layer.confirm('真的删除行么', function(index) {
				obj.del(); // 删除对应行（tr）的DOM结构
				layer.close(index);
				active.del(data);
				// 向服务端发送删除指令
			});
		} else if (layEvent === 'edit') {
			active.update(data.id);
		} else if (layEvent === 'permission') {
			operationlogpermission(data.id);
		}
	});
	//监听搜索
	form.on('submit(lay-table-search)', function(data) {
		var field = data.field;
		// 执行重载
		table.reload('lay-table', {
			where : field
		});
	});
	
	//事件
	var active = {


	    reload: function() {
	        table.reload('lay-table', {
	            where: {}
	        });
	    }
	};

});




