package com.xc.web.immanage.immsghistory.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistory;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistoryVO;

public interface ImMessageHistoryService {

    PageDataResult queryList(int page, int limit, ImMessageHistory record);

    ResponseResult queryAll();

    ResponseResult selectByPrimaryKey(Long id);

    PageDataResult queryUserList(int page, int limit, ImMessageHistoryVO record);

    ResponseResult location(ImMessageHistoryVO record);


}
