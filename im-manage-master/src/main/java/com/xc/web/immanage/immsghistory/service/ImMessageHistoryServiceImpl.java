package com.xc.web.immanage.immsghistory.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistory;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistoryVO;
import com.xc.web.immanage.immsghistory.mapper.ImMessageHistoryMapper;
import com.xc.web.immanage.immsghistoryless.entity.ImMessageHistoryLessVO;
import com.xc.web.utils.IStatusMessage;
import com.xc.web.immanage.utils.ResponseUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImMessageHistoryServiceImpl implements ImMessageHistoryService {
    @Autowired
    private ImMessageHistoryMapper imMessageHistoryMapper;

    public ResponseResult queryAll() {
        ResponseResult rs = new ResponseResult();
        List<ImMessageHistory> list = imMessageHistoryMapper.queryList(null);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    public ResponseResult selectByPrimaryKey(Long id) {
        ImMessageHistory record = imMessageHistoryMapper.selectByPrimaryKey(id);
        return ResponseUtil.getObjectRes(record);
    }


    public PageDataResult queryList(int page, int limit, ImMessageHistory record) {
        //模糊查询群名称
        if (StringUtils.isNotBlank(record.getContent())) {
            record.setContent("%" + record.getContent() + "%");
        }
        PageHelper.startPage(page, limit);
        List<ImMessageHistory> list = imMessageHistoryMapper.queryList(record);
        return ResponseUtil.getPageRes(page,limit,list);
    }



    @Override
    public PageDataResult queryUserList(int page, int limit, ImMessageHistoryVO record) {
        //模糊查询群名称
        if (StringUtils.isNotBlank(record.getContent())) {
            record.setContent("%" + record.getContent() + "%");
        }
        PageHelper.startPage(page, limit);
        List<ImMessageHistory> list = imMessageHistoryMapper.queryUserList(record);
        for (ImMessageHistory imMessageHistory : list) {
            if(imMessageHistory.getContent().contains("msgString")){
                imMessageHistory.setContent(JSONObject.parseObject(imMessageHistory.getContent()).getString("msgString"));
            }
            if(imMessageHistory.getMessagetype().equals(ImMessageHistory.fileMessage)){
                imMessageHistory.setContent(JSONObject.parseObject(imMessageHistory.getContent()).getString("fileUrl"));
            }

            if(imMessageHistory.getMessagetype().equals(ImMessageHistory.videoMessage)){
                imMessageHistory.setContent(JSONObject.parseObject(imMessageHistory.getContent()).getString("vedioUrl"));
            }

            if(imMessageHistory.getMessagetype().equals(ImMessageHistory.voiceMessage)){
                imMessageHistory.setContent(JSONObject.parseObject(imMessageHistory.getContent()).getString("url"));
            }
        }
        return ResponseUtil.getPageRes(page,limit,list);
    }

    @Override
    public ResponseResult location(ImMessageHistoryVO record) {
        List<ImMessageHistory> list = imMessageHistoryMapper.queryUserList(record);

        int top = 0;
        for (int i=0;i<list.size();i++){
            if(list.get(i).getId().equals(record.getId())){
                top = i+1;
            }
        }
        int page=1;
        if(top>10){
            page=(top % 10)==0 ? top/10:top/10+1;
        }
        record.setPage(page);
        return ResponseUtil.getObjectRes(record);
    }


}
