package com.xc.web.immanage.imfriend.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imfriend.entity.ImFriend;
import com.xc.web.immanage.imfriend.entity.ImFriendVO;

public interface ImFriendService {

    PageDataResult queryList(int page, int limit, ImFriendVO record);

    ResponseResult queryAll();

    ResponseResult selectByPrimaryKey(Long id);

    ResponseResult add(ImFriend record);

    ResponseResult update(ImFriend record);

    ResponseResult queryListFriend(ImFriendVO record);

    /**
     * 查看好友详情
     *
     * @param record
     * @return
     */
    ResponseResult queryMemberDetail(ImFriend record);

}
