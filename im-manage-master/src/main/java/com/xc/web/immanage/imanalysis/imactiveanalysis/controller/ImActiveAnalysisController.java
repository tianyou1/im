package com.xc.web.immanage.imanalysis.imactiveanalysis.controller;

import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imuser.entity.ImUserVO;
import com.xc.web.immanage.imuser.service.ImUserService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/imanalysis")
@TitleAnno(title = "活跃分析")
public class ImActiveAnalysisController {
	
	@Autowired
    ImUserService imUserService;
	


    @MethodAnno(title = "活跃分析", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/activeAnalysis", method = RequestMethod.POST)
    public ResponseResult activeAnalysis(ImUserVO record, HttpServletResponse response, String type, String time,
                                         HttpServletRequest request) {
        return imUserService.activeAnalysis(record);
    }


}
