package com.xc.web.immanage.imfriend.mapper;

import com.xc.web.immanage.imfriend.entity.ImFriend;
import com.xc.web.immanage.imfriend.entity.ImFriendVO;
import com.xc.web.immanage.imuser.entity.ImUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImFriendMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImFriend record);

    int insertSelective(ImFriend record);

    ImFriend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImFriend record);

    int updateByPrimaryKey(ImFriend record);

    List<ImFriendVO> queryList(ImFriendVO record);

    ImFriendVO queryMemberDetail(ImFriend record);
}