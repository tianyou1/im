package com.xc.web.immanage.imuserreports.entity;

public class ImUserReportsWithBLOBsVO extends ImUserReportsWithBLOBs {
    private String name;

    private String objName;

    private String reporteddate;

    private String strId;

    private String nickName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }

    public String getReporteddate() {
        return reporteddate;
    }

    public void setReporteddate(String reporteddate) {
        this.reporteddate = reporteddate;
    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}