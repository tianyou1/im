package com.xc.web.immanage.imfriend.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imfriend.entity.ImFriend;
import com.xc.web.immanage.imfriend.entity.ImFriendVO;
import com.xc.web.immanage.imfriend.mapper.ImFriendMapper;
import com.xc.web.utils.IStatusMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImFriendServiceImpl implements ImFriendService {

    @Autowired
    private ImFriendMapper imFriendMapper;

    public ResponseResult queryAll() {
        ResponseResult rs = new ResponseResult();
        List<ImFriendVO> list = imFriendMapper.queryList(null);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    public ResponseResult selectByPrimaryKey(Long id) {
        ResponseResult rpr = new ResponseResult();
        ImFriend record = imFriendMapper.selectByPrimaryKey(id);
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("读取失败，根据id" + id + "未找到数据！");
        } else {
            rpr.setData(record);
            rpr.setMsg("读取成功！");
        }
        return rpr;
    }


    public ResponseResult add(ImFriend record) {
        ResponseResult rpr = new ResponseResult();
        int num = imFriendMapper.insertSelective(record);
        if (1 == num) {
            rpr.setMsg("新增成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("新增失败，请刷新页面后重试！");
        }
        return rpr;
    }

    public ResponseResult update(ImFriend record) {
        ResponseResult rpr = new ResponseResult();
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("需要修改的数据id不能为空");
        } else {
            int num = imFriendMapper.updateByPrimaryKeySelective(record);
            if (1 == num) {
                rpr.setMsg("修改成功！");
            } else {
                rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
                rpr.setMsg("修改异常！");
            }
        }
        return rpr;
    }

    @Override
    public ResponseResult queryListFriend(ImFriendVO record) {
        ResponseResult rpr = new ResponseResult();
        List<ImFriendVO> list = imFriendMapper.queryList(record);
        rpr.setData(list);
        return rpr;
    }

    @Override
    public ResponseResult queryMemberDetail(ImFriend record) {
        ResponseResult rpr = new ResponseResult();
        ImFriend friend = imFriendMapper.queryMemberDetail(record);
        rpr.setData(friend);
        return rpr;
    }


    public PageDataResult queryList(int page, int limit, ImFriendVO record) {

        PageDataResult pdr = new PageDataResult();
        PageHelper.startPage(page, limit);
        List<ImFriendVO> list = imFriendMapper.queryList(record);
        // 获取分页查询后的数据
        PageInfo<ImFriendVO> pageInfo = new PageInfo<>(list);
        // 获取总记录数total
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if (0 == total) {
            pdr.setCount(0);
            pdr.setData(null);
            pdr.setCode(-1);
            pdr.setMsg("未查询到数据！");
        } else {
            pdr.setCount(total);
            pdr.setData(list);
            pdr.setCode(0);
            pdr.setMsg("查询成功！");
        }
        return pdr;
    }

}
