package com.xc.web.immanage.imgroupmember.service;

import com.github.pagehelper.PageHelper;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMember;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import com.xc.web.immanage.imgroupmember.mapper.ImGroupMemberMapper;
import com.xc.web.immanage.utils.ResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImGroupMemberServiceImpl implements ImGroupMemberService {


    @Autowired
    private ImGroupMemberMapper imGroupMemberMapper ;


    @Override
    public int deleteByPrimaryKey(Long id) {
        return this.imGroupMemberMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ImGroupMember record) {
        return this.imGroupMemberMapper.insert(record);
    }

    @Override
    public int insertSelective(ImGroupMember record) {
        return this.imGroupMemberMapper.insertSelective(record);
    }

    @Override
    public ImGroupMember selectByPrimaryKey(Long id) {
        return this.imGroupMemberMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ImGroupMember record) {
        return this.imGroupMemberMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ImGroupMember record) {
        return this.imGroupMemberMapper.updateByPrimaryKey(record);
    }

    @Override
    public PageDataResult queryMemberList(int page, int limit, ImGroupMemberVO record){
        PageHelper.startPage(page, limit);
        List<ImGroupMemberVO> list = this.imGroupMemberMapper.queryMemberList(record);
        return  ResponseUtil.getPageRes(page,limit,list);
    }

    @Override
    public ResponseResult queryMemberDetail(Long id) {
        return ResponseUtil.getObjectRes(this.imGroupMemberMapper.queryMemberDetail(id));
    }
}