package com.xc.web.immanage.imuserreports.entity;

public class ImUserReportsVO extends ImUserReports {
    private String name;

    private String objName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }


}