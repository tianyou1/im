package com.xc.web.immanage.imsuggestions.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imsuggestions.entity.ImSuggestions;
import com.xc.web.immanage.imsuggestions.mapper.ImSuggestionsMapper;
import com.xc.web.utils.IStatusMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImSuggestionsServiceImpl implements ImSuggestionsService {
    @Autowired
    private ImSuggestionsMapper imSuggestionsMapper;

    public ResponseResult queryAll() {
        ResponseResult rs = new ResponseResult();
        List<ImSuggestions> list = imSuggestionsMapper.queryList(null);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    public ResponseResult selectByPrimaryKey(Long id) {
        ResponseResult rpr = new ResponseResult();
        ImSuggestions record = imSuggestionsMapper.selectByPrimaryKey(id);
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("读取失败，根据id" + id + "未找到数据！");
        } else {
            rpr.setData(record);
            rpr.setMsg("读取成功！");
        }
        return rpr;
    }


    public ResponseResult add(ImSuggestions record) {
        ResponseResult rpr = new ResponseResult();
        int num = imSuggestionsMapper.insertSelective(record);
        if (1 == num) {
            rpr.setMsg("新增成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("新增失败，请刷新页面后重试！");
        }
        return rpr;
    }

    public ResponseResult update(ImSuggestions record) {
        ResponseResult rpr = new ResponseResult();
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("需要修改的数据id不能为空");
        } else {
            int num = imSuggestionsMapper.updateByPrimaryKeySelective(record);
            if (1 == num) {
                rpr.setMsg("修改成功！");
            } else {
                rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
                rpr.setMsg("修改异常！");
            }
        }
        return rpr;
    }


    public ResponseResult deleteByPrimaryKeys(Long[] delIds) {
        ResponseResult rpr = new ResponseResult();
        int num = imSuggestionsMapper.deleteByPrimaryKeys(delIds);
        if (num > 0) {
            rpr.setMsg("删除成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("删除异常！");
        }
        return rpr;
    }

    public PageDataResult queryList(int page, int limit, ImSuggestions record) {
        PageDataResult pdr = new PageDataResult();
        PageHelper.startPage(page, limit);
        List<ImSuggestions> list = imSuggestionsMapper.queryList(record);
        // 获取分页查询后的数据
        PageInfo<ImSuggestions> pageInfo = new PageInfo<>(list);
        // 获取总记录数total
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if (0 == total) {
            pdr.setCount(0);
            pdr.setData(null);
            pdr.setCode(-1);
            pdr.setMsg("未查询到数据！");
        } else {
            pdr.setCount(total);
            pdr.setData(list);
            pdr.setCode(0);
            pdr.setMsg("查询成功！");
        }
        return pdr;
    }
}
