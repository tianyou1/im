package com.xc.web.immanage.imsuggestions.controller;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imnotice.entity.ImNotice;
import com.xc.web.immanage.imnotice.entity.ImNoticeVO;
import com.xc.web.immanage.imnotice.service.ImNoticeService;
import com.xc.web.immanage.imsuggestions.entity.ImSuggestions;
import com.xc.web.immanage.imsuggestions.entity.ImSuggestionsVO;
import com.xc.web.immanage.imsuggestions.service.ImSuggestionsService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

@RestController
@RequestMapping("/imsuggestions")
@TitleAnno(title = "意见管理")
public class ImSuggestionController {

    @Autowired
    private ImSuggestionsService imSuggestionsService;

    @MethodAnno(title = "查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public ResponseResult read(@RequestParam("id") Long id,
                               HttpServletResponse response, HttpServletRequest request) {
        return imSuggestionsService.selectByPrimaryKey(id);
    }

    @MethodAnno(title = "新增", operation = MethodAnno.OPERA_TYPE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult add(ImSuggestions record, HttpServletResponse response,
                              HttpServletRequest request) {
        record.setCreateTime(System.currentTimeMillis());
        return imSuggestionsService.add(record);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseResult update(ImSuggestions record, HttpServletResponse response,
                                 HttpServletRequest request) {
        return imSuggestionsService.update(record);
    }

    @MethodAnno(title = "删除", operation = MethodAnno.OPERA_TYPE_DEL)
    @RequestMapping(value = "/batchdel", method = RequestMethod.POST)
    public ResponseResult batchdel(@RequestParam("delIds[]") Long[] delIds,
                                   HttpServletResponse response, HttpServletRequest request) {
        return imSuggestionsService.deleteByPrimaryKeys(delIds);
    }

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public PageDataResult queryList(@RequestParam("page") int page,
                                    @RequestParam("limit") int limit, ImSuggestionsVO record,
                                    HttpServletResponse response, HttpServletRequest request) throws ParseException {
        if(StringUtils.isNotEmpty(record.getCreateTimeDeal())){
            record.setCreateTime(DateUtils.parseDate(record.getCreateTimeDeal(),"yyyy-MM-dd").getTime()/1000);
        }
        return imSuggestionsService.queryList(page, limit, record);
    }

    @MethodAnno(title = "查询所有", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET)
    public ResponseResult queryList(HttpServletResponse response,
                                    HttpServletRequest request) {
        return imSuggestionsService.queryAll();
    }

}
