package com.xc.web.immanage.imuser.entity;

import java.util.Date;

public class ImUser {
    private Long id;

    private String name;

    private String openid;

    private String qqopenid;

    private String account;

    private String idno;

    private String nickname;

    private String realname;

    private String pwd;

    private String mobile;

    private String mail;

    private String sex;

    private Long birthday;

    private String sign;

    private String province;

    private Integer isonline;

    private Integer needauth;

    private String period;

    private Integer searchmobile;

    private Integer newnotification;

    private String city;

    private String district;

    private Long createtime;

    private Integer status;

    private String detail;

    private Integer isauth;

    private Long recommanduserid;

    private String longitude;

    private String latitude;

    private String location;

    private String alipayname;

    private String alipayaccount;

    private String firstredpacket;

    private String secretModePwd;

    private String chatLogPwd;

    private Integer isShowInputStatus;

    private Integer isAutoCancel;

    private Integer autoCancelMonths;

    private Integer isShowUserid;

    private Integer searchAccount;

    private Integer searchUserId;

    private Integer searchGroupId;

    private Integer isGroupedNeedAuth;

    private Date lastLoginTime;

    private String headurl;

    private String feedbackimage;

    private Long isbanned;

    public String getHeadurl() {
        return headurl;
    }

    public void setHeadurl(String headurl) {
        this.headurl = headurl == null ? null : headurl.trim();
    }

    public String getFeedbackimage() {
        return feedbackimage;
    }

    public void setFeedbackimage(String feedbackimage) {
        this.feedbackimage = feedbackimage == null ? null : feedbackimage.trim();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getQqopenid() {
        return qqopenid;
    }

    public void setQqopenid(String qqopenid) {
        this.qqopenid = qqopenid == null ? null : qqopenid.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno == null ? null : idno.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public Integer getIsonline() {
        return isonline;
    }

    public void setIsonline(Integer isonline) {
        this.isonline = isonline;
    }

    public Integer getNeedauth() {
        return needauth;
    }

    public void setNeedauth(Integer needauth) {
        this.needauth = needauth;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period == null ? null : period.trim();
    }

    public Integer getSearchmobile() {
        return searchmobile;
    }

    public void setSearchmobile(Integer searchmobile) {
        this.searchmobile = searchmobile;
    }

    public Integer getNewnotification() {
        return newnotification;
    }

    public void setNewnotification(Integer newnotification) {
        this.newnotification = newnotification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    public Integer getIsauth() {
        return isauth;
    }

    public void setIsauth(Integer isauth) {
        this.isauth = isauth;
    }

    public Long getRecommanduserid() {
        return recommanduserid;
    }

    public void setRecommanduserid(Long recommanduserid) {
        this.recommanduserid = recommanduserid;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    public String getAlipayname() {
        return alipayname;
    }

    public void setAlipayname(String alipayname) {
        this.alipayname = alipayname == null ? null : alipayname.trim();
    }

    public String getAlipayaccount() {
        return alipayaccount;
    }

    public void setAlipayaccount(String alipayaccount) {
        this.alipayaccount = alipayaccount == null ? null : alipayaccount.trim();
    }

    public String getFirstredpacket() {
        return firstredpacket;
    }

    public void setFirstredpacket(String firstredpacket) {
        this.firstredpacket = firstredpacket == null ? null : firstredpacket.trim();
    }

    public String getSecretModePwd() {
        return secretModePwd;
    }

    public void setSecretModePwd(String secretModePwd) {
        this.secretModePwd = secretModePwd == null ? null : secretModePwd.trim();
    }

    public String getChatLogPwd() {
        return chatLogPwd;
    }

    public void setChatLogPwd(String chatLogPwd) {
        this.chatLogPwd = chatLogPwd == null ? null : chatLogPwd.trim();
    }

    public Integer getIsShowInputStatus() {
        return isShowInputStatus;
    }

    public void setIsShowInputStatus(Integer isShowInputStatus) {
        this.isShowInputStatus = isShowInputStatus;
    }

    public Integer getIsAutoCancel() {
        return isAutoCancel;
    }

    public void setIsAutoCancel(Integer isAutoCancel) {
        this.isAutoCancel = isAutoCancel;
    }

    public Integer getAutoCancelMonths() {
        return autoCancelMonths;
    }

    public void setAutoCancelMonths(Integer autoCancelMonths) {
        this.autoCancelMonths = autoCancelMonths;
    }

    public Integer getIsShowUserid() {
        return isShowUserid;
    }

    public void setIsShowUserid(Integer isShowUserid) {
        this.isShowUserid = isShowUserid;
    }

    public Integer getSearchAccount() {
        return searchAccount;
    }

    public void setSearchAccount(Integer searchAccount) {
        this.searchAccount = searchAccount;
    }

    public Integer getSearchUserId() {
        return searchUserId;
    }

    public void setSearchUserId(Integer searchUserId) {
        this.searchUserId = searchUserId;
    }

    public Integer getSearchGroupId() {
        return searchGroupId;
    }

    public void setSearchGroupId(Integer searchGroupId) {
        this.searchGroupId = searchGroupId;
    }

    public Integer getIsGroupedNeedAuth() {
        return isGroupedNeedAuth;
    }

    public void setIsGroupedNeedAuth(Integer isGroupedNeedAuth) {
        this.isGroupedNeedAuth = isGroupedNeedAuth;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getIsbanned() {
        return isbanned;
    }

    public void setIsbanned(Long isbanned) {
        this.isbanned = isbanned;
    }
}