package com.xc.web.immanage.imuser.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imuser.entity.ImUser;
import com.xc.web.immanage.imuser.entity.ImUserVO;

public interface ImUserService {

    PageDataResult queryList(int page, int limit, ImUser record);

    ResponseResult queryAll();

    ResponseResult deleteByPrimaryKeys(Long[] delIds);

    ResponseResult selectByPrimaryKey(Long id);

    ResponseResult selectByUserId(Long id);

    ResponseResult add(ImUser record);

    ResponseResult update(ImUser record);

    ResponseResult activeAnalysis(ImUserVO imUserVO);

}
