package com.xc.web.immanage.imnotice.controller;

import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/impages/notice")
@TitleAnno(title = "公告页面")
public class ImNoticePages {

    @RequestMapping("/noticelist")
    @MethodAnno(title = "用户列表", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imnoticemanage")
    public String noticelist() {
        return "immanage/imnotice/imnoticelist";
    }

    @RequestMapping("/imnoticeform")
    @MethodAnno(title = "用户详情", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imnoticemanage")
    public String noticeform() {
        return "immanage/imnotice/imnoticeform";
    }

    @RequestMapping("/imnoticeadd")
    @MethodAnno(title = "用户详情", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imnoticemanage")
    public String noticeAdd() {
        return "immanage/imnotice/imnoticeadd";
    }

}
