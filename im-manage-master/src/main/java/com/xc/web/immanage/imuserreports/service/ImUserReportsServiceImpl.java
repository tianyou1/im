package com.xc.web.immanage.imuserreports.service;

import com.github.pagehelper.PageHelper;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroup.entity.ImGroup;
import com.xc.web.immanage.imgroup.mapper.ImGroupMapper;
import com.xc.web.immanage.imuser.entity.ImUser;
import com.xc.web.immanage.imuser.mapper.ImUserMapper;
import com.xc.web.immanage.imuserreports.entity.ImUserReports;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBs;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBsVO;
import com.xc.web.immanage.imuserreports.mapper.ImUserReportsMapper;
import com.xc.web.immanage.utils.BanedTimeSet;
import com.xc.web.immanage.utils.MyDateUtils;
import com.xc.web.immanage.utils.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImUserReportsServiceImpl implements ImUserReportsService {

    @Autowired
    private ImUserReportsMapper imUserReportsMapper;
    @Autowired
    private ImUserMapper imUserMapper;
    @Autowired
    private ImGroupMapper imGroupMapper;

    @Override
    public ResponseResult deleteByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public ResponseResult insert(ImUserReportsWithBLOBs record) {
        return null;
    }

    @Override
    public ResponseResult insertSelective(ImUserReportsWithBLOBs record) {
        return null;
    }

    @Override
    public ResponseResult selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public ResponseResult updateByPrimaryKeySelective(ImUserReportsWithBLOBs record) {
        //如果是修改举报状态
        int status=0;
        status=record.getReportStatus();
        if(status>0){
            record.setReportHandleDate(MyDateUtils.getDate());
            //获取被举报对象的userId
            ImUserReportsWithBLOBs imUserReportsWithBLOBs=imUserReportsMapper.selectByPrimaryKey(record.getId());
            if(imUserReportsWithBLOBs.getFromType()!=null&&imUserReportsWithBLOBs.getFromType()==1){
                //单聊
                long reportedId=imUserReportsWithBLOBs.getReportedId();
                ImUser imUser=imUserMapper.selectByPrimaryKey(reportedId);
                if(status==1){
                    //
                }else if(status==2){
                    //禁言
                    //对被举报对象进行禁言操作
                    imUser.setIsbanned(BanedTimeSet.setBanned(record.getBannedTime()));
                    imUserMapper.updateByPrimaryKeySelective(imUser);
                    //添加处理说明
                    //被封时间
                    int day=record.getBannedTime()/60/24;
                    String bannedTime=MyDateUtils.getStringTime(imUser.getIsbanned(),"yyyy-MM-dd HH:mm:ss");
                    String beginTime=BanedTimeSet.getNowTime(imUser.getIsbanned(),-record.getBannedTime());
                    record.setReportHandleMark(imUser.getNickname()+"被"+"设置了"+day+"天的禁言，"+"封禁时间从"+beginTime+"，到"+bannedTime);

                }else if(status==3){
                    //冻结
                    //对被举报对象进行禁言操作
                    if(imUser.getStatus()!=0){
                        imUser.setStatus(2);
                        imUserMapper.updateByPrimaryKeySelective(imUser);
                    }
                }
            }else if(imUserReportsWithBLOBs.getFromType()!=null&&imUserReportsWithBLOBs.getFromType()==2){
                //群聊
                long groupId=imUserReportsWithBLOBs.getReportedId();
                ImGroup imGroup=imGroupMapper.selectByPrimaryKey(groupId);
                if(status==3){
                    //冻结
                    if(imGroup.getGroupStatus()!=0){
                        imGroup.setGroupStatus(2);
                        imGroupMapper.updateByPrimaryKeySelective(imGroup);
                    }

                }
            }
        }


        return ResponseUtil.getUpdateRes(imUserReportsMapper.updateByPrimaryKeySelective(record));
    }

    @Override
    public ResponseResult updateByPrimaryKeyWithBLOBs(ImUserReportsWithBLOBs record) {
        return ResponseUtil.getUpdateRes(imUserReportsMapper.updateByPrimaryKeyWithBLOBs(record));
    }

    @Override
    public ResponseResult updateByPrimaryKey(ImUserReports record) {
        return null;
    }

    @Override
    public PageDataResult queryList(int page, int limit, ImUserReportsWithBLOBsVO record) {
        //模糊查询举报人
        if(StringUtils.isNotBlank(record.getNickName())){
            record.setNickName("%"+record.getNickName()+"%");
        }
        //模糊查询被举报人
        if(StringUtils.isNotBlank(record.getObjName())){
            record.setObjName("%"+record.getObjName()+"%");
        }
        PageHelper.startPage(page, limit);
        List<ImUserReportsWithBLOBsVO> list = imUserReportsMapper.queryList(record);
        return ResponseUtil.getPageRes(page,limit,list);
    }
}
