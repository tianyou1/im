package com.xc.web.immanage.imuserreports.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imuserreports.entity.ImUserReports;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBs;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBsVO;

public interface ImUserReportsService {
    ResponseResult deleteByPrimaryKey(Integer id);

    ResponseResult insert(ImUserReportsWithBLOBs record);

    ResponseResult insertSelective(ImUserReportsWithBLOBs record);

    ResponseResult selectByPrimaryKey(Integer id);

    ResponseResult updateByPrimaryKeySelective(ImUserReportsWithBLOBs record);

    ResponseResult updateByPrimaryKeyWithBLOBs(ImUserReportsWithBLOBs record);

    ResponseResult updateByPrimaryKey(ImUserReports record);

    PageDataResult queryList(int page, int limit, ImUserReportsWithBLOBsVO record);
}