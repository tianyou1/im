package com.xc.web.immanage.imuserreports.controller;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBs;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBsVO;
import com.xc.web.immanage.imuserreports.service.ImUserReportsService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/imreport")
@TitleAnno(title = "举报管理")
public class ImUserReportController {
	
	@Autowired
    ImUserReportsService imUserReportsService;
	
	@MethodAnno(title = "列表查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public PageDataResult queryList(@RequestParam("page") int page,
            @RequestParam("limit") int limit, ImUserReportsWithBLOBsVO record,
            HttpServletResponse response, HttpServletRequest request) {
        return imUserReportsService.queryList(page, limit, record);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseResult update(ImUserReportsWithBLOBs record, HttpServletResponse response,
                                 HttpServletRequest request) {
        return imUserReportsService.updateByPrimaryKeySelective(record);
    }


}
