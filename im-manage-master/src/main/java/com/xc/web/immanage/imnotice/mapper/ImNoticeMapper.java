package com.xc.web.immanage.imnotice.mapper;

import com.xc.web.immanage.imnotice.entity.ImNotice;
import com.xc.web.immanage.imnotice.entity.ImNoticeVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImNoticeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImNotice record);

    int insertSelective(ImNotice record);

    ImNotice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImNotice record);

    int updateByPrimaryKeyWithBLOBs(ImNotice record);

    int updateByPrimaryKey(ImNotice record);

    List<ImNotice> queryList(ImNoticeVO record);

    int deleteByPrimaryKeys(Long[] delIds);

    /**
     * 查看公告列表
     * @param id
     * @return
     */
    ImNotice selectByNoticeId(Long id);
}