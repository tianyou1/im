package com.xc.web.immanage.imgroupmember.mapper;

import com.xc.web.immanage.imgroupmember.entity.ImGroupMember;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImGroupMemberMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImGroupMember record);

    int insertSelective(ImGroupMember record);

    ImGroupMember selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImGroupMember record);

    int updateByPrimaryKey(ImGroupMember record);

    List<ImGroupMemberVO> queryMemberList(ImGroupMemberVO record);

    ImGroupMemberVO queryMemberDetail(Long id);
}