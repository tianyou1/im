package com.xc.web.immanage.imgroupmember.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMember;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistoryVO;


public interface ImGroupMemberService {
    int deleteByPrimaryKey(Long id);

    int insert(ImGroupMember record);

    int insertSelective(ImGroupMember record);

    ImGroupMember selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImGroupMember record);

    int updateByPrimaryKey(ImGroupMember record);

    PageDataResult queryMemberList(int page, int limit, ImGroupMemberVO record);

    ResponseResult queryMemberDetail(Long id);


}