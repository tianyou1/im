package com.xc.web.immanage.imnotice.entity;

public class ImNoticeVO extends ImNotice {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String createTimeDeal;

    public String getCreateTimeDeal() {
        return createTimeDeal;
    }

    public void setCreateTimeDeal(String createTimeDeal) {
        this.createTimeDeal = createTimeDeal;
    }
}