package com.xc.web.immanage.imsuggestions.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imsuggestions.entity.ImSuggestions;

public interface ImSuggestionsService {

    PageDataResult queryList(int page, int limit, ImSuggestions record);

    ResponseResult queryAll();

    ResponseResult deleteByPrimaryKeys(Long[] delIds);

    ResponseResult selectByPrimaryKey(Long id);

    ResponseResult add(ImSuggestions record);

    ResponseResult update(ImSuggestions record);
}
