package com.xc.web.immanage.imgroupmember.entity;

public class ImGroupMember {
    private Long id;

    private Long groupid;

    private Long userid;

    private String markname;

    private Integer role;

    private Long creatorid;

    private Long createtime;

    private Integer receivetip;

    private Integer isaccept;

    private Long isbanned;

    private Integer isblack;

    private Integer isanonymous;

    private String anonymousheaderurl;

    private String anonymousname;

    private Integer isSecretGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupid() {
        return groupid;
    }

    public void setGroupid(Long groupid) {
        this.groupid = groupid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getMarkname() {
        return markname;
    }

    public void setMarkname(String markname) {
        this.markname = markname == null ? null : markname.trim();
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Long getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(Long creatorid) {
        this.creatorid = creatorid;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Integer getReceivetip() {
        return receivetip;
    }

    public void setReceivetip(Integer receivetip) {
        this.receivetip = receivetip;
    }

    public Integer getIsaccept() {
        return isaccept;
    }

    public void setIsaccept(Integer isaccept) {
        this.isaccept = isaccept;
    }

    public Long getIsbanned() {
        return isbanned;
    }

    public void setIsbanned(Long isbanned) {
        this.isbanned = isbanned;
    }

    public Integer getIsblack() {
        return isblack;
    }

    public void setIsblack(Integer isblack) {
        this.isblack = isblack;
    }

    public Integer getIsanonymous() {
        return isanonymous;
    }

    public void setIsanonymous(Integer isanonymous) {
        this.isanonymous = isanonymous;
    }

    public String getAnonymousheaderurl() {
        return anonymousheaderurl;
    }

    public void setAnonymousheaderurl(String anonymousheaderurl) {
        this.anonymousheaderurl = anonymousheaderurl == null ? null : anonymousheaderurl.trim();
    }

    public String getAnonymousname() {
        return anonymousname;
    }

    public void setAnonymousname(String anonymousname) {
        this.anonymousname = anonymousname == null ? null : anonymousname.trim();
    }

    public Integer getIsSecretGroup() {
        return isSecretGroup;
    }

    public void setIsSecretGroup(Integer isSecretGroup) {
        this.isSecretGroup = isSecretGroup;
    }
}