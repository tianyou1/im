package com.xc.web.immanage.imfriend.controller;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imfriend.entity.ImFriend;
import com.xc.web.immanage.imfriend.entity.ImFriendVO;
import com.xc.web.immanage.imfriend.service.ImFriendService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/imfriend")
@TitleAnno(title = "用户管理")
public class ImFriendController {

    @Autowired
    private ImFriendService imFriendService;

    @MethodAnno(title = "查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public ResponseResult read(@RequestParam("id") Long id,
                               HttpServletResponse response, HttpServletRequest request) {
        return imFriendService.selectByPrimaryKey(id);
    }

    @MethodAnno(title = "新增", operation = MethodAnno.OPERA_TYPE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult add(ImFriend record, HttpServletResponse response,
                              HttpServletRequest request) {
        record.setCreatetime(System.currentTimeMillis());
        return imFriendService.add(record);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseResult update(ImFriend record, HttpServletResponse response,
                                 HttpServletRequest request) {
        return imFriendService.update(record);
    }

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public PageDataResult queryList(@RequestParam("page") int page,
                                    @RequestParam("limit") int limit, ImFriendVO record,
                                    HttpServletResponse response, HttpServletRequest request) {
        return imFriendService.queryList(page, limit, record);
    }

    @MethodAnno(title = "查询所有", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET)
    public ResponseResult queryAll(HttpServletResponse response,
                                   HttpServletRequest request) {
        return imFriendService.queryAll();
    }


    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryListFriend", method = RequestMethod.POST)
    public ResponseResult queryList(ImFriendVO record,
                                    HttpServletResponse response, HttpServletRequest request) {
        return imFriendService.queryListFriend(record);
    }


    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryMemberDetail", method = RequestMethod.POST)
    public ResponseResult queryMemberDetail(ImFriend record,
                                            HttpServletResponse response, HttpServletRequest request) {
        System.err.println("ss1:" + record.getFriendid());
        System.err.println("ss2:" + record.getUserid());
        return imFriendService.queryMemberDetail(record);
    }

}
