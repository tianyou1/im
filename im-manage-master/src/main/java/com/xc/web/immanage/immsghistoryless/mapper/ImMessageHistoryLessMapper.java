package com.xc.web.immanage.immsghistoryless.mapper;

import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import com.xc.web.immanage.immsghistoryless.entity.ImMessageHistoryLess;
import com.xc.web.immanage.immsghistoryless.entity.ImMessageHistoryLessVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImMessageHistoryLessMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImMessageHistoryLess record);

    int insertSelective(ImMessageHistoryLess record);

    ImMessageHistoryLess selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImMessageHistoryLess record);

    int updateByPrimaryKey(ImMessageHistoryLess record);

    List<ImMessageHistoryLessVO> queryHistory(ImMessageHistoryLessVO record);
}