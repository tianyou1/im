package com.xc.web.immanage.imuserreports.entity;

import java.util.Date;

public class ImUserReports {
    private Long id;

    private Integer reportType;

    private Long fromUserId;

    private Integer fromType;

    private Date reportedDate;

    private Integer reportedType;

    private Long reportedId;

    private String reportFilePaths;

    private Integer reportStatus;

    private Date reportHandleDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Integer getFromType() {
        return fromType;
    }

    public void setFromType(Integer fromType) {
        this.fromType = fromType;
    }

    public Date getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(Date reportedDate) {
        this.reportedDate = reportedDate;
    }

    public Integer getReportedType() {
        return reportedType;
    }

    public void setReportedType(Integer reportedType) {
        this.reportedType = reportedType;
    }

    public Long getReportedId() {
        return reportedId;
    }

    public void setReportedId(Long reportedId) {
        this.reportedId = reportedId;
    }

    public String getReportFilePaths() {
        return reportFilePaths;
    }

    public void setReportFilePaths(String reportFilePaths) {
        this.reportFilePaths = reportFilePaths == null ? null : reportFilePaths.trim();
    }

    public Integer getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(Integer reportStatus) {
        this.reportStatus = reportStatus;
    }

    public Date getReportHandleDate() {
        return reportHandleDate;
    }

    public void setReportHandleDate(Date reportHandleDate) {
        this.reportHandleDate = reportHandleDate;
    }
}