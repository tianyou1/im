package com.xc.web.immanage.imanalysis.imactiveanalysis.controller;

import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/impages/analysis")
@TitleAnno(title = "活跃分析页面")
public class ImActiveAnalysisPages {

    @RequestMapping("/active")
    @MethodAnno(title = "活跃分析", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("imactiveanalysis")
    public String grouplist() {
        return "imanalysis/imactiveanalysis/activeanalysis";
    }
    

}
