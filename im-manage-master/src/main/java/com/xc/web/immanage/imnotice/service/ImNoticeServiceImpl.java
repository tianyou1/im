package com.xc.web.immanage.imnotice.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imnotice.entity.ImNotice;
import com.xc.web.immanage.imnotice.entity.ImNoticeVO;
import com.xc.web.immanage.imnotice.mapper.ImNoticeMapper;
import com.xc.web.immanage.utils.ShiroUtils;
import com.xc.web.user.entity.User;
import com.xc.web.utils.IStatusMessage;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImNoticeServiceImpl implements ImNoticeService {
    @Autowired
    private ImNoticeMapper imNoticeMapper;

    public ResponseResult queryAll() {
        ResponseResult rs = new ResponseResult();
        List<ImNotice> list = imNoticeMapper.queryList(null);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    public ResponseResult selectByPrimaryKey(Long id) {
        ResponseResult rpr = new ResponseResult();
        ImNotice record = imNoticeMapper.selectByPrimaryKey(id);
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("读取失败，根据id" + id + "未找到数据！");
        } else {
            rpr.setData(record);
            rpr.setMsg("读取成功！");
        }
        return rpr;
    }


    public ResponseResult add(ImNotice record) {
        ResponseResult rpr = new ResponseResult();
        record.setOrgid(ShiroUtils.getUserId());
        int num = imNoticeMapper.insertSelective(record);
        if (1 == num) {
            rpr.setMsg("新增成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("新增失败，请刷新页面后重试！");
        }
        return rpr;
    }

    public ResponseResult update(ImNotice record) {
        ResponseResult rpr = new ResponseResult();
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("需要修改的数据id不能为空");
        } else {
            int num = imNoticeMapper.updateByPrimaryKeySelective(record);
            if (1 == num) {
                rpr.setMsg("修改成功！");
            } else {
                rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
                rpr.setMsg("修改异常！");
            }
        }
        return rpr;
    }


    public ResponseResult deleteByPrimaryKeys(Long[] delIds) {
        ResponseResult rpr = new ResponseResult();
        int num = imNoticeMapper.deleteByPrimaryKeys(delIds);
        if (num > 0) {
            rpr.setMsg("删除成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("删除异常！");
        }
        return rpr;
    }

    public PageDataResult queryList(int page, int limit, ImNoticeVO record) {

        System.out.println(record.getCreateTime());
        PageDataResult pdr = new PageDataResult();
        PageHelper.startPage(page, limit);
        List<ImNotice> list = imNoticeMapper.queryList(record);
        // 获取分页查询后的数据
        PageInfo<ImNotice> pageInfo = new PageInfo<>(list);
        // 获取总记录数total
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if (0 == total) {
            pdr.setCount(0);
            pdr.setData(null);
            pdr.setCode(-1);
            pdr.setMsg("未查询到数据！");
        } else {
            pdr.setCount(total);
            pdr.setData(list);
            pdr.setCode(0);
            pdr.setMsg("查询成功！");
        }
        return pdr;
    }
}
