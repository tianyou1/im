package com.xc.web.immanage.immsghistoryless.entity;


public class ImMessageHistoryLessVO extends ImMessageHistoryLess {

    private String sendTime;

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }
}