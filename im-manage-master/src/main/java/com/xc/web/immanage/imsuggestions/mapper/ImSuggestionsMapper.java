package com.xc.web.immanage.imsuggestions.mapper;

import com.xc.web.immanage.imsuggestions.entity.ImSuggestions;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImSuggestionsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImSuggestions record);

    int insertSelective(ImSuggestions record);

    ImSuggestions selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImSuggestions record);

    int updateByPrimaryKeyWithBLOBs(ImSuggestions record);

    int updateByPrimaryKey(ImSuggestions record);

    List<ImSuggestions> queryList(ImSuggestions record);

    int deleteByPrimaryKeys(Long[] delIds);
}