package com.xc.web.immanage.imsuggestions.entity;

public class ImSuggestionsVO extends ImSuggestions{

    private String createTimeDeal;

    public String getCreateTimeDeal() {
        return createTimeDeal;
    }

    public void setCreateTimeDeal(String createTimeDeal) {
        this.createTimeDeal = createTimeDeal;
    }
}