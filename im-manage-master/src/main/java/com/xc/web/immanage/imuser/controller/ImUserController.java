package com.xc.web.immanage.imuser.controller;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imuser.entity.ImUser;
import com.xc.web.immanage.imuser.service.ImUserService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/imuser")
@TitleAnno(title = "用户管理")
public class ImUserController {

    @Autowired
    private ImUserService imUserService;

    @MethodAnno(title = "查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public ResponseResult read(@RequestParam("id") Long id,
                               HttpServletResponse response, HttpServletRequest request) {
        return imUserService.selectByUserId(id);
    }

    @MethodAnno(title = "新增", operation = MethodAnno.OPERA_TYPE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult add(ImUser record, HttpServletResponse response,
                              HttpServletRequest request) {
        record.setCreatetime(System.currentTimeMillis());
        return imUserService.add(record);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseResult update(ImUser record, HttpServletResponse response,
                                 HttpServletRequest request) {
        return imUserService.update(record);
    }

    @MethodAnno(title = "删除", operation = MethodAnno.OPERA_TYPE_DEL)
    @RequestMapping(value = "/batchdel", method = RequestMethod.POST)
    public ResponseResult batchdel(@RequestParam("delIds[]") Long[] delIds,
                                   HttpServletResponse response, HttpServletRequest request) {
        return imUserService.deleteByPrimaryKeys(delIds);
    }

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public PageDataResult queryList(@RequestParam("page") int page,
                                    @RequestParam("limit") int limit, ImUser record,
                                    HttpServletResponse response, HttpServletRequest request) {
        return imUserService.queryList(page, limit, record);
    }

    @MethodAnno(title = "查询所有", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET)
    public ResponseResult queryList(HttpServletResponse response,
                                    HttpServletRequest request) {
        return imUserService.queryAll();
    }

}
