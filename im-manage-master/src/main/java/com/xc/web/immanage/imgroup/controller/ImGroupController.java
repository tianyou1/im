package com.xc.web.immanage.imgroup.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xc.web.immanage.imgroup.entity.ImGroupVO;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMember;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import com.xc.web.immanage.imgroupmember.service.ImGroupMemberService;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistory;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistoryVO;
import com.xc.web.immanage.immsghistory.service.ImMessageHistoryService;
import com.xc.web.immanage.immsghistoryless.entity.ImMessageHistoryLessVO;
import com.xc.web.immanage.immsghistoryless.service.ImMessageHistoryLessService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroup.entity.ImGroup;
import com.xc.web.immanage.imgroup.service.ImGroupService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.thymeleaf.util.DateUtils;

import java.sql.Date;

@RestController
@RequestMapping("/imgroup")
@TitleAnno(title = "群组管理")
public class ImGroupController {
	
	@Autowired
	ImGroupService imGroupService;

	@Autowired
    ImGroupMemberService imGroupMemberService;

	@Autowired
    ImMessageHistoryService imMessageHistoryService;

    @Autowired
    ImMessageHistoryLessService imMessageHistoryLessService;

    @MethodAnno(title = "查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public ResponseResult read(@RequestParam("id") Long id,
            HttpServletResponse response, HttpServletRequest request) {
        return imGroupService.selectByPrimaryKey(id);
    }

    @MethodAnno(title = "新增", operation = MethodAnno.OPERA_TYPE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult add(ImGroup record, HttpServletResponse response,
            HttpServletRequest request) {
        record.setCreatetime(System.currentTimeMillis());
        return imGroupService.add(record);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseResult update(ImGroup record, HttpServletResponse response,
            HttpServletRequest request) {
        return imGroupService.update(record);
    }

    @MethodAnno(title = "删除", operation = MethodAnno.OPERA_TYPE_DEL)
    @RequestMapping(value = "/batchdel", method = RequestMethod.POST)
    public ResponseResult batchdel(@RequestParam("delIds[]") Long[] delIds,
            HttpServletResponse response, HttpServletRequest request) {
        return imGroupService.deleteByPrimaryKeys(delIds);
    }
	
	@MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public PageDataResult queryList(@RequestParam("page") int page,
            @RequestParam("limit") int limit, ImGroupVO record,
            HttpServletResponse response, HttpServletRequest request) {

        return imGroupService.queryList(page, limit, record);
    }

    @MethodAnno(title = "查询所有", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET)
    public ResponseResult queryList(HttpServletResponse response,
            HttpServletRequest request) {
        return imGroupService.queryAll();
    }


    @MethodAnno(title = "查询群成员", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryMemberList", method = RequestMethod.POST)
    public PageDataResult queryMemberList(@RequestParam("page") int page,
                                          @RequestParam("limit") int limit, ImGroupMemberVO record,
                                          HttpServletResponse response, HttpServletRequest request) {
        //获取群id
        record.setGroupid(Long.parseLong(request.getParameter("groupId")));
        //获取搜索的值
        if(StringUtils.isNotBlank(request.getParameter("searchValue"))){
            String searchValue="%"+request.getParameter("searchValue")+"%";
            record.setSearchValue(searchValue);
        }
        return imGroupMemberService.queryMemberList(page, limit, record);
    }

    @MethodAnno(title = "查询群成员详情", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryMemberDetail", method = RequestMethod.POST)
    public ResponseResult queryMemberDetail(@RequestParam("id") Long id,
                               HttpServletResponse response, HttpServletRequest request) {
        return imGroupMemberService.queryMemberDetail(id);
    }

    @MethodAnno(title = "查询群成员在群里的发言", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryHistory", method = RequestMethod.POST)
    public PageDataResult queryHistory(@RequestParam("page") int page,
                                       @RequestParam("limit") int limit, @RequestParam(value = "groupId",required = false) Long groupId,
                                       @RequestParam(value = "id",required = false) Long id, ImMessageHistoryLessVO record,
                                       HttpServletResponse response, HttpServletRequest request) {

        //默认进来查全部
        record.setDestid(groupId);

        //获取群成员id 如果0.1
        if(id!=0){
            ImGroupMember imGroupMember=imGroupMemberService.selectByPrimaryKey(id);
            long userId=imGroupMember.getUserid();
            //添加条件
            record.setFromid(userId);
        }

        //搜索框条件
        if(StringUtils.isNotBlank(record.getContent())){
            record.setContent("%"+record.getContent()+"%");
        }
        if(StringUtils.isNotBlank(record.getSendTime())){
            long sendtime= Date.valueOf(record.getSendTime()).getTime()/1000;
            record.setSendtime(sendtime);
        }

        //获取搜索的值
        //destId 群id belongUserId 自己id （userId） fromId（自己id userId）
        return imMessageHistoryLessService.queryHistory(page, limit, record);
    }

}
