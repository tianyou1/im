package com.xc.web.immanage.imsuggestions.controller;

import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/impages/suggestions")
@TitleAnno(title = "意见页面")
public class ImSuggestionPages {

    @RequestMapping("/suggestionslist")
    @MethodAnno(title = "意见列表", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imsuggestionsmanage")
    public String suggestionslist() {
        return "immanage/imsuggestions/imsuggestionslist";
    }

    @RequestMapping("/suggestionsform")
    @MethodAnno(title = "意见详情", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imsuggestionsmanage")
    public String suggestionsform() {
        return "immanage/imsuggestions/imsuggestionsform";
    }


}
