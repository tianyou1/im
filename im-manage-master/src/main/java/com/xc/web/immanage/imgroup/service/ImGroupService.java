package com.xc.web.immanage.imgroup.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroup.entity.ImGroup;
import com.xc.web.immanage.imgroup.entity.ImGroupVO;

public interface ImGroupService {

	PageDataResult queryList(int page, int limit, ImGroupVO record);

    ResponseResult queryAll();

    ResponseResult deleteByPrimaryKeys(Long[] delIds);

    ResponseResult selectByPrimaryKey(Long id);

    ResponseResult add(ImGroup record);

    ResponseResult update(ImGroup record);
}
