package com.xc.web.immanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
// 开启属性自动解密功能
@EnableEncryptableProperties
@ComponentScan("com.xc.web")
public class ImManageApplication {

    public static void main(String[] args) {
    	System.out.println("xChat后台管理系统开始启动");
        SpringApplication.run(ImManageApplication.class, args);
        System.out.println("xChat后台管理系统启动成功");
    }

}
