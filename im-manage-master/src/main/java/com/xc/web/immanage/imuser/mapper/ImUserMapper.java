package com.xc.web.immanage.imuser.mapper;

import com.xc.web.immanage.imuser.entity.ImUser;
import com.xc.web.immanage.imuser.entity.ImUserVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUser record);

    int insertSelective(ImUser record);

    ImUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUser record);

    int updateByPrimaryKeyWithBLOBs(ImUser record);

    int updateByPrimaryKey(ImUser record);

    List<ImUserVO> queryList(ImUser record);

    int deleteByPrimaryKeys(Long[] delIds);

    ImUser selectByUserId(Long id);

}