package com.xc.web.immanage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @ClassName: FileConfig
 * @Description: 文件配置
 * @author cx
 */
@Component
@ConfigurationProperties(prefix = "file")
@PropertySource(value = "classpath:fileConfig.properties")
public class FileConfig {
	/**
	 * 访问前缀
	 */
	private String visitPrefix;

	public String getVisitPrefix() {
		return visitPrefix;
	}

	public void setVisitPrefix(String visitPrefix) {
		this.visitPrefix = visitPrefix;
	}
}