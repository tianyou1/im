package com.xc.web.immanage.immsghistoryless.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.immsghistoryless.entity.ImMessageHistoryLess;
import com.xc.web.immanage.immsghistoryless.entity.ImMessageHistoryLessVO;
import com.xc.web.immanage.immsghistoryless.mapper.ImMessageHistoryLessMapper;
import com.xc.web.immanage.utils.ResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImMessageHistoryLessServiceImpl implements ImMessageHistoryLessService {


    @Autowired
    ImMessageHistoryLessMapper imMessageHistoryLessMapper;

    @Override
    public ResponseResult deleteByPrimaryKey(Long id) {
        return ResponseUtil.getDelRes(this.imMessageHistoryLessMapper.deleteByPrimaryKey(id));
    }

    @Override
    public ResponseResult insert(ImMessageHistoryLess record) {
        return null;
    }

    @Override
    public ResponseResult insertSelective(ImMessageHistoryLess record) {
        return null;
    }

    @Override
    public ResponseResult selectByPrimaryKey(Long id) {
        return null;
    }

    @Override
    public ResponseResult updateByPrimaryKeySelective(ImMessageHistoryLess record) {
        return null;
    }

    @Override
    public ResponseResult updateByPrimaryKey(ImMessageHistoryLess record) {
        return null;
    }


    @Override
    public PageDataResult queryHistory(int page, int limit, ImMessageHistoryLessVO record) {
        //查全部
        if(limit!=0){
            PageHelper.startPage(page, limit);
        }
        List<ImMessageHistoryLessVO> list = this.imMessageHistoryLessMapper.queryHistory(record);
        /*for (ImMessageHistoryLessVO imMessageHistory : list) {
            if(imMessageHistory.getContent().contains("msgString")){
                imMessageHistory.setContent(JSONObject.parseObject(imMessageHistory.getContent()).getString("msgString"));
            }
        }*/
        return ResponseUtil.getPageRes(page,limit,list);
    }
}
