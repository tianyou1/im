package com.xc.web.immanage.immsghistory.controller;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistory;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistoryVO;
import com.xc.web.immanage.immsghistory.service.ImMessageHistoryService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

@RestController
@RequestMapping("/immsghistory")
@TitleAnno(title = "历史记录管理")
public class ImMsgHistoryController {
    @Autowired
    private ImMessageHistoryService imMessageHistoryService;

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.GET)
    public PageDataResult queryList(@RequestParam("page") int page,
                                    @RequestParam("limit") int limit, ImMessageHistory record,
                                    HttpServletResponse response, HttpServletRequest request) {
        return imMessageHistoryService.queryList(page, limit, record);
    }


    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryUserList", method = RequestMethod.POST)
    public PageDataResult queryUserList(@RequestParam("page") int page,
                                        @RequestParam("limit") int limit, ImMessageHistoryVO record,
                                        HttpServletResponse response, HttpServletRequest request) throws ParseException {

        if (StringUtils.isNotEmpty(record.getSendTime())) {
            record.setSendtime(DateUtils.parseDate(record.getSendTime(), "yyyy-MM-dd").getTime() / 1000);
        }

        return imMessageHistoryService.queryUserList(page, limit, record);
    }


    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/location", method = RequestMethod.POST)
    public ResponseResult location(ImMessageHistoryVO record,
                                   HttpServletResponse response, HttpServletRequest request) {

        return imMessageHistoryService.location(record);
    }





}
