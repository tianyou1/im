package com.xc.web.immanage.immsghistory.entity;

public class ImMessageHistory {


    /**
     * 消息类型：4文件16语音30视频6其他
     */
    public static final Integer fileMessage=4;
    public static final Integer voiceMessage=16;
    public static final Integer videoMessage=30;


    private Long id;

    private Long belonguserid;

    private Integer devtype;

    private Integer geoid;

    private String msgid;

    private Long fromid;

    private Integer fromtype;

    private String imageiconurl;

    private Long destid;

    private String fromname;

    private Integer messagetype;

    private Long sendtime;

    private Long receivetime;

    private Integer version;

    private Integer status;

    private Integer isEphemeralChat;

    private Integer ephemeralChatTime;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBelonguserid() {
        return belonguserid;
    }

    public void setBelonguserid(Long belonguserid) {
        this.belonguserid = belonguserid;
    }

    public Integer getDevtype() {
        return devtype;
    }

    public void setDevtype(Integer devtype) {
        this.devtype = devtype;
    }

    public Integer getGeoid() {
        return geoid;
    }

    public void setGeoid(Integer geoid) {
        this.geoid = geoid;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid == null ? null : msgid.trim();
    }

    public Long getFromid() {
        return fromid;
    }

    public void setFromid(Long fromid) {
        this.fromid = fromid;
    }

    public Integer getFromtype() {
        return fromtype;
    }

    public void setFromtype(Integer fromtype) {
        this.fromtype = fromtype;
    }

    public String getImageiconurl() {
        return imageiconurl;
    }

    public void setImageiconurl(String imageiconurl) {
        this.imageiconurl = imageiconurl == null ? null : imageiconurl.trim();
    }

    public Long getDestid() {
        return destid;
    }

    public void setDestid(Long destid) {
        this.destid = destid;
    }

    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname == null ? null : fromname.trim();
    }

    public Integer getMessagetype() {
        return messagetype;
    }

    public void setMessagetype(Integer messagetype) {
        this.messagetype = messagetype;
    }

    public Long getSendtime() {
        return sendtime;
    }

    public void setSendtime(Long sendtime) {
        this.sendtime = sendtime;
    }

    public Long getReceivetime() {
        return receivetime;
    }

    public void setReceivetime(Long receivetime) {
        this.receivetime = receivetime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsEphemeralChat() {
        return isEphemeralChat;
    }

    public void setIsEphemeralChat(Integer isEphemeralChat) {
        this.isEphemeralChat = isEphemeralChat;
    }

    public Integer getEphemeralChatTime() {
        return ephemeralChatTime;
    }

    public void setEphemeralChatTime(Integer ephemeralChatTime) {
        this.ephemeralChatTime = ephemeralChatTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}