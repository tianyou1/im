package com.xc.web.immanage.imgroup.controller;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;

@Controller
@RequestMapping("/impages/group")
@TitleAnno(title = "群组页面")
public class ImGroupPages {

    @RequestMapping("/grouplist")
    @MethodAnno(title = "群组列表", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("imgroupmanage")
    public String grouplist() {
        return "immanage/imgroup/imgrouplist";
    }
    
    @RequestMapping("/imgroupform")
    @MethodAnno(title = "群组详情", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("imgroupmanage")
    public String groupform() {
        return "immanage/imgroup/imgroupform";
    }


    @RequestMapping("/memberhistorylist")
    @MethodAnno(title = "群成员聊天历史记录查询", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("imgroupmanage")
    public String memberhistorylist() {
        return "immanage/imgroup/viewhistory";
    }
}
