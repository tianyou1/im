package com.xc.web.immanage.imusersv.mapper;

import com.xc.web.immanage.imusersv.entity.ImUserSecondaryVerification;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImUserSecondaryVerificationMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImUserSecondaryVerification record);

    int insertSelective(ImUserSecondaryVerification record);

    ImUserSecondaryVerification selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserSecondaryVerification record);

    int updateByPrimaryKey(ImUserSecondaryVerification record);

    /**
     * 根据用户id查找
     * @param userId
     * @return
     */
    ImUserSecondaryVerification selectByUserId(Long userId);
}