package com.xc.web.immanage.imgroup.entity;

public class ImGroupVO {
    private Long id;

    private String name;

    private String descriptions;

    private String detail;

    private Long createrid;

    private String ownerName;

    private Integer memberCount;

    private Long createtime;

    private Integer istop;

    private Long orgid;

    private Integer isbanned;

    private Integer isviewfriend;

    private Integer groupStatus;
    
    private String groupStatusDisplay;

    private Integer isInviteConfirm;

    private Integer isScreenshotTip;

    private Integer isEphemeralChat;

    private Integer ephemeralChatTime;

    private Integer isAnonymous;

    private String headurl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions == null ? null : descriptions.trim();
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }

    public Long getCreaterid() {
        return createrid;
    }

    public void setCreaterid(Long createrid) {
        this.createrid = createrid;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Integer getIstop() {
        return istop;
    }

    public void setIstop(Integer istop) {
        this.istop = istop;
    }

    public Long getOrgid() {
        return orgid;
    }

    public void setOrgid(Long orgid) {
        this.orgid = orgid;
    }

    public Integer getIsbanned() {
        return isbanned;
    }

    public void setIsbanned(Integer isbanned) {
        this.isbanned = isbanned;
    }

    public Integer getIsviewfriend() {
        return isviewfriend;
    }

    public void setIsviewfriend(Integer isviewfriend) {
        this.isviewfriend = isviewfriend;
    }

    public Integer getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(Integer groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getGroupStatusDisplay() {
		return groupStatusDisplay;
	}

	public void setGroupStatusDisplay(String groupStatusDisplay) {
		this.groupStatusDisplay = groupStatusDisplay;
	}

	public Integer getIsInviteConfirm() {
        return isInviteConfirm;
    }

    public void setIsInviteConfirm(Integer isInviteConfirm) {
        this.isInviteConfirm = isInviteConfirm;
    }

    public Integer getIsScreenshotTip() {
        return isScreenshotTip;
    }

    public void setIsScreenshotTip(Integer isScreenshotTip) {
        this.isScreenshotTip = isScreenshotTip;
    }

    public Integer getIsEphemeralChat() {
        return isEphemeralChat;
    }

    public void setIsEphemeralChat(Integer isEphemeralChat) {
        this.isEphemeralChat = isEphemeralChat;
    }

    public Integer getEphemeralChatTime() {
        return ephemeralChatTime;
    }

    public void setEphemeralChatTime(Integer ephemeralChatTime) {
        this.ephemeralChatTime = ephemeralChatTime;
    }

    public Integer getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(Integer isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public String getHeadurl() {
        return headurl;
    }

    public void setHeadurl(String headurl) {
        this.headurl = headurl == null ? null : headurl.trim();
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Integer getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Integer memberCount) {
        this.memberCount = memberCount;
    }
}