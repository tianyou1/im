package com.xc.web.immanage.immsghistory.entity;

public class ImMessageHistoryVO extends ImMessageHistory{

    private String sendTime;

    private int page;

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

}