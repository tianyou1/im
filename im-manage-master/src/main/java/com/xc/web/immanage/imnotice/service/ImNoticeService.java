package com.xc.web.immanage.imnotice.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imnotice.entity.ImNotice;
import com.xc.web.immanage.imnotice.entity.ImNoticeVO;

public interface ImNoticeService {

    PageDataResult queryList(int page, int limit, ImNoticeVO record);

    ResponseResult queryAll();

    ResponseResult deleteByPrimaryKeys(Long[] delIds);

    ResponseResult selectByPrimaryKey(Long id);

    ResponseResult add(ImNotice record);

    ResponseResult update(ImNotice record);
}
