package com.xc.web.immanage.imuserreports.controller;

import com.xc.web.immanage.config.FileConfig;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/impages/report")
@TitleAnno(title = "群组页面")
public class ImUserReportPages {

    @Autowired
    private FileConfig fileConfig;

    @RequestMapping("/reportlist")
    @MethodAnno(title = "群组列表", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("imreportmanage")
    public String grouplist(ModelMap map) {
        return "immanage/imreport/reportlist";
    }

    @RequestMapping("/picshow")
    @MethodAnno(title = "图片展示", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("imreportmanage")
    public String grouplist(@Param("pics") String pics,ModelMap map) {
        //图片存在
        if(pics.indexOf(",")>0){
            String[] picArr = pics.split(",");
            if(picArr.length>0){
                for(int i=0;i<picArr.length;i++){

                    picArr[i]=fileConfig.getVisitPrefix()+picArr[i];
                }
            }
            map.put("picArr",picArr);
        }else {
            map.put("picArr","");
        }
        return "immanage/imreport/reportpiclist";
    }
}
