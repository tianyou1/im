package com.xc.web.immanage.utils;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;

import java.util.Date;

public class BanedTimeSet {


    /**
     * -1不展示 0展示未禁言 1禁言
     */
    public static final Integer notShowBanned=-1;
    public static final Integer notBanned=0;
    public static final Integer banned=1;



    //取消禁言
    public static final Integer unBanned=0;
    //永久禁言
    public static final Integer foreverBanned=-1;


    //未禁言时间
    public static final Long unBannedTime=DateUtil.offset(new Date(), DateField.YEAR,-1).getTime();
    //永久禁言时间
    public static final Long foreverBannedTime=DateUtil.offset(new Date(), DateField.YEAR,100).getTime();


    /**
     * 设置禁言时间
     * @param time 时间（分）
     * @return
     */
    public static Long setBanned(int time){
        return  DateUtil.offsetMinute(new Date(),time).getTime();
    }

    /**
     * 获取当前时间禁言时间
     * @param time 时间（分）
     * @return
     */
    public static String getNowTime(Long bannedTime,int time){
        Date d= MyDateUtils.getDate(bannedTime);
        return  MyDateUtils.getStringTime(DateUtil.offsetMinute(d,time),"yyyy-MM-dd HH:mm:ss");
    }
}
