package com.xc.web.immanage.imgroup.mapper;

import java.util.List;

import com.xc.web.base.PageDataResult;
import com.xc.web.immanage.imgroupmember.entity.ImGroupMember;
import org.apache.ibatis.annotations.Mapper;

import com.xc.web.immanage.imgroup.entity.ImGroup;
import com.xc.web.immanage.imgroup.entity.ImGroupVO;

@Mapper
public interface ImGroupMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImGroup record);

    int insertSelective(ImGroup record);

    ImGroup selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImGroup record);

    int updateByPrimaryKeyWithBLOBs(ImGroup record);

    int updateByPrimaryKey(ImGroup record);
    
    int deleteByPrimaryKeys(Long[] delIds);
    
    List<ImGroupVO> queryList(ImGroupVO record);
}