package com.xc.web.immanage.utils;

import com.github.pagehelper.PageInfo;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.utils.IStatusMessage;

import java.util.List;

public class ResponseUtil {

    /**
     * 分页类型
     * @param page 页数
     * @param limit 条数
     * @param list
     * @return
     */
    public static PageDataResult getPageRes(int page, int limit, List<?> list){
        PageDataResult pdr = new PageDataResult();
        // 获取分页查询后的数据
        PageInfo<?> pageInfo = new PageInfo<>(list);
        // 获取总记录数total
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if ( 0==total ) {
            pdr.setCount(0);
            pdr.setData(null);
            pdr.setCode(-1);
            pdr.setMsg("未查询到数据！");
        } else {
            pdr.setCount(total);
            pdr.setData(list);
            pdr.setCode(0);
            pdr.setMsg("查询成功！");
        }
        return pdr;
    }

    /**
     * 实体类型
     * @param o
     * @return
     */
    public static ResponseResult  getObjectRes(Object o){
        ResponseResult rpr = new ResponseResult();
        if(o==null){
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("读取失败，未找到数据！");
        } else {
            rpr.setMsg("读取成功！");
            rpr.setData(o);
        }
        return rpr;
    }

    /**
     * 新增返回
     * @param num
     * @return
     */
    public static ResponseResult  getAddRes(int num){
        ResponseResult rpr = new ResponseResult();
        if (1 == num) {
            rpr.setMsg("新增成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("新增失败，请刷新页面后重试！");
        }
        return rpr;
    }

    /**
     * 修改返回
     * @param num
     * @return
     */
    public static ResponseResult  getUpdateRes(int num){
        ResponseResult rpr = new ResponseResult();
        if (1 == num) {
            rpr.setMsg("修改成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("修改异常！");
        }
        return rpr;
    }

    /**
     * 删除返回
     * @param num
     * @return
     */
    public static ResponseResult  getDelRes(int num){
        ResponseResult rpr = new ResponseResult();
        if (1 == num) {
            rpr.setMsg("删除成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("删除异常");
        }
        return rpr;
    }

}
