package com.xc.web.immanage.imfriend.entity;

public class ImFriend {
    private Long id;

    private Long userid;

    private Long friendid;

    private String remark;

    private Long createrid;

    private Long createtime;

    private Integer isblack;

    private Integer isfriend;

    private Integer receivetip;

    private Integer isSecretFriend;

    private Integer isSecretHistory;

    private Integer isScreenshotTip;

    private Integer isEphemeralChat;

    private Integer ephemeralChatTime;

    private Integer sourceType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getFriendid() {
        return friendid;
    }

    public void setFriendid(Long friendid) {
        this.friendid = friendid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCreaterid() {
        return createrid;
    }

    public void setCreaterid(Long createrid) {
        this.createrid = createrid;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Integer getIsblack() {
        return isblack;
    }

    public void setIsblack(Integer isblack) {
        this.isblack = isblack;
    }

    public Integer getIsfriend() {
        return isfriend;
    }

    public void setIsfriend(Integer isfriend) {
        this.isfriend = isfriend;
    }

    public Integer getReceivetip() {
        return receivetip;
    }

    public void setReceivetip(Integer receivetip) {
        this.receivetip = receivetip;
    }

    public Integer getIsSecretFriend() {
        return isSecretFriend;
    }

    public void setIsSecretFriend(Integer isSecretFriend) {
        this.isSecretFriend = isSecretFriend;
    }

    public Integer getIsSecretHistory() {
        return isSecretHistory;
    }

    public void setIsSecretHistory(Integer isSecretHistory) {
        this.isSecretHistory = isSecretHistory;
    }

    public Integer getIsScreenshotTip() {
        return isScreenshotTip;
    }

    public void setIsScreenshotTip(Integer isScreenshotTip) {
        this.isScreenshotTip = isScreenshotTip;
    }

    public Integer getIsEphemeralChat() {
        return isEphemeralChat;
    }

    public void setIsEphemeralChat(Integer isEphemeralChat) {
        this.isEphemeralChat = isEphemeralChat;
    }

    public Integer getEphemeralChatTime() {
        return ephemeralChatTime;
    }

    public void setEphemeralChatTime(Integer ephemeralChatTime) {
        this.ephemeralChatTime = ephemeralChatTime;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }
}