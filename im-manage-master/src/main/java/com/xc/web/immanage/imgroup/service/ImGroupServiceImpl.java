package com.xc.web.immanage.imgroup.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.immanage.imgroup.entity.ImGroup;
import com.xc.web.immanage.imgroup.entity.ImGroupVO;
import com.xc.web.immanage.imgroup.mapper.ImGroupMapper;
import com.xc.web.immanage.utils.ResponseUtil;

@Service
public class ImGroupServiceImpl implements ImGroupService {
	
	@Autowired
    private ImGroupMapper imGroupMapper;
	
    public ResponseResult queryAll() {
        ResponseResult rs = new ResponseResult();
        List<ImGroupVO> list = imGroupMapper.queryList(null);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    public ResponseResult selectByPrimaryKey(Long id) {
        ImGroup record = imGroupMapper.selectByPrimaryKey(id);
        return ResponseUtil.getObjectRes(record);
    }

    public ResponseResult add(ImGroup record) {
        return ResponseUtil.getAddRes( imGroupMapper.insertSelective(record));
    }

    public ResponseResult update(ImGroup record) {
       return ResponseUtil.getUpdateRes(imGroupMapper.updateByPrimaryKeySelective(record));
    }
    
    
    public ResponseResult deleteByPrimaryKeys(Long[] delIds) {
        return ResponseUtil.getDelRes(imGroupMapper.deleteByPrimaryKeys(delIds));
    }
    
    public PageDataResult queryList(int page, int limit, ImGroupVO record) {
        //模糊查询群名称
        if(StringUtils.isNotBlank(record.getName())){
            record.setName("%"+record.getName()+"%");
        }
        PageHelper.startPage(page, limit);
        List<ImGroupVO> list = imGroupMapper.queryList(record);
       return ResponseUtil.getPageRes(page,limit,list);
    }
}
