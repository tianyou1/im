package com.xc.web.immanage.imuser.controller;

import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/impages/user")
@TitleAnno(title = "用户页面")
public class ImUserPages {

    @RequestMapping("/userlist")
    @MethodAnno(title = "用户列表", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imusermanage")
    public String userlist() {
        return "immanage/imuser/imuserlist";
    }

    @RequestMapping("/imuserform")
    @MethodAnno(title = "用户详情", operation = MethodAnno.OPERA_TYPE_PAGE)
    @RequiresRoles(value = {"user", "admin"}, logical = Logical.OR)
    @RequiresPermissions("imusermanage")
    public String userform() {
        return "immanage/imuser/imuserform";
    }


}
