package com.xc.web.immanage.immsghistory.mapper;

import com.xc.web.immanage.imgroupmember.entity.ImGroupMemberVO;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistory;
import com.xc.web.immanage.immsghistory.entity.ImMessageHistoryVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImMessageHistoryMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ImMessageHistory record);

    int insertSelective(ImMessageHistory record);

    ImMessageHistory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImMessageHistory record);

    int updateByPrimaryKeyWithBLOBs(ImMessageHistory record);

    int updateByPrimaryKey(ImMessageHistory record);

    List<ImMessageHistory> queryList(ImMessageHistory record);

    List<ImMessageHistory> queryUserList(ImMessageHistoryVO record);

    /**
     * 定位
     * @param id
     * @return
     */
    ImMessageHistoryVO location(Long id);

}