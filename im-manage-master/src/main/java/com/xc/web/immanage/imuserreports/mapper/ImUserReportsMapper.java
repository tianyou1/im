package com.xc.web.immanage.imuserreports.mapper;

import com.xc.web.immanage.imuserreports.entity.ImUserReports;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBs;
import com.xc.web.immanage.imuserreports.entity.ImUserReportsWithBLOBsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ImUserReportsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ImUserReportsWithBLOBs record);

    int insertSelective(ImUserReportsWithBLOBs record);

    ImUserReportsWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ImUserReportsWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImUserReportsWithBLOBs record);

    int updateByPrimaryKey(ImUserReports record);

    List<ImUserReportsWithBLOBsVO> queryList(ImUserReportsWithBLOBsVO record);
}