-- --------------------------------------------------------
-- 主机:                           119.29.151.20
-- 服务器版本:                        5.7.31 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 monty 的数据库结构
DROP DATABASE IF EXISTS `monty`;
CREATE DATABASE IF NOT EXISTS `monty` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `monty`;

-- 导出  表 monty.t_test 结构
DROP TABLE IF EXISTS `t_test`;
CREATE TABLE IF NOT EXISTS `t_test` (
  `id` int(10) NOT NULL,
  `test` varchar(50) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='测试表';

-- 正在导出表  monty.t_test 的数据：~0 rows (大约)
DELETE FROM `t_test`;
/*!40000 ALTER TABLE `t_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_test` ENABLE KEYS */;

-- 导出  表 monty.web_department 结构
DROP TABLE IF EXISTS `web_department`;
CREATE TABLE IF NOT EXISTS `web_department` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `code` varchar(100) DEFAULT NULL COMMENT '部门编码',
  `type` varchar(50) DEFAULT NULL COMMENT '部门类型（COMMON_DEPT_TYPE）',
  `pid` int(10) DEFAULT NULL COMMENT '上级部门ID',
  `zindex` int(2) DEFAULT NULL COMMENT '部门序号',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='部门信息表';

-- 正在导出表  monty.web_department 的数据：~4 rows (大约)
DELETE FROM `web_department`;
/*!40000 ALTER TABLE `web_department` DISABLE KEYS */;
INSERT INTO `web_department` (`id`, `name`, `code`, `type`, `pid`, `zindex`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, '龙岩市', 'lonyan', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, '新罗区', 'xinluo', 'QX', 1, NULL, NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 19:59:46', 1, '0:0:0:0:0:0:0:1', '2020-06-23 19:59:46', 1),
	(5, '铁山镇', 'tieshang', 'XZ', 3, NULL, NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-28 09:11:17', 1, '0:0:0:0:0:0:0:1', '2020-06-28 09:11:17', 1),
	(6, '江山镇', 'jiangshan', 'XZ', 3, NULL, NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-28 09:12:41', 1, '0:0:0:0:0:0:0:1', '2020-06-28 09:12:41', 1);
/*!40000 ALTER TABLE `web_department` ENABLE KEYS */;

-- 导出  表 monty.web_dictionary 结构
DROP TABLE IF EXISTS `web_dictionary`;
CREATE TABLE IF NOT EXISTS `web_dictionary` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(50) DEFAULT NULL COMMENT '字典类型',
  `type_name` varchar(50) DEFAULT NULL COMMENT '字典名称',
  `seq_no` int(5) DEFAULT NULL COMMENT '排序号',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='字典表';

-- 正在导出表  monty.web_dictionary 的数据：~2 rows (大约)
DELETE FROM `web_dictionary`;
/*!40000 ALTER TABLE `web_dictionary` DISABLE KEYS */;
INSERT INTO `web_dictionary` (`id`, `type`, `type_name`, `seq_no`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, 'COMMON_SEX', '性别', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'SYS_DEPT_TYPE', '部门类型', 2, NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 19:39:25', 1, '0:0:0:0:0:0:0:1', '2020-06-23 19:39:25', 1);
/*!40000 ALTER TABLE `web_dictionary` ENABLE KEYS */;

-- 导出  表 monty.web_dictionary_detail 结构
DROP TABLE IF EXISTS `web_dictionary_detail`;
CREATE TABLE IF NOT EXISTS `web_dictionary_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dictionary_id` int(10) DEFAULT NULL COMMENT '字典id',
  `name` varchar(50) DEFAULT NULL COMMENT '字典名称',
  `value` varchar(50) DEFAULT NULL COMMENT '字典值',
  `seq_no` int(5) DEFAULT NULL COMMENT '排序号',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='字典明细表';

-- 正在导出表  monty.web_dictionary_detail 的数据：~6 rows (大约)
DELETE FROM `web_dictionary_detail`;
/*!40000 ALTER TABLE `web_dictionary_detail` DISABLE KEYS */;
INSERT INTO `web_dictionary_detail` (`id`, `dictionary_id`, `name`, `value`, `seq_no`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, 1, '男', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, '女', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 2, '市级', 'SJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 2, '区县级', 'QX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 2, '乡镇级', 'XZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 2, '村级', 'CJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `web_dictionary_detail` ENABLE KEYS */;

-- 导出  表 monty.web_operation_log 结构
DROP TABLE IF EXISTS `web_operation_log`;
CREATE TABLE IF NOT EXISTS `web_operation_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `operator_orgid` int(10) DEFAULT NULL COMMENT '操作用户机构id',
  `operator_id` int(10) DEFAULT NULL COMMENT '操作用户id',
  `operator_name` varchar(152) DEFAULT NULL COMMENT '操作用户名（user_name+name）',
  `module_name` varchar(30) DEFAULT NULL COMMENT '模块名称',
  `operation_type` varchar(20) DEFAULT NULL COMMENT '操作类型（查询、新增、修改、删除、导入、导出、上传、下载）',
  `operation_status` varchar(20) DEFAULT NULL COMMENT '操作状态（成功、失败）',
  `request_url` varchar(500) DEFAULT NULL COMMENT '请求URL地址',
  `operation_params` varchar(500) DEFAULT NULL COMMENT '操作参数',
  `request_method` varchar(20) DEFAULT NULL COMMENT '请求方法',
  `user_agent` varchar(500) DEFAULT NULL COMMENT '用户代理',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='操作日志表';

-- 正在导出表  monty.web_operation_log 的数据：~7 rows (大约)
DELETE FROM `web_operation_log`;
/*!40000 ALTER TABLE `web_operation_log` DISABLE KEYS */;
INSERT INTO `web_operation_log` (`id`, `operator_orgid`, `operator_id`, `operator_name`, `module_name`, `operation_type`, `operation_status`, `request_url`, `operation_params`, `request_method`, `user_agent`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, 5, 1, '管理员', '操作日志', '查询', '成功', '/operationlog/queryList', '{"page":["1"],"limit":["10"]}', '查询', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:34:26', 1, '110.87.75.188', '2020-08-17 05:34:26', 1),
	(2, 5, 1, '管理员', '角色管理', '查询', '成功', '/role/queryList', '{"page":["1"],"limit":["10"]}', '查询', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:36:50', 1, '110.87.75.188', '2020-08-17 05:36:50', 1),
	(3, 5, 1, '管理员', '角色管理', '查看角色权限', '成功', '/rolepermission/selectRolePermissionByRoleId', '{"roleId":["1"]}', '查看角色权限', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:36:56', 1, '110.87.75.188', '2020-08-17 05:36:56', 1),
	(4, 5, 1, '管理员', '角色管理', '修改角色权限', '成功', '/rolepermission/setRolePermissionByRoleId', '{"roleId":["1"],"permissionIds[]":["1","2","3","4","18","19","17"]}', '修改角色权限', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:37:06', 1, '110.87.75.188', '2020-08-17 05:37:06', 1),
	(5, 5, 1, '管理员', '角色管理', '查看', '成功', '/role/read', '{"id":["1"]}', '查看', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:37:13', 1, '110.87.75.188', '2020-08-17 05:37:13', 1),
	(6, 5, 1, '管理员', '角色管理', '查询', '成功', '/role/queryList', '{"page":["1"],"limit":["10"]}', '查询', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:37:15', 1, '110.87.75.188', '2020-08-17 05:37:15', 1),
	(7, 5, 1, '管理员', '角色管理', '查看角色权限', '成功', '/rolepermission/selectRolePermissionByRoleId', '{"roleId":["1"]}', '查看角色权限', '操作系统名称:Linux操作系统构架amd64操作系统版本3.10.0-1062.18.1.el7.x86_64', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 05:37:16', 1, '110.87.75.188', '2020-08-17 05:37:16', 1);
/*!40000 ALTER TABLE `web_operation_log` ENABLE KEYS */;

-- 导出  表 monty.web_permission 结构
DROP TABLE IF EXISTS `web_permission`;
CREATE TABLE IF NOT EXISTS `web_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '菜单名称',
  `pid` int(11) DEFAULT NULL COMMENT '父菜单id',
  `zindex` int(2) DEFAULT NULL COMMENT '菜单排序',
  `istype` int(1) DEFAULT NULL COMMENT '权限分类（0 菜单；1 功能）',
  `descpt` varchar(50) DEFAULT NULL COMMENT '描述',
  `code` varchar(20) DEFAULT NULL COMMENT '菜单编码',
  `icon` varchar(30) DEFAULT NULL COMMENT '菜单图标名称',
  `page` varchar(50) DEFAULT NULL COMMENT '菜单url',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='权限信息表';

-- 正在导出表  monty.web_permission 的数据：~13 rows (大约)
DELETE FROM `web_permission`;
/*!40000 ALTER TABLE `web_permission` DISABLE KEYS */;
INSERT INTO `web_permission` (`id`, `name`, `pid`, `zindex`, `istype`, `descpt`, `code`, `icon`, `page`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, '系统管理', 0, 100, 0, '系统管理', 'system', 'layui-icon-home', '/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(2, '用户管理', 1, 1100, 0, '用户管理', 'usermanage', '', '/pages/user/userlist', NULL, NULL, NULL, 1, '110.87.75.188', '2020-08-17 03:48:23', NULL, NULL, NULL, 2),
	(3, '角色管理', 1, 1200, 0, '角色管理', 'rolemanage', '', '/pages/role/list', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(4, '菜单管理', 1, 1300, 0, '权限管理', 'permissionmmanage', '', '/pages/permission/permissionlist', NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 09:59:43', NULL, NULL, NULL, 3),
	(5, '商品管理', 0, 300, 0, '商品管理', 'shops', NULL, '/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(6, '渠道管理', 0, 200, 0, '渠道管理', 'channel', NULL, '/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(10, '渠道信息列表', 6, 2200, 0, '渠道信息列表', 'channelPage', NULL, '/channel/channelListPage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(11, '渠道会员列表', 6, 2300, 0, '渠道会员列表', 'channelUsers', NULL, '/channel/channelUserListPage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(13, '商品列表1', 5, 3100, 0, '商品列表', 'shopPage', '', '/shop/shopPage', NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 09:41:47', NULL, NULL, NULL, 2),
	(16, '控制台', 15, 15100, 0, '控制台', 'console', NULL, '/pages/home/console', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(17, '操作日志', 1, 1700, 0, NULL, 'operationlogmanage', '', '/pages/log/operationloglist', NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 18:09:34', NULL, NULL, NULL, 5),
	(18, '字典管理', 1, 1400, 0, NULL, 'dictionarymanage', '', '/pages/dictionary/dictionarylist', NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 10:00:52', 1, '0:0:0:0:0:0:0:1', '2020-06-23 09:59:21', 3),
	(19, '部门管理', 1, 1600, 0, NULL, 'departmentmanage', '', '/pages/department/departmentlist', NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 17:50:42', 1, '0:0:0:0:0:0:0:1', '2020-06-23 17:50:42', 1);
/*!40000 ALTER TABLE `web_permission` ENABLE KEYS */;

-- 导出  表 monty.web_role 结构
DROP TABLE IF EXISTS `web_role`;
CREATE TABLE IF NOT EXISTS `web_role` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) DEFAULT NULL COMMENT '角色名称',
  `descpt` varchar(50) DEFAULT NULL COMMENT '角色描述',
  `code` varchar(20) DEFAULT NULL COMMENT '角色编号',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

-- 正在导出表  monty.web_role 的数据：~2 rows (大约)
DELETE FROM `web_role`;
/*!40000 ALTER TABLE `web_role` DISABLE KEYS */;
INSERT INTO `web_role` (`id`, `role_name`, `descpt`, `code`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, '超级管理员', '超级管理员', 'superrmanage', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, '管理员', '普通管理员1', 'manage', NULL, NULL, NULL, 1, '0:0:0:0:0:0:0:1', '2020-06-23 09:41:00', 1, '0:0:0:0:0:0:0:1', '2020-06-22 14:00:24', 6);
/*!40000 ALTER TABLE `web_role` ENABLE KEYS */;

-- 导出  表 monty.web_role_permission 结构
DROP TABLE IF EXISTS `web_role_permission`;
CREATE TABLE IF NOT EXISTS `web_role_permission` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `permit_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permit_id` (`permit_id`,`role_id`),
  KEY `perimitid` (`permit_id`) USING BTREE,
  KEY `roleid` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='角色权限信息表';

-- 正在导出表  monty.web_role_permission 的数据：~7 rows (大约)
DELETE FROM `web_role_permission`;
/*!40000 ALTER TABLE `web_role_permission` DISABLE KEYS */;
INSERT INTO `web_role_permission` (`id`, `permit_id`, `role_id`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 18, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 19, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `web_role_permission` ENABLE KEYS */;

-- 导出  表 monty.web_user 结构
DROP TABLE IF EXISTS `web_user`;
CREATE TABLE IF NOT EXISTS `web_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `department_id` int(10) DEFAULT NULL COMMENT '部门id',
  `name` varchar(100) DEFAULT NULL COMMENT '姓名',
  `sex` int(1) NOT NULL DEFAULT '0' COMMENT '性别（0：未知，1：男，2女）',
  `idcard` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机号',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `is_lock` int(1) NOT NULL DEFAULT '0' COMMENT '是否锁定（0：正常；1：已锁定）',
  `is_del` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除（0：正常；1：已删除）',
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';

-- 正在导出表  monty.web_user 的数据：~1 rows (大约)
DELETE FROM `web_user`;
/*!40000 ALTER TABLE `web_user` DISABLE KEYS */;
INSERT INTO `web_user` (`id`, `username`, `password`, `department_id`, `name`, `sex`, `idcard`, `mobile`, `email`, `is_lock`, `is_del`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, 'admin', '038bdaf98f2037b31f1e75b5b4c9b26e', 5, '管理员', 1, '2', '112', '1121212', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `web_user` ENABLE KEYS */;

-- 导出  表 monty.web_user_role 结构
DROP TABLE IF EXISTS `web_user_role`;
CREATE TABLE IF NOT EXISTS `web_user_role` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(5) NOT NULL,
  `status` int(5) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `access_lock_code` int(5) DEFAULT NULL,
  `update_by` int(10) DEFAULT NULL,
  `update_by_address` varchar(16) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_by` int(10) DEFAULT NULL,
  `create_by_address` varchar(16) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `version` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`role_id`),
  KEY `userid` (`user_id`) USING BTREE,
  KEY `roleid` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户角色信息表';

-- 正在导出表  monty.web_user_role 的数据：~0 rows (大约)
DELETE FROM `web_user_role`;
/*!40000 ALTER TABLE `web_user_role` DISABLE KEYS */;
INSERT INTO `web_user_role` (`id`, `user_id`, `role_id`, `status`, `remark`, `access_lock_code`, `update_by`, `update_by_address`, `update_time`, `create_by`, `create_by_address`, `create_time`, `version`) VALUES
	(1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `web_user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
