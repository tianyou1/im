/**
 * 
 */
var XcSelect = function(config) {
	//没有数据则返回，防止后面程序执行
	if (!config){
		return;
	}
	//防止对象调用异常
	if (!this instanceof XcSelect){
		return new XcSelect();
	}
	this._idSelector = config.selector;//div元素id
	this._tips = config.tips;//默认提示信息
	this._data = config.data;//初始化数据
	this._dicType = config.dicType;//字典类型
	this._needAll = config.needAll;//是否包含全部选项
	this._defaultVal = config.defaultVal;//是否包含全部选项
	this._success = config.success || function(){};//成功之后执行
	this._fail = config.fail || function(){};//失败执行
};
XcSelect.prototype = {
	init : function() {
		var selector = this._idSelector;
		var needAll = this._needAll;
		var onSelectValue = this._defaultVal;
		var param = {
			page : 0,
			limit : 0,
			type : this._dicType
		};
		$.ajax({
            type: 'POST',
            url: '/dictionarydetail/selectByDictionaryType',
            data: param,
            success: (res) => {
            	var arr = needAll === true ? [ {
        			"id" : "",
        			"value" : "",
        			"name" : "全部"
        		} ] : [];
        		arr = arr.concat(res.data);
        		var $html = "";
        		for (var i = 0; i < arr.length; i++) {
        			if (onSelectValue && arr[i].value == onSelectValue) {
        				$html += "<option value='" + arr[i].value + "' selected>"
        						+ arr[i].name + "</option>";
        			} else {
        				$html += "<option value='" + arr[i].value + "'>" + arr[i].name
        						+ "</option>";
        			}
        		}
        		$("#" + selector).append($html);
        		this.render();
            }
        });
	},
	render : function() {
		// layui重新渲染
		layui.use('form', function() {
			var form = layui.form;// 高版本建议把括号去掉，有的低版本，需要加()
			form.render("select");
		});
	}

};

var XcSelectTree = function(config) {
	//没有数据则返回，防止后面程序执行
	if (!config){
		return;
	}
	//防止对象调用异常
	if (!this instanceof XcSelectTree){
		return new XcSelectTree();
	}
	this._idSelector = config.selector;//div元素id
	this._tips = config.tips;//默认提示信息
	this._data = config.data;//初始化数据
	this._success = config.success || function(){};//成功之后执行
	this._fail = config.fail || function(){};//失败执行
};
XcSelectTree.prototype = {
	/**
	 * 初始化返回数据类型下拉框
	 * 
	 * @param selector
	 *            下拉框ID
	 * @param param
	 *            自定义sql要传入的参数，例如
	 *            {cfgCode:'ENUM_GETTER',enumCategoryId:enumCategoryId}
	 *            其中cfgCode为必传参数
	 * @param needAll
	 *            true:代表显示全部
	 * @param onSelectValue
	 *            默认需要选中的值,否则默认选中第一个
	 */
	init : function() {
		var selector = this._idSelector;
		var tips = this._tips;
		var date = this._data;
		var $html = $('\
				<div class="layui-unselect layui-form-select downpanel"> \
					<div class="layui-select-title"> \
						<span class="layui-input layui-unselect" id="treeclass">'+tips+'</span> \
						<input type="hidden" id="id'+selector+'" name="'+selector+'"> \
						<i class="layui-edge"></i> \
					</div> \
					<dl class="layui-anim layui-anim-upbit">\
						<dd>\
							<ul id="classtree"></ul> \
						</dd> \
					</dl>\
				</div>');
		layui.use([ 'element', 'tree', 'layer', 'form', 'upload' ], function() {
			var $ = layui.jquery, tree = layui.tree;
			tree.render({
						elem : "#" + selector+" #classtree",
						accordion : true,// 手风琴模式
						onlyIconControl : true, // 是否仅允许节点左侧图标控制展开收缩
						data : date,
						click : function(node) {
							var data = node.data;
							var $select = $($(this)[0].elem).parents(
									".layui-form-select");
							$select.removeClass("layui-form-selected").find(
									".layui-select-title span")
									.html(data.title).end().find(
											"input:hidden[name='"+selector+"']")
									.val(data.id);
						}
					});
			$("#" + selector+" .downpanel").on(
					"click",
					".layui-select-title",
					function(e) {
						$(".layui-form-select").not(
								$(this).parents(".layui-form-select"))
								.removeClass("layui-form-selected");
						$(this).parents(".downpanel").toggleClass(
								"layui-form-selected");
						layui.stope(e);
					}).on("click", "dl i", function(e) {
				layui.stope(e);
			});
			$(document).on("click", function(e) {
				$("#" + selector+" .layui-form-select").removeClass("layui-form-selected");
			});

		});
		$("#" + selector).append($html);
		Object.defineProperty(document.getElementById("id"+selector),"value",{
			set:(v)=>{
			if ( v!='' ){
				this._value= v;
				var treeText = $("div  [data-id="+v+"] span.layui-tree-txt").first().text();
				$("#" + selector + " #treeclass").text(treeText);
			}
			return v;
			},
		    get:()=>{
		        return this._value
		    }  
		});
		this._success;
	},
	render : function() {
		// layui重新渲染
		layui.use('form', function() {
			var form = layui.form;// 高版本建议把括号去掉，有的低版本，需要加()
			form.render("select");
		});
	}

}

var XcTree = function() {
	if (!this instanceof XcTree){
		return new XcTree();
	}
};
XcTree.prototype = {
	getLayUiTtreeData : function(baseid,list, config, isSpread, data) {
		for ( var y in list) {
			if ( baseid === list[y][config.pid]){
				var o = {};
				o.id = list[y][config.id];
				o.title = list[y][config.title];
				o.checked = list[y][config.checked]=="true"?true:false;
				o.spread = config.spread == null ? isSpread
						: list[y][config.spread];
	
				var result1 = [];
				var childrenList = this.getChildrenList(o.id, list, config.pid);
				if ( childrenList.length>0 ){
					o.checked=false;
				}
				o.children = this.getLayUiTtreeData(o.id,list, config, isSpread,
						result1);
				data.push(o);
			}
		}
		return data;
	},

	getChildrenList : function(id, list, pid) {
		var childrenList = [];
		for ( var y in list) {
			if (id === list[y][pid]) {
				childrenList.push(list[y]);
			}
		}
		return childrenList;
	}
}

var XcTableDicTool = function() {
    if (!this instanceof XcTableDicTool){
        return new XcTableDicTool();
    }
};
XcTableDicTool.prototype = {
	getDicDatasByType: function (type) {
        var param = {
            page : 0,
            limit : 0,
            type : type
        };
        var list = [];
        $.ajax({
            url : "/dictionarydetail/selectByDictionaryType", //后台数据请求地址
            type : "post",
            data : param,
            async : false,
            success : function(res) {
                if (0 === res.code) {
                    var result = [];
                    var datas = res.data;
                    for (var index in datas) {
                        result.push({id: datas[index].value, name: datas[index].name})
                    }
                    list = result;
                }
            }
        });
        return list;
    },
	matchDicValueById: function (id, list) {
        for (var each in list) {
            if (list[each].id == id) {
                return list[each].name;
            }
        }
    }

};
