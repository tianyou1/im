var LTSM2Util = function(){
	if (! this instanceof LTSM2Util ){
		return new LTSM2Util();
	}
};
LTSM2Util.prototype = {
		encode : function( str ) {
			return sm2Encrypt( str, this.pubkeyHex, 0 );
		},
		pubkeyHex : "04c20aa94cab0f9127426d4487492abc319efcfaa9f0d9145f891491ffdde38ec8b0791c150cf7c94931c75c44c5e98b1a907435ed4341b2ff7b71e99c0b6574d0"
};