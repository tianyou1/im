/**
 * 
 */
layui.config({
	base : '/layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'form' ], function() {
	var $ = layui.$, form = layui.form;
	new XcSelect({
		selector: "sex",
		tips: "请选择性别",
		dicType: "COMMON_SEX",
		needAll: false,
		defaultVal: ""
	}).init();
	
	function getData() {
		var data = [];
		$.ajax({
			url : "/department/queryAll", //后台数据请求地址
			type : "get",
			data : {},
			async : false,
			success : function(res) {
				var config = {
					"id" : "id",
					"title" : "name",
					"pid" : "pid",
					"checked" : "ischeck",
					"spread" : null
				}
				if (0 === res.code) {
					data = new XcTree().getLayUiTtreeData(0, res.data,
							config, true, data);
				}
			}
		});
		return data;
	};
	
	var data = getData();
	new XcSelectTree({
		selector: "departmentId",
		tips: "请选择部门",
		data: data,
        success: readmyinfo()
	}).init();

	function readmyinfo() {
        var param = {};
        $.ajax({
            type : 'POST',
            url : '/user/readmyinfo',
            data : param,
            success : function(res) {
                form.val('layuiadmin-form-myinfo', res.data);
                form.render();
            }
        })
    }
	
	form.on('submit(setmyinfo)', function(data) {
		var field = data.field; // 获取提交的字段
		var url = '/user/update';
		// 提交 Ajax 成功后，静态更新表格中的数据
		$.ajax({
			type : 'POST',
			url : url,
			data : field,
			success : function(res) {
				layer.msg(res.msg);
			}
		});
	});
	
	form.on('submit(resetmyinfo)', function(data) {
		readmyinfo();
	});

})