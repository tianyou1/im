/**
 * 菜单
 * */
  layui.config({
    base: '../layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use('index');
//获取路径uri
var pathUri=window.location.href;

// console.log("pathUrl:"+pathUri);
$(function(){
    layui.use('element', function(){
        var element = layui.element;
        // 左侧导航区域（可配合layui已有的垂直导航）
        $.get("/auth/getUserPerms",function(data){
            if(data!=null){
                getMenus(data);
                element.render('nav');
            }else{
                layer.alert("权限不足，请联系管理员",function () {
                    //退出
                    window.location.href="/logout";
                });
            }
        });
    });
})
var getMenus=function(data){
    //回显选中
    var ul=$('<ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">');
    for(var i=0;i < data.length;i++){
        var node=data[i];
        if( node.istype==0){
            if(node.pid==0){
                var li=$('<li data-name="'+node.code+'" class="layui-nav-item"></li>');
                //父级无page
                var lia=$('<a href="javascript:;" lay-tips="'+node.name+'" lay-direction="2"></a>');
                var ai=$('<i class="layui-icon '+node.icon+'"></i>');
                var acite=$('<cite>'+node.name+'</cite>');
                lia.append(ai);
                lia.append(acite);
                li.append(lia);
                //获取子节点
                var childArry = getParentArry(node.id, data);
                if(childArry.length>0){
                    var dl=$('<dl class="layui-nav-child"></dl>');
                    for (var y in childArry) {
                        var dd=$('<dd data-name="'+childArry[y].code+'" class=""> <a lay-href="'+childArry[y].page+'">'+childArry[y].name+'</a> </dd>');
                        //判断选中状态
                        if(pathUri.indexOf(childArry[y].page)>0){
                            li.addClass("layui-nav-itemed");
                            dd.addClass("layui-this")
                        }
                        //TODO 由于layui菜单不是规范统一的，多级菜单需要手动更改样式实现；
                        dl.append(dd);
                    }
                    li.append(dl);
                }
                ul.append(li);
            }
        }
    }
    $(".layui-side-scroll").append(ul);
}
//根据菜单主键id获取下级菜单
//id：菜单主键id
//arry：菜单数组信息
function getParentArry(id, arry) {
    var newArry = new Array();
    for (var x in arry) {
        if (arry[x].pid == id)
            newArry.push(arry[x]);
    }
    return newArry;
}