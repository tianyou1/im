/**
 * 
 */
layui.config({
	base : '../../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'tree', 'util' ], function() {
	var $ = layui.$,tree = layui.tree, layer = layui.layer, util = layui.util;
	var param = {};
	param.roleId = parent.layui.$("#edit_id").val();
	// 基本演示
	tree.render({
		elem : '#permission-tree',
		data : getData(),
		showCheckbox : true ,// 是否显示复选框
		id : 'permission-tree'
	});
	
	function getData(){
	    var data = [];
	    $.ajax({
	        url: "/rolepermission/selectRolePermissionByRoleId",    //后台数据请求地址
	        type: "post",
	        data : param,
	        async:false,
	        success: function(res){
	        	var config = {
	        			"id": "id",
	        			"title": "name",
	        			"pid": "pid",
	        			"checked": "ischeck",
	        			"spread": null
	        	}
	        	if ( 0 === res.code ) {
	        		data = new XcTree().getLayUiTtreeData(0,res.data,config,true,data);
	        	}
	        }
	    });
	    return data;
	};
	// 按钮事件
	util.event('permission-tree-event', {
		submit : function(othis) {
			var checkedData = tree.getChecked('permission-tree'); // 获取选中节点的数据
			var ids = [];
			getCheckIds(checkedData,ids);
			param.permissionIds=ids;
			$.ajax({
			        url: "/rolepermission/setRolePermissionByRoleId",    //后台数据请求地址
			        type: "post",
			        data : param,
			        async:false,
			        success: function(res){
			        	layer.alert(res.msg, {
							shade : 0
						});
			        }
			    });
		}
	});
	function getCheckIds(checkedData,ids){
		for ( var y in checkedData) {
			ids.push(checkedData[y].id)
			if(checkedData[y].children.length>0){
				getCheckIds(checkedData[y].children,ids);
			}
		}
	}
});