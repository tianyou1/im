/**
 * 
 */

layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'table' ], function() {
	var $ = layui.$, form = layui.form, table = layui.table;
	// 用户管理
    var DicTool = new XcTableDicTool();
	var dicTempMap1 =  []; // 缓存字典对应数据
	table.render({
		id : 'LAY-user-manage',
		elem : '#LAY-user-manage',
		url : '/user/queryList' // 模拟接口
		,
		method : 'post',
		toolbar : 'default'
		// ,toolbar: '#toolbarDemo'
		// ,defaultToolbar: ['filter', 'exports', 'print']
		,
		cols : [ [ {
			type : 'checkbox',
			fixed : 'left'
		}, {
			type : 'numbers',
			title : '序号'
		}, {
			field : 'username',
			title : '用户名'
		}, {
			field : 'name',
			title : '姓名'
		}, {
			field : 'sex',
			title : '性别',
            templet: function(d){
				if (dicTempMap1.length === 0) { // 没有字典缓存则需要去后台读取
                    dicTempMap1 = DicTool.getDicDatasByType("COMMON_SEX");
				}
				// 匹配字典值
                return DicTool.matchDicValueById(d.sex, dicTempMap1);
            }
		}, {
			field : 'mobile',
			title : '手机号'
		}, {
			field : 'email',
			title : '邮箱'
		}, {
			title : '操作',
			width : 220,
			align : 'center',
			fixed : 'right',
			toolbar : '#table-useradmin-webuser'
		} ] ],
		page : true,
		height : 'full-180',
		text : '对不起，加载出现异常！'
	});
	new XcSelect({
		selector: "sex",
		tips: "请选择性别",
		dicType: "COMMON_SEX",
		needAll: true,
		defaultVal: ""
	}).init();
	// 监听头工具栏事件
	table.on('toolbar(LAY-user-manage)',function(obj) {
		var checkStatus = table.checkStatus(obj.config.id), data = checkStatus.data; // 获取选中的数据
		switch (obj.event) {
		case 'add':
			active.add();
			break;
		case 'update':
			if (data.length === 0) {
				layer.msg('请选择一行');
			} else if (data.length > 1) {
				layer.msg('只能同时编辑一个');
			} else {
				active.update(checkStatus.data[0].id);
			}
			break;
		case 'delete':
			active.batchdel();
			break;
		}
		;
	});
	// 监听行工具事件
	table.on('tool(LAY-user-manage)', function(obj) {
		var data = obj.data // 获得当前行数据
		, layEvent = obj.event; // 获得 lay-event 对应的值
		if (layEvent === 'detail') {
			layer.msg('查看操作');
		} else if (layEvent === 'del') {
			layer.confirm('真的删除行么', function(index) {
				obj.del(); // 删除对应行（tr）的DOM结构
				layer.close(index);
				active.del(data);
				// 向服务端发送删除指令
			});
		} else if (layEvent === 'edit') {
			active.update(data.id);
		} else if (layEvent === 'userrole'){
			updateUserRole(data.id)
		}
	});
	//监听搜索
	form.on('submit(LAY-user-front-search)', function(data) {
		var field = data.field;
		// 执行重载
		table.reload('LAY-user-manage', {
			where : field
		});
	});
	
	var updateUserRole = function(id) {
		$("#edit_id").val(id);
        layer.open({
            type: 2,
            title: '角色分配',
            content: 'userroleform.html',
            maxmin: true,
            area: ['550px', '550px'],
            btn: ['确定', '取消'],
            success: function(layero, index) {},
            yes: function(index, layero) {
                var iframeWindow = window['layui-layer-iframe' + index],
                submitID = 'LAY-user-role-submit',
                submit = layero.find('iframe').contents().find('#' + submitID);
                submit.trigger('click');
            }
        });
		
	}
	
	//事件
	var active = {
	    batchdel: function() {
	        var checkStatus = table.checkStatus('LAY-user-manage'),
	        checkData = checkStatus.data; // 得到选中的数据
	        if (checkData.length === 0) {
	            return layer.msg('请选择数据');
	        }

	        layer.prompt({
	            formType: 1,
	            title: '敏感操作，请验证口令'
	        },
	        function(value, index) {
	            layer.close(index);

	            layer.confirm('确定删除吗？',
	            function(index) {
	                var delIds = new Array();
	                checkData.forEach((item, index, array) => {
	                    // 执行代码
	                    delIds.push(item.id);
	                });
	                var param = {};
	                param.delIds = delIds;
	                // 执行 Ajax 后重载
	                $.ajax({
	                    type: 'POST',
	                    url: '/user/batchdel',
	                    data: param,
	                    success: function(res) {
	                        layer.msg(res.msg);
	                        active.reload();
	                    }
	                });
	            });
	        });
	    },
	    del: function(item) {
	        var delIds = new Array();
	        delIds.push(item.id);
	        var param = {};
	        param.delIds = delIds;
	        // 执行 Ajax 后重载
	        $.ajax({
	            type: 'POST',
	            url: '/user/batchdel',
	            data: param,
	            success: function(res) {
	                layer.msg(res.msg);
	                active.reload();
	            }
	        });
	    },
	    add: function() {
			$("#edit_id").val('');
	        layer.open({
	            type: 2,
	            title: '添加用户',
	            content: 'userform.html',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            yes: function(index, layero) {
	                var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'LAY-user-front-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    update: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'userform.html',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	                var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'LAY-user-front-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    reload: function() {
	        table.reload('LAY-user-manage', {
	            where: {}
	        });
	    }
	};

});