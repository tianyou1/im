layui.config({
	base : '../../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'form' ], function() {
	var $ = layui.$, form = layui.form;
	var param = {};
	param.userId = parent.layui.$("#edit_id").val();
	$.ajax({
		type : 'POST',
		url : '/userrole/readuserrole',
		data : param,
		success : function(res) {
			if ( 0===res.code ){
				var data = res.data;
				$("#user_role_limits").html("");
				for(var i=0;i < data.length;i++){
					var rodeNode=data[i];
					var role_box=null;
					if ("true"===rodeNode.ischeck){
						role_box=$('<input type="checkbox" name="limits['+i+']" lay-skin="primary" title="'+rodeNode.role_name+'" value="'+rodeNode.id+'" checked="">');
					} else {
						role_box=$('<input type="checkbox" name="limits['+i+']" lay-skin="primary" title="'+rodeNode.role_name+'" value="'+rodeNode.id+'" >');
					}
					$("#user_role_limits").append(role_box);
				}
			} else {
				
			}
			form.render('checkbox');
		}
	});
	form.on('submit(LAY-user-role-submit)', function(data) {
		var url = '/userrole/updateuserrole';
		var roleIds = [];
	    $('input[type=checkbox]:checked').each(function() {
	    	roleIds.push($(this).val());
	    });
	    param.roleIds=roleIds;
		// 提交 Ajax 成功后，静态更新表格中的数据
		$.ajax({
			type : 'POST',
			url : url,
			data : param,
			success : function(res) {
				layer.alert(res.msg, {
					shade : 0
				});
			}
		});

	});
})
