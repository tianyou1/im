//登录失效的时候挑战到首页重新登录
$(function(){
  if (top != window){
    top.location.href = window.location.href; 
  }
})

layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'user' ], function() {
	var $ = layui.$ 
	,setter = layui.setter 
	,admin = layui.admin 
	,form = layui.form 
	,router = layui.router()
    ,search = router.search;

    form.render();
    
    $('input[name="vercode"]').on('keydown', function (event) {
        if (event.keyCode == 13) {
        	//回车登录
        	$("#LAY-user-login-submit").trigger("click");
        }
    });

    //提交
    form.on('submit(LAY-user-login-submit)', function(obj){
    	var param = obj.field;
    	param.username = new LTSM2Util().encode(param.username+"vercode:"+param.vercode);
    	param.password = new LTSM2Util().encode(param.password+"vercode:"+param.vercode);
    	$.post("login.action",obj.field,function(data){
            if ( data.code=="1000" ){
            	window.location.href="/index";
            } else {
            	//实际使用时记得删除该代码
                layer.msg(data.msg, {
                  offset: '15px'
                  ,icon: 5
                });
            }
        });
    });
});