/**
 * 
 */
layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'form','layer' ], function() {
	var $ = layui.$, form = layui.form,layer=layui.layer;;

	var param = {};
	param.id = parent.layui.$("#edit_id").val();
	$.ajax({
		type : 'POST',
		url : '/permission/read',
		data : param,
		success : function(res) {
			form.val('layuiadmin-form-perm', res.data);
		}
	});
	form.on('submit(lay-table-submit)', function(data) {
		var field = data.field; // 获取提交的字段
		var url = "";
		if ( field.id==="" ){
			url = '/permission/add';
		} else {
			url = '/permission/update';
		}
		// 提交 Ajax 成功后，静态更新表格中的数据
		$.ajax({
			type : 'POST',
			url : url,
			data : field,
			success : function(res) {
				layer.msg(res.msg);
			}
		});

	});
})