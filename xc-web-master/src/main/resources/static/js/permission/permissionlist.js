/**
 * 
 */

var editObj=null,ptable=null,treeGrid=null,tableId='treeTable',layer=null;
layui.config({
	base : '../../../layuiadmin/'
}).extend({
    treeGrid:'layui/extend/treeGrid'
}).use(['jquery','treeGrid','layer'], function(){
    var $=layui.jquery;
    treeGrid = layui.treeGrid;//很重要
    layer=layui.layer;
    table = layui.table;
    ptable=treeGrid.render({
        id:tableId
        ,elem: '#'+tableId
        ,idField:'id'
        ,url:'/permission/queryAll'
        ,method: 'get'
        ,cellMinWidth: 100
        ,treeId:'id'//树形id字段名称
        ,treeUpId:'pid'//树形父id字段名称
        ,treeShowName:'name'//以树形式显示的字段
        ,cols: [
        	[
             {field:'name', width:300, title:'菜单名称'}
            ,{field:'id', title:'菜单ID'}
            ,{field:'pid', title:'上级菜单ID'}
            ,{field:'zindex', title:'排序号'}
            ,{field:'code', title:'权限编码'}
            ,{field:'icon', title:'菜单图标',templet:function(data){
            	return '<i class="layui-icon '+data.icon+'"></i>';
            }}
            ,{field:'page', width:300, title:'URL'}
            ,{width:150,title: '操作', align:'center'/*toolbar: '#barDemo'*/
                ,templet: function(d){
                    var html='';
                    var addBtn='<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="add">添加</a>';
                    var updateBtn='<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="update">修改</a>';
                    var delBtn='<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>';
                    return addBtn+updateBtn+delBtn;
                	}
                }
        ]
        	]
        ,page:false
    });
    // 监听头工具栏事件
    treeGrid.on('toolbar('+tableId+')',function(obj) {
		switch (obj.event) {
		case 'tool-add':
			active.add();
			break;
		case 'update':
			if (data.length === 0) {
				layer.msg('请选择一行');
			} else if (data.length > 1) {
				layer.msg('只能同时编辑一个');
			} else {
				layer.msg('编辑 [id]：' + checkStatus.data[0].id);
				active.update(checkStatus.data[0].id);
			}
			break;
		case 'delete':
			active.batchdel();
			break;
		}
		;
	});

    treeGrid.on('tool('+tableId+')',function (obj) {
    	var data = obj.data // 获得当前行数据
		, layEvent = obj.event; // 获得 lay-event 对应的值
        if (obj.event === 'del'){//删除行
        	layer.confirm("你确定删除数据吗？如果存在下级节点则一并删除，此操作不能撤销！", {icon: 3, title:'提示'},
                    function(index){//确定回调
        				active.del(data);
                        layer.close(index);
                    },function (index) {//取消回调
                       layer.close(index);
                    }
                );
        } else if (obj.event==="add"){//添加行
        	active.add();
        } else if (obj.event==="update"){//添加行
        	active.update(data.id);
        } else if (obj.event==='tool-add'){
        	aler(123)
        }
    });
    
    //事件
	var active = {
	    batchdel: function() {
	        var checkStatus = table.checkStatus('LAY-user-manage'),
	        checkData = checkStatus.data; // 得到选中的数据
	        if (checkData.length === 0) {
	            return layer.msg('请选择数据');
	        }

	        layer.prompt({
	            formType: 1,
	            title: '敏感操作，请验证口令'
	        },
	        function(value, index) {
	            layer.close(index);

	            layer.confirm('确定删除吗？',
	            function(index) {
	                var delIds = new Array();
	                checkData.forEach((item, index, array) => {
	                    // 执行代码
	                    delIds.push(item.id);
	                });
	                var param = {};
	                param.delIds = delIds;
	                // 执行 Ajax 后重载
	                $.ajax({
	                    type: 'POST',
	                    url: '/user/batchdel',
	                    data: param,
	                    success: function(res) {
	                        layer.msg(res.msg);
	                        active.reload();
	                    }
	                });
	            });
	        });
	    },
	    del: function(item) {
	        var delIds = new Array();
	        delIds.push(item.id);
	        var param = {};
	        param.delIds = delIds;
	        // 执行 Ajax 后重载
	        $.ajax({
	            type: 'POST',
	            url: '/permission/batchdel',
	            data: param,
	            success: function(res) {
	                layer.msg(res.msg);
	                active.reload();
	            }
	        });
	    },
	    add: function() {
			$("#edit_id").val('');
	        layer.open({
	            type: 2,
	            title: '新增',
	            content: 'permissionform.html',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();
	            }
	        });
	    },
	    update: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'permissionform.html',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();
	            }
	        });
	    },
	    reload: function() {
	    	window.location.reload();
	    }
	};

});