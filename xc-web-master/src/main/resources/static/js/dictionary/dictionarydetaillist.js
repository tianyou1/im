/**
 * 
 */

layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'table' ], function() {
	var $ = layui.$, form = layui.form, table = layui.table;
	var param = {};
	param.id = parent.layui.$("#edit_id").val();
	$("#dictionary_id").val(param.id);
	// 用户管理
	table.render({
		id : 'lay-table',
		elem : '#lay-table',
		url : '/dictionarydetail/queryList', // 模拟接口
		method : 'post',
		toolbar : 'default',
		cols : [ [
		{ type : 'checkbox', fixed : 'left' }
		,{ type : 'numbers', title : '序号' }
        ,{field:'name', title:'字典名称' }
        ,{field:'value', title:'字典值' }
        ,{field:'seqNo', title:'排序号' , sort: true}
		, {
			title : '操作',
			align : 'center',
			fixed : 'right',
			toolbar : '#lay-table-tool'
		} ] ],
		page : true,
		height : 'full-150',
		text : '对不起，加载出现异常！',
		where : {
          //传值
          dictionaryId : param.id,
		}
	});
	// 监听头工具栏事件
	table.on('toolbar(lay-table)',function(obj) {
		var checkStatus = table.checkStatus(obj.config.id), data = checkStatus.data; // 获取选中的数据
		switch (obj.event) {
		case 'add':
			active.add();
			break;
		case 'update':
			if (data.length === 0) {
				layer.msg('请选择一行');
			} else if (data.length > 1) {
				layer.msg('只能同时编辑一个');
			} else {
				layer.msg('编辑 [id]：' + checkStatus.data[0].id);
				active.update(checkStatus.data[0].id);
			}
			break;
		case 'delete':
			active.batchdel();
			break;
		}
		;
	});
	// 监听行工具事件
	table.on('tool(lay-table)', function(obj) {
		var data = obj.data // 获得当前行数据
		, layEvent = obj.event; // 获得 lay-event 对应的值
		if (layEvent === 'detail') {
			active.view(data.id);
		} else if (layEvent === 'del') {
			layer.confirm('真的删除行么', function(index) {
				obj.del(); // 删除对应行（tr）的DOM结构
				layer.close(index);
				active.del(data);
				// 向服务端发送删除指令
			});
		} else if (layEvent === 'edit') {
			active.update(data.id);
		}
	});
	//监听搜索
	form.on('submit(lay-table-search)', function(data) {
		var field = data.field;
		// 执行重载
		table.reload('lay-table', {
			where : field
		});
	});
	
	//事件
	var active = {
	    batchdel: function() {
	        var checkStatus = table.checkStatus('lay-table'),
	        checkData = checkStatus.data; // 得到选中的数据
	        if (checkData.length === 0) {
	            return layer.msg('请选择数据');
	        }

	        layer.prompt({
	            formType: 1,
	            title: '敏感操作，请验证口令'
	        },
	        function(value, index) {
	            layer.close(index);

	            layer.confirm('确定删除吗？',
	            function(index) {
	                var delIds = new Array();
	                checkData.forEach((item, index, array) => {
	                    // 执行代码
	                    delIds.push(item.id);
	                });
	                var param = {};
	                param.delIds = delIds;
	                // 执行 Ajax 后重载
	                $.ajax({
	                    type: 'POST',
	                    url: '/dictionarydetail/batchdel',
	                    data: param,
	                    success: function(res) {
	                        layer.msg(res.msg);
	                        active.reload();
	                    }
	                });
	            });
	        });
	    },
	    del: function(item) {
	        var delIds = new Array();
	        delIds.push(item.id);
	        var param = {};
	        param.delIds = delIds;
	        // 执行 Ajax 后重载
	        $.ajax({
	            type: 'POST',
	            url: '/dictionarydetail/batchdel',
	            data: param,
	            success: function(res) {
	                layer.msg(res.msg);
	                active.reload();
	            }
	        });
	    },
	    add: function() {
			$("#edit_id").val('');
	        layer.open({
	            type: 2,
	            title: '添加用户',
	            content: 'dictionarydetailform.html',
	            maxmin: true,
	            area: ['400px', '300px'],
	            btn: ['确定', '取消'],
	            yes: function(index, layero) {
	                var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    update: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'dictionarydetailform.html',
	            maxmin: true,
	            area: ['400px', '300px'],
	            btn: ['确定', '取消'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();//刷新数据
	            }
	        });
	    },
	    view: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'dictionarydetailform.html',
	            maxmin: true,
	            area: ['400px', '300px'],
	            btn: ['关闭'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	//active.reload();//刷新数据
	            }
	        });
	    },
	    reload: function() {
	        table.reload('lay-table', {
	            where: {}
	        });
	    }
	};

});