/**
 * 
 */
layui.config({
	base : '../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'form','layer' ], function() {
	var $ = layui.$, form = layui.form,layer=layui.layer;;

	var param = {};
	param.id = parent.layui.$("#edit_id").val();
	param.add_id = parent.layui.$("#add_id").val();
	var xcSelect = new XcSelect();
	var paramXcSelect = {
		page : 0,
		limit : 0,
		type : "SYS_DEPT_TYPE"
	}
	xcSelect.initSelect("type",paramXcSelect,false,'');
	$("#pid").val(param.add_id);
	$.ajax({
		type : 'POST',
		url : '/department/read',
		data : param,
		success : function(res) {
			form.val('layuiadmin-form-useradmin', res.data);
		}
	});
	form.on('submit(lay-table-submit)', function(data) {
		var field = data.field; // 获取提交的字段
		var url = "";
		if ( field.id==="" ){
			url = '/department/add';
		} else {
			url = '/department/update';
		}
		// 提交 Ajax 成功后，静态更新表格中的数据
		$.ajax({
			type : 'POST',
			url : url,
			data : field,
			success : function(res) {
				layer.msg(res.msg);
			}
		});

	});
})