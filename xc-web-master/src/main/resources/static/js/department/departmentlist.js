/**
 * 
 */
layui.config({
	base : '../../../layuiadmin/' // 静态资源所在路径
}).extend({
	index : 'lib/index' // 主入口模块
}).use([ 'index', 'tree', 'util' ], function() {
	var $ = layui.$,tree = layui.tree, layer = layui.layer, util = layui.util;
	var param = {};
	param.roleId = parent.layui.$("#edit_id").val();
	// 基本演示
	tree.render({
		id : 'department-tree',
		elem : '#department-tree',
		data : getData(),
		customOperate: true,
	    edit: ['add', 'update', 'del'],// 操作节点的图标
	    operate: function(obj){
	        var type = obj.type; // 得到操作类型：add、edit、del
	        var data = obj.data; // 得到当前节点的数据
	        var elem = obj.elem; // 得到当前节点元素
	        // Ajax 操作
	        var id = data.id; // 得到节点索引
	        if(type === 'add'){ // 增加节点
	          // 返回 key 值
	        	active.add(id);
	        } else if(type === 'update'){ // 修改节点
	        	active.update(id); // 得到修改后的内容
	        } else if(type === 'del'){ // 删除节点
	        	active.del(id);
	        };
	        return false;
	      }
	});
	
	function getData(){
	    var data = [];
	    $.ajax({
	        url: "/department/queryAll",    //后台数据请求地址
	        type: "get",
	        data : param,
	        async:false,
	        success: function(res){
	        	var config = {
	        			"id": "id",
	        			"title": "name",
	        			"pid": "pid",
	        			"checked": "ischeck",
	        			"spread": null
	        	}
	        	if ( 0 === res.code ) {
	        		data = new XcTree().getLayUiTtreeData(0,res.data,config,true,data);
	        	}
	        }
	    });
	    return data;
	};
	
	//事件
	var active = {
	    batchdel: function() {
	        var checkStatus = table.checkStatus('LAY-department-manage'),
	        checkData = checkStatus.data; // 得到选中的数据
	        if (checkData.length === 0) {
	            return layer.msg('请选择数据');
	        }
	        
            layer.confirm('确定删除吗？',
            function(index) {
                var delIds = new Array();
                checkData.forEach((item, index, array) => {
                    // 执行代码
                    delIds.push(item.id);
                });
                var param = {};
                param.delIds = delIds;
                // 执行 Ajax 后重载
                $.ajax({
                    type: 'POST',
                    url: '/department/batchdel',
                    data: param,
                    success: function(res) {
                        layer.msg(res.msg);
                        active.reload();
                    }
                });
            });
            
	    },
	    del: function(id) {
	        var delIds = new Array();
	        delIds.push(id);
	        var param = {};
	        param.delIds = delIds;
	        layer.confirm('确定删除吗？',function(){
		        // 执行 Ajax 后重载
		        $.ajax({
		            type: 'POST',
		            url: '/department/batchdel',
		            data: param,
		            success: function(res) {
		                layer.msg(res.msg);
		                active.reload();
		            }
		        });
	        });
	    },
	    add: function(id) {
	    	$("#add_id").val(id);
	        layer.open({
	            type: 2,
	            title: '新增',
	            content: 'departmentform.html',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();
	            }
	        });
	    },
	    update: function(id) {
	        $("#edit_id").val(id);
	        layer.open({
	            type: 2,
	            title: '修改',
	            content: 'departmentform.html',
	            maxmin: true,
	            area: ['550px', '550px'],
	            btn: ['确定', '取消'],
	            success: function(layero, index) {},
	            yes: function(index, layero) {
	            	var iframeWindow = window['layui-layer-iframe' + index],
	                submitID = 'lay-table-submit',
	                submit = layero.find('iframe').contents().find('#' + submitID);
	                submit.trigger('click');
	                layer.close(index); // 关闭弹层
	            },
	            end: function() {
	            	active.reload();
	            }
	        });
	    },
	    reload: function() {
	    	window.location.reload();
	    }
	};
	
});