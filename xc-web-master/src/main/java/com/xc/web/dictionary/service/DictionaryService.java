package com.xc.web.dictionary.service;

import com.xc.web.base.BaseService;
import com.xc.web.dictionary.entity.Dictionary;

public interface DictionaryService extends BaseService<Dictionary> {

}
