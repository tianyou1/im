package com.xc.web.dictionary.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.dictionary.entity.Dictionary;
import com.xc.web.dictionary.service.DictionaryService;
import com.xc.web.log.annotation.TitleAnno;

@RestController
@RequestMapping("/dictionary")
@TitleAnno(title = "字典管理")
public class DictionaryController extends BaseController<Dictionary> {

    @Autowired
    DictionaryService dictionaryService;

    @Override
    public BaseService<Dictionary> getBaseServie() {
        // TODO Auto-generated method stub
        return dictionaryService;
    }

}
