package com.xc.web.dictionary.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.base.PageDataResult;
import com.xc.web.dictionary.entity.DictionaryDetail;
import com.xc.web.dictionary.service.DictionaryDetailService;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;

@RestController
@RequestMapping("/dictionarydetail")
@TitleAnno(title = "字典明细")
public class DictionaryDetailController
        extends BaseController<DictionaryDetail> {

    @Autowired
    DictionaryDetailService dictionaryDetailService;

    @Override
    public BaseService<DictionaryDetail> getBaseServie() {
        return dictionaryDetailService;
    }

    @MethodAnno(title = "查询列表", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/selectByDictionaryType", method = RequestMethod.POST)
    public PageDataResult selectByDictionaryType(@RequestParam("page") int page,
            @RequestParam("limit") int limit, @RequestParam("type") String type,
            HttpServletResponse response, HttpServletRequest request) {
        return dictionaryDetailService.selectByDictionaryType(page, limit,
                type);
    }

}
