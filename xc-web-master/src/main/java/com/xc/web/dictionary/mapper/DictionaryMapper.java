package com.xc.web.dictionary.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.dictionary.entity.Dictionary;

@Mapper
public interface DictionaryMapper extends BaseMapper<Dictionary> {

}