package com.xc.web.dictionary.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.base.PageDataResult;
import com.xc.web.dictionary.entity.DictionaryDetail;
import com.xc.web.dictionary.mapper.DictionaryDetailMapper;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DictionaryDetailServiceImpl extends
        BaseServiceImpl<DictionaryDetail> implements DictionaryDetailService {

    @Autowired
    private DictionaryDetailMapper dictionaryDetailMapper;

    @Override
    public BaseMapper<DictionaryDetail> getMapper() {
        return dictionaryDetailMapper;
    }

    @Override
    public PageDataResult selectByDictionaryType(int page, int limit,
            String type) {
        PageDataResult pdr = new PageDataResult();
       PageHelper.startPage(page, limit);
        List<DictionaryDetail> list = dictionaryDetailMapper
                .selectByDictionaryType(type);
        // 获取分页查询后的数据
        PageInfo<DictionaryDetail> pageInfo = new PageInfo<>(list);
        // 获取总记录数total
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if (0 == total) {
            pdr.setCount(0);
            pdr.setData(null);
            pdr.setCode(-1);
            pdr.setMsg("未查询到数据！");
        } else {
            pdr.setCount(total);
            pdr.setData(list);
            pdr.setCode(0);
            pdr.setMsg("查询成功！");
        }
        return pdr;
    }

}
