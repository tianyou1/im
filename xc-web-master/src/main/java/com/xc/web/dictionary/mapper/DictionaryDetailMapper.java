package com.xc.web.dictionary.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.dictionary.entity.DictionaryDetail;

@Mapper
public interface DictionaryDetailMapper extends BaseMapper<DictionaryDetail> {

    List<DictionaryDetail> selectByDictionaryType(String type);

}