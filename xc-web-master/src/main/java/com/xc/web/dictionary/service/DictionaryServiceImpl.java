package com.xc.web.dictionary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.dictionary.entity.Dictionary;
import com.xc.web.dictionary.mapper.DictionaryMapper;

@Service
public class DictionaryServiceImpl extends BaseServiceImpl<Dictionary>
        implements DictionaryService {

    @Autowired
    private DictionaryMapper dictionaryMapper;

    @Override
    public BaseMapper<Dictionary> getMapper() {
        return dictionaryMapper;
    }

}
