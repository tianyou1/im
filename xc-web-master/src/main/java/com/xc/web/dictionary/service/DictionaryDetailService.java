package com.xc.web.dictionary.service;

import com.xc.web.base.BaseService;
import com.xc.web.base.PageDataResult;
import com.xc.web.dictionary.entity.DictionaryDetail;

public interface DictionaryDetailService extends BaseService<DictionaryDetail> {

    public PageDataResult selectByDictionaryType(int page, int limit,
            String type);

}
