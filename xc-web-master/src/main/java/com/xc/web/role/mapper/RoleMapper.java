package com.xc.web.role.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.role.entity.Role;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {
}