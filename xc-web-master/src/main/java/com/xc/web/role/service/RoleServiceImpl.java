package com.xc.web.role.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.role.entity.Role;
import com.xc.web.role.mapper.RoleMapper;

@Service
public class RoleServiceImpl extends BaseServiceImpl<Role>
        implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public BaseMapper<Role> getMapper() {
        return roleMapper;
    }

}
