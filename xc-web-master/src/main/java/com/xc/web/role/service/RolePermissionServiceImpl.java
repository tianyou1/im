package com.xc.web.role.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.role.entity.RolePermission;
import com.xc.web.role.mapper.RolePermissionMapper;
import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.base.ResponseResult;

@Service
public class RolePermissionServiceImpl extends BaseServiceImpl<RolePermission>
        implements RolePermissionService {

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Override
    public BaseMapper<RolePermission> getMapper() {
        return rolePermissionMapper;
    }

    @Override
    public ResponseResult selectRolePermissionByRoleId(Integer roleId) {
        ResponseResult rs = new ResponseResult();
        List<Map<String, Object>> list = rolePermissionMapper
                .selectRolePermissionByRoleId(roleId);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    @Override
    public ResponseResult setRolePermissionByRoleId(Integer roleId,
            Integer[] permissionIds) {
        ResponseResult rs = new ResponseResult();
        rolePermissionMapper.deleteRolePermissionByRoleId(roleId);
        for (Integer permissionId : permissionIds) {
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermitId(permissionId);
            rolePermissionMapper.insert(rolePermission);
        }
        rs.setData("");
        rs.setCode(0);
        rs.setMsg("权限设置成功！");
        return rs;
    }
}
