package com.xc.web.role.service;

import com.xc.web.base.BaseService;
import com.xc.web.role.entity.Role;

public interface RoleService extends BaseService<Role> {

}
