package com.xc.web.role.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.role.entity.Role;
import com.xc.web.role.service.RoleService;

@RestController
@RequestMapping("/role")
@TitleAnno(title = "角色管理")
public class RoleController extends BaseController<Role> {

    @Autowired
    RoleService roleService;

    @Override
    public BaseService<Role> getBaseServie() {
        return roleService;
    }

}
