package com.xc.web.role.entity;

import com.xc.web.base.BaseEntity;

public class RolePermission extends BaseEntity {
    private Integer permitId;

    private Integer roleId;

    public Integer getPermitId() {
        return permitId;
    }

    public void setPermitId(Integer permitId) {
        this.permitId = permitId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}