package com.xc.web.role.service;

import com.xc.web.role.entity.RolePermission;
import com.xc.web.base.BaseService;
import com.xc.web.base.ResponseResult;

public interface RolePermissionService extends BaseService<RolePermission> {

    ResponseResult selectRolePermissionByRoleId(Integer roleId);

    ResponseResult setRolePermissionByRoleId(Integer roleId,
            Integer[] permissionIds);

}
