package com.xc.web.role.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.base.ResponseResult;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.role.entity.RolePermission;
import com.xc.web.role.service.RolePermissionService;

@RestController
@RequestMapping("/rolepermission")
@TitleAnno(title = "角色管理")
public class RolePermissionController extends BaseController<RolePermission> {

    @Autowired
    RolePermissionService rolePermissionService;

    @Override
    public BaseService<RolePermission> getBaseServie() {
        return rolePermissionService;
    }

    @MethodAnno(title = "查看角色权限", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/selectRolePermissionByRoleId", method = RequestMethod.POST)
    public ResponseResult selectRolePermissionByRoleId(
            @RequestParam("roleId") Integer roleId,
            HttpServletResponse response, HttpServletRequest request) {
        return rolePermissionService.selectRolePermissionByRoleId(roleId);
    }

    @MethodAnno(title = "修改角色权限", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/setRolePermissionByRoleId", method = RequestMethod.POST)
    public ResponseResult setRolePermissionByRoleId(
            @RequestParam("roleId") Integer roleId,
            @RequestParam("permissionIds[]") Integer[] permissionIds,
            HttpServletResponse response, HttpServletRequest request) {
        return rolePermissionService.setRolePermissionByRoleId(roleId,
                permissionIds);
    }

}
