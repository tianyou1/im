package com.xc.web.role.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.role.entity.RolePermission;

@Mapper
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    List<Map<String, Object>> selectRolePermissionByRoleId(Integer roleId);

    int deleteRolePermissionByRoleId(Integer roleId);

}