package com.xc.web.advice;

import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xc.web.base.ResponseResult;
import com.xc.web.utils.ExceptionEnum;

/**
 * 全局异常捕捉类
 * 
 * @author Bean
 *
 */
//@ControllerAdvice
public class AllException {
    
    private static final Logger logger = LoggerFactory
            .getLogger(AllException.class);

    /// 角色權权限异常捕捉
    @ExceptionHandler(value = UnauthorizedException.class)
    @ResponseBody // 在返回自定义相应类的情况下必须有，这是@ControllerAdvice注解的规定
    public ResponseResult roleException(UnauthorizedException e) {
        logger.error("---------------------->" + e);
        e.printStackTrace();
        ResponseResult rs = new ResponseResult();
        rs.setCode(ExceptionEnum.FORBIDDEN.getType());
        rs.setMsg(ExceptionEnum.FORBIDDEN.getMsg());
        return rs;
    }

    // 其它异常异常捕捉
    @ExceptionHandler(value = Exception.class)
    @ResponseBody // 在返回自定义相应类的情况下必须有，这是@ControllerAdvice注解的规定
    public ResponseResult allException(Exception e) {
        logger.error("---------------------->" + e);
        e.printStackTrace();
        ResponseResult rs = new ResponseResult();
        rs.setCode(ExceptionEnum.UNKNOW_ERROR.getType());
        rs.setMsg(ExceptionEnum.UNKNOW_ERROR.getMsg());
        return rs;
    }

}