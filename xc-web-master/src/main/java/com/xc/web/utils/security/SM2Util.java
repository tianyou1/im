package com.xc.web.utils.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.asn1.gm.GMNamedCurves;
import org.bouncycastle.asn1.gm.GMObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 国密SM2加解密工具类 椭圆参数 sm2p256v1
 */
public class SM2Util {

    private static Logger logger = LoggerFactory.getLogger(SM2Util.class);

    private volatile static SM2Util sm2Util;

    private SM2Util() {
    }

    public static SM2Util getInstance() {
        if (sm2Util == null) {
            synchronized (SM2Util.class) {
                if (sm2Util == null) {
                    sm2Util = new SM2Util();
                }
            }
        }
        return sm2Util;
    }

    /**
     * 生成秘钥对
     *
     * @return
     */
    public KeyPair createKeyPair() throws Exception {
        // 获取SM2 椭圆曲线推荐参数
        X9ECParameters ecParameters = GMNamedCurves.getByName("sm2p256v1");
        // 构造EC 算法参数
        ECNamedCurveParameterSpec sm2Spec = new ECNamedCurveParameterSpec(
                // 设置SM2 算法的 OID
                GMObjectIdentifiers.sm2p256v1.toString()
                // 设置曲线方程
                , ecParameters.getCurve()
                // 椭圆曲线G点
                , ecParameters.getG()
                // 大整数N
                , ecParameters.getN());
        // 创建 密钥对生成器
        KeyPairGenerator gen = KeyPairGenerator.getInstance("EC",
                new BouncyCastleProvider());
        // 使用SM2的算法区域初始化密钥生成器
        gen.initialize(sm2Spec, new SecureRandom());
        // 获取密钥对
        KeyPair keyPair = gen.generateKeyPair();
        logger.info("SM2私钥：");
        logger.info(Base64.toBase64String(keyPair.getPrivate().getEncoded()));
        logger.info("SM2公钥：");
        logger.info(Base64.toBase64String(keyPair.getPublic().getEncoded()));
        return keyPair;
    }

    /**
     * SM2公钥加密
     *
     * @param enData
     *            待加密明文
     * @param publickey
     *            SM2公钥（base64编码值）
     * @return 返回密文的base64编码值
     * @throws Exception
     */
    public String SM2Encrypt(String enData, String publickey)
            throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeySpecException, BadPaddingException,
            IllegalBlockSizeException, InvalidKeyException {
        Security.addProvider(new BouncyCastleProvider());
        byte[] sourceData = enData.getBytes();
        Cipher cp1 = Cipher.getInstance("SM2");
        // 解密由base64编码的公钥
        byte[] keyBytes = Base64.decode(publickey);
        // 构造X509EncodedKeySpec对象
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        // KEY_ALGORITHM 指定的加密算法
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        // 取公钥匙对象
        PublicKey pubkey = keyFactory.generatePublic(keySpec);
        // 加密
        cp1.init(Cipher.ENCRYPT_MODE, pubkey);
        return Base64.toBase64String(cp1.doFinal(sourceData));
    }

    /**
     * SM2私钥解密
     *
     * @param data
     *            待解密数据 base64编码值
     * @param privateKey
     *            SM2私钥
     * @return 返回解密后明文
     * @throws Exception
     */
    public String SM2Decrypt(String data, String privateKey)
            throws NoSuchPaddingException, NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException {
        Security.addProvider(new BouncyCastleProvider());
        Cipher cp2 = Cipher.getInstance("SM2");
        byte[] keyBytes = Base64.decode(privateKey);
        byte[] decryData = Base64.decode(data);
        // 构造PKCS8EncodedKeySpec对象
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        // KEY_ALGORITHM 指定的加密算法
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        // 取私钥匙对象
        PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
        cp2.init(Cipher.DECRYPT_MODE, priKey);
        return new String(cp2.doFinal(decryData), "utf-8");
    }

    /**
     * SM2签名
     *
     * @param data
     * @param privateKey
     * @return
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public String SM2Sign(String data, String privateKey)
            throws NoSuchProviderException, NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, SignatureException {
        Security.addProvider(new BouncyCastleProvider());
        byte[] keyBytes = Base64.decode(privateKey);
        byte[] signData = data.getBytes();
        // 构造PKCS8EncodedKeySpec对象
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        // KEY_ALGORITHM 指定的加密算法
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        // 取私钥匙对象
        PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
        Signature sig = Signature.getInstance("SM3withSM2", "BC");
        sig.initSign(priKey);
        sig.update(signData);
        return Base64.toBase64String(sig.sign());
    }

    /**
     * SM2验签
     *
     * @param data
     * @param publickey
     * @param signMsg
     * @return
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean SM2Verify(String data, String publickey, String signMsg)
            throws NoSuchProviderException, NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, SignatureException {
        Security.addProvider(new BouncyCastleProvider());
        byte[] pubBytes = Base64.decode(publickey);
        byte[] sourceData = data.getBytes();
        byte[] signData = Base64.decode(signMsg);
        Signature sign = Signature.getInstance("SM3withSM2", "BC");
        // 构造X509EncodedKeySpec对象
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pubBytes);
        // KEY_ALGORITHM 指定的加密算法
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        // 取公钥匙对象
        PublicKey pubkey = keyFactory.generatePublic(keySpec);
        sign.initVerify(pubkey);
        sign.update(sourceData);
        return sign.verify(signData);
    }

    public static void main(String[] args) {
        try {
            SM2Util.getInstance().createKeyPair();
            String temp = SM2Util.getInstance().SM2Encrypt("123456",
                    "MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAE40SYSyfHcRflCtqySluUlJsguFCiWM7665Y8N52bJZ5G+LQlPbfNSjnYSVFaxBrhBq9crMZJeaN+ljiyoHYaDQ==\r\n"
                            + "");
            logger.info(temp);
            logger.info(SM2Util.getInstance().SM2Decrypt(temp,
                    "MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQgkOLv0WCjVQVpV55TlXnXyg6pssGanwUGeqvNUhbmEKegCgYIKoEcz1UBgi2hRANCAATjRJhLJ8dxF+UK2rJKW5SUmyC4UKJYzvrrljw3nZslnkb4tCU9t81KOdhJUVrEGuEGr1ysxkl5o36WOLKgdhoN\r\n"
                            + ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
