package com.xc.web.utils.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

/**
 * 国密分组对称SM4加解密工具类 当前模式支持：ECB、CBC、OFB、CFB 算法PKCS5Padding、PKCS7Padding
 */
public class Sm4Util {

    private volatile static Sm4Util sm4Util;

    private Sm4Util() {
    }

    public static Sm4Util getInstance() {
        if (sm4Util == null) {
            synchronized (Sm4Util.class) {
                if (sm4Util == null) {
                    sm4Util = new Sm4Util();
                }
            }
        }
        return sm4Util;
    }

    /**
     * 生成SM4秘钥
     *
     * @param keySize
     * @return 返回为Base64编码的秘钥
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     */
    public String generateKey(int keySize)
            throws NoSuchProviderException, NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());
        KeyGenerator kg = KeyGenerator.getInstance("SM4",
                BouncyCastleProvider.PROVIDER_NAME);
        kg.init(keySize, new SecureRandom());
        byte[] keyByte = kg.generateKey().getEncoded();
        return Base64.toBase64String(keyByte);
    }

    /**
     * SM4加密
     *
     * @param msg
     *            待机密明文
     * @param algorithmname
     *            加密算法
     * @param secretkey
     *            秘钥 这里传16byte秘钥的base64编码值
     * @param ivkey
     *            初始向量 16byte 这里传16byte秘钥的base64编码值
     * @return 这里返回密文的base64编码值
     * @throws Exception
     */
    public String encrypt(String msg, String algorithmname, String secretkey,
            String ivkey)
            throws NoSuchPaddingException, NoSuchAlgorithmException,
            NoSuchProviderException, InvalidKeyException,
            InvalidAlgorithmParameterException, UnsupportedEncodingException,
            BadPaddingException, IllegalBlockSizeException {
        byte[] keyBytes = Base64.decode(secretkey);
        Security.addProvider(new BouncyCastleProvider());
        Key key = new SecretKeySpec(keyBytes, "SM4");
        Cipher in = Cipher.getInstance(algorithmname, "BC");
        if (algorithmname.contains("ECB")) {
            in.init(Cipher.ENCRYPT_MODE, key);
        } else if (algorithmname.contains("CBC")
                || algorithmname.contains("OFB")
                || algorithmname.contains("CFB")) {
            byte[] iv = Base64.decode(ivkey);
            in.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        }
        return Base64.toBase64String(in.doFinal(msg.getBytes("utf-8")));
    }

    /**
     * SM4解密
     *
     * @param msg
     *            待解密密文 这里传密文的base64编码值
     * @param algorithmname
     *            加密算法
     * @param secretkey
     *            加密秘钥 这里传16byte秘钥的base64编码值
     * @param ivkey
     *            初始向量 16byte 这里传16byte IV的base64编码值
     * @return
     * @throws Exception
     */
    public String decrypt(String msg, String algorithmname, String secretkey,
            String ivkey)
            throws NoSuchPaddingException, NoSuchAlgorithmException,
            NoSuchProviderException, InvalidKeyException,
            InvalidAlgorithmParameterException, BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException {
        byte[] keyBytes = Base64.decode(secretkey);
        Security.addProvider(new BouncyCastleProvider());
        Key key = new SecretKeySpec(keyBytes, "SM4");
        Cipher out = Cipher.getInstance(algorithmname, "BC");
        if (algorithmname.contains("ECB")) {
            out.init(Cipher.DECRYPT_MODE, key);
        } else if (algorithmname.contains("CBC")
                || algorithmname.contains("OFB")
                || algorithmname.contains("CFB")) {
            byte[] iv = Base64.decode(ivkey);
            out.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        }
        return new String(out.doFinal(Base64.decode(msg)), "utf-8");
    }

}
