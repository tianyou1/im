package com.xc.web.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String getCurrDateTime() {
        return sdf.format(new Date());
    }

}
