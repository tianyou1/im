package com.xc.web.auth.service;

import java.util.List;
import java.util.Set;

import com.xc.web.permission.entity.Permission;

public interface AuthService {

    List<Permission> getUserPerms(Integer id);

    Set<String> getUserPermsCode(Integer id);

}
