package com.xc.web.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.permission.entity.Permission;
import com.xc.web.permission.mapper.PermissionMapper;

import java.util.List;
import java.util.Set;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> getUserPerms(Integer id) {
        return this.permissionMapper.getUserPerms(id);
    }

    @Override
    public Set<String> getUserPermsCode(Integer id) {
        return this.permissionMapper.getUserPermsCode(id);
    }

}
