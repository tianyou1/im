package com.xc.web.auth.controller;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.xc.web.auth.service.AuthService;
import com.xc.web.permission.entity.Permission;
import com.xc.web.user.entity.User;

import java.util.List;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private static final Logger logger = LoggerFactory
            .getLogger(AuthController.class);
    @Autowired
    private AuthService authService;

    /**
     * 根据用户id查询权限树数据
     * 
     * @return PermTreeDTO
     */
    @RequestMapping(value = "/getUserPerms", method = RequestMethod.GET)
    @ResponseBody
    public List<Permission> getUserPerms() {
        logger.debug("根据用户id查询限树列表！");
        List<Permission> pvo = null;
        User currentUser = (User) SecurityUtils.getSubject().getPrincipal();
        if (null == currentUser) {
            logger.debug("根据用户id查询限树列表！用户未登录");
            return pvo;
        }
        try {
            pvo = authService.getUserPerms(currentUser.getId());
            // 生成页面需要的json格式
            logger.debug("根据用户id查询限树列表查询=pvo:" + pvo);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("根据用户id查询权限树列表查询异常！", e);
        }
        return pvo;
    }
    
}
