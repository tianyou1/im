package com.xc.web.user.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.user.entity.UserRole;

@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    void deleteUserRoleByUserId(Integer userId);

    List<Map<String, Object>> selectUserRoleByUserId(Integer userId);

}