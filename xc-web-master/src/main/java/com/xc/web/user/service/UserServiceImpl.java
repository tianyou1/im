package com.xc.web.user.service;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.config.ConstantConfig;
import com.xc.web.user.entity.User;
import com.xc.web.user.mapper.UserMapper;
import com.xc.web.utils.IStatusMessage;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    // 判断是否用户名是否存在
    @Override
    public User selectUsername(String username) {
        return userMapper.selectByUserName(username);
    }

    @Override
    public PageDataResult queryList(int page, int limit, User user) {

        PageDataResult pdr = new PageDataResult();
        PageHelper.startPage(page, limit);
        List<User> urList = userMapper.queryList(user);
        // 获取分页查询后的数据
        PageInfo<User> pageInfo = new PageInfo<>(urList);
        // 设置获取到的总记录数total：
        pdr.setCount(Long.valueOf(pageInfo.getTotal()).intValue());
        pdr.setData(urList);
        pdr.setCode(0);
        return pdr;
    }

    public static String getSourcePassword(String username, String password) {
        // 盐值用的用的是对用户名的加密
        ByteSource credentialsSalt01 = ByteSource.Util.bytes(username);
        Object credential = password;// 密码
        String hashAlgorithmName = "MD5";// 加密方式
        // 1024指的是加密的次数
        Object simpleHash = new SimpleHash(hashAlgorithmName, credential,
                credentialsSalt01, 1024);
        return simpleHash.toString();
    }

    @Override
    public ResponseResult add(User user) {
        // 用户新增的默认密码
        if (StringUtil.isEmpty(user.getPassword())) {
            user.setPassword(ConstantConfig.DEFAULT_PASSWORD);
        }
        String password = getSourcePassword(user.getUsername(),
                user.getPassword());
        user.setPassword(password);
        // TODO Auto-generated method stub
        ResponseResult rpr = new ResponseResult();
        int num = userMapper.insertSelective(user);
        if (1 != num) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("新增异常");
        }
        return rpr;
    }

    @Override
    public ResponseResult update(User user) {
        ResponseResult rpr = new ResponseResult();
        if (null == user.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("被修改的用户id不能为空");
        } else {
            int num = userMapper.updateByPrimaryKeySelective(user);
            if (1 != num) {
                rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
                rpr.setMsg("修改异常");
            }
        }
        return rpr;
    }

    @Override
    public ResponseResult deleteByPrimaryKeys(Integer[] delIds) {
        ResponseResult rpr = new ResponseResult();
        int num = userMapper.deleteByPrimaryKeys(delIds);
        if (num < 0) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("删除异常");
        }
        return rpr;
    }

    @Override
    public ResponseResult selectByPrimaryKey(Integer id) {
        ResponseResult rpr = new ResponseResult();
        User user = userMapper.selectByPrimaryKey(id);
        if (user == null) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("未找到该用户！");
        } else {
            rpr.setData(user);
        }
        return rpr;
    }

    @Override
    public ResponseResult setmypass(String oldPassword, String password) {
        ResponseResult rpr = new ResponseResult();
        User currentUser = (User) SecurityUtils.getSubject().getPrincipal();
        String curroldPassword = getSourcePassword(currentUser.getUsername(),
                oldPassword);
        if (curroldPassword.equals(currentUser.getPassword())) {
            String newpassword = getSourcePassword(currentUser.getUsername(),
                    password);
            User user = new User();
            user.setId(currentUser.getId());
            user.setPassword(newpassword);
            int num = userMapper.updateByPrimaryKeySelective(user);
            if (1 == num) {
                rpr.setMsg("密码修改成功！");
            } else {
                rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
                rpr.setMsg("密码修改异常！");
            }
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("旧密码不正确！");
        }
        return rpr;
    }

}