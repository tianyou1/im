package com.xc.web.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.base.ResponseResult;
import com.xc.web.user.entity.UserRole;
import com.xc.web.user.mapper.UserRoleMapper;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public ResponseResult readUserRoleByUserId(Integer userId) {
        ResponseResult rs = new ResponseResult();
        List<Map<String, Object>> list = userRoleMapper
                .selectUserRoleByUserId(userId);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    @Override
    public ResponseResult updateUserRoleByUserId(Integer userId,
            Integer[] roleIds) {
        ResponseResult rs = new ResponseResult();
        userRoleMapper.deleteUserRoleByUserId(userId);
        for (Integer roleId : roleIds) {
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);
            userRoleMapper.insert(userRole);
        }
        rs.setData("");
        rs.setCode(0);
        rs.setMsg("权限设置成功！");
        return rs;
    }

}