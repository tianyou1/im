package com.xc.web.user.service;

import com.xc.web.base.ResponseResult;

public interface UserRoleService {

    ResponseResult readUserRoleByUserId(Integer userId);

    ResponseResult updateUserRoleByUserId(Integer userId, Integer[] roleIds);

}
