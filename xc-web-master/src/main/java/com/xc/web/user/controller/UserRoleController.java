package com.xc.web.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.ResponseResult;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.user.service.UserRoleService;

@RestController
@RequestMapping("/userrole")
@TitleAnno(title = "用户角色管理")
public class UserRoleController {
    @Autowired
    UserRoleService userRoleService;

    @MethodAnno(title = "用户角色查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/readuserrole", method = RequestMethod.POST)
    public ResponseResult readUserRoleByUserId(
            @RequestParam("userId") Integer userId,
            HttpServletResponse response, HttpServletRequest request) {
        return userRoleService.readUserRoleByUserId(userId);
    }

    @MethodAnno(title = "用户角色修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/updateuserrole", method = RequestMethod.POST)
    public ResponseResult updateUserRoleByUserId(
            @RequestParam("userId") Integer userId,
            @RequestParam("roleIds[]") Integer[] roleIds,
            HttpServletResponse response, HttpServletRequest request) {
        return userRoleService.updateUserRoleByUserId(userId, roleIds);
    }

}
