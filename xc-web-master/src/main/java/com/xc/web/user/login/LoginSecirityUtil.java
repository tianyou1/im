package com.xc.web.user.login;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

import org.bouncycastle.asn1.gm.GMNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;

public class LoginSecirityUtil {

    private static final String private_key = "8921413a604182968c860c12ca2d5bf8a05b82b1de8039691c4c46afd5101540";
    // private static final String public_key =
    // "04c20aa94cab0f9127426d4487492abc319efcfaa9f0d9145f891491ffdde38ec8b0791c150cf7c94931c75c44c5e98b1a907435ed4341b2ff7b71e99c0b6574d0";

    public static void generatorKey() throws NoSuchAlgorithmException {
        // 生成密钥对
        X9ECParameters sm2ECParameters = GMNamedCurves.getByName("sm2p256v1");
        ECDomainParameters domainParameters = new ECDomainParameters(
                sm2ECParameters.getCurve(), sm2ECParameters.getG(),
                sm2ECParameters.getN());
        ECKeyPairGenerator keyPairGenerator = new ECKeyPairGenerator();
        keyPairGenerator.init(new ECKeyGenerationParameters(domainParameters,
                SecureRandom.getInstance("SHA1PRNG")));
        AsymmetricCipherKeyPair asymmetricCipherKeyPair = keyPairGenerator
                .generateKeyPair();

        // 私钥，16进制格式，自己保存，格式如a2081b5b81fbea0b6b973a3ab6dbbbc65b1164488bf22d8ae2ff0b8260f64853
        BigInteger privatekey = ((ECPrivateKeyParameters) asymmetricCipherKeyPair
                .getPrivate()).getD();
        String privateKeyHex = privatekey.toString(16);
        System.out.println(privateKeyHex);
        // 公钥，16进制格式，发给前端，格式如04813d4d97ad31bd9d18d785f337f683233099d5abed09cb397152d50ac28cc0ba43711960e811d90453db5f5a9518d660858a8d0c57e359a8bf83427760ebcbba
        ECPoint ecPoint = ((ECPublicKeyParameters) asymmetricCipherKeyPair
                .getPublic()).getQ();
        String publicKeyHex = Hex.toHexString(ecPoint.getEncoded(false));
        System.out.println(publicKeyHex);
    }

    public static String decrypt(String str) {

        // 生成密钥对
        X9ECParameters sm2ECParameters = GMNamedCurves.getByName("sm2p256v1");
        ECDomainParameters domainParameters = new ECDomainParameters(
                sm2ECParameters.getCurve(), sm2ECParameters.getG(),
                sm2ECParameters.getN());
        // JS加密产生的密文
        // String str =
        // "04b998bf52d2e4dd34e6468de52f3a6574318e221196db7ac754145349ca26a05a3dd9e106ae6c96373978231e8fe3fee0e9448dbfde808228e541e009230ecbe104973107b8f523319b91b967d92872d3684c50ce0556aadb7dff9008e6fc9e76971d34837ed1f3d743c0487dc5e8b55583b670f47c604f1b344500c2";
        byte[] cipherDataByte = Hex.decode(str);
        // 刚才的私钥Hex，先还原私钥
        // String privateKey =
        // "c711c57aa6f4bf6ae690fa4d06f607874dc07d071665254b64cf115cf157f1d8";
        BigInteger privateKeyD = new BigInteger(private_key, 16);
        ECPrivateKeyParameters privateKeyParameters = new ECPrivateKeyParameters(
                privateKeyD, domainParameters);
        // 用私钥解密
        SM2Engine sm2Engine = new SM2Engine();
        sm2Engine.init(false, privateKeyParameters);
        // processBlock得到Base64格式，记得解码
        byte[] arrayOfBytes = null;
        try {
            arrayOfBytes = Base64.getDecoder().decode(sm2Engine
                    .processBlock(cipherDataByte, 0, cipherDataByte.length));
        } catch (InvalidCipherTextException e) {
            e.printStackTrace();
        }
        // 得到明文：SM2 Encryption Test
        return new String(arrayOfBytes);
    }

}
