package com.xc.web.user.service;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.user.entity.User;

public interface UserService {

    User selectUsername(String username);

    PageDataResult queryList(int page, int limit, User user);

    ResponseResult add(User user);

    ResponseResult deleteByPrimaryKeys(Integer[] delIds);

    ResponseResult selectByPrimaryKey(Integer id);

    ResponseResult update(User user);

    ResponseResult setmypass(String oldPassword, String password);

}
