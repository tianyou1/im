package com.xc.web.user.page;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pages/user")
public class UserPage {

    @RequestMapping("/userlist")
    @RequiresRoles(value = { "user", "admin" }, logical = Logical.OR)
    @RequiresPermissions("usermanage")
    public String indexHtml() {
        return "user/userlist";
    }

}
