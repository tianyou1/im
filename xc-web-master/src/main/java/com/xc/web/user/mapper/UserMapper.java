package com.xc.web.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.user.entity.User;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User selectByUserName(String username);

    List<User> queryList(User user);

    int deleteByPrimaryKeys(Integer[] delIds);
}