package com.xc.web.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.PageDataResult;
import com.xc.web.base.ResponseResult;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.user.entity.User;
import com.xc.web.user.service.UserService;

@RestController
@RequestMapping("/user")
@TitleAnno(title = "用户管理")
public class UserController {
    
    private static final Logger logger = LoggerFactory
            .getLogger(UserController.class);

    @Autowired
    UserService userService;

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @RequiresPermissions("usermanage")
    public PageDataResult queryList(@RequestParam("page") int page,
            @RequestParam("limit") int limit, User user,
            HttpServletResponse response, HttpServletRequest request) {
        logger.debug("用户列表【读取】user:" + user);
        return userService.queryList(page, limit, user);
    }

    @MethodAnno(title = "查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    @RequiresPermissions("usermanage")
    public ResponseResult read(@RequestParam("id") Integer id,
            HttpServletResponse response, HttpServletRequest request) {
        logger.debug("用户信息【读取】user:" + id);
        return userService.selectByPrimaryKey(id);
    }

    @MethodAnno(title = "新增", operation = MethodAnno.OPERA_TYPE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @RequiresPermissions("usermanage")
    public ResponseResult add(User user, HttpServletResponse response,
            HttpServletRequest request) {
        logger.debug("用户信息【新增】！user:" + user);
        return userService.add(user);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @RequiresPermissions("usermanage")
    public ResponseResult update(User user, HttpServletResponse response,
            HttpServletRequest request) {
        logger.debug("用户信息【修改】！user:" + user);
        return userService.update(user);
    }

    @MethodAnno(title = "删除", operation = MethodAnno.OPERA_TYPE_DEL)
    @RequestMapping(value = "/batchdel", method = RequestMethod.POST)
    @RequiresPermissions("usermanage")
    public ResponseResult batchdel(@RequestParam("delIds[]") Integer[] delIds,
            HttpServletResponse response, HttpServletRequest request) {
        logger.debug("用户信息【删除】！user:" + delIds);
        return userService.deleteByPrimaryKeys(delIds);
    }

    @MethodAnno(title = "查看个人资料", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/readmyinfo", method = RequestMethod.POST)
    public ResponseResult readmyinfo(HttpServletResponse response,
            HttpServletRequest request) {
        User currentUser = (User) SecurityUtils.getSubject().getPrincipal();
        Integer id = currentUser.getId();
        logger.debug("用户信息【读取】user:" + id);
        return userService.selectByPrimaryKey(id);
    }

    @MethodAnno(title = "修改密码", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/setmypass", method = RequestMethod.POST)
    public ResponseResult setmypass(
            @RequestParam("oldPassword") String oldPassword,
            @RequestParam("password") String password,
            HttpServletResponse response, HttpServletRequest request) {
        logger.debug("修改用户密码！user:" + oldPassword);
        return userService.setmypass(oldPassword, password);
    }

}
