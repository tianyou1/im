package com.xc.web.user.login;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.ResponseResult;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.utils.IStatusMessage;
import com.xc.web.utils.code.VerifyCode;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

@RestController
@TitleAnno(title = "登录模块")
public class LoginHandler {

    private static final Logger logger = LoggerFactory
            .getLogger(LoginHandler.class);
    
    //混淆变量防止密码被劫持
    static final String SYMBOL = "vercode:";

    @MethodAnno(title = "用户登陆", operation = MethodAnno.OPERA_TYPE_LOGIN)
    @RequestMapping(value = "/login.action", method = RequestMethod.POST)
    public ResponseResult login(String username, String password,
            String vercode, String remember, Map<String, Object> map,
            HttpServletResponse response, HttpServletRequest request) {
       
        ResponseResult result = new ResponseResult();
        // 获得当前Subject
        Subject currentUser = SecurityUtils.getSubject();
        // 验证用户是否验证，即是否登录
        if (!currentUser.isAuthenticated() || !currentUser.isRemembered()) {
        	String symblestr = LoginHandler.SYMBOL+vercode;
        	username = LoginSecirityUtil.decrypt( username );
            password = LoginSecirityUtil.decrypt( password );
            String msg = "";
            Integer code = IStatusMessage.SystemStatus.ERROR.getCode();
            if ( checkUserInfo( username, password, symblestr ) ) {
            	username = username.replace(symblestr, "");
            	password = password.replace(symblestr, "");
            
	            // 把用户名和密码封装为 UsernamePasswordToken 对象
	            UsernamePasswordToken token = new UsernamePasswordToken(username,
	                    password);
	
	            // remembermMe记住密码
	            token.setRememberMe("on".equals(remember));
	            try {
	                if (!vercode.equalsIgnoreCase((String) request.getSession()
	                        .getAttribute("verifyCode"))) {
	                    msg = "验证码信息错误";
	                    logger.info("验证码信息错误!!!");
	                } else {
	                    // 执行登录.
	                    currentUser.login(token);
	                    msg = "登录成功！";
	                    code = IStatusMessage.SystemStatus.SUCCESS.getCode();
	                    logger.info("登录成功!!!");
					} 
	            } catch (IncorrectCredentialsException e) {
	                msg = "登录密码错误";
	                logger.info("登录密码错误!!!" + e);
	            } catch (ExcessiveAttemptsException e) {
	                msg = "登录失败次数过多";
	                logger.info("登录失败次数过多!!!" + e);
	            } catch (LockedAccountException e) {
	                msg = "帐号已被锁定";
	                logger.info("帐号已被锁定!!!" + e);
	            } catch (DisabledAccountException e) {
	                msg = "帐号已被禁用";
	                logger.info("帐号已被禁用!!!" + e);
	            } catch (ExpiredCredentialsException e) {
	                msg = "帐号已过期";
	                logger.info("帐号已过期!!!" + e);
	            } catch (UnknownAccountException e) {
	                msg = "帐号不存在";
	                logger.info("帐号不存在!!!" + e);
	            } catch (UnauthorizedException e) {
	                msg = "您没有得到相应的授权！";
	                logger.info("您没有得到相应的授权！" + e);
	            } catch (Exception e) {
	                msg = "未知错误！";
	                logger.info("出错！！！" + e);
	            }
            } else {
            	msg = "用户凭据异常！";
            }
            result.setMsg(msg);
            result.setCode(code);
        }

        // 登录成功
        return result;

    }

	private boolean checkUserInfo(String username, String password, String vercode) {
		boolean flag = false;
		if (username.contains(vercode)||password.contains(vercode)) {
			flag = true; 
		}
		return flag;
	}

	@RequestMapping(value = "/getVerifyCodeImg", method = RequestMethod.GET)
    public void getVerifyCodeImg(HttpServletResponse response,
            HttpServletRequest request) {
        try {
            int width = 200;
            int height = 69;
            BufferedImage verifyImg = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            // 生成对应宽高的初始图片
            String randomText = VerifyCode.drawRandomText(width, height,
                    verifyImg);
            // 单独的一个类方法，出于代码复用考虑，进行了封装。
            // 功能是生成验证码字符并加上噪点，干扰线，返回值为验证码字符
            request.getSession().setAttribute("verifyCode", randomText);
            response.setContentType("image/png");// 必须设置响应内容类型为图片，否则前台不识别
            OutputStream os = response.getOutputStream(); // 获取文件输出流
            ImageIO.write(verifyImg, "png", os);// 输出图片流
            os.flush();
            os.close();// 关闭流
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

}