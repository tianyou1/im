package com.xc.web.log.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD })
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface MethodAnno {
	  /**
     * 新增
     */
    String OPERA_TYPE_PAGE = "100";
    /**
     * 新增
     */
    String OPERA_TYPE_ADD = "0";

    /**
     * 编辑
     */
    String OPERA_TYPE_EDIT = "1";

    /**
     * 删除
     */
    String OPERA_TYPE_DEL = "2";

    /**
     * 查询
     */
    String OPERA_TYPE_SELECT = "3";

    /**
     * 查看
     */
    String OPERA_TYPE_VIEW = "4";

    /**
     * 登录
     */
    String OPERA_TYPE_LOGIN = "5";

    /**
     * 退出
     */
    String OPERA_TYPE_LOGOUT = "6";

    /**
     * 修改用户密码
     */
    String OPERA_TYPE_MODIFY_PASSWORD = "7";

    public String operation() default MethodAnno.OPERA_TYPE_EDIT;

    public String title();

}