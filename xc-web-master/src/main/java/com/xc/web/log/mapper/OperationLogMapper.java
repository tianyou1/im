package com.xc.web.log.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.log.entity.OperationLog;

@Mapper
public interface OperationLogMapper extends BaseMapper<OperationLog> {

}