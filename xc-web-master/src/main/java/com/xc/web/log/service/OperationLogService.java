package com.xc.web.log.service;

import com.xc.web.base.BaseService;
import com.xc.web.log.entity.OperationLog;

public interface OperationLogService extends BaseService<OperationLog> {

}
