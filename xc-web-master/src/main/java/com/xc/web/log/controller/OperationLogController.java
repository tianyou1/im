package com.xc.web.log.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.log.entity.OperationLog;
import com.xc.web.log.service.OperationLogService;

@RestController
@RequestMapping("/operationlog")
@TitleAnno(title = "操作日志")
public class OperationLogController extends BaseController<OperationLog> {

    @Autowired
    OperationLogService operationLogService;

    @Override
    public BaseService<OperationLog> getBaseServie() {
        // TODO Auto-generated method stub
        return operationLogService;
    }

}
