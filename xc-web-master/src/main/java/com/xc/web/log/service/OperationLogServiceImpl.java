package com.xc.web.log.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.log.entity.OperationLog;
import com.xc.web.log.mapper.OperationLogMapper;

@Service
public class OperationLogServiceImpl extends BaseServiceImpl<OperationLog>
        implements OperationLogService {

    @Autowired
    private OperationLogMapper operationLogMapper;

    @Override
    public BaseMapper<OperationLog> getMapper() {
        return operationLogMapper;
    }

}
