package com.xc.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app")
public class ApplicationConfig {

    private String loginPageCaption;
    private String loginPageMainTitle;
    private String loginPageSubTitle;
    private String loginPageFooter;
    private String loginPageBackgroundVideo;
    private String loginPageBackgroundImage;
    private String mainPageCaption;
    private String mainPageMainTitle;
    private String contentTitle;
    private String sideBarFooterTitle;

    public String getLoginPageCaption() {
        return loginPageCaption;
    }

    public void setLoginPageCaption(String loginPageCaption) {
        this.loginPageCaption = loginPageCaption;
    }

    public String getLoginPageMainTitle() {
        return loginPageMainTitle;
    }

    public void setLoginPageMainTitle(String loginPageMainTitle) {
        this.loginPageMainTitle = loginPageMainTitle;
    }

    public String getLoginPageSubTitle() {
        return loginPageSubTitle;
    }

    public void setLoginPageSubTitle(String loginPageSubTitle) {
        this.loginPageSubTitle = loginPageSubTitle;
    }

    public String getLoginPageFooter() {
        return loginPageFooter;
    }

    public void setLoginPageFooter(String loginPageFooter) {
        this.loginPageFooter = loginPageFooter;
    }

    public String getLoginPageBackgroundVideo() {
        return loginPageBackgroundVideo;
    }

    public void setLoginPageBackgroundVideo(String loginPageBackgroundVideo) {
        this.loginPageBackgroundVideo = loginPageBackgroundVideo;
    }

    public String getLoginPageBackgroundImage() {
        return loginPageBackgroundImage;
    }

    public void setLoginPageBackgroundImage(String loginPageBackgroundImage) {
        this.loginPageBackgroundImage = loginPageBackgroundImage;
    }

    public String getMainPageCaption() {
        return mainPageCaption;
    }

    public void setMainPageCaption(String mainPageCaption) {
        this.mainPageCaption = mainPageCaption;
    }

    public String getMainPageMainTitle() {
        return mainPageMainTitle;
    }

    public void setMainPageMainTitle(String mainPageMainTitle) {
        this.mainPageMainTitle = mainPageMainTitle;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getSideBarFooterTitle() {
        return sideBarFooterTitle;
    }

    public void setSideBarFooterTitle(String sideBarFooterTitle) {
        this.sideBarFooterTitle = sideBarFooterTitle;
    }

}
