package com.xc.web.config.security;

import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyResolver;

public class MyEncryptablePropertyResolver
        implements EncryptablePropertyResolver {
    // 自定义解密方法
    @Override
    public String resolvePropertyValue(String s) {
        if (null != s && s.startsWith(
                MyEncryptablePropertyDetector.ENCODED_PASSWORD_HINT)) {
            return ConfigSecurityUtil.decrypt(s.substring(
                    MyEncryptablePropertyDetector.ENCODED_PASSWORD_HINT
                            .length()));
        }
        return s;
    }
}