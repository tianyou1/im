package com.xc.web.config.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang3.StringUtils;

import com.xc.web.utils.security.Sm4Util;

/**
 * @author fly
 *  配置文件安全配置工具
 */
public class ConfigSecurityUtil {

    public static final String ALGORITHM_NAME = "SM4/ECB/PKCS5Padding";
    public static final String SM4_KEY = "A0yXSBjRayOcC2E5cVwf6g==";

    /**
     * 用sm4key加密
     * 
     * @param str
     * @return
     * @throws Exception
     */
    public static String encrypt(String str) throws Exception {
        String encryptedStr = "";
        try {
            if (StringUtils.isNotBlank(str)) {
                encryptedStr = Sm4Util.getInstance().encrypt(str,
                        ALGORITHM_NAME, SM4_KEY, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedStr;
    }

    public static String decrypt(String str) {
        String decryptedStr = "";
        try {
            if (StringUtils.isNotBlank(str)) {
                decryptedStr = Sm4Util.getInstance().decrypt(str,
                        ALGORITHM_NAME, SM4_KEY, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedStr;
    }

    public static void main(String[] args)
            throws NoSuchProviderException, NoSuchAlgorithmException,
            InvalidKeyException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, UnsupportedEncodingException,
            BadPaddingException, IllegalBlockSizeException {
        String key = Sm4Util.getInstance().generateKey(128);
        System.out.println(key);
        String str = "monty";
        String encryptedStr = Sm4Util.getInstance().encrypt(str, ALGORITHM_NAME,
                SM4_KEY, "");
        System.out.println(encryptedStr);
        String decryptedStr = Sm4Util.getInstance().decrypt(encryptedStr,
                ALGORITHM_NAME, SM4_KEY, "");
        System.out.println(decryptedStr);
    }

}
