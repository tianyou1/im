package com.xc.web.base;

import java.util.Date;

public class BaseEntity {

    private Integer id;

    private Integer status;

    private String remark;

    private Integer accessLockCode;

    private Integer updateBy;

    private String updateByAddress;

    private Date updateTime;

    private Integer createBy;

    private String createByAddress;

    private Date createTime;

    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getAccessLockCode() {
        return accessLockCode;
    }

    public void setAccessLockCode(Integer accessLockCode) {
        this.accessLockCode = accessLockCode;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateByAddress() {
        return updateByAddress;
    }

    public void setUpdateByAddress(String updateByAddress) {
        this.updateByAddress = updateByAddress == null ? null
                : updateByAddress.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public String getCreateByAddress() {
        return createByAddress;
    }

    public void setCreateByAddress(String createByAddress) {
        this.createByAddress = createByAddress == null ? null
                : createByAddress.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
