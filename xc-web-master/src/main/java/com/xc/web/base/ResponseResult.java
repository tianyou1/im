package com.xc.web.base;

import java.io.Serializable;

import com.xc.web.utils.IStatusMessage;

public class ResponseResult implements Serializable {

    private static final long serialVersionUID = 7285065610386199394L;

    /**
     * 返回值
     */
    private Integer code;
    /**
     * 提示信息
     */
    private String msg;
    /**
     * 返回数据内容
     */
    private Object data;

    public ResponseResult() {
        this.code = IStatusMessage.SystemStatus.SUCCESS.getCode();
        this.msg = IStatusMessage.SystemStatus.SUCCESS.getMessage();
    }

    public ResponseResult(IStatusMessage statusMessage) {
        this.code = statusMessage.getCode();
        this.msg = statusMessage.getMessage();

    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
