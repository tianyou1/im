package com.xc.web.base;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.user.entity.User;
import com.xc.web.utils.NetWorkUtil;

public abstract class BaseController<T extends BaseEntity> {

    public abstract BaseService<T> getBaseServie();

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryList", method = RequestMethod.POST)
    public PageDataResult queryList(@RequestParam("page") int page,
            @RequestParam("limit") int limit, T record,
            HttpServletResponse response, HttpServletRequest request) {
        return getBaseServie().queryList(page, limit, record);
    }

    @MethodAnno(title = "查询", operation = MethodAnno.OPERA_TYPE_SELECT)
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET)
    public ResponseResult queryList(HttpServletResponse response,
            HttpServletRequest request) {
        return getBaseServie().queryAll();
    }

    @MethodAnno(title = "查看", operation = MethodAnno.OPERA_TYPE_VIEW)
    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public ResponseResult read(@RequestParam("id") Integer id,
            HttpServletResponse response, HttpServletRequest request) {
        return getBaseServie().selectByPrimaryKey(id);
    }

    @MethodAnno(title = "新增", operation = MethodAnno.OPERA_TYPE_ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseResult add(T record, HttpServletResponse response,
            HttpServletRequest request) {
        User currentUser = (User) SecurityUtils.getSubject().getPrincipal();
        record.setCreateBy(currentUser.getId());
        record.setCreateTime(new Date());
        record.setCreateByAddress(NetWorkUtil.getIP(request));
        record.setUpdateBy(currentUser.getId());
        record.setUpdateTime(new Date());
        record.setUpdateByAddress(NetWorkUtil.getIP(request));
        record.setVersion(1);
        return getBaseServie().add(record);
    }

    @MethodAnno(title = "修改", operation = MethodAnno.OPERA_TYPE_EDIT)
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseResult update(T record, HttpServletResponse response,
            HttpServletRequest request) {
        User currentUser = (User) SecurityUtils.getSubject().getPrincipal();
        record.setUpdateBy(currentUser.getId());
        record.setUpdateTime(new Date());
        record.setUpdateByAddress(NetWorkUtil.getIP(request));
        record.setVersion(record.getVersion() + 1);
        return getBaseServie().update(record);
    }

    @MethodAnno(title = "删除", operation = MethodAnno.OPERA_TYPE_DEL)
    @RequestMapping(value = "/batchdel", method = RequestMethod.POST)
    public ResponseResult batchdel(@RequestParam("delIds[]") Integer[] delIds,
            HttpServletResponse response, HttpServletRequest request) {
        return getBaseServie().deleteByPrimaryKeys(delIds);
    }

}
