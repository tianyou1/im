package com.xc.web.base;

import java.util.List;

public interface BaseMapper<T extends BaseEntity> {

    int deleteByPrimaryKey(Integer id);

    int insert(T record);

    int insertSelective(T record);

    T selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

    int deleteByPrimaryKeys(Integer[] delIds);

    List<T> queryList(T record);

}
