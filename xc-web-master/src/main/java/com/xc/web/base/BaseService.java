package com.xc.web.base;

public interface BaseService<T extends BaseEntity> {

    PageDataResult queryList(int page, int limit, T record);

    ResponseResult queryAll();

    ResponseResult deleteByPrimaryKeys(Integer[] delIds);

    ResponseResult selectByPrimaryKey(Integer id);

    ResponseResult add(T record);

    ResponseResult update(T record);

}
