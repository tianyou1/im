package com.xc.web.base;

import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xc.web.utils.IStatusMessage;

public abstract class BaseServiceImpl<T extends BaseEntity> {

    public abstract BaseMapper<T> getMapper();

    public PageDataResult queryList(int page, int limit, T record) {

        PageDataResult pdr = new PageDataResult();
        PageHelper.startPage(page, limit);
        List<T> list = getMapper().queryList(record);
        // 获取分页查询后的数据
        PageInfo<T> pageInfo = new PageInfo<>(list);
        // 获取总记录数total
        int total = Long.valueOf(pageInfo.getTotal()).intValue();
        if ( 0==total ) {
            pdr.setCount(0);
            pdr.setData(null);
            pdr.setCode(-1);
            pdr.setMsg("未查询到数据！");
        } else {
            pdr.setCount(total);
            pdr.setData(list);
            pdr.setCode(0);
            pdr.setMsg("查询成功！");
        }
        return pdr;
    }

    public ResponseResult queryAll() {
        ResponseResult rs = new ResponseResult();
        List<T> list = getMapper().queryList(null);
        rs.setData(list);
        rs.setCode(0);
        return rs;
    }

    public ResponseResult selectByPrimaryKey(Integer id) {
        ResponseResult rpr = new ResponseResult();
        T record = getMapper().selectByPrimaryKey(id);
        if ( null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("读取失败，根据id" + id + "未找到数据！");
        } else {
            rpr.setData(record);
            rpr.setMsg("读取成功！");
        }
        return rpr;
    }

    public ResponseResult add(T record) {
        ResponseResult rpr = new ResponseResult();
        int num = getMapper().insertSelective(record);
        if (1 == num) {
            rpr.setMsg("新增成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("新增失败，请刷新页面后重试！");
        }
        return rpr;
    }

    public ResponseResult update(T record) {
        ResponseResult rpr = new ResponseResult();
        if (null == record.getId()) {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("需要修改的数据id不能为空");
        } else {
            int num = getMapper().updateByPrimaryKeySelective(record);
            if (1 == num) {
                rpr.setMsg("修改成功！");
            } else {
                rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
                rpr.setMsg("修改异常！");
            }
        }
        return rpr;
    }

    public ResponseResult deleteByPrimaryKeys(Integer[] delIds) {
        ResponseResult rpr = new ResponseResult();
        int num = getMapper().deleteByPrimaryKeys(delIds);
        if (num > 0) {
            rpr.setMsg("删除成功！");
        } else {
            rpr.setCode(IStatusMessage.SystemStatus.ERROR.getCode());
            rpr.setMsg("删除异常！");
        }
        return rpr;
    }
}
