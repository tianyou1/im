package com.xc.web.department.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.department.entity.Department;

@Mapper
public interface DepartmentMapper extends BaseMapper<Department> {

}