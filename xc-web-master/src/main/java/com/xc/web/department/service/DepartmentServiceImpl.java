package com.xc.web.department.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.department.entity.Department;
import com.xc.web.department.mapper.DepartmentMapper;

@Service
public class DepartmentServiceImpl extends BaseServiceImpl<Department>
        implements DepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public BaseMapper<Department> getMapper() {
        return departmentMapper;
    }

}
