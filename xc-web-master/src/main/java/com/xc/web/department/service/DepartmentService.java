package com.xc.web.department.service;

import com.xc.web.base.BaseService;
import com.xc.web.department.entity.Department;

public interface DepartmentService extends BaseService<Department> {

}
