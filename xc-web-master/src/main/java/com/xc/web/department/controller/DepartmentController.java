package com.xc.web.department.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.department.entity.Department;
import com.xc.web.department.service.DepartmentService;
import com.xc.web.log.annotation.TitleAnno;

@RestController
@RequestMapping("/department")
@TitleAnno(title = "部门管理")
public class DepartmentController extends BaseController<Department> {

    @Autowired
    DepartmentService departmentService;

    @Override
    public BaseService<Department> getBaseServie() {
        // TODO Auto-generated method stub
        return departmentService;
    }

}
