package com.xc.web.page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pages")
public class DefaultPage {

    private static final Logger logger = LoggerFactory
            .getLogger(DefaultPage.class);

    @RequestMapping("/{page}")
    public String toPage1(@PathVariable("page") String page) {
        logger.debug("-------------toindex------------" + page);
        return page;
    }

    @RequestMapping("/{page}/{page2}")
    public String toPage2(@PathVariable("page") String page,
            @PathVariable("page2") String page2) {
        logger.debug("-------------toindex------------" + page);
        return page + "/" + page2;
    }

    @RequestMapping("/{page}/{page2}/{page3}")
    public String toPage32(@PathVariable("page") String page,
            @PathVariable("page2") String page2,
            @PathVariable("page3") String page3) {
        logger.debug("-------------toindex------------" + page);
        return page + "/" + page2 + "/" + page3;
    }

}
