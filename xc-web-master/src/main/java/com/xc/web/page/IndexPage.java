package com.xc.web.page;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xc.web.config.ApplicationConfig;
import com.xc.web.user.entity.User;

@Controller
public class IndexPage {

    private static final Logger logger = LoggerFactory
            .getLogger(IndexPage.class);

    @Autowired
    ApplicationConfig applicationConfig;

    @RequestMapping(value = { "/", "/login", "/index" })
    public String login(Model model) {
        Subject currentUser = SecurityUtils.getSubject();
        // 验证用户是否验证，即是否登录
        if (currentUser.isAuthenticated() || currentUser.isRemembered()) {
            logger.debug("-------------index------------");
            User loginUser = (User) currentUser.getPrincipal();
            model.addAttribute("currentUserName", loginUser.getName());
            model.addAttribute("mainPageCaption",
                    applicationConfig.getMainPageCaption());
            model.addAttribute("mainPageMainTitle",
                    applicationConfig.getMainPageMainTitle());
            model.addAttribute("contentTitle",
                    applicationConfig.getContentTitle());
            model.addAttribute("sideBarFooterTitle",
                    applicationConfig.getSideBarFooterTitle());
            return "index";
        } else {
            logger.debug("-------------login------------");
            model.addAttribute("loginPageCaption",
                    applicationConfig.getLoginPageCaption());
            model.addAttribute("loginPageMainTitle",
                    applicationConfig.getLoginPageMainTitle());
            model.addAttribute("loginPageSubTitle",
                    applicationConfig.getLoginPageSubTitle());
            model.addAttribute("loginPageFooter",
                    applicationConfig.getLoginPageFooter());
            return "user/login";
        }
    }

    @RequestMapping("/forget")
    public String forget(Model model) {
        logger.debug("-------------login------------");
        model.addAttribute("loginPageCaption",
                applicationConfig.getLoginPageCaption());
        model.addAttribute("loginPageMainTitle",
                applicationConfig.getLoginPageMainTitle());
        model.addAttribute("loginPageSubTitle",
                applicationConfig.getLoginPageSubTitle());
        model.addAttribute("loginPageFooter",
                applicationConfig.getLoginPageFooter());
        return "user/forget";
    }

    @RequestMapping("/reg")
    public String reg(Model model) {
        logger.debug("-------------login------------");
        model.addAttribute("loginPageCaption",
                applicationConfig.getLoginPageCaption());
        model.addAttribute("loginPageMainTitle",
                applicationConfig.getLoginPageMainTitle());
        model.addAttribute("loginPageSubTitle",
                applicationConfig.getLoginPageSubTitle());
        model.addAttribute("loginPageFooter",
                applicationConfig.getLoginPageFooter());
        return "user/reg";
    }

}
