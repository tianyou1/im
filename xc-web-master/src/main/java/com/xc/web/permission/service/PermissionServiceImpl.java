package com.xc.web.permission.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xc.web.base.BaseMapper;
import com.xc.web.base.BaseServiceImpl;
import com.xc.web.permission.entity.Permission;
import com.xc.web.permission.mapper.PermissionMapper;

@Service
public class PermissionServiceImpl extends BaseServiceImpl<Permission>
        implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public BaseMapper<Permission> getMapper() {
        return permissionMapper;
    }

}
