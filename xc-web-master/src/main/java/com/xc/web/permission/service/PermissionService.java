package com.xc.web.permission.service;

import com.xc.web.base.BaseService;
import com.xc.web.permission.entity.Permission;

public interface PermissionService extends BaseService<Permission> {
}
