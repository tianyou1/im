package com.xc.web.permission.mapper;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;

import com.xc.web.base.BaseMapper;
import com.xc.web.permission.entity.Permission;

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> getUserPerms(Integer id);

    Set<String> getUserPermsCode(Integer id);

}