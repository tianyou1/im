package com.xc.web.permission.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xc.web.base.BaseController;
import com.xc.web.base.BaseService;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.permission.entity.Permission;
import com.xc.web.permission.service.PermissionService;

@RestController
@RequestMapping("/permission")
@TitleAnno(title = "菜单管理")
public class PermissionController extends BaseController<Permission> {

    @Autowired
    PermissionService permissionService;

    @Override
    public BaseService<Permission> getBaseServie() {
        // TODO Auto-generated method stub
        return permissionService;
    }

}
