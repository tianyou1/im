package com.xc.web.interceptor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.xc.web.log.annotation.MethodAnno;
import com.xc.web.log.annotation.TitleAnno;
import com.xc.web.log.entity.OperationLog;
import com.xc.web.log.service.OperationLogService;
import com.xc.web.user.entity.User;
import com.xc.web.utils.NetWorkUtil;

@Component
public class OperationLogInterceptor implements HandlerInterceptor {
	
	private static final Logger logger = LoggerFactory
            .getLogger(OperationLogInterceptor.class);

    @Autowired
    OperationLogService operationLogService;

    /**
     * 该方法将在整个请求结束之后，也就是在DispatcherServlet 渲染了对应的视图之后执行。 这个方法的主要作用是用于进行资源清理工作的
     * 
     * @param request
     * @param response
     * @param handler
     * @param e
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception e)
            throws Exception {
        try {
        	logger.debug("LogInterceptor afterCompletion");
            if (handler instanceof HandlerMethod) {
                HandlerMethod handlerMethod = (HandlerMethod) handler;
                TitleAnno titleAnno = handlerMethod.getBean().getClass()
                        .getAnnotation(TitleAnno.class);
                MethodAnno methodAnno = handlerMethod.getMethod()
                        .getAnnotation(MethodAnno.class);
                if (null != methodAnno && null != titleAnno) {
                    // 获取操作系统等信息
                    StringBuffer sb = new StringBuffer();
                    sb.append("操作系统名称:" + System.getProperty("os.name"));// 操作系统名称
                    sb.append("操作系统构架" + System.getProperty("os.arch"));// 操作系统构架
                    sb.append("操作系统版本" + System.getProperty("os.version"));// 操作系统版本
                    // String userAgent = request.getHeader("User-Agent");
                    OperationLog operationLog = new OperationLog();

                    Map<String, String[]> map = request.getParameterMap();
                    String paramsData = JSONObject.toJSONString(map);
                    User currentUser = (User) SecurityUtils.getSubject()
                            .getPrincipal();
                    operationLog
                            .setOperatorOrgid(currentUser.getDepartmentId());
                    operationLog.setOperatorId(currentUser.getId());
                    operationLog.setOperatorName(currentUser.getName());
                    if (MethodAnno.OPERA_TYPE_LOGIN
                            .endsWith(methodAnno.operation())) {
                        operationLog.setModuleName("");
                    } else {
                        operationLog.setModuleName(titleAnno.title());
                    }
                    operationLog.setOperationType(methodAnno.title());
                    operationLog.setRequestUrl(request.getRequestURI());
                    operationLog.setUserAgent(sb.toString());
                    operationLog.setRequestMethod(methodAnno.title());
                    operationLog.setOperationParams(paramsData);

                    operationLog.setCreateBy(currentUser.getId());
                    operationLog.setCreateTime(new Date());
                    operationLog.setCreateByAddress(NetWorkUtil.getIP(request));
                    operationLog.setUpdateBy(currentUser.getId());
                    operationLog.setUpdateTime(new Date());
                    operationLog.setUpdateByAddress(NetWorkUtil.getIP(request));
                    operationLog.setVersion(1);

                    /**
                     * 解决request参数过长，数据库字段空间不足，无法存储
                     * 
                     * @author CMH 2017/07/28
                     */
                    if (paramsData.length() >= 2000) {
                        operationLog
                                .setOperationParams("the params are too long!");
                    }

                    // setOperateContent(operationLog, methodAnno.operation(),
                    // request);// 根据模块设置操作内容
                    // 如果controller报错，则记录异常错误
                    if (e != null) {
                        operationLog.setExceptionMsg(getStackTraceAsString(e));
                        operationLog.setOperationStatus("失败");
                    } else {
                        operationLog.setOperationStatus("成功");
                    }
                    try {
                        operationLogService.add(operationLog);
                    } catch (Exception e1) {
                    	logger.error("操作日志记录异常："+getStackTraceAsString(e1));
                    }
                }
            }
        } catch (Exception el) {
            el.printStackTrace();
        }
    }

    /**
     * 该方法在目标方法调用之后，渲染视图之前被调用； 可以对请求域中的属性或视图做出修改
     * 
     */
    @Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object obj, ModelAndView model)
            throws Exception {
    }

    /**
     * 可以考虑作权限，日志，事务等等 该方法在目标方法调用之前被调用； 若返回TURE,则继续调用后续的拦截器和目标方法
     * 若返回FALSE,则不会调用后续的拦截器和目标方法
     * 
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object object) throws Exception {
        return true;
    }

    /**
     * 将ErrorStack转化为String.
     */
    public static String getStackTraceAsString(Throwable e) {
        if (e == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    /**
     * 根据模块设置操作内容
     * 
     * @param log
     * @param operation
     * @return void
     * @throws ServiceException
     */
    @SuppressWarnings("unused")
    private void setOperateContent(OperationLog log, String operation,
            HttpServletRequest request) {
        switch (operation) {
        case MethodAnno.OPERA_TYPE_LOGIN: // 用户登陆系统
            log.setModuleName("登录系统");
            break;
        default:
            break;
        }
    }
}
